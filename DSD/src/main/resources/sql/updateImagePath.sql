select id, name from menus;

update menus
set imagePath = "fa ion-ios-location fa-stack-1x fa-inverse" 
where id = 1; 

update menus
set imagePath = "fa ion-ios-pulse-strong fa-stack-1x fa-inverse" 
where id = 2; 

update menus
set imagePath = "fa fa-table fa-stack-1x fa-inverse" 
where id = 3; 

update menus
set imagePath = "fa fa-line-chart fa-stack-1x fa-inverse" 
where id = 4; 

update menus
set imagePath = "fa ion-ios-pulse-strong fa-stack-1x fa-inverse" 
where id = 5; 

update menus
set imagePath = "fa fa-table fa-stack-1x fa-inverse" 
where id = 6; 

update menus
set imagePath = "fa fa-line-chart fa-stack-1x fa-inverse" 
where id = 7; 

update menus
set imagePath = "fa fa-bell fa-stack-1x fa-inverse" 
where id = 8; 

update menus
set imagePath = "fa fa-wrench fa-stack-1x fa-inverse" 
where id = 9;

update menus
set imagePath = "fa fa-sliders fa-stack-1x fa-inverse" 
where id = 10;

update menus
set imagePath = "fa fa-globe fa-stack-1x fa-inverse" 
where id = 11;

update menus
set imagePath = "fa ion-pricetags fa-stack-1x fa-inverse" 
where id = 12;

update menus
set imagePath = "fa fa-table fa-stack-1x fa-inverse" 
where id = 13;

update menus
set imagePath = "fa fa-sitemap fa-stack-1x fa-inverse" 
where id = 14;

update menus
set imagePath = "fa ion-person-add fa-stack-1x fa-inverse" 
where id = 15;

update menus
set imagePath = "fa ion-ios-people fa-stack-1x fa-inverse" 
where id = 16;

update menus
set imagePath = "fa fa-heartbeat fa-stack-1x fa-inverse" 
where id = 19;

update menus
set imagePath = "fa fa-object-group fa-stack-1x fa-inverse" 
where id = 20;

update menus
set imagePath = "fa fa-object-group fa-stack-1x fa-inverse" 
where id = 21;

update modules
set backgroundImage = 'linear-gradient(to right, #3a9eff, #a5d3ff)'  
where id = 1; 

update modules
set backgroundImage = 'linear-gradient(to right, #e20f47, #ffb2c6)'  
where id = 2; 

update modules
set backgroundImage = 'linear-gradient(to right, #ffdd00, #fff6bf)'  
where id = 3; 

update modules
set backgroundImage = 'linear-gradient(to right, #99ff26, #dbffb2)'  
where id = 4; 

update modules
set backgroundImage = 'linear-gradient(to right, #9b9b9b, #dbd9d9)'  
where id = 5; 

update modules
set backgroundImage = 'linear-gradient(to right, #cb38d8, #eba2f2)'  
where id = 6; 

