update menus
set imagePath = 'img/shortCut/floorPlanView.png',
mobileImagePath = 'img/mobile/mapView.svg'
where ID = 1;

update menus
set imagePath = 'img/shortCut/tagSensorView.png',
mobileImagePath = 'img/mobile/tagSensorView.svg',
mobileSupport=false
where ID = 2;

update menus
set imagePath = 'img/shortCut/tableView.png',
mobileImagePath = 'img/mobile/tableView.svg',
mobileSupport=true
where ID = 3;

update menus
set imagePath = 'img/shortCut/chartView.png',
mobileImagePath = 'img/mobile/chartView.svg'
where ID = 4;

update menus
set imagePath = 'img/shortCut/diagnosticSensorView.png',
mobileImagePath = 'img/mobile/diagnosticSensorView.svg',
mobileSupport=true
where ID = 5;

update menus
set imagePath = 'img/shortCut/diagnostiTableView.png',
mobileImagePath = 'img/mobile/diagnosticTableView.svg',
mobileSupport=true
where ID = 6;

update menus
set imagePath = 'img/shortCut/diagnosticChartView.png',
mobileImagePath = 'img/mobile/diagnosticChartView.svg',
mobileSupport=true
where ID = 7;

update menus
set imagePath = 'img/shortCut/alertHistory.png',
mobileImagePath = 'img/mobile/alertHistory.svg'
where ID = 8;

update menus
set imagePath = 'img/shortCut/alertRuleSettings.png'
where ID = 9;

update menus
set imagePath = 'img/shortCut/alertCriteria.png'
where ID = 10;


update menus
set imagePath = 'img/shortCut/siteConfiguration.png'
where ID = 11;

update menus
set imagePath = 'img/shortCut/tagConfiguration.png'
where ID = 12;

update menus
set imagePath = 'img/shortCut/networkTableView.png'
where ID = 13;

update menus
set imagePath = 'img/shortCut/networkDiagramView.png'
where ID = 14;

update menus
set imagePath = 'img/shortCut/usersView.png'
where ID = 15;

update menus
set SIDEMENU = true; 

update modules 
set mobileSupport=true
where id = 2;

update modules
set MOBILESUPPORT = true 
where name = 'SYSTEM_CONFIGURATION';

update menus
set mobilesupport = true
where MODULE_ID = (select id from modules where name = 'SYSTEM_CONFIGURATION')
and name = 'TAG_CONFIGURATION';

update menus
set MOBILEBACKGROUNDIMAGE = 'border-color:#3a9eff;background-image:radial-gradient(#3a9eff, #3a9eff, #a5d3ff);'
where MODULE_ID = (select id from modules where name = 'VIEW');

update menus
set MOBILEBACKGROUNDIMAGE = 'border-color:#e20f47;background-image:radial-gradient(#e20f47, #e20f47, #ffb2c6);'
where MODULE_ID = (select id from modules where name = 'DIAGNOSTIC');

update menus
set MOBILEBACKGROUNDIMAGE = 'border-color:#ffdd00;background-image:radial-gradient(#ffdd00, #ffdd00, #fff6bf);'
where MODULE_ID = (select id from modules where name = 'ALERT');

update menus
set MOBILEBACKGROUNDIMAGE = 'border-color:#99ff26;background-image:radial-gradient(#99ff26, #99ff26, #dbffb2);'
where MODULE_ID = (select id from modules where name = 'SYSTEM_CONFIGURATION');
