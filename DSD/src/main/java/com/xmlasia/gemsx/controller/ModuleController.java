package com.xmlasia.gemsx.controller;

import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.domain.Image;
import com.xmlasia.gemsx.domain.Module;
import com.xmlasia.gemsx.service.ModuleService;

@Path("/module")
@Singleton
public class ModuleController {

	private static Logger logger = Logger.getLogger(ModuleController.class);

	private ModuleService moduleService;
	
	public ModuleController(@Context ServletContext ctx) {
		moduleService = (ModuleService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("moduleService");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<Module> searchData(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("description") String description, @QueryParam("displayOrder") Integer displayOrder,
			@QueryParam("homepage") Boolean homepage, @QueryParam("active") Boolean active, @QueryParam("path") String path, @QueryParam("useOrder") Boolean useOrder) {
	
		List<Module> result = moduleService.searchByCriteria(id, name, description, displayOrder, homepage, active, path, useOrder);
		return result;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findModuleById")
	public Module findModuleById(@QueryParam("id") Long id) {
		Module result = moduleService.retrieveByModuleId(id);
		return result;
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteData")
	public Module deleteModule(@QueryParam("id") Long id) {
		return moduleService.deleteModule(id);
	}

	@POST
	@Path("/addData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Module addModule(@FormDataParam("name") String name,
			@FormDataParam("description") String description, @FormDataParam("displayOrder") Integer displayOrder, @FormDataParam("homepage") Boolean homepage,
			@FormDataParam("active") Boolean active, @FormDataParam("path") String path, @FormDataParam("fileName") String fileName, @FormDataParam("file") InputStream fileData) {
		
		Module module = new Module();
		module.setName(name);
		module.setDescription(description);
		module.setDisplayOrder(displayOrder);
		module.setActive(active);
		module.setPath(path);
		module.setHomepage(homepage);
		
		Module newModule = moduleService.createModule(module);
		return newModule;
	}
	
	@POST
	@Path("/updateData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Module updateModule(
			@FormDataParam("id") Integer id, 
			@FormDataParam("name") String name,
			@FormDataParam("description") String description, 
			@FormDataParam("displayOrder") Integer displayOrder, 
			@FormDataParam("homepage") Boolean homepage,
			@FormDataParam("active") Boolean active, 
			@FormDataParam("path") String path, 
			@FormDataParam("fileName") String fileName, 
			@FormDataParam("file") InputStream fileData) {
		
		Module module = moduleService.retrieveByModuleId(id);
		module.setName(name);
		module.setDescription(description);
		module.setDisplayOrder(displayOrder);
		module.setActive(active);
		module.setPath(path);
		module.setHomepage(homepage);
		
	   	Module updatedModule = moduleService.updateModule(module);
		return updatedModule;
	}
}
