package com.xmlasia.gemsx.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.AlertCriteriaDao;
import com.xmlasia.gemsx.dao.AlertSettingDao;
import com.xmlasia.gemsx.domain.AlertCriteria;
import com.xmlasia.gemsx.domain.AlertSetting;


@Path("/alertCriteria")
@Singleton
public class AlertCriteriaController {

	private static Logger logger = Logger.getLogger(AlertSettingController.class);
	
	private AlertSettingDao alertSettingDao;
	private AlertCriteriaDao alertCriteriaDao;
	
	public AlertCriteriaController(@Context ServletContext ctx) {
		alertSettingDao = (AlertSettingDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("alertSettingDao");
		alertCriteriaDao = (AlertCriteriaDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("alertCriteriaDao");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAlertCriteria")
	public List<AlertCriteria> searchData(
			@QueryParam("id") String id, 
			@QueryParam("name") String name,
			@QueryParam("isCoverLevel") String isCoverLevel, 
			@QueryParam("isWaterLevel") String isWaterLevel, 
			@QueryParam("isH2s") String isH2s, 
			@QueryParam("isSo2") String isSo2, 
			@QueryParam("isCo2") String isCo2,
			@QueryParam("isCh4") String isCh4, 
			@QueryParam("isNh3") String isNh3,
			@QueryParam("isO2") String isO2 ) { 
		
		return alertCriteriaDao.searchByCriteria(id, name, isCoverLevel, isWaterLevel, isH2s, isSo2, isCo2, isCh4, isNh3, isO2 );
	}
	
	@POST
	@Path("/addAlertCriteria")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public AlertCriteria addAlertSetting(
			@FormDataParam("name") String name,
			@FormDataParam("isCoverLevel") String isCoverLevel, 
			@FormDataParam("coverLevelCriteria") String coverLevelCriteria, 
		
			@FormDataParam("isWaterLevel") String isWaterLevel, 
			@FormDataParam("waterLevelCriteria") String waterLevelCriteria, 

			@FormDataParam("isH2s") String isH2s, 
			@FormDataParam("h2sCriteria") String h2sCriteria, 

			@FormDataParam("isSo2") String isSo2, 
			@FormDataParam("so2Criteria") String so2Criteria, 
			
			@FormDataParam("isCo2") String isCo2, 
			@FormDataParam("co2Criteria") String co2Criteria, 

			@FormDataParam("isCh4") String isCh4, 
			@FormDataParam("ch4Criteria") String ch4Criteria, 
	
			@FormDataParam("isNh3") String isNh3, 
			@FormDataParam("nh3Criteria") String nh3Criteria, 

			@FormDataParam("isO2") String isO2, 
			@FormDataParam("o2Criteria") String o2Criteria, 
			
			@FormDataParam("isNotCoverLevelBetween") String isNotCoverLevelBetween,
			@FormDataParam("isNotWaterLevelBetween") String isNotWaterLevelBetween,
			@FormDataParam("isNotH2sBetween") String isNotH2sBetween,
			@FormDataParam("isNotSo2Between") String isNotSo2Between,
			@FormDataParam("isNotCo2Between") String isNotCo2Between,
			@FormDataParam("isNotCh4Between") String isNotCh4Between,
			@FormDataParam("isNotNh3Between") String isNotNh3Between,
			@FormDataParam("isNotO2Between") String isNotO2Between,
			
			@FormDataParam("maxCoverLevelValue") String maxCoverLevelValue,
			@FormDataParam("maxWaterLevelValue") String maxWaterLevelValue,
			@FormDataParam("maxH2sValue") String maxH2sValue,
			@FormDataParam("maxSo2Value") String maxSo2Value,
			@FormDataParam("maxCo2Value") String maxCo2Value,			
			@FormDataParam("maxCh4Value") String maxCh4Value,
			@FormDataParam("maxNh3Value") String maxNh3Value,
			@FormDataParam("maxO2Value") String maxO2Value,
			
			@FormDataParam("minCoverLevelValue") String minCoverLevelValue,
			@FormDataParam("minWaterLevelValue") String minWaterLevelValue,
			@FormDataParam("minH2sValue") String minH2sValue,
			@FormDataParam("minSo2Value") String minSo2Value,
			@FormDataParam("minCo2Value") String minCo2Value,			
			@FormDataParam("minCh4Value") String minCh4Value,
			@FormDataParam("minNh3Value") String minNh3Value,
			@FormDataParam("minO2Value") String minO2Value, 
			
			@FormDataParam("isCoverDetect") String isCoverDetect,
			@FormDataParam("coverDetect") String coverDetect,
			@FormDataParam("coverDetectOpenOrCloseStr") String coverDetectOpenOrCloseStr
			) {
		
		List<AlertCriteria> alertCriterias = searchData(null, name,null,null,null,null,null,null,null,null);
		if ( alertCriterias != null && alertCriterias.size() > 0) {
			AlertCriteria validationObject = new AlertCriteria();
			validationObject.setValidationSubject("New Alert Criteria");
			validationObject.setFailedValidationMessage(String.format("Name:%s already used by existing alert criteria", name));
			return validationObject;
		}
		
		AlertCriteria alertCriteria = new AlertCriteria();
		alertCriteria.setName(name);
	
		alertCriteria.setIsNotCoverLevelBetween(Boolean.valueOf(isNotCoverLevelBetween));
		alertCriteria.setIsNotWaterLevelBetween(Boolean.valueOf(isNotWaterLevelBetween));
		alertCriteria.setIsNotH2sBetween(Boolean.valueOf(isNotH2sBetween));
		alertCriteria.setIsNotSo2Between(Boolean.valueOf(isNotSo2Between));
		alertCriteria.setIsNotCo2Between(Boolean.valueOf(isNotCo2Between));
		alertCriteria.setIsNotCh4Between(Boolean.valueOf(isNotCh4Between));
		alertCriteria.setIsNotNh3Between(Boolean.valueOf(isNotNh3Between));		
		alertCriteria.setIsNotO2Between(Boolean.valueOf(isNotO2Between));
		
		alertCriteria.setMaxCoverLevelValue(Integer.valueOf(maxCoverLevelValue));
		alertCriteria.setMaxWaterLevelValue(Integer.valueOf(maxWaterLevelValue));
		alertCriteria.setMaxH2sValue(Double.valueOf(maxH2sValue));
		alertCriteria.setMaxSo2Value(Double.valueOf(maxSo2Value));
		alertCriteria.setMaxCo2Value(Double.valueOf(maxCo2Value));
		alertCriteria.setMaxCh4Value(Double.valueOf(maxCh4Value));
		alertCriteria.setMaxNh3Value(Double.valueOf(maxNh3Value));
		alertCriteria.setMaxO2Value(Double.valueOf(maxO2Value));
		
		alertCriteria.setMinCoverLevelValue(Integer.valueOf(minCoverLevelValue));
		alertCriteria.setMinWaterLevelValue(Integer.valueOf(minWaterLevelValue));
		alertCriteria.setMinH2sValue(Double.valueOf(minH2sValue));
		alertCriteria.setMinSo2Value(Double.valueOf(minSo2Value));
		alertCriteria.setMinCo2Value(Double.valueOf(minCo2Value));
		alertCriteria.setMinCh4Value(Double.valueOf(minCh4Value));
		alertCriteria.setMinNh3Value(Double.valueOf(minNh3Value));
		alertCriteria.setMinO2Value(Double.valueOf(minO2Value));
		
	
		alertCriteria.setIsCoverLevel(Boolean.valueOf(isCoverLevel));
		alertCriteria.setIsWaterLevel(Boolean.valueOf(isWaterLevel));
		alertCriteria.setIsH2s(Boolean.valueOf(isH2s));
		alertCriteria.setIsSo2(Boolean.valueOf(isSo2));
		alertCriteria.setIsCo2(Boolean.valueOf(isCo2));
		alertCriteria.setIsCh4(Boolean.valueOf(isCh4));
		alertCriteria.setIsNh3(Boolean.valueOf(isNh3));
		alertCriteria.setIsO2(Boolean.valueOf(isO2));

		alertCriteria.setIsCoverDetect(Boolean.valueOf(isCoverDetect));
		alertCriteria.setCoverDetect(Boolean.valueOf(coverDetect));
		alertCriteria.setCoverDetectOpenOrCloseStr(coverDetectOpenOrCloseStr);
			
		populateAlertCriteria(alertCriteria);
		
		AlertCriteria newAlertCriteria = (AlertCriteria)alertSettingDao.create(alertCriteria);
		newAlertCriteria.setValidationSubject("New Alert Criteria");
		newAlertCriteria.setValidationMessages("Save alert criteria success");
		return newAlertCriteria;
	}

	private void populateAlertCriteria(AlertCriteria alertCriteria) {
		
		if (alertCriteria.getIsCoverLevel()) {
			if (alertCriteria.getIsNotCoverLevelBetween()) {
				String cond1 = String.format("%d <= Cover level < %d", AlertCriteria.minCoverLevel, alertCriteria.getMinCoverLevelValue());
				String cond2 = String.format("%d < Cover level <= %d", alertCriteria.getMaxCoverLevelValue(), AlertCriteria.maxCoverLevel);
				alertCriteria.setCoverLevelCriteria(cond1 + ";" + cond2);
			} else {
				String cond1 = String.format("%d <= Cover level <= %d", alertCriteria.getMinCoverLevelValue(), alertCriteria.getMaxCoverLevelValue());
				alertCriteria.setCoverLevelCriteria(cond1);				
			}
		} else {
			alertCriteria.setCoverLevelCriteria("");		
		}
		
		if (alertCriteria.getIsWaterLevel()) {
			if (alertCriteria.getIsNotWaterLevelBetween()) {
				String cond1 = String.format("%d <= Water level < %d", AlertCriteria.minWaterLevel, alertCriteria.getMinWaterLevelValue());
				String cond2 = String.format("%d < Water level <= %d", alertCriteria.getMaxWaterLevelValue(), AlertCriteria.maxWaterLevel);
				alertCriteria.setWaterLevelCriteria(cond1 + ";" + cond2);
			} else {
				String cond1 = String.format("%d <= Water level <= %d", alertCriteria.getMinWaterLevelValue(), alertCriteria.getMaxWaterLevelValue());
				alertCriteria.setWaterLevelCriteria(cond1);				
			}
		} else {
			alertCriteria.setWaterLevelCriteria("");		
		}
		
		if (alertCriteria.getIsH2s()) {
			if (alertCriteria.getIsNotH2sBetween()) {
				String cond1 = String.format("%.1f <= h2s < %.1f", AlertCriteria.minH2s, alertCriteria.getMinH2sValue());
				String cond2 = String.format("%.1f < h2s <= %.1f", alertCriteria.getMaxH2sValue(), AlertCriteria.maxH2s);
				alertCriteria.setH2sCriteria(cond1 + ";" + cond2);
			} else {
				String cond1 = String.format("%.1f <= h2s <= %.1f", alertCriteria.getMinH2sValue(), alertCriteria.getMaxH2sValue());
				alertCriteria.setH2sCriteria(cond1);				
			}
		} else {
			alertCriteria.setH2sCriteria("");		
		}
		
		if (alertCriteria.getIsSo2()) {
			if (alertCriteria.getIsNotSo2Between()) {
				String cond1 = String.format("%.1f <= so2 < %.1f", AlertCriteria.minSo2, alertCriteria.getMinSo2Value());
				String cond2 = String.format("%.1f < so2 <= %.1f", alertCriteria.getMaxSo2Value(), AlertCriteria.maxSo2);
				alertCriteria.setSo2Criteria(cond1 + ";" + cond2);
			} else {
				String cond1 = String.format("%.1f <= so2 <= %.1f", alertCriteria.getMinSo2Value(), alertCriteria.getMaxSo2Value());
				alertCriteria.setSo2Criteria(cond1);				
			}
		} else {
			alertCriteria.setSo2Criteria("");		
		}
		
		if (alertCriteria.getIsCo2()) {
			if (alertCriteria.getIsNotCo2Between()) {
				String cond1 = String.format("%.1f <= co2 < %.1f", AlertCriteria.minCo2, alertCriteria.getMinCo2Value());
				String cond2 = String.format("%.1f < co2 <= %.1f", alertCriteria.getMaxCo2Value(), AlertCriteria.maxCo2);
				alertCriteria.setCo2Criteria(cond1 + ";" + cond2);
			} else {
				String cond1 = String.format("%.1f <= co2 <= %.1f", alertCriteria.getMinCo2Value(), alertCriteria.getMaxCo2Value());
				alertCriteria.setCo2Criteria(cond1);				
			}
		} else {
			alertCriteria.setCo2Criteria("");		
		}
		
		if (alertCriteria.getIsCh4()) {
			if (alertCriteria.getIsNotCh4Between()) {
				String cond1 = String.format("%.1f <= ch4 < %.1f", AlertCriteria.minCh4, alertCriteria.getMinCh4Value());
				String cond2 = String.format("%.1f < ch4 <= %.1f", alertCriteria.getMaxCh4Value(), AlertCriteria.maxCh4);
				alertCriteria.setCh4Criteria(cond1 + ";" + cond2);
			} else {
				String cond1 = String.format("%.1f <= ch4 <= %.1f", alertCriteria.getMinCh4Value(), alertCriteria.getMaxCh4Value());
				alertCriteria.setCh4Criteria(cond1);				
			}
		} else {
			alertCriteria.setCh4Criteria("");		
		}
		
		if (alertCriteria.getIsNh3()) {
			if (alertCriteria.getIsNotNh3Between()) {
				String cond1 = String.format("%.1f <= nh3 < %.1f", AlertCriteria.minNh3, alertCriteria.getMinNh3Value());
				String cond2 = String.format("%.1f < nh3 <= %.1f", alertCriteria.getMaxNh3Value(), AlertCriteria.maxNh3);
				alertCriteria.setNh3Criteria(cond1 + ";" + cond2);
			} else {
				String cond1 = String.format("%.1f <= nh3 <= %.1f", alertCriteria.getMinNh3Value(), alertCriteria.getMaxNh3Value());
				alertCriteria.setNh3Criteria(cond1);				
			}
		} else {
			alertCriteria.setNh3Criteria("");		
		}
		
		if (alertCriteria.getIsO2()) {
			if (alertCriteria.getIsNotO2Between()) {
				String cond1 = String.format("%.1f <= o2 < %.1f", AlertCriteria.minO2, alertCriteria.getMinO2Value());
				String cond2 = String.format("%.1f < o2 <= %.1f", alertCriteria.getMaxO2Value(), AlertCriteria.maxO2);
				alertCriteria.setO2Criteria(cond1 + ";" + cond2);
			} else {
				String cond1 = String.format("%.1f <= o2 <= %.1f", alertCriteria.getMinO2Value(), alertCriteria.getMaxO2Value());
				alertCriteria.setO2Criteria(cond1);				
			}
		} else {
			alertCriteria.setO2Criteria("");		
		}
		
//		if (alertCriteria.getIsCoverDetect()) {
//			if ( alertCriteria.getCoverDetect()) {
//				alertCriteria.setCoverDetectCriteria("Cover Detect Closed");
//			} else {
//				alertCriteria.setCoverDetectCriteria("Cover Detect Open");
//			}
//		} else {
//			alertCriteria.setCoverDetectCriteria("");
//		}
	}

	@POST
	@Path("/updateAlertCriteria")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public AlertCriteria updateAlertSetting(
			@FormDataParam("id") String id,
			@FormDataParam("name") String name,

			@FormDataParam("isCoverLevel") String isCoverLevel, 
			@FormDataParam("coverLevelCriteria") String coverLevelCriteria, 
		
			@FormDataParam("isWaterLevel") String isWaterLevel, 
			@FormDataParam("waterLevelCriteria") String waterLevelCriteria, 

			@FormDataParam("isH2s") String isH2s, 
			@FormDataParam("h2sCriteria") String h2sCriteria, 

			@FormDataParam("isSo2") String isSo2, 
			@FormDataParam("so2Criteria") String so2Criteria, 
			
			@FormDataParam("isCo2") String isCo2, 
			@FormDataParam("co2Criteria") String co2Criteria, 

			@FormDataParam("isCh4") String isCh4, 
			@FormDataParam("ch4Criteria") String ch4Criteria, 
	
			@FormDataParam("isNh3") String isNh3, 
			@FormDataParam("nh3Criteria") String nh3Criteria, 

			@FormDataParam("isO2") String isO2, 
			@FormDataParam("o2Criteria") String o2Criteria, 
			
			@FormDataParam("isNotCoverLevelBetween") String isNotCoverLevelBetween,
			@FormDataParam("isNotWaterLevelBetween") String isNotWaterLevelBetween,
			@FormDataParam("isNotH2sBetween") String isNotH2sBetween,
			@FormDataParam("isNotSo2Between") String isNotSo2Between,
			@FormDataParam("isNotCo2Between") String isNotCo2Between,
			@FormDataParam("isNotCh4Between") String isNotCh4Between,
			@FormDataParam("isNotNh3Between") String isNotNh3Between,
			@FormDataParam("isNotO2Between") String isNotO2Between,
			
			@FormDataParam("maxCoverLevelValue") String maxCoverLevelValue,
			@FormDataParam("maxWaterLevelValue") String maxWaterLevelValue,
			@FormDataParam("maxH2sValue") String maxH2sValue,
			@FormDataParam("maxSo2Value") String maxSo2Value,
			@FormDataParam("maxCo2Value") String maxCo2Value,			
			@FormDataParam("maxCh4Value") String maxCh4Value,
			@FormDataParam("maxNh3Value") String maxNh3Value,
			@FormDataParam("maxO2Value") String maxO2Value,
			
			@FormDataParam("minCoverLevelValue") String minCoverLevelValue,
			@FormDataParam("minWaterLevelValue") String minWaterLevelValue,
			@FormDataParam("minH2sValue") String minH2sValue,
			@FormDataParam("minSo2Value") String minSo2Value,
			@FormDataParam("minCo2Value") String minCo2Value,			
			@FormDataParam("minCh4Value") String minCh4Value,
			@FormDataParam("minNh3Value") String minNh3Value,
			@FormDataParam("minO2Value") String minO2Value,
			
			@FormDataParam("isCoverDetect") String isCoverDetect,
			@FormDataParam("coverDetect") String coverDetect,
			@FormDataParam("coverDetectOpenOrCloseStr") String coverDetectOpenOrCloseStr) {
		
	
		List<AlertCriteria> alertCriterias = searchData(null, name,null,null,null,null,null,null,null,null);
		if ( alertCriterias != null && alertCriterias.size() > 0) {
			for (AlertCriteria existAlertCriteria : alertCriterias ) {
				if ( existAlertCriteria.getId() != Long.valueOf(id) ) {
					AlertCriteria validationObject = new AlertCriteria();
					validationObject.setValidationSubject("Update Alert Criteria");
					validationObject.setFailedValidationMessage(String.format("Name %s was assigned by existing alert criteria", name));
					return validationObject;
				}
			}
		}
		
		AlertCriteria alertCriteria = alertCriteriaDao.retrieveAlertCriteria(Integer.valueOf(id));
	
		alertCriteria.setIsNotCoverLevelBetween(Boolean.valueOf(isNotCoverLevelBetween));
		alertCriteria.setIsNotWaterLevelBetween(Boolean.valueOf(isNotWaterLevelBetween));
		alertCriteria.setIsNotH2sBetween(Boolean.valueOf(isNotH2sBetween));
		alertCriteria.setIsNotSo2Between(Boolean.valueOf(isNotSo2Between));
		alertCriteria.setIsNotCo2Between(Boolean.valueOf(isNotCo2Between));
		alertCriteria.setIsNotCh4Between(Boolean.valueOf(isNotCh4Between));
		alertCriteria.setIsNotNh3Between(Boolean.valueOf(isNotNh3Between));		
		alertCriteria.setIsNotO2Between(Boolean.valueOf(isNotO2Between));
		
		alertCriteria.setMaxCoverLevelValue(Integer.valueOf(maxCoverLevelValue));
		alertCriteria.setMaxWaterLevelValue(Integer.valueOf(maxWaterLevelValue));
		alertCriteria.setMaxH2sValue(Double.valueOf(maxH2sValue));
		alertCriteria.setMaxSo2Value(Double.valueOf(maxSo2Value));
		alertCriteria.setMaxCo2Value(Double.valueOf(maxCo2Value));
		alertCriteria.setMaxCh4Value(Double.valueOf(maxCh4Value));
		alertCriteria.setMaxNh3Value(Double.valueOf(maxNh3Value));
		alertCriteria.setMaxO2Value(Double.valueOf(maxO2Value));
		
		alertCriteria.setMinCoverLevelValue(Integer.valueOf(minCoverLevelValue));
		alertCriteria.setMinWaterLevelValue(Integer.valueOf(minWaterLevelValue));
		alertCriteria.setMinH2sValue(Double.valueOf(minH2sValue));
		alertCriteria.setMinSo2Value(Double.valueOf(minSo2Value));
		alertCriteria.setMinCo2Value(Double.valueOf(minCo2Value));
		alertCriteria.setMinCh4Value(Double.valueOf(minCh4Value));
		alertCriteria.setMinNh3Value(Double.valueOf(minNh3Value));
		alertCriteria.setMinO2Value(Double.valueOf(minO2Value));
		
	
		alertCriteria.setIsCoverLevel(Boolean.valueOf(isCoverLevel));
		alertCriteria.setIsWaterLevel(Boolean.valueOf(isWaterLevel));
		alertCriteria.setIsH2s(Boolean.valueOf(isH2s));
		alertCriteria.setIsSo2(Boolean.valueOf(isSo2));
		alertCriteria.setIsCo2(Boolean.valueOf(isCo2));
		alertCriteria.setIsCh4(Boolean.valueOf(isCh4));
		alertCriteria.setIsNh3(Boolean.valueOf(isNh3));
		alertCriteria.setIsO2(Boolean.valueOf(isO2));

		alertCriteria.setIsCoverDetect(Boolean.valueOf(isCoverDetect));
		alertCriteria.setCoverDetect(Boolean.valueOf(coverDetect));
		alertCriteria.setCoverDetectOpenOrCloseStr(coverDetectOpenOrCloseStr);
		
		populateAlertCriteria(alertCriteria);
		
		AlertCriteria updatedAlertSetting =  (AlertCriteria)alertCriteriaDao.update(alertCriteria);
		updatedAlertSetting.setValidationSubject("Update Alert Criteria");
		updatedAlertSetting.setValidationMessages("Save alert criteria success");
		return updatedAlertSetting;
		
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteData")
	public AlertCriteria deleteAlertCriteria(@QueryParam("id") Long id) {
		AlertCriteria a = alertCriteriaDao.retrieveAlertCriteria(id);

		List<AlertSetting> alertSettings = alertSettingDao.searchByCriteria(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, a.getId(), null, false);
		if (alertSettings != null && alertSettings.size() > 0) {
			AlertCriteria failedDeleteCritera = new AlertCriteria();
			
			failedDeleteCritera.setValidationSubject("Delete Alert Criteria");
			failedDeleteCritera.setFailedValidationMessage( String.format("Alert criteria %s still assigned to existing alert setting:%s" , a.getName(), alertSettings.get(0).getName() ));
			return failedDeleteCritera;
		}
		
		alertCriteriaDao.deleteAlertCriteria(id);
		a.setValidationSubject("Delete Alert Criteria");
		a.setValidationMessages("Delete alert criteria success");
		return a;
	}
}
