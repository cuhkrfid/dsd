package com.xmlasia.gemsx.controller;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.DeviceDataDao;
import com.xmlasia.gemsx.device.WebClientDeviceDataEndPoint;
import com.xmlasia.gemsx.domain.DeviceData;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.SensorChartData;
import com.xmlasia.gemsx.service.ChartViewService;
import com.xmlasia.gemsx.service.impl.ServerConfigService;

@Path("/chartView")
@Singleton
public class ChartViewController {

	private static Logger logger = Logger.getLogger(ChartViewController.class);

	
	private ChartViewService chartViewService;	
	private DeviceDataDao deviceDataDao;
	private ServerConfigService serverConfigService;
	
	public ChartViewController(@Context ServletContext ctx) {
		
		serverConfigService = (ServerConfigService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("serverConfigService");
		
		chartViewService = (ChartViewService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("chartViewService");
		deviceDataDao = (DeviceDataDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("deviceDataDao");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/webSocketViewData")
	public List<SensorChartData> searchByCriteria(
			@QueryParam("deviceId") String deviceId,
			@QueryParam("sensorId") String sensorId, 
			@QueryParam("dateFrom") String dateFrom, 
			@QueryParam("dateTo") String dateTo,
			@QueryParam("desc") Boolean descending ) {
		
		List<SensorChartData> sensorChartDatas = new ArrayList<SensorChartData>();
		
		Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (o.getClass() == String.class){
			return sensorChartDatas;
		}
		
		UserDetails userDetails = (UserDetails) o;
		String userName = userDetails.getUsername();

		sensorChartDatas = searchByCriteriaMobile(deviceId, sensorId, dateFrom, dateTo, descending );
		
		if ( deviceId != null && sensorId != null) {
			WebClientDeviceDataEndPoint.updateCurrentUserView(userName, Integer.valueOf(deviceId), Integer.valueOf(sensorId));
		}
		
		return sensorChartDatas;
	}

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/webSocketViewDataMobile")
	public List<SensorChartData> searchByCriteriaMobile(
			@QueryParam("deviceId") String deviceId,
			@QueryParam("sensorId") String sensorId, 
			@QueryParam("dateFrom") String dateFrom, 
			@QueryParam("dateTo") String dateTo,
			@QueryParam("desc") Boolean descending ) {
		
		List<SensorChartData> sensorChartDatas = new ArrayList<SensorChartData>();
		List<Sensor> sensorResult = chartViewService.searchByCriteria(descending, deviceId, sensorId, dateFrom, dateTo, null );
		
		SensorChartData maxMinSensorChartData = chartViewService.searchMinMaxByCriteria(descending, deviceId, sensorId, dateFrom, dateTo, null);
		
		List<DeviceData> deviceDataResult = deviceDataDao.searchByCriteria(null, null, null, null, null, null, deviceId, dateFrom, dateTo, descending, null);
		
		Sensor previousSensor = null;
		if (sensorResult != null && sensorResult.size() > 0 && deviceDataResult != null && deviceDataResult.size() > 0){
			
			for (int i = 0; i < sensorResult.size(); ++i ) {

					SensorChartData sensorChartData = new SensorChartData(); 
					
					Sensor sensor = sensorResult.get(i);
					if ( previousSensor != null ) {
						long timeDiffInMs = Math.abs( sensor.getInputDate().getTime() - previousSensor.getInputDate().getTime()); 
						sensorChartData.setTimeDiffInMsStr(String.valueOf(timeDiffInMs));
					} 
					sensorChartData.setDate(sensor.getInputDate());
					
					DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					String dateString = df.format(sensor.getInputDate());
					sensorChartData.setDateString(dateString);
					
					df = new SimpleDateFormat("HH:mm:ss");
					dateString = df.format(sensor.getInputDate());
					
					sensorChartData.setDateLong(sensor.getInputDate().getTime());
					sensorChartData.setDateString2(dateString);	
					
					sensorChartData.setIsH2s(sensor.getIsH2s());
					sensorChartData.setIsSo2(sensor.getIsSo2());
					sensorChartData.setIsCh4(sensor.getIsCh4());
					sensorChartData.setIsCo2(sensor.getIsCo2());
					sensorChartData.setIsO2(sensor.getIsO2());
					sensorChartData.setIsWaterLevel(sensor.getIsWaterLevel());
					sensorChartData.setIsCoverLevel(sensor.getIsCoverLevel());
					sensorChartData.setIsNh3(sensor.getIsNh3());
					
					SensorChartData previousSensorChartData = (i == 0 ? sensorChartData :  sensorChartDatas.get(i-1) );
					
					if (sensor.getIsH2s()) {
						Double currentH2s = sensor.getH2s();
						sensorChartData.setH2s(currentH2s);
						
						sensorChartData.setMinH2s(maxMinSensorChartData.getMinH2s());
						sensorChartData.setMaxH2s(maxMinSensorChartData.getMaxH2s());
						sensorChartData.setH2sDiff(getDiffValueToField(i, previousSensorChartData, currentH2s, "getH2s"));
					} 
					
					if (sensor.getIsSo2()) {
						Double currenSo2 = sensor.getSo2();
						sensorChartData.setSo2(currenSo2);
						sensorChartData.setMinSo2(maxMinSensorChartData.getMinSo2());
						sensorChartData.setMaxSo2(maxMinSensorChartData.getMaxSo2());
						sensorChartData.setSo2Diff(getDiffValueToField(i, previousSensorChartData, currenSo2, "getSo2"));
					} 
					
					if (sensor.getIsCh4()) {
						Double currentCh4 = sensor.getCh4();
						sensorChartData.setCh4(currentCh4);
						sensorChartData.setMinCh4(maxMinSensorChartData.getMinCh4());
						sensorChartData.setMaxCh4(maxMinSensorChartData.getMaxCh4());
						sensorChartData.setCh4Diff(getDiffValueToField(i, previousSensorChartData, currentCh4, "getCh4"));
					} 
					
					if (sensor.getIsCo2()) {
						Double currentCo2 = sensor.getCo2();
						sensorChartData.setCo2(currentCo2);
						sensorChartData.setMinCo2(maxMinSensorChartData.getMinCo2());
						sensorChartData.setMaxCo2(maxMinSensorChartData.getMaxCo2());
						sensorChartData.setCo2Diff(getDiffValueToField(i, previousSensorChartData, currentCo2, "getCo2"));
					}
						
					if (sensor.getIsO2()) {
						Double currentO2 = sensor.getO2();
						sensorChartData.setO2(currentO2);
						sensorChartData.setMinO2(maxMinSensorChartData.getMinO2());
						sensorChartData.setMaxO2(maxMinSensorChartData.getMaxO2());
						sensorChartData.setO2Diff(getDiffValueToField(i, previousSensorChartData, currentO2, "getO2"));
					} 
					
					if (sensor.getIsWaterLevel()) {
						
						int waterLevel = sensor.getWaterLevel();
						sensorChartData.setWaterLevel(waterLevel);
		
						int negativeWaterLevel = -1 * waterLevel;
						sensorChartData.setWaterLevelFromZero2(-1 * waterLevel);
						
						int waterLevelFromTrough = Math.abs( maxMinSensorChartData.getWaterLevelFromZeroMin()  - negativeWaterLevel );
						sensorChartData.setWaterLevelFromZero(waterLevelFromTrough);
						
						sensorChartData.setWaterLevelFromTroughMax(getMaxValueToField(i, previousSensorChartData, Double.valueOf(waterLevelFromTrough), "getWaterLevelFromTroughMax").intValue());
						
						if (i == 0 ) {
							sensorChartData.setWaterLevelFromZeroDiff(0);
						} else {
							if ( sensorChartData != null 
								&& previousSensorChartData != null 
								&& sensorChartData.getWaterLevelFromZero() != null 
								&& previousSensorChartData.getWaterLevelFromZero() != null) {
								Integer changeWaterLevelFromZero = sensorChartData.getWaterLevelFromZero() - previousSensorChartData.getWaterLevelFromZero();
								sensorChartData.setWaterLevelFromZeroDiff( changeWaterLevelFromZero);
							}
						}
						
						sensorChartData.setWaterLevelFromZeroMax( maxMinSensorChartData.getWaterLevelFromZeroMax() );
						sensorChartData.setWaterLevelFromZeroMin( maxMinSensorChartData.getWaterLevelFromZeroMin() );
					} 
					
					if (sensor.getIsNh3()) {
						Double currentNh3 = sensor.getNh3();
						sensorChartData.setNh3(currentNh3);
						sensorChartData.setMinNh3(maxMinSensorChartData.getMinNh3());
						sensorChartData.setMaxNh3(maxMinSensorChartData.getMaxNh3());
						sensorChartData.setNh3Diff(getDiffValueToField(i, previousSensorChartData, currentNh3, "getNh3"));
					} 
					
					if (sensor.getIsCoverLevel()) {
						sensorChartData.setCoverLevel(sensor.getCoverLevel());
					} 
					
					Double currentRssi = sensor.getRssi();
					sensorChartData.setRssi(currentRssi);
					sensorChartData.setMinRssi(maxMinSensorChartData.getMinRssi());
					sensorChartData.setMaxRssi(maxMinSensorChartData.getMaxRssi());
					sensorChartData.setRssiDiff(getDiffValueToField(i, previousSensorChartData, currentRssi, "getRssi"));
					
					sensorChartData.setAntennaOptimized(sensor.getAntennaOptimized());
					sensorChartData.setWaterLevel1Detect(sensor.getWaterLevel1Detect());
					sensorChartData.setWaterLevel2Detect(sensor.getWaterLevel2Detect());
					sensorChartData.setCoverDetect(sensor.getCoverDetect());	
					
					sensorChartData.setAntennaOptimizedStr(sensor.getAntennaOptimizedStr());
					sensorChartData.setWaterLevel2DetectStr(sensor.getWaterLevel2DetectStr());
					sensorChartData.setWaterLevel1DetectStr(sensor.getWaterLevel1DetectStr());
					sensorChartData.setCoverDetectStr(sensor.getCoverDetectStr());
					sensorChartData.setBatteryStr(sensor.getBattery());
					sensorChartData.setBattery(sensor.getIsLowBattery() ? 0 : 1);
					sensorChartData.setReaderId(sensor.getReaderId());
					sensorChartData.setMaxStep(sensor.getMaxStep());
					sensorChartData.setAdcValue(sensor.getAdcValue());
					sensorChartData.setDeviceId(sensor.getDeviceId());
					sensorChartData.setSensorId(sensor.getSensorId());
					
					DeviceData device = deviceDataResult.get(i);
					sensorChartData.setId(device.getId());
					sensorChartData.setPacketCounter(device.getPacketCounter());
					sensorChartData.setMeasurePeriod(device.getMeasurePeriod());
					sensorChartData.setReaderIds(device.getReaderIds());
					sensorChartData.setRssiStr(device.getRssiStr());
					
					sensorChartData.setReaderRssi(device.getReaderRssi());
					sensorChartData.setReaderNameRssi(device.getReaderNameRssi());
					sensorChartData.setReaderRssiSnr(device.getReaderRssiSnr());
					sensorChartData.setReaderNameRssiSnr(device.getReaderNameRssiSnr());
					
					sensorChartData.setUserDefine(device.getUserDefine());
					
					if (i == 0) {
						sensorChartData.setTimeDiffStr("--");
					} else {
						
						Map<TimeUnit,Long> timeDiffMap =  computeDiff( previousSensorChartData.getDate(), sensorChartData.getDate());
						Long timeDiffInMs = sensorChartData.getDate().getTime() - previousSensorChartData.getDate().getTime();
						
						if ( timeDiffInMs < 60*1000 ) {
							//under minute, use secs
							Long timeDiffInSecs = timeDiffMap.get(TimeUnit.SECONDS);
							
							String label = " secs later";
							if ( timeDiffInSecs < 2)
								label = " sec later";
							
							sensorChartData.setTimeDiffStr(timeDiffInSecs.toString() + label);
							
						} else if (  timeDiffInMs < 60*60*1000 ) {
							//under hour, use mins
							Long timeDiffInMins = timeDiffMap.get(TimeUnit.MINUTES);
							
							String label = " mins later";
							if ( timeDiffInMins < 2)
								label = " min later";
							
							sensorChartData.setTimeDiffStr(timeDiffInMins.toString() + label);
							
						} else if (  timeDiffInMs < 24*60*60*1000 ) {
							//under day, use hours
							Long timeDiffInHours = timeDiffMap.get(TimeUnit.HOURS);
							
							String label = " hours later";
							if ( timeDiffInHours < 2)
								label = " hour later";
							
							sensorChartData.setTimeDiffStr(timeDiffInHours.toString() + label);
						} else {
							//use days
							Long timeDiffInDays = timeDiffMap.get(TimeUnit.DAYS);
					
							String label = " days later";
							if ( timeDiffInDays < 2)
								label = " day later";
							
							sensorChartData.setTimeDiffStr(timeDiffInDays.toString()  + label);
						}
					}
					
					sensorChartDatas.add(sensorChartData);
					previousSensor = sensor;
				}				
//			}
		}
		
		return sensorChartDatas;
	}
	
	private Double getDiffValueToField(int i, 
			SensorChartData previousSensorChartData,
			Double currentValue, 
			String getMethodName) {
	
		if ( i == 0) {
			return 0.0;
		} else {
			Class cl = previousSensorChartData.getClass();
			Class noparams[] = {};
			try {
				Method method = cl.getDeclaredMethod(getMethodName, noparams);
				Object returnValue = method.invoke(previousSensorChartData, null);

				Double prevDoubleValue = null;
				if ( returnValue != null)
				{
					if (returnValue.getClass() == Double.class) {
						prevDoubleValue = (Double) returnValue;
					}
					if (returnValue.getClass() == Integer.class) {
						Integer intValue = (Integer) returnValue;
						prevDoubleValue = Double.valueOf(intValue);
					}
					
					if ( prevDoubleValue != null && currentValue != null) {
						double diff = currentValue - prevDoubleValue;
						return Math.round(diff * 100.0) / 100.0;
					}

				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return currentValue;
	}
	
	private Double getMaxValueToField(int i, 
			SensorChartData previousSensorChartData,
			Double currentValue, 
			String getMethodName) {
		if ( i == 0) {
			return currentValue;
		} else { 
			
			Class cl = previousSensorChartData.getClass();
			Class noparams[] = {};
			try {
				Method method = cl.getDeclaredMethod(getMethodName, noparams);
				Object returnValue = method.invoke(previousSensorChartData, null);

				Double prevDoubleValue = null;
				if ( returnValue != null)
				{
					if (returnValue.getClass() == Double.class) {
						prevDoubleValue = (Double) returnValue;
					}
					if (returnValue.getClass() == Integer.class) {
						Integer intValue = (Integer) returnValue;
						prevDoubleValue = Double.valueOf(intValue);
					}
					
					if ( prevDoubleValue != null && currentValue != null) {
						if ( prevDoubleValue < currentValue ) {
							return currentValue;
						}
						else 
						{
							return prevDoubleValue;
						}
					}

				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			return currentValue;
		}
	}
	
	private Double getMinValueToField(int i, 
			SensorChartData previousSensorChartData,
			Double currentValue, 
			String getMethodName) {
		if ( i == 0) {
			return currentValue;
		} else { 
			
			Class cl = previousSensorChartData.getClass();
			Class noparams[] = {};
			try {
				Method method = cl.getDeclaredMethod(getMethodName, noparams);
				Object returnValue = method.invoke(previousSensorChartData, null);

				Double prevDoubleValue = null;
				if ( returnValue != null)
				{
					if (returnValue.getClass() == Double.class) {
						prevDoubleValue = (Double) returnValue;
					}
					if (returnValue.getClass() == Integer.class) {
						Integer intValue = (Integer) returnValue;
						prevDoubleValue = Double.valueOf(intValue);
					}
					
					if ( prevDoubleValue != null && currentValue != null) {
						if ( prevDoubleValue > currentValue ) {
							return currentValue;
						}
						else 
						{
							return prevDoubleValue;
						}
					}

				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			return currentValue;
		}
	}
	
	public Map<TimeUnit,Long> computeDiff(Date date1, Date date2) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    List<TimeUnit> units = new ArrayList<TimeUnit>(EnumSet.allOf(TimeUnit.class));
	    Collections.reverse(units);
	    Map<TimeUnit,Long> result = new LinkedHashMap<TimeUnit,Long>();
	    long milliesRest = diffInMillies;
	    for ( TimeUnit unit : units ) {
	        long diff = unit.convert(milliesRest,TimeUnit.MILLISECONDS);
	        long diffInMilliesForUnit = unit.toMillis(diff);
	        milliesRest = milliesRest - diffInMilliesForUnit;
	        result.put(unit,diff);
	    }
	    return result;
	}
}
