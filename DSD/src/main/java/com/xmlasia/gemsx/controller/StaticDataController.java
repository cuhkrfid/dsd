package com.xmlasia.gemsx.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.domain.StaticData;
import com.xmlasia.gemsx.service.StaticDataService;

@Path("/staticData")
@Singleton
public class StaticDataController {

	private static Logger logger = Logger.getLogger(DepartmentController.class);

	private StaticDataService staticDataService;
	
	private Client client;

	public StaticDataController(@Context ServletContext ctx) {
		staticDataService = (StaticDataService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("staticDataService");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<StaticData> searchData(@QueryParam("dataName") String dataName) {
		List<StaticData> result = staticDataService.searchByCriteria(dataName);
		return result;
	}

}