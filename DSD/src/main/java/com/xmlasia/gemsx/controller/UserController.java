package com.xmlasia.gemsx.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.GroupDao;
import com.xmlasia.gemsx.domain.Company;
import com.xmlasia.gemsx.domain.Group;
import com.xmlasia.gemsx.domain.Menu;
import com.xmlasia.gemsx.domain.Module;
import com.xmlasia.gemsx.domain.Permission;
import com.xmlasia.gemsx.domain.Role;
import com.xmlasia.gemsx.domain.User;
import com.xmlasia.gemsx.domain.UserProfile;
import com.xmlasia.gemsx.domain.Validation;
import com.xmlasia.gemsx.service.CompanyService;
import com.xmlasia.gemsx.service.UserService;

@Path("/user")
@Singleton
public class UserController {

	private static Logger logger = Logger.getLogger(UserController.class);

	private UserService userService;
	private CompanyService companyService;
	private GroupDao groupDao;
	
	private	final String deleteUserSubject = "Delete User";
	
	public UserController(@Context ServletContext ctx) {
		userService = (UserService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("userService");
		companyService = (CompanyService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("companyService");
		groupDao = (GroupDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("groupDao");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}

	//heart beat from client to keep session alive
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getCheckSessionTimeout") 
	public Validation getCheckSessionTimeout() {
		Validation validation = new Validation();
		validation.setValidationSubject("User session");
		
		Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (o.getClass() == String.class){
			validation.setFailedValidationMessage("User session has expired.  Please login again");
		}		
		return validation;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getModuleAndMenus")
	public List<Module> getModuleAndMenus(@QueryParam("searchText") String searchText) {
		Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (o.getClass() == String.class){
			return null;
		}
		UserDetails userDetails = (UserDetails) o;
		String userName = userDetails.getUsername();
		
		List<Module> modules = userService.getParentModules(userName);
		List<Module> modulesList = new ArrayList<Module>();
		User user = userService.findByLoginId(userName);
		
		if ( modules != null && user != null) {
			for (Module module : modules){
				if (module == null){
					continue;
				}
				List<Menu> menus = userService.getMenus(user, module.getId(), searchText);
				for (Menu m : menus){
					m.setModuleId(null);
					m.setModule(null);
				}
				if (menus.size() > 0){
					module.setMenus(menus);
					modulesList.add(module);
				}
			}
		}
		return modulesList;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getModuleAndMenusMobile")
	public List<Module> getModuleAndMenusMobile(@QueryParam("userName") String userName) {
		
		List<Module> modules = userService.getParentModules(userName);
		List<Module> mobileModuleList = new ArrayList<Module>();
		User user = userService.findByLoginId(userName);
		
		if ( modules != null && user != null) {
			for (Module module : modules){
				if (module == null || !module.getMobileSupport()){
					continue;
				}
				
				List<Menu> menus = userService.getMenus(user, module.getId(), null);
				List<Menu> mobileMenuList = new ArrayList<Menu>();
				
				for (Menu m : menus){
					if (!m.getMobileSupport())
						continue; 
					
					m.setModuleId(null);
					m.setModule(null);
					
					mobileMenuList.add(m);
				}
				
				
				if (menus.size() > 0){
					module.setMenus(mobileMenuList);
					mobileModuleList.add(module);
				}
			}
		}
		return mobileModuleList;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getLoginUser")
	public User getLoginUser() {

		Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (o.getClass() == String.class){
			return null;
		}
		UserDetails userDetails = (UserDetails) o;
		User user = userService.findByLoginId(userDetails.getUsername());

		user.setDepartment(null);
		return user;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/initData")
	public List<User> getInitData() {
		return userService.getAllUser();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findUserById")
	public User findUserById(@QueryParam("id") Integer id) {
		User result = userService.retrieveByUserId(id);
		return result;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findUserRoleById")
	public Map findUserRoleById(@QueryParam("id") Integer id) {
		List<Role> result1 = userService.findAssignedRoleById(id);
		List<Role> result2 = userService.findUnassignedRoleById(id);
		for (Role r : result1){
			r.setMenus(new HashSet<Menu>());
		}
		for (Role r : result2){
			r.setMenus(new HashSet<Menu>());
		}
		Map<String, List<Role>> result = new HashMap<String, List<Role>>();
		result.put("assignedRoles", result1);
		result.put("unassignedRoles", result2);
		return result;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findUserGroupById")
	public Map findUserGroupById(@QueryParam("id") Integer id) {
		List<Group> result1 = userService.findAssignedGroupById(id);
		List<Group> result2 = userService.findUnassignedGroupById(id);
		Map<String, List<Group>> result = new HashMap<String, List<Group>>();
		result.put("assignedGroups", result1);
		result.put("unassignedGroups", result2);
		return result;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<User> searchData(@QueryParam("id") String id, 
			@QueryParam("chiName") String chiName,
			@QueryParam("engName") String engName, 
			@QueryParam("userName") String userName,
			@QueryParam("email") String email,
			@QueryParam("dept") String dept, 
			@QueryParam("password") String password, 
			@QueryParam("phone") String phone, 
			@QueryParam("notification") Boolean notification, 
			@QueryParam("fax") String fax, 
			@QueryParam("title") String title,
			@QueryParam("pwdResetAfterLogin") Boolean pwdResetAfterLogin,
			@QueryParam("active") Boolean active ) {

		List<User> result = userService.searchByCriteria(id, chiName, engName, userName, email, dept, password, phone, notification, fax, title, pwdResetAfterLogin, active);
		List<Group> groups = groupDao.searchByCriteria(null,null,null,null);
		
		for (User u : result){
		
			for ( Group group : groups) {
				if ( group.getId() == u.getGroupId()) {
					u.setGroup(group);
					break;
				}
			}
		}
		return result;
	}

	@POST
	@Path("/addData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public User addUser(@FormDataParam("name") String name,
			@FormDataParam("userName") String userName,
			@FormDataParam("email") String email, 
			@FormDataParam("department") String department,
			@FormDataParam("groupId") Long groupId,
			@FormDataParam("password") String password,
			@FormDataParam("phone") String phone, 
			@FormDataParam("fax") String fax, 
			@FormDataParam("title") String title,
			@FormDataParam("active") Boolean active, 
			@FormDataParam("fileName") String fileName,
			@FormDataParam("file") InputStream fileData ) {
		
		User user = new User();
		user.setName(name);
		user.setUsername(userName);
		user.setEmail(email);
		user.setPassword(password);
		user.setPhone(phone);
		user.setFax(fax);
		user.setTitle(title);
		user.setActive(active);
		user.setEnabled(active);
		user.setDepartment(department);
		user.setGroupId(groupId);
		user.setRole("ROLE_USER");
		
		UserProfile userProfile = new UserProfile(); 
		userProfile.setLocale("en");
		user.setUserProfile(userProfile);
		
		User newUser = userService.createUser(user);
		newUser.setDepartment(null);
		
		newUser.setValidationSubject("Create New User");
		newUser.setValidationMessages("Create new user success");
		return newUser;
	}

	@POST
	@Path("/updateData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public User updateUser(@FormDataParam("id") Integer id, 
			@FormDataParam("name") String name, 
			@FormDataParam("userName") String userName,
			@FormDataParam("email") String email, 
			@FormDataParam("department") String department, 
			@FormDataParam("groupId") Long groupId,
			@FormDataParam("password") String password,
			@FormDataParam("phone") String phone, 
			@FormDataParam("fax") String fax, 
			@FormDataParam("title") String title,
			@FormDataParam("active") Boolean active,
			@FormDataParam("fileName") String fileName,
			@FormDataParam("file") InputStream fileData ) {

		User user = userService.retrieveByUserId(id);

		user.setName(name);
		user.setUsername(userName);
		

		if (!email.isEmpty() && email.equals("null") == false )
			user.setEmail(email);
		else
			user.setEmail(null);
	
		if (!phone.isEmpty() && phone.equals("null") == false )
			user.setPhone(phone);
		else
			user.setPhone(null);
	
		
		if (!title.isEmpty() && title.equals("null") == false )
			user.setTitle(title);
		else
			user.setTitle(null);
	
		
		if (!department.isEmpty() && department.equals("null") == false )
			user.setDepartment(department);
		else
			user.setDepartment(null);
		
		user.setPassword(password);
		user.setFax(fax);
		user.setActive(active);
		user.setEnabled(active);
		user.setGroupId(groupId);
		
		User newUser = userService.updateUser(user);
		newUser.setDepartment(null);
		newUser.setValidationSubject("Update User");
		newUser.setValidationMessages("Update user success");
		
		return newUser;
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteData")
	public Validation deleteUser(@QueryParam("id") Integer id) {
		
		Validation validation = new Validation(); 
		validation.setValidationSubject(this.deleteUserSubject);
		validation.setValidationMessages("Delete user success");
		Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (o.getClass() == String.class){
			return null;
		}
		UserDetails userDetails = (UserDetails) o;
		User user = userService.findByLoginId(userDetails.getUsername());
		if (user.getId() == id) {
			validation.setFailedValidationMessage("Cannot delete current logged in user");
		}
		if (user.getName() == "admin") {
			validation.setFailedValidationMessage("Cannot delete admin");
		}
		userService.deleteUser(id);
		return validation;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateUserRole")
	public void updateUserRole(@QueryParam("id") Integer id, @QueryParam("roleIdList") List<Integer> roleIdList) {
		userService.updateUserRole(id, roleIdList);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateUserGroup")
	public void updateUserGroup(@QueryParam("id") Integer id, @QueryParam("groupIdList") List<Integer> groupIdList) {
		userService.updateUserGroup(id, groupIdList);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addCompany")
	public Company addCompany(@QueryParam("chiName") String chiName, @QueryParam("engName") String engName,
			@QueryParam("address") String address, @QueryParam("telNo") String telNo, @QueryParam("faxNo") String faxNo) {
		Company company = new Company();
		company.setChiName(chiName);
		company.setEngName(engName);
		company.setAddress(address);
		company.setTelNo(telNo);
		company.setFaxNo(faxNo);

		return companyService.createCompany(company);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchCompany")
	public List<Company> searchCompany(@QueryParam("id") String id, @QueryParam("chiName") String chiName, @QueryParam("engName") String engName,
			@QueryParam("address") String address, @QueryParam("telNo") String telNo, @QueryParam("faxNo") String faxNo) {

		List<Company> result = companyService.searchByCriteria(id, chiName, engName, address, telNo, faxNo);
		
		return result;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateCompany")
	public Company updateCompany(@QueryParam("id") Integer id, @QueryParam("chiName") String chiName, @QueryParam("engName") String engName,
			@QueryParam("address") String address, @QueryParam("telNo") String telNo, @QueryParam("faxNo") String faxNo) {
		Company company = companyService.retrieveByCompanyId(id);

		company.setChiName(chiName);
		company.setEngName(engName);
		company.setAddress(address);
		company.setTelNo(telNo);
		company.setFaxNo(faxNo);

		company = companyService.updateCompany(company);
		return companyService.retrieveByCompanyId(company.getId());
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteCompany")
	public Company deleteCompany(@QueryParam("id") Integer id) {
		return companyService.deleteCompany(id);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findCompanyById")
	public Company findCompanyById(@QueryParam("id") Integer id) {
		return companyService.retrieveByCompanyId(id);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateUserProfile")
	public UserProfile updateUserProfile(@QueryParam("userId") Integer userId,
			@QueryParam("userProfileId") Integer userProfileId,
			@QueryParam("locale") String locale) {
		
		User user = userService.retrieveByUserId(userId);
		UserProfile userProfile = user.getUserProfile();
		
		Date currentTime = new Date();
		userProfile.setUpdatedBy("SYSTEM");
		userProfile.setUpdatedDate(currentTime);
		userProfile.setLocale(locale);
		
		User newUser = userService.updateUser(user);
		return newUser.getUserProfile();
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateUserPassword")
	public User updateUserPassword(@QueryParam("id") Integer id, @QueryParam("password") String newPassword) {

		User user = userService.retrieveByUserId(id);
		user.setPassword(newPassword);
		User newUser = userService.updateUser(user);
		newUser.setDepartment(null);
		return newUser;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchGroup")
	public List<Group> searchGroup(@QueryParam("id") String id, 
			@QueryParam("name") String name,
			@QueryParam("description") String description,
			@QueryParam("active") Boolean active ) {

		List<Group> result = groupDao.searchByCriteria(id, name, description, active);
		return result;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAllPermission")
	public List<Permission> getAllPermission() {
		List<Permission> result = groupDao.listAllPermission();
		return result;
	}
	
	@POST
	@Path("/loginMobile")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public boolean loginMobile(
			@FormDataParam("username") String username,
			@FormDataParam("password") String password) {
		User user  = userService.findByLoginId(username);
    	if ( user == null) {
    		return false;
    	}
    	
    	if (! user.getPassword().equals(password))
    		return false;
    	
    	List<Group> groups = groupDao.searchByCriteria(String.valueOf(user.getGroupId()), null, null, null);
    	if ( groups == null || groups.size() == 0) {
    		return false;
    	}
    	if ( groups.get(0).getActive() == false) {
    		return false;
    	}		
    	return true;
	}
	
}