package com.xmlasia.gemsx.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.FieldUtil;
import com.xmlasia.gemsx.dao.SiteConfigurationDao;
import com.xmlasia.gemsx.dao.TagConfigurationDao;
import com.xmlasia.gemsx.device.TagConfigurationValidator;
import com.xmlasia.gemsx.device.WebClientDeviceDataEndPoint;
import com.xmlasia.gemsx.domain.AlertSetting;
import com.xmlasia.gemsx.domain.SensorConfig;
import com.xmlasia.gemsx.domain.TagConfiguration;
import com.xmlasia.gemsx.service.AlertSettingService;
import com.xmlasia.gemsx.service.TagConfigurationService;

@Path("/tagConfiguration")
@Singleton
public class TagConfigurationController {

	private static Logger logger = Logger.getLogger(TagConfigurationController.class);

	private TagConfigurationService tagConfigurationService;
	private TagConfigurationDao tagConfigurationDao;
	private SiteConfigurationDao siteConfigurationDao;
	private TagConfigurationValidator tagConfigurationValidator;
	private AlertSettingService alertSettingService;
	
	private	final String deleteTagSubject = "Delete Tag Configuration";
	private final String deleteTagSuccessful= "Delete tag successful";

	private	final String updateTagSubject = "Update Tag Configuration";
	private final String updateTagSuccessful = "Update tag successful";
	
	private	final String createTagSubject = "Create Tag Configuration";
	private final String createTagSuccessful= "Create tag successful";
	
	public TagConfigurationController(@Context ServletContext ctx) {
		tagConfigurationService = (TagConfigurationService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("tagConfigurationService");
		tagConfigurationDao = (TagConfigurationDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("tagConfigurationDao");
		
		siteConfigurationDao = (SiteConfigurationDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("siteConfigurationDao");
		
		tagConfigurationValidator = (TagConfigurationValidator) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("tagConfigurationValidator");
		alertSettingService = (AlertSettingService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("alertSettingService");	
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/validateTagConfigurationMobile")
	public List<TagConfiguration> validateTagConfigurationMobile(@QueryParam("siteId") Long siteId) {
		tagConfigurationValidator.validate();
		List<TagConfiguration> result = searchData(null,null,null,null,null,null,null,siteId);	
		if (result != null) {
			for (TagConfiguration tagConfiguration : result) {
				
				String imageUrlPath = tagConfiguration.getImageUrlPath(); 
				if ( imageUrlPath== null || imageUrlPath.isEmpty() ) {
					continue;
				}
				
				int ind = imageUrlPath.length();
				imageUrlPath = imageUrlPath.substring(1, ind );
				String encodedFile = encodeFileToBase64Binary(imageUrlPath);
				if ( !encodedFile.isEmpty()) {
					tagConfiguration.setImageUri(encodedFile);
				}
			}
			
		}
		return result;
	}
	
    private static String encodeFileToBase64Binary(String imageUrlPath){
    	String encodedfile = "";
    	
    	try {
			String dir = TagConfigurationController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
			int start = dir.indexOf("DSD");
			dir = dir.substring(0, start + 3);
			File file = new File( dir + imageUrlPath);
			
	        try {
	            FileInputStream fileInputStreamReader = new FileInputStream(file);
	            byte[] bytes = new byte[(int)file.length()];
	            fileInputStreamReader.read(bytes);
	            encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
	            
	            fileInputStreamReader.close();
	            
	        } catch (FileNotFoundException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
			
			
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	


        return encodedfile;
    }
    
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/validateTagConfiguration")
	public List<TagConfiguration> validateTagConfiguration(@QueryParam("siteId") Long siteId) {
		tagConfigurationValidator.validate();
		return searchData(null,null,null,null,null,null,null,siteId);	
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getMeasurementPeriod")
	public Long getMeasurementPeriod(
			@QueryParam("deviceIdStr") String deviceIdStr ) {
		
		if ( deviceIdStr == null || deviceIdStr.isEmpty())
			return new Long(0);
		
		List<TagConfiguration> result = tagConfigurationService.searchByCriteria(null, null, null, deviceIdStr, null, null, null, null, null);
		if ( result != null && result.size() > 0) {
			return new Long(result.get(0).getMeasurePeriod());
		}
		return new Long(0);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<TagConfiguration> searchData(
			@QueryParam("id") String id, 
			@QueryParam("name") String name,
			@QueryParam("deviceId") String deviceId,
			@QueryParam("deviceAddress") String deviceAddress,
			@QueryParam("groupId") String groupId,
			@QueryParam("description") String description, 
			@QueryParam("siteName") String siteName,
			@QueryParam("siteId") Long siteId) {
		List<TagConfiguration> result = tagConfigurationService.searchByCriteria(id, name, deviceAddress, deviceId, null, groupId, description, siteName, siteId);
		return result;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchDataTableView")
	public List<SensorConfig> searchDataTableView(
			@QueryParam("groupId") String groupId,
			@QueryParam("siteId") Long siteId) {
		List<TagConfiguration> result = tagConfigurationService.searchByCriteria(null, null, null, null, null, groupId, null, null, siteId);
		List<SensorConfig> sensorConfigs = new ArrayList<SensorConfig>();
		
		for (TagConfiguration tagConfiguration: result) {
			sensorConfigs.addAll( tagConfiguration.getSensorConfigsList() );
		}
		
		return sensorConfigs;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/webSocketViewDataMobile")
	public List<TagConfiguration> webSocketViewDataMobile(
			@QueryParam("id") String id, 
			@QueryParam("name") String name,
			@QueryParam("deviceId") String deviceId,
			@QueryParam("sensorId") String sensorId,
			@QueryParam("groupId") String groupId,
			@QueryParam("description") String description, 
			@QueryParam("siteName") String siteName,
			@QueryParam("siteId") Long siteId,
			@QueryParam("userName") String userName) {
		
		List<TagConfiguration> searchTagConfigs = new ArrayList<TagConfiguration>();
		
		searchTagConfigs = tagConfigurationService.searchByCriteria(id, name, null, deviceId, sensorId, groupId, description, siteName, siteId);
		WebClientDeviceDataEndPoint.updateCurrentUserView(userName, searchTagConfigs);
		
		return searchTagConfigs;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/webSocketViewData")
	public List<TagConfiguration> webSocketViewData(
			@QueryParam("id") String id, 
			@QueryParam("name") String name,
			@QueryParam("deviceId") String deviceId,
			@QueryParam("sensorId") String sensorId,
			@QueryParam("groupId") String groupId,
			@QueryParam("description") String description, 
			@QueryParam("siteName") String siteName,
			@QueryParam("siteId") Long siteId) {
		
		List<TagConfiguration> searchTagConfigs = new ArrayList<TagConfiguration>();
		
		Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (o.getClass() == String.class){
			return searchTagConfigs;
		}
		
		UserDetails userDetails = (UserDetails) o;
		String userName = userDetails.getUsername();
		
		searchTagConfigs = tagConfigurationService.searchByCriteria(id, name, null, deviceId, sensorId, groupId, description, siteName, siteId);
		WebClientDeviceDataEndPoint.updateCurrentUserView(userName, searchTagConfigs);
		
		return searchTagConfigs;
	}
	
			
	@POST
	@Path("/addData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public TagConfiguration addTagConfiguration(
			@FormDataParam("id") String idStr,
			@FormDataParam("deviceAddress") String deviceAddress,
			@FormDataParam("deviceId") String deviceId,
			@FormDataParam("name") String name,
			@FormDataParam("description") String description, 
			@FormDataParam("groupId") String groupId, 
						
			@FormDataParam("measurementPeriod") String measurementPeriod,
			@FormDataParam("siteId") String siteId,
			@FormDataParam("lat") String lat,
			@FormDataParam("lon") String lon,
			@FormDataParam("zoomLevel") String zoomLevel,
			
			@FormDataParam("readerInSequence") String readerInSequence,
			@FormDataParam("readerInSequenceInMs") String readerInSequenceInMs,
			@FormDataParam("readerWithHighestRssi") String readerWithHighestRssi,
			@FormDataParam("readerWithHighestRssiInMs") String readerWithHighestRssiInMs,
			@FormDataParam("assignToSpecificReaders") String assignToSpecificReaders,
			@FormDataParam("assignToSpecificReadersInMs") String assignToSpecificReadersInMs,
			//@FormDataParam("assignToSpecificReadersConfigs") String assignToSpecificReadersConfigs,
			
			@FormDataParam("fileName") String fileName, 
			@FormDataParam("file") InputStream fileData,
			@FormDataParam("imageUriBase64") String imageUriBase64Data,
			
			@FormDataParam("manHoleId") String manHoleIdStr,
			@FormDataParam("manHoleHeight") String manHoleHeightStr,
			@FormDataParam("createDateTime") String createDateTimeStr,
			
			@FormDataParam("sensorConfigs") String sensorConfig) {
		
		TagConfiguration tagConfiguration;
		Long id = Long.valueOf(idStr); 
		if ( id == 0) {
			tagConfiguration = new TagConfiguration();
			tagConfiguration.setSiteConfiguration(siteConfigurationDao.retrieveSiteConfiguration(Long.valueOf(siteId)));
		} else {
			tagConfiguration = tagConfigurationService.retrieveByTagConfigurationId(id);
		} 
		
//		List<Network> networkExist = networkDao.searchByCriteria(null, null, deviceId, null, String.valueOf(Network.ENDDEVICE), null, null, null, null, false);
//		if ( networkExist == null || networkExist.size() == 0) {
//			tagConfiguration.setValidationSubject(id == 0 ? this.createTagSubject :  this.updateTagSubject);
//        	String message = "Unable to find end device with device Id:%s in the network";
//        	message = String.format(message, deviceId);
//        	tagConfiguration.setFailedValidationMessage(message);
//        	return tagConfiguration;			
//		}
		
		List<TagConfiguration> tagConfigurationExist = tagConfigurationService.searchByCriteria(null, null, null, deviceId, null, null, null, null, null);
        if ( tagConfigurationExist != null && tagConfigurationExist.size() > 0 
        	&& tagConfigurationExist.get(0).getId() != tagConfiguration.getId()) {
        	
        	tagConfiguration.setValidationSubject(id == 0 ? this.createTagSubject :  this.updateTagSubject);
        	String message = "Device Id:%s already assigned to tag configuration name:%s \nSave tag configuration failed.";
        	message = String.format(message, tagConfigurationExist.get(0).getDeviceId(), tagConfigurationExist.get(0).getName());
        	tagConfiguration.setFailedValidationMessage(message);
        	return tagConfiguration;
        }
        
		tagConfiguration.setDeviceId(Integer.valueOf(deviceId));
		tagConfiguration.setName(name);
		tagConfiguration.setDescription(description);
	
		//tagConfiguration.setSensorId(Integer.valueOf(sensorId));
		tagConfiguration.setGroupId(Integer.valueOf(groupId));

		JSONArray jsonArray;
		List<SensorConfig> sensorConfigs = new ArrayList<SensorConfig>();
		try {
			JSONObject jsnobject = new JSONObject(sensorConfig);
			jsonArray = jsnobject.getJSONArray("sensorConfigs");
			
		    for (int i = 0; i < jsonArray.length(); i++) {
		        JSONObject explrObject = jsonArray.getJSONObject(i);
		        
		        SensorConfig newSensorConfig = new SensorConfig();
		        
		        //String id = (String)explrObject.get("id");
		        String sensorId = (String)explrObject.get("sensorId");
//		     	List<SensorConfig> sensorConfigsExist = sensorConfigDao.retrieveSensorBySensorId(Long.valueOf(sensorId), tagConfiguration.getId());
//		        if ( sensorConfigsExist != null && sensorConfigsExist.size() > 0 ) {
//		        	
//		        	TagConfiguration tagConfigAlreadyAssigned = tagConfigurationService.retrieveByTagConfigurationId(sensorConfigsExist.get(0).getTagConfigurationId()); 
//		        	
//		        	tagConfiguration.setValidationSubject(this.createTagSubject);
//		        	String message = "SensorId:%s already assigned to tag configuration name:%s, deviceAddress:%s";
//		        	message = String.format(message, sensorId, tagConfigAlreadyAssigned.getName(), tagConfigAlreadyAssigned.getDeviceAddress());
//		        	tagConfiguration.setFailedValidationMessage(message);
//		        	
//		        	return tagConfiguration;
//		        }
		        
		        Long sensorIdLong = Long.valueOf(sensorId);
		        newSensorConfig.setSensorId(sensorIdLong);
		        
		        String isCoverLevel = (String) explrObject.get("isCoverLevel");
		        newSensorConfig.setIsCoverLevel("true".equals(isCoverLevel));
		        
		        String isWaterLevel = (String) explrObject.get("isWaterLevel");
		        newSensorConfig.setIsWaterLevel("true".equals(isWaterLevel));
		        
		        String isH2s = (String) explrObject.get("isH2s");
		        newSensorConfig.setIsH2s("true".equals(isH2s));	
		        
		        String isSo2 = (String) explrObject.get("isSo2");
		        newSensorConfig.setIsSo2("true".equals(isSo2));
		        
		        String isCo2 = (String) explrObject.get("isCo2");
		        newSensorConfig.setIsCo2("true".equals(isCo2));	        

		        String isCh4 = (String) explrObject.get("isCh4");
		        newSensorConfig.setIsCh4("true".equals(isCh4));	  
	
		        String isNh3 = (String) explrObject.get("isNh3");
		        newSensorConfig.setIsNh3("true".equals(isNh3));	

		        String isO2 = (String) explrObject.get("isO2");
		        newSensorConfig.setIsO2("true".equals(isO2));	
		        
		        newSensorConfig.setDeviceId(Long.valueOf(deviceId));
		        newSensorConfig.setMeasurePeriod( Integer.valueOf(measurementPeriod) );
		        newSensorConfig.setInputDate(new Date());
		        sensorConfigs.add(newSensorConfig);
		    }
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SensorConfig[] sensorConfigsArray = sensorConfigs.toArray(new SensorConfig[sensorConfigs.size()]);
		for (int i = 0; i < sensorConfigsArray.length; ++i) {
			for ( int j = 0; j < sensorConfigsArray.length; ++j ) {
				if ( i != j &&  sensorConfigsArray[i].getSensorId() == sensorConfigsArray[j].getSensorId() ) {
		        	
		        	tagConfiguration.setValidationSubject(id == 0 ? this.createTagSubject :  this.updateTagSubject);
		        	String message = "Duplicate sensorId:%d found in tag configuration";
		        	message = String.format(message,  sensorConfigsArray[i].getSensorId());
		        	tagConfiguration.setFailedValidationMessage(message);
		        	
		        	return tagConfiguration;		
				}
			}
		}
		
//		if ( cordId != null && tagConfiguration.getId() == 0 || ( tagConfiguration.getId() > 0 && Integer.valueOf(measurementPeriod) != tagConfiguration.getMeasurePeriod() )) {
//			Validation result;
//			
//			if ( deviceService.getSocketServerClass().equals("SocketServer"))
//				result = SocketServer.sendEndPointConfigStatic(4, 11, deviceAddress, String.valueOf(cordId), measurementPeriod);
//			else if ( deviceService.getSocketServerClass().equals("NioSocketServer"))
//				result = NioSocketServer.sendEndPointConfigStatic(4, 11, deviceAddress, String.valueOf(cordId), measurementPeriod);
//			else { 
//				result = new Validation();
//				result.setValidationSubject("Send Config");
//				result.setValidationMessages("Send config success");
//			}
//			
//			if (! result.getIsValid()) {
//				tagConfiguration.clone(result);
//				return tagConfiguration;
//			} 
//		}
		
		tagConfiguration.setSensorConfigs(sensorConfigs.toArray(new SensorConfig[sensorConfigs.size()]));
		tagConfiguration.setMeasurePeriod(Integer.valueOf(measurementPeriod));
		tagConfiguration.setVibrationThreshold(null);
	
		tagConfiguration.setSiteConfigurationId(Long.valueOf(siteId));
		
		tagConfiguration.setLat(Double.valueOf(lat));
		tagConfiguration.setLon(Double.valueOf(lon));
		tagConfiguration.setZoomLevel(Integer.valueOf(zoomLevel));
		
		if ( FieldUtil.isFieldNonBlank(manHoleIdStr) ) {
			tagConfiguration.setManHoleId(manHoleIdStr);
		}
		if ( FieldUtil.isFieldNonBlank(manHoleHeightStr) ) {
			tagConfiguration.setManHoleHeight( Integer.valueOf(manHoleHeightStr));
		}
		if ( FieldUtil.isFieldNonBlank(createDateTimeStr) ) {
			tagConfiguration.setCreateDateTime(createDateTimeStr);
		}
		
//		if ( FieldUtil.isFieldNonBlank(readerInSequence) ) {
//			tagConfiguration.setReaderInSequence(Boolean.valueOf(readerInSequence)); 
//		}
//		if ( FieldUtil.isFieldNonBlank(readerInSequenceInMs) ) {
//			tagConfiguration.setReaderInSequenceInMs(Integer.valueOf(readerInSequenceInMs)); 
//		}
//		if ( FieldUtil.isFieldNonBlank(readerWithHighestRssi) ) {
//			tagConfiguration.setReaderWithHighestRssi(Boolean.valueOf(readerWithHighestRssi)); 
//		}
//		if ( FieldUtil.isFieldNonBlank(readerWithHighestRssiInMs) ) {
//			tagConfiguration.setReaderWithHighestRssiInMs(Integer.valueOf(readerWithHighestRssiInMs)); 
//		}
//		if ( FieldUtil.isFieldNonBlank(assignToSpecificReaders) ) {
//			tagConfiguration.setAssignToSpecificReaders(Boolean.valueOf(assignToSpecificReaders)); 
//		}
//		if ( FieldUtil.isFieldNonBlank(assignToSpecificReadersInMs) ) {
//			tagConfiguration.setAssignToSpecificReadersInMs(Integer.valueOf(assignToSpecificReadersInMs)); 
//		}
		
		if (tagConfiguration.getId() == 0) {
			if ( fileName != null && !fileName.isEmpty()) {
				
				if ( imageUriBase64Data != null && !imageUriBase64Data.equals("null") ) {
					tagConfigurationDao.writeMapFileBase64(imageUriBase64Data, tagConfiguration, fileName);
				}
				else if ( fileData != null && !fileData.equals("null")) {
					tagConfigurationDao.writeMapFile(fileData, tagConfiguration, fileName);
				}
			} 
			
			TagConfiguration newTagConfiguration = tagConfigurationService.createTagConfiguration(tagConfiguration);
			newTagConfiguration.setValidationSubject(this.createTagSubject);
			newTagConfiguration.setValidationMessages(this.createTagSuccessful);
			return newTagConfiguration;
		} else {
			String existingImage = tagConfiguration.getImageFileName();
			
			if ( fileName != null && !fileName.isEmpty()) {
				if ( imageUriBase64Data != null && !imageUriBase64Data.equals("null") ) {
					tagConfigurationDao.writeMapFileBase64(imageUriBase64Data, tagConfiguration, fileName);
				} else {
					if ("<deleted_@>".equals(fileName))
						tagConfigurationDao.deleteMapFile(tagConfiguration, existingImage);
					else {
						if ( existingImage == null || existingImage.isEmpty() ) {
							tagConfigurationDao.writeMapFile(fileData, tagConfiguration, fileName);
						} else {
							if (! existingImage.equals(fileName)) {
								tagConfigurationDao.deleteMapFile(tagConfiguration, existingImage);
								tagConfigurationDao.writeMapFile(fileData, tagConfiguration, fileName);
							}
						}				
					}					
				}
			}		
			TagConfiguration updatedTagConfiguration = tagConfigurationService.updateTagConfiguration(tagConfiguration, true);
			updatedTagConfiguration.setValidationSubject(this.updateTagSubject);
			updatedTagConfiguration.setValidationMessages(this.updateTagSuccessful);
			return updatedTagConfiguration;
		}
	}

	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteData")
	public TagConfiguration deleteTagConfiguration(@QueryParam("id") Long id) {
		
		List<AlertSetting> alertSettings = alertSettingService.searchByCriteria(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, id, true);
		if ( alertSettings == null || alertSettings.size() == 0) {
			TagConfiguration deletedTag =  tagConfigurationService.deleteTagConfiguration(id);
			deletedTag.setValidationSubject(deleteTagSubject);
			deletedTag.setValidationMessages(deleteTagSuccessful);
			return deletedTag;
		} else {
			TagConfiguration validationError = new TagConfiguration();
			validationError.setValidationSubject(deleteTagSubject);
			
			String alertSettingNames = "";
			for (AlertSetting alertSetting : alertSettings ) {
				alertSettingNames += (alertSetting.getName()); 
				if ( alertSetting != alertSettings.get(alertSettings.size()-1) ) {
					alertSettingNames += (", ");
				}
			}
			String message = "Cannot delete tag configuration.\nAlert settings [ " + alertSettingNames + " ] are found using the tag configuration.";
			validationError.setFailedValidationMessage(message);
			return validationError;
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findTagConfigurationById")
	public TagConfiguration findTagConfigurationById(@QueryParam("id") Long id) {
		TagConfiguration result = tagConfigurationService.retrieveByTagConfigurationId(id);
		return result;
	}
}
