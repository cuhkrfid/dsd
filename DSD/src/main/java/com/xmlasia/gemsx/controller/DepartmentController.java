package com.xmlasia.gemsx.controller;

import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.domain.Department;
import com.xmlasia.gemsx.service.DepartmentService;

@Path("/department")
@Singleton
public class DepartmentController {

	private static Logger logger = Logger.getLogger(DepartmentController.class);

	private DepartmentService departmentService;
	
	private Client client;

	public DepartmentController(@Context ServletContext ctx) {
		departmentService = (DepartmentService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("departmentService");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findDepartmentById")
	public Department findDepartmentById(@QueryParam("id") Integer id) {
		Department result = departmentService.retrieveByDepartmentId(id);
		result.setCompany(null);
		return result;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<Department> searchData(@QueryParam("id") String id, @QueryParam("chiName") String chiName,
			@QueryParam("engName") String engName) {

		List<Department> result = departmentService.searchByCriteria(id, chiName, engName);
		for (Department d : result){
			d.setCompany(null);
		}
		return result;
	}

//	@POST
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/addData")
//	public Department addDepartment(@QueryParam("chiName") String chiName, @QueryParam("engName") String engName) {
//		Department department = new Department();
//		department.setChiName(chiName);
//		department.setEngName(engName);
//	
//		//TODO
//		d.setCompanyId(1);
//
//		Department newDepartment = departmentService.createDepartment(user);
//		newDepartment.setCompany(null);
//		return newDepartment;
//	}

//	@DELETE
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/deleteData")
//	public Department deleteDepartment(@QueryParam("id") Integer id) {
//		return departmentService.deleteDepartment(id);
//	}

//	@PUT
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/updateData")
//	public Department updateDepartment(@QueryParam("id") Integer id, @QueryParam("chiName") String chiName,
//			@QueryParam("engName") String engName) {
//		Department department = departmentService.retrieveByUserId(id);
//
//		department.setChiName(chiName);
//		department.setEngName(engName);
//
//		Department newDepartment = departmentService.updateUser(department);
//		newDepartment.setCompany(null);
//		return newDepartment;
//	}

}