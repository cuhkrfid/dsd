package com.xmlasia.gemsx.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.domain.Alert;
import com.xmlasia.gemsx.service.AlertService;


@Path("/alert")
@Singleton
public class AlertController {

	private static Logger logger = Logger.getLogger(AlertController.class);
	
	private AlertService alertService;
	
	public AlertController(@Context ServletContext ctx) {
		alertService = (AlertService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("alertService");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAlert")
	public List<Alert> searchData(
			@QueryParam("id") String id, 
			@QueryParam("fromDate") String fromDate, 
			@QueryParam("toDate") String toDate, 
			@QueryParam("level") String level,
			@QueryParam("alertSettingId") String alertSettingId,
			@QueryParam("tagConfigurationId") String tagConfigurationId,
			@QueryParam("siteConfigurationId") String siteConfigurationId,
			@QueryParam("desc") String desc) { 
		
		if ( alertSettingId != null && Long.valueOf(alertSettingId) == 0) {
			return new ArrayList<Alert>();
		}
		
		List<Alert> alerts = alertService.searchByCriteria(id, fromDate, toDate, level, alertSettingId, tagConfigurationId, siteConfigurationId, desc);
		return alerts;
	}


}
