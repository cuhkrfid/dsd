package com.xmlasia.gemsx.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.AlertCriteriaDao;
import com.xmlasia.gemsx.dao.SensorConfigDao;
import com.xmlasia.gemsx.dao.TagConfigurationDao;
import com.xmlasia.gemsx.domain.AlertSetting;
import com.xmlasia.gemsx.domain.AlertSettingSensorConfig;
import com.xmlasia.gemsx.domain.SensorConfig;
import com.xmlasia.gemsx.service.AlertSettingService;


@Path("/alertSetting")
@Singleton
public class AlertSettingController {

	private static Logger logger = Logger.getLogger(AlertSettingController.class);
	
	private SensorConfigDao sensorConfigDao;
	private TagConfigurationDao tagConfigurationDao;
	private AlertCriteriaDao alertCriteriaDao;
	private AlertSettingService alertSettingService;
	
	public AlertSettingController(@Context ServletContext ctx) {
		sensorConfigDao = (SensorConfigDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("sensorConfigDao");
		tagConfigurationDao = (TagConfigurationDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("tagConfigurationDao");
		alertCriteriaDao = (AlertCriteriaDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("alertCriteriaDao");
		alertSettingService = (AlertSettingService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("alertSettingService");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAlertSetting")
	public List<AlertSetting> searchData(
			@QueryParam("id") String id, 
			@QueryParam("name") String name,
			@QueryParam("level") String level,
			@QueryParam("useDateRange") String useDateRange,
			@QueryParam("fromDate") String fromDate, 
			@QueryParam("toDate") String toDate, 
			@QueryParam("deviceAddress") String deviceAddress, 
			@QueryParam("deviceId") String deviceId, 
			@QueryParam("groupId") String groupId, 
			@QueryParam("isSendEmail") String isSendEmail, 
	
			@QueryParam("isCoverLevel") String isCoverLevel, 
			@QueryParam("isWaterLevel") String isWaterLevel, 
			@QueryParam("isH2s") String isH2s, 
			@QueryParam("isSo2") String isSo2, 
			@QueryParam("isCo2") String isCo2, 
			@QueryParam("isCh4") String isCh4, 
			@QueryParam("isNh3") String isNh3, 
			@QueryParam("isO2") String isO2 ) { 
		return alertSettingService.searchByCriteria(id, name, level, useDateRange, fromDate, toDate, deviceAddress, deviceId, groupId, isCoverLevel, 
				isWaterLevel, isCo2, isH2s, isSo2, isCh4, isNh3, isO2, null, null, true);
	}
	
	@POST
	@Path("/addAlertSetting")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public AlertSetting addAlertSetting(
			@FormDataParam("name") String name,
			@FormDataParam("level") String level,
			@FormDataParam("useDateRange") String useDateRange,
			@FormDataParam("fromDate") String fromDate, 
			@FormDataParam("toDate") String toDate, 
			@FormDataParam("deviceAddress") String deviceAddress, 
			@FormDataParam("deviceId") String deviceId, 
			@FormDataParam("siteId") String siteId, 
			@FormDataParam("groupId") String groupId, 
			
			@FormDataParam("useSiteId") String useSiteId, 
			@FormDataParam("useGroupId") String useGroupId, 
			
			@FormDataParam("isSendEmail") String isSendEmail, 
			@FormDataParam("emails") String emails, 
			
			@FormDataParam("isRepeatAlert") String isRepeatAlert, 
			@FormDataParam("repeatAlertInSecs") String repeatAlertInSecs,
			@FormDataParam("alertCriteriaId") Long alertCriteriaId,
			@FormDataParam("alertSettingSensorConfigs") String alertSettingSensorConfigsStr) {
		
		AlertSetting alertSetting = new AlertSetting();
		alertSetting.setName(name);
		alertSetting.setLevel(level);
		alertSetting.setUseDateRange(Boolean.valueOf(useDateRange));
		
		alertSetting.setUseSiteId(Boolean.valueOf(useSiteId));
		alertSetting.setUseGroupId(Boolean.valueOf(useGroupId));
		
		if (fromDate.equals("null") == false && toDate.equals("null") == false) {
			alertSetting.setFromDate( new Date(Long.valueOf(fromDate)));
			alertSetting.setToDate(new Date(Long.valueOf(toDate)));
		} else {
			alertSetting.setFromDate(null);
			alertSetting.setToDate(null);			
		}
		
		alertSetting.setDeviceAddress(deviceAddress);
		
		if (!siteId.isEmpty() && siteId.equals("null") == false )
			alertSetting.setSiteId(Long.valueOf(siteId));
		else
			alertSetting.setSiteId(null);
		
		if (!groupId.isEmpty() && groupId.equals("null") == false )
			alertSetting.setGroupId(Integer.valueOf(groupId));
		else
			alertSetting.setGroupId(null);
		
		alertSetting.setIsSendEmail(Boolean.valueOf(isSendEmail));
		if (emails.equals("null") == false )
			alertSetting.setEmails(emails);
		else 
			alertSetting.setEmails(null);
			
		
		alertSetting.setIsRepeatAlert(Boolean.valueOf(isRepeatAlert));
		if(!repeatAlertInSecs.equals("null"))
			alertSetting.setRepeatAlertInSecs(Integer.valueOf(repeatAlertInSecs));
		
		alertSetting.setAlertCriteriaId(alertCriteriaId);
		alertSetting.setAlertSettingSensorConfigs(readJSONAlertSettingSensorConfig(alertSettingSensorConfigsStr));
		
		AlertSetting newAlertSetting = (AlertSetting)alertSettingService.createAlertSetting(alertSetting);
		newAlertSetting.setValidationSubject("New Alert Setting");
		newAlertSetting.setValidationMessages("Save alert setting success");
		newAlertSetting.setAlertCriteria( alertCriteriaDao.retrieveAlertCriteria(newAlertSetting.getAlertCriteriaId()) );
		
		return newAlertSetting;
	}

	private AlertSettingSensorConfig[] readJSONAlertSettingSensorConfig(String alertSettingSensorConfigsStr) {
		JSONArray jsonArray;
		List<AlertSettingSensorConfig> alertSettingSensorConfigs = new ArrayList<AlertSettingSensorConfig>();
		
		try {
			JSONObject jsnobject = new JSONObject(alertSettingSensorConfigsStr);
			jsonArray = jsnobject.getJSONArray("alertSettingSensorConfigs");
			
		    for (int i = 0; i < jsonArray.length(); i++) {
		        JSONObject explrObject = jsonArray.getJSONObject(i);
		        String alertSettingIdStr = (String)explrObject.get("alertSettingId");
		        String tagConfigurationIdStr = (String)explrObject.get("tagConfigurationId");
		       
		        long alertSettingId = Long.valueOf(alertSettingIdStr);
		        long tagConfigurationId = Long.valueOf(tagConfigurationIdStr);
		        
		        AlertSettingSensorConfig newAlertSettingSensorConfig = new AlertSettingSensorConfig();
		        newAlertSettingSensorConfig.setAlertSettingId(alertSettingId);
		        newAlertSettingSensorConfig.setTagConfigurationId(tagConfigurationId);	
		        alertSettingSensorConfigs.add(newAlertSettingSensorConfig);
		    }
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return alertSettingSensorConfigs.toArray(new AlertSettingSensorConfig[alertSettingSensorConfigs.size()]);
	}

	@POST
	@Path("/updateAlertSetting")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public AlertSetting updateAlertSetting(
			@FormDataParam("id") String id,
			@FormDataParam("name") String name,
			@FormDataParam("level") String level,
			@FormDataParam("useDateRange") String useDateRange,
			@FormDataParam("fromDate") String fromDate, 
			@FormDataParam("toDate") String toDate, 
			@FormDataParam("deviceAddress") String deviceAddress, 
			@FormDataParam("deviceId") String deviceId,
			@FormDataParam("siteId") String siteId, 
			@FormDataParam("groupId") String groupId, 
			
			@FormDataParam("useSiteId") String useSiteId, 
			@FormDataParam("useGroupId") String useGroupId, 
			
			
			@FormDataParam("isSendEmail") String isSendEmail, 
			@FormDataParam("emails") String emails, 

			@FormDataParam("isRepeatAlert") String isRepeatAlert, 
			@FormDataParam("repeatAlertInSecs") String repeatAlertInSecs,
			
			@FormDataParam("alertCriteriaId") Long alertCriteriaId,
			@FormDataParam("alertSettingSensorConfigs") String alertSettingSensorConfigsStr) {
		
		AlertSetting alertSetting = alertSettingService.retrieveByAlertSettingId(Integer.valueOf(id));
		alertSetting.setName(name);
		alertSetting.setLevel(level);
		alertSetting.setUseDateRange(Boolean.valueOf(useDateRange));
		
		alertSetting.setUseSiteId(Boolean.valueOf(useSiteId));
		alertSetting.setUseGroupId(Boolean.valueOf(useGroupId));

		
		if (fromDate.equals("null") == false && toDate.equals("null") == false) {
			alertSetting.setFromDate( new Date(Long.valueOf(fromDate)));
			alertSetting.setToDate(new Date(Long.valueOf(toDate)));
		} else {
			alertSetting.setFromDate(null);
			alertSetting.setToDate(null);			
		}
		
		alertSetting.setDeviceAddress(deviceAddress);
		if (!siteId.isEmpty() && siteId.equals("null") == false )
			alertSetting.setSiteId(Long.valueOf(siteId));
		else
			alertSetting.setSiteId(null);
		
		if (!groupId.isEmpty() && groupId.equals("null") == false )
			alertSetting.setGroupId(Integer.valueOf(groupId));
		else
			alertSetting.setGroupId(null);
		
		alertSetting.setIsSendEmail(Boolean.valueOf(isSendEmail));
		if (emails.equals("null") == false )
			alertSetting.setEmails(emails);
		else 
			alertSetting.setEmails(null);
		
		alertSetting.setIsRepeatAlert(Boolean.valueOf(isRepeatAlert));
		if(!repeatAlertInSecs.equals("null"))
			alertSetting.setRepeatAlertInSecs(Integer.valueOf(repeatAlertInSecs));
		
		alertSetting.setAlertCriteriaId(alertCriteriaId);
		alertSetting.setAlertSettingSensorConfigs(readJSONAlertSettingSensorConfig(alertSettingSensorConfigsStr));
		
		AlertSetting updatedAlertSetting =  (AlertSetting)alertSettingService.updateAlertSetting(alertSetting);
		updatedAlertSetting.setValidationSubject("Update Alert Setting");
		updatedAlertSetting.setValidationMessages("Save alert setting success");
		updatedAlertSetting.setAlertCriteria( alertCriteriaDao.retrieveAlertCriteria(updatedAlertSetting.getAlertCriteriaId()) );		
		return updatedAlertSetting;
		
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteData")
	public AlertSetting deleteAlertSetting(@QueryParam("id") Long id) {
		AlertSetting a = alertSettingService.deleteAlertSetting(id);
		
		a.setValidationSubject("Delete Alert Setting");
		a.setValidationMessages("Delete alert setting success");
		
		return a;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSensor")
	public SensorConfig getSensor(@QueryParam("id") String id ){
		List<SensorConfig> sensorConfigs =  sensorConfigDao.retrieveSensorByDeviceIdAndSensorId(null,Long.valueOf(id),false, null);
		if ( sensorConfigs != null && sensorConfigs.size() > 0) {
			return sensorConfigs.get(0);
		} 
		return null;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAllGroupId")
	public List<String> getAllGroupId(){
		Set<String> groupIdsStr = new TreeSet<String>();
		List<Integer> groupIds =  tagConfigurationDao.listAllGroupIds();
		if (groupIds != null) {
			for (Integer groupId : groupIds) {
				groupIdsStr.add(String.valueOf(groupId));
			}
		}
		
		return new ArrayList<String>(groupIdsStr);
	}
}
