package com.xmlasia.gemsx.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.NetworkDao;
import com.xmlasia.gemsx.dao.ReaderHeartBeatDao;
import com.xmlasia.gemsx.device.WebClientNetworkEndPoint;
import com.xmlasia.gemsx.domain.Network;
import com.xmlasia.gemsx.domain.ReaderHeartBeat;


@Path("/networkView")
@Singleton
public class NetworkViewController {

	private static Logger logger = Logger.getLogger(NetworkViewController.class);
	
	private NetworkDao networkDao;
	private ReaderHeartBeatDao readerHeartBeatDao; 
	
	public NetworkViewController(@Context ServletContext ctx) {
		setNetworkDao((NetworkDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("networkDao"));
		
		readerHeartBeatDao = (ReaderHeartBeatDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("readerHeartBeatDao");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getTableData")
	public List<Network> getTableAllNetwork(){
		List<Network> result =  networkDao.listAll();
		List<Network> connectedNetwork =  new ArrayList<Network>();
		
		if ( result != null) {
			for (Network network : result) {
				if ( network.getConnected() == false ) 
					continue; 
				
		    	DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String dateString = df.format(network.getUpdatedTime());
				network.setDateString(dateString);
				network.setTimeDiffInMsStr( network.getTimeDiffInMs() != null ? String.valueOf(network.getTimeDiffInMs()) : "N/A" );
				if ( network.getDeviceId() == null ) {
					network.setDeviceIdStr("N/A");
				} else {
					network.setDeviceIdStr(String.valueOf(network.getDeviceId()));
				}
				
				if ( network.getRouterId() == null ) {
					network.setRouterIdStr("N/A");
				} else {
					network.setRouterIdStr(String.valueOf(network.getRouterId()));
				}
				
				connectedNetwork.add(network);
			}
		}
		return connectedNetwork;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<Network> searchData(
			@QueryParam("id") String id, 
			@QueryParam("cordId") String cordId,
			@QueryParam("deviceId") String deviceId, 
			@QueryParam("routerId") String routerId,
			@QueryParam("deviceType") String deviceType, 
			@QueryParam("parentAddress") String parentAddress, 
			@QueryParam("deviceAddress") String deviceAddress, 
			@QueryParam("connected") Boolean connected, 
			@QueryParam("orphaned") Boolean orphaned ) {
		return networkDao.searchByCriteria(id, cordId, deviceId, routerId, deviceType, parentAddress, deviceAddress, connected, orphaned, true);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchNetwork")
	public Network searchNetwork(
			@QueryParam("id") String id, 
			@QueryParam("deviceType") String deviceType ) {
		if ( id != null && ! id.isEmpty() 
			 && deviceType != null && !deviceType.isEmpty()) {
		
			if ( Network.END.equals(deviceType)) {
				List<Network> networks = networkDao.searchByCriteria(null, null, id, null, String.valueOf(Network.ENDDEVICE), null, null, null, null, true);
				if ( networks != null && networks.size() > 0) {
					return networks.get(0);
				}
			}
			
			if ( Network.ROUT.equals(deviceType)) {
				List<Network> networks = networkDao.searchByCriteria(null, null, null, id,  String.valueOf(Network.ROUTER), null, null, null, null, true);
				if ( networks != null && networks.size() > 0) {
					return networks.get(0);
				}
			}
		}
	
		return null;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchReaderHeartBeat")
	public List<ReaderHeartBeat> searchReaderHeartBeat(@QueryParam("id") String idStr,
												 @QueryParam("readerId") String readerIdStr, 
												 @QueryParam("dateFrom") String dateFromLong, 
												 @QueryParam("dateTo") String dateToLong, 
												 @QueryParam("fromRssi") String fromRssiStr, 
												 @QueryParam("toRssi") String toRssiStr, 
												 @QueryParam("desc") Boolean descending ) {
		List<ReaderHeartBeat> readerHeartBeats =  readerHeartBeatDao.searchByCriteria(idStr, readerIdStr, dateFromLong, dateToLong, fromRssiStr, toRssiStr, descending, null);
		if ( readerHeartBeats != null) {
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			
			for (ReaderHeartBeat readerHeartBeat : readerHeartBeats) {
				String dateString2 = df.format(readerHeartBeat.getInputDate());
				readerHeartBeat.setDateString2(dateString2);
			}
		}
		return readerHeartBeats;
	}
								
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getChartData")
	public String getChartAllNetwork(){
		return networkDao.getNetworkChart();
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteNetwork")
	public String deleteNetwork(@QueryParam("id") Long id) {
		return networkDao.deleteNetwork(id);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/resetMaxTimeDiff")
	public boolean resetMaxTimeDiff(
			@QueryParam("deviceAddress") String deviceAddress ) {
		WebClientNetworkEndPoint.resetMaxTimeDiff(deviceAddress);
		return true;
	}
	
	public NetworkDao getNetworkDao() {
		return networkDao;
	}


	public void setNetworkDao(NetworkDao networkDao) {
		this.networkDao = networkDao;
	}	
	
}
