package com.xmlasia.gemsx.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.domain.Menu;
import com.xmlasia.gemsx.service.MenuService;

@Path("/menu")
@Singleton
public class MenuController {
	private static Logger logger = Logger.getLogger(MenuController.class);
	
	private MenuService menuService;
	
	public MenuController(@Context ServletContext ctx) {
		menuService = (MenuService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("menuService");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<Menu> searchData(@QueryParam("id") String id, @QueryParam("name") String name, @QueryParam("module") String module,
			@QueryParam("description") String description, @QueryParam("displayOrder") Integer displayOrder,
			@QueryParam("active") Boolean active, @QueryParam("path") String path, @QueryParam("useOrder") Boolean useOrder) {
	
		List<Menu> result = menuService.searchByCriteria(id, name, module, description, displayOrder, active, path, useOrder);
		return result;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findMenuById")
	public Menu findMenuById(@QueryParam("id") Long id) {
		Menu result = menuService.retrieveByMenuId(id);
		return result;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findMenuByModuleId")
	public List<Menu> findMenuByModuleId(@QueryParam("moduleId") Long moduleId) {
		List<Menu> result = menuService.retrieveMenuByModuleId(moduleId);
		return result;
	}
}
