package com.xmlasia.gemsx.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.GroupPolyLineDao;
import com.xmlasia.gemsx.dao.GroupTagConfigurationDao;
import com.xmlasia.gemsx.dao.GroupTagConfigurationMapDao;
import com.xmlasia.gemsx.domain.GroupPolyLine;
import com.xmlasia.gemsx.domain.GroupTagConfiguration;
import com.xmlasia.gemsx.domain.GroupTagConfigurationMap;

@Path("/groupTagConfiguration")
@Singleton
public class GroupTagConfigurationController {

	private static Logger logger = Logger.getLogger(GroupTagConfigurationController.class);
	private GroupTagConfigurationDao groupTagConfigurationDao;
	private GroupTagConfigurationMapDao groupTagConfigurationMapDao;
	private GroupPolyLineDao groupPolyLineDao;
	
	
	private	final String deleteGroupTagSubject = "Delete Group Tag Configuration";
	private final String deleteGroupTagSuccessful= "Delete group tag successful";

	private	final String updateGroupTagSubject = "Update Group Tag Configuration";
	private final String updateGroupTagSuccessful = "Update group tag successful";
	
	private	final String createGroupTagSubject = "Create Group Tag Configuration";
	private final String createGroupTagSuccessful= "Create group tag successful";
	
	public GroupTagConfigurationController(@Context ServletContext ctx) {
		groupTagConfigurationDao = (GroupTagConfigurationDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("groupTagConfigurationDao");
		groupTagConfigurationMapDao = (GroupTagConfigurationMapDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("groupTagConfigurationMapDao");
		groupPolyLineDao = (GroupPolyLineDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("groupPolyLineDao");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<GroupTagConfiguration> searchData(
			@QueryParam("id") String id, 
			@QueryParam("fromDate") String fromDate, 
			@QueryParam("toDate") String toDate, 
			@QueryParam("siteConfigurationId") String siteConfigurationId,
			@QueryParam("tagConfigurationId") String tagConfigurationId,
			@QueryParam("alertId") String alertId,
			@QueryParam("name") String name,
			@QueryParam("desc") String desc) { 
		
		List<GroupTagConfiguration> groupTagConfigurations = new ArrayList<GroupTagConfiguration>();
		
		List<GroupTagConfiguration> result = groupTagConfigurationDao.searchByCriteria(id, fromDate, toDate, siteConfigurationId, tagConfigurationId, alertId, name, desc);
		for (GroupTagConfiguration groupTagConfiguration : result) {
		
			List<GroupPolyLine> groupPolyLines = groupPolyLineDao.searchByCriteria(null, String.valueOf(groupTagConfiguration.getId()), String.valueOf(groupTagConfiguration.getSiteConfigurationId()) ); 
			groupTagConfiguration.setPolylines(groupPolyLines);
			
			groupTagConfigurations.add(groupTagConfiguration);
		}
		
		return groupTagConfigurations;
	}

	@POST
	@Path("/addData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public GroupTagConfiguration addGroupTagConfiguration(
			@FormDataParam("id") String idStr,
			@FormDataParam("name") String name,
			@FormDataParam("siteId") String siteId,
			@FormDataParam("tagConfigurationIds") String tagConfigurationIdsStr,
			@FormDataParam("polyLine") String polyLineStr) {
		
		GroupTagConfiguration savedGroupTagConfiguration = null;
		Long id = Long.valueOf(idStr); 
		String[] tagConfigurationArrayStrs = tagConfigurationIdsStr.split(",");
		String[] jsonPolyLineStrs = polyLineStr.split("&&");
		
		List<GroupTagConfiguration> groupTagConfigurations = searchData(null, null, null, null, null, null, name, null);
		if ( groupTagConfigurations != null && groupTagConfigurations.size() > 0 ) {
			savedGroupTagConfiguration = groupTagConfigurations.get(0);
			
			String message = "Group name:%s already assigned to existing group tag configuration.\nSave group tag configuration failed.";
        	message = String.format(message, savedGroupTagConfiguration.getName());
        	savedGroupTagConfiguration.setValidationSubject(id == 0 ? createGroupTagSubject : updateGroupTagSubject);
			savedGroupTagConfiguration.setFailedValidationMessage(message);
        	return savedGroupTagConfiguration;
     	}
				
		if ( id == 0) {
			GroupTagConfiguration groupTagConfiguration = new GroupTagConfiguration();
			groupTagConfiguration.setName(name);
			groupTagConfiguration.setSiteConfigurationId(Long.valueOf(siteId));
			savedGroupTagConfiguration = groupTagConfigurationDao.createGroupTagConfiguration(groupTagConfiguration);
			
			try {
				JSONArray jsonArray;
				long lineNo = 1;
				for ( String jsonPolyLineStr : jsonPolyLineStrs) {
					JSONObject jsnobject = new JSONObject(jsonPolyLineStr);
					jsonArray = jsnobject.getJSONArray("polyLine");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject explrObject = jsonArray.getJSONObject(i);
						String lat = (String)explrObject.get("lat");
					    String lng = (String)explrObject.get("lng");
					    String tagConfigurationIds = (String)explrObject.get("tagConfigurationIds");
					    
					    tagConfigurationIds = tagConfigurationIds.replaceAll("AND", ",");
					    
					    
					    GroupPolyLine groupPolyLine = new GroupPolyLine();
					    groupPolyLine.setLineNo(lineNo);
					    groupPolyLine.setGroupTagConfigurationId(savedGroupTagConfiguration.getId());
					    groupPolyLine.setSiteConfigurationId(savedGroupTagConfiguration.getSiteConfigurationId());
					    groupPolyLine.setLat(Double.valueOf(lat));
					    groupPolyLine.setLng(Double.valueOf(lng));
					    groupPolyLine.setTagConfigurationIdsStr(tagConfigurationIds);
					    groupPolyLineDao.createGroupPolyLine(groupPolyLine);
					}
					lineNo++;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
			}
			
			for ( String tagConfigurationArrayStr : tagConfigurationArrayStrs) {
				GroupTagConfigurationMap groupTagConfigurationMap = new GroupTagConfigurationMap();
				groupTagConfigurationMap.setGroupTagConfigurationId(savedGroupTagConfiguration.getId());
				groupTagConfigurationMap.setTagConfigurationId(Long.valueOf(tagConfigurationArrayStr));
				groupTagConfigurationMapDao.createGroupTagConfigurationMap(groupTagConfigurationMap);
			}
		}
		
		if( savedGroupTagConfiguration != null) {
			savedGroupTagConfiguration.setValidationSubject(this.createGroupTagSubject);
			savedGroupTagConfiguration.setValidationMessages(this.createGroupTagSuccessful);
		} 
		
		return savedGroupTagConfiguration;
	}
}
