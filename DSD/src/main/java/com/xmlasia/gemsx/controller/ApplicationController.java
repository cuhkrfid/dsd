package com.xmlasia.gemsx.controller;

import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.domain.Application;
import com.xmlasia.gemsx.service.ApplicationService;

@Path("/application")
@Singleton
public class ApplicationController {

	private static Logger logger = Logger.getLogger(ApplicationController.class);

	private ApplicationService applicationService;
	
	public ApplicationController(@Context ServletContext ctx) {
		applicationService = (ApplicationService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("applicationService");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/searchData")
//	public List<Application> searchData(@QueryParam("id") String id, @QueryParam("name") String name,
//			@QueryParam("description") String description, @QueryParam("displayOrder") Integer displayOrder,
//			@QueryParam("active") Boolean active, @QueryParam("path") String path, @QueryParam("useOrder") Boolean useOrder) {
//	
//		List<Application> result = applicationService.searchByCriteria(id, name, description, displayOrder, active, path, useOrder);
//		return result;
//	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public String searchData(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("description") String description, @QueryParam("displayOrder") Integer displayOrder,
			@QueryParam("active") Boolean active, @QueryParam("path") String path, @QueryParam("useOrder") Boolean useOrder) {
	
		
		
		JsonObject jdata = new JsonObject();
        jdata.addProperty("id", 666);
        jdata.addProperty("temperature", 25);
        
        JsonArray jdataa = new JsonArray();
        jdataa.add(jdata);

        
        jdata = new JsonObject();
        jdata.addProperty("id", 6662);
        jdata.addProperty("temperature", 32);
        jdataa.add(jdata);   
        
        return jdataa.toString();
	}
	
	@POST
	@Path("/addData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Application addApplication(@FormDataParam("name") String name,
			@FormDataParam("description") String description, @FormDataParam("displayOrder") Integer displayOrder,
			@FormDataParam("active") Boolean active, @FormDataParam("path") String path, @FormDataParam("fileName") String fileName, @FormDataParam("file") InputStream fileData) {
		
		Application application = new Application();
		application.setName(name);
		application.setDescription(description);
		application.setDisplayOrder(displayOrder);
		application.setActive(active);
		application.setPath(path);		
		Application newApplication = applicationService.createApplication(application);
		return newApplication;
	}

	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteData")
	public Application deleteApplication(@QueryParam("id") Long id) {
		return applicationService.deleteApplication(id);
	}

	@POST
	@Path("/updateData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Application updateApplication(@FormDataParam("id") Integer id, 
									  @FormDataParam("name") String name,
									  @FormDataParam("description") String description, 
									  @FormDataParam("displayOrder") Integer displayOrder,
									  @FormDataParam("active") Boolean active, 
									  @FormDataParam("path") String path, 
									  @FormDataParam("fileName") String fileName,
									  @FormDataParam("file") InputStream fileData) {
		
		Application application = applicationService.retrieveByApplicationId(id);

		application.setName(name);
		application.setDescription(description);
		application.setDisplayOrder(displayOrder);
		application.setActive(active);
		application.setPath(path);

	   	Application updatedApplication = applicationService.updateApplication(application);
		
		return updatedApplication;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findApplicationById")
	public Application findApplicationById(@QueryParam("id") Long id) {
		Application result = applicationService.retrieveByApplicationId(id);
		return result;
	}
}
