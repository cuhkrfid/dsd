package com.xmlasia.gemsx.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.domain.Opex;
import com.xmlasia.gemsx.service.OpexService;

@Path("/opex")
@Singleton
public class OpexController {

	private static Logger logger = Logger.getLogger(OpexController.class);

	private OpexService opexService;
	
	private Client client;
	
	public OpexController(@Context ServletContext ctx) {
		opexService = (OpexService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("opexService");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<Opex> searchData(@QueryParam("id") String id, @QueryParam("mainCategoryCode") String mainCategoryCode,
			@QueryParam("mainCategoryName") String mainCategoryName, @QueryParam("categoryCode") String categoryCode,
			@QueryParam("categoryName") String categoryName, @QueryParam("subCategoryCode") String subCategoryCode,
			@QueryParam("subCategoryName") String subCategoryName, @QueryParam("remark") String remark) {

		
		List<Opex> result = opexService.searchByCriteria(id, mainCategoryCode, mainCategoryName, categoryCode, categoryName, subCategoryCode, subCategoryName, remark);
		return result;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addData")
	public Opex addOpex(@QueryParam("mainCategoryCode") String mainCategoryCode,
			@QueryParam("mainCategoryName") String mainCategoryName, @QueryParam("categoryCode") String categoryCode,
			@QueryParam("categoryName") String categoryName, @QueryParam("subCategoryCode") String subCategoryCode,
			@QueryParam("subCategoryName") String subCategoryName, @QueryParam("remark") String remark) {
		
		Opex opex = new Opex();
		opex.setMainCategoryCode(mainCategoryCode);
		opex.setMainCategoryName(mainCategoryName);
		opex.setCategoryCode(categoryCode);
		opex.setCategoryName(categoryName);
		opex.setSubCategoryCode(subCategoryCode);
		opex.setSubCategoryName(subCategoryName);
		opex.setRemark(remark);
		
		Opex newApplication = opexService.createOpex(opex);
		return newApplication;
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteData")
	public Opex deleteOpex(@QueryParam("id") Integer id) {
		return opexService.deleteOpex(id);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateData")
	public Opex updateOpex(@QueryParam("id") Integer id, @QueryParam("mainCategoryCode") String mainCategoryCode,
			@QueryParam("mainCategoryName") String mainCategoryName, @QueryParam("categoryCode") String categoryCode,
			@QueryParam("categoryName") String categoryName, @QueryParam("subCategoryCode") String subCategoryCode,
			@QueryParam("subCategoryName") String subCategoryName, @QueryParam("remark") String remark) {
		
		Opex opex = opexService.retrieveByOpexId(id);

		opex.setMainCategoryCode(mainCategoryCode);
		opex.setMainCategoryName(mainCategoryName);
		opex.setCategoryCode(categoryCode);
		opex.setCategoryName(categoryName);
		opex.setSubCategoryCode(subCategoryCode);
		opex.setSubCategoryName(subCategoryName);
		opex.setRemark(remark);

		Opex newOpex = opexService.updateOpex(opex);
		return newOpex;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findOpexById")
	public Opex findOpexById(@QueryParam("id") Integer id) {
		Opex result = opexService.retrieveByOpexId(id);
		return result;
	}
}
