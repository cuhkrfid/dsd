package com.xmlasia.gemsx.controller;

import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.TagConfigurationDao;
import com.xmlasia.gemsx.domain.SiteConfiguration;
import com.xmlasia.gemsx.domain.TagConfiguration;
import com.xmlasia.gemsx.domain.Validation;
import com.xmlasia.gemsx.service.SiteConfigurationService;

@Path("/siteConfiguration")
@Singleton
public class SiteConfigurationController {

	private static Logger logger = Logger.getLogger(SiteConfigurationController.class);

	private SiteConfigurationService siteConfigurationService;
	private TagConfigurationDao tagConfigurationDao; 
	
	private	final String deleteSiteSubject = "Delete Site Configuration";
	private final String deleteSiteSuccessful= "Delete site successful";

	private	final String updateSiteSubject = "Update Site Configuration";
	private final String updateSiteSuccessful = "Update site successful";
	
	private	final String createSiteSubject = "Create Site Configuration";
	private final String createSiteSuccessful= "Create site successful";

	
	public SiteConfigurationController(@Context ServletContext ctx) {
		siteConfigurationService = (SiteConfigurationService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("siteConfigurationService");
		tagConfigurationDao = (TagConfigurationDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("tagConfigurationDao");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);
		
		logger.debug("web serivce initialize");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<SiteConfiguration> searchData(
			@QueryParam("id") String id, 
			@QueryParam("name") String name,
			@QueryParam("description") String description,
			@QueryParam("defaultSite") Boolean defaultSite) {
	
		List<SiteConfiguration> result = siteConfigurationService.searchByCriteria(id, name, description, defaultSite);
		return result;
	}
	
	@POST
	@Path("/addData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public SiteConfiguration addSiteConfiguration(@FormDataParam("name") String name,
			@FormDataParam("description") String description, 
			@FormDataParam("defaultSite") Boolean defaultSite,
			@FormDataParam("lat") String lat,
			@FormDataParam("lon") String lon,
			@FormDataParam("zoomLevel") String zoomLevel,
			@FormDataParam("fileName") String fileName, 
			@FormDataParam("file") InputStream fileData) {
		
		SiteConfiguration siteConfiguration = new SiteConfiguration();
		siteConfiguration.setName(name);
		siteConfiguration.setDescription(description);
		siteConfiguration.setMapFileName(fileName);
		siteConfiguration.setDefaultSite(defaultSite);
		siteConfiguration.setLat(Double.valueOf(lat));
		siteConfiguration.setLon(Double.valueOf(lon));
		siteConfiguration.setZoomLevel(Integer.valueOf(zoomLevel));

		if ( defaultSite == true) {
			updateSiteConfigateDefaultSite();
		}
		
		SiteConfiguration newSiteConfiguration = siteConfigurationService.createSiteConfiguration(siteConfiguration, fileData);
		
		newSiteConfiguration.setValidationSubject(createSiteSubject);
		newSiteConfiguration.setValidationMessages(createSiteSuccessful);
		
		return newSiteConfiguration;
	}

	private void updateSiteConfigateDefaultSite() {
		List<SiteConfiguration> siteConfigurations = searchData(null,null,null, null);
		if ( siteConfigurations != null) {
			for (SiteConfiguration siteConfiguration1 : siteConfigurations) {
				siteConfiguration1.setDefaultSite(false);
				siteConfigurationService.updateSiteConfiguration(siteConfiguration1);
			}
		}
	}

	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteData")
	public SiteConfiguration deleteSiteConfiguration(@QueryParam("id") Long id) {
		
		Validation validationResult = validateDeleteSite(id);
		if (! validationResult.getIsValid())
			return (SiteConfiguration)validationResult;
		
		SiteConfiguration deletedSite = siteConfigurationService.deleteSiteConfiguration(id);
		
		deletedSite.setValidationSubject(deleteSiteSubject);
		deletedSite.setValidationMessages(deleteSiteSuccessful);
		return deletedSite;
	}

	private Validation validateDeleteSite(Long id) {
		
		
		List<SiteConfiguration> siteConfigurations = searchData(String.valueOf(id),null,null,null);
		if ( siteConfigurations == null || siteConfigurations.size() == 0)
			return new Validation();
		
		SiteConfiguration violatedSite = new SiteConfiguration(); 
		violatedSite.setValidationSubject(deleteSiteSubject);
		
		if ( siteConfigurations.get(0).getDefaultSite()) {
			violatedSite.setFailedValidationMessage("Unable to delete site since it is a default site");
			return violatedSite;
		}
	
		List<TagConfiguration> tagConfigurations = tagConfigurationDao.searchByCriteria(null, null, null, null, null, null, null, id);
		if (tagConfigurations != null && tagConfigurations.size() > 0) {
			
			String validationMessages = "";
			validationMessages += "Unable to delete site due to attached tag configurations:\n";
			for (TagConfiguration tagConfiguration : tagConfigurations) {
				validationMessages += ("Tag Id: " + tagConfiguration.getId() + ", Tag Name: " + tagConfiguration.getName() + "\n");
			}
			violatedSite.setFailedValidationMessage(validationMessages);

		}
		return violatedSite;
	}

	@POST
	@Path("/updateData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public SiteConfiguration updateSiteConfiguration(@FormDataParam("id") Integer id, 
									  @FormDataParam("name") String name,
									  @FormDataParam("description") String description, 
									  @FormDataParam("defaultSite") Boolean defaultSite,
									  @FormDataParam("lat") String lat,
									  @FormDataParam("lon") String lon,
									  @FormDataParam("zoomLevel") String zoomLevel,
									  @FormDataParam("fileName") String fileName,
									  @FormDataParam("file") InputStream fileData) {
		
		if ( defaultSite == true) {
			updateSiteConfigateDefaultSite();
		}
		SiteConfiguration siteConfiguration = siteConfigurationService.retrieveBySiteConfigurationId(id);
		siteConfiguration.setName(name);
		siteConfiguration.setDescription(description);
		siteConfiguration.setDefaultSite(defaultSite);
		siteConfiguration.setLat(Double.valueOf(lat));
		siteConfiguration.setLon(Double.valueOf(lon));
		siteConfiguration.setZoomLevel(Integer.valueOf(zoomLevel));
		
	   	SiteConfiguration updatedSiteConfiguration = siteConfigurationService.updateSiteConfiguration(siteConfiguration, fileName, fileData);
		updatedSiteConfiguration.setValidationSubject(updateSiteSubject);
		updatedSiteConfiguration.setValidationMessages(updateSiteSuccessful);
		return updatedSiteConfiguration;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findSiteConfigurationById")
	public SiteConfiguration findSiteConfigurationById(@QueryParam("id") Long id) {
		SiteConfiguration result = siteConfigurationService.retrieveBySiteConfigurationId(id);
		return result;
	}
	
}
