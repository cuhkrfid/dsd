package com.xmlasia.gemsx.controller;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.DeviceDataDao;
import com.xmlasia.gemsx.dao.SensorDao;
import com.xmlasia.gemsx.domain.DeviceData;


@Path("/deviceData")
@Singleton
public class DeviceDataController {

	private static Logger logger = Logger.getLogger(DeviceDataController.class);
	
	private DeviceDataDao deviceDataDao;
	private SensorDao sensorDao;
	
	public DeviceDataController(@Context ServletContext ctx) {
		deviceDataDao = (DeviceDataDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("deviceDataDao");
		sensorDao = (SensorDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("sensorDao");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<DeviceData> searchData(
			@QueryParam("id") String id, 
			@QueryParam("numberOfSensor") String numberOfSensor,
			@QueryParam("measurePeriod") String measurePeriod,
			@QueryParam("readerId") String readerId, 
			@QueryParam("parentAddress") String parentAddress, 
			@QueryParam("deviceAddress") String deviceAddress,
			@QueryParam("tagId") String tagId,
			@QueryParam("dateFrom") String dateFrom, 
			@QueryParam("dateTo") String dateTo,
			@QueryParam("desc") Boolean descending ) { 
		
		List<DeviceData> deviceDatas = deviceDataDao.searchByCriteria(id, numberOfSensor, measurePeriod, readerId, parentAddress, deviceAddress, tagId, dateFrom, dateTo, descending, null);
		return deviceDatas;
	}
	
}
