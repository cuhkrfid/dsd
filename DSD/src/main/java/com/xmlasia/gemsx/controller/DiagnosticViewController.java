package com.xmlasia.gemsx.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.DeviceDataDao;
import com.xmlasia.gemsx.dao.FieldUtil;
import com.xmlasia.gemsx.dao.SensorConfigDao;
import com.xmlasia.gemsx.dao.SensorDao;
import com.xmlasia.gemsx.dao.TagConfigurationDao;
import com.xmlasia.gemsx.device.DeviceService;
import com.xmlasia.gemsx.device.NioSocketServer;
import com.xmlasia.gemsx.device.WebClientDeviceDataEndPoint;
import com.xmlasia.gemsx.domain.DeviceData;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.SensorChartData;
import com.xmlasia.gemsx.domain.SensorConfig;
import com.xmlasia.gemsx.domain.TagConfiguration;
import com.xmlasia.gemsx.domain.Validation;
import com.xmlasia.gemsx.service.TagConfigurationService;
import com.xmlasia.gemsx.service.impl.ServerConfigService;

@Path("/diagnosticView")
@Singleton
public class DiagnosticViewController {

	private Log logger = LogFactory.getLog(this.getClass());
	
	private SensorConfigDao sensorConfigDao; 
	
	private TagConfigurationService tagConfigurationService;
	private TagConfigurationDao tagConfigurationDao; 
	
	private DeviceService deviceService;
	private SensorDao sensorDao;
	private DeviceDataDao deviceDataDao;
	private ServerConfigService serverConfigService;
	
	public DiagnosticViewController(@Context ServletContext ctx) {
		serverConfigService = (ServerConfigService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("serverConfigService");
		
		sensorConfigDao = (SensorConfigDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("sensorConfigDao");
				
		tagConfigurationService = (TagConfigurationService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("tagConfigurationService");
		tagConfigurationDao = (TagConfigurationDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("tagConfigurationDao");
		
		deviceService = (DeviceService) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("deviceService");
		
		deviceDataDao = (DeviceDataDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("deviceDataDao");
		sensorDao = (SensorDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("sensorDao");
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/webSocketViewDataAll")
	public boolean registerDiagnosticViewAll(
			@QueryParam("registerInfo") String registerInfo ) {
		
		Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (o.getClass() == String.class){
			return false;
		}
		
		UserDetails userDetails = (UserDetails) o;
		String userName = userDetails.getUsername();
		WebClientDeviceDataEndPoint.updateCurrentUserView(userName, -1, -1);
		
		return true;
		
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/resetMaxTimeDiff")
	public boolean resetMaxTimeDiff(
			@QueryParam("deviceAddress") String deviceAddress ) {
		WebClientDeviceDataEndPoint.resetMaxTimeDiff(deviceAddress);
		return true;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/webSocketViewDataByDeviceSensor")
	public boolean registerDiagnosticViewDeviceSensor(
			@QueryParam("deviceId") String deviceId, 
			@QueryParam("sensorId") String sensorId,
			@QueryParam("viewName") String viewName ) {
		
		Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (o.getClass() == String.class){
			return false;
		}
		
		UserDetails userDetails = (UserDetails) o;
		String userName = userDetails.getUsername();
		WebClientDeviceDataEndPoint.updateCurrentUserView(userName, Integer.valueOf(deviceId), Integer.valueOf(sensorId));
		
		if ( FieldUtil.isFieldNonBlank(viewName) && "diagnostic".equals(viewName) ) {
			WebClientDeviceDataEndPoint.setViewAllSnr(true);
		} else {
			WebClientDeviceDataEndPoint.setViewAllSnr(false);
		}
		
		return true;
		
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAllDeviceId")
	public List<String> getAllDeviceId( @QueryParam("dateFrom") String dateFrom, 
										@QueryParam("dateTo") String dateTo )
	{
		Set<String> deviceIds = new TreeSet<String>();
		List<SensorConfig> sensorConfigs =  sensorConfigDao.retrieveSensorByDeviceIdAndSensorId(null,null,true, null);
		
		if (sensorConfigs != null) {
			for (SensorConfig sensorConfig : sensorConfigs) {
				
				Date dateFromDate = new Date( Long.valueOf(dateFrom));
				Date dateToDate = new Date( Long.valueOf(dateTo));			
				if ( dateFromDate.getTime() <= sensorConfig.getInputDate().getTime()
					&& sensorConfig.getInputDate().getTime() <= dateToDate.getTime()) {
					deviceIds.add(String.valueOf(sensorConfig.getDeviceId()));
				}
			}
		}
		return new ArrayList<String>(deviceIds);
	}	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getRecentSensorChartDataAllDeviceId")
	public List<SensorChartData> getRecentSensorChartDataAllDeviceId( @QueryParam("deviceId") String deviceIdParam, 
																	  @QueryParam("dateFrom") String dateFrom, 
																	  @QueryParam("dateTo") String dateTo,
																	  @QueryParam("setSensorConfig") Boolean setSensorConfig )
	{
		List<SensorChartData> sensorChartDatas = new ArrayList<SensorChartData>();
		
		Long deviceId = null;
		if ( deviceIdParam != null && !deviceIdParam.equals("null")) {
			deviceId = Long.valueOf(deviceIdParam);
		}
		
		List<SensorConfig> sensorConfigs =  sensorConfigDao.retrieveSensorByDeviceIdAndSensorId(deviceId,null,true, null);
		if (sensorConfigs != null) {
			for (SensorConfig sensorConfig : sensorConfigs) {
				Date dateFromDate = new Date( Long.valueOf(dateFrom));
				Date dateToDate = new Date( Long.valueOf(dateTo));		
				
				if ( dateFromDate.getTime() <= sensorConfig.getInputDate().getTime()
					&& sensorConfig.getInputDate().getTime() <= dateToDate.getTime()) {
					
					String fromDateLBoundStr = String.valueOf(sensorConfig.getInputDate().getTime() - 10000);
					String toDateHBoundStr = String.valueOf(sensorConfig.getInputDate().getTime() + 10000);
					 
					List<Sensor> sensorResult = sensorDao.searchByCriteria(true, String.valueOf(sensorConfig.getDeviceId()), String.valueOf(sensorConfig.getSensorId()), fromDateLBoundStr, toDateHBoundStr, 1 );
					List<DeviceData> deviceDataResult = deviceDataDao.searchByCriteria(null, null, null, null, null, null, String.valueOf(sensorConfig.getDeviceId()), fromDateLBoundStr, toDateHBoundStr, true, 1);
				
					if ( sensorResult != null && deviceDataResult != null  
						&& sensorResult.size() > 0 
						&& sensorResult.size() == deviceDataResult.size()) {
					
						Sensor recentSensorData = sensorResult.get(0);
						DeviceData recentDeviceData = deviceDataResult.get(0);
						
						if( recentDeviceData.getId() != recentSensorData.getId() ) {
							continue;
						}
						
						SensorChartData recentChartData = new SensorChartData(); 
						recentChartData.setDate(recentSensorData.getInputDate());
						
						DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						String dateString = df.format(recentSensorData.getInputDate());
						recentChartData.setDateString(dateString);
						
						df = new SimpleDateFormat("HH:mm:ss");
						dateString = df.format(recentSensorData.getInputDate());
						
						recentChartData.setDateLong(recentSensorData.getInputDate().getTime());
						recentChartData.setDateString2(dateString);	
						
						recentChartData.setIsH2s(recentSensorData.getIsH2s());
						if (recentSensorData.getIsH2s())
							recentChartData.setH2s(recentSensorData.getH2s());
						
						recentChartData.setIsSo2(recentSensorData.getIsSo2());
						if (recentSensorData.getIsSo2())
							recentChartData.setSo2(recentSensorData.getSo2());
					
						recentChartData.setIsCh4(recentSensorData.getIsCh4());
						if (recentSensorData.getIsCh4())
							recentChartData.setCh4(Double.valueOf(recentSensorData.getCh4()));
					
						recentChartData.setIsCo2(recentSensorData.getIsCo2());
						if (recentSensorData.getIsCo2())
							recentChartData.setCo2(recentSensorData.getCo2());
						
						recentChartData.setIsO2(recentSensorData.getIsO2());
						if (recentSensorData.getIsO2())
							recentChartData.setO2(recentSensorData.getO2());
						
						recentChartData.setIsWaterLevel(recentSensorData.getIsWaterLevel());
						if (recentSensorData.getIsWaterLevel()) {
							int waterLevel = recentSensorData.getWaterLevel();
							recentChartData.setWaterLevel(waterLevel);
							recentChartData.setWaterLevelFromZero2(-1 * waterLevel);
						} 
						
						recentChartData.setIsCoverLevel(recentSensorData.getIsCoverLevel());
						if (recentSensorData.getIsCoverLevel())
							recentChartData.setCoverLevel(recentSensorData.getCoverLevel());
						
						recentChartData.setIsNh3(recentSensorData.getIsNh3());
						if (recentSensorData.getIsNh3())
							recentChartData.setNh3(recentSensorData.getNh3());
						
						recentChartData.setRssi(recentSensorData.getRssi());
						recentChartData.setAntennaOptimized(recentSensorData.getAntennaOptimized());
						recentChartData.setWaterLevel1Detect(recentSensorData.getWaterLevel1Detect());
						recentChartData.setWaterLevel2Detect(recentSensorData.getWaterLevel2Detect());
						recentChartData.setCoverDetect(recentSensorData.getCoverDetect());	
						
						recentChartData.setAntennaOptimizedStr(recentSensorData.getAntennaOptimizedStr());
						recentChartData.setWaterLevel2DetectStr(recentSensorData.getWaterLevel2DetectStr());
						recentChartData.setWaterLevel1DetectStr(recentSensorData.getWaterLevel1DetectStr());
						recentChartData.setCoverDetectStr(recentSensorData.getCoverDetectStr());
						recentChartData.setBatteryStr(recentSensorData.getBattery());
						recentChartData.setBattery(recentSensorData.getIsLowBattery() ? 0 : 1);
						recentChartData.setReaderId(recentSensorData.getReaderId());
						recentChartData.setMaxStep(recentSensorData.getMaxStep());
						recentChartData.setAdcValue(recentSensorData.getAdcValue());
						recentChartData.setDeviceId(recentDeviceData.getTagId());
						recentChartData.setSensorId(recentSensorData.getSensorId());
						
						recentChartData.setPacketCounter(recentDeviceData.getPacketCounter());
						recentChartData.setMeasurePeriod(recentDeviceData.getMeasurePeriod());
						recentChartData.setReaderIds(recentDeviceData.getReaderIds());
						recentChartData.setRssiStr(recentDeviceData.getRssiStr());
						recentChartData.setReaderRssi(recentDeviceData.getReaderRssi());
						recentChartData.setReaderNameRssi(recentDeviceData.getReaderNameRssi());
						recentChartData.setReaderRssiSnr(recentDeviceData.getReaderRssiSnr());
						recentChartData.setReaderNameRssiSnr(recentDeviceData.getReaderNameRssiSnr());
						
						recentChartData.setUserDefine(recentDeviceData.getUserDefine());
						
						if ( setSensorConfig != null && setSensorConfig.booleanValue() == true) {
							SensorConfig[] newSensorConfig = new SensorConfig[] { sensorConfig };
							recentChartData.setSensorConfigs(newSensorConfig);
						}
						sensorChartDatas.add(recentChartData);
					}
				}
//				
//				long milliDiff = new Date().getTime() - startDate.getTime();
//				logger.debug(String.format("Takes %d to complete", milliDiff)) ;
			}
		}
		return sensorChartDatas;
	}
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAllSensorId")
	public List<String> getSensorId(@QueryParam("deviceId") String deviceId ){
		Set<String> sensorIds = new TreeSet<String>();
		List<SensorConfig> sensorConfigs =  sensorConfigDao.retrieveSensorByDeviceIdAndSensorId(Long.valueOf(deviceId),null,true, false);
		if (sensorConfigs != null) {
			for (SensorConfig sensorConfig : sensorConfigs) {
				sensorIds.add(String.valueOf(sensorConfig.getSensorId()));
			}
		}
		
		return new ArrayList<String>(sensorIds);
	}	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAllSensor")
	public List<SensorConfig> getAllSensor(@QueryParam("deviceId") String deviceId ){
		List<SensorConfig> sensorConfigs =  sensorConfigDao.retrieveSensorByDeviceIdAndSensorId(Long.valueOf(deviceId),null,null,null);
		return sensorConfigs;
	}	
	
	
	@POST
	@Path("/sendConfigToTag")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Validation sendConfigToTag(
			@FormDataParam("command") Integer command,
			@FormDataParam("length") Integer length,
			
			@FormDataParam("deviceAddress") String deviceAddress,
			@FormDataParam("readerId") String readerId,
			
			@FormDataParam("measurePeriod") String measurePeriod )
	{
		
		Validation validation;
		
		if ( deviceService.getSocketServerClass().equals("NioSocketServer"))
			validation = NioSocketServer.sendEndPointConfigStatic(command, length, deviceAddress, readerId, measurePeriod);
		else { 
			validation = new Validation();
			validation.setValidationSubject("Send Config");
			validation.setValidationMessages("Send config success");
		}
		
		if ( validation.getIsValid()) {
			List<TagConfiguration> tagConfigs = tagConfigurationService.searchByCriteria(null, null, deviceAddress, null, null, null, null, null, null);
			if ( tagConfigs != null && tagConfigs.size() > 0) {
				TagConfiguration tagToUpdate = tagConfigs.get(0);
				tagToUpdate.setMeasurePeriod(Integer.valueOf(measurePeriod));
				tagConfigurationDao.update(tagToUpdate);
			}
		}
		return validation;
	}
}
