package com.xmlasia.gemsx.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.spi.resource.Singleton;
import com.xmlasia.gemsx.dao.GroupDao;
import com.xmlasia.gemsx.dao.GroupMenuMappingDao;
import com.xmlasia.gemsx.dao.MenuDao;
import com.xmlasia.gemsx.dao.ModuleDao;
import com.xmlasia.gemsx.domain.Group;
import com.xmlasia.gemsx.domain.GroupMenuMapping;
import com.xmlasia.gemsx.domain.Menu;
import com.xmlasia.gemsx.domain.Module;

@Path("/group")
@Singleton
public class GroupController {

	private static Logger logger = Logger.getLogger(GroupController.class);
	private GroupDao groupDao;
	private MenuDao menuDao;
	private GroupMenuMappingDao groupMenuMappingDao;
	private ModuleDao moduleDao; 
	
	public GroupController(@Context ServletContext ctx) {
		groupDao = (GroupDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("groupDao");
		menuDao = (MenuDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("menuDao");
		groupMenuMappingDao = (GroupMenuMappingDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("groupMenuMappingDao");
		moduleDao = (ModuleDao) WebApplicationContextUtils.getWebApplicationContext(ctx).getBean("moduleDao");
		
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client.create(clientConfig);

		logger.debug("web serivce initialize");
	}
	
	
	@POST
	@Path("/addData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Group addGroup(@FormDataParam("name") String name,
						  @FormDataParam("description") String description,
						  @FormDataParam("active") String active,
						  @FormDataParam("groupMenuMapping") String groupMenuMappingsStr ) {				

		List<Group> existedGroup = groupDao.searchByCriteria(null, name, null, null);
		if (existedGroup != null && existedGroup.size() > 0) {
			Group failedToCreate = new Group();
			failedToCreate.setValidationSubject("Create New Group");
			failedToCreate.setFailedValidationMessage(String.format("Group with name: %s already exist", name ));
			return failedToCreate;
		}
		
		List<GroupMenuMapping> groupMenuMappings = new ArrayList<GroupMenuMapping>();
		parseGroupMenuMappingsJson(groupMenuMappingsStr, groupMenuMappings);
		
		Group group = new Group();
		group.setDescription(description);
		group.setActive(Boolean.valueOf(active));
		group.setName(name);
		
		Group createdGroup = (Group)groupDao.create(group);
		
		List<GroupMenuMapping> savedGroupMenuMappings = new ArrayList<GroupMenuMapping>();
		for (GroupMenuMapping groupMenuMapping : groupMenuMappings) {
			groupMenuMapping.setGroupId(createdGroup.getId());
			GroupMenuMapping newGrpMapping = (GroupMenuMapping)groupMenuMappingDao.create(groupMenuMapping);
			savedGroupMenuMappings.add(newGrpMapping);
		}

		if ( groupMenuMappings.size() > 0) {
			group.setGroupMenuMapping(savedGroupMenuMappings.toArray(new GroupMenuMapping[savedGroupMenuMappings.size()]));
		}
		
		createdGroup.setValidationSubject("Create New Group");
		createdGroup.setValidationMessages("Create group success");
		return createdGroup;
	}


	@POST
	@Path("/updateData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Group updateGroup(@FormDataParam("id") String id, 
						  @FormDataParam("name") String name,
						  @FormDataParam("description") String description,
						  @FormDataParam("active") String active,
						  @FormDataParam("groupMenuMapping") String groupMenuMappingsStr ) {				
		
		List<Group> existedGroup = groupDao.searchByCriteria(id, null, null, null);
		if (existedGroup != null && existedGroup.size() > 0) {
		
			
			List<GroupMenuMapping> groupMenuMappingsToDelete = groupMenuMappingDao.searchByCriteria(null, Long.valueOf(id), null, null, null);
			if ( groupMenuMappingsToDelete != null) {
				for ( GroupMenuMapping GroupMenuMappingToDelete : groupMenuMappingsToDelete) {
					groupMenuMappingDao.delete(GroupMenuMapping.class, GroupMenuMappingToDelete.getId());
				}
			}
			
			List<GroupMenuMapping> groupMenuMappings = new ArrayList<GroupMenuMapping>();
			parseGroupMenuMappingsJson(groupMenuMappingsStr, groupMenuMappings);
			

			List<GroupMenuMapping> updateGroupMenuMappings = new ArrayList<GroupMenuMapping>();
			
			for (GroupMenuMapping groupMenuMapping : groupMenuMappings) {
				groupMenuMapping.setId(0);
				groupMenuMapping.setGroupId(Long.valueOf(id));
				updateGroupMenuMappings.add( (GroupMenuMapping) groupMenuMappingDao.create(groupMenuMapping));
			}
			
			Group toUpdateGroup = existedGroup.get(0);
			toUpdateGroup.setDescription(description);
			toUpdateGroup.setActive(Boolean.valueOf(active));
			
			Group savedGroup = (Group)groupDao.update(toUpdateGroup);
			if ( updateGroupMenuMappings.size() > 0) {
				savedGroup.setGroupMenuMapping(updateGroupMenuMappings.toArray(new GroupMenuMapping[updateGroupMenuMappings.size()]));
			}
			
			savedGroup.setValidationSubject("Save Group");
			savedGroup.setValidationMessages("Save group success");
			return savedGroup;
		
		}
		return null;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchData")
	public List<Group> searchData(@QueryParam("id") String id, 
			@QueryParam("name") String name,
			@QueryParam("active") Boolean active ) {

		List<Group> groups = groupDao.searchByCriteria(id, name, null, active);
		for (Group group : groups) {
			List<GroupMenuMapping> groupMenuMappings = groupMenuMappingDao.searchByCriteria(null, group.getId(), null, null, null);
			group.setGroupMenuMapping(groupMenuMappings.toArray(new GroupMenuMapping[groupMenuMappings.size()]));
		}
		
		return groups;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getUnAssignFunctionByGroupId")
	public List<GroupMenuMapping> getUnAssignFunction(@QueryParam("groupId") String groupId ) {
		
		List<GroupMenuMapping> groupMenuMapping = new ArrayList<GroupMenuMapping>();
		List<Menu> menus = menuDao.searchByCriteria(null, null, null, null, null, null, null, false);
		for (Menu menu : menus) {
			
			List<GroupMenuMapping> queriedGroupMenuMapping = new ArrayList<GroupMenuMapping>();
			Module module = null;
			Long moduleId = new Long(0);
			String moduleName = "";
			
			if ( menu.getSideMenu()) {
				List<Module> modules = moduleDao.searchByCriteria(String.valueOf(menu.getModuleId()), null, null, null, null, null, null, false);
				if ( modules == null || modules.size() == 0) 
					continue;
				
				module = modules.get(0);
				queriedGroupMenuMapping = groupMenuMappingDao.searchByCriteria(null, Long.valueOf(groupId), module.getId(), menu.getId(), null);
				moduleId = module.getId();
				moduleName = module.getName();
			} else {
				queriedGroupMenuMapping = groupMenuMappingDao.searchByCriteria(null, Long.valueOf(groupId), null, menu.getId(), null);
			}
			
			if ( queriedGroupMenuMapping == null || queriedGroupMenuMapping.size() == 0) {
				GroupMenuMapping unassignGroupMenuMapping = new GroupMenuMapping();
				unassignGroupMenuMapping.setGroupId(0);
				
				unassignGroupMenuMapping.setModuleId( moduleId );
				unassignGroupMenuMapping.setModuleName( moduleName );
				unassignGroupMenuMapping.setMenuId(menu.getId());
				unassignGroupMenuMapping.setMenuName(menu.getName());
				unassignGroupMenuMapping.setPermissionId(3);
				groupMenuMapping.add(unassignGroupMenuMapping);
			}
			
		}
		return groupMenuMapping;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getAssignFunctionByGroupId")
	public List<GroupMenuMapping> getAssignFunctionByGroupId(@QueryParam("groupId") String groupId ) {
		
		List<GroupMenuMapping> groupMenuMapping = new ArrayList<GroupMenuMapping>();

		List<Menu> menus = menuDao.searchByCriteria(null, null, null, null, null, null, null, null);
		for (Menu menu : menus) {
	
			List<GroupMenuMapping> queriedGroupMenuMapping = new ArrayList<GroupMenuMapping>();
			Module module = null;
			
			if ( menu.getSideMenu()) {
				List<Module> modules = moduleDao.searchByCriteria(String.valueOf(menu.getModuleId()), null, null, null, null, null, null, false);
				if ( modules == null || modules.size() == 0) 
					continue;
				
				module = modules.get(0);
				queriedGroupMenuMapping = groupMenuMappingDao.searchByCriteria(null, Long.valueOf(groupId), module.getId(), menu.getId(), null);
			} else {
				queriedGroupMenuMapping = groupMenuMappingDao.searchByCriteria(null, Long.valueOf(groupId), new Long(0), menu.getId(), null);
			}

			if ( queriedGroupMenuMapping != null && queriedGroupMenuMapping.size() > 0) {
				groupMenuMapping.addAll(queriedGroupMenuMapping);
			}
		}
		return groupMenuMapping;
	}
	

	private void parseGroupMenuMappingsJson(String groupMenuMappingsStr, List<GroupMenuMapping> groupMenuMappings) {
		try {
			JSONObject jsnobject = new JSONObject(groupMenuMappingsStr);
			JSONArray jsonArray = jsnobject.getJSONArray("groupMenuMappings");
		    for (int i = 0; i < jsonArray.length(); i++) {
		    	GroupMenuMapping groupMenuMapping = new GroupMenuMapping();
		    	
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
		        
		    	String groupIdStr = (String)jsonObject.get("groupId");
		        Long groupId = Long.valueOf(groupIdStr);
		    	groupMenuMapping.setGroupId(groupId);
		    
		    	String menuIdStr = (String)jsonObject.get("menuId");
		        Long menuId = Long.valueOf(menuIdStr);
		        groupMenuMapping.setMenuId(menuId);
		        
		        List<Menu> menu = menuDao.searchByCriteria(menuIdStr, null, null, null, null, null, null, null);
		        if (menu != null && menu.size() > 0 ) {
		        	groupMenuMapping.setMenuName(menu.get(0).getName());
		        }
		        
		    	String moduleIdStr = (String)jsonObject.get("moduleId");
		    	Long moduleId = Long.valueOf(moduleIdStr);
		        groupMenuMapping.setModuleId(moduleId);
		        
		        List<Module> module = moduleDao.searchByCriteria(moduleIdStr, null, null, null, null, null, null, null);
		        if (module != null && module.size() > 0 ) {
		        	groupMenuMapping.setModuleName(module.get(0).getName());
		        }
		        
		        String permissionStr = (String)jsonObject.get("permission");
		        groupMenuMapping.setPermission(permissionStr);
		        
		        long permissionId = 3;
		        if ( permissionStr.equalsIgnoreCase("All")) {
		        	permissionId = 1;
		        }
		        if ( permissionStr.equalsIgnoreCase("Modify")) {
		        	permissionId = 2;
		        }		        
		        if ( permissionStr.equalsIgnoreCase("Read only")) {
		        	permissionId = 3;
		        }		        
		        groupMenuMapping.setPermissionId(permissionId);
		        
		        groupMenuMappings.add(groupMenuMapping);
		    }
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
