package com.xmlasia.gemsx.service;

import java.io.InputStream;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.SiteConfiguration;

public interface SiteConfigurationService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<SiteConfiguration> getAllSiteConfiguration();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<SiteConfiguration> searchByCriteria(String id, String name, String description, Boolean defaultSite);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	SiteConfiguration createSiteConfiguration(SiteConfiguration a, InputStream fileData);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	SiteConfiguration retrieveBySiteConfigurationId(long id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	SiteConfiguration updateSiteConfiguration(SiteConfiguration loadedSiteConfig, String fileName, InputStream fileData);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	SiteConfiguration deleteSiteConfiguration(long id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	SiteConfiguration updateSiteConfiguration(SiteConfiguration loadedSiteConfig);
	
}
