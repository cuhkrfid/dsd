package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Opex;

public interface OpexService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Opex> getAllOpex();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Opex> searchByCriteria(String id, String mainCategoryCode, String mainCategoryName, String categoryCode, String categoryName, 
			String subCategoryCode, String subCategoryName, String remark);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Opex createOpex(Opex a);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Opex retrieveByOpexId(int id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Opex updateOpex(Opex a);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Opex deleteOpex(int id);
	
}
