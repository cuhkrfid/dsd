package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Application;

public interface ApplicationService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Application> getAllApplication();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Application> searchByCriteria(String id, String name, String description,
			Integer displayOrder, Boolean active, String path, Boolean useOrder);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Application createApplication(Application a);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Application retrieveByApplicationId(long id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Application updateApplication(Application fullyLoadedApp);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Application deleteApplication(long id);
}
