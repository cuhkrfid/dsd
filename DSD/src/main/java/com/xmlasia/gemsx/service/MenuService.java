package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Module;
import com.xmlasia.gemsx.domain.Menu;

public interface MenuService {
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Menu> getAllMenus();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Menu> searchByCriteria(String id, String name, String module, String description,
		Integer displayOrder, Boolean active, String path, Boolean useOrder);
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Menu retrieveByMenuId(long id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Menu updateMenu(Menu fullyLoadedMenu, Module module);


	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	List<Menu> retrieveMenuByModuleId(Long moduleId);	
}
