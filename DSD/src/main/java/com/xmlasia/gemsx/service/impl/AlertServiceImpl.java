package com.xmlasia.gemsx.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.xmlasia.gemsx.dao.AlertDao;
import com.xmlasia.gemsx.dao.SensorConfigDao;
import com.xmlasia.gemsx.dao.TagConfigurationDao;
import com.xmlasia.gemsx.domain.Alert;
import com.xmlasia.gemsx.domain.AlertSetting;
import com.xmlasia.gemsx.domain.SensorConfig;
import com.xmlasia.gemsx.domain.TagConfiguration;
import com.xmlasia.gemsx.service.AlertService;
import com.xmlasia.gemsx.service.AlertSettingService;

public class AlertServiceImpl implements AlertService{

	private AlertDao alertDao;
	private AlertSettingService alertSettingService; 
	private TagConfigurationDao tagConfigurationDao; 
	private SensorConfigDao sensorConfigDao; 
	
	@Override
	public Alert createAlert(Alert a) {
		// TODO Auto-generated method stub
		return getAlertDao().createAlert(a);
	}

	@Override
	public Alert updateAlert(Alert a) {
		// TODO Auto-generated method stub
		return getAlertDao().updateAlert(a);
	}

	@Override
	public void deleteAlert(long id) {
		// TODO Auto-generated method stub
		getAlertDao().deleteAlert(id);
	}

	@Override
	public Alert retrieveAlert(long id) {
		// TODO Auto-generated method stub
		Alert alert = getAlertDao().retrieveAlert(id);
		getAlertSetting(alert);
		return alert;
	}

	private void getAlertSetting(Alert alert) {
		AlertSetting alertSetting = alertSettingService.retrieveByAlertSettingId(alert.getAlertSettingId());
		alert.setAlertSetting(alertSetting);
		
		TagConfiguration tagConfiguration = tagConfigurationDao.retrieveTagConfiguration(alert.getTagConfigurationId());
		alert.setTagConfiguration(tagConfiguration);
	
		SensorConfig sensorConfig = sensorConfigDao.retrieveSensorConfig(alert.getSensorConfigId());
		alert.setSensorConfig(sensorConfig);
	}

	@Override
	public List<Alert> searchByCriteria(String id, String fromDate, String toDate, String level, String alertSettingId, String tagConfigurationId, String siteConfigurationId, String descending) {
		// TODO Auto-generated method stub
		List<Alert> resultAlertsSiteConfigurationId = new ArrayList<Alert>();
		
		List<Alert> alerts = alertDao.searchByCriteria(id, fromDate, toDate, level, alertSettingId, tagConfigurationId, descending);
		if (alerts != null) {
			for (Alert alert : alerts) {
				getAlertSetting(alert);
				
				if ( siteConfigurationId == null || "null".equals(siteConfigurationId)) {
					resultAlertsSiteConfigurationId.add(alert);
				} else {
					TagConfiguration tagConfiguration = alert.getTagConfiguration();
					if (tagConfiguration != null && tagConfiguration.getSiteConfigurationId() == Integer.valueOf(siteConfigurationId).longValue()) {
						resultAlertsSiteConfigurationId.add(alert);
					}
				}
			}
		}
		return resultAlertsSiteConfigurationId;
	}

	public AlertDao getAlertDao() {
		return alertDao;
	}

	public void setAlertDao(AlertDao alertDao) {
		this.alertDao = alertDao;
	}

	public AlertSettingService getAlertSettingService() {
		return alertSettingService;
	}

	public void setAlertSettingService(AlertSettingService alertSettingService) {
		this.alertSettingService = alertSettingService;
	}

	public TagConfigurationDao getTagConfigurationDao() {
		return tagConfigurationDao;
	}

	public void setTagConfigurationDao(TagConfigurationDao tagConfigurationDao) {
		this.tagConfigurationDao = tagConfigurationDao;
	}

	public SensorConfigDao getSensorConfigDao() {
		return sensorConfigDao;
	}

	public void setSensorConfigDao(SensorConfigDao sensorConfigDao) {
		this.sensorConfigDao = sensorConfigDao;
	}


}
