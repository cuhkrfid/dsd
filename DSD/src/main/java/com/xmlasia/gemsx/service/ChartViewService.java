package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.SensorChartData;

public interface ChartViewService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Sensor> searchByCriteria(Boolean descending, String devicedId, String sensorId, String dateFrom, String DateTo, Integer limit);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public SensorChartData searchMinMaxByCriteria(Boolean descending, String devicedId, String sensorId, String dateFrom, String DateTo, Integer limit);
	
}
