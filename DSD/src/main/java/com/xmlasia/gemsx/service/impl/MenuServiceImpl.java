package com.xmlasia.gemsx.service.impl;

import java.util.Date;
import java.util.List;

import com.xmlasia.gemsx.dao.MenuDao;
import com.xmlasia.gemsx.domain.Menu;
import com.xmlasia.gemsx.domain.Module;
import com.xmlasia.gemsx.service.MenuService;

public class MenuServiceImpl implements MenuService {

	private MenuDao menuDao; 
	
	@Override
	public List<Menu> getAllMenus() {
		// TODO Auto-generated method stub
		return getMenuDao().listAll();
	}

	@Override
	public List<Menu> searchByCriteria(String id, String name, String module, String description, Integer displayOrder, Boolean active,
			String path, Boolean useOrder) {
		// TODO Auto-generated method stub
		return getMenuDao().searchByCriteria(id, name, description, module, displayOrder, active, path, useOrder);
	}


	@Override
	public Menu retrieveByMenuId(long id) {
		Menu m = getMenuDao().retrieveMenu(id);
		return m;
	}

	@Override
	public Menu updateMenu(Menu fullyLoadedMenu, Module module) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		fullyLoadedMenu.setUpdatedBy("SYSTEM");
		fullyLoadedMenu.setUpdatedDate(currentTime);
		
		Menu updatedMenu = (Menu) menuDao.updateMenu(fullyLoadedMenu);
		return updatedMenu;
	}

	public MenuDao getMenuDao() {
		return menuDao;
	}

	public void setMenuDao(MenuDao menuDao) {
		this.menuDao = menuDao;
	}

	public List<Menu> retrieveMenuByModuleId(Long moduleId) {
		List<Menu> menus = menuDao.retrieveMenuByModuleId(moduleId);
		return menus;
	}
	
}
