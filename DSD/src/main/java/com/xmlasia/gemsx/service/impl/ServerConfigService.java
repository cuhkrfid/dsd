package com.xmlasia.gemsx.service.impl;

public class ServerConfigService {

	private Double minSnr;
	
	public ServerConfigService() {
	}

	public Double getMinSnr() {
		return minSnr;
	}

	public void setMinSnr(double minSnr) {
		this.minSnr = minSnr;
	}
}
