package com.xmlasia.gemsx.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;

import com.xmlasia.gemsx.dao.GroupMenuMappingDao;
import com.xmlasia.gemsx.dao.UserDao;
import com.xmlasia.gemsx.dao.UserGroupMappingDao;
import com.xmlasia.gemsx.dao.UserRoleMappingDao;
import com.xmlasia.gemsx.domain.Group;
import com.xmlasia.gemsx.domain.GroupMenuMapping;
import com.xmlasia.gemsx.domain.Menu;
import com.xmlasia.gemsx.domain.Module;
import com.xmlasia.gemsx.domain.Role;
import com.xmlasia.gemsx.domain.User;
import com.xmlasia.gemsx.domain.UserGroupMapping;
import com.xmlasia.gemsx.domain.UserGroupMappingPK;
import com.xmlasia.gemsx.domain.UserRoleMapping;
import com.xmlasia.gemsx.domain.UserRoleMappingPK;
import com.xmlasia.gemsx.service.UserService;

public class UserServiceImpl implements UserService{

	private UserDao userDao;
	private UserRoleMappingDao userRoleMappingDao;
	private UserGroupMappingDao userGroupMappingDao;
	private GroupMenuMappingDao groupMenuMappingDao; 
	
	public void createUserByUserName(String userName) {
		
		User user = new User();
		user.setName(userName);
		user.setUsername(userName);
		user.setEmail(userName);
		user.setPassword(userName);
		user.setPhone(userName);
		user.setFax(userName);
		user.setTitle(userName);
		user.setActive(true);
		
		this.createUser(user);

	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<User> getAllUser() {
		return userDao.listAll();
	}

	@Override
	public List<User> searchByCriteria(String id, 
		    String chiName, 
		    String engName, 
		    String userName, 
		    String email, 
		    String dept, 
		    String password, 
		    String phone, 
		    Boolean notification, 
		    String fax, 
		    String title,
		    Boolean pwdResetAfterLogin,
		    Boolean active) {
		return userDao.searchByCriteria(id, chiName, engName, userName, email, dept, password, phone, notification, fax, title, pwdResetAfterLogin, active);
	}

	@Override
	public User createUser(User u) {
		Date currentTime = new Date();

		u.setCreatedBy("SYSTEM");
		u.setCreatedDate(currentTime);
		u.setUpdatedBy("SYSTEM");
		u.setUpdatedDate(currentTime);
		u.setVersion(0);
		
		User newUser = userDao.createUser(u);
		return retrieveByUserId(newUser.getId());
	}

	@Override
	public User retrieveByUserId(int id) {
		User u = userDao.retrieveUser(id);
		return u;
	}

	@Override
	public User updateUser(User fullyLoadedUser) {
		Date currentTime = new Date();

		fullyLoadedUser.setUpdatedBy("SYSTEM");
		fullyLoadedUser.setUpdatedDate(currentTime);
	
		User updatedUser = (User) userDao.update(fullyLoadedUser);
		return updatedUser;
	}

	@Override
	public void updateUserRole(Integer id, List<Integer> roleIdList) {
		List<Role> currRoles = userRoleMappingDao.findAssignedRoleById(id);
		List<UserRoleMappingPK> toDelete = new ArrayList<UserRoleMappingPK>();
		List<UserRoleMapping> toInsert = new ArrayList<UserRoleMapping>();
		for (Role r : currRoles){
			boolean found = false;
			for (Integer i : roleIdList){
				if (r.getId() == i){
					found = true;
					break;
				}
			}
			if (!found){
				UserRoleMappingPK urmPK = new UserRoleMappingPK(id, r.getId());
				toDelete.add(urmPK);
			}
		}
		for (Integer i : roleIdList){
			boolean found = false;
			for (Role r : currRoles){
				if (r.getId() == i){
					found = true;
					break;
				}
			}
			if (!found){
				UserRoleMapping urm = new UserRoleMapping(id, i);
				toInsert.add(urm);
			}
		}
		for (UserRoleMappingPK urmPK : toDelete){
			userRoleMappingDao.delete(UserRoleMapping.class, urmPK);
		}
		for (UserRoleMapping urm : toInsert){
			userRoleMappingDao.create(urm);
		}
	}

	@Override
	public void updateUserGroup(Integer id, List<Integer> groupIdList) {
		List<Group> currGroups = userGroupMappingDao.findAssignedGroupById(id);
		List<UserGroupMappingPK> toDelete = new ArrayList<UserGroupMappingPK>();
		List<UserGroupMapping> toInsert = new ArrayList<UserGroupMapping>();
		for (Group g : currGroups){
			boolean found = false;
			for (Integer i : groupIdList){
				if (g.getId() == i){
					found = true;
					break;
				}
			}
			if (!found){
				UserGroupMappingPK ugmPK = new UserGroupMappingPK(id, (int)g.getId());
				toDelete.add(ugmPK);
			}
		}
		for (Integer i : groupIdList){
			boolean found = false;
			for (Group g : currGroups){
				if (g.getId() == i){
					found = true;
					break;
				}
			}
			if (!found){
				UserGroupMapping ugm = new UserGroupMapping(id, i);
				toInsert.add(ugm);
			}
		}
		for (UserGroupMappingPK ugmPK : toDelete){
			userGroupMappingDao.delete(UserGroupMapping.class, ugmPK);
		}
		for (UserGroupMapping ugm : toInsert){
			userGroupMappingDao.create(ugm);
		}
	}
	
	@Override
	public User findByLoginId(String userName) {
		User user = userDao.findByLoginId(userName);
		if ( user == null)
			return null;
		
		List<GroupMenuMapping> groupMenuMappings = groupMenuMappingDao.searchByCriteria(null, user.getGroupId(), null, null, null);
		if ( groupMenuMappings != null) {
			Map<String, String> permissionMap = new HashMap<String,String>();
			for ( GroupMenuMapping grpMenuMapping : groupMenuMappings ) {
				if (grpMenuMapping.getModuleId() > 0)
					permissionMap.put(grpMenuMapping.getModuleName() + "_" + grpMenuMapping.getMenuName(), String.valueOf(grpMenuMapping.getPermissionId()) );
				else 
					permissionMap.put(grpMenuMapping.getMenuName(), String.valueOf(grpMenuMapping.getPermissionId()) );
			}
			
			String jsonPermissionString = new JSONObject(permissionMap).toString();
			user.setPermissionMap(jsonPermissionString);
		}
		return user;
	}
	
	@Override
	public User deleteUser(int id) {
		User u = userDao.retrieveUser(id);
		userDao.deleteUser(id);
		return u;
	}

	@Override
	public List<Module> getParentModules(String userName) {
		List<Module> result = userDao.getParentModules(userName);
		return result;
	}
	
	@Override
	public List<Menu> getMenus(User user, Long moduleId, String searchText) {
		List<Menu> result = groupMenuMappingDao.getMenus(user, moduleId, searchText);
		return result;
	}

	@Override
	public List<Role> findAssignedRoleById(int userId) {
		return userRoleMappingDao.findAssignedRoleById(userId);
	}

	@Override
	public List<Role> findUnassignedRoleById(int userId) {
		return userRoleMappingDao.findUnassignedRoleById(userId);
	}

	@Override
	public List<Group> findAssignedGroupById(int userId) {
		return userGroupMappingDao.findAssignedGroupById(userId);
	}

	@Override
	public List<Group> findUnassignedGroupById(int userId) {
		return userGroupMappingDao.findUnassignedGroupById(userId);
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public UserRoleMappingDao getUserRoleMappingDao() {
		return userRoleMappingDao;
	}

	public void setUserRoleMappingDao(UserRoleMappingDao userRoleMappingDao) {
		this.userRoleMappingDao = userRoleMappingDao;
	}

	public UserGroupMappingDao getUserGroupMappingDao() {
		return userGroupMappingDao;
	}

	public void setUserGroupMappingDao(UserGroupMappingDao userGroupMappingDao) {
		this.userGroupMappingDao = userGroupMappingDao;
	}

	public GroupMenuMappingDao getGroupMenuMappingDao() {
		return groupMenuMappingDao;
	}

	public void setGroupMenuMappingDao(GroupMenuMappingDao groupMenuMappingDao) {
		this.groupMenuMappingDao = groupMenuMappingDao;
	}

}
