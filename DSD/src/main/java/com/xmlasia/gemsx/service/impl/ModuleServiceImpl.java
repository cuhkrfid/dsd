package com.xmlasia.gemsx.service.impl;

import java.util.Date;
import java.util.List;

import com.xmlasia.gemsx.dao.MenuDao;
import com.xmlasia.gemsx.dao.ModuleDao;
import com.xmlasia.gemsx.domain.Module;
import com.xmlasia.gemsx.service.ModuleService;

public class ModuleServiceImpl implements ModuleService {

	
	private ModuleDao moduleDao;
	private MenuDao menuDao;
	
	public ModuleDao getModuleDao() {
		return moduleDao;
	}

	public void setModuleDao(ModuleDao moduleDao) {
		this.moduleDao = moduleDao;
	}

	@Override
	public List<Module> getAllModules() {
		// TODO Auto-generated method stub
		return moduleDao.listAll();
	}

	@Override
	public List<Module> searchByCriteria(String id, String name, String description, Integer displayOrder,
			Boolean homepage, Boolean active, String path, Boolean useOrder) {
		// TODO Auto-generated method stub
		return moduleDao.searchByCriteria(id, name, description, displayOrder, homepage, active, path, useOrder);
	}

	@Override
	public Module createModule(Module m) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		m.setCreatedBy("SYSTEM");
		m.setCreatedDate(currentTime);
		m.setUpdatedBy("SYSTEM");
		m.setUpdatedDate(currentTime);
		m.setVersion(0);
	
		Module newModule = moduleDao.createModule(m);
		return retrieveByModuleId(newModule.getId());
	}

	@Override
	public Module retrieveByModuleId(long id) {
		// TODO Auto-generated method stub
		Module m = moduleDao.retrieveModule(id);
		return m;
	}

	@Override
	public Module updateModule(Module fullyLoadedModule) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();
		fullyLoadedModule.setUpdatedBy("SYSTEM");
		fullyLoadedModule.setUpdatedDate(currentTime);
		
		Module updatedModule = (Module) moduleDao.update(fullyLoadedModule);
		return updatedModule;
	}

	@Override
	public Module deleteModule(long id) {
		// TODO Auto-generated method stub
		Module m = moduleDao.retrieveModule(id);
		moduleDao.deleteModule(id);
		return m;
	}

	public MenuDao getMenuDao() {
		return menuDao;
	}

	public void setMenuDao(MenuDao menuDao) {
		this.menuDao = menuDao;
	}
	
	
}
