package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Alert;

public interface AlertService {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Alert createAlert(Alert a);
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Alert updateAlert(Alert a);
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteAlert(long id);

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Alert retrieveAlert(long id);
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Alert> searchByCriteria(String id, String fromDate, String toDate, String level, String alertSettingId, String tagConfigurationId, String siteConfigurationId, String descending);
}
