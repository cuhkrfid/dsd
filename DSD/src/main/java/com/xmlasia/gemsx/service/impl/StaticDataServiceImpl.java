package com.xmlasia.gemsx.service.impl;

import java.util.List;

import com.xmlasia.gemsx.dao.StaticDataDao;
import com.xmlasia.gemsx.domain.StaticData;
import com.xmlasia.gemsx.service.StaticDataService;

public class StaticDataServiceImpl implements StaticDataService{

	private StaticDataDao staticDataDao;

	@Override
	public List<StaticData> searchByCriteria(String dataName) {
		return staticDataDao.searchByCriteria(dataName);
	}

	public StaticDataDao getStaticDataDao() {
		return staticDataDao;
	}

	public void setStaticDataDao(StaticDataDao staticDataDao) {
		this.staticDataDao = staticDataDao;
	}

}
