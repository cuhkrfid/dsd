package com.xmlasia.gemsx.service.impl;

import java.util.List;

import com.xmlasia.gemsx.dao.AlertSettingDao;
import com.xmlasia.gemsx.dao.AlertSettingSensorConfigDao;
import com.xmlasia.gemsx.domain.AlertSetting;
import com.xmlasia.gemsx.domain.AlertSettingSensorConfig;
import com.xmlasia.gemsx.domain.TagConfiguration;
import com.xmlasia.gemsx.service.AlertSettingService;
import com.xmlasia.gemsx.service.TagConfigurationService;

public class AlertSettingServiceImpl implements  AlertSettingService{

	private AlertSettingDao alertSettingDao;
	private AlertSettingSensorConfigDao alertSettingSensorConfigDao;
	private TagConfigurationService tagConfigurationService; 
	
	@Override
	public List<AlertSetting> getAllAlertSetting() {
		// TODO Auto-generated method stub
		return alertSettingDao.searchByCriteria(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, true);
	}

	@Override
	public List<AlertSetting> searchByCriteria(String id, 
			String name, 
			String level, 
			String useDateRange,
			String fromDate, 
			String toDate, 
			String deviceAddress, 
			String deviceId, 
			String groupId, 
			String isCoverLevel,
			String isWaterLevel, 
			String isCo2, 
			String isH2s, 
			String isSo2, 
			String isCh4, 
			String isNh3, 
			String isO2,
			Long alertCriteriaId, 
			Long tagConfigurationId,
			boolean determineCriteriaDescription) {
		// TODO Auto-generated method stub
		List<AlertSetting> alertSettings =  alertSettingDao.searchByCriteria(id, name, level, useDateRange, fromDate, toDate, deviceAddress, deviceId, groupId, isCoverLevel, 
				isWaterLevel, isCo2, isH2s, isSo2, isCh4, isNh3, isO2, null, tagConfigurationId, true);
		
		if ( alertSettings != null) {
			for (AlertSetting as : alertSettings) {
				retrieveByAlertSettingId(as.getId());
			}
		}
		return alertSettings;
	}

	@Override
	public AlertSetting createAlertSetting(AlertSetting a) {
		// TODO Auto-generated method stub
		AlertSetting newCreatedAlertSetting = alertSettingDao.createAlertSetting(a);
		
		for ( AlertSettingSensorConfig alertSettingSensorConfig : a.getAlertSettingSensorConfigs() ) {
			alertSettingSensorConfig.setAlertSettingId(newCreatedAlertSetting.getId());
			alertSettingSensorConfigDao.createAlertSettingSensorConfig(alertSettingSensorConfig);
		}
		
		return retrieveByAlertSettingId(newCreatedAlertSetting.getId());
	}

	@Override
	public AlertSetting retrieveByAlertSettingId(long id) {
		// TODO Auto-generated method stub
		AlertSetting as = alertSettingDao.retrieveAlertSetting(id);
		if ( as != null) {
			loadSensorConfigToAlertSetting(as);
		}
		return as;
	}

	private void loadSensorConfigToAlertSetting(AlertSetting as) {
		List<AlertSettingSensorConfig> asList = alertSettingSensorConfigDao.searchByCriteria(null, as.getId(), null);
		if ( asList != null && asList.size() > 0 ) {
			as.setAlertSettingSensorConfigs((asList.toArray(new AlertSettingSensorConfig[asList.size()])));
			for (AlertSettingSensorConfig assc : asList) {
//				assc.setAlertSetting(as);
//				
				TagConfiguration tagConfiguration = tagConfigurationService.retrieveByTagConfigurationId(assc.getTagConfigurationId());
				assc.setTagConfiguration(tagConfiguration);
			}
		}	
	}
	
	@Override
	public AlertSetting updateAlertSetting(AlertSetting alertSetting) {
		
		AlertSetting updatedAlertSetting =  (AlertSetting)alertSettingDao.updateAlertSetting(alertSetting);
		deleteAlertSettingSensorConfig(alertSetting.getId());
	
		for ( AlertSettingSensorConfig assc : alertSetting.getAlertSettingSensorConfigs()) {
			assc.setId(0);
			alertSettingSensorConfigDao.createAlertSettingSensorConfig(assc);
		}
		
		return retrieveByAlertSettingId(updatedAlertSetting.getId());
	}

	@Override
	public AlertSetting deleteAlertSetting(long alertSettingId) {
		// TODO Auto-generated method stub
		AlertSetting a = alertSettingDao.retrieveAlertSetting(alertSettingId);
		deleteAlertSettingSensorConfig(alertSettingId);
		alertSettingDao.deleteAlertSetting(alertSettingId);
		return a;
	}

	private void deleteAlertSettingSensorConfig(long alertSettingId) {
		List<AlertSettingSensorConfig> alertSettingSensorConfigs = alertSettingSensorConfigDao.searchByCriteria(null, alertSettingId, null);
		if ( alertSettingSensorConfigs != null) {
			for ( AlertSettingSensorConfig assc : alertSettingSensorConfigs) {
				alertSettingSensorConfigDao.deleteAlertSettingSensorConfig(assc.getId());
			}
		}
	}

	public AlertSettingDao getAlertSettingDao() {
		return alertSettingDao;
	}

	public void setAlertSettingDao(AlertSettingDao alertSettingDao) {
		this.alertSettingDao = alertSettingDao;
	}

	public AlertSettingSensorConfigDao getAlertSettingSensorConfigDao() {
		return alertSettingSensorConfigDao;
	}

	public void setAlertSettingSensorConfigDao(AlertSettingSensorConfigDao alertSettingSensorConfigDao) {
		this.alertSettingSensorConfigDao = alertSettingSensorConfigDao;
	}

	public TagConfigurationService getTagConfigurationService() {
		return tagConfigurationService;
	}

	public void setTagConfigurationService(TagConfigurationService tagConfigurationService) {
		this.tagConfigurationService = tagConfigurationService;
	}

}
