package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.AlertSetting;

public interface AlertSettingService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<AlertSetting> getAllAlertSetting();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<AlertSetting> searchByCriteria(String id, 
			String name,
			String level,
			String useDateRange,
			
			String fromDate, 
			String toDate, 
			
			String deviceAddress, 
			String deviceId, 
			String groupId, 
	
			String isCoverLevel, 
			String isWaterLevel, 
			String isCo2, 
			String isH2s, 
			String isSo2, 
			String isCh4,
			String isNh3,
			String isO2,
			
			Long alertCriteriaId,
			Long tagConfigurationId,
			boolean determineCriteriaDescription);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	AlertSetting createAlertSetting(AlertSetting a);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	AlertSetting retrieveByAlertSettingId(long id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	AlertSetting updateAlertSetting(AlertSetting fullyLoadedAlertSetting);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	AlertSetting deleteAlertSetting(long id);
	
}
