package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.TagConfiguration;

public interface TagConfigurationService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<TagConfiguration> getAllTagConfiguration();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<TagConfiguration> searchByCriteria(String id, String name, String deviceAddress, String deviceId, String sensorId, String groupId, String description, String siteName, Long siteId);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	TagConfiguration createTagConfiguration(TagConfiguration a);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	TagConfiguration retrieveByTagConfigurationId(long id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	TagConfiguration updateTagConfiguration(TagConfiguration fullyLoadedApp, boolean updateSensorConfig);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	TagConfiguration deleteTagConfiguration(long id);
}
