package com.xmlasia.gemsx.service.impl;

import java.util.Date;
import java.util.List;

import com.xmlasia.gemsx.dao.DepartmentDao;
import com.xmlasia.gemsx.domain.Department;
import com.xmlasia.gemsx.service.DepartmentService;

public class DepartmentServiceImpl implements DepartmentService{

	private DepartmentDao departmentDao;

	@Override
	@SuppressWarnings("unchecked")
	public List<Department> getAllDepartment() {
		return departmentDao.listAll();
	}

	@Override
	public List<Department> searchByCriteria(String id, String chiName,
			String engName) {
		return departmentDao.searchByCriteria(id, chiName, engName);
	}

	@Override
	public Department createDepartment(Department d) {
		Date currentTime = new Date();

		d.setCreatedBy("SYSTEM");
		d.setCreatedDate(currentTime);
		d.setUpdatedBy("SYSTEM");
		d.setUpdatedDate(currentTime);
		d.setVersion(0);
		Department newDepartment = departmentDao.createDepartment(d);
		return retrieveByDepartmentId(newDepartment.getId());
	}

	@Override
	public Department retrieveByDepartmentId(int id) {
		Department d = departmentDao.retrieveDepartment(id);
		return d;
	}

	@Override
	public Department updateDepartment(Department d) {
		Date currentTime = new Date();

		d.setUpdatedBy("SYSTEM");
		d.setUpdatedDate(currentTime);
		
		return (Department) departmentDao.update(d);
	}

	@Override
	public Department deleteDepartment(int id) {
		Department d = departmentDao.retrieveDepartment(id);
		departmentDao.deleteDepartment(id);
		return d;
	}

	public DepartmentDao getDepartmentDao() {
		return departmentDao;
	}

	public void setDepartmentDao(DepartmentDao departmentDao) {
		this.departmentDao = departmentDao;
	}

}
