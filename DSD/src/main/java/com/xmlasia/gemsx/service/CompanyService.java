package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Company;

public interface CompanyService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Company> getAllCompany();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Company> searchByCriteria(String id, String chiName, String engName, String address, String telNo, String faxNo);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Company createCompany(Company u);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Company retrieveByCompanyId(int id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Company updateCompany(Company c);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Company deleteCompany(int id);
	
}