package com.xmlasia.gemsx.service.impl;

import java.util.List;

import com.xmlasia.gemsx.dao.SensorDao;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.SensorChartData;
import com.xmlasia.gemsx.service.ChartViewService;

public class ChartViewServiceImpl implements ChartViewService {

	private SensorDao sensorDao;
	
	@Override
	public List<Sensor> searchByCriteria(Boolean descending, String devicedId, String sensorId, String dateFrom, String DateTo, Integer limit) {
		// TODO Auto-generated method stub
		return sensorDao.searchByCriteria(descending, devicedId, sensorId, dateFrom, DateTo, limit);
	}

	public SensorDao getSensorDao() {
		return sensorDao;
	}

	public void setSensorDao(SensorDao sensorDao) {
		this.sensorDao = sensorDao;
	}
	
	public SensorChartData searchMinMaxByCriteria(Boolean descending, String devicedId, String sensorId, String dateFrom, String DateTo, Integer limit) {
		return sensorDao.searchMinMaxByCriteria(descending, devicedId, sensorId, dateFrom, DateTo, limit);
	}

}
