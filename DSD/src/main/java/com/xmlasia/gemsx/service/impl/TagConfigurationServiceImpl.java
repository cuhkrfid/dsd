package com.xmlasia.gemsx.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.xmlasia.gemsx.dao.TagConfigurationDao;
import com.xmlasia.gemsx.dao.ValidationMessageDao;
import com.xmlasia.gemsx.dao.SensorConfigDao;
import com.xmlasia.gemsx.dao.SiteConfigurationDao;
import com.xmlasia.gemsx.domain.TagConfiguration;
import com.xmlasia.gemsx.domain.ValidationMessage;
import com.xmlasia.gemsx.service.TagConfigurationService;
import com.xmlasia.gemsx.domain.SensorConfig;

public class TagConfigurationServiceImpl implements TagConfigurationService {
	private TagConfigurationDao tagConfigurationDao; 
	private SiteConfigurationDao siteConfigurationDao; 
	private SensorConfigDao sensorConfigDao;
	private ValidationMessageDao validationMessageDao; 
	
	@Override
	public List<TagConfiguration> getAllTagConfiguration() {
		// TODO Auto-generated method stub
		return tagConfigurationDao.listAll();
	}

	@Override
	public List<TagConfiguration> searchByCriteria(String id, String name, String deviceAddress, String deviceId, String sensorId, String groupId, String description, String siteName, Long siteId) {
		
		// TODO Auto-generated method stub
		List<TagConfiguration> tagConfigurations = tagConfigurationDao.searchByCriteria(id, name, deviceAddress, deviceId, groupId, description, siteName, siteId);
		for(TagConfiguration tagConfig : tagConfigurations) {
			LoadSensorConfigAndMassageTagConfiguration(tagConfig);
		}
		return tagConfigurations;
	}

	private void LoadSensorConfigAndMassageTagConfiguration(TagConfiguration tagConfig) {
	
		List<SensorConfig> sensorConfigsList = sensorConfigDao.retrieveSensorByTagConfigurationId(tagConfig.getId(), false);
		tagConfig.setSensorConfigs(sensorConfigsList.toArray(new SensorConfig[sensorConfigsList.size()]));
		tagConfig.setSensorConfigsList(sensorConfigsList);
		
		SensorConfig[] sensorConfigs = tagConfig.getSensorConfigs();
		List<String> sensorIds = new ArrayList<String>();
		
		if ( sensorConfigs != null && sensorConfigs.length > 0) {
			for (SensorConfig sensorConfig : sensorConfigs) {
				long sensorIdLong = sensorConfig.getSensorId();
				sensorIds.add(String.valueOf(sensorIdLong));
			}
			tagConfig.setTempSensorId(sensorConfigs[0].getSensorId());
		}
		tagConfig.setSensorIds(sensorIds.toArray(new String[sensorIds.size()]));
		
		tagConfigurationDao.setSensorTypeHtml(tagConfig);
		
		List<ValidationMessage> validationMessageObjects = validationMessageDao.searchByCriteria(tagConfig.getId(), null, null, null, null);
		tagConfig.setValidationMessageObjects(validationMessageObjects);
	}

	@Override
	public TagConfiguration createTagConfiguration(TagConfiguration a) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		a.setCreatedBy("SYSTEM");
		a.setCreatedDate(currentTime);
		a.setUpdatedBy("SYSTEM");
		a.setUpdatedDate(currentTime);
		a.setVersion(0);

		TagConfiguration newTagConfiguration = tagConfigurationDao.createTagConfiguration(a);
		for ( SensorConfig sensorConfig : a.getSensorConfigs() ) {
			sensorConfig.setTagConfigurationId(newTagConfiguration.getId());
			sensorConfigDao.create(sensorConfig);
		}
		return retrieveByTagConfigurationId(newTagConfiguration.getId());
	}

	@Override
	public TagConfiguration retrieveByTagConfigurationId(long id) {
		// TODO Auto-generated method stub
		TagConfiguration tagConfig = tagConfigurationDao.retrieveTagConfiguration(id);
		LoadSensorConfigAndMassageTagConfiguration(tagConfig);
		return tagConfig;
	}

	@Override
	public TagConfiguration updateTagConfiguration(TagConfiguration fullyLoadedTag, boolean updateSensorConfig) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		fullyLoadedTag.setUpdatedBy("SYSTEM");
		fullyLoadedTag.setUpdatedDate(currentTime);
		
		TagConfiguration updatedApp  = (TagConfiguration) tagConfigurationDao.update(fullyLoadedTag);
		
		if ( updateSensorConfig ) {
			List<SensorConfig> sensorConfigsList = sensorConfigDao.retrieveSensorByTagConfigurationId(fullyLoadedTag.getId(), false);
			if (sensorConfigsList != null) {
				for (SensorConfig sensorConfig : sensorConfigsList) {
					sensorConfigDao.deleteSensorConfig(sensorConfig.getId());
				}
			}
			
			for ( SensorConfig sensorConfig : fullyLoadedTag.getSensorConfigs() ) {
				sensorConfig.setTagConfigurationId(fullyLoadedTag.getId());
				sensorConfig.setId(0);
				sensorConfig.setMeasurePeriod(fullyLoadedTag.getMeasurePeriod());
				sensorConfig.setInputDate(new Date());
				sensorConfigDao.create(sensorConfig);
			}
		}
		return retrieveByTagConfigurationId(updatedApp.getId());
	}

	@Override
	public TagConfiguration deleteTagConfiguration(long id) {
		// TODO Auto-generated method stub
		TagConfiguration a = tagConfigurationDao.retrieveTagConfiguration(id);
		tagConfigurationDao.deleteMapFile(a, a.getImageFileName());
		tagConfigurationDao.deleteTagConfiguration(id);
		
		List<SensorConfig> sensorConfigsList = sensorConfigDao.retrieveSensorByTagConfigurationId(a.getId(), false);
		for (SensorConfig sensorToDelete : sensorConfigsList) {
			sensorConfigDao.deleteSensorConfig(sensorToDelete.getId());
		}
		return a;
	}

	
	public TagConfigurationDao getTagConfigurationDao() {
		return tagConfigurationDao;
	}

	public void setTagConfigurationDao(TagConfigurationDao tagConfigurationDao) {
		this.tagConfigurationDao = tagConfigurationDao;
	}

	public SiteConfigurationDao getSiteConfigurationDao() {
		return siteConfigurationDao;
	}

	public void setSiteConfigurationDao(SiteConfigurationDao siteConfigurationDao) {
		this.siteConfigurationDao = siteConfigurationDao;
	}

	public SensorConfigDao getSensorConfigDao() {
		return sensorConfigDao;
	}

	public void setSensorConfigDao(SensorConfigDao sensorConfigDao) {
		this.sensorConfigDao = sensorConfigDao;
	}

	public ValidationMessageDao getValidationMessageDao() {
		return validationMessageDao;
	}

	public void setValidationMessageDao(ValidationMessageDao validationMessageDao) {
		this.validationMessageDao = validationMessageDao;
	}
}
