package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Module;
import com.xmlasia.gemsx.domain.Image;

public interface ModuleService {
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Module> getAllModules();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Module> searchByCriteria(String id, String name, String description,
		Integer displayOrder, Boolean homepage, Boolean active, String path, Boolean useOrder);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Module createModule(Module m);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Module retrieveByModuleId(long id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Module updateModule(Module fullyLoadedModule);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Module deleteModule(long id);
}
