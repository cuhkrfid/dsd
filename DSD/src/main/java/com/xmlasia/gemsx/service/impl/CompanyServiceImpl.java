package com.xmlasia.gemsx.service.impl;

import java.util.Date;
import java.util.List;

import com.xmlasia.gemsx.dao.CompanyDao;
import com.xmlasia.gemsx.domain.Company;
import com.xmlasia.gemsx.service.CompanyService;

public class CompanyServiceImpl implements CompanyService{

	private CompanyDao companyDao;
	
	@Override
	public List<Company> getAllCompany() {
		// TODO Auto-generated method stub
		return getCompanyDao().listAll();
	}

	@Override
	public List<Company> searchByCriteria(String id, String chiName, String engName, String address, String telNo,
			String faxNo) {
		// TODO Auto-generated method stub
		return getCompanyDao().searchByCriteria(id, chiName, engName, address, telNo, faxNo);
	}

	@Override
	public Company createCompany(Company c) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		c.setCreatedBy("SYSTEM");
		c.setCreatedDate(currentTime);
		c.setUpdatedBy("SYSTEM");
		c.setUpdatedDate(currentTime);
		c.setVersion(0);
		Company newCompany = getCompanyDao().createCompany(c);
		return retrieveByCompanyId(newCompany.getId());
	}

	@Override
	public Company retrieveByCompanyId(int id) {
		// TODO Auto-generated method stub
		return getCompanyDao().retrieveCompany(id);
	}

	@Override
	public Company updateCompany(Company c) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();
		c.setUpdatedBy("SYSTEM");
		c.setUpdatedDate(currentTime);
		return (Company) getCompanyDao().update(c);
	}

	@Override
	public Company deleteCompany(int id) {
		// TODO Auto-generated method stub
		Company c = getCompanyDao().retrieveCompany(id);
		getCompanyDao().deleteCompany(id);
		return c;
	}

	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

}
