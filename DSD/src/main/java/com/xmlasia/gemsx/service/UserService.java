package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Group;
import com.xmlasia.gemsx.domain.Menu;
import com.xmlasia.gemsx.domain.Module;
import com.xmlasia.gemsx.domain.Role;
import com.xmlasia.gemsx.domain.User;

public interface UserService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<User> getAllUser();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<User> searchByCriteria(String id, 
							    String chiName, 
							    String engName, 
							    String userName, 
							    String email, 
							    String dept, 
							    String password, 
							    String phone, 
							    Boolean notification, 
							    String fax, 
							    String title,
							    Boolean pwdResetAfterLogin,
							    Boolean active);
	 
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	User createUser(User u);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	User retrieveByUserId(int id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	User updateUser(User fullyLoadedUser);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	User deleteUser(int id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	List<Module> getParentModules(String userName);
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	List<Menu> getMenus(User user, Long moduleId, String searchText);

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	void updateUserRole(Integer id, List<Integer> roleIdList);
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	void updateUserGroup(Integer id, List<Integer> groupIdList);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	User findByLoginId (String userName);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Role> findAssignedRoleById(int userId);
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Role> findUnassignedRoleById(int userId);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Group> findAssignedGroupById(int userId);
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Group> findUnassignedGroupById(int userId);
	
	
}
