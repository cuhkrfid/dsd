package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Department;

public interface DepartmentService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Department> getAllDepartment();
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<Department> searchByCriteria(String id, String chiName, String engName);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Department createDepartment(Department d);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	Department retrieveByDepartmentId(int id);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Department updateDepartment(Department d);
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	Department deleteDepartment(int id);
	
}
