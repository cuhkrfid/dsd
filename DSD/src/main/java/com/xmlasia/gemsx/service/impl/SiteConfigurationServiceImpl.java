package com.xmlasia.gemsx.service.impl;

import java.io.InputStream;

import java.util.Date;
import java.util.List;


import com.xmlasia.gemsx.dao.SiteConfigurationDao;
import com.xmlasia.gemsx.domain.SiteConfiguration;
import com.xmlasia.gemsx.service.SiteConfigurationService;


public class SiteConfigurationServiceImpl implements SiteConfigurationService {
	private SiteConfigurationDao siteConfigurationDao; 
	
	@Override
	public List<SiteConfiguration> getAllSiteConfiguration() {
		// TODO Auto-generated method stub
		return siteConfigurationDao.listAll();
	}

	@Override
	public List<SiteConfiguration> searchByCriteria(String id, String name, String description, Boolean defaultSite) {
		// TODO Auto-generated method stub
		return siteConfigurationDao.searchByCriteria(id, name, description, defaultSite);
	}

	@Override
	public SiteConfiguration createSiteConfiguration(SiteConfiguration newSiteConfig, InputStream fileData) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		newSiteConfig.setCreatedBy("SYSTEM");
		newSiteConfig.setCreatedDate(currentTime);
		newSiteConfig.setUpdatedBy("SYSTEM");
		newSiteConfig.setUpdatedDate(currentTime);
		newSiteConfig.setVersion(0);
		
		
		SiteConfiguration newSiteConfiguration = siteConfigurationDao.createSiteConfiguration(newSiteConfig, fileData);
		return retrieveBySiteConfigurationId(newSiteConfiguration.getId());
	}

	@Override
	public SiteConfiguration retrieveBySiteConfigurationId(long id) {
		// TODO Auto-generated method stub
		SiteConfiguration a = siteConfigurationDao.retrieveSiteConfiguration(id);
		return a;
	}

	@Override
	public SiteConfiguration updateSiteConfiguration(SiteConfiguration loadedSiteConfig, String fileName, InputStream fileData) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		loadedSiteConfig.setUpdatedBy("SYSTEM");
		loadedSiteConfig.setUpdatedDate(currentTime);
		String existMapFileName = loadedSiteConfig.getMapFileName();
		
		if ( fileName != null && !fileName.isEmpty()) {
			if ("<deleted_@>".equals(fileName)) {
				loadedSiteConfig.setMapFileName(null);
				if ( existMapFileName != null)
					siteConfigurationDao.deleteMapFile(loadedSiteConfig, existMapFileName);
					siteConfigurationDao.setSiteConfigMapHeightWidth(loadedSiteConfig, null);
			} else {
				loadedSiteConfig.setMapFileName(fileName);
				if ( existMapFileName != null) 
					siteConfigurationDao.deleteMapFile(loadedSiteConfig, existMapFileName);
				//save new file
				siteConfigurationDao.writeMapFile(fileData, loadedSiteConfig, fileName);	
				siteConfigurationDao.setSiteConfigMapHeightWidth(loadedSiteConfig, fileName);
			}
		}
		
		SiteConfiguration updatedApp = (SiteConfiguration) siteConfigurationDao.update(loadedSiteConfig);
		return updatedApp;
		
	}

	@Override
	public SiteConfiguration updateSiteConfiguration(SiteConfiguration loadedSiteConfig) {
		SiteConfiguration updatedApp = (SiteConfiguration) siteConfigurationDao.update(loadedSiteConfig);
		return updatedApp;	
	}
			
	@Override
	public SiteConfiguration deleteSiteConfiguration(long id) {
		// TODO Auto-generated method stub
		SiteConfiguration loadedSiteConfig = siteConfigurationDao.retrieveSiteConfiguration(id);
		siteConfigurationDao.deleteMapFile(loadedSiteConfig, loadedSiteConfig.getMapFileName());
		siteConfigurationDao.deleteSiteConfiguration(id);
		return loadedSiteConfig;
	}

	public SiteConfigurationDao getSiteConfigurationDao() {
		return siteConfigurationDao;
	}

	public void setSiteConfigurationDao(SiteConfigurationDao siteConfigurationDao) {
		this.siteConfigurationDao = siteConfigurationDao;
	}
}
