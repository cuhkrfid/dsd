package com.xmlasia.gemsx.service.impl;

import java.util.Date;
import java.util.List;

import com.xmlasia.gemsx.dao.ApplicationDao;
import com.xmlasia.gemsx.domain.Application;
import com.xmlasia.gemsx.domain.Image;
import com.xmlasia.gemsx.service.ApplicationService;

public class ApplicationServiceImpl implements ApplicationService {

	private ApplicationDao applicationDao; 
	
	@Override
	public List<Application> getAllApplication() {
		// TODO Auto-generated method stub
		return applicationDao.listAll();
	}

	@Override
	public List<Application> searchByCriteria(String id, String name, String description,
												Integer displayOrder, Boolean active, String path, Boolean useOrder) {
		// TODO Auto-generated method stub
		return applicationDao.searchByCriteria(id, name, description, displayOrder, active, path, useOrder);
	}

	@Override
	public Application createApplication(Application a) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		a.setCreatedBy("SYSTEM");
		a.setCreatedDate(currentTime);
		a.setUpdatedBy("SYSTEM");
		a.setUpdatedDate(currentTime);
		a.setVersion(0);

		Application newApplication = applicationDao.createApplication(a);
		return retrieveByApplicationId(newApplication.getId());
	}

	@Override
	public Application retrieveByApplicationId(long id) {
		// TODO Auto-generated method stub
		Application a = applicationDao.retrieveApplication(id);
		return a;
	}

	@Override
	public Application updateApplication(Application fullyLoadedApp) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		fullyLoadedApp.setUpdatedBy("SYSTEM");
		fullyLoadedApp.setUpdatedDate(currentTime);
		
		Application	updatedApp = (Application) applicationDao.update(fullyLoadedApp);
		return updatedApp;
	}

	@Override
	public Application deleteApplication(long id) {
		// TODO Auto-generated method stub
		Application a = applicationDao.retrieveApplication(id);
		applicationDao.deleteApplication(id);

		return a;
	}

	public ApplicationDao getApplicationDao() {
		return applicationDao;
	}

	public void setApplicationDao(ApplicationDao applicationDao) {
		this.applicationDao = applicationDao;
	}

}
