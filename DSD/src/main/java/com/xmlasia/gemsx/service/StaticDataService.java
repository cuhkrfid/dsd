package com.xmlasia.gemsx.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.StaticData;

public interface StaticDataService {

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	List<StaticData> searchByCriteria(String dataName);
	
}
