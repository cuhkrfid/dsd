package com.xmlasia.gemsx.service.impl;

import java.util.Date;
import java.util.List;

import com.xmlasia.gemsx.dao.OpexDao;
import com.xmlasia.gemsx.domain.Opex;
import com.xmlasia.gemsx.service.OpexService;

public class OpexServiceImpl implements OpexService {

	private OpexDao opexDao;
	
	@Override
	public List<Opex> getAllOpex() {
		// TODO Auto-generated method stub
		return opexDao.listAll();
	}

	@Override
	public List<Opex> searchByCriteria(String id, String mainCategoryCode, String mainCategoryName,
			String categoryCode, String categoryName, String subCategoryCode, String subCategoryName, String remark) {
		// TODO Auto-generated method stub
		return opexDao.searchByCriteria(id, mainCategoryCode, mainCategoryName, categoryCode, categoryName, subCategoryCode, subCategoryName, remark);
	}

	@Override
	public Opex createOpex(Opex o) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		o.setCreatedBy("SYSTEM");
		o.setCreatedDate(currentTime);
		o.setUpdatedBy("SYSTEM");
		o.setUpdatedDate(currentTime);
		o.setVersion(0);
		Opex newOpex = opexDao.createOpex(o);
		return retrieveByOpexId(newOpex.getId());
	}

	@Override
	public Opex retrieveByOpexId(int id) {
		// TODO Auto-generated method stub
		Opex o = opexDao.retrieveOpex(id);
		return o;
	}

	@Override
	public Opex updateOpex(Opex o) {
		// TODO Auto-generated method stub
		Date currentTime = new Date();

		o.setUpdatedBy("SYSTEM");
		o.setUpdatedDate(currentTime);
		
		return (Opex) opexDao.update(o);
	}

	@Override
	public Opex deleteOpex(int id) {
		// TODO Auto-generated method stub
		Opex o = opexDao.retrieveOpex(id);
		opexDao.deleteOpex(id);
		return o;
		
	}

	public OpexDao getOpexDao() {
		return opexDao;
	}

	public void setOpexDao(OpexDao opexDao) {
		this.opexDao = opexDao;
	}

}
