package com.xmlasia.gemsx.domain;

public class NetworkChartElement {

	private int id; 
	private Integer parentId;
	
	//type of device/address of device
	private String Name;
	//last updated time
	private String Title;
	private String Image;
	private int deviceType; 
	
	public NetworkChartElement(int id, String Name, int deviceType, String dateString, String Image) {
		this.id = id;
		this.parentId = parentId;
		this.Name = Name;
		this.Title = dateString;
		this.Image = Image;
		this.setDeviceType(deviceType);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getImage() {
		return Image;
	}
	public void setImage(String image) {
		Image = image;
	}

	public int getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(int deviceType) {
		this.deviceType = deviceType;
	}
	
}
