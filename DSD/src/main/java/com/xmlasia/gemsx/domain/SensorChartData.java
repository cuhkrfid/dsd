package com.xmlasia.gemsx.domain;

import java.util.Date;

public class SensorChartData {

	private Long id; 
	
	private Date date;
	private Long dateLong; 
	
	private String dateString;
	private String dateString2;
	
    private Integer readerId;
    private Integer deviceId; 
    private Integer sensorId;
    
	private Integer waterLevel;
	
	private String timeDiffInMsStr = "N/A";
	
    private String antennaOptimizedStr;
    private String waterLevel2DetectStr;
    private String waterLevel1DetectStr;
    private String coverDetectStr;
    private String batteryStr;
    private Integer measurePeriod;
    
    private Integer antennaOptimized;
    private Integer battery;
    
    private Integer coverDetect;
    private Integer waterLevel1Detect;
    private Integer waterLevel2Detect;
    private Integer coverLevel;
	
    private Integer waterLevelFromZero; 
	private Integer waterLevelFromZero2; 
	
    private Integer waterLevelFromZeroMax;
	private Integer waterLevelFromZeroMin;
	private Integer waterLevelFromTroughMax;
	
	private Integer waterLevelFromZeroDiff; 
    private String timeDiffStr;
    
	private Double maxValue;
	private Double minValue;
	
	private Double h2s;
	private Double minH2s;
	private Double maxH2s;
	private Double h2sDiff;
	
	private Double so2;
	private Double minSo2;
	private Double maxSo2;
	private Double so2Diff;
	
	private Double co2;
	private Double minCo2;
	private Double maxCo2;
	private Double co2Diff;
	
	private Double ch4; 
	private Double minCh4;
	private Double maxCh4;
	private Double ch4Diff;
	
	private Double nh3;
	private Double minNh3;
	private Double maxNh3; 
	private Double nh3Diff;
	
	private Double o2;
	private Double minO2;
    private Double maxO2; 
    private Double o2Diff;
    
	private Double rssi;
	private Double minRssi;
    private Double maxRssi;
	private Double rssiDiff;
    
    private String rssiStr; 
    private String readerRssi;
    private String readerNameRssi;
    private String readerRssiSnr;
    private String readerNameRssiSnr;
    
    
    private Integer packetCounter;
    private Integer maxStep;
    private String userDefine;
    
    private Integer adcValue;
    
    private String readerIds; 
    private SensorConfig sensorConfigs[];
    
    private boolean isCoverLevel;
    private boolean isWaterLevel;
    private boolean isCo2;
	private boolean isH2s;
	private boolean isSo2; 
	private boolean isCh4;
	private boolean isNh3;
	private boolean isO2;
	
    public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Integer getCoverLevel() {
		return coverLevel;
	}
	public void setCoverLevel(Integer coverLevel) {
		this.coverLevel = coverLevel;
	}
	
	public Integer getWaterLevel() {
		return waterLevel;
	}
	public void setWaterLevel(Integer waterLevel) {
		this.waterLevel = waterLevel;
	}
	
	public Double getH2s() {
		return h2s;
	}
	public void setH2s(Double h2s) {
		this.h2s = h2s;
	}
	
	public Double getSo2() {
		return so2;
	}
	public void setSo2(Double so2) {
		this.so2 = so2;
	}
	
	public Double getCo2() {
		return co2;
	}
	public void setCo2(Double co2) {
		this.co2 = co2;
	}
	
	public Double getCh4() {
		return ch4;
	}
	public void setCh4(Double ch4) {
		this.ch4 = ch4;
	}
	
	public Double getNh3() {
		return nh3;
	}
	public void setNh3(Double nh3) {
		this.nh3 = nh3;
	}

	public Double getO2() {
		return o2;
	}
	public void setO2(Double o2) {
		this.o2 = o2;
	}

	public String getDateString() {
		return dateString;
	}
	public void setDateString(String dateString) {
		this.dateString = dateString;
	}
	public String getTimeDiffInMsStr() {
		return timeDiffInMsStr;
	}
	public void setTimeDiffInMsStr(String timeDiffInMsStr) {
		this.timeDiffInMsStr = timeDiffInMsStr;
	}
	public Double getRssi() {
		return rssi;
	}
	public void setRssi(Double rssi) {
		this.rssi = rssi;
	}
	public String getDateString2() {
		return dateString2;
	}
	public void setDateString2(String dateString2) {
		this.dateString2 = dateString2;
	}
	public String getAntennaOptimizedStr() {
		return antennaOptimizedStr;
	}
	public void setAntennaOptimizedStr(String antennaOptimizedStr) {
		this.antennaOptimizedStr = antennaOptimizedStr;
	}
	public String getWaterLevel2DetectStr() {
		return waterLevel2DetectStr;
	}
	public void setWaterLevel2DetectStr(String waterLevel2DetectStr) {
		this.waterLevel2DetectStr = waterLevel2DetectStr;
	}
	public String getWaterLevel1DetectStr() {
		return waterLevel1DetectStr;
	}
	public void setWaterLevel1DetectStr(String waterLevel1DetectStr) {
		this.waterLevel1DetectStr = waterLevel1DetectStr;
	}
	public String getCoverDetectStr() {
		return coverDetectStr;
	}
	public void setCoverDetectStr(String coverDetectStr) {
		this.coverDetectStr = coverDetectStr;
	}
	public String getBatteryStr() {
		return batteryStr;
	}
	public void setBatteryStr(String batteryStr) {
		this.batteryStr = batteryStr;
	}
	public Integer getMaxStep() {
		return maxStep;
	}
	public void setMaxStep(Integer maxStep) {
		this.maxStep = maxStep;
	}
	public Integer getAdcValue() {
		return adcValue;
	}
	public void setAdcValue(Integer adcValue) {
		this.adcValue = adcValue;
	}
	public Integer getPacketCounter() {
		return packetCounter;
	}
	public void setPacketCounter(Integer packetCounter) {
		this.packetCounter = packetCounter;
	}
	public Integer getReaderId() {
		return readerId;
	}
	public void setReaderId(Integer readerId) {
		this.readerId = readerId;
	}
	public Integer getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}
	public Integer getSensorId() {
		return sensorId;
	}
	public void setSensorId(Integer sensorId) {
		this.sensorId = sensorId;
	}
	public Integer getBattery() {
		return battery;
	}
	public void setBattery(Integer battery) {
		this.battery = battery;
	}
	public Integer getAntennaOptimized() {
		return antennaOptimized;
	}
	public void setAntennaOptimized(Integer antennaOptimized) {
		this.antennaOptimized = antennaOptimized;
	}
	public Integer getWaterLevel2Detect() {
		return waterLevel2Detect;
	}
	public void setWaterLevel2Detect(Integer waterLevel2Detect) {
		this.waterLevel2Detect = waterLevel2Detect;
	}
	public Integer getWaterLevel1Detect() {
		return waterLevel1Detect;
	}
	public void setWaterLevel1Detect(Integer waterLevel1Detect) {
		this.waterLevel1Detect = waterLevel1Detect;
	}
	public Integer getCoverDetect() {
		return coverDetect;
	}
	public void setCoverDetect(Integer coverDetect) {
		this.coverDetect = coverDetect;
	}
	public Long getDateLong() {
		return dateLong;
	}
	public void setDateLong(Long dateLong) {
		this.dateLong = dateLong;
	}
	public Integer getMeasurePeriod() {
		return measurePeriod;
	}
	public void setMeasurePeriod(Integer measurePeriod) {
		this.measurePeriod = measurePeriod;
	}
	public String getReaderIds() {
		return readerIds;
	}
	public void setReaderIds(String readerIds) {
		this.readerIds = readerIds;
	}
	public String getRssiStr() {
		return rssiStr;
	}
	public void setRssiStr(String rssiStr) {
		this.rssiStr = rssiStr;
	}
	public String getReaderRssi() {
		return readerRssi;
	}
	public void setReaderRssi(String readerRssi) {
		this.readerRssi = readerRssi;
	}
	public SensorConfig[] getSensorConfigs() {
		return sensorConfigs;
	}
	public void setSensorConfigs(SensorConfig sensorConfigs[]) {
		this.sensorConfigs = sensorConfigs;
	}
	public Integer getWaterLevelFromZero() {
		return waterLevelFromZero;
	}
	public void setWaterLevelFromZero(Integer waterLevelFromZero) {
		this.waterLevelFromZero = waterLevelFromZero;
	}
	
	public boolean getIsCoverLevel() {
		return isCoverLevel;
	}
	public void setIsCoverLevel(boolean isCoverLevel) {
		this.isCoverLevel = isCoverLevel;
	}
	
	public boolean getIsWaterLevel() {
		return isWaterLevel;
	}
	public void setIsWaterLevel(boolean isWaterLevel) {
		this.isWaterLevel = isWaterLevel;
	}
	
	public boolean getIsCo2() {
		return isCo2;
	}
	public void setIsCo2(boolean isCo2) {
		this.isCo2 = isCo2;
	}
	
	public boolean getIsH2s() {
		return isH2s;
	}
	public void setIsH2s(boolean isH2s) {
		this.isH2s = isH2s;
	}
	
	public boolean getIsSo2() {
		return isSo2;
	}
	public void setIsSo2(boolean isSo2) {
		this.isSo2 = isSo2;
	}
	
	public boolean getIsCh4() {
		return isCh4;
	}
	public void setIsCh4(boolean isCh4) {
		this.isCh4 = isCh4;
	}
	
	public boolean getIsNh3() {
		return isNh3;
	}
	public void setIsNh3(boolean isNh3) {
		this.isNh3 = isNh3;
	}
	
	public boolean getIsO2() {
		return isO2;
	}
	public void setIsO2(boolean isO2) {
		this.isO2 = isO2;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getWaterLevelFromZeroMin() {
		return waterLevelFromZeroMin;
	}
	public void setWaterLevelFromZeroMin(Integer waterLevelFromZeroMin) {
		this.waterLevelFromZeroMin = waterLevelFromZeroMin;
	}
	public Integer getWaterLevelFromZeroDiff() {
		return waterLevelFromZeroDiff;
	}
	public void setWaterLevelFromZeroDiff(Integer waterLevelFromZeroDiff) {
		this.waterLevelFromZeroDiff = waterLevelFromZeroDiff;
	}
	public String getTimeDiffStr() {
		return timeDiffStr;
	}
	public void setTimeDiffStr(String timeDiffStr) {
		this.timeDiffStr = timeDiffStr;
	}
	public Double getMaxH2s() {
		return maxH2s;
	}
	public void setMaxH2s(Double maxH2s) {
		this.maxH2s = maxH2s;
	}
	public Double getMaxSo2() {
		return maxSo2;
	}
	public void setMaxSo2(Double maxSo2) {
		this.maxSo2 = maxSo2;
	}
	public Double getMaxCo2() {
		return maxCo2;
	}
	public void setMaxCo2(Double maxCo2) {
		this.maxCo2 = maxCo2;
	}
	public Double getMaxCh4() {
		return maxCh4;
	}
	public void setMaxCh4(Double maxCh4) {
		this.maxCh4 = maxCh4;
	}
	public Double getMaxNh3() {
		return maxNh3;
	}
	public void setMaxNh3(Double maxNh3) {
		this.maxNh3 = maxNh3;
	}
	public Double getMaxO2() {
		return maxO2;
	}
	public void setMaxO2(Double maxO2) {
		this.maxO2 = maxO2;
	}
	public Double getMaxRssi() {
		return maxRssi;
	}
	public void setMaxRssi(Double maxRssi) {
		this.maxRssi = maxRssi;
	}
	public Double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}
	public Double getH2sDiff() {
		return h2sDiff;
	}
	public void setH2sDiff(Double h2sDiff) {
		this.h2sDiff = h2sDiff;
	}
	public Double getSo2Diff() {
		return so2Diff;
	}
	public void setSo2Diff(Double so2Diff) {
		this.so2Diff = so2Diff;
	}
	public Double getCo2Diff() {
		return co2Diff;
	}
	public void setCo2Diff(Double co2Diff) {
		this.co2Diff = co2Diff;
	}
	public Double getCh4Diff() {
		return ch4Diff;
	}
	public void setCh4Diff(Double ch4Diff) {
		this.ch4Diff = ch4Diff;
	}
	public Double getNh3Diff() {
		return nh3Diff;
	}
	public void setNh3Diff(Double nh3Diff) {
		this.nh3Diff = nh3Diff;
	}
	public Double getO2Diff() {
		return o2Diff;
	}
	public void setO2Diff(Double o2Diff) {
		this.o2Diff = o2Diff;
	}
	public Double getRssiDiff() {
		return rssiDiff;
	}
	public void setRssiDiff(Double rssiDiff) {
		this.rssiDiff = rssiDiff;
	}
	public Double getMinValue() {
		return minValue;
	}
	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}
	public Double getMinH2s() {
		return minH2s;
	}
	public void setMinH2s(Double minH2s) {
		this.minH2s = minH2s;
	}
	public Double getMinSo2() {
		return minSo2;
	}
	public void setMinSo2(Double minSo2) {
		this.minSo2 = minSo2;
	}
	public Double getMinCo2() {
		return minCo2;
	}
	public void setMinCo2(Double minCo2) {
		this.minCo2 = minCo2;
	}
	public Double getMinCh4() {
		return minCh4;
	}
	public void setMinCh4(Double minCh4) {
		this.minCh4 = minCh4;
	}
	public Double getMinNh3() {
		return minNh3;
	}
	public void setMinNh3(Double minNh3) {
		this.minNh3 = minNh3;
	}
	public Double getMinO2() {
		return minO2;
	}
	public void setMinO2(Double minO2) {
		this.minO2 = minO2;
	}
	public Double getMinRssi() {
		return minRssi;
	}
	public void setMinRssi(Double minRssi) {
		this.minRssi = minRssi;
	}
	public Integer getWaterLevelFromZeroMax() {
		return waterLevelFromZeroMax;
	}
	public void setWaterLevelFromZeroMax(Integer waterLevelFromZeroMax) {
		this.waterLevelFromZeroMax = waterLevelFromZeroMax;
	}
	public Integer getWaterLevelFromTroughMax() {
		return waterLevelFromTroughMax;
	}
	public void setWaterLevelFromTroughMax(Integer waterLevelFromTroughMax) {
		this.waterLevelFromTroughMax = waterLevelFromTroughMax;
	}
	public Integer getWaterLevelFromZero2() {
		return waterLevelFromZero2;
	}
	public void setWaterLevelFromZero2(Integer waterLevelFromZero2) {
		this.waterLevelFromZero2 = waterLevelFromZero2;
	}
	public String getReaderRssiSnr() {
		return readerRssiSnr;
	}
	public void setReaderRssiSnr(String readerRssiSnr) {
		this.readerRssiSnr = readerRssiSnr;
	}
	public String getUserDefine() {
		return userDefine;
	}
	public void setUserDefine(String userDefine) {
		this.userDefine = userDefine;
	}
	public String getReaderNameRssi() {
		return readerNameRssi;
	}
	public void setReaderNameRssi(String readerNameRssi) {
		this.readerNameRssi = readerNameRssi;
	}
	public String getReaderNameRssiSnr() {
		return readerNameRssiSnr;
	}
	public void setReaderNameRssiSnr(String readerNameRssiSnr) {
		this.readerNameRssiSnr = readerNameRssiSnr;
	}

}
