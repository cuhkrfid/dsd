package com.xmlasia.gemsx.domain;

import javax.persistence.Transient;

public class Validation extends BaseDomain {/**
	 * 
	 */
	private static final long serialVersionUID = 2992442994370328729L;

	@Transient
	private boolean isValid = true;
	
	@Transient
	private String validationMessages = "";

	@Transient
	private String validationSubject = "";
	
	
	public boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public String getValidationMessages() {
		return validationMessages;
	}

	public void setValidationMessages(String validationMessages) {
		this.validationMessages = validationMessages;
	}
	
	public void setFailedValidationMessage(String validationMessages) {
		setIsValid(false);
		setValidationMessages(validationMessages);
	}

	public String getValidationSubject() {
		return validationSubject;
	}

	public void setValidationSubject(String validationSubject) {
		this.validationSubject = validationSubject;
	}

	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return null;
	} 
	
	public void clone(Validation validationObj) {
		this.isValid = validationObj.getIsValid();
		this.validationSubject = validationObj.getValidationSubject();
		this.validationMessages = validationObj.getValidationMessages();
	}
}
