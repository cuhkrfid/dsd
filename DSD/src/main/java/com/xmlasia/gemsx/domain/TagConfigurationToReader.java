package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TAGCONFIGURATIONTOREADER")
public class TagConfigurationToReader implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5592980052918588937L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
    @Column(name = "CREATEDATETIME")    
    private Date createDateTime; 
   
    @Column(name = "UPDATEDATETIME")    
    private Date updateDateTime; 
    
    @Column(name = "TAGCONFIGURATIONID")
    private Long tagConfigurationId;
    
    @Column(name = "READERID")
    private Long readerId;
    
    @Transient
    private Reader reader;
    
    @Transient
    private TagConfiguration tagConfiguration;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public Long getTagConfigurationId() {
		return tagConfigurationId;
	}

	public void setTagConfigurationId(Long tagConfigurationId) {
		this.tagConfigurationId = tagConfigurationId;
	}

	public Long getReaderId() {
		return readerId;
	}

	public void setReaderId(Long readerId) {
		this.readerId = readerId;
	}

	public Reader getReader() {
		return reader;
	}

	public void setReader(Reader reader) {
		this.reader = reader;
	}

	public TagConfiguration getTagConfiguration() {
		return tagConfiguration;
	}

	public void setTagConfiguration(TagConfiguration tagConfiguration) {
		this.tagConfiguration = tagConfiguration;
	}
}
