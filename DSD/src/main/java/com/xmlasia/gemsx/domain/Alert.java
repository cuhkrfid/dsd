package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ALERT")
public class Alert  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3096606665265851164L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Transient
	private AlertSetting alertSetting;
	
	@Column(name = "ALERTSETTING_ID", nullable = true)
	private Long alertSettingId;
	
    @Column(name = "CREATEDATETIME")    
    private Date createDateTime; 
    
    @Column(name = "ALERTSETTINGLEVEL")    
    private String alertSettingLevel; 
    
    @Column(name = "ALERTSETTINGNAME")    
    private String alertSettingName; 
    
    @Transient
    private String createDateTimeStr; 
   
    @Transient
    private String createTimeStr; 
    
    @Column(name = "DEVICEADDRESS")
    private String deviceAddress;
    
    @Column(name = "DEVICEID")
    private int deviceId;
    
    @Column(name = "GROUPID")
    private Integer groupId;
    
    @Column(name = "SENSORID")
    private int sensorId;
    
    @Column(name = "TAGCONFIGURATIONID")
    private long tagConfigurationId;
    
    @Column(name = "SITECONFIGURATIONID")
    private Long siteConfigurationId;
    
    @Column(name = "SENSORCONFIGID")
    private long sensorConfigId;
        
    @Transient
    private String messageHtml; 
    
    @Transient
    private String level;
    
    @Transient
    private String name;
    
    @Transient
    private boolean isRead = true;
    
    @Column(name = "COVERDETECT")
    private String coverDetect;
    
    @Column(name = "COVERLEVEL")
    private Integer coverLevel;
    
    @Column(name = "WATERLEVEL")
    private Integer waterLevel;
    
    @Column(name = "H2S")
    private Double h2s;
    
    @Column(name = "SO2")
    private Double so2;
  
    @Column(name = "CO2")
    private Double co2;
    
    @Column(name = "CH4")
    private Double ch4;

    @Column(name = "NH3")        
    private Double nh3;
    
    @Column(name = "O2")
    private Double o2;
        
    @Column(name = "ISCOVERDETECT")
    private Boolean isCoverDetect;
    
    @Column(name = "ISCOVERLEVEL")
    private boolean isCoverLevel;
    
    @Column(name = "ISWATERLEVEL")
    private boolean isWaterLevel;
    
	@Column(name = "ISCO2")
	private boolean isCo2;

	@Column(name = "ISH2S")
	private boolean isH2s;
	
	@Column(name = "ISSO2")
	private boolean isSo2; 
	
	@Column(name = "ISCH4")
	private boolean isCh4;
	
	@Column(name = "ISNH3")
	private boolean isNh3;
	
	@Column(name = "ISO2")
	private boolean isO2;
    
	@Column(name = "TAGCONFIGURATIONNAME")
	private String tagConfigurationName;
	    
    @Transient
    private TagConfiguration tagConfiguration;
    
    @Transient
    private SensorConfig sensorConfig;
    
	@Column(name = "COVERLEVELALERTMESSAGE")
	private String coverLevelAlertMessage = "";
	
	@Column(name = "WATERLEVELALERTMESSAGE")
	private String waterLevelAlertMessage = "";
	
	@Column(name = "H2SALERTMESSAGE")
	private String h2sAlertMessage = "";
	
	@Column(name = "SO2ALERTMESSAGE")
	private String so2AlertMessage = "";
	
	@Column(name = "CO2ALERTMESSAGE")
	private String co2AlertMessage = "";
    
	@Column(name = "CH4ALERTMESSAGE")
	private String ch4AlertMessage = "";
	  
	@Column(name = "NH3ALERTMESSAGE")
	private String nh3AlertMessage = "";

	@Column(name = "O2ALERTMESSAGE")
	private String o2AlertMessage = "";
    
	@Column(name = "COVERDETECTALERTMESSAGE")
	private String coverDetectAlertMessage = "";
	
	@Transient
	private String mobileAlertIcon;
	
	public void enrichMobileAlertIcon(String alertSettingLevel) {
		
		if ("Critical".equals(alertSettingLevel)) 
			setMobileAlertIcon("img/mobile/alertCritical.svg");
		else if ("High".equals(alertSettingLevel)) 
			setMobileAlertIcon("img/mobile/alertHigh.svg");
		else if ("Medium".equals(alertSettingLevel)) 
			setMobileAlertIcon("img/mobile/alertMedium.svg");
		else
			setMobileAlertIcon("img/mobile/alertLow.svg");
	}
	
    public static String createMessage(Alert alert, String platformLineSeperator) {
    	String generalMessage = "";
    	
    	if (alert.getCoverDetectAlertMessage() != null && !alert.getCoverDetectAlertMessage().isEmpty())
    		generalMessage += ("Cover Detect: " + alert.getCoverDetectAlertMessage() + platformLineSeperator);
    	
    	if (alert.getCoverLevelAlertMessage() != null && !alert.getCoverLevelAlertMessage().isEmpty())
    		generalMessage += ("Cover Level: " + alert.getCoverLevelAlertMessage() + platformLineSeperator);
    	
      	if (alert.getWaterLevelAlertMessage() != null && !alert.getWaterLevelAlertMessage().isEmpty())
      		generalMessage += ("Water Level: " + alert.getWaterLevelAlertMessage() + platformLineSeperator);
    	
      	if (alert.getH2sAlertMessage() != null && !alert.getH2sAlertMessage().isEmpty())
      		generalMessage += ("H2s: " + alert.getH2sAlertMessage() + platformLineSeperator);
    	
      	if (alert.getSo2AlertMessage() != null && !alert.getSo2AlertMessage().isEmpty())
      		generalMessage += ("So2: " + alert.getSo2AlertMessage() + platformLineSeperator);
    	
      	if (alert.getCo2AlertMessage() != null && !alert.getCo2AlertMessage().isEmpty())
      		generalMessage += ("Co2: " + alert.getCo2AlertMessage() + platformLineSeperator);
    	
      	if (alert.getNh3AlertMessage() != null && !alert.getNh3AlertMessage().isEmpty())
      		generalMessage += ("Nh3: " + alert.getNh3AlertMessage() + platformLineSeperator);
    	
      	return generalMessage;
    }
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getAlertSettingId() {
		return alertSettingId;
	}

	public void setAlertSettingId(Long alertSettingId) {
		this.alertSettingId = alertSettingId;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public int getSensorId() {
		return sensorId;
	}

	public void setSensorId(int sensorId) {
		this.sensorId = sensorId;
	}

	public String getCreateDateTimeStr() {
		return createDateTimeStr;
	}

	public void setCreateDateTimeStr(String createDateTimeStr) {
		this.createDateTimeStr = createDateTimeStr;
	}

	public String getMessageHtml() {
		return messageHtml;
	}

	public void setMessageHtml(String messageHtml) {
		this.messageHtml = messageHtml;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public AlertSetting getAlertSetting() {
		return alertSetting;
	}

	public void setAlertSetting(AlertSetting alertSetting) {
		this.alertSetting = alertSetting;
		

	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getAlertSettingLevel() {
		return alertSettingLevel;
	}

	public void setAlertSettingLevel(String alertSettingLevel) {
		this.alertSettingLevel = alertSettingLevel;
		enrichMobileAlertIcon(alertSettingLevel);
	}

	public String getAlertSettingName() {
		return alertSettingName;
	}

	public void setAlertSettingName(String alertSettingName) {
		this.alertSettingName = alertSettingName;
	}
	
    public double getO2() {
        return o2;
    }

    public void setO2(double o2) {
        this.o2 = o2;
    }

    public double getH2s() {
        return h2s;
    }

    public void setH2s(double h2s) {
        this.h2s = h2s;
    }

    public double getSo2() {
        return so2;
    }

    public void setSo2(double so2) {
        this.so2 = so2;
    }

    
    public double getCo2() {
        return this.co2;
    }

    public void setCo2(double co2) {
        this.co2 = co2;
    }

    public double getCh4() {
        return ch4;
    }

    public void setCh4(double ch4) {
        this.ch4 = ch4;
    }

    public double getNh3() {
        return nh3;
    }

    public void setNh3(double nh3) {
        this.nh3 = nh3;
    }
	
	public int getCoverLevel() {
		return coverLevel;
	}

	public void setCoverLevel(int coverLevel) {
		this.coverLevel = coverLevel;
	}

	public int getWaterLevel() {
		return waterLevel;
	}

	public void setWaterLevel(int waterLevel) {
		this.waterLevel = waterLevel;
	}
	
	public boolean getIsCo2() {
		return isCo2;
	}

	public void setIsCo2(boolean isCo2) {
		this.isCo2 = isCo2;
	}

	public boolean getIsH2s() {
		return isH2s;
	}

	public void setIsH2s(boolean isH2s) {
		this.isH2s = isH2s;
	}

	public boolean getIsSo2() {
		return isSo2;
	}

	public void setIsSo2(boolean isSo2) {
		this.isSo2 = isSo2;
	}

	public boolean getIsCh4() {
		return isCh4;
	}

	public void setIsCh4(boolean isCh4) {
		this.isCh4 = isCh4;
	}

	public boolean getIsNh3() {
		return isNh3;
	}

	public void setIsNh3(boolean isNh3) {
		this.isNh3 = isNh3;
	}

	public boolean getIsO2() {
		return isO2;
	}

	public void setIsO2(boolean isO2) {
		this.isO2 = isO2;
	}

	public boolean getIsCoverLevel() {
		return isCoverLevel;
	}
	public void setIsCoverLevel(boolean isCoverLevel) {
		this.isCoverLevel = isCoverLevel;
	}

	public boolean getIsWaterLevel() {
		return isWaterLevel;
	}
	public void setIsWaterLevel(boolean isWaterLevel) {
		this.isWaterLevel = isWaterLevel;
	}

	public long getTagConfigurationId() {
		return tagConfigurationId;
	}

	public void setTagConfigurationId(long tagConfigurationId) {
		this.tagConfigurationId = tagConfigurationId;
	}

	public long getSensorConfigId() {
		return sensorConfigId;
	}

	public void setSensorConfigId(long sensorConfigId) {
		this.sensorConfigId = sensorConfigId;
	}

	public TagConfiguration getTagConfiguration() {
		return tagConfiguration;
	}

	public void setTagConfiguration(TagConfiguration tagConfiguration) {
		this.tagConfiguration = tagConfiguration;
	}

	public SensorConfig getSensorConfig() {
		return sensorConfig;
	}

	public void setSensorConfig(SensorConfig sensorConfig) {
		this.sensorConfig = sensorConfig;
	}

	public String getTagConfigurationName() {
		return tagConfigurationName;
	}

	public void setTagConfigurationName(String tagConfigurationName) {
		this.tagConfigurationName = tagConfigurationName;
	}

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

	public String getCoverLevelAlertMessage() {
		return coverLevelAlertMessage;
	}

	public void setCoverLevelAlertMessage(String coverLevelAlertMessage) {
		this.coverLevelAlertMessage = coverLevelAlertMessage;
	}

	public String getWaterLevelAlertMessage() {
		return waterLevelAlertMessage;
	}

	public void setWaterLevelAlertMessage(String waterLevelAlertMessage) {
		this.waterLevelAlertMessage = waterLevelAlertMessage;
	}

	public String getH2sAlertMessage() {
		return h2sAlertMessage;
	}

	public void setH2sAlertMessage(String h2sAlertMessage) {
		this.h2sAlertMessage = h2sAlertMessage;
	}

	public String getSo2AlertMessage() {
		return so2AlertMessage;
	}

	public void setSo2AlertMessage(String so2AlertMessage) {
		this.so2AlertMessage = so2AlertMessage;
	}

	public String getCo2AlertMessage() {
		return co2AlertMessage;
	}

	public void setCo2AlertMessage(String co2AlertMessage) {
		this.co2AlertMessage = co2AlertMessage;
	}

	public String getCh4AlertMessage() {
		return ch4AlertMessage;
	}

	public void setCh4AlertMessage(String ch4AlertMessage) {
		this.ch4AlertMessage = ch4AlertMessage;
	}

	public String getNh3AlertMessage() {
		return nh3AlertMessage;
	}

	public void setNh3AlertMessage(String nh3AlertMessage) {
		this.nh3AlertMessage = nh3AlertMessage;
	}

	public String getO2AlertMessage() {
		return o2AlertMessage;
	}

	public void setO2AlertMessage(String o2AlertMessage) {
		this.o2AlertMessage = o2AlertMessage;
	}

	public boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(boolean isRead) {
		this.isRead = isRead;
	}

	public String getMobileAlertIcon() {
		return mobileAlertIcon;
	}

	public void setMobileAlertIcon(String mobileAlertIcon) {
		this.mobileAlertIcon = mobileAlertIcon;
	}

	public String getCoverDetect() {
		return coverDetect;
	}

	public void setCoverDetect(String coverDetect) {
		this.coverDetect = coverDetect;
	}

	public Boolean getIsCoverDetect() {
		return isCoverDetect;
	}

	public void setIsCoverDetect(Boolean isCoverDetect) {
		this.isCoverDetect = isCoverDetect;
	}

	public String getCoverDetectAlertMessage() {
		return coverDetectAlertMessage;
	}

	public void setCoverDetectAlertMessage(String coverDetectAlertMessage) {
		this.coverDetectAlertMessage = coverDetectAlertMessage;
	}

	public Long getSiteConfigurationId() {
		return siteConfigurationId;
	}

	public void setSiteConfigurationId(Long siteConfigurationId) {
		this.siteConfigurationId = siteConfigurationId;
	}
}
