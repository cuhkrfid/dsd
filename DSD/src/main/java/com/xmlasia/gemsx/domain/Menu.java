package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "MENUS")
@Audited
public class Menu extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565873292246413977L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "NAME", length = 255)
	private String name;

	@Column(name = "DESCRIPTION", length = 255)
	private String description;

	@Column(name = "DISPLAY_ORDER")
	private Integer displayOrder;

	@Column(name = "ACTIVE")
	private Boolean active;
	
	@Column(name = "PATH", length = 255)
	private String path;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "MODULE_ID", nullable = true, updatable = false, insertable = false, referencedColumnName = "ID")
	private Module module;
	
	@Column(name = "MODULE_ID", nullable = true)
	private Long moduleId;

	@Column(name = "IMAGEPATH")
	private String imagePath; 
	
	@Column(name = "SIDEMENU")
	private boolean sideMenu = true;
	
	@Column(name = "MOBILESUPPORT")
	private boolean mobileSupport = true;
	
	@Column(name = "MOBILEIMAGEPATH")
	private String mobileImagePath = ""; 
	
	@Column(name = "MOBILEBACKGROUNDIMAGE")
	private String mobileBackgroundImage; 
	
	@Override
	public Object getKey() {
		return id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public boolean getSideMenu() {
		return sideMenu;
	}

	public void setSideMenu(boolean sideMenu) {
		this.sideMenu = sideMenu;
	}

	public boolean getMobileSupport() {
		return mobileSupport;
	}

	public void setMobileSupport(boolean mobileSupport) {
		this.mobileSupport = mobileSupport;
	}

	public String getMobileImagePath() {
		return mobileImagePath;
	}

	public void setMobileImagePath(String mobileImagePath) {
		this.mobileImagePath = mobileImagePath;
	}

	public String getMobileBackgroundImage() {
		return mobileBackgroundImage;
	}

	public void setMobileBackgroundImage(String mobileBackgroundImage) {
		this.mobileBackgroundImage = mobileBackgroundImage;
	}

}