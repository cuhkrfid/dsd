package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "ROLE_FUNCTION_MAPPINGS")
@IdClass(RoleFunctionMappingPK.class)
@Audited
public class RoleFunctionMapping extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565873292246413977L;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ROLE_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private Role role;
	
	@Column(name = "ROLE_ID", nullable = false, length=10)
	@Id
	private int roleId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="FUNCTION_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private Function function;
	
	@Id
	@Column(name = "FUNCTION_ID", nullable = false)
	private int functionId;
	
	@Override
	public RoleFunctionMappingPK getKey() {
		return new RoleFunctionMappingPK(roleId, functionId);
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public int getFunctionId() {
		return functionId;
	}

	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}

}