package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "ROLE_GROUP_MAPPINGS")
@IdClass(RoleGroupMappingPK.class)
@Audited
public class RoleGroupMapping extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565873292246413977L;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ROLE_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private Role role;
	
	@Column(name = "ROLE_ID", nullable = false, length=10)
	@Id
	private int roleId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="GROUP_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private Group group;
	
	@Id
	@Column(name = "GROUP_ID", nullable = false)
	private int groupId;
	
	@Override
	public RoleGroupMappingPK getKey() {
		return new RoleGroupMappingPK(roleId, groupId);
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

}