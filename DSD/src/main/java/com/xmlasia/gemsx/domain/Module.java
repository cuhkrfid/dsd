package com.xmlasia.gemsx.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "MODULES")
@Audited
public class Module extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565873292246413977L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "NAME", length = 255)
	private String name;

	@Column(name = "DESCRIPTION", length = 255)
	private String description;

	@Column(name = "DISPLAY_ORDER")
	private Integer displayOrder;
	
	@Column(name = "HOMEPAGE")
	private Boolean homepage;
	
	@Column(name = "ACTIVE")
	private Boolean active;
	
	@Column(name = "PATH", length = 255)
	private String path;
	
	//
	@Column(name = "IMAGEURLPATH")
	private String imageUrlPath;
	//
	@Column(name = "IMAGEFILENAME")
	private String imageFileName;
	
	@Column(name = "MOBILESUPPORT")
	private boolean mobileSupport = true;
	
	@Column(name = "BACKGROUNDIMAGE")
	private String backgroundImage;

	@Column(name = "MOBILEBACKGROUNDIMAGE")
	private String mobileBackgroundImage; 
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "module")
	private List<Menu> menus;
	
	@Override
	public Object getKey() {
		return id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public Boolean getHomepage() {
		return homepage;
	}

	public void setHomepage(Boolean homepage) {
		this.homepage = homepage;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getImageUrlPath() {
		return imageUrlPath;
	}

	public void setImageUrlPath(String imageUrlPath) {
		this.imageUrlPath = imageUrlPath;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public boolean getMobileSupport() {
		return mobileSupport;
	}

	public void setMobileSupport(boolean mobileSupport) {
		this.mobileSupport = mobileSupport;
	}

	public String getBackgroundImage() {
		return backgroundImage;
	}

	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}

	public String getMobileBackgroundImage() {
		return mobileBackgroundImage;
	}

	public void setMobileBackgroundImage(String mobileBackgroundImage) {
		this.mobileBackgroundImage = mobileBackgroundImage;
	}
}