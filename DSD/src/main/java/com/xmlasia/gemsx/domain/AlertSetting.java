package com.xmlasia.gemsx.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ALERTSETTING")
public class AlertSetting extends Validation {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5977295003560097473L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
    @Column(name = "NAME")
	private String name;
    
    @Column(name = "LEVEL")
	private String level;
    
    @Column(name = "USEDATERANGE")
	private boolean useDateRange; 
    
    @Column(name = "FROMDATE")
	private Date fromDate; 
    
    @Transient
    private String fromDateStr; 
    
    
    @Column(name = "TODATE")
	private Date toDate;
  
    @Transient
    private String toDateStr; 
  
    
    @Column(name = "DEVICEADDRESS")
	private String deviceAddress;
    
	@Column(name = "DEVICE_ID")
	private int deviceId;

	@Column(name = "SITE_ID")
	private Long siteId;
	
	@Column(name = "TAG_ID")
	private Integer tagId;
 	
    @Column(name = "GROUP_ID")
    private Integer groupId;
    
    @Column(name = "ISSENDEMAIL")
    private boolean isSendEmail; 
    
    @Column(name = "EMAILS")
    private String emails;

    @Column(name = "ISREPEATALERT")
    private boolean isRepeatAlert;

    @Column(name = "REPEATALERTINSECS")
    private Integer repeatAlertInSecs;

    @Column(name = "TRIGGERTIME")
    private Date triggerTime;
    
    @Column(name = "ISTRIGGERED")
    private boolean isTriggered;

    @Column(name = "USESITEID")
    private boolean useSiteId; 
    
    @Column(name = "USEGROUPID")
    private boolean useGroupId; 

    @Column(name = "ALERTCRITERIAID")
    private Long alertCriteriaId;
    
    @Transient
    private AlertCriteria alertCriteria;
    
    @Transient
	private String[] coverLevelHtml;
	
	@Transient
	private String[] waterLevelHtml;
	
	@Transient
	private String[] h2sHtml;
	
	@Transient
	private String[] so2Html;
	
	@Transient
	private String[] co2Html;

	@Transient
	private String[] ch4Html;
	
	@Transient
	private String[] nh3Html;
	
	@Transient
	private String[] o2Html;
	
	@Transient
	private String[] emailsHtml;

	@Transient
	private String[] repeatAlertHtml;
	
	@Transient
	private SiteConfiguration siteConfiguration;
	
	@Transient 
	private AlertSettingSensorConfig[] alertSettingSensorConfigs;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isUseDateRange() {
		return useDateRange;
	}

	public void setUseDateRange(boolean useDateRange) {
		this.useDateRange = useDateRange;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public boolean getIsSendEmail() {
		return isSendEmail;
	}

	public void setIsSendEmail(boolean isSendEmail) {
		this.isSendEmail = isSendEmail;
	}

	public String getEmails() {
		return emails;
	}

	public void setEmails(String emails) {
		this.emails = emails;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getFromDateStr() {
		return fromDateStr;
	}

	public void setFromDateStr(String fromDateStr) {
		this.fromDateStr = fromDateStr;
	}

	public String getToDateStr() {
		return toDateStr;
	}

	public void setToDateStr(String toDateStr) {
		this.toDateStr = toDateStr;
	}

	public String[] getCoverLevelHtml() {
		return coverLevelHtml;
	}
	public void setCoverLevelHtml(String coverLevelHtml) {
		if ( coverLevelHtml == null || coverLevelHtml.isEmpty())
			return;
		this.coverLevelHtml = coverLevelHtml.split(";");
	}

	public String[] getWaterLevelHtml() {
		return waterLevelHtml;
	}
	public void setWaterLevelHtml(String waterLevelHtml) {
		if ( waterLevelHtml == null || waterLevelHtml.isEmpty())
			return;
		this.waterLevelHtml = waterLevelHtml.split(";");
	}

	public String[] getH2sHtml() {
		return h2sHtml;
	}
	public void setH2sHtml(String h2sHtml) {
		if ( h2sHtml == null || h2sHtml.isEmpty())
			return;
		this.h2sHtml = h2sHtml.split(";");
	}

	public String[] getSo2Html() {
		return so2Html;
	}
	public void setSo2Html(String so2Html) {
		if ( so2Html == null || so2Html.isEmpty())
			return;
		this.so2Html = so2Html.split(";");
	}

	public String[] getCo2Html() {
		return co2Html;
	}
	public void setCo2Html(String co2Html) {
		if ( co2Html == null || co2Html.isEmpty())
			return;
		this.co2Html = co2Html.split(";");
	}
	
	public String[] getCh4Html() {
		return ch4Html;
	}
	public void setCh4Html(String ch4Html) {
		if ( ch4Html == null || ch4Html.isEmpty())
			return;
		
		this.ch4Html = ch4Html.split(";");
	}

	public String[] getNh3Html() {
		return nh3Html;
	}
	public void setNh3Html(String nh3Html) {
		if ( nh3Html == null || nh3Html.isEmpty())
			return;
		
		this.nh3Html = nh3Html.split(";");
	}

	public String[] getO2Html() {
		return o2Html;
	}
	public void setO2Html(String o2Html) {
		if ( o2Html == null || o2Html.isEmpty())
			return;
		
		this.o2Html = o2Html.split(";");
	} 

	public String[] getEmailsHtml() {
		return emailsHtml;
	}
	public void setEmailsHtml(String[] emailsHtml) {
		this.emailsHtml = emailsHtml;
	}

	public boolean getIsRepeatAlert() {
		return isRepeatAlert;
	}
	public void setIsRepeatAlert(boolean isRepeatAlert) {
		this.isRepeatAlert = isRepeatAlert;
	}

	public Integer getRepeatAlertInSecs() {
		return repeatAlertInSecs;
	}
	public void setRepeatAlertInSecs(Integer repeatAlertInSecs) {
		this.repeatAlertInSecs = repeatAlertInSecs;
	}

	public String[] getRepeatAlertHtml() {
		return repeatAlertHtml;
	}
	public void setRepeatAlertHtml(String[] repeatAlertHtml) {
		this.repeatAlertHtml = repeatAlertHtml;
	}

	public Date getTriggerTime() {
		return triggerTime;
	}
	public void setTriggerTime(Date triggerTime) {
		this.triggerTime = triggerTime;
	}

	public boolean getIsTriggered() {
		return isTriggered;
	}
	public void setIsTriggered(boolean isTriggered) {
		this.isTriggered = isTriggered;
	}

	public Integer getTagId() {
		return tagId;
	}
	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	public boolean getUseGroupId() {
		return useGroupId;
	}
	public void setUseGroupId(boolean useGroupId) {
		this.useGroupId = useGroupId;
	}

	public boolean getUseSiteId() {
		return useSiteId;
	}
	public void setUseSiteId(boolean useSiteId) {
		this.useSiteId = useSiteId;
	}

	public Long getAlertCriteriaId() {
		return alertCriteriaId;
	}
	public void setAlertCriteriaId(Long alertCriteriaId) {
		this.alertCriteriaId = alertCriteriaId;
	}

	public AlertCriteria getAlertCriteria() {
		return alertCriteria;
	}
	public void setAlertCriteria(AlertCriteria alertCriteria) {
		this.alertCriteria = alertCriteria;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public AlertSettingSensorConfig[] getAlertSettingSensorConfigs() {
		return alertSettingSensorConfigs;
	}

	public void setAlertSettingSensorConfigs(AlertSettingSensorConfig[] alertSettingSensorConfigs) {
		this.alertSettingSensorConfigs = alertSettingSensorConfigs;
	}

	public SiteConfiguration getSiteConfiguration() {
		return siteConfiguration;
	}

	public void setSiteConfiguration(SiteConfiguration siteConfiguration) {
		this.siteConfiguration = siteConfiguration;
	}
    
    
}
