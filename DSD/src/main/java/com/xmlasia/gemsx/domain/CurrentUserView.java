package com.xmlasia.gemsx.domain;


public class CurrentUserView {

	private int deviceId; 
	private int sensorId;
	
	public int getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	public int getSensorId() {
		return sensorId;
	}
	public void setSensorId(int sensorId) {
		this.sensorId = sensorId;
	} 
	
	@Override
    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof CurrentUserView)) {
            return false;
        }
        CurrentUserView castOther = (CurrentUserView) other;

        return (this.getDeviceId() == castOther.getDeviceId())
                && (this.getSensorId() == castOther.getSensorId());
    }
	@Override
    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getDeviceId();
        result = 37 * result + this.getSensorId();
        return result;
    }
    
	
}
