package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "GROUPTAGCONFIGURATIONMAP")
public class GroupTagConfigurationMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3318359088847141532L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "GROUPTAGCONFIGURATIONID")
	private long groupTagConfigurationId;
	
	@Column(name = "TAGCONFIGURATIONID")
	private long tagConfigurationId;

    @Column(name = "CREATEDATETIME")    
    private Date createDateTime; 
    
	@Transient
	private GroupTagConfiguration groupTagConfiguration;

	@Transient
	private TagConfiguration tagConfiguration;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getGroupTagConfigurationId() {
		return groupTagConfigurationId;
	}

	public void setGroupTagConfigurationId(long groupTagConfigurationId) {
		this.groupTagConfigurationId = groupTagConfigurationId;
	}

	public long getTagConfigurationId() {
		return tagConfigurationId;
	}

	public void setTagConfigurationId(long tagConfigurationId) {
		this.tagConfigurationId = tagConfigurationId;
	}

	public GroupTagConfiguration getGroupTagConfiguration() {
		return groupTagConfiguration;
	}

	public void setGroupTagConfiguration(GroupTagConfiguration groupTagConfiguration) {
		this.groupTagConfiguration = groupTagConfiguration;
	}

	public TagConfiguration getTagConfiguration() {
		return tagConfiguration;
	}

	public void setTagConfiguration(TagConfiguration tagConfiguration) {
		this.tagConfiguration = tagConfiguration;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}
}
