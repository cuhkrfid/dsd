package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "ROLE_MENU_MAPPINGS")
@IdClass(RoleMenuMappingPK.class)
@Audited
public class RoleMenuMapping extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565873292246413977L;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ROLE_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private Role role;
	
	@Column(name = "ROLE_ID", nullable = false, length=10)
	@Id
	private int roleId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="MENU_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private Menu menu;
	
	@Id
	@Column(name = "MENU_ID", nullable = false)
	private long menuId;

	
	@Override
	public RoleMenuMappingPK getKey() {
		return new RoleMenuMappingPK(roleId, menuId);
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public long getMenuId() {
		return menuId;
	}

	public void setMenuId(long menuId) {
		this.menuId = menuId;
	}

}