package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "GROUPS")
@Audited
public class Group extends Validation {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565873292246413977L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "NAME", length = 255)
	private String name;
	
	@Column(name = "DESCRIPTION", length = 255)
	private String description = "";
	
	@Column(name = "ACTIVE")
	private Boolean active = false;
	
	@Transient
	private GroupMenuMapping[] groupMenuMapping;
	
	@Override
	public Object getKey() {
		return id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getActive() {
		return active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}

	public GroupMenuMapping[] getGroupMenuMapping() {
		return groupMenuMapping;
	}

	public void setGroupMenuMapping(GroupMenuMapping[] groupMenuMapping) {
		this.groupMenuMapping = groupMenuMapping;
	}

	
}