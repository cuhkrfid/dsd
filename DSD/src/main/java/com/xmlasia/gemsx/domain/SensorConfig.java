package com.xmlasia.gemsx.domain;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

import com.xmlasia.gemsx.domain.BaseDomain;

@Entity
@Table(name = "SENSORCONFIG")
@Audited
public class SensorConfig extends BaseDomain {


	/**
	 * 
	 */
	private static final long serialVersionUID = -7294855946936894466L;


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;

	
	@Column(name = "SENSORID")
	private long sensorId;
	
	@Column(name = "ISCOVERLEVEL")
	private boolean isCoverLevel;

	@Column(name = "ISWATERLEVEL")
	private boolean isWaterLevel;
	
	@Column(name = "ISCO2")
	private boolean isCo2;
	
	@Column(name = "ISH2S")
	private boolean isH2s;
	
	@Column(name = "ISSO2")
	private boolean isSo2;
	
	@Column(name = "ISCH4")
	private boolean isCh4;	
	
	@Column(name = "ISNH3")
	private boolean isNh3;	
	
	@Column(name = "ISO2")
	private boolean isO2;	
	
	@Column(name = "TAGCONFIGURATIONID")
	private long tagConfigurationId;

	@Column(name = "AUTOCREATE")
	private boolean autoCreate; 

	@Column(name = "EXPIRED")
	private boolean expired;
	
	@Column(name = "INPUTDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date inputDate;
	
	@Transient
	private String name;
	
	@Transient
	private String deviceAddress;
	
	@Column(name = "DEVICEID")
	private long deviceId;
	
	@Transient
	private String description;
	
	@Transient
	private String sensorTypeHtml;
	
	@Column(name = "MEASUREPERIOD")
	private int measurePeriod;
	
	@Transient
    private String timeDiffInMsStr = "N/A";
	
	public boolean getIsCoverLevel() {
		return isCoverLevel;
	}
	public void setIsCoverLevel(boolean isCoverLevel) {
		this.isCoverLevel = isCoverLevel;
	}

	public boolean getIsWaterLevel() {
		return isWaterLevel;
	}
	public void setIsWaterLevel(boolean isWaterLevel) {
		this.isWaterLevel = isWaterLevel;
	}

	public boolean getIsCo2() {
		return isCo2;
	}
	public void setIsCo2(boolean isCo2) {
		this.isCo2 = isCo2;
	}

	public boolean getIsH2s() {
		return isH2s;
	}
	public void setIsH2s(boolean isH2s) {
		this.isH2s = isH2s;
	}

	public boolean getIsSo2() {
		return isSo2;
	}
	public void setIsSo2(boolean isSo2) {
		this.isSo2 = isSo2;
	}

	public boolean getIsCh4() {
		return isCh4;
	}
	public void setIsCh4(boolean isCh4) {
		this.isCh4 = isCh4;
	}

	public boolean getIsNh3() {
		return isNh3;
	}
	public void setIsNh3(boolean isNh3) {
		this.isNh3 = isNh3;
	}

	public boolean getIsO2() {
		return isO2;
	}
	public void setIsO2(boolean isO2) {
		this.isO2 = isO2;
	}
	
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getId();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getSensorId() {
		return sensorId;
	}

	public void setSensorId(long sensorId) {
		this.sensorId = sensorId;
	}
	
	public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }
        if ((other == null)) {
            return false;
        }
        if (!(other instanceof SensorConfig)) {
            return false;
        }
        SensorConfig castOther = (SensorConfig) other;

        return (this.getId() == castOther.getId())
                && (this.getSensorId() == castOther.getSensorId());
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (int)this.getId();
        result = 37 * result + (int)this.getSensorId();
        return result;
    }

	public long getTagConfigurationId() {
		return tagConfigurationId;
	}

	public void setTagConfigurationId(long tagConfigurationId) {
		this.tagConfigurationId = tagConfigurationId;
	}

	public String getSensorTypeHtml() {
		return sensorTypeHtml;
	}

	public void setSensorTypeHtml(String sensorTypeHtml) {
		this.sensorTypeHtml = sensorTypeHtml;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	public String getTimeDiffInMsStr() {
		return timeDiffInMsStr;
	}

	public void setTimeDiffInMsStr(String timeDiffInMsStr) {
		this.timeDiffInMsStr = timeDiffInMsStr;
	}

	public int getMeasurePeriod() {
		return measurePeriod;
	}

	public void setMeasurePeriod(int measurePeriod) {
		this.measurePeriod = measurePeriod;
	}

	public boolean getAutoCreate() {
		return autoCreate;
	}

	public void setAutoCreate(boolean autoCreate) {
		this.autoCreate = autoCreate;
	}
	
	public void copy (SensorConfig other) {
		this.setSensorId(other.getSensorId());
		this.setDeviceId(other.getDeviceId());
		this.setDeviceAddress(other.getDeviceAddress());	
		this.setMeasurePeriod(other.getMeasurePeriod());

		this.setIsWaterLevel(other.getIsWaterLevel());
		this.setIsCoverLevel(other.getIsCoverLevel());
		this.setIsCo2(other.getIsCo2());
		this.setIsH2s(other.getIsH2s());
		this.setIsSo2(other.getIsSo2());
		this.setIsCh4(other.getIsCh4());
		this.setIsNh3(other.getIsNh3());
		this.setIsO2(other.getIsO2());
		
		this.setAutoCreate(other.getAutoCreate());
		this.setExpired(other.getExpired());
		this.setInputDate(other.getInputDate());
	}

	public boolean getExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public Date getInputDate() {
		return inputDate;
	}

	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}


}
