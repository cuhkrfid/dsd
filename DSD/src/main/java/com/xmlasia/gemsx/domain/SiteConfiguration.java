package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;


@Entity
@Table(name = "SITECONFIGURATION")
@Audited
public class SiteConfiguration extends Validation {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9093027509030192742L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "NAME", length = 255)
	private String name;
	
	@Column(name = "MAPFILENAME", length = 255)
	private String mapFileName;
	
	@Column(name = "MAPHEIGHT", length = 255)
	private long mapHeight = 0;
	
	@Column(name = "MAPWIDTH", length = 255)
	private long mapWidth = 0;
	
	
	@Column(name = "DESCRIPTION", length = 255)
	private String description;

	@Column(name = "IMAGEURLPATH", length = 255)
	private String imageUrlPath;

	@Column(name = "DEFAULTSITE")
	private boolean defaultSite;

	@Column(name = "LAT")
	private double lat;
	
	@Column(name = "LON")
	private double lon;
	
	@Column(name = "ZOOMLEVEL")
	private int zoomLevel;
	
	
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getId();
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMapFileName() {
		return mapFileName;
	}

	public void setMapFileName(String mapFileName) {
		this.mapFileName = mapFileName;
	}

	public long getMapHeight() {
		return mapHeight;
	}

	public void setMapHeight(long mapHeight) {
		this.mapHeight = mapHeight;
	}

	public long getMapWidth() {
		return mapWidth;
	}

	public void setMapWidth(long mapWidth) {
		this.mapWidth = mapWidth;
	}

	public String getImageUrlPath() {
		return imageUrlPath;
	}

	public void setImageUrlPath(String imageURLPath) {
		this.imageUrlPath = imageURLPath;
	}

	public boolean getDefaultSite() {
		return defaultSite;
	}

	public void setDefaultSite(boolean defaultSite) {
		this.defaultSite = defaultSite;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public int getZoomLevel() {
		return zoomLevel;
	}

	public void setZoomLevel(int zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

}
