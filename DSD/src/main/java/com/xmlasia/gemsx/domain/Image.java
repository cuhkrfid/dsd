package com.xmlasia.gemsx.domain;

import java.io.IOException;
import java.io.InputStream;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.IOUtils;
import org.hibernate.envers.Audited;

@Entity
@Table(name = "IMAGES")
@Audited
public class Image extends BaseDomain {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4792603307015273191L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "NAME", length = 255)
	private String name;
	
	@Column(name = "DESCRIPTION", length = 255)
	private String description;
	
	@Column(name = "SIZE")
	private Long size; 
		
	@Column(name = "ACTIVE")
	private Boolean active;

	@Lob
	@Column(name = "CONTENT")
	private byte[] content;

	@Transient 
	private String base64Content;
	
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getId();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBase64Content() {
		return base64Content;
	}

	public void setBase64Content(String displayContent) {
		this.base64Content = displayContent;
	}
	
	public static Image convertToBase64(Image image) {
		if (image != null && image.getContent()  != null) {
			byte[] imageByteArray = image.getContent();
		    StringBuilder sb = new StringBuilder();
		    sb.append("data:image/JPEG;base64,");
		    sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(imageByteArray, false)));
		    image.setBase64Content(sb.toString());
		} 
			
		return image;
	}
	
	public static Image createNewImage(String description, Boolean active, String fileName, InputStream fileData) {
		Image newImage;
		newImage = new Image();
		newImage.setActive(active);
		newImage.setDescription(description);
		newImage.setName(fileName);
		try {
			byte[] fileInBytes = IOUtils.toByteArray(fileData);
			newImage.setSize(fileInBytes.length);
			newImage.setContent(fileInBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newImage;
	}
}
