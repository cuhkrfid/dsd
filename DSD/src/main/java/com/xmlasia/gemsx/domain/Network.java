package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "NETWORK", indexes = { @Index(columnList = "cordId", name = "network_cordId_idx")})
public class Network implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4327956213268798098L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
    @Column(name = "CORDID")
	private int cordId;
    
    @Column(name = "DEVICEID")
    private Integer deviceId;
    
    @Column(name = "ROUTERID")
    private Integer routerId;
    
    //0 = EndDevice
    //1 = Router
    //2 = Coordinator
    
    @Column(name = "DEVICETYPE")
    private int deviceType; 
    
    public static final int ENDDEVICE = 0;
    public static String END = "END";
    
    public static final int ROUTER = 1;
    public static String ROUT = "ROUT";
    
    public static final int COORDINATOR = 2;
    public static String COORD= "COORD";
    
    @Column(name = "PARENTADDRESS")
    private String parentAddress;
	
    @Column(name = "STATUSID")
    private Integer statusId;
    
    @Column(name = "UPDATEDTIME")
    private Date updatedTime;
    
    @Column(name = "LASTUPDATEDTIME")    
    private Date lastUpdatedTime; 
    
    @Column(name = "DEVICEADDRESS")
    private String deviceAddress;
    
    @Column(name = "DEVICETYPENAME")
    private String deviceTypeName;
    
    @Column(name = "TIMEDIFFINMS")
    private Long timeDiffInMs;
    
    @Column(name = "CONNECTED")
    private boolean connected = true; 
    
    @Column(name = "ORPHANED")
    private boolean orphaned;
    
    @Transient
    private String dateString;
    
    @Transient
    private String timeDiffInMsStr = "N/A";
    
    @Transient
    private String routerIdStr;
    
    @Transient
    private String deviceIdStr;
    
    @Column(name = "RECORDLASTSAVETIME")
    private Date recordLastSaveTime;
    
    @Transient
    private String timeDiffMaxInMsStr = "N/A";
    
    public int getCordId() {
        return cordId;
    }

    public void setCordId(int cordId) {
        this.cordId = cordId;
    }

    public String getParentAddress() {
        return this.parentAddress;
    }

    public void setParentAddress(String parentAddress) {
        this.parentAddress = parentAddress;
    }

    public Integer getStatusId() {
        return this.statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Date getUpdatedTime() {
        return this.updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getDeviceAddress() {
        return this.deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public Integer getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }
    
    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
        
        switch(this.deviceType) {
        case ENDDEVICE:
        	deviceTypeName = "End Device";
        	break;
        case ROUTER:
        	deviceTypeName = "Router";
        	break;
        case COORDINATOR:
        	deviceTypeName = "Coordinator";
        	break;
        }
    }

	public Integer getRouterId() {
		return routerId;
	}

	public void setRouterId(Integer routerId) {
		this.routerId = routerId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDeviceTypeName() {
		return deviceTypeName;
	}

	public void setDeviceTypeName(String deviceTypeName) {
		this.deviceTypeName = deviceTypeName;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public String getTimeDiffInMsStr() {
		return timeDiffInMsStr;
	}

	public void setTimeDiffInMsStr(String timeDiffInMsStr) {
		this.timeDiffInMsStr = timeDiffInMsStr;
	}

	public String getRouterIdStr() {
		return routerIdStr;
	}

	public void setRouterIdStr(String routerIdStr) {
		this.routerIdStr = routerIdStr;
	}

	public String getDeviceIdStr() {
		return deviceIdStr;
	}

	public void setDeviceIdStr(String deviceIdStr) {
		this.deviceIdStr = deviceIdStr;
	}

	public Date getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Date lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Long getTimeDiffInMs() {
		return timeDiffInMs;
	}

	public void setTimeDiffInMs(Long timeDiffInMs) {
		this.timeDiffInMs = timeDiffInMs;
	}

	public boolean getConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public boolean getOrphaned() {
		return orphaned;
	}

	public void setOrphaned(boolean orphaned) {
		this.orphaned = orphaned;
	}

	public Date getRecordLastSaveTime() {
		return recordLastSaveTime;
	}

	public void setRecordLastSaveTime(Date recordLastSaveTime) {
		this.recordLastSaveTime = recordLastSaveTime;
	}

	public String getTimeDiffMaxInMsStr() {
		return timeDiffMaxInMsStr;
	}

	public void setTimeDiffMaxInMsStr(String timeDiffMaxInMsStr) {
		this.timeDiffMaxInMsStr = timeDiffMaxInMsStr;
	}

}
