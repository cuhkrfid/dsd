package com.xmlasia.gemsx.domain;

import java.io.Serializable;

public class UserGroupMappingPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6483603708564297114L;

	private int userId;

	private int groupId;
	
	public UserGroupMappingPK(){
		super();
	}
	
	public UserGroupMappingPK(int userId, int groupId){
		super();
		this.userId = userId;
		this.groupId = groupId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

}