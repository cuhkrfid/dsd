package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ALERTSETTINGSENSORCONFIG")
public class AlertSettingSensorConfig  extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -471670537319244040L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "ALERTSETTINGID")
	private long alertSettingId; 
	
	@Transient
	private AlertSetting alertSetting;
	
	@Column(name = "TAGCONFIGURATIONID")
	private long tagConfigurationId;
	
	@Transient
	private TagConfiguration tagConfiguration;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAlertSettingId() {
		return alertSettingId;
	}

	public void setAlertSettingId(long alertSettingId) {
		this.alertSettingId = alertSettingId;
	}

	public TagConfiguration getTagConfiguration() {
		return tagConfiguration;
	}

	public void setTagConfiguration(TagConfiguration tagConfiguration) {
		this.tagConfiguration = tagConfiguration;
	}

	public AlertSetting getAlertSetting() {
		return alertSetting;
	}

	public void setAlertSetting(AlertSetting alertSetting) {
		this.alertSetting = alertSetting;
	}

	public long getTagConfigurationId() {
		return tagConfigurationId;
	}

	public void setTagConfigurationId(long tagConfigurationId) {
		this.tagConfigurationId = tagConfigurationId;
	}

	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getId();
	}
}
