package com.xmlasia.gemsx.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "GROUPTAGCONFIGURATION")
public class GroupTagConfiguration extends Validation{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1756625025540301431L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "SITECONFIGURATIONID")
	private long siteConfigurationId; 
	
	@Column(name = "NAME")
	private String name;

	@Transient
	private SiteConfiguration siteConfiguration;
	
	@Transient
	private List<TagConfiguration> tagConfigurations = new ArrayList<TagConfiguration>(); 
	
	@Transient
	private List<Alert> alerts = new ArrayList<Alert>(); 
	
	@Transient
	private List<GroupPolyLine> polylines = new ArrayList<GroupPolyLine>();
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSiteConfigurationId() {
		return siteConfigurationId;
	}

	public void setSiteConfigurationId(long siteConfigurationId) {
		this.siteConfigurationId = siteConfigurationId;
	}

	public List<TagConfiguration> getTagConfigurations() {
		return tagConfigurations;
	}

	public void setTagConfigurations(List<TagConfiguration> tagConfigurations) {
		this.tagConfigurations = tagConfigurations;
	}

	public List<Alert> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<Alert> alerts) {
		this.alerts = alerts;
	}

	public SiteConfiguration getSiteConfiguration() {
		return siteConfiguration;
	}

	public void setSiteConfiguration(SiteConfiguration siteConfiguration) {
		this.siteConfiguration = siteConfiguration;
	}

	public List<GroupPolyLine> getPolylines() {
		return polylines;
	}

	public void setPolylines(List<GroupPolyLine> polylines) {
		this.polylines = polylines;
	}
	
	
}
