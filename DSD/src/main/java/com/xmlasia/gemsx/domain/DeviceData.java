package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.xmlasia.gemsx.domain.Sensor;

@Entity
@Table(name = "DEVICEDATA", indexes = { @Index(columnList = "inputDate", name = "devicedata_inputdate_idx")})
public class DeviceData  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2921875336281309006L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
    public static final int LENGTH_SENSOR_INDICATION = 1;
   
    public static final int LENGTH_PACKET_COUNTER = 2;
    public static final int LENGTH_MEASUREMENT_PERIOD = 2;
    public static final int LENGTH_RSSI = 4;
    public static final int LENGTH_USER_DEFINE = 2;
    public static final int LENGTH_ADCVALUE = 2;
    public static final int LENGTH_SNR = 4;
    
    public static final int LENGTH_PARENTADDRESS = 2;
    public static final int LENGTH_NUMBER_OF_SENSOR = 1;
   
    
    @Column(name = "NUMBEROFSENSOR")
    private int numberOfSensor;
    
    @Column(name = "MEASUREPERIOD")
    private int measurePeriod;
    
    @Column(name = "DEVICEID")
    private int deviceId;
    
    @Column(name = "PARENTADDRESS")
    private String parentAddress;
   
    @Column(name = "DEVICEADDRESS")
    private String deviceAddress;
   
    
    @Transient
    private ArrayList<Sensor> sensors;
    
    @Column(name = "ISLOWBATTERY")
    private boolean isLowBattery;


	@Column(name = "INPUTDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date inputDate;
	
	@Column(name = "READERID")
	private int readerId; 
	
	@Column(name = "READERIDS")
	private String readerIds; 
	
    @Column(name = "TAGID")
	private int tagId; 
	
    @Column(name = "TIMEDIFFINMS")
    private Long timeDiffInMs;
    
    @Transient
    private String timeDiffInMsStr;

    @Transient
    private String maxTimeDiffInMsStr;
    
    @Column(name = "PACKETCOUNTER")
    private int packetCounter;
    
    @Column(name = "RSSI")
    private double rssi;
 
    @Column(name = "SNR")
    private Double snr;
    
    @Column(name = "RSSISTR")
    private String rssiStr;
    
    @Column(name = "READERRSSI")
    private String readerRssi;

    @Column(name = "READERNAMERSSI")
    private String readerNameRssi;
    
    @Column(name = "READERRSSISNR")
    private String readerRssiSnr;

    @Column(name = "READERNAMERSSISNR")
    private String readerNameRssiSnr;

    
    @Column(name = "MAXSTEP")
    private Integer maxStep;

    @Column(name = "TIMESLOT")
    private Integer timeSlot;
    
    @Column(name = "USERDEFINE")
    private String userDefine;
    
    @Column(name = "ADCVALUE")
    private Integer adcValue;
    
	@Column(name = "CREATEDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
    @Transient
	private RawHexStringDeviceData rawHexStringDeviceData = null;
    
    public int getMeasurePeriod() {
        return measurePeriod;
    }

    public void setMeasurePeriod(int measurePeriod) {
        this.measurePeriod = measurePeriod;
    }

    public boolean getIsLowBattery() {
        return isLowBattery;
    }
    public void setIsLowBattery(boolean lowBattery) {
        this.isLowBattery = lowBattery;
    }

    public int getNumberOfSensor() {
        return numberOfSensor;
    }

    public void setNumberOfSensor(int numberOfSensor) {
        this.numberOfSensor = numberOfSensor;
    }

    public DeviceData() {
        sensors = new ArrayList<Sensor>();
    }

    public ArrayList<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(ArrayList<Sensor> sensors) {
        this.sensors = sensors;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getParentAddress() {
        return parentAddress;
    }

    public void setParentAddress(String parentAddress) {
        this.parentAddress = parentAddress;
    }

    @Override
    public String toString() {
        return "DeviceData{" + "numberOfSensor=" + numberOfSensor + ", measurePeriod=" + measurePeriod + ", deviceId=" + deviceId + ", parentAddress=" + parentAddress + ", sensors=" + sensors + ", lowBattery=" + isLowBattery + '}';
    }


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public Date getInputDate() {
		return inputDate;
	}


	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	public String getTimeDiffInMsStr() {
		return timeDiffInMsStr;
	}

	public void setTimeDiffInMsStr(String timeDiffInMs) {
		this.timeDiffInMsStr = timeDiffInMs;
	}

	public Long getTimeDiffInMs() {
		return timeDiffInMs;
	}

	public void setTimeDiffInMs(Long timeDiffInMs) {
		this.timeDiffInMs = timeDiffInMs;
	}

	public String getMaxTimeDiffInMsStr() {
		return maxTimeDiffInMsStr;
	}

	public void setMaxTimeDiffInMsStr(String maxTimeDiffInMsStr) {
		this.maxTimeDiffInMsStr = maxTimeDiffInMsStr;
	}

	public int getPacketCounter() {
		return packetCounter;
	}

	public void setPacketCounter(int packetCounter) {
		this.packetCounter = packetCounter;
	}

	public double getRssi() {
		return rssi;
	}

	public void setRssi(double rssi) {
		this.rssi = rssi;
	}

	public int getReaderId() {
		return readerId;
	}

	public void setReaderId(int readerId) {
		this.readerId = readerId;
	}

	public RawHexStringDeviceData getRawHexStringDeviceData() {
		return rawHexStringDeviceData;
	}

	public void setRawHexStringDeviceData(RawHexStringDeviceData rawHexStringDeviceData) {
		this.rawHexStringDeviceData = rawHexStringDeviceData;
	}

	public Integer getMaxStep() {
		return maxStep;
	}

	public void setMaxStep(Integer maxStep) {
		this.maxStep = maxStep;
	}

	public Integer getAdcValue() {
		return adcValue;
	}

	public void setAdcValue(Integer adcValue) {
		this.adcValue = adcValue;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getReaderIds() {
		return readerIds;
	}

	public void setReaderIds(String readerIds) {
		this.readerIds = readerIds;
	}

	public String getRssiStr() {
		return rssiStr;
	}

	public void setRssiStr(String rssiStr) {
		this.rssiStr = rssiStr;
	}

	public String getReaderRssi() {
		return readerRssi;
	}

	public void setReaderRssi(String readerRssi) {
		this.readerRssi = readerRssi;
	}

	public Double getSnr() {
		return snr;
	}

	public void setSnr(Double snr) {
		this.snr = snr;
	}

	public String getReaderRssiSnr() {
		return readerRssiSnr;
	}

	public void setReaderRssiSnr(String readerRssiSnr) {
		this.readerRssiSnr = readerRssiSnr;
	}

	public String getUserDefine() {
		return userDefine;
	}

	public void setUserDefine(String userDefine) {
		this.userDefine = userDefine;
	}

	public Integer getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(Integer timeSlot) {
		this.timeSlot = timeSlot;
	}

	public String getReaderNameRssi() {
		return readerNameRssi;
	}

	public void setReaderNameRssi(String readerNameRssi) {
		this.readerNameRssi = readerNameRssi;
	}

	public String getReaderNameRssiSnr() {
		return readerNameRssiSnr;
	}

	public void setReaderNameRssiSnr(String readerNameRssiSnr) {
		this.readerNameRssiSnr = readerNameRssiSnr;
	}
}
