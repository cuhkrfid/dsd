package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SENSOR", indexes = { @Index(columnList = "inputDate", name = "devicedata_inputdate_idx")})
public class Sensor implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2473328642977315838L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;

    public static final int LENGTH_SENSOR_INDICATION = 1;
    public static final int LENGTH_RESERVED = 1;
	public static final int LENGTH_SENSOR_ID = 2;

	
    public static final int LENGTH_H2S = 4;
    public static final int LENGTH_SO2 = 4;
    public static final int LENGTH_CH4 = 4;
    public static final int LENGTH_TOP_ULTRASONIC = 2;
    public static final int LENGTH_BOTTOM_ULTRASONIC = 2;
    
    @Column(name = "READERID")
    private int readerId;
    
    @Column(name = "DEVICEID")
    private int deviceId;
    
    @Column(name = "SENSORID")
    private int sensorId;

    @Column(name = "COVERLEVEL")
    private int coverLevel;
    
    @Column(name = "WATERLEVEL")
    private int waterLevel;
    
    @Column(name = "H2S")
    private double h2s;
    
    @Column(name = "SO2")
    private double so2;
  
    @Column(name = "CO2")
    private double co2;
    
    @Column(name = "CH4")
    private double ch4;

    @Column(name = "NH3")        
    private double nh3;
    
    @Column(name = "O2")
    private double o2;
    
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "DEVICEDATA_ID", nullable = false, updatable = false, insertable = false, referencedColumnName = "ID")
	private DeviceData deviceData;
	
	@Column(name = "DEVICEDATA_ID", nullable = true)
	private Long deviceDataId;
	
	@Column(name = "INPUTDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date inputDate;
	
    @Column(name = "ISCOVERLEVEL")
    private boolean isCoverLevel;
    
    @Column(name = "ISWATERLEVEL")
    private boolean isWaterLevel;
    
	@Column(name = "ISCO2")
	private boolean isCo2;

	@Column(name = "ISH2S")
	private boolean isH2s;
	
	@Column(name = "ISSO2")
	private boolean isSo2; 
	
	@Column(name = "ISCH4")
	private boolean isCh4;
	
	@Column(name = "ISNH3")
	private boolean isNh3;
	
	@Column(name = "ISO2")
	private boolean isO2;
	
	@Column(name = "RESERVED")
    private int reserved;
    
    @Column(name = "INDIC")
    private int indic;
    
    @Column(name = "ANTENNAOPTIMIZE")
    private Integer antennaOptimized;
 
    @Column(name = "ANTENNAOPTIMIZESTR")
    private String antennaOptimizedStr;
    
    @Column(name = "WATERLEVEL2DETECT")
    private Integer waterLevel2Detect;

    @Column(name = "WATERLEVEL2DETECTSTR")
    private String waterLevel2DetectStr;
    
    @Column(name = "WATERLEVEL1DETECT")
    private Integer waterLevel1Detect;

    @Column(name = "WATERLEVEL1DETECTSTR")
    private String waterLevel1DetectStr;
    
    @Column(name = "COVERDETECT")
    private Integer coverDetect;

    @Column(name = "COVERDETECTSTR")
    private String coverDetectStr;
    
    @Column(name = "ISLOWBATTERY")
    private boolean isLowBattery;
    
    @Column(name = "BATTERY")
    private String battery; 
    
    //DeviceData specific values
    @Column(name = "TAGID")
	private Integer tagId; 
    
    @Column(name = "PACKETCOUNTER")
    private Integer packetCounter;
    
    @Column(name = "RSSI")
    private double rssi;
    
    @Column(name = "MAXSTEP")
    private Integer maxStep;
   
    @Column(name = "ADCVALUE")
    private Integer adcValue;
    //
	@Column(name = "CREATEDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }
    
    public int getSensorId() {
        return sensorId;
    }

	public int getCoverLevel() {
		return coverLevel;
	}

	public void setCoverLevel(int coverLevel) {
		this.coverLevel = coverLevel;
		this.setIsCoverLevel(true);
	}

	public int getWaterLevel() {
		return waterLevel;
	}

	public void setWaterLevel(int waterLevel) {
		this.waterLevel = waterLevel;
		this.setIsWaterLevel(true);
	}
	
    public double getO2() {
        return o2;
    }

    public void setO2(double o2) {
        this.o2 = o2;
        this.setIsO2(true);
    }

    public double getH2s() {
        return h2s;
    }

    public void setH2s(double h2s) {
        this.h2s = h2s;
        this.isH2s = true;
    }

    public double getSo2() {
        return so2;
    }

    public void setSo2(double so2) {
        this.so2 = so2;
        this.setIsSo2(true);
    }

    
    public double getCo2() {
        return this.co2;
    }

    public void setCo2(double co2) {
        this.co2 = co2;
        this.isCo2 = true;
    }

    public double getCh4() {
        return ch4;
    }

    public void setCh4(double ch4) {
        this.ch4 = ch4;
        this.isCh4 = true;
    }

    public double getNh3() {
        return nh3;
    }

    public void setNh3(double nh3) {
        this.nh3 = nh3;
        this.setIsNh3(true);
    }
	
    @Override
    public String toString() {
        return "Sensor{" + "sensorId=" + sensorId + ", h2s=" + h2s + ", so2=" + so2 + ", co2=" + co2 + ", ch4=" + ch4 + ", nh3=" + nh3 + ", o2=" + o2 + '}';
    }


	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public DeviceData getDeviceData() {
		return deviceData;
	}

	public void setDeviceData(DeviceData deviceData) {
		this.deviceData = deviceData;
	}

	public Long getDeviceDataId() {
		return deviceDataId;
	}

	public void setDeviceDataId(Long deviceDataId) {
		this.deviceDataId = deviceDataId;
	}


	public Date getInputDate() {
		return inputDate;
	}


	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public boolean getIsCo2() {
		return isCo2;
	}

	public void setIsCo2(boolean isCo2) {
		this.isCo2 = isCo2;
	}

	public boolean getIsH2s() {
		return isH2s;
	}

	public void setIsH2s(boolean isH2s) {
		this.isH2s = isH2s;
	}

	public boolean getIsSo2() {
		return isSo2;
	}

	public void setIsSo2(boolean isSo2) {
		this.isSo2 = isSo2;
	}

	public boolean getIsCh4() {
		return isCh4;
	}

	public void setIsCh4(boolean isCh4) {
		this.isCh4 = isCh4;
	}

	public boolean getIsNh3() {
		return isNh3;
	}
	public void setIsNh3(boolean isNh3) {
		this.isNh3 = isNh3;
	}

	public boolean getIsO2() {
		return isO2;
	}

	public void setIsO2(boolean isO2) {
		this.isO2 = isO2;
	}

	public boolean getIsCoverLevel() {
		return isCoverLevel;
	}

	public void setIsCoverLevel(boolean isCoverLevel) {
		this.isCoverLevel = isCoverLevel;
	}

	public boolean getIsWaterLevel() {
		return isWaterLevel;
	}

	public void setIsWaterLevel(boolean isWaterLevel) {
		this.isWaterLevel = isWaterLevel;
	}

    public int getIndic() {
        return indic;
    }

    public void setIndic(int indic) {
        this.indic = indic;
    }

    public boolean getIsLowBattery() {
		return isLowBattery;
	}

    public void setIsLowBattery(boolean isLowBattery) {
		this.isLowBattery = isLowBattery;
		this.setBattery(this.isLowBattery  ? "Low" : "High");
	}

	public double getRssi() {
		return rssi;
	}

	public void setRssi(double rssi) {
		this.rssi = rssi;
	}

	public int getReaderId() {
		return readerId;
	}

	public void setReaderId(int readerId) {
		this.readerId = readerId;
	}

	public int getReserved() {
		return reserved;
	}

	public void setReserved(int reserved) {
		this.reserved = reserved;
	}

	public Integer getAntennaOptimized() {
		return antennaOptimized;
	}

	public void setAntennaOptimized(int antennaOptimized) {
		this.antennaOptimized = antennaOptimized;
		if (this.antennaOptimized > 0) {
			this.setAntennaOptimizedStr("Yes");
		} else {
			this.setAntennaOptimizedStr("No");
		}
	}

	public String getAntennaOptimizedStr() {
		return antennaOptimizedStr;
	}

	public void setAntennaOptimizedStr(String antennaOptimizeStr) {
		this.antennaOptimizedStr = antennaOptimizeStr;
	}

	public Integer getWaterLevel2Detect() {
		return waterLevel2Detect;
	}

	public void setWaterLevel2Detect(int waterLevel2Detect) {
		this.waterLevel2Detect = waterLevel2Detect;
		
		if (this.waterLevel2Detect > 0) {
			this.setWaterLevel2DetectStr("High");
		} else {
			this.setWaterLevel2DetectStr("Low");
		}
	}

	public String getWaterLevel2DetectStr() {
		return waterLevel2DetectStr;
	}

	public void setWaterLevel2DetectStr(String waterLevel2DetectStr) {
		this.waterLevel2DetectStr = waterLevel2DetectStr;
	}

	public Integer getWaterLevel1Detect() {
		return waterLevel1Detect;
	}

	public void setWaterLevel1Detect(int waterLevel1Detect) {
		this.waterLevel1Detect = waterLevel1Detect;
		
		if (this.waterLevel1Detect > 0) {
			this.setWaterLevel1DetectStr("High");
		} else {
			this.setWaterLevel1DetectStr("Low");
		}
		
	}

	public String getWaterLevel1DetectStr() {
		return waterLevel1DetectStr;
	}

	public void setWaterLevel1DetectStr(String waterLevel1DetectStr) {
		this.waterLevel1DetectStr = waterLevel1DetectStr;
	}

	public Integer getCoverDetect() {
		return coverDetect;
	}

	public void setCoverDetect(int coverDetect) {
		this.coverDetect = coverDetect;
		
		if (this.coverDetect > 0) {
			this.setCoverDetectStr("Closed");
		} else {
			this.setCoverDetectStr("Open");
		}
	}

	public String getCoverDetectStr() {
		return coverDetectStr;
	}

	public void setCoverDetectStr(String coverDetectStr) {
		this.coverDetectStr = coverDetectStr;
	}

	public String getBattery() {
		return battery;
	}

	public void setBattery(String battery) {
		this.battery = battery;
	}

	public Integer getTagId() {
		return tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	public Integer getPacketCounter() {
		return packetCounter;
	}

	public void setPacketCounter(Integer packetCounter) {
		this.packetCounter = packetCounter;
	}

	public Integer getMaxStep() {
		return maxStep;
	}

	public void setMaxStep(Integer maxStep) {
		this.maxStep = maxStep;
	}

	public Integer getAdcValue() {
		return adcValue;
	}

	public void setAdcValue(Integer adcValue) {
		this.adcValue = adcValue;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
