package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "OPEX")
@Audited

public class Opex extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3635427520842990622L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private int id;
	
	@Column(name = "MAIN_CATEGORY_CODE", length = 255)
	private String mainCategoryCode;
	
	@Column(name = "MAIN_CATEGORY_NAME", length = 255)
	private String mainCategoryName;
	
	@Column(name = "CATEGORY_CODE", length = 255)
	private String categoryCode;	

	@Column(name = "CATEGORY_NAME", length = 255)
	private String categoryName;	

	@Column(name = "SUB_CATEGORY_CODE", length = 255)
	private String subCategoryCode;	
	
	@Column(name = "SUB_CATEGORY_NAME", length = 255)
	private String subCategoryName;	
	
	@Column(name = "REMARK", length = 255)
	private String remark;	
	
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getId();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getMainCategoryCode() {
		return mainCategoryCode;
	}

	public void setMainCategoryCode(String mainCategoryCode) {
		this.mainCategoryCode = mainCategoryCode;
	}

	public String getMainCategoryName() {
		return mainCategoryName;
	}

	public void setMainCategoryName(String mainCategoryName) {
		this.mainCategoryName = mainCategoryName;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSubCategoryCode() {
		return subCategoryCode;
	}

	public void setSubCategoryCode(String subCategoryCode) {
		this.subCategoryCode = subCategoryCode;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
