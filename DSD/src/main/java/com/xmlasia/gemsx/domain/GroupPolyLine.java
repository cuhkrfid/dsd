package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GROUPPOLYLINE")
public class GroupPolyLine implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4778565420234548075L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "LINENO")
	private long lineNo; 
	
	@Column(name = "GROUPTAGCONFIGURATIONID")
	private long groupTagConfigurationId;
	
	@Column(name = "SITECONFIGURATIONID")
	private long siteConfigurationId; 
	
    @Column(name = "CREATEDATETIME")    
    private Date createDateTime;

    @Column(name = "LAT")   
    private double lat; 
    
    @Column(name = "LNG")   
    private double lng;
    
    @Column(name = "TAGCONFIGURATIONIDSSTR")   
    private String tagConfigurationIdsStr; 
    
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getGroupTagConfigurationId() {
		return groupTagConfigurationId;
	}

	public void setGroupTagConfigurationId(long groupTagConfigurationId) {
		this.groupTagConfigurationId = groupTagConfigurationId;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public long getLineNo() {
		return lineNo;
	}

	public void setLineNo(long lineNo) {
		this.lineNo = lineNo;
	}

	public long getSiteConfigurationId() {
		return siteConfigurationId;
	}

	public void setSiteConfigurationId(long siteConfigurationId) {
		this.siteConfigurationId = siteConfigurationId;
	}

	public String getTagConfigurationIdsStr() {
		return tagConfigurationIdsStr;
	}

	public void setTagConfigurationIdsStr(String tagConfigurationIdsStr) {
		this.tagConfigurationIdsStr = tagConfigurationIdsStr;
	} 
}
