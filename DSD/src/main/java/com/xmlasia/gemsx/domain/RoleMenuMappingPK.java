package com.xmlasia.gemsx.domain;

import java.io.Serializable;

public class RoleMenuMappingPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4759007763249802462L;

	private int roleId;

	private long menuId;
	
	public RoleMenuMappingPK(){
		super();
	}
	
	public RoleMenuMappingPK(int roleId, long menuId){
		super();
		this.roleId = roleId;
		this.menuId = menuId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public long getMenuId() {
		return menuId;
	}

	public void setMenuId(long menuId) {
		this.menuId = menuId;
	}

}