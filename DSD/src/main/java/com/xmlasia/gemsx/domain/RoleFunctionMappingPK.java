package com.xmlasia.gemsx.domain;

import java.io.Serializable;

public class RoleFunctionMappingPK implements Serializable {

	private static final long serialVersionUID = -1855288800351741906L;

	private int roleId;

	private int functionId;
	
	public RoleFunctionMappingPK(){
		super();
	}
	
	public RoleFunctionMappingPK(int roleId, int functionId){
		super();
		this.roleId = roleId;
		this.functionId = functionId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getFunctionId() {
		return functionId;
	}

	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}
	
}