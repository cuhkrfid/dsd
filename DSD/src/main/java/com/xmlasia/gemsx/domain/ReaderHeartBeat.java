package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "READERHEARTBEAT", indexes = { @Index(columnList = "inputDate", name = "readerheartbeat_inputdate_idx")})
public class ReaderHeartBeat  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1022414371495441738L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "INPUTDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date inputDate;
	
	@Column(name = "READERID")
	private int readerId; 

	@Column(name = "READERNAME")
	private String readerName;
	
    @Column(name = "PACKETCOUNTER")
    private int packetCounter;
    
    @Column(name = "RSSI")
    private double rssi;
    
	@Column(name = "CREATEDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Transient
	private String dateString2 = "";
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getPacketCounter() {
		return packetCounter;
	}

	public void setPacketCounter(int packetCounter) {
		this.packetCounter = packetCounter;
	}

	public double getRssi() {
		return rssi;
	}

	public void setRssi(double rssi) {
		this.rssi = rssi;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getInputDate() {
		return inputDate;
	}

	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}

	public int getReaderId() {
		return readerId;
	}

	public void setReaderId(int readerId) {
		this.readerId = readerId;
	}

	public String getDateString2() {
		return dateString2;
	}

	public void setDateString2(String dateString2) {
		this.dateString2 = dateString2;
	}

	public String getReaderName() {
		return readerName;
	}

	public void setReaderName(String readerName) {
		this.readerName = readerName;
	}
}
