package com.xmlasia.gemsx.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "TAGCONFIGURATION")
@Audited
public class TagConfiguration extends Validation {

	/**
	 * 
	 */
	private static final long serialVersionUID = -870154457770412630L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	  
	@Column(name = "MEASUREMENT_PERIOD")
	private int measurePeriod;
	
	@Column(name = "VIBRATION_THRESHOLD")
	private Integer vibrationThreshold;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "SITECONFIGURATION_ID", nullable = false, updatable = false, insertable = false, referencedColumnName = "ID")
	private SiteConfiguration siteConfiguration;
	
	@Transient
	private SensorConfig[] sensorConfigs; 
	
	@Transient
	private List<SensorConfig> sensorConfigsList;
	
	
	@Column(name = "SITECONFIGURATION_ID", nullable = true)
	private Long siteConfigurationId;
	
	@Column(name = "NAME", length = 255)
	private String name;
	
	@Column(name = "DESCRIPTION", length = 255)
	private String description;
	
	@Column(name = "DEVICE_ID")
	private int deviceId;

	@Transient
    private String[] sensorIds;
 
    @Column(name = "GROUP_ID")
    private int groupId = 0;
    
	@Column(name = "LAT")
	private double lat;
	
	@Column(name = "LON")
	private double lon;

	@Column(name = "ZOOMLEVEL")
	private int zoomLevel;
	
	@Column(name = "DEVICEADDRESS")
	private String deviceAddress;
	
	@Column(name = "IMAGEURLPATH")
	private String imageUrlPath;
	
	@Column(name = "IMAGEFILENAME")
	private String imageFileName;

	@Column(name = "CREATOR")
	private String creator; 

	@Column(name = "VALIDATETAGIMAGEPATH")
	private String validateTagImagePath = "img/normal/help.png";
	
	@Column(name = "VALIDATIONRESULT")
	private Boolean validationResult;
	
	@Column(name = "READERINSEQUENCE")
	private Boolean readerInSequence;
	
	@Column(name = "READERINSEQUENCEINMS")
	private Integer readerInSequenceInMs;
	
	@Column(name = "READERWITHHIGHESTRSSI")
	private Boolean readerWithHighestRssi; 
	
	@Column(name = "READERWITHHIGHESTRSSIINMS")
    private Integer readerWithHighestRssiInMs; 
    
	@Column(name = "ASSIGNTOSPECIFICREADERS")
	private Boolean assignToSpecificReaders;
	
	@Column(name = "ASSIGNTOSPECIFICREADERSINMS")
	private Integer assignToSpecificReadersInMs; 
	
	@Column(name = "MANHOLEID")
	private String manHoleId; 
	
	@Column(name = "MANHOLEHEIGHT")
	private Integer manHoleHeight;
	
	@Column(name = "CREATEDATETIME")
	private String createDateTime;
	
	@Transient
	private String[] sensorTypeHtml;
	
	@Transient
	private boolean isCoverLevel;

	@Transient
	private  boolean isWaterLevel;
	
	@Transient
	private  boolean isH2s;
	
	@Transient		
	private boolean isSo2;
	
	@Transient		
	private boolean isCo2;

	@Transient		
	private boolean isCh4;
	
	@Transient		
	private boolean isNh3;
	
	@Transient		
	private boolean isO2;
	
	@Transient
	private List<ValidationMessage> validationMessageObjects = new ArrayList<ValidationMessage>();;
	
	@Transient
	private long tempSensorId; 
	
	@Transient
	private List<TagConfigurationToReader> tagConfigurationToReader = new ArrayList<TagConfigurationToReader>();
	
	@Transient
	private List<Alert> alerts = new ArrayList<Alert>();
	
	@Transient
	private String imageUri;
	
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getId();
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public Integer getVibrationThreshold() {
		return vibrationThreshold;
	}
	
	public void setVibrationThreshold(Integer vibrationThreshold) {
		this.vibrationThreshold = vibrationThreshold;
	}

	public Long getSiteConfigurationId() {
		return siteConfigurationId;
	}

	public void setSiteConfigurationId(Long siteConfigurationId) {
		this.siteConfigurationId = siteConfigurationId;
	}

	public SiteConfiguration getSiteConfiguration() {
		return siteConfiguration;
	}

	public void setSiteConfiguration(SiteConfiguration siteConfiguration) {
		this.siteConfiguration = siteConfiguration;
	}
	
	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}
	
	public void setLon(double lon) {
		this.lon = lon;
	}
	
	public String[] getSensorTypeHtml() {
		return sensorTypeHtml;
	}

	public void setSensorTypeHtml(String[] sensorTypeHtml) {
		this.sensorTypeHtml = sensorTypeHtml;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	public String[] getSensorIds() {
		return sensorIds;
	}

	public void setSensorIds(String[] sensorIds) {
		this.sensorIds = sensorIds;
	}

	public SensorConfig[] getSensorConfigs() {
		return sensorConfigs;
	}

	public void setSensorConfigs(SensorConfig[] sensorConfigs) {
		this.sensorConfigs = sensorConfigs;
	}

	public List<SensorConfig> getSensorConfigsList() {
		return sensorConfigsList;
	}

	public void setSensorConfigsList(List<SensorConfig> sensorConfigsList) {
		this.sensorConfigsList = sensorConfigsList;
	}

	public boolean getIsCoverLevel() {
		return isCoverLevel;
	}
	public void setIsCoverLevel(boolean isCoverLevel) {
		this.isCoverLevel = isCoverLevel;
	}

	public boolean getIsWaterLevel() {
		return isWaterLevel;
	}
	public void setIsWaterLevel(boolean isWaterLevel) {
		this.isWaterLevel = isWaterLevel;
	}

	public boolean getIsH2s() {
		return isH2s;
	}
	public void setIsH2s(boolean isH2s) {
		this.isH2s = isH2s;
	}

	public boolean getIsSo2() {
		return isSo2;
	}
	public void setIsSo2(boolean isSo2) {
		this.isSo2 = isSo2;
	}

	public boolean getIsCo2() {
		return isCo2;
	}
	public void setIsCo2(boolean isCo2) {
		this.isCo2 = isCo2;
	}

	public boolean getIsCh4() {
		return isCh4;
	}
	public void setIsCh4(boolean isCh4) {
		this.isCh4 = isCh4;
	}

	public boolean getIsNh3() {
		return isNh3;
	}
	public void setIsNh3(boolean isNh3) {
		this.isNh3 = isNh3;
	}

	public boolean getIsO2() {
		return isO2;
	}
	public void setIsO2(boolean isO2) {
		this.isO2 = isO2;
	}
	
	public String getImageUrlPath() {
		return imageUrlPath;
	}

	public void setImageUrlPath(String imageUrlPath) {
		this.imageUrlPath = imageUrlPath;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public int getMeasurePeriod() {
		return measurePeriod;
	}

	public void setMeasurePeriod(int measurePeriod) {
		this.measurePeriod = measurePeriod;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getValidateTagImagePath() {
		return validateTagImagePath;
	}

	public void setValidateTagImagePath(String validateTagImagePath) {
		this.validateTagImagePath = validateTagImagePath;
	}

	public Boolean getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(Boolean validationResult) {
		this.validationResult = validationResult;
		if (!validationResult)
			setValidateTagImagePath("img/basic/Cancel-48.png");
		else 
			setValidateTagImagePath("img/basic/Checked-48.png");
	}

	public List<ValidationMessage> getValidationMessageObjects() {
		return validationMessageObjects;
	}

	public void setValidationMessageObjects(List<ValidationMessage> validationMessageObjects) {
		this.validationMessageObjects = validationMessageObjects;
	}

	public int getZoomLevel() {
		return zoomLevel;
	}

	public void setZoomLevel(int zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

	public long getTempSensorId() {
		return tempSensorId;
	}

	public void setTempSensorId(long tempSensorId) {
		this.tempSensorId = tempSensorId;
	}

	public List<TagConfigurationToReader> getTagConfigurationToReader() {
		return tagConfigurationToReader;
	}

	public void setTagConfigurationToReader(List<TagConfigurationToReader> tagConfigurationToReader) {
		this.tagConfigurationToReader = tagConfigurationToReader;
	}

	public Boolean getReaderInSequence() {
		return readerInSequence;
	}

	public void setReaderInSequence(Boolean readerInSequence) {
		this.readerInSequence = readerInSequence;
	}

	public Integer getReaderInSequenceInMs() {
		return readerInSequenceInMs;
	}

	public void setReaderInSequenceInMs(Integer readerInSequenceInMs) {
		this.readerInSequenceInMs = readerInSequenceInMs;
	}

	public Boolean getReaderWithHighestRssi() {
		return readerWithHighestRssi;
	}

	public void setReaderWithHighestRssi(Boolean readerWithHighestRssi) {
		this.readerWithHighestRssi = readerWithHighestRssi;
	}

	public Integer getReaderWithHighestRssiInMs() {
		return readerWithHighestRssiInMs;
	}

	public void setReaderWithHighestRssiInMs(Integer readerWithHighestRssiInMs) {
		this.readerWithHighestRssiInMs = readerWithHighestRssiInMs;
	}

	public Boolean getAssignToSpecificReaders() {
		return assignToSpecificReaders;
	}

	public void setAssignToSpecificReaders(Boolean assignToSpecificReaders) {
		this.assignToSpecificReaders = assignToSpecificReaders;
	}

	public Integer getAssignToSpecificReadersInMs() {
		return assignToSpecificReadersInMs;
	}

	public void setAssignToSpecificReadersInMs(Integer assignToSpecificReadersInMs) {
		this.assignToSpecificReadersInMs = assignToSpecificReadersInMs;
	}

	public List<Alert> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<Alert> alerts) {
		this.alerts = alerts;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	public String getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(String createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Integer getManHoleHeight() {
		return manHoleHeight;
	}

	public void setManHoleHeight(Integer manHoleHeight) {
		this.manHoleHeight = manHoleHeight;
	}

	public String getManHoleId() {
		return manHoleId;
	}

	public void setManHoleId(String manHoleId) {
		this.manHoleId = manHoleId;
	}


}
