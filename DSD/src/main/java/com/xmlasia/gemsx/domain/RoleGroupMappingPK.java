package com.xmlasia.gemsx.domain;

import java.io.Serializable;

public class RoleGroupMappingPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -530829848289014338L;

	private int roleId;

	private int groupId;
	
	public RoleGroupMappingPK(){
		super();
	}
	
	public RoleGroupMappingPK(int roleId, int groupId){
		super();
		this.roleId = roleId;
		this.groupId = groupId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	
}