package com.xmlasia.gemsx.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
@Entity
@Table(name = "USER_PROFILE")
@Audited
public class UserProfile extends BaseDomain {

	private static final long serialVersionUID = -4212573581689778829L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	
	@Column(name = "LOCALE", length = 10)
	private String locale;
	
	
	@Override
	public Object getKey() {
		// TODO Auto-generated method stub
		return getId();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

}
