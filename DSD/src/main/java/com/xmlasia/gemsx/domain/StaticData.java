package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "STATIC_DATA")
@Audited
public class StaticData extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565873292246413977L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private int id;
	
	@Column(name = "DATA_NAME", nullable = false)
	private String dataName;
	
	@Column(name = "CONTENT_CHI", length = 255)
	private String contentChi;
	
	@Column(name = "CONTENT_ENG", length = 255)
	private String contentEng;

	@Override
	public Object getKey() {
		return id; 
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDataName() {
		return dataName;
	}

	public void setDataName(String dataName) {
		this.dataName = dataName;
	}

	public String getContentChi() {
		return contentChi;
	}

	public void setContentChi(String contentChi) {
		this.contentChi = contentChi;
	}

	public String getContentEng() {
		return contentEng;
	}

	public void setContentEng(String contentEng) {
		this.contentEng = contentEng;
	}

}