package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ROUTERDATA")
public class RouterData implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4370049184990177302L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	public static final int LENGTH_ROUTERID = 2;
    public static final int LENGTH_PARENTADDRESS = 2;
    
    @Column(name = "ROUTERID")    
    private int routerId;
    
    @Column(name = "PARENTADDRESS")       
    private String parentAddress;
    
    @Column(name = "UPDATEDTIME")       
    private Date updatedTime;
    
    public int getRouterId() {
        return routerId;
    }

    public void setRouterId(int routerId) {
        this.routerId = routerId;
    }

    public String getParentAddress() {
        return parentAddress;
    }

    public void setParentAddress(String ParentAddress) {
        this.parentAddress = ParentAddress;
    }

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


    @Override
    public String toString() {
        return "RouterData{" + "routerId=" + routerId + ", ParentAddress=" + parentAddress + '}';
    }
}
