package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "USER_ROLE_MAPPINGS")
@IdClass(UserRoleMappingPK.class)
@Audited
public class UserRoleMapping extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565873292246413977L;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="USER_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private User user;
	
	@Column(name = "USER_ID", nullable = false, length=10)
	@Id
	private int userId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ROLE_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private Role role;
	
	@Id
	@Column(name = "ROLE_ID", nullable = false)
	private int roleId;
	
	public UserRoleMapping(){
		super();
	}
	
	public UserRoleMapping(int userId, int roleId){
		super();
		this.userId = userId;
		this.roleId = roleId;
	}

	@Override
	public UserRoleMappingPK getKey() {
		return new UserRoleMappingPK(userId, roleId);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

}