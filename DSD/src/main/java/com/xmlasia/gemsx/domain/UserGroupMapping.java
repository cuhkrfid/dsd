package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "USER_GROUP_MAPPINGS")
@IdClass(UserGroupMappingPK.class)
@Audited
public class UserGroupMapping extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565873292246413977L;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="USER_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private User user;
	
	@Column(name = "USER_ID", nullable = false, length=10)
	@Id
	private int userId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="GROUP_ID", referencedColumnName="ID", updatable=false, insertable=false)
	private Group group;
	
	@Id
	@Column(name = "GROUP_ID", nullable = false)
	private int groupId;
	
	public UserGroupMapping(){
		super();
	}
	
	public UserGroupMapping(int userId, int groupId){
		super();
		this.userId = userId;
		this.groupId = groupId;
	}
	
	@Override
	public UserGroupMappingPK getKey() {
		return new UserGroupMappingPK(userId, groupId);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

}