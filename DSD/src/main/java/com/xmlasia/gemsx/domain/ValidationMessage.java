package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "VALIDATIONMESSAGE")
public class ValidationMessage implements Serializable {

	private static final long serialVersionUID = 3755084600035928306L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	
	@Column(name = "MESSAGETYPE")
	private String messageType;
	
	@Column(name = "DEVICEID")
	private Long deviceId;
	
	@Column(name = "SENSORID")
	private Long sensorId;
	
	@Column(name = "ISCOVERLEVEL")
	private boolean isCoverLevel;

	@Column(name = "ISWATERLEVEL")
	private boolean isWaterLevel;
	
	@Column(name = "ISCO2")
	private boolean isCo2; 
	
	@Column(name = "ISH2S")
	private boolean isH2s;
	
	@Column(name = "ISSO2")
	private boolean isSo2;
	
	@Column(name = "ISCH4")
	private boolean isCh4;
	
	@Column(name = "ISNH3")
	private boolean isNh3;
	
	@Column(name = "ISO2")
	private boolean isO2;
	
	@Column(name = "MESSAGE")
	private String message;

	@Column(name = "SENSORCONFIGID")
	private long sensorConfigId;
	
	@Column(name = "TAGCONFIGURATIONID")
	private long tagConfigurationId;
	
	@Column(name = "UPDATEDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	
	@Column(name = "DEVICEACTION")
	private String deviceAction = "none";
	
	@Column(name = "SENSORACTION")
	private String sensorAction = "none";
	
	public void setDeviceIdNotFound(long deviceId, String message) {
		this.messageType = "deviceIdNotFound"; 
		this.deviceId = deviceId;
		this.message = message;
		this.deviceAction = "remove";
	}

	public void setDeviceIdTimeout(long deviceId, String message) {
		this.messageType = "deviceIdTimeout"; 
		this.deviceId = deviceId;
		this.message = message;
		this.deviceAction = "remove";
	}
	
	public void setSensorIdNotFound(long sensorId, String message, String sensorAction) {
		this.messageType = "sensorIdNotFound"; 
		this.sensorId = sensorId;
		this.message = message;
		this.sensorAction = sensorAction;
	}
	
	public void setSensorTypeNotFound(long sensorId, 
									  boolean isCoverLevel, 
									  boolean isWaterLevel,
									  boolean isCo2,
									  boolean isH2s,
									  boolean isSo2,
									  boolean isCh4,
									  boolean isNh3,
									  boolean isO2,
									  String message,
									  String sensorAction)  {
		this.messageType = "sensorTypeNotFound"; 
		this.sensorId = sensorId;
		
	    this.isCoverLevel = isCoverLevel;
	    this.isWaterLevel = isWaterLevel;
	    this.isCo2 = isCo2;
	    this.isH2s = isH2s;
	    this.isSo2 = isSo2;
	    this.isCh4 = isCh4;
	    this.isNh3 = isNh3;
	    this.isO2 = isO2;
		
		this.message = message;
		this.sensorAction = sensorAction;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Long getSensorId() {
		return sensorId;
	}

	public void setSensorId(Long sensorId) {
		this.sensorId = sensorId;
	}

	public boolean getIsCoverLevel() {
		return isCoverLevel;
	}
	public void setIsCoverLevel(boolean isCoverLevel) {
		this.isCoverLevel = isCoverLevel;
	}

	public boolean getIsWaterLevel() {
		return isWaterLevel;
	}
	public void setIsWaterLevel(boolean temperature) {
		this.isWaterLevel = temperature;
	}

	public boolean getIsCo2() {
		return isCo2;
	}
	public void setIsCo2(boolean humidity) {
		this.isCo2 = humidity;
	}

	public boolean getIsH2s() {
		return isH2s;
	}
	public void setIsH2s(boolean isH2s) {
		this.isH2s = isH2s;
	}

	public boolean getIsSo2() {
		return isSo2;
	}

	public void setIsSo2(boolean isSo2) {
		this.isSo2 = isSo2;
	}

	public boolean getIsCh4() {
		return isCh4;
	}

	public void setIsCh4(boolean isCh4) {
		this.isCh4 = isCh4;
	}

	public boolean getIsNh3() {
		return isNh3;
	}

	public void setIsNh3(boolean isNh3) {
		this.isNh3 = isNh3;
	}

	public boolean getIsO2() {
		return isO2;
	}

	public void setIsO2(boolean isO2) {
		this.isO2 = isO2;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getSensorConfigId() {
		return sensorConfigId;
	}

	public void setSensorConfigId(long sensorConfigId) {
		this.sensorConfigId = sensorConfigId;
	}

	public long getTagConfigurationId() {
		return tagConfigurationId;
	}

	public void setTagConfigurationId(long tagConfigurationId) {
		this.tagConfigurationId = tagConfigurationId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	
	public void copy(ValidationMessage other) {
		this.messageType = other.messageType;
		this.message = other.message;

	    this.isCoverLevel = other.isCoverLevel;
	    this.isWaterLevel = other.isWaterLevel;
	    this.isCo2 = other.isCo2;
	    this.isH2s = other.isH2s;
	    this.isSo2 = other.isSo2;
	    this.isCh4 = other.isCh4;
	    this.isNh3 = other.isNh3;
	    this.isO2 = other.isO2;
	}

	public String getDeviceAction() {
		return deviceAction;
	}

	public void setDeviceAction(String deviceAction) {
		this.deviceAction = deviceAction;
	}

	public String getSensorAction() {
		return sensorAction;
	}

	public void setSensorAction(String sensorAction) {
		this.sensorAction = sensorAction;
	}


}
