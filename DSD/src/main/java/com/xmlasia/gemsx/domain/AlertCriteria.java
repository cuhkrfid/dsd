package com.xmlasia.gemsx.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sun.jersey.multipart.FormDataParam;

@Entity
@Table(name = "ALERTCRITERIA")
public class AlertCriteria extends Validation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6613564074737518208L;

	public static final Integer minCoverLevel = 0; 
	public static final Integer maxCoverLevel = 200; 

	public static final Integer minWaterLevel = 0; 
	public static final Integer maxWaterLevel = 200; 

	public static final Double minH2s = -10.0; 
	public static final Double maxH2s = 100.0; 

	public static final Double minSo2 = -10.0; 
	public static final Double maxSo2 = 100.0; 

	public static final Double minCo2 = -10.0; 
	public static final Double maxCo2 = 10.0; 

	public static final Double minCh4 = -50.0; 
	public static final Double maxCh4 = 50.0; 

	public static final Double minNh3 = -50.0; 
	public static final Double maxNh3 = 50.0; 

	public static final Double minO2 = -10.0; 
	public static final Double maxO2 = 10.0; 
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;

    @Column(name = "NAME")
	private String name;
    
	@Column(name = "ISCOVERLEVEL")
    private boolean isCoverLevel; 
    @Column(name = "COVERLEVELCRITERIA")
    private String coverLevelCriteria = ""; 

    @Column(name = "ISWATERLEVEL")
    private boolean isWaterLevel; 
    @Column(name = "WATERLEVELCRITERIA")
    private String waterLevelCriteria = ""; 
    
    @Column(name = "ISH2S")
    private boolean isH2s; 
    @Column(name = "H2SCRITERIA")
    private String h2sCriteria = ""; 
    
    @Column(name = "ISSO2")
    private boolean isSo2; 
    @Column(name = "SO2CRITERIA")
    private String so2Criteria = "";     
    
    @Column(name = "ISCO2")
    private boolean isCo2; 
    @Column(name = "CO2CRITERIA")
    private String co2Criteria = "";     
    
    @Column(name = "ISCH4")
    private boolean isCh4; 
    @Column(name = "CH4CRITERIA")
    private String ch4Criteria = "";     

    @Column(name = "ISNH3")
    private boolean isNh3; 
    @Column(name = "NH3CRITERIA")
    private String nh3Criteria= ""; 
    
    @Column(name = "ISO2")
    private boolean isO2; 
    @Column(name = "O2CRITERIA")
    private String o2Criteria = ""; 
    
    @Column(name = "MAXCOVERLEVELVALUE")
    private Integer maxCoverLevelValue;
    @Column(name = "MINCOVERLEVELVALUE")
    private Integer minCoverLevelValue;
    
    @Column(name = "MINWATERLEVELVALUE")
    private Integer minWaterLevelValue; 
    @Column(name = "MAXWATERLEVELVALUE")
    private Integer maxWaterLevelValue; 
    
    @Column(name = "MAXH2SVALUE")
    private Double maxH2sValue;
    @Column(name = "MINH2SVALUE")
    private Double minH2sValue;
    
    @Column(name = "MAXSO2VALUE")
    private Double maxSo2Value;
    @Column(name = "MINSO2VALUE")
    private Double minSo2Value;

    @Column(name = "MAXCO2VALUE")
    private Double maxCo2Value;
    @Column(name = "MINCO2VALUE")
    private Double minCo2Value;

    @Column(name = "MAXCH4VALUE")
    private Double maxCh4Value;
    @Column(name = "MINCH4VALUE")
    private Double minCh4Value;

    @Column(name = "MAXNH3VALUE")
    private Double maxNh3Value;
    @Column(name = "MINNH3VALUE")
    private Double minNh3Value;

    @Column(name = "MAXO2VALUE")
    private Double maxO2Value;
    @Column(name = "MINO2VALUE")
    private Double minO2Value;
    
    @Column(name = "ISNOTCOVERLEVELBETWEEN")      
    private Boolean isNotCoverLevelBetween;    

    @Column(name = "ISNOTWATERLEVELBETWEEN")      
    private Boolean isNotWaterLevelBetween;    

    @Column(name = "ISNOTH2SBETWEEN")      
    private Boolean isNotH2sBetween;    
    
    @Column(name = "ISNOTSO2BETWEEN")      
    private Boolean isNotSo2Between;

    @Column(name = "ISNOTCO2BETWEEN")   
    private Boolean isNotCo2Between;

    @Column(name = "ISNOTCH4BETWEEN")   
    private Boolean isNotCh4Between;
    
    @Column(name = "ISNOTNH3BETWEEN")   
    private Boolean isNotNh3Between;    

    @Column(name = "ISNOTO2BETWEEN")   
    private Boolean isNotO2Between;
    
    @Column(name = "ISCOVERDETECT")
	private Boolean isCoverDetect;

    @Column(name = "COVERDETECT")
	private Boolean coverDetect;
	
    @Column(name = "COVERDETECTOPENORCLOSESTR")
	private String coverDetectOpenOrCloseStr = "--";
	
    
    @Transient
	private String[] coverLevelHtml;
	
	@Transient
	private String[] waterLevelHtml;
	
	@Transient
	private String[] h2sHtml;
	
	@Transient
	private String[] so2Html;
	
	@Transient
	private String[] co2Html;
	
	@Transient
	private String[] ch4Html;
	
	@Transient
	private String[] nh3Html;

	@Transient
	private String[] o2Html;

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public Integer getMaxCoverLevelValue() {
		return maxCoverLevelValue;
	}
	public void setMaxCoverLevelValue(Integer maxCoverLevelValue) {
		this.maxCoverLevelValue = maxCoverLevelValue;
	}

	public Integer getMinCoverLevelValue() {
		return minCoverLevelValue;
	}
	public void setMinCoverLevelValue(Integer minCoverLevelValue) {
		this.minCoverLevelValue = minCoverLevelValue;
	}
	
	public Integer getMinWaterLevelValue() {
		return minWaterLevelValue;
	}
	public void setMinWaterLevelValue(Integer minWaterLevelValue) {
		this.minWaterLevelValue = minWaterLevelValue;
	}

	public Integer getMaxWaterLevelValue() {
		return maxWaterLevelValue;
	}
	public void setMaxWaterLevelValue(Integer maxWaterLevelValue) {
		this.maxWaterLevelValue = maxWaterLevelValue;
	}

	public Double getMaxH2sValue() {
		return maxH2sValue;
	}
	public void setMaxH2sValue(Double maxH2sValue) {
		this.maxH2sValue = maxH2sValue;
	}
	
	public Double getMinH2sValue() {
		return minH2sValue;
	}
	public void setMinH2sValue(Double minH2sValue) {
		this.minH2sValue = minH2sValue;
	}

	public Double getMaxSo2Value() {
		return maxSo2Value;
	}
	public void setMaxSo2Value(Double maxSo2Value) {
		this.maxSo2Value = maxSo2Value;
	}

	public Double getMinSo2Value() {
		return minSo2Value;
	}
	public void setMinSo2Value(Double minSo2Value) {
		this.minSo2Value = minSo2Value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	} 
	
	
	public String[] getCoverLevelHtml() {
		return coverLevelHtml;
	}
	public void setCoverLevelHtml(String coverLevelHtml) {
		if ( coverLevelHtml == null || coverLevelHtml.isEmpty())
			return;
		this.coverLevelHtml = coverLevelHtml.split(";");
	}

	public String[] getWaterLevelHtml() {
		return waterLevelHtml;
	}
	public void setWaterLevelHtml(String waterLevelHtml) {
		if ( waterLevelHtml == null || waterLevelHtml.isEmpty())
			return;
		this.waterLevelHtml = waterLevelHtml.split(";");
	}

	public String[] getH2sHtml() {
		return h2sHtml;
	}
	public void setH2sHtml(String h2sHtml) {
		if ( h2sHtml == null || h2sHtml.isEmpty())
			return;
		this.h2sHtml = h2sHtml.split(";");
	}

	public String[] getSo2Html() {
		return so2Html;
	}
	public void setSo2Html(String so2Html) {
		if ( so2Html == null || so2Html.isEmpty())
			return;
		this.so2Html = so2Html.split(";");
	}

	public String[] getCo2Html() {
		return co2Html;
	}
	public void setCo2Html(String co2Html) {
		if ( co2Html == null || co2Html.isEmpty())
			return;
		this.co2Html = co2Html.split(";");
	}
	
	public String[] getCh4Html() {
		return ch4Html;
	}
	public void setCh4Html(String ch4Html) {
		if ( ch4Html == null || ch4Html.isEmpty())
			return;
		this.ch4Html = ch4Html.split(";");
	}
	
	public String[] getNh3Html() {
		return nh3Html;
	}
	public void setNh3Html(String nh3Html) {
		if ( nh3Html == null || nh3Html.isEmpty())
			return;
		this.nh3Html = nh3Html.split(";");
	}
	
	public String[] getO2Html() {
		return o2Html;
	}
	public void setO2Html(String o2Html) {
		if ( o2Html == null || o2Html.isEmpty())
			return;
		this.o2Html = o2Html.split(";");
	}	
	
	public Double getMaxCo2Value() {
		return maxCo2Value;
	}

	public void setMaxCo2Value(Double maxCo2Value) {
		this.maxCo2Value = maxCo2Value;
	}

	public Double getMinCo2Value() {
		return minCo2Value;
	}

	public void setMinCo2Value(Double minCo2Value) {
		this.minCo2Value = minCo2Value;
	}

	public Double getMaxCh4Value() {
		return maxCh4Value;
	}

	public void setMaxCh4Value(Double maxCh4Value) {
		this.maxCh4Value = maxCh4Value;
	}

	public Double getMinCh4Value() {
		return minCh4Value;
	}

	public void setMinCh4Value(Double minCh4Value) {
		this.minCh4Value = minCh4Value;
	}

	public Double getMaxNh3Value() {
		return maxNh3Value;
	}

	public void setMaxNh3Value(Double maxNh3Value) {
		this.maxNh3Value = maxNh3Value;
	}

	public Double getMinNh3Value() {
		return minNh3Value;
	}

	public void setMinNh3Value(Double minNh3Value) {
		this.minNh3Value = minNh3Value;
	}

	public Double getMaxO2Value() {
		return maxO2Value;
	}

	public void setMaxO2Value(Double maxO2Value) {
		this.maxO2Value = maxO2Value;
	}

	public Double getMinO2Value() {
		return minO2Value;
	}

	public void setMinO2Value(Double minO2Value) {
		this.minO2Value = minO2Value;
	}

	public boolean getIsCoverLevel() {
		return isCoverLevel;
	}

	public void setIsCoverLevel(boolean isCoverLevel) {
		this.isCoverLevel = isCoverLevel;
	}

	public String getCoverLevelCriteria() {
		return coverLevelCriteria;
	}

	public void setCoverLevelCriteria(String coverLevelCriteria) {
		this.coverLevelCriteria = coverLevelCriteria;
	}

	public boolean getIsWaterLevel() {
		return isWaterLevel;
	}

	public void setIsWaterLevel(boolean isWaterLevel) {
		this.isWaterLevel = isWaterLevel;
	}

	public String getWaterLevelCriteria() {
		return waterLevelCriteria;
	}

	public void setWaterLevelCriteria(String waterLevelCriteria) {
		this.waterLevelCriteria = waterLevelCriteria;
	}

	public boolean getIsSo2() {
		return isSo2;
	}
	public void setIsSo2(boolean isSo2) {
		this.isSo2 = isSo2;
	}

	public String getSo2Criteria() {
		return so2Criteria;
	}
	public void setSo2Criteria(String so2Criteria) {
		this.so2Criteria = so2Criteria;
	}

	public boolean getIsH2s() {
		return isH2s;
	}
	public void setIsH2s(boolean isH2s) {
		this.isH2s = isH2s;
	}

	public String getH2sCriteria() {
		return h2sCriteria;
	}
	public void setH2sCriteria(String h2sCriteria) {
		this.h2sCriteria = h2sCriteria;
	}

	public boolean getIsCo2() {
		return isCo2;
	}
	public void setIsCo2(boolean isCo2) {
		this.isCo2 = isCo2;
	}

	public String getCo2Criteria() {
		return co2Criteria;
	}
	public void setCo2Criteria(String co2Criteria) {
		this.co2Criteria = co2Criteria;
	}

	public boolean getIsCh4() {
		return isCh4;
	}
	public void setIsCh4(boolean isCh4) {
		this.isCh4 = isCh4;
	}

	public String getCh4Criteria() {
		return ch4Criteria;
	}
	public void setCh4Criteria(String ch4Criteria) {
		this.ch4Criteria = ch4Criteria;
	}

	public boolean getIsNh3() {
		return isNh3;
	}
	public void setIsNh3(boolean isNh3) {
		this.isNh3 = isNh3;
	}

	public String getNh3Criteria() {
		return nh3Criteria;
	}
	public void setNh3Criteria(String nh3Criteria) {
		this.nh3Criteria = nh3Criteria;
	}

	public boolean getIsO2() {
		return isO2;
	}
	public void setIsO2(boolean isO2) {
		this.isO2 = isO2;
	}

	public String getO2Criteria() {
		return o2Criteria;
	}
	public void setO2Criteria(String o2Criteria) {
		this.o2Criteria = o2Criteria;
	}

	public Boolean getIsNotCoverLevelBetween() {
		return isNotCoverLevelBetween;
	}
	public void setIsNotCoverLevelBetween(Boolean isNotCoverLevelBetween) {
		this.isNotCoverLevelBetween = isNotCoverLevelBetween;
	}
	
	public Boolean getIsNotWaterLevelBetween() {
		return isNotWaterLevelBetween;
	}
	public void setIsNotWaterLevelBetween(Boolean isNotWaterLevelBetween) {
		this.isNotWaterLevelBetween = isNotWaterLevelBetween;
	}

	public Boolean getIsNotH2sBetween() {
		return isNotH2sBetween;
	}
	public void setIsNotH2sBetween(Boolean isNotH2sBetween) {
		this.isNotH2sBetween = isNotH2sBetween;
	}

	public Boolean getIsNotSo2Between() {
		return isNotSo2Between;
	}
	public void setIsNotSo2Between(Boolean isNotSo2Between) {
		this.isNotSo2Between = isNotSo2Between;
	}

	public Boolean getIsNotCo2Between() {
		return isNotCo2Between;
	}
	public void setIsNotCo2Between(Boolean isNotCo2Between) {
		this.isNotCo2Between = isNotCo2Between;
	}

	public Boolean getIsNotCh4Between() {
		return isNotCh4Between;
	}
	public void setIsNotCh4Between(Boolean isNotCh4Between) {
		this.isNotCh4Between = isNotCh4Between;
	}

	public Boolean getIsNotNh3Between() {
		return isNotNh3Between;
	}
	public void setIsNotNh3Between(Boolean isNotNh3Between) {
		this.isNotNh3Between = isNotNh3Between;
	}

	public Boolean getIsNotO2Between() {
		return isNotO2Between;
	}
	public void setIsNotO2Between(Boolean isNotO2Between) {
		this.isNotO2Between = isNotO2Between;
	}

	public Boolean getIsCoverDetect() {
		return isCoverDetect;
	}

	public void setIsCoverDetect(Boolean isCoverDetect) {
		this.isCoverDetect = isCoverDetect;
	}

	public Boolean getCoverDetect() {
		return coverDetect;
	}

	public void setCoverDetect(Boolean coverDetect) {
		this.coverDetect = coverDetect;
	}

	public String getCoverDetectOpenOrCloseStr() {
		return coverDetectOpenOrCloseStr;
	}

	public void setCoverDetectOpenOrCloseStr(String coverDetectOpenOrCloseStr) {
		this.coverDetectOpenOrCloseStr = coverDetectOpenOrCloseStr;
	}
	
}
