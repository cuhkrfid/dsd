package com.xmlasia.gemsx.domain;

import java.io.Serializable;

public class UserRoleMappingPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8975760329187072956L;

	private int userId;

	private int roleId;
	
	public UserRoleMappingPK(){
		super();
	}
	
	public UserRoleMappingPK(int userId, int roleId){
		super();
		this.userId = userId;
		this.roleId = roleId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

}