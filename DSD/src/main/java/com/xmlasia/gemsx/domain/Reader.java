package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "READER")
public class Reader implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6488815064120290886L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
    @Column(name = "CREATEDATETIME")    
	@Temporal(TemporalType.TIMESTAMP)
    private Date createDateTime; 
    
    @Column(name = "UPDATEDATETIME") 
	@Temporal(TemporalType.TIMESTAMP)
    private Date updateDateTime; 
    
	@Column(name = "INPUTDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date inputDate;
	
    @Column(name = "READERIDNAME")
    private long readerIdName;
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public long getReaderIdName() {
		return readerIdName;
	}

	public void setReaderIdName(long readerIdName) {
		this.readerIdName = readerIdName;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
    
	
}
