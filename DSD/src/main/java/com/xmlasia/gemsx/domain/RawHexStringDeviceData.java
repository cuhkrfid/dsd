package com.xmlasia.gemsx.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RAWHEXSTRINGDEVICEDATA", indexes = { @Index(columnList = "inputDate", name = "rawhexstringdevicedata_inputdate_idx")})
public class RawHexStringDeviceData  implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -3867904869867669985L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
	private long id;
	
	@Column(name = "INPUTDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date inputDate;

	@Column(name = "RAWHEXSTRINGDEVICEDATA", length = 255)
	private String rawHexStringDeviceData;
	
	public Date getInputDate() {
		return inputDate;
	}

	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRawHexStringDeviceData() {
		return rawHexStringDeviceData;
	}

	public void setRawHexStringDeviceData(String rawHexStringDeviceData) {
	
		if (rawHexStringDeviceData.length() > 255) {
			this.rawHexStringDeviceData = rawHexStringDeviceData.substring(0, 254);
		} else {
			this.rawHexStringDeviceData = rawHexStringDeviceData;
		}
	}

}
