package com.xmlasia.gemsx.dao;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.persistence.Query;

import org.apache.commons.io.IOUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.SiteConfiguration;

public class SiteConfigurationDao extends BaseDao {

	private String mapFilePath = System.getProperty("catalina.base") + "\\wtpwebapps\\GEMSX\\mapFiles\\"; 
	//"C:\\apache-tomcat-8.0.32\\webapps\\GEMSX\\mapFiles\\";

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public SiteConfiguration createSiteConfiguration(SiteConfiguration a, InputStream fileData) {
		
		SiteConfiguration newSiteConfig;
        newSiteConfig = (SiteConfiguration) create(a);
        
        String mapFileName = a.getMapFileName();
	    writeMapFile(fileData, newSiteConfig, mapFileName);
	        
	    newSiteConfig = setSiteConfigMapHeightWidth(newSiteConfig, mapFileName);
	    newSiteConfig = (SiteConfiguration) update(newSiteConfig);
	    
		return newSiteConfig;
	}

	public SiteConfiguration setSiteConfigMapHeightWidth(SiteConfiguration siteConfig, String mapFileName) {
		BufferedImage bimg;
		
		if ( mapFileName == null || mapFileName.isEmpty()) {
			 siteConfig.setMapWidth(0);
			 siteConfig.setMapHeight(0);
			 return siteConfig;
		}
		
		try {
			bimg = ImageIO.read( new File( getMapFileFullPath(siteConfig, mapFileName) ) );
		    int width          = bimg.getWidth();
		    int height         = bimg.getHeight();
		    
		    siteConfig.setMapWidth(width);
		    siteConfig.setMapHeight(height);
		    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return siteConfig;
	}

	public void writeMapFile(InputStream fileData, SiteConfiguration siteConfig, String mapFileName) {
		if (fileData != null) {
			
			byte[] fileInBytes;
			try {
				fileInBytes = IOUtils.toByteArray(fileData);
				
				OutputStream os = new FileOutputStream(getMapFileFullPath(siteConfig, mapFileName));
				siteConfig.setImageUrlPath(getImageURLSubPath() + getFileName(siteConfig, mapFileName));
				
	        	os.write(fileInBytes, 0, fileInBytes.length);
		        os.flush();
		        os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public boolean deleteMapFile(SiteConfiguration siteConfig, String mapFileName) {
		File file = new File(getMapFileFullPath(siteConfig, mapFileName));
		return file.delete();
	}

	private String getMapFileFullPath(SiteConfiguration siteConfig, String mapFileName) {
		return getMapFilePath() + getFileName(siteConfig, mapFileName);
	}

	private String getFileName(SiteConfiguration siteConfig, String mapFileName) {
		return "Id_" + siteConfig.getId() + "_" + mapFileName;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public SiteConfiguration updateSiteConfiguration(SiteConfiguration a) {
		return (SiteConfiguration) update(a);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void deleteSiteConfiguration(long id) {
		delete(SiteConfiguration.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public SiteConfiguration retrieveSiteConfiguration(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ SiteConfiguration.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<SiteConfiguration> result = query.getResultList();
		if (result.size() > 0){
			return result.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<SiteConfiguration> listAll() {
		return getEm().createQuery("SELECT a FROM "+ SiteConfiguration.class.getName()+" a ").getResultList();
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<SiteConfiguration> searchByCriteria(String id, String name, String description, Boolean defaultSite ) {
		
		String nameParam = "%";
		String descriptionParam = "%";
		Boolean defaultSiteParam = null;
		
		String text = "";
		if (id != null && id.length() > 0){
			text += " and a.id = :idParam ";
		}
		if (name != null && name.length() > 0){
			nameParam += name+"%";
			text += " and a.name like :nameParam ";
		}
		if (description != null && description.length() > 0){
			descriptionParam += description+"%";
			text += " and a.description like :descriptionParam ";
		}
		if ( defaultSite != null) {
			defaultSiteParam = defaultSite;
			text += " and a.defaultSite = :defaultSiteParam ";
		}
		
		
		text = "select a from " + SiteConfiguration.class.getName() + " a "
				+ " where 1=1 " + text;
		
		Query query = getEm().createQuery(text);
		if (id != null && id.length() > 0){
			query.setParameter("idParam", Long.valueOf(id));
		}
		if (name != null && name.length() > 0){
			query.setParameter("nameParam", nameParam);
		}
		if (description != null && description.length() > 0){
			query.setParameter("descriptionParam", descriptionParam);
		}
		if (defaultSiteParam != null) {
			query.setParameter("defaultSiteParam", defaultSiteParam);
		}
		
		List<SiteConfiguration> result = query.getResultList();
		return result;
	}

	public String getMapFilePath() {
		return mapFilePath;
	}

	public void setMapFilePath(String mapFilePath) {
		this.mapFilePath = System.getProperty("catalina.base") + mapFilePath;
	}

	public String getImageURLSubPath() {
		return "./mapFiles/";
	}
}
