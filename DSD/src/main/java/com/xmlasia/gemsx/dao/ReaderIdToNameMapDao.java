package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.ReaderHeartBeat;
import com.xmlasia.gemsx.domain.ReaderIdToNameMap;
import com.xmlasia.gemsx.domain.User;

public class ReaderIdToNameMapDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ReaderIdToNameMap createReaderIdToNameMap(ReaderIdToNameMap a) {
		a.setCreateDate(new Date());
		return (ReaderIdToNameMap) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ReaderIdToNameMap updateReaderIdToNameMap(ReaderIdToNameMap a) {
		a.setUpdateDate(new Date());
		return (ReaderIdToNameMap) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ReaderIdToNameMap createReaderIdToNameMapIfNotFound(ReaderIdToNameMap a) {
		
		List<ReaderIdToNameMap> readerIdToNameMapList = searchByCriteria(null, String.valueOf(a.getReaderId()), null);
		if ( readerIdToNameMapList.size() > 0) {
			return readerIdToNameMapList.get(0);
		} 
		return (ReaderIdToNameMap) createReaderIdToNameMap(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteReaderIdToNameMap(long id) {
		delete(ReaderIdToNameMap.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<ReaderIdToNameMap> listAll() {
		return getEm().createQuery("SELECT a FROM "+ReaderIdToNameMap.class.getName()+" a ").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public ReaderIdToNameMap retrieveReaderIdToNameMap(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ ReaderIdToNameMap.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<ReaderIdToNameMap> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<ReaderIdToNameMap> searchByCriteria(String idStr, String readerIdStr, String readerNameStr ) {

		
		Long id = null;
		if ( FieldUtil.isFieldNonBlank(idStr)) {
			id = Long.valueOf(readerIdStr);
		}
		
		Long readerId = null;
		if ( FieldUtil.isFieldNonBlank(readerIdStr)) {
			readerId = Long.valueOf(readerIdStr);
		}
		
		String text = "";
		
		if (id != null){
			text += " and a.id = :idParam ";
		}
		
		if (readerId != null){
			text += " and a.readerId = :readerIdParam ";
		}
	
		if (readerNameStr != null){
			text += " and a.readerName = :readerNameParam ";
		}
		
		text = "select a from " + ReaderIdToNameMap.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		
		if (id != null){
			query.setParameter("idParam", id);
		}
		
		if (readerId != null){
			query.setParameter("readerIdParam", readerId);
		}

		if (readerNameStr != null){
			query.setParameter("readerNameParam", readerNameStr);
		}
		
		return query.getResultList();
	}
}
