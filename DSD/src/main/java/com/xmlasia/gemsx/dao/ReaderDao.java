package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Reader;

public class ReaderDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Reader createReader(Reader a) {
		Date date = new Date();
		a.setCreateDateTime(date);
		a.setUpdateDateTime(date);
		return (Reader) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Reader updateReader(Reader a) {
		Date date = new Date();
		a.setUpdateDateTime(date);
		return (Reader) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteReader(long id) {
		delete(Reader.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Reader retrieveReader(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ Reader.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<Reader> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Reader> searchByCriteria(String id, String fromDate, String toDate, String readerIdName, String descending) {
	
		Date dateFromDate = null;
		Date dateToDate = null;

		String text = "";
		if ( FieldUtil.isFieldNonBlank(id)) {
			text += " and a.id = :idParam";
		} 
		
		if ( FieldUtil.isFieldNonBlank(fromDate) &&  FieldUtil.isFieldNonBlank(toDate)) {
			dateFromDate = new Date( Long.valueOf(fromDate));
			dateToDate = new Date( Long.valueOf(toDate));
			text += " and a.updateDateTime between :dateFromParam AND :dateToParam ";
		} 
		

	
		
		text = "select a from " + Reader.class.getName() + " a "
				+ " where 1=1 " + text;
		
		if ( Boolean.valueOf(descending) == true) {
			text += " order by a.createDateTime desc";
		}
		
		Query query = getEm().createQuery(text);
		
		if ( FieldUtil.isFieldNonBlank(fromDate) &&  FieldUtil.isFieldNonBlank(toDate)) {
			query.setParameter("dateFromParam", dateFromDate);
			query.setParameter("dateToParam", dateToDate);
		}
		
		if ( FieldUtil.isFieldNonBlank(id)) {
			query.setParameter("idParam", Integer.valueOf(id));
		} 
		
		List<Reader> result = query.getResultList();

		return result;
	}
	
}
