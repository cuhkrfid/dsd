package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Module;

public class ModuleDao extends BaseDao{

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Module createModule(Module d) {
		return (Module) create(d);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Module updateModule(Module d) {
		return (Module) update(d);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void deleteModule(long id) {
		delete(Module.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Module retrieveModule(long id) {
		Query query = getEm().createQuery("SELECT m FROM "+ Module.class.getName()+" m "
				+ " where m.id= :id");
		query.setParameter("id", id);
		List<Module> result = query.getResultList();
		if (result.size() > 0){
			Module module = result.get(0);
			module.setMenus(null);
			return module;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Module> listAll() {
		List<Module> result = getEm().createQuery("SELECT u FROM "+Module.class.getName()+" m ").getResultList();
		if (result != null) {
			for (Module module : result) {
				module.setMenus(null);
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Module> searchByCriteria(String id, String name, String description,
			Integer displayOrder, Boolean homepage, Boolean active, String path, Boolean useOrder ) {
		
		String idParam = "%";
		String nameParam = "%";
		String descriptionParam = "%";
		Integer displayOrderParam = null;
		Boolean homepageParam = null;
		Boolean activeParam = null;
		String pathParam = "%";
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id+"%";
			text += " and STR(a.id) like :idParam ";
		}
		if (name != null && name.length() > 0){
			nameParam += name+"%";
			text += " and a.name like :nameParam ";
		}
		if (description != null && description.length() > 0){
			descriptionParam += description+"%";
			text += " and a.description like :descriptionParam ";
		}
		if (displayOrder != null ){
			displayOrderParam = displayOrder;
			text += " and a.displayOrder = :displayOrderParam ";
		}
		if (homepage != null ){
			homepageParam = homepage;
			text += " and a.homepage = :homepageParam ";
		}
		if (active != null ){
			activeParam = active;
			text += " and a.active = :activeParam ";
		}
		if (path != null && path.length() > 0){
			pathParam += path+"%";
			text += " and a.path like :pathParam ";
		}
		
		if ( useOrder != null && useOrder.equals(true)) {
			text += " order by a.displayOrder";
		} else {
			text += " order by a.id ";
		}
		
		text = "select a from " + Module.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (name != null && name.length() > 0){
			query.setParameter("nameParam", nameParam);
		}
		if (description != null && description.length() > 0){
			query.setParameter("descriptionParam", descriptionParam);
		}
		if (displayOrder != null ){
			query.setParameter("displayOrderParam", displayOrderParam);
		}
		if (homepage != null ){
			query.setParameter("homepageParam", homepageParam);
		}
		if (active != null ){
			query.setParameter("activeParam", activeParam);
		}
		if (path != null && path.length() > 0){
			query.setParameter("pathParam", pathParam);
		}
		List<Module> result = query.getResultList();
		if (result.size() > 0){
			for (Module module : result) {
				module.setMenus(null);
			}
		}
		
		return result;
	}
	
	
	
}
