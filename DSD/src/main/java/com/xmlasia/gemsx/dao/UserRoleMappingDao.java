package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Role;
import com.xmlasia.gemsx.domain.UserRoleMapping;

public class UserRoleMappingDao extends BaseDao{
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Role> findAssignedRoleById(int userId) {
		Query query = getEm().createQuery("SELECT distinct ur.role FROM "+UserRoleMapping.class.getName()+" ur "
				+ " left join ur.role "
				+ " where ur.userId= :userId");
		query.setParameter("userId", userId);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Role> findUnassignedRoleById(int userId) {
		Query query = getEm().createQuery("SELECT distinct r FROM "+Role.class.getName()+" r "
				+ " where r.id not in "
				+ " ( select ur.roleId from "+UserRoleMapping.class.getName()+" ur "
				+ " where ur.userId = :userId )");
		query.setParameter("userId", userId);
		return query.getResultList();
	}
	
}
