package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Company;

public class CompanyDao extends BaseDao {
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Company createCompany(Company c) {
		return (Company) create(c);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Company updateCompany(Company c) {
		return (Company) update(c);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void deleteCompany(int id) {
		delete(Company.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Company retrieveCompany(int id) {
		Query query = getEm().createQuery("SELECT c FROM "+Company.class.getName()+" c "
//				+ " left join fetch u.department d "
//				+ " left join fetch d.company "
				+ " where c.id= :id");
		query.setParameter("id", id);
		List<Company> result = query.getResultList();
		if (result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Company> listAll() {
		return getEm().createQuery("SELECT u FROM "+Company.class.getName()+" c ").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Company> searchByCriteria(String id, String chiName, String engName, String address, String telNo, String faxNo) {
		String idParam = "%";
		String chiNameParam = "%";
		String engNameParam = "%";
		String addressParam = "%";
		String telNoParam = "%";
		String faxNoParam = "%";
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id+"%";
			text += " and STR(c.id) like :idParam ";
		}
		if (chiName != null && chiName.length() > 0){
			chiNameParam += chiName+"%";
			text += " and c.chiName like :chiNameParam ";
		}
		if (engName != null && engName.length() > 0){
			engNameParam += engName+"%";
			text += " and c.engName like :engNameParam ";
		}
		if (address != null && address.length() > 0){
			addressParam += address+"%";
			text += " and c.address like :addressParam ";
		}
		if (telNo != null && telNo.length() > 0){
			telNoParam += telNo+"%";
			text += " and c.telNo like :telNoParam ";
		}
		if (faxNo != null && faxNo.length() > 0){
			faxNoParam += faxNo+"%";
			text += " and c.faxNo like :faxNoParam ";
		}
		
		text = "select c from " + Company.class.getName() + " c "
//				+ " left join fetch u.department d "
//				+ " left join fetch d.company "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (chiName != null && chiName.length() > 0){
			query.setParameter("chiNameParam", chiNameParam);
		}
		if (engName != null && engName.length() > 0){
			query.setParameter("engNameParam", engNameParam);
		}
		if (address != null && address.length() > 0){
			query.setParameter("addressParam", addressParam);
		}
		if (telNo != null && telNo.length() > 0){
			query.setParameter("telNoParam", telNoParam);
		}
		if (faxNo != null && faxNo.length() > 0){
			query.setParameter("faxNoParam", faxNoParam);
		}		
		List<Company> result = query.getResultList();
		return result;
	}
}
