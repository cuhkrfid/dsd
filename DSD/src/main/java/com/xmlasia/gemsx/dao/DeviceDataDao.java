package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.DeviceData;

public class DeviceDataDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public DeviceData createDeviceData(DeviceData a) {
		a.setCreateDate(new Date());
		return (DeviceData) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public DeviceData updateDeviceData(DeviceData a) {
		return (DeviceData) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteDeviceData(long id) {
		delete(DeviceData.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Integer> distinctDeviceId() {
		return getEm().createQuery("SELECT distinct a.deviceId FROM "+ DeviceData.class.getName()+" a order by a.deviceId asc").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<DeviceData> searchByCriteria(String id, String numberOfSensor, String measurePeriod, String readerId, 
			String parentAddress, String deviceAddress, String tagId, String dateFrom, String dateTo, Boolean descending, Integer limit ) { 
		
		String idParam = "";
		String readerIdParam = "";
		String numberOfSensorParam = "";
		String measurePeriodParam = "";
		Integer tagIdParam = null;
		String parentAddressParam = "";
		String deviceAddressParam = "";

		Date dateFromDate = null;
		Date dateToDate = null;
		
		if ( dateFrom != null && !dateFrom.isEmpty() &&  dateTo != null && !dateTo.isEmpty() ) {
			dateFromDate = new Date( Long.valueOf(dateFrom));
			dateToDate = new Date( Long.valueOf(dateTo));
		}
		
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id;
			text += " and STR(a.id) = :idParam ";
		}
		if (readerId != null && readerId.length() > 0){
			readerIdParam += readerId;
			text += " and a.readerId = :readerIdParam ";
		}
		if (numberOfSensor != null && numberOfSensor.length() > 0){
			numberOfSensorParam += numberOfSensor;
			text += " and a.numberOfSensor = :numberOfSensorParam ";
		}
		if (measurePeriod != null && measurePeriod.length() > 0){
			measurePeriodParam += measurePeriod;
			text += " and a.measurePeriod = :measurePeriodParam ";
		}
		if (tagId != null && tagId.length() > 0){
			tagIdParam = Integer.valueOf(tagId);
			text += " and a.deviceId = :tagIdParam ";
		}		
		if (parentAddress != null && parentAddress.length() > 0){
			parentAddressParam += parentAddress;
			text += " and a.parentAddress = :parentAddressParam ";
		}
		if (deviceAddress != null && deviceAddress.length() > 0){
			deviceAddressParam += deviceAddress;
			text += " and a.deviceAddress = :deviceAddressParam ";
		}		
		
		if ( dateFromDate != null && dateToDate != null ) {
			text += " and a.inputDate between :dateFromParam AND :dateToParam";
		}
		
		if ( descending == true) {
			text += " order by a.inputDate desc";
		} else {
			text += " order by a.inputDate asc";
		}
		
		
		text = "select a from " + DeviceData.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		
		
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (tagId != null && tagId.length() > 0){
			query.setParameter("tagIdParam", tagIdParam);
		}
		if (numberOfSensor != null && numberOfSensor.length() > 0){
			query.setParameter("numberOfSensorParam", numberOfSensorParam);
		}	
		if (measurePeriod != null && measurePeriod.length() > 0){
			query.setParameter("measurePeriodParam", measurePeriodParam);
		}		
		if (readerId != null && readerId.length() > 0){
			query.setParameter("readerIdParam", readerIdParam);
		}	
		if (parentAddress != null && parentAddress.length() > 0){
			query.setParameter("parentAddressParam", parentAddressParam);
		}		
		if (deviceAddress != null && deviceAddress.length() > 0){
			query.setParameter("deviceAddressParam", deviceAddressParam);
		}		
		if ( dateFromDate != null && dateToDate != null ) {
			query.setParameter("dateFromParam", dateFromDate);
			query.setParameter("dateToParam", dateToDate);
		}
		List<DeviceData> result;
		if ( limit == null) {
			result = query.getResultList();
		} else {
			result = query.setFirstResult(0).setMaxResults(limit).getResultList();
		}
		return result;
	}
	
}
