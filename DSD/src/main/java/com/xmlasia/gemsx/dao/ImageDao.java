package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Image;

public class ImageDao extends BaseDao {

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Image createImage(Image i) {
		Date currentTime = new Date();

		i.setCreatedBy("SYSTEM");
		i.setCreatedDate(currentTime);
		i.setUpdatedBy("SYSTEM");
		i.setUpdatedDate(currentTime);
		i.setVersion(0);
	
		return (Image) create(i);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Image updateImage(Image i) {
		return (Image) update(i);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void deleteImage(long id) {
		delete(Image.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Image retrieveImage(int id) {
		Query query = getEm().createQuery("SELECT i FROM "+ Image.class.getName()+" i "
				+ " where i.id= :id");
		query.setParameter("id", id);
		List<Image> result = query.getResultList();
		if (result.size() > 0){
			Image image = result.get(0);
			return Image.convertToBase64(image);
		}
		return null;
	}
	
	
}
