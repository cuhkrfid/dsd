package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.GroupPolyLine;

public class GroupPolyLineDao  extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public GroupPolyLine createGroupPolyLine(GroupPolyLine a) {
		a.setCreateDateTime(new Date());
		return (GroupPolyLine) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public GroupPolyLine updateGroupPolyLine(GroupPolyLine a) {
		return (GroupPolyLine) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteGroupPolyLine(long id) {
		delete(GroupPolyLine.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public GroupPolyLine retrieveGroupPolyLine(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ GroupPolyLine.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<GroupPolyLine> result = query.getResultList();
		if (result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<GroupPolyLine> searchByCriteria(
			String id, 
			String groupTagConfigurationId,
			String siteConfigurationId) {
		
		String text = "";
		
		if (FieldUtil.isFieldNonBlank(id)){
			text += " and a.id = :idParam ";
		}
		if (FieldUtil.isFieldNonBlank(groupTagConfigurationId)){
			text += " and a.groupTagConfigurationId = :groupTagConfigurationIdParam";
		}
		if (FieldUtil.isFieldNonBlank(siteConfigurationId)){
			text += " and a.siteConfigurationId = :siteConfigurationIdParam";
		}
		
		text = "select a from " + GroupPolyLine.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		
		if (FieldUtil.isFieldNonBlank(id)){
			query.setParameter("idParam", Long.valueOf(id));
		}
		if (FieldUtil.isFieldNonBlank(groupTagConfigurationId)){
			query.setParameter("groupTagConfigurationIdParam", Long.valueOf(groupTagConfigurationId));
		}
		if (FieldUtil.isFieldNonBlank(siteConfigurationId)){
			query.setParameter("siteConfigurationIdParam", Long.valueOf(siteConfigurationId));
		}
		
		List<GroupPolyLine> result = query.getResultList();
		return result;
	}
			

}
