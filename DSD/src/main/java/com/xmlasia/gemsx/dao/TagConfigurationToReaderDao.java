package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Reader;
import com.xmlasia.gemsx.domain.TagConfigurationToReader;

public class TagConfigurationToReaderDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public TagConfigurationToReader createTagConfigurationToReader(TagConfigurationToReader a) {
		Date date = new Date();
		a.setCreateDateTime(date);
		a.setUpdateDateTime(date);
		return (TagConfigurationToReader) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public TagConfigurationToReader updateTagConfigurationToReader(TagConfigurationToReader a) {
		return (TagConfigurationToReader) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteTagConfigurationToReader(long id) {
		delete(TagConfigurationToReader.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public TagConfigurationToReader retrieveTagConfigurationToReader(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ TagConfigurationToReader.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<TagConfigurationToReader> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<TagConfigurationToReader> searchByCriteria(String id, String fromDate, String toDate, String tagConfigurationId, String readerId,  String descending) {
		
		Date dateFromDate = null;
		Date dateToDate = null;

		String text = "";
		if ( FieldUtil.isFieldNonBlank(id)) {
			text += " and a.id = :idParam";
		} 
		
		if ( FieldUtil.isFieldNonBlank(fromDate) &&  FieldUtil.isFieldNonBlank(toDate)) {
			dateFromDate = new Date( Long.valueOf(fromDate));
			dateToDate = new Date( Long.valueOf(toDate));
			text += " and a.updateDateTime between :dateFromParam AND :dateToParam ";
		} 
		
		if ( FieldUtil.isFieldNonBlank(tagConfigurationId)) {
			text += " and a.tagConfigurationId = :tagConfigurationId ";
		}
	
		if ( FieldUtil.isFieldNonBlank(readerId)) {
			text += " and a.readerId = :readerId ";
		}
		
		text = "select a from " + Reader.class.getName() + " a "
				+ " where 1=1 " + text;
		
		if ( Boolean.valueOf(descending) == true) {
			text += " order by a.updateDateTime desc";
		}
		
		Query query = getEm().createQuery(text);
		
		if ( FieldUtil.isFieldNonBlank(fromDate) &&  FieldUtil.isFieldNonBlank(toDate)) {
			query.setParameter("dateFromParam", dateFromDate);
			query.setParameter("dateToParam", dateToDate);
		}
		
		if ( FieldUtil.isFieldNonBlank(tagConfigurationId)) {
			query.setParameter("tagConfigurationId", Integer.valueOf(tagConfigurationId));
		}

		if ( FieldUtil.isFieldNonBlank(readerId)) {
			query.setParameter("readerId", Integer.valueOf(readerId));
		}
		
		if ( FieldUtil.isFieldNonBlank(id)) {
			query.setParameter("idParam", Integer.valueOf(id));
		} 
		
		List<TagConfigurationToReader> result = query.getResultList();
		return result;
	}
}
