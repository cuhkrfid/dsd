package com.xmlasia.gemsx.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.xmlasia.gemsx.domain.Network;
import com.xmlasia.gemsx.domain.NetworkChartElement;


public class NetworkDao extends BaseDao {

	private Log log = LogFactory.getLog(this.getClass());
	
	private List<NetworkChartElement> networkChart = new ArrayList<NetworkChartElement>();
	private String jsonNetworkChart = "";
			
	private Integer assignedChartId = 1;
	
	private long timeoutDelayInMin; 
	private long absoluteTimeoutInMin; 
	
	private boolean checkIfValidNetworkObject(Network toBeSaved) {
		if ( toBeSaved.getParentAddress() != null 
			&& toBeSaved.getDeviceAddress() != null 
			&& !toBeSaved.getParentAddress().isEmpty() 
			&& !toBeSaved.getDeviceAddress().isEmpty() ) {
				return true;
		}
		return false;
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Network createNetwork(Network a) {
		a.setRecordLastSaveTime(new Date());
		return (checkIfValidNetworkObject(a) ? (Network) create(a) : null );
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Network updateNetwork(Network a) {
		a.setRecordLastSaveTime(new Date());
		return (checkIfValidNetworkObject(a) ? (Network) update(a) : null );
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String deleteNetwork(long id) {
		delete(Network.class, id);
		updateNetworkStatus(new Date());
		return jsonNetworkChart;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public synchronized  List<Network> createNewOrUpdate(Network networkToSearch) {
		
		String text = "";
		if (networkToSearch.getDeviceType() == Network.COORDINATOR)
			text += " and a.cordId = :cordId ";
		else if (networkToSearch.getDeviceType() == Network.ENDDEVICE)
			text += " and a.deviceId = :deviceId ";
		else if (networkToSearch.getDeviceType() == Network.ROUTER)
			text += " and a.routerId = :routerId ";
		else 
			return null;
		
		text = "select distinct a from " + Network.class.getName() + " a "
				+ " where 1=1 " + text;
		
		Query query = getEm().createQuery(text);
		
		if (networkToSearch.getDeviceType() == Network.COORDINATOR ) {
			if (networkToSearch.getCordId() > 0)
				query.setParameter("cordId", networkToSearch.getCordId());
			else
				return null;
		} else if (networkToSearch.getDeviceType() == Network.ENDDEVICE ) {
			if (networkToSearch.getDeviceId() != null)
				query.setParameter("deviceId", networkToSearch.getDeviceId().intValue());
			else
				return null;
		} else if (networkToSearch.getDeviceType() == Network.ROUTER ) {
			if (networkToSearch.getRouterId() != null)
				query.setParameter("routerId", networkToSearch.getRouterId().intValue());
			else
				return null;
		}  else {
			return null;
		}
		
		List<Network> result = query.getResultList();
		List<Network> networkSaved = new ArrayList<Network>();
		
		Network saved = null;
		if ( result != null && result.size() > 0 )  {
			Network networkToUpdate = result.get(0);
			
			networkToUpdate.setCordId(networkToSearch.getCordId());
			networkToUpdate.setDeviceId(networkToSearch.getDeviceId());
			networkToUpdate.setDeviceType(networkToSearch.getDeviceType());
			networkToUpdate.setParentAddress(networkToSearch.getParentAddress());
			networkToUpdate.setDeviceAddress(networkToSearch.getDeviceAddress());
			
			networkToUpdate.setRouterId(networkToSearch.getRouterId());
			networkToUpdate.setStatusId(networkToSearch.getStatusId());
			
			networkToUpdate.setLastUpdatedTime(networkToUpdate.getUpdatedTime());
			networkToUpdate.setUpdatedTime(networkToSearch.getUpdatedTime());
			long timeDiffInMs = networkToUpdate.getUpdatedTime().getTime()
					- networkToUpdate.getLastUpdatedTime().getTime();
			networkToUpdate.setTimeDiffInMs(timeDiffInMs);
			networkToUpdate.setConnected(true);
			saved = updateNetwork(networkToUpdate);
		} else {
			saved = createNetwork(networkToSearch);
		}
		
		if ( saved != null) {
			networkSaved.add(saved);
		}
		
		return networkSaved;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<Network> listAll() {
		return getEm().createQuery("SELECT a FROM "+ Network.class.getName()+" a ").getResultList();
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public synchronized void updateNetworkStatus(Date currentDateTime) {
		List<Network> networks =  listAll();	
		for (Network network : networks) { 
			Long lastTimeDiff = network.getTimeDiffInMs();
			if ( lastTimeDiff == null ) {
				Date lastUpdatedTime = network.getUpdatedTime();
				if ( lastUpdatedTime == null)		
					continue; //lastUpdatedTime must be updated by SocketServer, impossible if clause
				
				if ( currentDateTime.getTime() - lastUpdatedTime.getTime() >= absoluteTimeoutInMin * 60 * 1000 ) {
					network.setConnected(false);
				} else {
					network.setConnected(true);
				}
				updateNetwork(network);		
				continue;
			}

			long currentTimeDiff = currentDateTime.getTime() - network.getUpdatedTime().getTime();
			if ( currentTimeDiff > lastTimeDiff + timeoutDelayInMin * 60000) {
				network.setConnected(false);
			} else {
				network.setConnected(true);
			}
			updateNetwork(network);	
		}
		
		resetChart();
		createChart();
		sortParentIdForOrgChart();
	}

	private void resetChart() {
		networkChart.clear();
		jsonNetworkChart = "";
		assignedChartId = 1;
	}
	
	private void sortParentIdForOrgChart() {
		JsonArray jdataList = new JsonArray();
     	for ( int i = networkChart.size() - 1; i >= 0; i-- ) {
     		NetworkChartElement networkElement = networkChart.get(i);
		
     		JsonObject networkChartElement = new JsonObject();
     		networkChartElement.addProperty("id", networkElement.getId());
     		networkChartElement.addProperty("parentId", networkElement.getParentId());
     		networkChartElement.addProperty("Name", networkElement.getName());
	    	networkChartElement.addProperty("Title", networkElement.getTitle());
	    	networkChartElement.addProperty("Image", networkElement.getImage());
	    	
	       	jdataList.add(networkChartElement);
		}
	    jsonNetworkChart = jdataList.toString();   		
	}
	
	private void createChart() {
		String text = "select a from " + Network.class.getName() + " a ";
		Query query = getEm().createQuery(text);
	
		List<Network> allNetworks = query.getResultList();
		List<Network> rootNetworks = new ArrayList<Network>();
		
		if (allNetworks != null ) {
			for (int i = 0; i < allNetworks.size(); ++i) {
				Network outerElement = allNetworks.get(i);
				if (outerElement.getDeviceAddress().equals(outerElement.getParentAddress())) {
					rootNetworks.add(outerElement);
				} else {
					
					for (int j = 0; j < allNetworks.size(); ++j) {
						Network innerElement = allNetworks.get(j);
						if ( i != j ) {
							if ( innerElement.getDeviceAddress().equals(outerElement.getParentAddress()) ) {
								outerElement.setOrphaned(false);
								break;
							}
						}
						if (j == allNetworks.size() - 1) {
							rootNetworks.add(outerElement);
							//child element without an actual parent device
							outerElement.setOrphaned(true);
						}
					}
				}
			}
		}
	
		Date startDate = new Date();
		log.debug(String.format("NetworkDao createChart startDate: %s", startDate.toString()) ) ;
			
		for (Network rootNetwork : rootNetworks) {
			List<NetworkChartElement> childNodes = findChildNode(rootNetwork, allNetworks, 1);
			
			//add the rootNode
			NetworkChartElement e = createNetworkElementInChart(rootNetwork);
			e.setParentId(null);
			for (NetworkChartElement chartElem : childNodes) {
				chartElem.setParentId(e.getId());
			}
			networkChart.add(e);	
		}
		
		Date endDate = new Date();
		long timeInMs = endDate.getTime() - startDate.getTime();
		log.debug(String.format("NetworkDao createChart endDate: %s", endDate.toString()) );
		log.debug(String.format("NetworkDao createChart takes: %d ms to complete", timeInMs) );
	}
	
	private List<NetworkChartElement> findChildNode(Network parentNode, List<Network> networks, int currentLevel ) {
		List<NetworkChartElement> networkElemToReturn = new ArrayList<NetworkChartElement>();
		List<NetworkChartElement> childNodes = new ArrayList<NetworkChartElement>();
		NetworkChartElement e;
		
		for (Network network : networks) {
			
			if ( network.getParentAddress().equals(parentNode.getDeviceAddress())
				&& network != parentNode) {
				
				String childParentAddress = network.getDeviceAddress();
				List<Network> networkChildNodes = this.searchByCriteria(null, null, null, null, null, childParentAddress, null, null, null, false);
				childNodes = findChildNode(network, networkChildNodes, currentLevel + 1);
				if ( childNodes.size() == 0) {
					e = createNetworkElementInChart(network);
					networkChart.add(e);
					networkElemToReturn.add(e);
				} else {
					e = createNetworkElementInChart(network);
					for (NetworkChartElement chartElem : childNodes) {
						chartElem.setParentId(e.getId());
					}
					networkElemToReturn.add(e);
					networkChart.add(e);
				}				
			}
		}

		return networkElemToReturn;
	}

	private NetworkChartElement createNetworkElementInChart(Network network) {
		//									  12345678:1234
	  	DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dateString = df.format(network.getUpdatedTime());
		String name = "";
		String image = "";
		switch (network.getDeviceType()) {
			case Network.COORDINATOR:
				name =  Network.COORD + ":" + String.valueOf(network.getCordId());
				if (network.getConnected())
					image = "./img/network/72/Coordinator.png";
				else
					image = "./img/network/72/CoordinatorDisconnected.png";
				break;
			case Network.ENDDEVICE:
				name = Network.END + ":" + String.valueOf(network.getDeviceId());
				if (network.getConnected())
					image = "./img/network/72/EndDevice.png";
				else 
					image = "./img/network/72/EndDeviceDisconnected.png";
				
				break;
			case Network.ROUTER:
				name = Network.ROUT + ":" + String.valueOf(network.getRouterId());
				if (network.getConnected())
					image = "./img/network/72/Router.png";
				else 
					image = "./img/network/72/RouterDisconnected.png";
		
				break;
		}
		NetworkChartElement newChartElement = new NetworkChartElement(assignedChartId++, name,  network.getDeviceType(), dateString, image);
		return newChartElement;
	}

	public String getNetworkChart() {
		return this.jsonNetworkChart;
	}

	public void setNetworkChart(List<NetworkChartElement> networkChart) {
		this.networkChart = networkChart;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Network> searchByCriteria(String id, String cordId, String deviceId, 
			String routerId, String deviceType, String parentAddress, String deviceAddress, Boolean connected, Boolean orphaned, boolean isCalculateTimeDiff )  {
		
		String idParam = "";
		String cordIdParam = "";
		String deviceIdParam = "";
		String routerIdParam = "";
		String deviceTypeParam = "";
		String parentAddressParam = "";
		String deviceAddressParam = "";
		Boolean connectedParam = true;
		Boolean orphanedParam = false;		
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id;
			text += " and STR(a.id) = :idParam ";
		}
		if (cordId != null && cordId.length() > 0){
			cordIdParam += cordId;
			text += " and STR(a.cordId) = :cordIdParam ";
		}
		if (deviceId != null && deviceId.length() > 0){
			deviceIdParam += deviceId;
			text += " and STR(a.deviceId) = :deviceIdParam ";
		}
		if (routerId != null && routerId.length() > 0){
			routerIdParam += routerId;
			text += " and STR(a.routerId) = :routerIdParam ";
		}		
		if (deviceType != null && deviceType.length() > 0){
			deviceTypeParam += deviceType;
			text += " and STR(a.deviceType) = :deviceTypeParam ";
		}				
		if (parentAddress != null && parentAddress.length() > 0){
			parentAddressParam += parentAddress;
			text += " and a.parentAddress = :parentAddressParam ";
		}
		if (deviceAddress != null && deviceAddress.length() > 0){
			deviceAddressParam += deviceAddress;
			text += " and a.deviceAddress = :deviceAddressParam ";
		}		
		if (connected != null ){
			connectedParam = connected;
			text += " and a.connected = :connectedParam ";
		}
		if (orphaned != null ){
			orphanedParam = orphaned;
			text += " and a.orphaned = :orphanedParam ";
		}
		
		text = "select a from " + Network.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		
		
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (cordId != null && cordId.length() > 0){
			query.setParameter("cordIdParam", cordIdParam);
		}
		if (deviceId != null && deviceId.length() > 0){
			query.setParameter("deviceIdParam", deviceIdParam);
		}
		if (routerId != null && routerId.length() > 0){
			query.setParameter("routerIdParam", routerIdParam);
		}	
		if (deviceType != null && deviceType.length() > 0){
			query.setParameter("deviceTypeParam", deviceTypeParam);
		}			
		if (parentAddress != null && parentAddress.length() > 0){
			query.setParameter("parentAddressParam", parentAddressParam);
		}		
		if (deviceAddress != null && deviceAddress.length() > 0){
			query.setParameter("deviceAddressParam", deviceAddressParam);
		}			
		if (connected != null ){
			query.setParameter("connectedParam", connectedParam);
		}			
		if (orphaned != null ){
			query.setParameter("orphanedParam", orphanedParam);
		}	
		
		List<Network> result = query.getResultList();
		if ( isCalculateTimeDiff && result != null ) {
			for (Network network : result) {
				if ( network.getUpdatedTime() != null && network.getLastUpdatedTime() != null) {
					long timeDiffInMs = network.getUpdatedTime().getTime()
						- network.getLastUpdatedTime().getTime();
					network.setTimeDiffInMsStr(String.valueOf(timeDiffInMs));
				} else {
					network.setTimeDiffInMsStr("N/A");
				}
			}
		}
		return ( result == null ? new ArrayList<Network>() : result );
	}

	public long getAbsoluteTimeoutInMin() {
		return absoluteTimeoutInMin;
	}

	public void setAbsoluteTimeoutInMin(long absoluteTimeoutInMin) {
		this.absoluteTimeoutInMin = absoluteTimeoutInMin;
	}

	public long getTimeoutDelayInMin() {
		return timeoutDelayInMin;
	}

	public void setTimeoutDelayInMin(long timeoutDelayInMin) {
		this.timeoutDelayInMin = timeoutDelayInMin;
	}
}
