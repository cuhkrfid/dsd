package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.ReaderHeartBeat;

public class ReaderHeartBeatDao  extends BaseDao {

	private Log log = LogFactory.getLog(this.getClass());
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ReaderHeartBeat createReaderHeartBeat(ReaderHeartBeat a) {
		a.setCreateDate(new Date());
		return (ReaderHeartBeat) create(a);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<ReaderHeartBeat> listAll() {
		return getEm().createQuery("SELECT a FROM "+ ReaderHeartBeat.class.getName()+" a ").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<ReaderHeartBeat> searchByCriteria(String idStr, String readerIdStr, String dateFrom, String dateTo, String fromRssiStr, String toRssiStr, Boolean descending, Integer limit ) {

		Integer readerId = null;
		if ( FieldUtil.isFieldNonBlank(readerIdStr)) {
			readerId = Integer.valueOf(readerIdStr);
		}
		
		Long id = null;
		if ( FieldUtil.isFieldNonBlank(idStr)) {
			id = Long.valueOf(readerIdStr);
		}
		
		Date dateFromDate = null;
		Date dateToDate = null;
		
		if ( FieldUtil.isFieldNonBlank(dateFrom) && FieldUtil.isFieldNonBlank(dateTo) ) {
			dateFromDate = new Date( Long.valueOf(dateFrom));
			dateToDate = new Date( Long.valueOf(dateTo));
		}
		
		Double fromRssi = null;
		Double toRssi = null;
			
		if ( FieldUtil.isFieldNonBlank(fromRssiStr) && FieldUtil.isFieldNonBlank(toRssiStr) ) {
			fromRssi = Double.valueOf(fromRssiStr);
			toRssi = Double.valueOf(toRssiStr);
		}
		
		String text = "";
		
		if (id != null){
			text += " and a.id = :idParam ";
		}
		
		if (readerId != null){
			text += " and a.readerId = :readerIdParam ";
		}
	
		if (fromRssi != null && toRssi != null){
			text += " and a.rssi between = :fromRssiParam And :toRssiParam ";
		}
		
		if ( dateFromDate != null && dateToDate != null ) {
			text += " and a.inputDate between :dateFromParam AND :dateToParam";
		}
		
		if (descending != null) {
			if ( descending.booleanValue() ) {
				text += " order by a.id desc";
			} else {
				text += " order by a.id asc";
			}
		}
		
		text = "select a from " + ReaderHeartBeat.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		
		if (id != null){
			query.setParameter("idParam", id);
		}
		
		if (readerId != null){
			query.setParameter("readerIdParam", readerId);
		}
	
		if (fromRssi != null && toRssi != null){
			query.setParameter("fromRssiParam", fromRssi);
			query.setParameter("toRssiParam", toRssi);
		}
		
		if ( dateFromDate != null && dateToDate != null ) {
			query.setParameter("dateFromParam", dateFromDate);
			query.setParameter("dateToParam", dateToDate);
		}
		
		List<ReaderHeartBeat> result;
		if ( limit == null) {
			result = query.getResultList();
		} else {
			result = query.setFirstResult(0).setMaxResults(limit).getResultList();
		}
		return result;

	}
}
