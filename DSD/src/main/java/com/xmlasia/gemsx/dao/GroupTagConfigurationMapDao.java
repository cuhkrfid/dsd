package com.xmlasia.gemsx.dao;

import com.xmlasia.gemsx.domain.Alert;
import com.xmlasia.gemsx.domain.GroupTagConfigurationMap;
import com.xmlasia.gemsx.domain.TagConfiguration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


public class GroupTagConfigurationMapDao  extends BaseDao{

	private AlertDao alertDao;
	private TagConfigurationDao tagConfigurationDao;
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public GroupTagConfigurationMap createGroupTagConfigurationMap(GroupTagConfigurationMap a) {
		a.setCreateDateTime(new Date());
		return (GroupTagConfigurationMap) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public GroupTagConfigurationMap updateGroupTagConfigurationMap(GroupTagConfigurationMap a) {
		return (GroupTagConfigurationMap) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteGroupTagConfigurationMap(long id) {
		delete(GroupTagConfigurationMap.class, id);
	}	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public GroupTagConfigurationMap retrieveGroupTagConfigurationMap(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ GroupTagConfigurationMap.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<GroupTagConfigurationMap> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	public List<GroupTagConfigurationMap> searchByCriteria(String fromDate, String toDate, long groupTagConfigurationId, String tagConfigurationId, String alertId) {
		
		List<GroupTagConfigurationMap> groupTagConfigurationMaps = new ArrayList<GroupTagConfigurationMap>();
		
		String text = "";
		text += " and a.groupTagConfigurationId = :groupTagConfigurationId";
		if ( FieldUtil.isFieldNonBlank(tagConfigurationId)) {
			text += " and a.tagConfigurationId = :tagConfigurationId";
		} 
		
		text = "select a from " + GroupTagConfigurationMap.class.getName() + " a "
				+ " where 1=1 " + text;

		Query query = getEm().createQuery(text);
		
		query.setParameter("groupTagConfigurationId", Long.valueOf(groupTagConfigurationId));
		if ( FieldUtil.isFieldNonBlank(tagConfigurationId)) {
			query.setParameter("tagConfigurationId", Long.valueOf(tagConfigurationId));
		}
		
		List<GroupTagConfigurationMap> result = query.getResultList();
		for (GroupTagConfigurationMap groupTagConfigurationMap : result) {
			List<TagConfiguration> tagConfigurations = tagConfigurationDao.searchByCriteria(String.valueOf(groupTagConfigurationMap.getTagConfigurationId()), null, null, null, null, null, null, null);
			
			boolean isAdd = true;
			
			if ( tagConfigurations != null && tagConfigurations.size() > 0) {
				TagConfiguration tagConfiguration = tagConfigurations.get(0);
				if ( fromDate != null && toDate != null) {
					List<Alert> alerts = alertDao.searchByCriteria(alertId, fromDate, toDate, null, null, String.valueOf(groupTagConfigurationMap.getTagConfigurationId()),  String.valueOf(true));
					if ( alerts.size() == 0 && FieldUtil.isFieldNonBlank(alertId)) {
						isAdd = false;
					} 
					tagConfiguration.setAlerts(alerts);
				}
				groupTagConfigurationMap.setTagConfiguration(tagConfiguration);
				
			}
			if ( isAdd) {
				groupTagConfigurationMaps.add(groupTagConfigurationMap);
			}
		}
		
		return groupTagConfigurationMaps;
	}

	public TagConfigurationDao getTagConfigurationDao() {
		return tagConfigurationDao;
	}

	public void setTagConfigurationDao(TagConfigurationDao tagConfigurationDao) {
		this.tagConfigurationDao = tagConfigurationDao;
	}

	public AlertDao getAlertDao() {
		return alertDao;
	}

	public void setAlertDao(AlertDao alertDao) {
		this.alertDao = alertDao;
	}
}
