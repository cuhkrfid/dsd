package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Group;
import com.xmlasia.gemsx.domain.Permission;


public class GroupDao extends BaseDao {

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Group> searchByCriteria(String id, 
			String name,
			String description,
			Boolean active )  {
		
		String idParam = "";
		String nameParam = "";
		String descriptionParam = "%";
		Boolean activeParam = null;
	
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id;
			text += " and STR(a.id) = :idParam ";
		}
		if (name != null && name.length() > 0){
			nameParam += name;
			text += " and a.name = :nameParam ";
		}		
		if (description != null && description.length() > 0){
			descriptionParam += description+"%";
			text += " and a.description like :descriptionParam ";
		}		
		if (active != null ){
			activeParam = active;
			text += " and a.active = :activeParam ";
		}	
		
		text = "select a from " + Group.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		
		
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (name != null && name.length() > 0){
			query.setParameter("nameParam", nameParam);
		}
		if (description != null && description.length() > 0){
			query.setParameter("descriptionParam", descriptionParam);
		}	
		if (active != null ){
			query.setParameter("activeParam", activeParam);
		}	
		
		
		List<Group> result = query.getResultList();
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Permission> listAllPermission() {
		return getEm().createQuery("SELECT p FROM "+ Permission.class.getName()+" p ").getResultList();
	}
	
}
