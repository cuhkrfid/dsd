package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class BaseDao {
	final static Logger logger = LoggerFactory.getLogger(BaseDao.class);

	@PersistenceContext
	protected EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Object create(Object object) {
		return em.merge(object);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Object update(Object object) {
		Object o = em.merge(object);
		em.flush();
		return o;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void delete(Class c, Object key) {
		em.remove(em.getReference(c, key));
		em.flush();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Object retrieve(Class c, Object key) {
		return (Object) em.find(c, key);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List listAll(Class c) {
		return getEm().createQuery("SELECT b FROM " + c.getName() + " b")
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List query(String hql, Object[] args) {
		Query query = getEm().createQuery(hql);
		if (null != args) {
			for (int i = 0; i < args.length; i++) {
				query.setParameter(i + 1, args[i]);
			}
		}
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List query(String hql) {
		return query(hql, null);
	}

	/**
	 * Only used for load template data.
	 * 
	 * @param sql
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int executSql(String sql) {
		return em.createNativeQuery(sql).executeUpdate();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public int executSql(String sql, Object[] args) {
		Query query = em.createNativeQuery(sql);
		if (null != args) {
			for (int i = 0; i < args.length; i++) {
				query.setParameter(i + 1, args[i]);
			}
		}
		return query.executeUpdate();
	}
}
