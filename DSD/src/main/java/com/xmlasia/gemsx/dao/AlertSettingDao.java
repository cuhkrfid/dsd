package com.xmlasia.gemsx.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.AlertCriteria;
import com.xmlasia.gemsx.domain.AlertSetting;
import com.xmlasia.gemsx.domain.AlertSettingSensorConfig;
import com.xmlasia.gemsx.domain.SensorConfig;
import com.xmlasia.gemsx.domain.SiteConfiguration;
import com.xmlasia.gemsx.domain.TagConfiguration;

public class AlertSettingDao extends BaseDao {

	private AlertSettingSensorConfigDao alertSettingSensorConfigDao;
	private TagConfigurationDao tagConfigurationDao;
	private SensorConfigDao sensorConfigDao; 
	private SiteConfigurationDao siteConfigurationDao;
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public AlertSetting createAlertSetting(AlertSetting a) {
		return (AlertSetting) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public AlertSetting updateAlertSetting(AlertSetting a) {
		return (AlertSetting) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteAlertSetting(long id) {
		delete(AlertSetting.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public AlertSetting retrieveAlertSetting(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ AlertSetting.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<AlertSetting> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<AlertSetting> searchByCriteria(
			String id, 
			String name,
			String level,
			String useDateRange,
			
			String fromDate, 
			String toDate, 
			
			String deviceAddress, 
			String deviceId, 
			String groupId, 
	
			String isCoverLevel, 
			String isWaterLevel, 
			String isCo2, 
			String isH2s, 
			String isSo2, 
			String isCh4,
			String isNh3,
			String isO2,
			
			Long alertCriteriaId,
			Long tagConfigurationId,
			boolean determineCriteriaDescription) { 
		
		String idParam = "%";
		String nameParam = "%";
		String levelParam = "%";
		
		String deviceAddressParam = "%";
		String deviceIdParam = "%";
		String groupIdParam = "%";
			
		Boolean isSendEmailParam = null;
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id+"%";
			text += " and STR(a.id) like :idParam ";
		}
		if (name != null && name.length() > 0){
			nameParam += name+"%";
			text += " and a.name like :nameParam ";
		}
		if (level != null && level.length() > 0){
			levelParam += level+"%";
			text += " and a.level like :levelParam ";
		}
		if (deviceAddress != null && deviceAddress.length() > 0){
			deviceAddressParam += deviceAddress+"%";
			text += " and STR(a.deviceAddress) like :deviceAddressParam ";
		}
		if (deviceId != null && deviceId.length() > 0){
			deviceIdParam += deviceId+"%";
			text += " and STR(a.deviceId) like :deviceIdParam ";
		}
		if (groupId != null && groupId.length() > 0){
			groupIdParam += groupId+"%";
			text += " and STR(a.groupId) like :groupIdParam ";
		}
		if (isCoverLevel != null ){
			isSendEmailParam = Boolean.valueOf(isCoverLevel);
			text += " and a.isSendEmail = :isSendEmailParam ";
		}	
		if ( alertCriteriaId != null) {
			text += " and a.alertCriteriaId = :alertCriteriaId ";
		}
		
		if ( fromDate != null && fromDate.length() > 0  
				&& toDate != null && toDate.length() > 0) {
			text += " and a.fromDate >= :dateFromParam AND a.toDate <= :dateToParam ";
		}
		
		
		text = "select a from " + AlertSetting.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (name != null && name.length() > 0){
			query.setParameter("nameParam", nameParam);
		}
		if (level != null && level.length() > 0){
			query.setParameter("levelParam", levelParam);
		}
		if (deviceAddress != null && deviceAddress.length() > 0){
			query.setParameter("deviceAddressParam", deviceAddressParam);
		}
		if (deviceId != null && deviceId.length() > 0){
			query.setParameter("deviceIdParam", deviceIdParam);
		}
		if (groupId != null && groupId.length() > 0){
			query.setParameter("groupIdParam", groupIdParam);
		}
		if (isCoverLevel != null){
			query.setParameter("isSendEmailParam", isSendEmailParam);
		}
		if ( alertCriteriaId != null) {
			query.setParameter("alertCriteriaId", alertCriteriaId);
		}
		
		if ( fromDate != null && fromDate.length() > 0  
				&& toDate != null && toDate.length() > 0) {
			
			Date dateFromDate = new Date( Long.valueOf(fromDate));
			Date dateToDate = new Date( Long.valueOf(toDate));
			
			query.setParameter("dateFromParam", dateFromDate);
			query.setParameter("dateToParam", dateToDate);
		}
				
		List<AlertSetting> result = query.getResultList();
		List<AlertSetting> finalResult = new ArrayList<AlertSetting>();
		
		for (AlertSetting alertSetting : result) {
	    	AlertCriteria alertCriteria = retrieveAlertCriteria(alertSetting.getAlertCriteriaId());
	    	if ( alertCriteria == null) 
	    		continue;
	    	if (isCoverLevel != null && alertCriteria.getIsCoverLevel() != Boolean.valueOf(isCoverLevel).booleanValue() )
	    		continue;
	    	if (isWaterLevel != null && alertCriteria.getIsWaterLevel() != Boolean.valueOf(isWaterLevel).booleanValue() )
	    		continue;
	    	if (isCo2 != null && alertCriteria.getIsCo2() != Boolean.valueOf(isCo2).booleanValue() )
	    		continue;	   
	    	if (isH2s != null && alertCriteria.getIsH2s() != Boolean.valueOf(isH2s).booleanValue() )
	    		continue;	    	
	    	if (isSo2 != null && alertCriteria.getIsSo2() != Boolean.valueOf(isSo2).booleanValue() )
	    		continue;
	     	if (isCh4 != null && alertCriteria.getIsCh4() != Boolean.valueOf(isCh4).booleanValue() )
	    		continue;	    	    	
	     	if (isNh3 != null && alertCriteria.getIsNh3() != Boolean.valueOf(isNh3).booleanValue() )
	    		continue;		    	    	
	     	if (isO2 != null && alertCriteria.getIsO2() != Boolean.valueOf(isO2).booleanValue() )
	    		continue;		    	    	
	    	
	    	if ( determineCriteriaDescription ) {
		    	if ( alertSetting.getUseSiteId() ) {
		    		
		    		List<AlertSettingSensorConfig> alertSettingSensorConfigs = alertSettingSensorConfigDao.searchByCriteria(null, alertSetting.getId(), null);
		    		if ( alertSettingSensorConfigs == null)
		    			continue;

		    		boolean isAdded = false;
		    		for (AlertSettingSensorConfig assc : alertSettingSensorConfigs) {
		    			TagConfiguration tagConfiguration =	tagConfigurationDao.retrieveTagConfiguration(assc.getTagConfigurationId());
		    			List<SensorConfig> sensorConfigs = sensorConfigDao.retrieveSensorByTagConfigurationId(tagConfiguration.getId(), false);
		    		    if (sensorConfigs != null && sensorConfigs.size() > 0) {
		    		    	SensorConfig sensorConfig = sensorConfigs.get(0);
		    		    	
		    		    	if (sensorConfig.getIsCoverLevel() == alertCriteria.getIsCoverLevel())
		    		    		alertSetting.setCoverLevelHtml(alertCriteria.getCoverLevelCriteria());
		    		    	
		    		    	if (sensorConfig.getIsWaterLevel() == alertCriteria.getIsWaterLevel())
		    		    		alertSetting.setWaterLevelHtml(alertCriteria.getWaterLevelCriteria());
		    		    	
		    		    	if (sensorConfig.getIsCo2() == alertCriteria.getIsCo2())
		    		    		alertSetting.setCo2Html(alertCriteria.getCo2Criteria());	
		    		
		    		    	if (sensorConfig.getIsH2s() == alertCriteria.getIsH2s())
		    		    		alertSetting.setH2sHtml(alertCriteria.getH2sCriteria());	

		    		    	if (sensorConfig.getIsSo2() == alertCriteria.getIsSo2())
		    		    		alertSetting.setSo2Html(alertCriteria.getSo2Criteria());	
		    		    	
		    		    	if (sensorConfig.getIsCh4() == alertCriteria.getIsCh4())
		    		    		alertSetting.setCh4Html(alertCriteria.getCh4Criteria());	
		    		    	
		    		    	if (sensorConfig.getIsNh3() == alertCriteria.getIsNh3())
		    		    		alertSetting.setNh3Html(alertCriteria.getNh3Criteria());	
		    		    	
		    		    	if (sensorConfig.getIsO2() == alertCriteria.getIsO2())
		    		    		alertSetting.setO2Html(alertCriteria.getO2Criteria());	
		    		    }
		    		    if ( tagConfigurationId != null ) {
	    		    		if (tagConfigurationId.longValue() == assc.getTagConfigurationId() && !isAdded ) {
	    		    			finalResult.add(alertSetting);
	    		    			isAdded = true;
	    		    		}
		    		    } else if ( !isAdded) {
		    		    	finalResult.add(alertSetting);
		    		    	isAdded = true;
		    		    }
		    		}
		    	} else {
	    			alertSetting.setCoverLevelHtml(alertCriteria.getCoverLevelCriteria());
		    		alertSetting.setWaterLevelHtml(alertCriteria.getWaterLevelCriteria());
		    		alertSetting.setCo2Html(alertCriteria.getCo2Criteria());	
		    		alertSetting.setH2sHtml(alertCriteria.getH2sCriteria());	
		    		alertSetting.setSo2Html(alertCriteria.getSo2Criteria());	
		    		alertSetting.setCh4Html(alertCriteria.getCh4Criteria());	
		    		alertSetting.setNh3Html(alertCriteria.getNh3Criteria());	
		    		alertSetting.setO2Html(alertCriteria.getO2Criteria());	
		    		finalResult.add(alertSetting);
    		   } 
	    	}
	    	alertSetting.setAlertCriteria(alertCriteria);
		    	
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			
	    	Date fromDateD = alertSetting.getFromDate();
	    	if ( fromDateD != null) {
	    		alertSetting.setFromDateStr(df.format(fromDateD));
	    	}
	    	
	    	Date toDateD = alertSetting.getToDate();
	    	if ( toDateD != null) {
	    		alertSetting.setToDateStr(df.format(toDateD));
	    	}
	    	
	    	List<String> html = new ArrayList<String>();
	    	if (alertSetting.getIsSendEmail()) {
	    		
	    		String emails = alertSetting.getEmails();
	    		if ( emails != null && emails.length() > 0) {
	    			String[] emailAddresses = emails.split(";");
	    		    for ( String emailAddr : emailAddresses ) {
	    		    	html.add(emailAddr);
	    		    }
	    		}
	    	} else 
		    	html.add(String.valueOf(alertSetting.getIsSendEmail()));
	    	
	    	alertSetting.setEmailsHtml(html.toArray(new String[html.size()]));	  
	    	SiteConfiguration siteConfiguration = siteConfigurationDao.retrieveSiteConfiguration(alertSetting.getSiteId());
	    	if ( siteConfiguration != null) {
	    		alertSetting.setSiteConfiguration(siteConfiguration);
	    	}
		}
		return finalResult;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public AlertCriteria retrieveAlertCriteria(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ AlertCriteria.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<AlertCriteria> result = query.getResultList();
		if (result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public SensorConfig retrieveSensorConfig(Long sensorConfigId) {
		Query query = getEm().createQuery("SELECT a FROM "+ SensorConfig.class.getName()+" a "
				+ " where a.sensorId= :sensorConfigId");
		query.setParameter("sensorConfigId", sensorConfigId);
		List<SensorConfig> result = query.getResultList();
		if (result.size() > 0){
			return result.get(0);
		}
		return null;
	}

	public AlertSettingSensorConfigDao getAlertSettingSensorConfigDao() {
		return alertSettingSensorConfigDao;
	}

	public void setAlertSettingSensorConfigDao(AlertSettingSensorConfigDao alertSettingSensorConfigDao) {
		this.alertSettingSensorConfigDao = alertSettingSensorConfigDao;
	}

	public TagConfigurationDao getTagConfigurationDao() {
		return tagConfigurationDao;
	}

	public void setTagConfigurationDao(TagConfigurationDao tagConfigurationDao) {
		this.tagConfigurationDao = tagConfigurationDao;
	}

	public SensorConfigDao getSensorConfigDao() {
		return sensorConfigDao;
	}

	public void setSensorConfigDao(SensorConfigDao sensorConfigDao) {
		this.sensorConfigDao = sensorConfigDao;
	}

	public SiteConfigurationDao getSiteConfigurationDao() {
		return siteConfigurationDao;
	}

	public void setSiteConfigurationDao(SiteConfigurationDao siteConfigurationDao) {
		this.siteConfigurationDao = siteConfigurationDao;
	}

}
