package com.xmlasia.gemsx.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Alert;

public class AlertDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Alert createAlert(Alert a) {
		return (Alert) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Alert updateAlert(Alert a) {
		return (Alert) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteAlert(long id) {
		delete(Alert.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Alert retrieveAlert(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ Alert.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<Alert> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Alert> searchByCriteria(String id, String fromDate, String toDate, String level, String alertSettingId, String tagConfigurationId, String descending) {
	
		Date dateFromDate = new Date( Long.valueOf(fromDate));
		Date dateToDate = new Date( Long.valueOf(toDate));

		String text = "";
		text += " and a.createDateTime between :dateFromParam AND :dateToParam ";
		
		if (FieldUtil.isFieldNonBlank(id)) {
			text += " and a.id = :idParam";
		}
		
		if (level != null && !level.equals("All")) {
			text += " and a.alertSettingLevel = :levelParam "; 
		}
		if (alertSettingId != null) {
			text += " and a.alertSettingId = :alertSettingId "; 
		}
		if (tagConfigurationId != null) {
			text += " and a.tagConfigurationId = :tagConfigurationId";
		}
		
		text = "select a from " + Alert.class.getName() + " a "
				+ " where 1=1 " + text;
		
		if ( Boolean.valueOf(descending) == true) {
			text += " order by a.createDateTime desc";
		}
		Query query = getEm().createQuery(text);
		query.setParameter("dateFromParam", dateFromDate);
		query.setParameter("dateToParam", dateToDate);
		
		if (FieldUtil.isFieldNonBlank(id)) {
			query.setParameter("idParam", Long.valueOf(id));
		}
		if (level != null && !level.equals("All")) {
			query.setParameter("levelParam", level);
		}
		if (alertSettingId != null) {
			query.setParameter("alertSettingId", Long.valueOf(alertSettingId));
		}
		if (tagConfigurationId != null) {
			query.setParameter("tagConfigurationId", Long.valueOf(tagConfigurationId));
		}
		
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		DateFormat df2 = new SimpleDateFormat("HH:mm:ss");
		
		List<Alert> result = query.getResultList();
		for (Alert alert : result) {
	    
			String dateTimeString = df.format(alert.getCreateDateTime());
			alert.setCreateDateTimeStr(dateTimeString);
		
	    	String timeString = df2.format(alert.getCreateDateTime());
			alert.setCreateTimeStr(timeString);
			
	    	String alertMessage = Alert.createMessage(alert, System.lineSeparator());
	    	if ( alertMessage != null && alertMessage.length() > 0 ) {
				alert.setMessageHtml(alertMessage);
			}
	    	
	    	alert.enrichMobileAlertIcon(alert.getAlertSettingLevel());
		}
		
		return result;
	}
	
}
