package com.xmlasia.gemsx.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.GroupMenuMapping;
import com.xmlasia.gemsx.domain.Menu;
import com.xmlasia.gemsx.domain.User;

public class GroupMenuMappingDao extends BaseDao {

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<GroupMenuMapping> searchByCriteria(Long id, 
			Long groupId,
			Long moduleId,
			Long menuId,
			Long permissionId )  {
		
		String text = "";
		
		if ( id != null ) {
			text += " and a.id = :idParam ";
		}
		if ( groupId != null ) {
			text += " and a.groupId = :groupIdParam ";
		}
		if ( moduleId != null ) {
			text += " and a.moduleId = :moduleIdParam ";
		}
		if ( menuId != null ) {
			text += " and a.menuId = :menuIdParam ";
		}
		if ( permissionId != null ) {
			text += " and a.permissionId = :permissionIdParam ";
		}				
		
		text = "select a from " + GroupMenuMapping.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);

		
		if ( id != null ) {
			query.setParameter("idParam", id);
		}
		if ( groupId != null ) {
			query.setParameter("groupIdParam", groupId);
		}
		if ( moduleId != null ) {
			query.setParameter("moduleIdParam", moduleId);			
		}
		if ( menuId != null ) {
			query.setParameter("menuIdParam", menuId);
		}	
		if ( permissionId != null ) {
			query.setParameter("permissionIdParam", permissionId);
		}	
		
		List<GroupMenuMapping> result = query.getResultList();
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Menu> getMenus(User user, Long moduleId, String searchText) {
		String text = "select m from "+ Menu.class.getName()+" m "
				+ " where m.moduleId = :moduleId order by m.displayOrder ";
		
		Query query = getEm().createQuery(text);
		query.setParameter("moduleId", moduleId);
		if (searchText != null){
			query.setParameter("searchText", "%" + searchText + "%");
		}
		
		List<Menu> menus = query.getResultList();
		if ( menus != null && menus.size() > 0) {
			List<Menu> assignedMenus = new ArrayList<Menu>();
			for (Menu menu : menus) {
				
				List<GroupMenuMapping> groupMenuMappings = searchByCriteria(null, user.getGroupId(), moduleId, menu.getId(), null );
				if ( groupMenuMappings != null && groupMenuMappings.size() > 0) {
					for (GroupMenuMapping grpMenuMapping : groupMenuMappings) {
						if (grpMenuMapping.getMenuId() == menu.getId() && menu.getSideMenu()) {
							assignedMenus.add(menu);
							break;
						}
					}
				}
				
			}
			//return assignedMenus;
			return assignedMenus; 
		}

		return new ArrayList<Menu>();
	}	
}
