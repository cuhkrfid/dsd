package com.xmlasia.gemsx.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Alert;
import com.xmlasia.gemsx.domain.GroupTagConfiguration;
import com.xmlasia.gemsx.domain.GroupTagConfigurationMap;
import com.xmlasia.gemsx.domain.SiteConfiguration;
import com.xmlasia.gemsx.domain.TagConfiguration;

public class GroupTagConfigurationDao extends BaseDao{

	private SiteConfigurationDao siteConfigurationDao;
	private GroupTagConfigurationMapDao groupTagConfigurationMapDao;
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public GroupTagConfiguration createGroupTagConfiguration(GroupTagConfiguration a) {
		a.setCreatedDate(new Date());
		return (GroupTagConfiguration) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public GroupTagConfiguration updateGroupTagConfiguration(GroupTagConfiguration a) {
		return (GroupTagConfiguration) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteGroupTagConfiguration(long id) {
		delete(GroupTagConfiguration.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public GroupTagConfiguration retrieveGroupTagConfiguration(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ GroupTagConfiguration.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<GroupTagConfiguration> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<GroupTagConfiguration> searchByCriteria(String id, String fromDate, String toDate, String siteConfigurationId, String tagConfigurationId, String alertId, String name, String descending) {
		
		List<GroupTagConfiguration> groupTagConfigurations = new ArrayList<GroupTagConfiguration>();
		
		String text = "";
		if ( FieldUtil.isFieldNonBlank(id)) {
			text += " and a.id = :id";
		} 
		
		if ( FieldUtil.isFieldNonBlank(siteConfigurationId)) {
			text += " and a.siteConfigurationId = :siteConfigurationId";
		} 
	
		if ( FieldUtil.isFieldNonBlank(name)) {
			text += " and a.name = :name";
		} 
		
		text = "select a from " + GroupTagConfiguration.class.getName() + " a "
				+ " where 1=1 " + text;
		
		if ( Boolean.valueOf(descending) == true) {
			text += " order by a.updateDateTime desc";
		}
		
		Query query = getEm().createQuery(text);
		
		if ( FieldUtil.isFieldNonBlank(id)) {
			query.setParameter("id", Long.valueOf(id));
		}

		if ( FieldUtil.isFieldNonBlank(siteConfigurationId)) {
			query.setParameter("siteConfigurationId", Long.valueOf(siteConfigurationId));
		}
		
		if ( FieldUtil.isFieldNonBlank(name)) {
			query.setParameter("name", name);
		}
		
		List<GroupTagConfiguration> result = query.getResultList();
		for (GroupTagConfiguration groupTagConfiguration : result) {
			
			List<GroupTagConfigurationMap> groupTagConfigurationMaps = groupTagConfigurationMapDao.searchByCriteria(fromDate, toDate, groupTagConfiguration.getId(), tagConfigurationId, alertId);
			List<TagConfiguration> tagConfigurations = new ArrayList<TagConfiguration>();
			List<Alert> alerts = new ArrayList<Alert>();
			
			if ( groupTagConfigurationMaps != null && groupTagConfigurationMaps.size() > 0) {
				for ( GroupTagConfigurationMap groupTagConfigurationMap : groupTagConfigurationMaps) {
					tagConfigurations.add( groupTagConfigurationMap.getTagConfiguration());
					alerts.addAll(groupTagConfigurationMap.getTagConfiguration().getAlerts());	
				}
			}
			
			SiteConfiguration siteConfiguration = siteConfigurationDao.retrieveSiteConfiguration(groupTagConfiguration.getSiteConfigurationId());
			groupTagConfiguration.setSiteConfiguration(siteConfiguration);
			groupTagConfiguration.setTagConfigurations(tagConfigurations);
			groupTagConfiguration.setAlerts(alerts);
		
			groupTagConfigurations.add(groupTagConfiguration);
		}
		
		return groupTagConfigurations;
	}


	public GroupTagConfigurationMapDao getGroupTagConfigurationMapDao() {
		return groupTagConfigurationMapDao;
	}

	public void setGroupTagConfigurationMapDao(GroupTagConfigurationMapDao groupTagConfigurationMapDao) {
		this.groupTagConfigurationMapDao = groupTagConfigurationMapDao;
	}

	public SiteConfigurationDao getSiteConfigurationDao() {
		return siteConfigurationDao;
	}

	public void setSiteConfigurationDao(SiteConfigurationDao siteConfigurationDao) {
		this.siteConfigurationDao = siteConfigurationDao;
	}


}
