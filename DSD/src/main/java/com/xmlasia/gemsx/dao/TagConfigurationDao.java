package com.xmlasia.gemsx.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.TagConfiguration;
import com.xmlasia.gemsx.domain.SensorConfig;

public class TagConfigurationDao extends BaseDao {

	private String tagImageFilePath = System.getProperty("catalina.base") + "\\wtpwebapps\\GEMSX\\tagImageFiles\\"; 
	//"C:\\apache-tomcat-8.0.32\\webapps\\GEMSX\\mapFiles\\";
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public TagConfiguration createTagConfiguration(TagConfiguration a) {
		return (TagConfiguration) create(a);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public TagConfiguration updateTagConfiguration(TagConfiguration a) {
		return (TagConfiguration) update(a);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void deleteTagConfiguration(long id) {
		delete(TagConfiguration.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public TagConfiguration retrieveTagConfiguration(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ TagConfiguration.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<TagConfiguration> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<TagConfiguration> listAll() {
		return getEm().createQuery("SELECT a FROM "+ TagConfiguration.class.getName()+" a ").getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Integer> listAllGroupIds() {
		return getEm().createQuery("SELECT distinct a.groupId FROM "+ TagConfiguration.class.getName()+" a order by a.groupId asc").getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<TagConfiguration> searchByCriteria(String id, String name, String deviceAddress, String deviceId, String groupId, String description, String siteName, Long siteId)  {
		
		Long idParam = null;
		String nameParam = "%";
		String deviceAddressParam = "";
			
		String descriptionParam = "%";
		String siteNameParam = "%";
		
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam = Long.valueOf(id);
			text += " and a.id = :idParam ";
		}
		if (name != null && name.length() > 0){
			nameParam += name;
			text += " and a.name = :nameParam ";
		}
		if (deviceAddress != null && deviceAddress.length() > 0){
			deviceAddressParam += deviceAddress+"%";
			text += " and STR(a.deviceAddress) like :deviceAddressParam ";
		}
		if (deviceId != null && deviceId.length() > 0){
			text += " and a.deviceId = :deviceIdParam ";
		}
		if (groupId != null && groupId.length() > 0){
			text += " and a.groupId = :groupIdParam ";
		}
		if (description != null && description.length() > 0){
			descriptionParam += description+"%";
			text += " and a.description like :descriptionParam ";
		}
		if (siteName != null && siteName.length() > 0){
			siteNameParam += siteName+"%";
			text += " and STR(a.siteConfiguration.name) like :siteNameParam ";
		}		
		if (siteId != null ){
			text += " and a.siteConfiguration.id = :siteIdParam ";
		}		
				
			
		text = "select a from " + TagConfiguration.class.getName() + " a "
				+ " where 1=1 " + text + " order by a.deviceId asc";
		Query query = getEm().createQuery(text);
		
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (name != null && name.length() > 0){
			query.setParameter("nameParam", nameParam);
		}
		if (deviceAddress != null && deviceAddress.length() > 0){
			query.setParameter("deviceAddressParam", deviceAddressParam);
		}
		if (deviceId != null && deviceId.length() > 0){
			query.setParameter("deviceIdParam", Integer.valueOf(deviceId));
		}
		if (groupId != null && groupId.length() > 0){
			query.setParameter("groupIdParam", Integer.valueOf(groupId));
		}
		if (description != null && description.length() > 0){
			query.setParameter("descriptionParam", descriptionParam);
		}
		if (siteName != null && siteName.length() > 0){
			query.setParameter("siteNameParam", siteNameParam);
		}
		if (siteId != null ){
			query.setParameter("siteIdParam", siteId);
		}				

		List<TagConfiguration> result = query.getResultList();
		return result;
	}
	
	public TagConfiguration setSensorTypeHtml(TagConfiguration tagConfig) {
		
		Set<String> htmlSensors = new HashSet<String>();
		SensorConfig[] sensorConfigs = tagConfig.getSensorConfigs();
		for (SensorConfig sensorConfig : sensorConfigs) {
			
			String sensorTypeHtml = "";
			if (sensorConfig.getIsCoverLevel()) {
				htmlSensors.add("Cover level");
				sensorTypeHtml += "Cover level,";
				tagConfig.setIsCoverLevel(true);
			} 
			
			if (sensorConfig.getIsWaterLevel()) {
				htmlSensors.add("Water level");
				sensorTypeHtml += "Water level,";
				tagConfig.setIsWaterLevel(true);
			}

			if (sensorConfig.getIsH2s()) {
				htmlSensors.add("H2s");
				sensorTypeHtml += "H2s,";
				tagConfig.setIsH2s(true);
			} 
			
			if (sensorConfig.getIsSo2()) {
				htmlSensors.add("So2");
				sensorTypeHtml += "So2,";
				tagConfig.setIsSo2(true);
			} 
			
			if (sensorConfig.getIsCo2()) {
				htmlSensors.add("Co2");
				sensorTypeHtml += "Co2,";
				tagConfig.setIsCo2(true);
			}
			
			if (sensorConfig.getIsCh4()) {
				htmlSensors.add("Ch4");	
				sensorTypeHtml += "Ch4,";
				tagConfig.setIsCh4(true);
			}

			if (sensorConfig.getIsNh3()) {
				htmlSensors.add("Nh3");	
				sensorTypeHtml += "Nh3,";
				tagConfig.setIsNh3(true);
			}
			
			if (sensorConfig.getIsO2()) {
				htmlSensors.add("O2");	
				sensorTypeHtml += "O2,";
				tagConfig.setIsO2(true);
			}
			
			if ( sensorTypeHtml.length() > 1) {
				sensorTypeHtml = sensorTypeHtml.substring(0, sensorTypeHtml.length() - 1); 
			} 
			
			sensorConfig.setSensorTypeHtml(sensorTypeHtml);
			sensorConfig.setName(tagConfig.getName());
			sensorConfig.setDescription(tagConfig.getDescription());
			sensorConfig.setDeviceId(tagConfig.getDeviceId());
			sensorConfig.setDeviceAddress(tagConfig.getDeviceAddress());
			sensorConfig.setMeasurePeriod(tagConfig.getMeasurePeriod());
		}
		
		String[] htmlArray = htmlSensors.toArray(new String[htmlSensors.size()]);
		tagConfig.setSensorTypeHtml(htmlArray);
		return tagConfig;
	}
	
	public void writeMapFileBase64(String imageUriBase64Data, TagConfiguration tagConfig, String mapFileName ) {
		if (imageUriBase64Data != null) {
			
			byte[] fileInBytes = Base64.decodeBase64(imageUriBase64Data);
			try {
				FileOutputStream  os = new FileOutputStream(new File(getMapFileFullPath(tagConfig, mapFileName)));
				tagConfig.setImageUrlPath(getImageURLSubPath() + getFileName(tagConfig, mapFileName));
				tagConfig.setImageFileName(getFileName(tagConfig, mapFileName));
				
				os.write(fileInBytes, 0, fileInBytes.length);
		        os.flush();
		        os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		
	}
	
	public void writeMapFile(InputStream fileData, TagConfiguration tagConfig, String mapFileName) {
		if (fileData != null) {
			
			byte[] fileInBytes;
			try {
				fileInBytes = IOUtils.toByteArray(fileData);
				
				OutputStream os = new FileOutputStream(getMapFileFullPath(tagConfig, mapFileName));
				tagConfig.setImageUrlPath(getImageURLSubPath() + getFileName(tagConfig, mapFileName));
				tagConfig.setImageFileName(getFileName(tagConfig, mapFileName));
				
	        	os.write(fileInBytes, 0, fileInBytes.length);
		        os.flush();
		        os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public boolean deleteMapFile(TagConfiguration tagConfig, String mapFileName) {
		File file = new File(getMapFileFullPath(tagConfig, mapFileName));
		tagConfig.setImageFileName(null);
		tagConfig.setImageUrlPath(null);
		return file.delete();
	}

	private String getMapFileFullPath(TagConfiguration tagConfig, String mapFileName) {
		return getTagImageFilePath() + getFileName(tagConfig, mapFileName);
	}

	private String getFileName(TagConfiguration tagConfig, String mapFileName) {
		return "Id_" + tagConfig.getId() + "_" + mapFileName;
	}
	

	public String getTagImageFilePath() {
		return tagImageFilePath;
	}

	public void setTagImageFilePath(String tagImageFilePath) {
		this.tagImageFilePath = System.getProperty("catalina.base") + tagImageFilePath;
	}

	public String getImageURLSubPath() {
		return "./tagImageFiles/";
	}
}
