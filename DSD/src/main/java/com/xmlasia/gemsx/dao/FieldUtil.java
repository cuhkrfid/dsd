package com.xmlasia.gemsx.dao;

public class FieldUtil {

	public static boolean isFieldNonBlank(String field) {
		
		return field != null && !field.equals("null");
	};
}
