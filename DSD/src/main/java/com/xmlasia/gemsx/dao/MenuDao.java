package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.xmlasia.gemsx.domain.Menu;

public class MenuDao extends BaseDao{

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Menu updateMenu(Menu m) {
		return toProxyData((Menu) update(m));
	}

	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Menu retrieveMenu(long id) {
		Query query = getEm().createQuery("SELECT me FROM "+ Menu.class.getName()+" me "
				+ " left join fetch me.module "
				+ " where me.id= :id");
		query.setParameter("id", id);
		List<Menu> result = query.getResultList();
		if ( result != null && result.size() > 0) {
			Menu menu = result.get(0);
			return toProxyData(menu);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Menu> retrieveMenuByModuleId(long moduleId) {
		Query query = getEm().createQuery("SELECT me FROM "+ Menu.class.getName()+" me "
				+ " where me.moduleId= :moduleId");
		query.setParameter("moduleId", moduleId);
		List<Menu> result = query.getResultList();
		return toProxyData(result);
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Menu> searchByCriteria(String id, String name, String module, String description,
			Integer displayOrder, Boolean active, String path, Boolean useOrder ) {
		
		String idParam = "%";
		String nameParam = "%";
		String descriptionParam = "%";
		Integer displayOrderParam = null;
		Boolean activeParam = null;
		String pathParam = "%";
		String moduleParam = "%";
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id+"%";
			text += " and STR(me.id) like :idParam ";
		}
		if (name != null && name.length() > 0){
			nameParam += name+"%";
			text += " and me.name like :nameParam ";
		}
		if (description != null && description.length() > 0){
			descriptionParam += description+"%";
			text += " and me.description like :descriptionParam ";
		}
		if (displayOrder != null ){
			displayOrderParam = displayOrder;
			text += " and me.displayOrder = :displayOrderParam ";
		}
		if (active != null ){
			activeParam = active;
			text += " and me.active = :activeParam ";
		}
		if (path != null && path.length() > 0){
			pathParam += path+"%";
			text += " and me.path like :pathParam ";
		}
		if (module != null && module.length() > 0){
			moduleParam += module+"%";
			text += " and m.name like :moduleParam ";
		}
		
		if ( useOrder != null && useOrder.equals(true)) {
			text += " order by me.displayOrder";
		} else {
			text += " order by me.id ";
		}
			
		text = "select me from " + Menu.class.getName() + " me "
				+ " left join fetch me.module m "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (name != null && name.length() > 0){
			query.setParameter("nameParam", nameParam);
		}
		if (description != null && description.length() > 0){
			query.setParameter("descriptionParam", descriptionParam);
		}
		if (displayOrder != null ){
			query.setParameter("displayOrderParam", displayOrderParam);
		}
		if (active != null ){
			query.setParameter("activeParam", activeParam);
		}
		if (path != null && path.length() > 0){
			query.setParameter("pathParam", pathParam);
		}
		if (module != null && module.length() > 0){
			query.setParameter("moduleParam", moduleParam);
		}
		
		List<Menu> result = query.getResultList();
		return toProxyData( result );
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Menu> listAll() {
		return toProxyData( getEm().createQuery("SELECT me FROM "+ Menu.class.getName()+" me ").getResultList() );
	}
	
	public List<Menu> toProxyData(List<Menu> menu) {
		if ( menu != null && menu.size() > 0) {
			for (Menu item : menu) {
				
				if ( item.getModule() != null)
					item.getModule().setMenus(null);
			}
		}
		return menu;
	}
	
	public Menu toProxyData(Menu menu) {
		if ( menu != null && menu.getModule() != null) {
			menu.getModule().setMenus(null);
		}
		return menu;
	}
}
