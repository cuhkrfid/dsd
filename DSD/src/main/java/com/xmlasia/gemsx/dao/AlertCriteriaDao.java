package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.AlertCriteria;

public class AlertCriteriaDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public AlertCriteria createAlertCriteria(AlertCriteria a) {
		return (AlertCriteria) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public AlertCriteria updateAlertCriteria(AlertCriteria a) {
		return (AlertCriteria) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteAlertCriteria(long id) {
		delete(AlertCriteria.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public AlertCriteria retrieveAlertCriteria(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ AlertCriteria.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<AlertCriteria> result = query.getResultList();
		if (result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<AlertCriteria> searchByCriteria(
			String id, 
			String name,
			String isCoverLevel, 
			String isWaterLevel, 
			String isH2s, 
			String isSo2, 
			String isCo2,
			String isCh4,
			String isNh3,
			String isO2) { 
		
		Long idParam = null;
		String nameParam = "%";
			
		Boolean isCoverLevelParam = null;
		Boolean isWaterLevelParam = null;
		Boolean isH2sParam = null;
		Boolean isSo2Param = null;
		Boolean isCo2Param = null;
		Boolean isCh4Param = null;	
		Boolean isNh3Param = null;			
		Boolean isO2Param = null;		
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam = Long.valueOf(id);
			text += " and a.id = :idParam ";
		}
		if (name != null && name.length() > 0){
			nameParam += name+"%";
			text += " and a.name like :nameParam ";
		}
	
		if (isCoverLevel != null ){
			isCoverLevelParam = Boolean.valueOf(isCoverLevel);
			text += " and a.isCoverLevel = :isCoverLevelParam ";
		}		
		if (isWaterLevel != null ){
			isWaterLevelParam = Boolean.valueOf(isWaterLevel);
			text += " and a.isWaterLevel = :isisWaterLevelram ";
		}			
		if (isH2s != null ){
			isH2sParam = Boolean.valueOf(isH2s);
			text += " and a.isH2s = :isH2sParam ";
		}				
		if (isSo2 != null ){
			isSo2Param = Boolean.valueOf(isSo2);
			text += " and a.isSo2 = :isSo2Param ";
		}			
		if (isCo2 != null ){
			isCo2Param = Boolean.valueOf(isCo2);
			text += " and a.isCo2 = :isCo2Param ";
		}			
		if (isCo2 != null ){
			isCh4Param = Boolean.valueOf(isCh4);
			text += " and a.isCh4 = :isCh4Param ";
		}	
		if (isNh3 != null ){
			isNh3Param = Boolean.valueOf(isNh3);
			text += " and a.isNh3 = :isNh3Param ";
		}	
		if (isO2 != null ){
			isO2Param = Boolean.valueOf(isO2);
			text += " and a.isO2 = :isO2Param ";
		}	
				
		
		text = "select a from " + AlertCriteria.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (name != null && name.length() > 0){
			query.setParameter("nameParam", nameParam);
		}
		if (isCoverLevel != null){
			query.setParameter("isCoverLevelParam", isCoverLevelParam);
		}
		if (isWaterLevel != null){
			query.setParameter("isWaterLevelParam", isWaterLevelParam);
		}		
		if (isH2s != null){
			query.setParameter("isH2sParam", isH2sParam);
		}		
		if (isSo2 != null){
			query.setParameter("isSo2Param", isSo2Param);
		}
		if (isCo2 != null){
			query.setParameter("isCo2Param", isCo2Param);
		}	
		if (isCh4 != null){
			query.setParameter("isCh4Param", isCh4Param);
		}	
		if (isNh3 != null){
			query.setParameter("isNh3Param", isNh3Param);
		}			
		if (isO2 != null){
			query.setParameter("isO2Param", isO2Param);
		}			
		
		
		List<AlertCriteria> result = query.getResultList();
		for (AlertCriteria alertCriteria : result) {
			alertCriteria.setCoverLevelHtml(alertCriteria.getCoverLevelCriteria());
			alertCriteria.setWaterLevelHtml(alertCriteria.getWaterLevelCriteria());
			alertCriteria.setH2sHtml(alertCriteria.getH2sCriteria());
			alertCriteria.setSo2Html(alertCriteria.getSo2Criteria());
			alertCriteria.setCo2Html(alertCriteria.getCo2Criteria());
			alertCriteria.setCh4Html(alertCriteria.getCh4Criteria());
			alertCriteria.setNh3Html(alertCriteria.getNh3Criteria());
			alertCriteria.setO2Html(alertCriteria.getO2Criteria());
		}
		
		return result;
	}
			
}
