package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.DeviceToReader;
import com.xmlasia.gemsx.domain.Reader;

public class DeviceToReaderDao extends BaseDao {

	private ReaderDao readerDao;
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public DeviceToReader createTagConfigurationToReader(DeviceToReader a) {
		Date date = new Date();
		a.setCreateDateTime(date);
		a.setUpdateDateTime(date);
		return (DeviceToReader) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public DeviceToReader updateTagConfigurationToReader(DeviceToReader a) {
		Date date = new Date();
		a.setUpdateDateTime(date);
		return (DeviceToReader) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteTagConfigurationToReader(long id) {
		delete(DeviceToReader.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public DeviceToReader retrieveTagConfigurationToReader(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ DeviceToReader.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<DeviceToReader> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public DeviceToReader createOrUpdate(DeviceToReader deviceToReader) {
		
		List<DeviceToReader> deviceToReaders = searchByCriteria(null, null, null, String.valueOf(deviceToReader.getDeviceId()), String.valueOf(deviceToReader.getReaderId()),  null, null, "true"); 
		if ( deviceToReaders != null && deviceToReaders.size() > 0 ) {
			DeviceToReader deviceToReaderToUpdate = deviceToReaders.get(0);
			deviceToReaderToUpdate.setRssi(deviceToReader.getRssi());
			return updateTagConfigurationToReader(deviceToReaderToUpdate);
		}
		
		return createTagConfigurationToReader(deviceToReader); 
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<DeviceToReader> searchByCriteria(String id, String fromDate, String toDate, String deviceId, String readerId,  String lowRssi, String highRssi,  String descending) {
		
		Date dateFromDate = null;
		Date dateToDate = null;

		String text = "";
		if ( FieldUtil.isFieldNonBlank(id)) {
			text += " and a.id = :idParam";
		} 
		
		if ( FieldUtil.isFieldNonBlank(lowRssi) &&  FieldUtil.isFieldNonBlank(highRssi)) {
			text += " and a.rssi between :rssiFromParam AND :rssiToParam ";
		}
		
		if ( FieldUtil.isFieldNonBlank(fromDate) &&  FieldUtil.isFieldNonBlank(toDate)) {
			dateFromDate = new Date( Long.valueOf(fromDate));
			dateToDate = new Date( Long.valueOf(toDate));
			text += " and a.updateDateTime between :dateFromParam AND :dateToParam ";
		} 
		
		if ( FieldUtil.isFieldNonBlank(deviceId)) {
			text += " and a.deviceId = :deviceId ";
		}
	
		if ( FieldUtil.isFieldNonBlank(readerId)) {
			text += " and a.readerId = :readerId ";
		}
		
		text = "select a from " + Reader.class.getName() + " a "
				+ " where 1=1 " + text;
		
		if ( Boolean.valueOf(descending) == true) {
			text += " order by a.updateDateTime desc";
		}
		
		Query query = getEm().createQuery(text);
		
		if ( FieldUtil.isFieldNonBlank(fromDate) &&  FieldUtil.isFieldNonBlank(toDate)) {
			query.setParameter("dateFromParam", dateFromDate);
			query.setParameter("dateToParam", dateToDate);
		}
		
		if ( FieldUtil.isFieldNonBlank(deviceId)) {
			query.setParameter("deviceId", Integer.valueOf(deviceId));
		}

		if ( FieldUtil.isFieldNonBlank(readerId)) {
			query.setParameter("readerId", Integer.valueOf(readerId));
		}
		
		if ( FieldUtil.isFieldNonBlank(id)) {
			query.setParameter("idParam", Integer.valueOf(id));
		} 
		
		if ( FieldUtil.isFieldNonBlank(lowRssi) &&  FieldUtil.isFieldNonBlank(highRssi)) {
			query.setParameter("rssiFromParam", Integer.valueOf(lowRssi));
			query.setParameter("rssiToParam", Integer.valueOf(highRssi));
		}
		
		List<DeviceToReader> result = query.getResultList();
		if ( result != null && result.size() > 0) {
			for (DeviceToReader deviceToReader : result) {
				Long readerIdLong = deviceToReader.getReaderId();	
				if ( readerIdLong != null ) {
					deviceToReader.setReader(readerDao.retrieveReader(readerIdLong)); 
				}
			}
		}
		return result;
	}

	public ReaderDao getReaderDao() {
		return readerDao;
	}

	public void setReaderDao(ReaderDao readerDao) {
		this.readerDao = readerDao;
	}
}
