package com.xmlasia.gemsx.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.GroupModuleMapping;
import com.xmlasia.gemsx.domain.Module;
import com.xmlasia.gemsx.domain.User;

public class UserDao extends BaseDao{
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public User createUser(User u) {
		return (User) create(u);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public User updateUser(User u) {
		return (User) update(u);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteUser(int id) {
		delete(User.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public User retrieveUser(int id) {
		Query query = getEm().createQuery("SELECT u FROM "+User.class.getName()+" u where u.id= :id");
		query.setParameter("id", id);
		List<User> result = query.getResultList();
		if (result.size() > 0){
			User user = result.get(0);
			return user;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<User> listAll() {
		return getEm().createQuery("SELECT u FROM "+User.class.getName()+" u ").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Module> getParentModules(String username) {
		List<Module> result = new ArrayList<Module>();
		User loginUser = findByLoginId(username);
		if (loginUser == null) {
			return result;
		}
		List<GroupModuleMapping> groupModuleMappings = getGroupModuleMapping(loginUser.getGroupId());
		if (groupModuleMappings == null ) {
			return result;
		}
		
		for(GroupModuleMapping groupModuleMapping : groupModuleMappings) {
			Query query = getEm().createQuery("SELECT u FROM "+ Module.class.getName()+" u where u.id= :moduleId");
			query.setParameter("moduleId", groupModuleMapping.getModuleId());
			List<Module> modules = query.getResultList();
			if ( modules != null && modules.size() > 0 ) {
				result.addAll(modules);
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public User findByLoginId(String username) {
		String text = "select u from "+User.class.getName()+" u "
				+ " where u.username = :username and u.active = true";
		Query query = getEm().createQuery(text);
		query.setParameter("username", username);
		List<User> result = query.getResultList();
		if (result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<User> searchByCriteria(String id, 
									   String chiName, 
									   String engName, 
									   String username, 
									   String email, 
									   String dept, 
									   String password, 
									   String phone, 
									   Boolean notification, 
									   String fax, 
									   String title,
									   Boolean pwdResetAfterLogin,
									   Boolean active) {
		String idParam = "%";
		String chiNameParam = "%";
		String engNameParam = "%";
		String userNameParam = "%";
		String emailParam = "%";
		String deptParam = "%";
		String passwordParam = "%";
		String phoneParam = "%";
		Boolean notificationParam = null;
		String faxParam = "%";
		String titleParam = "%";
		Boolean pwdResetAfterLoginParam = null;
		Boolean activeParam = null;
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id+"%";
			text += " and STR(u.id) like :idParam ";
		}
		if (chiName != null && chiName.length() > 0){
			chiNameParam += chiName+"%";
			text += " and u.chiName like :chiNameParam ";
		}
		if (engName != null && engName.length() > 0){
			engNameParam += engName+"%";
			text += " and u.engName like :engNameParam ";
		}
		if (username != null && username.length() > 0){
			userNameParam += username+"%";
			text += " and u.username like :userNameParam ";
		}
		if (email != null && email.length() > 0){
			emailParam += email+"%";
			text += " and u.email like :emailParam ";
		}
		if (password != null && password.length() > 0){
			passwordParam += password+"%";
			text += " and u.password like :passwordParam ";
		}
		if (phone != null && phone.length() > 0){
			phoneParam += phone+"%";
			text += " and u.phone like :phoneParam ";
		}
		if (notification != null ){
			notificationParam = notification;
			text += " and u.notification = :notificationParam ";
		}
		if (fax != null && fax.length() > 0){
			faxParam += dept+"%";
			text += " and u.fax like :faxParam ";
		}
		if ( pwdResetAfterLogin != null) {
			pwdResetAfterLoginParam = pwdResetAfterLogin;
			text += " and u.pwdResetAfterLogin = :pwdResetAfterLoginParam ";
		}
		if ( active != null) {
			activeParam = active;
			text += " and u.active = :activeParam ";
		}
		
		if (title != null && title.length() > 0){
			titleParam += title+"%";
			text += " and u.title like :titleParam ";
		}

		if (dept != null && dept.length() > 0){
			deptParam += dept+"%";
			text += " and u.department like :deptParam ";
		}

		
		text = "select u from " + User.class.getName() + " u "
				+ " where 1=1 " + text + " order by u.id ";
		Query query = getEm().createQuery(text);
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (chiName != null && chiName.length() > 0){
			query.setParameter("chiNameParam", chiNameParam);
		}
		if (engName != null && engName.length() > 0){
			query.setParameter("engNameParam", engNameParam);
		}
		if (username != null && username.length() > 0){
			query.setParameter("userNameParam", userNameParam);
		}
		if (email != null && email.length() > 0){
			query.setParameter("emailParam", emailParam);
		}
		if (dept != null && dept.length() > 0){
			query.setParameter("deptParam", deptParam);
		}
		if (password != null && password.length() > 0){
			query.setParameter("passwordParam", passwordParam);
		}		
		if (phone != null && phone.length() > 0){
			query.setParameter("phoneParam", phoneParam);
		}		
		if (notificationParam != null){
			query.setParameter("notificationParam", notificationParam);
		}		
		if (fax != null && fax.length() > 0){
			query.setParameter("faxParam", faxParam);
		}			
		if (title != null && title.length() > 0){
			query.setParameter("titleParam", titleParam);
		}						
		if ( pwdResetAfterLogin != null) {
			query.setParameter("pwdResetAfterLoginParam", pwdResetAfterLoginParam);
		}
		if (activeParam != null) {
			query.setParameter("activeParam", activeParam);
			
		}
		List<User> result = query.getResultList();
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<GroupModuleMapping> getGroupModuleMapping(long groupId) {

		Query query = getEm().createQuery("SELECT m FROM "+ GroupModuleMapping.class.getName()+" m "
				+ " where m.groupId= :groupId");
		query.setParameter("groupId", groupId);
		
		List<GroupModuleMapping> result = query.getResultList();
		return result;
	}
}
