package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.AlertSettingSensorConfig;

public class AlertSettingSensorConfigDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public AlertSettingSensorConfig createAlertSettingSensorConfig(AlertSettingSensorConfig a) {
		
		Date date = new Date();
		a.setCreatedDate(date);
		a.setUpdatedDate(date);
		
		return (AlertSettingSensorConfig) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public AlertSettingSensorConfig updateAlertSettingSensorConfig(AlertSettingSensorConfig a) {
		a.setUpdatedDate(new Date());
		return (AlertSettingSensorConfig) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteAlertSettingSensorConfig(long id) {
		delete(AlertSettingSensorConfig.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<AlertSettingSensorConfig> searchByCriteria(Long id, Long alertSettingId, Long tagConfigurationId) {
		
		String sql = "SELECT a FROM "+ AlertSettingSensorConfig.class.getName()+" a where 1=1 ";
		
		if ( id != null) {
			sql += " and a.id = :id";
		}
		if ( alertSettingId != null) {
			sql += " and a.alertSettingId = :alertSettingId";
		}
		if ( tagConfigurationId != null) {
			sql += " and a.tagConfigurationId = :tagConfigurationId";
		}
		
		Query query = getEm().createQuery(sql);
		
		if (id != null)
			query.setParameter("id", id);
		if (alertSettingId != null)
			query.setParameter("alertSettingId", alertSettingId);
		if (tagConfigurationId != null)
			query.setParameter("tagConfigurationId", tagConfigurationId);
		
		return query.getResultList();
	}
}
