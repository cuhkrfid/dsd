package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Group;
import com.xmlasia.gemsx.domain.UserGroupMapping;

public class UserGroupMappingDao extends BaseDao{
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Group> findAssignedGroupById(int userId) {
		Query query = getEm().createQuery("SELECT distinct ug.group FROM "+UserGroupMapping.class.getName()+" ug "
				+ " left join ug.group "
				+ " where ug.userId= :userId");
		query.setParameter("userId", userId);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Group> findUnassignedGroupById(int userId) {
		Query query = getEm().createQuery("SELECT distinct g FROM "+Group.class.getName()+" g "
				+ " where g.id not in "
				+ " ( select ug.groupId from "+UserGroupMapping.class.getName()+" ug "
				+ " where ug.userId = :userId )");
		query.setParameter("userId", userId);
		return query.getResultList();
	}
	
}
