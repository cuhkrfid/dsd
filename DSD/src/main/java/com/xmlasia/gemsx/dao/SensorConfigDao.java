package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.SensorConfig;

public class SensorConfigDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public SensorConfig createSensorConfig(SensorConfig a) {
		Date date = new Date();
		a.setCreatedDate(date);
		a.setUpdatedDate(date);
		return (SensorConfig) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public SensorConfig updateSensorConfig(SensorConfig a) {
		a.setUpdatedDate(new Date());
		return (SensorConfig) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteSensorConfig(long id) {
		delete(SensorConfig.class, id);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public SensorConfig createOrUpdate(SensorConfig sensorConfig, boolean autoConfig) {
		List<SensorConfig> existConfig = retrieveSensorByDeviceIdAndSensorId(sensorConfig.getDeviceId(), sensorConfig.getSensorId(), autoConfig, null);
		if (existConfig != null && existConfig.size() > 0) {
			existConfig.get(0).copy(sensorConfig);
			return updateSensorConfig(existConfig.get(0));
		} else {
			return createSensorConfig(sensorConfig);
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public SensorConfig updateManualSensorConfigInputDate(SensorConfig sensorConfig) {
		List<SensorConfig> existConfig = retrieveSensorByDeviceIdAndSensorId(sensorConfig.getDeviceId(), sensorConfig.getSensorId(), false, null);
		if (existConfig != null && existConfig.size() > 0) {
			existConfig.get(0).setInputDate(sensorConfig.getInputDate());
			return updateSensorConfig(existConfig.get(0));
		} 
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<SensorConfig> retrieveSensorByTagConfigurationId(long tagConfigurationId, boolean autoCreate) {
		Query query = getEm().createQuery("SELECT a FROM "+ SensorConfig.class.getName()+" a "
				+ " where a.tagConfigurationId= :tagConfigurationId and a.autoCreate = :autoCreate");
		query.setParameter("tagConfigurationId", tagConfigurationId);
		query.setParameter("autoCreate", autoCreate);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<SensorConfig> retrieveSensorByDeviceIdAndSensorId(Long deviceId, Long sensorId, Boolean autoCreate, Boolean expired) {
		
		String sql = "SELECT a FROM "+ SensorConfig.class.getName()+" a where 1=1 ";
		if (deviceId != null) {
			sql += " and a.deviceId = :deviceId";
		}
		if (sensorId != null) {
			sql += " and a.sensorId = :sensorId";
		}
		if (autoCreate != null) {
			sql += " and a.autoCreate = :autoCreate";
		}
		if (expired != null) {
			sql += " and a.expired = :expired";
		}
		sql += " order by a.sensorId asc ";
		
			
		Query query = getEm().createQuery(sql);
		
		if (deviceId != null)
			query.setParameter("deviceId", deviceId);
		if (sensorId != null)
			query.setParameter("sensorId", sensorId);
		if (autoCreate != null)		
			query.setParameter("autoCreate", autoCreate);
		if (expired != null)		
			query.setParameter("expired", expired);
		
		return query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<SensorConfig> retrieveSensorBySensorId(long sensorId, long notInTagConfigurationId) {
	
		Query query = getEm().createQuery("SELECT a FROM "+ SensorConfig.class.getName()+" a "
				+ " where a.sensorId = :sensorId and a.tagConfigurationId != :notInTagConfigurationId and a.autoCreate = false and a.expired = false");
		
		query.setParameter("sensorId", sensorId);
		query.setParameter("notInTagConfigurationId", notInTagConfigurationId);
		
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public SensorConfig retrieveSensorConfig(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ SensorConfig.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<SensorConfig> result = query.getResultList();
		if (result != null && result.size() > 0){
			return result.get(0);
		}
		return null;
	}
}