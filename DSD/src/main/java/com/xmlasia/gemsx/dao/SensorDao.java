package com.xmlasia.gemsx.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.SensorChartData;

public class SensorDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Sensor createDeviceData(Sensor a) {
		a.setCreateDate(new Date());
		return (Sensor) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Sensor updateDeviceData(Sensor a) {
		return (Sensor) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteDeviceData(long id) {
		delete(Sensor.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public SensorChartData searchMinMaxByCriteria(Boolean descending, String deviceId, String sensorId, String dateFrom, String DateTo, Integer limit) {
		Date dateFromDate = new Date( Long.valueOf(dateFrom));
		Date dateToDate = new Date( Long.valueOf(DateTo));

		String text = "";
		
		if (deviceId != null) {
			text += " and a.deviceId = :deviceIdParam ";
		}
		if (sensorId != null) {
			text += " and a.sensorId = :sensorIdParam ";
		}
		
		text += " and a.inputDate between :dateFromParam AND :dateToParam";
		text = "select min(waterLevel), max(waterLevel), min(h2s), max(h2s), min(so2), max(so2), min(co2), max(co2), min(ch4), max(ch4), min(nh3), max(nh3), min(o2), max(o2), min(rssi), max(rssi) from " + Sensor.class.getName() + " a "
				+ " where 1=1 " + text;
		
		if ( descending == true) {
			text += " order by a.inputDate desc";
		} else {
			text += " order by a.inputDate asc";
		}
			
		Query query = getEm().createQuery(text);

		if (deviceId != null) {
			query.setParameter("deviceIdParam", Integer.valueOf(deviceId));
		}
		if (sensorId != null) {
			query.setParameter("sensorIdParam", Integer.valueOf(sensorId));
		}

		query.setParameter("dateFromParam", dateFromDate);
		query.setParameter("dateToParam", dateToDate);
		
		List currentListResult = query.getResultList();
		if ( currentListResult != null) {
			
			SensorChartData sensorChartData = new SensorChartData();
			
			Object[] obj = (Object[])currentListResult.get(0);
			
			Integer maxWaterLevel = (Integer)obj[0];
			Integer minWaterLevel = (Integer)obj[1];
			
			Double minH2s = (Double)obj[2];
			Double maxH2s = (Double)obj[3];
			
			Double minCo2 = (Double)obj[4];
			Double maxCo2 = (Double)obj[5];
			
			Double minSo2 = (Double)obj[6];
			Double maxSo2 = (Double)obj[7];
			
			Double minCh4 = (Double)obj[8];
			Double maxCh4 = (Double)obj[9];
			
			Double minNh3 = (Double)obj[10];
			Double maxNh3 = (Double)obj[11];
			
			Double minO2 = (Double)obj[12];
			Double maxO2 = (Double)obj[13];
			
			Double minRssi = (Double)obj[14];
			Double maxRssi = (Double)obj[15];
			
			if ( minRssi == null) {
				sensorChartData.setWaterLevelFromZeroMax(0);
				sensorChartData.setWaterLevelFromZeroMin(0);
						
				sensorChartData.setMinH2s(0.0);
				sensorChartData.setMaxH2s(0.0);
			
				sensorChartData.setMinCo2(0.0);
				sensorChartData.setMaxCo2(0.0);
				
				sensorChartData.setMinSo2(0.0);
				sensorChartData.setMaxSo2(0.0);
				
				sensorChartData.setMinCh4(0.0);
				sensorChartData.setMaxCh4(0.0);
				
				sensorChartData.setMinNh3(0.0);
				sensorChartData.setMaxNh3(0.0);
				
				sensorChartData.setMinO2(0.0);
				sensorChartData.setMaxO2(0.0);
				
				sensorChartData.setMinRssi(0.0);
				sensorChartData.setMaxRssi(0.0); 
				return sensorChartData;
			} 
			
			sensorChartData.setWaterLevelFromZeroMax(-1 * maxWaterLevel);
			sensorChartData.setWaterLevelFromZeroMin(-1 * minWaterLevel);
					
			sensorChartData.setMinH2s(minH2s);
			sensorChartData.setMaxH2s(maxH2s);
		
			sensorChartData.setMinCo2(minCo2);
			sensorChartData.setMaxCo2(maxCo2);
			
			sensorChartData.setMinSo2(minSo2);
			sensorChartData.setMaxSo2(maxSo2);
			
			sensorChartData.setMinCh4(minCh4);
			sensorChartData.setMaxCh4(maxCh4);
			
			sensorChartData.setMinNh3(minNh3);
			sensorChartData.setMaxNh3(maxNh3);
			
			sensorChartData.setMinO2(minO2);
			sensorChartData.setMaxO2(maxO2);
			
			sensorChartData.setMinRssi(minRssi);
			sensorChartData.setMaxRssi(maxRssi); 
			return sensorChartData;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Sensor> searchByCriteria(Boolean descending, String deviceId, String sensorId, String dateFrom, String DateTo, Integer limit) {
	
		Date dateFromDate = new Date( Long.valueOf(dateFrom));
		Date dateToDate = new Date( Long.valueOf(DateTo));

		String text = "";
		
		if (deviceId != null) {
			text += " and a.deviceId = :deviceIdParam ";
		}
		if (sensorId != null) {
			text += " and a.sensorId = :sensorIdParam ";
		}
		
		text += " and a.inputDate between :dateFromParam AND :dateToParam";
		text = "select a from " + Sensor.class.getName() + " a "
				+ " where 1=1 " + text;
		
		if ( descending == true) {
			text += " order by a.inputDate desc";
		} else {
			text += " order by a.inputDate asc";
		}
			
		Query query = getEm().createQuery(text);

		if (deviceId != null) {
			query.setParameter("deviceIdParam", Integer.valueOf(deviceId));
		}
		if (sensorId != null) {
			query.setParameter("sensorIdParam", Integer.valueOf(sensorId));
		}

		query.setParameter("dateFromParam", dateFromDate);
		query.setParameter("dateToParam", dateToDate);
		
		
		List<Sensor> result;
		if (limit == null) {
			result = query.getResultList();
		} else {
			result = query.setFirstResult(0).setMaxResults(limit).getResultList();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Integer> distinctSensorIdByDeviceId(Integer deviceId) {
		
		String text = "";
		text += " and a.deviceId = :deviceIdParam ";
		
		text = "select distinct a.sensorId from " + Sensor.class.getName() + " a "
				+ " where 1=1 " + text;
		text += " order by a.sensorId asc";

		Query query = getEm().createQuery(text);
		query.setParameter("deviceIdParam", deviceId);
		List<Integer> result = query.getResultList();
		return result;
	}
	
//	public Sensor searchRecentSensorData(String deviceId, String sensorId, String dateFrom, String DateTo) {
//
//		DateFormat df = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
//		
//		Date dateFromDate = new Date( Long.valueOf(dateFrom));
//		Date dateToDate = new Date( Long.valueOf(DateTo));
//		
//		String dateFromDateStr = df.format(dateFromDate);
//		String dateToDateStr = df.format(dateToDate);
//							
//		
//		String sql = "Select  s from Sensor s where s.deviceId = " + deviceId;
//		sql += " and s.sensorId = " + sensorId;
//		sql += " and s.inputDate between '" + dateFromDateStr + "' and '" + dateToDateStr + "'";
//		sql += " order by s.inputDate desc limit 1"; //desc limit 1";
//		
//		Query query = em.createQuery(sql, Sensor.class);
//			      
//		List<Sensor> sensorDatas = query.getResultList();
//		if (sensorDatas != null && sensorDatas.size() > 0) {
//			return sensorDatas.get(0);
//		}
//		return null;
//	}
}
