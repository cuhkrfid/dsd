package com.xmlasia.gemsx.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.ValidationMessage;

public class ValidationMessageDao extends BaseDao {

//	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
//	public ValidationMessage createOrUpdateValidationMessage(ValidationMessage a) {
//		
//		List<ValidationMessage> validationMessages = searchByCriteria(a.getTagConfigurationId(), a.getSensorConfigId(), a.getDeviceId(), a.getSensorId(), null );
//		if ( validationMessages == null || validationMessages.size() == 0)
//			return createValidationMessage(a);
//		else {
//			ValidationMessage validationToUpdate = validationMessages.get(0);
//			validationToUpdate.copy(a);
//			return updateValidationMessage(validationToUpdate);
//		}
//	}

	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ValidationMessage createValidationMessage(ValidationMessage a) {
		a.setUpdateDate(new Date());
		return (ValidationMessage) create(a);
	}

//	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
//	private ValidationMessage updateValidationMessage(ValidationMessage a) {
//		a.setUpdateDate(new Date());
//		return (ValidationMessage) update(a);
//	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteValidationMessage(long id) {
		delete(ValidationMessage.class, id);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteValidationMessageByTagConfigId(long tagConfigurationId) {
		List<ValidationMessage> validationMessages = searchByCriteria(tagConfigurationId, null, null, null, null );
		if ( validationMessages == null || validationMessages.size() == 0)
			return;
		
		for (ValidationMessage validationMessage : validationMessages) {
			deleteValidationMessage(validationMessage.getId());
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<ValidationMessage> searchByCriteria(Long tagConfigurationId, Long sensorConfigId, Long deviceId, Long sensorId, Integer messageType ) {
	
		String text = "select a from " + ValidationMessage.class.getName() + " a "
				+ " where 1=1 ";
		
		if (tagConfigurationId != null) {
			text += " and a.tagConfigurationId = :tagConfigurationIdParam";
		}
		if (sensorConfigId != null) {
			text += " and a.sensorConfigId = :sensorConfigIdParam";
		}
		if (deviceId != null) {
			text += " and a.deviceId = :deviceIdParam";
		}		
		if (sensorId != null) {
			text += " and a.sensorId = :sensorIdParam";
		}			
		if (messageType != null) {
			text += " and a.messageType = :messageTypeParam";
		}			
		
		Query query = getEm().createQuery(text);
		if (tagConfigurationId != null) {
			query.setParameter("tagConfigurationIdParam", tagConfigurationId);
		}
		if (sensorConfigId != null) {
			query.setParameter("sensorConfigIdParam", sensorConfigId);
		}
		if (deviceId != null) {
			query.setParameter("deviceIdParam", deviceId);
		}		
		if (sensorId != null) {
			query.setParameter("sensorIdParam", sensorId);
		}			
		if (messageType != null) {
			query.setParameter("messageTypeParam", messageType);
		}			

		List<ValidationMessage> result = query.getResultList();
		return result;
	}
	
}
