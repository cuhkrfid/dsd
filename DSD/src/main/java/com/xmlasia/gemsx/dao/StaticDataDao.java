package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.StaticData;

public class StaticDataDao extends BaseDao{
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<StaticData> searchByCriteria(String dataName) {
		String dataNameParam = "%";
		String text = "";
		if (dataName != null && dataName.length() > 0){
			dataNameParam += dataName+"%";
			text += " and s.dataName like :dataNameParam ";
		}
		text = "select s from " + StaticData.class.getName() + " s "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		if (dataName != null && dataName.length() > 0){
			query.setParameter("dataNameParam", dataNameParam);
		}
		
		List<StaticData> result = query.getResultList();
		return result;
	}
}
