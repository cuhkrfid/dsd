package com.xmlasia.gemsx.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.RouterData;

public class RouterDataDao extends BaseDao {

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public RouterData createRouterData(RouterData a) {
		return (RouterData) create(a);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public RouterData updateRouterData(RouterData a) {
		return (RouterData) update(a);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteRouterData(long id) {
		delete(RouterData.class, id);
	}
	
}
