package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Department;

public class DepartmentDao extends BaseDao{
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Department createDepartment(Department d) {
		return (Department) create(d);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Department updateUserDepartment(Department d) {
		return (Department) update(d);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void deleteDepartment(int id) {
		delete(Department.class, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Department retrieveDepartment(int id) {
		Query query = getEm().createQuery("SELECT d FROM "+Department.class.getName()+" d "
				+ " left join fetch d.company "
				+ " where d.id= :id");
		query.setParameter("id", id);
		List<Department> result = query.getResultList();
		if (result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Department> listAll() {
		return getEm().createQuery("SELECT d FROM "+Department.class.getName()+" d ").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Department> searchByCriteria(String id, String chiName, String engName) {
		String idParam = "%";
		String chiNameParam = "%";
		String engNameParam = "%";
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id+"%";
			text += " and STR(d.id) like :idParam ";
		}
		if (chiName != null && chiName.length() > 0){
			chiNameParam += chiName+"%";
			text += " and d.chiName like :chiNameParam ";
		}
		if (engName != null && engName.length() > 0){
			engNameParam += engName+"%";
			text += " and d.engName like :engNameParam ";
		}
		text = "select d from " + Department.class.getName() + " d "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (chiName != null && chiName.length() > 0){
			query.setParameter("chiNameParam", chiNameParam);
		}
		if (engName != null && engName.length() > 0){
			query.setParameter("engNameParam", engNameParam);
		}
		
		List<Department> result = query.getResultList();
		return result;
	}
}
