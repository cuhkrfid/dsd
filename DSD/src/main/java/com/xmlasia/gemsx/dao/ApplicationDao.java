package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Application;

public class ApplicationDao extends BaseDao {

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Application createApplication(Application a) {
		return (Application) create(a);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Application updateApplication(Application a) {
		return (Application) update(a);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void deleteApplication(long id) {
		delete(Application.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Application retrieveApplication(long id) {
		Query query = getEm().createQuery("SELECT a FROM "+ Application.class.getName()+" a "
				+ " where a.id= :id");
		query.setParameter("id", id);
		List<Application> result = query.getResultList();
		if (result.size() > 0){
			Application application = result.get(0);
			return application;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Application> listAll() {
		return getEm().createQuery("SELECT a FROM "+ Application.class.getName()+" a ").getResultList();
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Application> searchByCriteria(String id, String name, String description,
			Integer displayOrder, Boolean active, String path, Boolean useOrder ) {
		
		String idParam = "%";
		String nameParam = "%";
		String descriptionParam = "%";
		Integer displayOrderParam = null;
		Boolean activeParam = null;
		String pathParam = "%";
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id+"%";
			text += " and STR(a.id) like :idParam ";
		}
		if (name != null && name.length() > 0){
			nameParam += name+"%";
			text += " and a.name like :nameParam ";
		}
		if (description != null && description.length() > 0){
			descriptionParam += description+"%";
			text += " and a.description like :descriptionParam ";
		}
		if (displayOrder != null ){
			displayOrderParam = displayOrder;
			text += " and a.displayOrder = :displayOrderParam ";
		}
		if (active != null ){
			activeParam = active;
			text += " and a.active = :activeParam ";
		}
		if (path != null && path.length() > 0){
			pathParam += path+"%";
			text += " and a.path like :pathParam ";
		}
		
		if ( useOrder != null && useOrder.equals(true)) {
			text += " order by a.displayOrder";
		} else {
			text += " order by a.id ";
		}
			
		text = "select a from " + Application.class.getName() + " a "
				+ " where 1=1 " + text;
		Query query = getEm().createQuery(text);
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (name != null && name.length() > 0){
			query.setParameter("nameParam", nameParam);
		}
		if (description != null && description.length() > 0){
			query.setParameter("descriptionParam", descriptionParam);
		}
		if (displayOrder != null ){
			query.setParameter("displayOrderParam", displayOrderParam);
		}
		if (active != null ){
			query.setParameter("activeParam", activeParam);
		}
		if (path != null && path.length() > 0){
			query.setParameter("pathParam", pathParam);
		}
		List<Application> result = query.getResultList();		
		return result;
	}
}
