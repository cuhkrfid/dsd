package com.xmlasia.gemsx.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xmlasia.gemsx.domain.Opex;
import com.xmlasia.gemsx.domain.User;

public class OpexDao extends BaseDao {

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Opex createOpex(Opex o) {
		return (Opex) create(o);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Opex updateOpex(Opex o) {
		return (Opex) update(o);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void deleteOpex(int id) {
		delete(Opex.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Opex retrieveOpex(int id) {
		Query query = getEm().createQuery("SELECT o FROM "+Opex.class.getName()+" o "
				+ " where o.id= :id");
		query.setParameter("id", id);
		List<Opex> result = query.getResultList();
		if (result.size() > 0){
			return result.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Opex> listAll() {
		return getEm().createQuery("SELECT o FROM "+Opex.class.getName()+" o ").getResultList();
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Opex> searchByCriteria(String id, String mainCategoryCode, String mainCategoryName,
			String categoryCode, String categoryName, String subCategoryCode, String subCategoryName, String remark) {
		
		String idParam = "%";
		String mainCategoryCodeParam = "%";
		String mainCategoryNameParam = "%";
		String categoryCodeParam = "%";
		String categoryNameParam = "%";
		String subCategoryCodeParam = "%";
		String subCategoryNameParam = "%";
		String remarkParam = "%";
		
		String text = "";
		if (id != null && id.length() > 0){
			idParam += id+"%";
			text += " and STR(o.id) like :idParam ";
		}
		if (mainCategoryCode != null && mainCategoryCode.length() > 0){
			mainCategoryCodeParam += mainCategoryCode+"%";
			text += " and o.mainCategoryCode like :mainCategoryCodeParam ";
		}
		if (mainCategoryName != null && mainCategoryName.length() > 0){
			mainCategoryNameParam += mainCategoryName+"%";
			text += " and o.mainCategoryName like :mainCategoryNameParam ";
		}
		if (categoryCode != null && categoryCode.length() > 0){
			categoryCodeParam += categoryCode+"%";
			text += " and o.categoryCode like :categoryCodeParam ";
		}
		if (categoryName != null && categoryName.length() > 0){
			categoryNameParam += categoryName+"%";
			text += " and o.categoryName like :categoryNameParam ";
		}
		if (subCategoryCode != null && subCategoryCode.length() > 0){
			subCategoryCodeParam += subCategoryCode+"%";
			text += " and o.subCategoryCode like :subCategoryCodeParam ";
		}
		if (subCategoryName != null && subCategoryName.length() > 0){
			subCategoryNameParam += subCategoryName+"%";
			text += " and o.subCategoryName like :subCategoryNameParam ";
		}
		if (remark != null && remark.length() > 0){
			remarkParam += remark+"%";
			text += " and o.remark like :remarkParam ";
		}
		
		text = "select o from " + Opex.class.getName() + " o "
				+ " where 1=1 " + text + " order by o.id ";
		Query query = getEm().createQuery(text);
		if (id != null && id.length() > 0){
			query.setParameter("idParam", idParam);
		}
		if (mainCategoryCode != null && mainCategoryCode.length() > 0){
			query.setParameter("mainCategoryCodeParam", mainCategoryCodeParam);
		}
		if (mainCategoryName != null && mainCategoryName.length() > 0){
			query.setParameter("mainCategoryNameParam", mainCategoryNameParam);
		}
		if (categoryCode != null && categoryCode.length() > 0){
			query.setParameter("categoryCodeParam", categoryCodeParam);
		}
		if (categoryName != null && categoryName.length() > 0){
			query.setParameter("categoryNameParam", categoryNameParam);
		}
		if (subCategoryCode != null && subCategoryCode.length() > 0){
			query.setParameter("subCategoryCodeParam", subCategoryCodeParam);
		}
		if (subCategoryName != null && subCategoryName.length() > 0){
			query.setParameter("subCategoryNameParam", subCategoryNameParam);
		}
		if (remark != null && remark.length() > 0){
			query.setParameter("remarkParam", remarkParam);
		}
		List<Opex> result = query.getResultList();
		return result;
	}
	
}
