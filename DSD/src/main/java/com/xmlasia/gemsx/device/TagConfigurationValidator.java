package com.xmlasia.gemsx.device;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.SensorConfigDao;
import com.xmlasia.gemsx.dao.ValidationMessageDao;
import com.xmlasia.gemsx.domain.SensorConfig;
import com.xmlasia.gemsx.domain.TagConfiguration;
import com.xmlasia.gemsx.domain.ValidationMessage;
import com.xmlasia.gemsx.service.TagConfigurationService;

public class TagConfigurationValidator extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	private SensorConfigDao sensorConfigDao;
	private TagConfigurationService tagConfigurationService;
	private ValidationMessageDao validationMessageDao;
	
	private long interval;
	private long configExpiredInMs;
	private long configExpiredInMins;
	
	@Override
    public void run() {
        while(!(Thread.currentThread().isInterrupted())) {
            try {
             	Thread.sleep(interval);
            	validate();
            } catch (InterruptedException e) {
                // Set interrupted flag.
                Thread.currentThread().interrupt();
            } catch (Exception e) {
            	log.error(this.getClass().toString() + ": "+ e.getMessage());
            }
        }
    }

	public synchronized void validate() {
		//update expire status for autoCreate sensorConfig
		List<SensorConfig> actualSensorConfigs = sensorConfigDao.retrieveSensorByDeviceIdAndSensorId(null, null, true, null);
		if ( actualSensorConfigs != null && actualSensorConfigs.size() > 0) {
			updateSensorConfigExpireStatus(actualSensorConfigs);
		}

		actualSensorConfigs = sensorConfigDao.retrieveSensorByDeviceIdAndSensorId(null, null, true, null);
		if ( actualSensorConfigs == null || actualSensorConfigs.size() == 0)
			return;
		
		List<TagConfiguration> existingTagConfigurations = tagConfigurationService.searchByCriteria(null, null, null, null, null, null, null, null, null);
		for ( TagConfiguration tagConfig : existingTagConfigurations ) {
			validationMessageDao.deleteValidationMessageByTagConfigId(tagConfig.getId());
			
			tagConfig.setValidationResult(true);
			
			long tagConfigDeviceId = tagConfig.getDeviceId();
			
			Set<Long> sensorIdSet = new HashSet<Long>();
			String[] sensorIdStrs = tagConfig.getSensorIds();
			if ( sensorIdStrs != null) {
				for (String sensorIdStr : sensorIdStrs) {
					sensorIdSet.add(Long.valueOf(sensorIdStr));
				}
			}
			
			if (!isTimeoutOrDeviceIdFound(tagConfig, actualSensorConfigs, tagConfigDeviceId, sensorIdSet)) {
				
				List<SensorConfig> actualSensorConfigsByDeviceIds = sensorConfigDao.retrieveSensorByDeviceIdAndSensorId(Long.valueOf(tagConfig.getDeviceId()), null, true, false);
				if ( actualSensorConfigsByDeviceIds == null )
					actualSensorConfigsByDeviceIds = new ArrayList<SensorConfig>();
				
				isSensorIdFoundTag(tagConfig, actualSensorConfigsByDeviceIds);
				isSensorIdFoundActual(tagConfig, actualSensorConfigsByDeviceIds);
			}
			tagConfigurationService.updateTagConfiguration(tagConfig, false);
			
		}
	}

	private void updateSensorConfigExpireStatus(List<SensorConfig> actualSensorConfigs) {
		Date currentTime = new Date();
		for (SensorConfig sensorConfig : actualSensorConfigs ) {
			if ( sensorConfig.getInputDate() != null ) {
				if( currentTime.getTime() - sensorConfig.getInputDate().getTime() > (sensorConfig.getMeasurePeriod() * 1000 + configExpiredInMs) ) {
					sensorConfig.setExpired(true);
				} else {
					sensorConfig.setExpired(false);	
				}
				
	    		SensorConfigStoreEndpoint.saveToDb(sensorConfig);
			}
		}
	}
	
	private boolean isTimeoutOrDeviceIdFound(TagConfiguration tagConfig, List<SensorConfig> actualSensorConfigs, long tagDeviceId, Set<Long> tagConfigSensorIdSet ) {
		ValidationMessage vdObject = new ValidationMessage();
		vdObject.setTagConfigurationId(tagConfig.getId() );
		
		for (SensorConfig actualSensorConfig : actualSensorConfigs ) {
			
			if (actualSensorConfig.getExpired() 
					&& actualSensorConfig.getDeviceId() == tagDeviceId 
					&& tagConfigSensorIdSet.contains( actualSensorConfig.getSensorId() ) ) {
				String validationMessage = String.format("Tag device Id=%s exceed timeout > %s mins", tagDeviceId, configExpiredInMins);
				tagConfig.setValidationResult(false);
				
				vdObject.setSensorConfigId(actualSensorConfig.getId());
				vdObject.setSensorId(actualSensorConfig.getSensorId());
				vdObject.setDeviceIdTimeout(tagDeviceId, validationMessage);
				validationMessageDao.createValidationMessage(vdObject);
				return true;
			}
			
			if (!actualSensorConfig.getExpired() && actualSensorConfig.getDeviceId() == tagDeviceId) {
				return false;
			}
		}
		String validationMessage = String.format("Tag device Id=%s not found", tagDeviceId);
		vdObject.setDeviceIdNotFound(tagDeviceId, validationMessage);
		validationMessageDao.createValidationMessage(vdObject);
		
		tagConfig.setValidationResult(false);
		return true;
	}
	
	private void isSensorIdFoundTag(TagConfiguration tagConfig, List<SensorConfig> actualSensorConfigs) {
		ValidationMessage vdObject = new ValidationMessage();
		vdObject.setTagConfigurationId(tagConfig.getId() );
	
		SensorConfig[] sensorConfigs = tagConfig.getSensorConfigs();
		if ( sensorConfigs == null || sensorConfigs.length == 0)
			return;
		
		List<SensorConfig> manualSensorConfigs = Arrays.asList(tagConfig.getSensorConfigs());
 		
		for (SensorConfig manualSensorConfig : manualSensorConfigs) {
			boolean isManualSensorConfigFound = false;
			for (SensorConfig actualSensorConfig : actualSensorConfigs ) {
				if (actualSensorConfig.getSensorId() == manualSensorConfig.getSensorId()) {
					if ( actualSensorConfig.getIsCoverLevel() != manualSensorConfig.getIsCoverLevel()) {
						String validationMessage = String.format("Tag sensor Id=%s expect cover level type=%s", 
								  									manualSensorConfig.getSensorId(), 
								  								    String.valueOf(actualSensorConfig.getIsCoverLevel()) );
						vdObject.setSensorConfigId(actualSensorConfig.getId());
						vdObject.setSensorId(actualSensorConfig.getSensorId());
						vdObject.setSensorTypeNotFound(manualSensorConfig.getSensorId(), true, false, false, false, false, false, false, false, validationMessage, "update");
						validationMessageDao.createValidationMessage(vdObject);
						
						tagConfig.setValidationResult(false);
					}
					if ( actualSensorConfig.getIsWaterLevel() != manualSensorConfig.getIsWaterLevel()) {
						String validationMessage = String.format("Tag sensor Id=%s expect water level type=%s", 
																  manualSensorConfig.getSensorId(), 
																  String.valueOf(actualSensorConfig.getIsWaterLevel()) );
						
						vdObject.setSensorConfigId(actualSensorConfig.getId());
						vdObject.setSensorId(actualSensorConfig.getSensorId());
						vdObject.setSensorTypeNotFound(manualSensorConfig.getSensorId(), false, true, false, false, false, false, false, false, validationMessage, "update");
						validationMessageDao.createValidationMessage(vdObject);
						
						tagConfig.setValidationResult(false);
					}
					if ( actualSensorConfig.getIsCo2() != manualSensorConfig.getIsCo2()) {
						String validationMessage = String.format("Tag sensor Id=%s expect co2 type=%s", 
													  manualSensorConfig.getSensorId(), 
													  String.valueOf(actualSensorConfig.getIsCo2()));
						
						vdObject.setSensorConfigId(actualSensorConfig.getId());
						vdObject.setSensorId(actualSensorConfig.getSensorId());
						vdObject.setSensorTypeNotFound(manualSensorConfig.getSensorId(), false, false, true, false, false, false, false, false, validationMessage, "update");
						validationMessageDao.createValidationMessage(vdObject);
						
						tagConfig.setValidationResult(false);
					}
	
					if ( actualSensorConfig.getIsH2s() != manualSensorConfig.getIsH2s()) {
						String validationMessage = String.format("Tag sensor Id=%s expect h2s type=%s", 
													  manualSensorConfig.getSensorId(), 
													  String.valueOf(actualSensorConfig.getIsH2s()) );
						vdObject.setSensorConfigId(actualSensorConfig.getId());
						vdObject.setSensorId(actualSensorConfig.getSensorId());
						vdObject.setSensorTypeNotFound(manualSensorConfig.getSensorId(), false, false, false, true, false, false, false, false, validationMessage, "update");
						validationMessageDao.createValidationMessage(vdObject);
						
						tagConfig.setValidationResult(false);
					}
					
					if ( actualSensorConfig.getIsSo2() != manualSensorConfig.getIsSo2()) {
						String validationMessage = String.format("Tag sensor Id=%s expect so2 type=%s", 
													  manualSensorConfig.getSensorId(), 
													  String.valueOf(actualSensorConfig.getIsSo2()) );
						
						vdObject.setSensorConfigId(actualSensorConfig.getId());
						vdObject.setSensorId(actualSensorConfig.getSensorId());
						vdObject.setSensorTypeNotFound(manualSensorConfig.getSensorId(), false, false, false, false, true, false, false, false, validationMessage, "update");
						validationMessageDao.createValidationMessage(vdObject);
						
						tagConfig.setValidationResult(false);
					}
					
					if ( actualSensorConfig.getIsCh4() != manualSensorConfig.getIsCh4()) {
						String validationMessage = String.format("Tag sensor Id=%s expect ch4 type=%s", 
													  manualSensorConfig.getSensorId(), 
													  String.valueOf(actualSensorConfig.getIsCh4()) );
						
						vdObject.setSensorConfigId(actualSensorConfig.getId());
						vdObject.setSensorId(actualSensorConfig.getSensorId());
						vdObject.setSensorTypeNotFound(manualSensorConfig.getSensorId(), false, false, false, false, false, true, false, false, validationMessage, "update");
						validationMessageDao.createValidationMessage(vdObject);
						
						tagConfig.setValidationResult(false);
					}
					
					if ( actualSensorConfig.getIsNh3() != manualSensorConfig.getIsNh3()) {
						String validationMessage = String.format("Tag sensor Id=%s expect nh3 type=%s", 
													  manualSensorConfig.getSensorId(), 
													  String.valueOf(actualSensorConfig.getIsNh3()) );
						
						vdObject.setSensorConfigId(actualSensorConfig.getId());
						vdObject.setSensorId(actualSensorConfig.getSensorId());
						vdObject.setSensorTypeNotFound(manualSensorConfig.getSensorId(), false, false, false, false, false, false, true, false, validationMessage, "update");
						validationMessageDao.createValidationMessage(vdObject);
						
						tagConfig.setValidationResult(false);
					}
					
					if ( actualSensorConfig.getIsO2() != manualSensorConfig.getIsO2()) {
						String validationMessage = String.format("Tag sensor Id=%s expect o2 type=%s", 
													  manualSensorConfig.getSensorId(), 
													  String.valueOf(actualSensorConfig.getIsO2()) );
						
						vdObject.setSensorConfigId(actualSensorConfig.getId());
						vdObject.setSensorId(actualSensorConfig.getSensorId());
						vdObject.setSensorTypeNotFound(manualSensorConfig.getSensorId(), false, false, false, false, false, false, false, true, validationMessage, "update");
						validationMessageDao.createValidationMessage(vdObject);
						
						tagConfig.setValidationResult(false);
					}
					
					isManualSensorConfigFound = true;
				}
 			}
			if ( !isManualSensorConfigFound) {
				String validationMessage = String.format("Tag sensor Id=%s not found", manualSensorConfig.getSensorId());
				vdObject.setSensorIdNotFound(manualSensorConfig.getSensorId(), validationMessage, "remove");
				validationMessageDao.createValidationMessage(vdObject);
				tagConfig.setValidationResult(false );
			}
		}
	}
	
	
	private void isSensorIdFoundActual(TagConfiguration tagConfig, List<SensorConfig> actualSensorConfigs) {
		ValidationMessage vdObject = new ValidationMessage();
		vdObject.setTagConfigurationId(tagConfig.getId() );
	
		SensorConfig[] sensorConfigs = tagConfig.getSensorConfigs();
		List<SensorConfig> manualSensorConfigs;
		
		if ( sensorConfigs == null || sensorConfigs.length == 0)
			manualSensorConfigs = new ArrayList<SensorConfig>();
		else
			manualSensorConfigs = Arrays.asList(tagConfig.getSensorConfigs());
 		
		for (SensorConfig actualSensorConfig : actualSensorConfigs) {
			boolean isActualSensorConfigFound = false;
			for (SensorConfig manualSensorConfig : manualSensorConfigs ) {
				if (actualSensorConfig.getSensorId() == manualSensorConfig.getSensorId()) {
					isActualSensorConfigFound = true;
				}
 			}
			if ( !isActualSensorConfigFound) {
				String validationMessage = String.format("Actual sensor Id=%s not found", actualSensorConfig.getSensorId());
				vdObject.setSensorIdNotFound(actualSensorConfig.getSensorId(), validationMessage, "add");
					
				vdObject.setIsCoverLevel(actualSensorConfig.getIsCoverLevel());
				vdObject.setIsWaterLevel(actualSensorConfig.getIsWaterLevel());
				vdObject.setIsH2s(actualSensorConfig.getIsH2s());
				vdObject.setIsCo2(actualSensorConfig.getIsCo2());
				vdObject.setIsCh4(actualSensorConfig.getIsCh4());
				vdObject.setIsNh3(actualSensorConfig.getIsNh3());
				vdObject.setIsO2(actualSensorConfig.getIsO2());
				vdObject.setIsSo2(actualSensorConfig.getIsSo2());
				
				validationMessageDao.createValidationMessage(vdObject);
				tagConfig.setValidationResult(false );
			}
		}
	}
	
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}


	public long getInterval() {
		return interval;
	}


	public void setInterval(long interval) {
		this.interval = interval;
	}


	public SensorConfigDao getSensorConfigDao() {
		return sensorConfigDao;
	}


	public void setSensorConfigDao(SensorConfigDao sensorConfigDao) {
		this.sensorConfigDao = sensorConfigDao;
	}

	public TagConfigurationService getTagConfigurationService() {
		return tagConfigurationService;
	}

	public void setTagConfigurationService(TagConfigurationService tagConfigurationService) {
		this.tagConfigurationService = tagConfigurationService;
	}

	public long getConfigExpired() {
		return configExpiredInMs;
	}

	public void setConfigExpired(long autoConfigExpiredInMins) {
		this.configExpiredInMs = (autoConfigExpiredInMins * 60 * 1000);
		this.configExpiredInMins = autoConfigExpiredInMins;
	}

	public ValidationMessageDao getValidationMessageDao() {
		return validationMessageDao;
	}

	public void setValidationMessageDao(ValidationMessageDao validationMessageDao) {
		this.validationMessageDao = validationMessageDao;
	}


}