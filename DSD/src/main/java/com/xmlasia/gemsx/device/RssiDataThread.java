package com.xmlasia.gemsx.device;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.domain.DeviceData;
import com.xmlasia.gemsx.domain.ReaderHeartBeat;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.service.impl.ServerConfigService;

public class RssiDataThread extends Thread {
	private Log log = LogFactory.getLog(this.getClass());
	private long rssiDataThreadSleepMs = 1000;
	private long rssiPacketReleaseTimeoutMs = 2000;
	private boolean isThreadStarted = false;
	private boolean viewHeartBeatInDiagnosticSensorView = false; 
	private Object readWriteLock = new Object();
	private int clearReaderRssiMins;
	private DbServerCache dbServerCache; 
	private ServerConfigService serverConfigService; 
	
	private Comparator<String> tagSeqNumComparator = new Comparator<String>() {
        @Override public int compare(String s1, String s2) {
            String[] keySplit1 = s1.split("_");
            int tagId1 = Integer.valueOf(keySplit1[0]);
            int seqNum1 = Integer.valueOf(keySplit1[1]);
            
            String[] keySplit2 = s2.split("_");
            int tagId2 = Integer.valueOf(keySplit2[0]);
            int seqNum2 = Integer.valueOf(keySplit2[1]);      
        	
            if (tagId1 < tagId2) {
            	return -1;
            }

            if (tagId1 > tagId2) {
            	return 1;
            }
            
            if ( seqNum1 < seqNum2)
               	return -1;
            if ( seqNum1 > seqNum2)
                return 1;
            
            return 0;
        }           
    };
    
    private ConcurrentSkipListMap<String, PacketRssiData> packetRssiDataMap = new ConcurrentSkipListMap<String,PacketRssiData>(tagSeqNumComparator);
    private HashMap<Integer, List<ReaderIdRssi> > readerIdToRssi = new HashMap<Integer, List<ReaderIdRssi> >();
    private ConcurrentSkipListMap<String, StringBuilder> readerIdStrMap = new ConcurrentSkipListMap<String,StringBuilder>(tagSeqNumComparator);
	
	@Override
    public void run() {
		setThreadStarted(true); 
		
        while(!(Thread.currentThread().isInterrupted())) {
        	try {
				Thread.sleep(rssiDataThreadSleepMs);
				//synchronized(lock) {
				Date now = new Date();
				
				for (Map.Entry<String,  PacketRssiData> entry : packetRssiDataMap.entrySet()) {	
					PacketRssiData packetRssiData = entry.getValue();
					if ( now.getTime() - packetRssiData.getPacketReceiveDate().getTime() > rssiPacketReleaseTimeoutMs) {
						try {
							StringBuilder readerIdBuilder = readerIdStrMap.get(entry.getKey());
							
							parsePacketAndWriteToDb(packetRssiData, readerIdBuilder.toString());
							packetRssiDataMap.remove(entry.getKey());
							readerIdStrMap.remove(entry.getKey());
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				//}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        log.debug("RssiDataThread exits");
	}


	public void AddPacketToQ(int tagId, int readerId, double rssi, double snr, IPacketReader processDataStream, Date packetReceiveDate, int seqNum ) {
		String key = PacketRssiData.generateKey(tagId, seqNum);
		PacketRssiData newPacketRssiData = new PacketRssiData(
				Long.valueOf(tagId), 
				Long.valueOf(readerId), String.valueOf(rssi), 
				createReaderRssiStr(readerId, rssi), 
				createReaderNameRssiStr(readerId, rssi), 
				createReaderRssiSnrStr(readerId, rssi, snr), 
				createReaderNameRssiSnrStr(readerId, rssi, snr), 
				processDataStream, 
				packetReceiveDate, 
				seqNum);
		
		PacketRssiData existingPacketRssiData = packetRssiDataMap.putIfAbsent(key, newPacketRssiData);
		if ( existingPacketRssiData != null) {
			existingPacketRssiData.setRssi(String.valueOf(rssi));
			existingPacketRssiData.setReaderRssi( createReaderRssiStr(readerId, rssi) );
			existingPacketRssiData.setReaderNameRssi( createReaderNameRssiStr(readerId, rssi));
			existingPacketRssiData.setReaderRssiSnr( createReaderRssiSnrStr(readerId, rssi, snr) );
			existingPacketRssiData.setReaderNameRssiSnr( createReaderNameRssiSnrStr(readerId, rssi, snr));
		}
		
		StringBuilder previousReaderIds = readerIdStrMap.putIfAbsent(key, new StringBuilder(String.valueOf(readerId)));
		if ( previousReaderIds != null) {
			previousReaderIds.append("," + String.valueOf(readerId));
		}
	}

	private String createReaderRssiStr(int readerId, double rssi) {
		return String.valueOf(readerId) + ":" + String.valueOf(rssi);
	}
	
	private String createReaderNameRssiStr(int readerId, double rssi) {
		String readerName = dbServerCache.getReaderName(readerId);
		return readerName + ":" + String.valueOf(rssi);
	}
	
	private String createReaderRssiSnrStr(int readerId, double rssi, double snr) {
		return String.valueOf(readerId) + ":" + String.valueOf(rssi) + "|" + String.valueOf(snr);
	}

	private String createReaderNameRssiSnrStr(int readerId, double rssi, double snr) {
		String readerName = dbServerCache.getReaderName(readerId);
		
		return readerName + ":" + String.valueOf(rssi) + "|" + String.valueOf(snr);
	}
	
	public void parsePacketAndWriteToDb (PacketRssiData packetToSend, String readerIds) throws IOException {
        
		IPacketReader dataStream = packetToSend.getPacketReader();
		
		int tagIdInt = packetToSend.getTagId().intValue();
		int readerIdInt = packetToSend.getReaderId().intValue();
		
		byte[] packetData = null;
        boolean isHighBattery = true;
        
        DeviceData deviceData = new DeviceData();
        deviceData.setNumberOfSensor(1);
        
        Sensor sensor = new Sensor();
        sensor.setSensorId(tagIdInt);
     	
        packetData = new byte[Sensor.LENGTH_SENSOR_INDICATION];
        dataStream.read(packetData, true); 
     
        sensor.setIndic(PacketTool.getInt(packetData));
    
        packetData = new byte[Sensor.LENGTH_RESERVED];
        dataStream.read(packetData, true); 
        sensor.setReserved(PacketTool.getInt(packetData));
     
        sensor.setAntennaOptimized( (sensor.getReserved() & 0x10) == 16 ? 1 : 0 );
        sensor.setWaterLevel2Detect( (sensor.getReserved() & 0x20) == 32 ? 1 : 0 );
        sensor.setWaterLevel1Detect( (sensor.getReserved() & 0x40) == 64 ? 1 : 0 );
        sensor.setCoverDetect( (sensor.getReserved() & 0x80) == 128 ? 1 : 0 );
        
        packetData = new byte[Sensor.LENGTH_SENSOR_ID];
        dataStream.read(packetData, true); 

        packetData = new byte[Sensor.LENGTH_H2S];
        dataStream.read(packetData, true); 
        if ( (sensor.getIndic() & 0x1) == 1) {
      	  sensor.setIsH2s(true);
      	  sensor.setH2s(PacketTool.round(PacketTool.getDouble(packetData)));
        }

        packetData = new byte[Sensor.LENGTH_SO2];
        dataStream.read(packetData, true); 
        if ( (sensor.getIndic() & 0x2) == 2) {
      	  sensor.setIsSo2(true);
      	  sensor.setSo2(PacketTool.round(PacketTool.getDouble(packetData)));
        }
        
        packetData = new byte[Sensor.LENGTH_CH4];
        dataStream.read(packetData, true); 
        if ( (sensor.getIndic() & 0x4) == 4) {
        	sensor.setIsCh4(true);
		  	sensor.setCh4(PacketTool.round(PacketTool.getDouble(packetData)));
        }
 
        packetData = new byte[Sensor.LENGTH_TOP_ULTRASONIC];
        dataStream.read(packetData, true); 
        if ( (sensor.getIndic() & 0x8) == 8) {
        	sensor.setIsCoverLevel(true);
        	sensor.setCoverLevel(PacketTool.getInt(packetData));
        }

        packetData = new byte[Sensor.LENGTH_BOTTOM_ULTRASONIC];
        dataStream.read(packetData, true); 
        if ( (sensor.getIndic() & 0x10) == 16) {
        	sensor.setIsWaterLevel(true);
        	sensor.setWaterLevel(PacketTool.getInt(packetData));
        	if ( sensor.getWaterLevel() <= 0) {
        		sensor.setIsWaterLevel(false);
        		log.debug("Water level <= 0");
        	}
        }

        isHighBattery = ( (sensor.getIndic() & 0x80) == 128);
     
        sensor.setIsLowBattery(!isHighBattery);
        deviceData.getSensors().add(sensor);
        
        packetData = new byte[DeviceData.LENGTH_PACKET_COUNTER];
        dataStream.read(packetData, true); 
        deviceData.setPacketCounter(PacketTool.getInt(packetData));
        
        packetData = new byte[DeviceData.LENGTH_MEASUREMENT_PERIOD];
        dataStream.read(packetData, true); 
        deviceData.setMeasurePeriod(PacketTool.getInt(packetData));

        byte[] rssiPacketData = new byte[DeviceData.LENGTH_RSSI];
        dataStream.read(rssiPacketData, true); 
        
        packetData = new byte[DeviceData.LENGTH_USER_DEFINE];
        dataStream.read(packetData, true); 
        
        boolean isTimeSlot = ( ( sensor.getIndic() & 0x40 ) == 64 );
        boolean isMaxStep = ( ( sensor.getIndic() & 0x20 ) == 32 );

        if (isTimeSlot) {
        	int timeSlot = PacketTool.getInt(packetData);
        	deviceData.setTimeSlot(timeSlot);	
        	String userDefine = "T:" + String.valueOf(timeSlot);
        	deviceData.setUserDefine(userDefine);
        }
        
        if (isMaxStep) {
         	int maxStep = PacketTool.getInt(packetData);
        	deviceData.setMaxStep(maxStep);
          	String userDefine = "M:" + String.valueOf(maxStep);
        	deviceData.setUserDefine(userDefine);
        }
        
        packetData = new byte[DeviceData.LENGTH_ADCVALUE];
        dataStream.read(packetData, true); 
        deviceData.setAdcValue(PacketTool.getInt(packetData));
        
        byte[] snrPacketData = new byte[DeviceData.LENGTH_SNR];
        dataStream.read(snrPacketData, true); 
        double snr = PacketTool.round(PacketTool.getDouble(snrPacketData));
        deviceData.setSnr(snr);
        
        if ( isReaderHeartBeat(packetToSend) ) {
        	//heartBeat 
        	double rssi = PacketTool.round(PacketTool.getDouble(rssiPacketData));
        	
        	deviceData.setRssi(rssi);
        	deviceData.setRssiStr(String.valueOf(rssi));
        	deviceData.setReaderRssi( createReaderRssiStr(readerIdInt, rssi) ) ;
        	deviceData.setReaderNameRssi( createReaderNameRssiStr(readerIdInt, rssi) ) ;
        	
        	
          	log.debug(String.format("readerId=%d,tagId=%d,seqNum=%d,rssi=%f,sensorIndic=%d", readerIdInt, tagIdInt, deviceData.getPacketCounter(), deviceData.getRssi(), sensor.getIndic()));
            
        } else { 
        	deviceData.setReaderRssi(packetToSend.getReaderRssi()); 
        	deviceData.setReaderNameRssi(packetToSend.getReaderNameRssi());
        	deviceData.setReaderRssiSnr(packetToSend.getReaderRssiSnr());
         	deviceData.setReaderNameRssiSnr(packetToSend.getReaderNameRssiSnr());
            
        	String rssiStr = packetToSend.getRssi();
	        if (rssiStr != null && rssiStr.length() > 0) {
	        	deviceData.setRssiStr(rssiStr);
	        	
	        	//getLargestRssi
	        	String[] rssiStrArray = rssiStr.split(",");
	        	double largestRssi = -100000;
	        	for (int i = 0; i < rssiStrArray.length; ++i) {
	        		double rssiInStr = Double.valueOf(rssiStrArray[0]).doubleValue();
	        		if ( rssiInStr > largestRssi ) {
	        			largestRssi = rssiInStr;
	        		}
	        	}
	        	deviceData.setRssi(largestRssi);
	        	log.debug(String.format("readerIds=%s,tagId=%d,seqNum=%d,rssi=%s,sensorIndic=%d", readerIds, tagIdInt, deviceData.getPacketCounter(), deviceData.getRssiStr(), sensor.getIndic()));
	        } 

	        //append latest heart beat readerId and rssi to deviceData
	        Set<String> deviceDataReaderId; new HashSet<String>();
	        String readerIdStrs = packetToSend.getReaderIdStr(); 
	        if ( readerIdStrs != null && readerIdStrs.length() > 0) {
	        	deviceDataReaderId = new HashSet<String>( Arrays.asList(readerIdStrs.split(",") ) );
	        }
	       
/*	        synchronized(readWriteLock) {
            	
            	List<ReaderIdRssi> readerIdRssiList = readerIdToRssi.get(readerHeartBeat.getReaderId());
            	if ( readerIdRssiList != null) {
            		readerIdRssiList.add(newReaderIdRssi);
            	} else {
            		readerIdToRssi.put(readerHeartBeat.getReaderId() ,  Arrays.asList(new ReaderIdRssi[] { newReaderIdRssi } ) );
            	}
            }
	        */
        }
        
        deviceData.setInputDate(packetToSend.getPacketReceiveDate());
        deviceData.setReaderId(readerIdInt);
        
        String readerIdStr[] = readerIds.split(",");
        Set<Integer> readerIdSet = new HashSet<Integer>();
        for (int i = 0; i < readerIdStr.length; ++i) {
        	readerIdSet.add(Integer.valueOf(readerIdStr[i]));
        }
        String readerIdSetStrs = readerIdSet.toString();
        readerIdSetStrs = readerIdSetStrs.replaceAll("\\s+","");
        
        deviceData.setReaderIds(readerIdSetStrs.substring(1, readerIdSetStrs.length() - 1) ); //remove the [ and ]
        
        deviceData.setTagId(tagIdInt);
        deviceData.setDeviceId(tagIdInt);

        log.debug(String.format("deviceId=%d,packetCounter=%d,measurementPeriod=%d", 
      		  deviceData.getDeviceId(), deviceData.getPacketCounter(), deviceData.getMeasurePeriod()));       

        String deviceAddress = String.format("%04d", tagIdInt);
        deviceData.setParentAddress(deviceAddress);
        deviceData.setDeviceAddress("0000");
        
        //must strictly proceed with following order 
        if (  isReaderHeartBeat(packetToSend) ) { 
    		
        	ReaderHeartBeat readerHeartBeat = new ReaderHeartBeat();
    		readerHeartBeat.setInputDate(deviceData.getInputDate());
    		readerHeartBeat.setReaderId(deviceData.getReaderId());
    	
    		String readerName = dbServerCache.getReaderName(deviceData.getReaderId());
    		if (readerName != null) {
    			readerHeartBeat.setReaderName(readerName);
    		}
    		
    		readerHeartBeat.setRssi(deviceData.getRssi());
    		readerHeartBeat.setPacketCounter(deviceData.getPacketCounter());
    		
    		WebClientReaderHeartBeatEndPoint.broadcastReaderHeartBeatToWebClients(readerHeartBeat);
        	if ( viewHeartBeatInDiagnosticSensorView ) {
        		WebClientDeviceDataEndPoint.broadcastDeviceDataToWebClients(deviceData);
        	}
    		ReaderHeartBeatStoreEndpoint.saveToDb(readerHeartBeat);
/*            synchronized(readWriteLock) {
            	
            	ReaderIdRssi newReaderIdRssi = new ReaderIdRssi( new Date(), deviceData.getReaderId(), deviceData.getRssi()); 
            	List<ReaderIdRssi> readerIdRssiList = readerIdToRssi.get(readerHeartBeat.getReaderId());
            	if ( readerIdRssiList != null) {
            		readerIdRssiList.add(newReaderIdRssi);
            	} else {
            		readerIdToRssi.put(readerHeartBeat.getReaderId() ,  Arrays.asList(new ReaderIdRssi[] { newReaderIdRssi } ) );
            	}
            }*/
    		
        } else {
        	if ( WebClientDeviceDataEndPoint.getViewAllSnr() == false ) {
         		Double minSnr = serverConfigService.getMinSnr(); 
        		if ( minSnr == null || deviceData.getSnr() > serverConfigService.getMinSnr() ) {
         			WebClientDeviceDataEndPoint.broadcastDeviceDataToWebClients(deviceData); 
         		}
            } else {
            	WebClientDeviceDataEndPoint.broadcastDeviceDataToWebClients(deviceData); //function to calculation timeDiffInMs
            }
            DeviceDataStoreEndpoint.saveToDb(deviceData); //also save timeDiffInMs to Db
            AlertEndpoint.sendToProcessAlert(deviceData);        	
        }
	}

	private boolean isReaderHeartBeat(PacketRssiData packetToSend) {
		return packetToSend.getTagId().longValue() == 0;
	}

	public long getRssiDataThreadSleepMs() {
		return rssiDataThreadSleepMs;
	}

	public void setRssiDataThreadSleepMs(long rssiDataThreadSleepMs) {
		this.rssiDataThreadSleepMs = rssiDataThreadSleepMs;
	}


	public long getRssiPacketReleaseTimeoutMs() {
		return rssiPacketReleaseTimeoutMs;
	}


	public void setRssiPacketReleaseTimeoutMs(long rssiPacketReleaseTimeoutMs) {
		this.rssiPacketReleaseTimeoutMs = rssiPacketReleaseTimeoutMs;
	}


	public boolean isThreadStarted() {
		return isThreadStarted;
	}


	public void setThreadStarted(boolean isThreadStarted) {
		this.isThreadStarted = isThreadStarted;
	}


	public boolean isViewHeartBeatInDiagnosticSensorView() {
		return viewHeartBeatInDiagnosticSensorView;
	}


	public void setViewHeartBeatInDiagnosticSensorView(boolean viewHeartBeatInDiagnosticSensorView) {
		this.viewHeartBeatInDiagnosticSensorView = viewHeartBeatInDiagnosticSensorView;
	}


	public int getClearReaderRssiMins() {
		return clearReaderRssiMins;
	}


	public void setClearReaderRssiMins(int clearReaderRssiMins) {
		this.clearReaderRssiMins = clearReaderRssiMins;
	}


	public DbServerCache getDbServerCache() {
		return dbServerCache;
	}


	public void setDbServerCache(DbServerCache dbServerCache) {
		this.dbServerCache = dbServerCache;
	}


	public ServerConfigService getServerConfigService() {
		return serverConfigService;
	}


	public void setServerConfigService(ServerConfigService serverConfigService) {
		this.serverConfigService = serverConfigService;
	}
}
