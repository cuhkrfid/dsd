package com.xmlasia.gemsx.device;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.Date;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.DeviceToReaderDao;
import com.xmlasia.gemsx.dao.ReaderIdToNameMapDao;
import com.xmlasia.gemsx.domain.DeviceData;
import com.xmlasia.gemsx.domain.RawHexStringDeviceData;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.service.impl.ServerConfigService;

//One readerId uses tcp/ip connection to one NioSocketChannel
public class NioSocketChannel implements CompletionHandler<AsynchronousSocketChannel,Void> {

	private Log log = LogFactory.getLog(this.getClass());
	
	private static int numberOfExistingConnections = 0;
	private AsynchronousServerSocketChannel listener;
	private int readTimeOutInMins;
	private boolean nioSocketServerSaveRaw; 
	private ConcurrentMap<Integer, AsynchronousSocketChannel> cordIdToAsyncChannel; 
	private int statusId = -1; 
	private DeviceToReaderDao deviceToReaderDao; 
	
	private long rssiDataThreadSleepMs = 1000;
	private long rssiPacketReleaseTimeoutMs = 2000;
	private static RssiDataThread rssiDataThread = new RssiDataThread();
	private boolean viewHeartBeatInDiagnosticSensorView = false;
	private int clearReaderRssiMins = 3;
	
	private ReaderIdToNameMapDao readerIdToNameMapDao;
	private DbServerCache dbServerCache; 
	private ServerConfigService serverConfigService; 
	
	public NioSocketChannel(int readTimeOutInMins, 
			boolean nioSocketServerSaveRaw, 
			AsynchronousServerSocketChannel listener, 
			ConcurrentMap<Integer, AsynchronousSocketChannel> cordIdToAsyncChannel, 
			DeviceToReaderDao deviceToReaderDao, 
			long rssiDataThreadSleepMs, 
			long rssiPacketReleaseTimeoutMs, 
			boolean viewHeartBeatInDiagnosticSensorView, 
			int clearReaderRssiMins,
			ReaderIdToNameMapDao readerIdToNameMapDao,
			DbServerCache dbServerCache,
			ServerConfigService serverConfigService ) {
		this.listener = listener;
		this.readTimeOutInMins = readTimeOutInMins;
		this.nioSocketServerSaveRaw = nioSocketServerSaveRaw;
		this.cordIdToAsyncChannel = cordIdToAsyncChannel;
		this.deviceToReaderDao = deviceToReaderDao;
		this.rssiDataThreadSleepMs = rssiDataThreadSleepMs; 
		this.rssiPacketReleaseTimeoutMs = rssiPacketReleaseTimeoutMs;
		this.clearReaderRssiMins = clearReaderRssiMins;
        
		this.readerIdToNameMapDao = readerIdToNameMapDao;
        this.dbServerCache = dbServerCache;
        this.serverConfigService = serverConfigService;
        
		this.viewHeartBeatInDiagnosticSensorView = viewHeartBeatInDiagnosticSensorView;
		if ( ! rssiDataThread.isThreadStarted() ) {
			rssiDataThread.setRssiDataThreadSleepMs(rssiDataThreadSleepMs);
			rssiDataThread.setRssiPacketReleaseTimeoutMs(rssiPacketReleaseTimeoutMs);
			rssiDataThread.setViewHeartBeatInDiagnosticSensorView(viewHeartBeatInDiagnosticSensorView);
			rssiDataThread.setClearReaderRssiMins(clearReaderRssiMins);
			rssiDataThread.setDbServerCache(dbServerCache);
			rssiDataThread.setServerConfigService(serverConfigService);
			rssiDataThread.start(); 
		}
	}
	
    @Override
    public void completed(AsynchronousSocketChannel ch, Void att)
    {
    	++numberOfExistingConnections;
    	// Accept the next connection
        listener.accept( null, new NioSocketChannel(readTimeOutInMins, 
        		nioSocketServerSaveRaw, 
        		listener, 
        		cordIdToAsyncChannel, 
        		deviceToReaderDao, 
        		rssiDataThreadSleepMs, 
        		rssiPacketReleaseTimeoutMs, 
        		viewHeartBeatInDiagnosticSensorView, 
        		clearReaderRssiMins,
        		readerIdToNameMapDao, 
        		dbServerCache, 
        		serverConfigService ) );
        
        // Allocate a byte buffer (4K) to read from the client
        ByteBuffer byteBuffer = ByteBuffer.allocate( 4096 );
        try
        {
            // Read the first line
            int bytesRead = 0;
            while( ( bytesRead = ch.read( byteBuffer ).get( readTimeOutInMins, TimeUnit.MINUTES ) ) != -1 )
            {
            	log.debug( String.format("Status: %d, Bytes read: %d, number of connections: %d ", statusId, bytesRead, numberOfExistingConnections) );

                // Make sure that we have data to read
                if( byteBuffer.position() > 0 )
                {
                    try {
                    	byteBuffer.flip();
                    	if ( nioSocketServerSaveRaw ) {
                    		saveRawHexStringToDb(byteBuffer, bytesRead);
                    	} 
                    	statusId = processData(byteBuffer);
						if ( statusId == -1) {
							log.error("ProcessData: Received Unknown Packet (Invalid Data End) - Dropped");
						} else {
							cordIdToAsyncChannel.putIfAbsent(statusId, ch);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						log.error("ProcessData error");
					}
                    byteBuffer.clear();
                }
            }
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        catch (ExecutionException e)
        {
            e.printStackTrace();
        }
        catch (TimeoutException e)
        {
        	log.error( String.format("Connection timed out after %d mins, closing connection", readTimeOutInMins ) );
        }

        try
        {
        	cordIdToAsyncChannel.remove(statusId);
            // Close the connection if we need to
            if( ch.isOpen() ) {
                ch.close();
            }
            log.debug("Connection closed");
            
        	numberOfExistingConnections--;
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
        }
    }

	private void saveRawHexStringToDb(ByteBuffer byteBuffer, int bytesRead) {
		ByteBuffer clonedBuffer = NioSocketServer.cloneByteBuffer(byteBuffer);
		byte[] lineBytes = new byte[ bytesRead ];
		clonedBuffer.get( lineBytes, 0, bytesRead );
		String hexStringData = NioSocketServer.bytesToHex(lineBytes);
		//System.out.println( "Message: " +  hexStringData );
		
		DeviceData rawDeviceData = new DeviceData();
		
		RawHexStringDeviceData rawHexStringData = new RawHexStringDeviceData();
		rawHexStringData.setRawHexStringDeviceData(hexStringData);
		rawHexStringData.setInputDate(new Date());
		rawDeviceData.setRawHexStringDeviceData(rawHexStringData);
		
		DeviceDataStoreEndpoint.saveToDb(rawDeviceData);
	}

	
    @Override
    public void failed(Throwable exc, Void att) {
        ///...
    }

	public int processData(ByteBuffer byteBuffer) throws IOException  {
		 final Packet packet = new Packet();
		 byte[] data = null;
		 IPacketReader dis = new NioPacketReader(byteBuffer);
		 int readerId = 0;
		 int tagId = 0;
		 
		 int readStatus = isStartInfo(dis);
		 if (readStatus == 2) {
			 return -1;
		 }
		 
		 if (readStatus == 0) {
			packet.setPacketReceiveTime(new Date());
			 
			data = new byte[Packet.LENGTH_LENGTH];
			dis.read(data, true);
			packet.setLength(PacketTool.getInt(data)); 
            
			data = new byte[Packet.LENGTH_READER_USER_ID];
            dis.read(data, true);
            packet.setReaderId(PacketTool.getInt(data));
            readerId = packet.getReaderId();
            
            data = new byte[Packet.LENGTH_COMMAND];
            dis.read(data, true);
            packet.setCommand(PacketTool.getInt(data));
            if (packet.getCommand() < 1 || packet.getCommand() > 5) {
	            log.error("Received Unknown Packet (Command ERROR) - Dropped");
	            log.error("Received Command Raw Data " + PacketTool.getHexString(data));
	            return -1;
            }
           
            data = new byte[Packet.LENGTH_TAG_USER_ID];
            dis.read(data, true);
            packet.setTagId(PacketTool.getInt(data));
            tagId = packet.getTagId();
            
            data = new byte[packet.getDataLength()];
            dis.read(data, false);
            packet.setData(data);
            
            data = new byte[Packet.LENGTH_CHECKSUM];
            dis.read(data, true);
            packet.setChecksum(PacketTool.getInt(data));
            
            log.debug(packet);
		 }
			 
		 if (isEndInfo(dis)) {
			 try {
				 DataInputStream deviceDataStream = new DataInputStream(new ByteArrayInputStream(packet.getData()));
				 IPacketReader dataStream = new PacketReader(deviceDataStream);
               
				 switch (packet.getCommand()) {
              		case Packet.TYPE_HEARTBEAT:
                      break;
                  case Packet.TYPE_DEVICEDATA:
                	  if (tagId > 0) {
                		  rssiDataHandler(packet, tagId, readerId, dataStream, packet.getPacketReceiveTime());
                	  } else {
                		  //heartBeat
                		  PacketRssiData packetHeartBeat = new PacketRssiData(Long.valueOf(tagId), Long.valueOf(readerId), "0", "0", "0", "0", "0", dataStream, packet.getPacketReceiveTime(), 0);
                		  rssiDataThread.parsePacketAndWriteToDb(packetHeartBeat, String.valueOf(readerId));
                	  }
                      break;
                  case Packet.TYPE_ROUTERDATA:
                      break;
              }
          } catch (Exception e) {
              e.printStackTrace();
          }
      } else {
    	  log.error("Received Unknown Packet (Invalid EndInfo) - Dropped");
    	  return -1;
      }
      return readerId;
	}

	private void rssiDataHandler(final Packet packet, int tagId, int readerId, IPacketReader processDataStream, Date packetReceiveDate ) throws IOException {
		DataInputStream deviceDataStream = new DataInputStream(new ByteArrayInputStream(packet.getData()));
		IPacketReader dataStream= new PacketReader(deviceDataStream);
		int offset = Sensor.LENGTH_SENSOR_INDICATION + Sensor.LENGTH_RESERVED + Sensor.LENGTH_SENSOR_ID + Sensor.LENGTH_H2S
			+ Sensor.LENGTH_SO2 +  Sensor.LENGTH_CH4 +  Sensor.LENGTH_TOP_ULTRASONIC + Sensor.LENGTH_BOTTOM_ULTRASONIC;
		
     	byte[] rssiData = new byte[offset];
		dataStream.read(rssiData, false);

		rssiData = new byte[DeviceData.LENGTH_PACKET_COUNTER];
        dataStream.read(rssiData, true); 
        int seqNum = PacketTool.getInt(rssiData);
        
        rssiData = new byte[DeviceData.LENGTH_MEASUREMENT_PERIOD];
        dataStream.read(rssiData, false); 
       		
		rssiData = new byte[DeviceData.LENGTH_RSSI];
		dataStream.read(rssiData, true); 
		double rssi = PacketTool.getDouble(rssiData);

		rssiData = new byte[DeviceData.LENGTH_USER_DEFINE];
		dataStream.read(rssiData, true);
		
		rssiData = new byte[DeviceData.LENGTH_ADCVALUE];
		dataStream.read(rssiData, true);
		
		rssiData = new byte[DeviceData.LENGTH_SNR];
		dataStream.read(rssiData, true); 
	    
		double snr = PacketTool.round( PacketTool.getDouble(rssiData) );
		rssiDataThread.AddPacketToQ(tagId, readerId, rssi, snr, processDataStream, packetReceiveDate, seqNum);
	}

	private int isStartInfo(IPacketReader dis) throws IOException {
		char[] symbol = Packet.PKT_PREFIX.toCharArray();
		byte[] temp = new byte[symbol.length];
		int read = dis.read(temp, false);
		if (read != -1) {
			for (int i = 0; i < symbol.length; i++) {
				if (temp[i] != symbol[i]) {
					log.error("Received Unknown Packet (Invalid StartInfo) - Dropped");
					return 1;
				}
			}
		} else {
			log.error("Received Unknown Packet (Invalid StartInfo Data -1 ) - Disconnected");
			return 2;
		}
		return 0;
	}
		 
	public boolean isEndInfo(IPacketReader dis) throws IOException {
       boolean isSame = true;
       char[] symbol = "EndInfo".toCharArray();
       byte[] temp = new byte[symbol.length];
       dis.read(temp, false);
       for (int i = 0; i < symbol.length; i++) {
           if (temp[i] != symbol[i]) {
               isSame = false;
               return isSame;
           }
       }
       return isSame;
	}
}