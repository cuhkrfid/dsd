package com.xmlasia.gemsx.device;


import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.NetworkDao;

public class NetworkMaintenanceTimer extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	private NetworkDao networkDao;
	private long interval;

	
	@Override
    public void run() {
		try {
			networkDao.updateNetworkStatus(new Date());
		} catch (Exception e) {
        	log.error(this.getClass().toString() + ": "+ e.getMessage());
		}
		
        while(!(Thread.currentThread().isInterrupted())) {
            try {
            	Thread.sleep(interval);
            	networkDao.updateNetworkStatus(new Date());
            } catch (InterruptedException e) {
                // Set interrupted flag.
                Thread.currentThread().interrupt();
            } catch (Exception e) {
            	log.error(this.getClass().toString() + ": "+ e.getMessage());
            }
        }
    }

   
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}

	public NetworkDao getNetworkDao() {
		return networkDao;
	}

	public void setNetworkDao(NetworkDao networkDao) {
		this.networkDao = networkDao;
	}


	public long getInterval() {
		return interval;
	}


	public void setInterval(long interval) {
		this.interval = interval;
	}

}