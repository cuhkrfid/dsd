/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xmlasia.gemsx.device;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author chankt
 */
public class Packet {

    private int length;
    private int readerId;
    private byte[] data;
    
    private int command;
    private int tagId;
    
    private String deviceAddress;
    private int checksum;
    private Date packetReceiveTime;
    
    public static final String PKT_PREFIX = "StartInfo";
    public static final String PKT_SUFFIX = "EndInfo";
    public static final int LENGTH_LENGTH = 2;
    public static final int LENGTH_READER_USER_ID = 2;
    public static final int LENGTH_COMMAND = 2;
    public static final int LENGTH_TAG_USER_ID = 2;
    public static final int LENGTH_CHECKSUM = 1;
    
    public static final int TYPE_HEARTBEAT = 1;
    public static final int TYPE_DEVICEDATA = 2;
    public static final int TYPE_ROUTERDATA = 3;
    public static final int TYPE_ENDEVICECONF = 4;
    public static final int TYPE_ROUTERCONF = 5;

    public int getChecksum() {
        return checksum;
    }

    public void setChecksum(int checksum) {
        this.checksum = checksum;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public void setTagId(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    public int getDataLength() {
        return length - PKT_PREFIX.length() - PKT_SUFFIX.length() - LENGTH_LENGTH - LENGTH_READER_USER_ID - LENGTH_COMMAND - LENGTH_TAG_USER_ID - LENGTH_CHECKSUM;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getReaderId() {
        return readerId;
    }
    public void setReaderId(int readerId) {
        this.readerId = readerId;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Packet{" + "length=" + length + ", coordID=" + readerId + ", data=" + data + ", command=" + command + ", deviceAddress=" + deviceAddress + ", checksum=" + checksum + '}';
    }

    public byte[] getByte() {
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            b.write(PKT_PREFIX.getBytes());
            String t = PacketTool.intToHex(length);
            b.write(PacketTool.getHexStringToByteArray(t, 2));
            t = PacketTool.intToHex(readerId);
            b.write(PacketTool.getHexStringToByteArray(t, 2));
            t = PacketTool.intToHex(command);
            b.write(PacketTool.getHexStringToByteArray(t, 2));
            b.write(PacketTool.getHexStringToByteArray(deviceAddress, 2));
            b.write(data);
            calcChecksum();
            b.write(checksum);
            b.write(PKT_SUFFIX.getBytes());
            byte[] r = b.toByteArray();
            b.close();
            return r;
        } catch (IOException ex) {
            Logger.getLogger(Packet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void calcChecksum() {
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            String t = PacketTool.intToHex(length);
            b.write(PacketTool.getHexStringToByteArray(t, 2));
            t = PacketTool.intToHex(readerId);
            b.write(PacketTool.getHexStringToByteArray(t, 2));
            t = PacketTool.intToHex(command);
            b.write(PacketTool.getHexStringToByteArray(t, 2));
            b.write(PacketTool.getHexStringToByteArray(deviceAddress, 2));
            b.write(data);
            int total = 0;
            for (byte te : b.toByteArray()) {
                total += te;
            }
            checksum = (byte) (total & 0xFF);
            b.close();
        } catch (IOException ex) {
            Logger.getLogger(Packet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

	public Date getPacketReceiveTime() {
		return packetReceiveTime;
	}

	public void setPacketReceiveTime(Date packetReceiveTime) {
		this.packetReceiveTime = packetReceiveTime;
	}

	public int getTagId() {
		return tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

}
