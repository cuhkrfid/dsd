package com.xmlasia.gemsx.device;

import java.util.Date;

public class ReaderIdRssi {

	private Date createDate;
	private int readerId;
	private double rssi;
	
	public ReaderIdRssi(Date createDate, int readerId, double rssi) {
		this.createDate = createDate;
		this.readerId = readerId;
		this.rssi = rssi;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public int getReaderId() {
		return readerId;
	}

	public void setReaderId(int readerId) {
		this.readerId = readerId;
	}

	public double getRssi() {
		return rssi;
	}

	public void setRssi(double rssi) {
		this.rssi = rssi;
	}



	
}
