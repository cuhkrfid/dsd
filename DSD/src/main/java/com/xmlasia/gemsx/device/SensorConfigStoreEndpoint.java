package com.xmlasia.gemsx.device;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.SensorConfigDao;
import com.xmlasia.gemsx.domain.SensorConfig;

public class SensorConfigStoreEndpoint extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	private SensorConfigDao sensorConfigDao;
	
	@Override
    public void run() {
        while(!(Thread.currentThread().isInterrupted())) {
            try {
                final SensorConfig sensorConfig = sensorConfigQueue.take();
                this.save(sensorConfig);

            } catch (InterruptedException e) {
                // Set interrupted flag.
                Thread.currentThread().interrupt();
            } catch (Exception e) {
            	log.error(this.getClass().toString() + ": "+ e.getMessage());
            }
        }

        // Thread is getting ready to die, but first,
        // drain remaining elements on the queue and process them.
        final List<SensorConfig> remainingObjects = new ArrayList<SensorConfig>();
        sensorConfigQueue.drainTo(remainingObjects);
       
        for(SensorConfig sensorConfig : remainingObjects) {
            this.save(sensorConfig);
        }
    }

    private void save(final SensorConfig sensorConfig) {
        // Do something with the complex object.
    	try {
    		sensorConfigDao.createOrUpdate(sensorConfig, true);
    		sensorConfigDao.updateManualSensorConfigInputDate(sensorConfig);
    	} catch (Exception e) {
    		log.error("save to db error: " + e.getMessage());
    	}
    }
	
    private static BlockingQueue<SensorConfig> sensorConfigQueue = new LinkedBlockingQueue<SensorConfig>();
    
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}

	public SensorConfigDao getSensorConfigDao() {
		return sensorConfigDao;
	}

	public void setSensorConfigDao(SensorConfigDao sensorConfigDao) {
		this.sensorConfigDao = sensorConfigDao;
	}
	
	static void saveToDb( SensorConfig sensorConfig) {
		sensorConfigQueue.offer(sensorConfig);
	}
}
