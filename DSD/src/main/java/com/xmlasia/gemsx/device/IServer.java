package com.xmlasia.gemsx.device;

import java.io.IOException;

import com.xmlasia.gemsx.domain.Validation;

public interface IServer {

	 void startServer();

	 void stopServer() throws IOException;
	 
	 Validation sendEndPointConfig(int command,	int length,	String deviceAddress, String cordId, String measurePeriod );
}
