package com.xmlasia.gemsx.device;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.TagConfigurationDao;
import com.xmlasia.gemsx.domain.Alert;
import com.xmlasia.gemsx.domain.AlertCriteria;
import com.xmlasia.gemsx.domain.AlertSetting;
import com.xmlasia.gemsx.domain.AlertSettingSensorConfig;
import com.xmlasia.gemsx.domain.DeviceData;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.SensorConfig;
import com.xmlasia.gemsx.domain.TagConfiguration;
import com.xmlasia.gemsx.service.AlertSettingService;

public class AlertEndpoint extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	private AlertSettingService alertSettingService; 
	private TagConfigurationDao tagConfigurationDao;
	
	private long alertSettingPullIntevalInSecs = 30;
	private Date lastAlertSettingPullDateTime = null;
	private List<AlertSetting> alertSettings = new ArrayList<AlertSetting>();

	private Map<Integer, Integer> deviceIdToGroupId = new HashMap<Integer, Integer>();
	private Map<Integer, Long> sensorIdToTriggerTime = new HashMap<Integer, Long>();
	
	@Override
    public void run() {
        while(!(Thread.currentThread().isInterrupted())) {
            try {
                final DeviceData deviceData = deviceDataQueue.take();
                this.processAlertSettings(deviceData);

            } catch (InterruptedException e) {
                // Set interrupted flag.
                Thread.currentThread().interrupt();
            } catch (Exception e) {
            	log.error(this.getClass().toString() + ": "+ e.getMessage());
            }
        }

        // Thread is getting ready to die, but first,
        // drain remaining elements on the queue and process them.
        final List<DeviceData> remainingObjects = new ArrayList<DeviceData>();
        deviceDataQueue.drainTo(remainingObjects);
    }

    private void processAlertSettings(final DeviceData deviceData) {
        // Do something with the complex object.
    	try {    	
    		alertSettings = pullAlertSetting(deviceData);
    		List<Sensor> sensors = deviceData.getSensors();
	    	for ( Sensor sensor : sensors) {
	    		List<Alert> newAlerts = triggerAlert(deviceData, sensor, alertSettings);
	    		for (Alert newAlert : newAlerts) {
		    		AlertStoreEndpoint.processAlert(newAlert);
		    		AlertEmailEndpoint.processAlert(newAlert);
		    		WebClientAlertEndPoint.sendAlert(newAlert);
	    		}
	    	}
    	} catch (Exception e) {
        	log.error(this.getClass().toString() + ": "+ e.getMessage());
    	}
    }

	private List<AlertSetting> pullAlertSetting(final DeviceData deviceData) {
		Date currentDateTime = new Date();
		if ( lastAlertSettingPullDateTime == null || ( currentDateTime.getTime() - lastAlertSettingPullDateTime.getTime()) >= alertSettingPullIntevalInSecs * 1000) {
			alertSettings = getAlertSettingService().searchByCriteria(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, false);
			
			deviceIdToGroupId.clear();
			List<TagConfiguration> tagConfigurations = tagConfigurationDao.searchByCriteria(null, null, null, null, null, null, null, null);
			if (tagConfigurations != null) {
				for (TagConfiguration tagConfig : tagConfigurations) {
					deviceIdToGroupId.put(tagConfig.getDeviceId(), tagConfig.getGroupId());
				}
			}
			lastAlertSettingPullDateTime = currentDateTime;
		} 
		return this.alertSettings;
	}
	
    private List<Alert> triggerAlert(final DeviceData deviceData, Sensor sensor, List<AlertSetting> alertSettings) {
    	
		Integer groupId = deviceIdToGroupId.get(deviceData.getDeviceId());
		int sensorId = sensor.getSensorId();
    	
    	List<Alert> newAlerts = new ArrayList<Alert>();
    	
    	for (AlertSetting alertSetting : alertSettings) {
    		
    		if ( !alertSetting.getUseGroupId() && !alertSetting.getUseSiteId()) {
    			continue;
    		}
    			
    		if ( alertSetting.getUseGroupId() ) {
    			if ( groupId.intValue() != alertSetting.getGroupId().intValue()) {
    				continue;
    			}
    		}
    		
    		TagConfiguration alertTagConfiguration = null;
    		Integer sensorConfigId = null; 
    		
    		if ( alertSetting.getUseSiteId()) {
    			AlertSettingSensorConfig[] alertSettingSensorConfigs = alertSetting.getAlertSettingSensorConfigs();
    			if ( alertSettingSensorConfigs != null ) {
    				for ( AlertSettingSensorConfig alertSettingSensorConfig : alertSettingSensorConfigs) {
    					TagConfiguration tagConfiguration = alertSettingSensorConfig.getTagConfiguration();
    					SensorConfig[] sensorConfigs = tagConfiguration.getSensorConfigs();
        				boolean isFind = false;
    					if ( sensorConfigs != null ) {
        					for ( SensorConfig sensorConfig : sensorConfigs) {
        						if ( sensorConfig.getSensorId() == sensorId ) {
        							sensorConfigId = sensorId;
        							alertTagConfiguration = tagConfiguration;
        							isFind = true;
        							break;
        						}
        					}
        				}
    					
    					if ( isFind )
    						break;
    				}
    			}
    			
    			if ( sensorConfigId == null || alertTagConfiguration == null) {
    				continue;
    			}
    		}
    		
    		if (alertSetting.getIsTriggered()) {
    			if (!alertSetting.getIsRepeatAlert() ) 
    				continue;
    			
    			Long sensorAlertTriggerTime =  sensorIdToTriggerTime.get(sensorConfigId);
    			if ( sensorAlertTriggerTime != null && 
    					deviceData.getInputDate().getTime() -  sensorAlertTriggerTime.longValue() <  alertSetting.getRepeatAlertInSecs() * 1000 )
    				continue;
    			
    		}
    		
    		if (alertSetting.isUseDateRange() 
    			&& alertSetting.getFromDate() != null 
    			&& alertSetting.getToDate() != null ) {

    			long deviceInputDateTime = deviceData.getInputDate().getTime();
    			if ( deviceInputDateTime <= alertSetting.getFromDate().getTime() 
    					|| deviceInputDateTime >= alertSetting.getToDate().getTime()) {
    				continue;
    			}
    			
    		}
    		boolean isCoverDetect = false; 
    		String coverDetectAlertMessage = "";
    		
    		boolean isCoverLevel = false; 
    		String coverLevelAlertMessage = "";
    		
    		boolean isWaterLevel = false;
    		String waterLevelAlertMessage = "";
        	
    		boolean isH2s = false; 
    		String h2sAlertMessage = "";
    		
    		boolean isSo2 = false; 
    		String so2AlertMessage = "";
        	
    		boolean isCo2 = false; 
    		String co2AlertMessage = "";
            
    		boolean isCh4 = false; 
      		String ch4AlertMessage = "";
      	  
    		boolean isNh3 = false;
      		String nh3AlertMessage = "";

    		boolean isO2 = false;
      		String o2AlertMessage = "";

    		AlertCriteria alertCriteria = alertSetting.getAlertCriteria();
    		if ( alertCriteria == null) {
    			continue;
    		} 
    		
    		if ( alertCriteria.getIsCoverDetect() == true && alertCriteria.getCoverDetect() != null) {
    			int openOrClosed = sensor.getCoverDetect(); 
    			if (openOrClosed > 0 && alertCriteria.getCoverDetect().booleanValue() == true) {
    				//closed
    				coverDetectAlertMessage = "Closed";
    			} 

    			if (openOrClosed == 0 && alertCriteria.getCoverDetect().booleanValue() == false) {
    				//opened
    				coverDetectAlertMessage = "Open";
    			}
    			
    			isCoverDetect = !coverDetectAlertMessage.isEmpty();
    		}
    		
      		if ( alertCriteria.getIsCoverLevel() && sensor.getIsCoverLevel()) {
      			coverLevelAlertMessage = testIntegerLevel(sensor.getCoverLevel(), 
      					alertCriteria, alertCriteria.getMinCoverLevelValue(), alertCriteria.getMaxCoverLevelValue(), 
      					AlertCriteria.minCoverLevel, AlertCriteria.maxCoverLevel, "Cover level", alertCriteria.getIsNotCoverLevelBetween());
      			
      			isCoverLevel = !coverLevelAlertMessage.isEmpty();
    		}
      		if ( alertCriteria.getIsWaterLevel() && sensor.getIsWaterLevel()) {
      			waterLevelAlertMessage = testIntegerLevel(sensor.getWaterLevel(), 
      					alertCriteria, alertCriteria.getMinWaterLevelValue(), alertCriteria.getMaxWaterLevelValue(), 
      					AlertCriteria.minWaterLevel, AlertCriteria.maxWaterLevel, "Water level", alertCriteria.getIsNotWaterLevelBetween());
      			isWaterLevel = !waterLevelAlertMessage.isEmpty();
      		}    		
    		if ( alertCriteria.getIsH2s() && sensor.getIsH2s() ) {
    			h2sAlertMessage = testDoubleLevel(sensor.getH2s(), 
      					alertCriteria, alertCriteria.getMinH2sValue(), alertCriteria.getMaxH2sValue(), 
      					AlertCriteria.minH2s, AlertCriteria.maxH2s, "h2s", alertCriteria.getIsNotH2sBetween());
    			isH2s = !h2sAlertMessage.isEmpty();
    		}
    		if ( alertCriteria.getIsSo2() && sensor.getIsSo2()) {
    			so2AlertMessage = testDoubleLevel(sensor.getSo2(), 
      					alertCriteria, alertCriteria.getMinSo2Value(), alertCriteria.getMaxSo2Value(), 
      					AlertCriteria.minSo2, AlertCriteria.maxSo2, "So2", alertCriteria.getIsNotSo2Between());
      			isSo2 = !so2AlertMessage.isEmpty();
    		}
      		if ( alertCriteria.getIsCo2() && sensor.getIsCo2()) {
      			co2AlertMessage = testDoubleLevel(sensor.getCo2(), 
      					alertCriteria, alertCriteria.getMinCo2Value(), alertCriteria.getMaxCo2Value(), 
      					AlertCriteria.minCo2, AlertCriteria.maxCo2, "Co2", alertCriteria.getIsNotCo2Between());
      			isCo2 = !co2AlertMessage.isEmpty();
    		}
      		if ( alertCriteria.getIsCh4() && sensor.getIsCh4()) {
      			ch4AlertMessage = testDoubleLevel(sensor.getCh4(), 
      					alertCriteria, alertCriteria.getMinCh4Value(), alertCriteria.getMaxCh4Value(), 
      					AlertCriteria.minCh4, AlertCriteria.maxCh4, "Ch4", alertCriteria.getIsNotCh4Between());
      			isCh4 = !ch4AlertMessage.isEmpty();
    		}
      		if ( alertCriteria.getIsNh3() && sensor.getIsNh3()) {
      			nh3AlertMessage = testDoubleLevel(sensor.getNh3(), 
      					alertCriteria, alertCriteria.getMinNh3Value(), alertCriteria.getMaxNh3Value(), 
      					AlertCriteria.minNh3, AlertCriteria.maxNh3, "Nh3", alertCriteria.getIsNotNh3Between());
    			isNh3 = !nh3AlertMessage.isEmpty();
      		}
      		if ( alertCriteria.getIsO2() && sensor.getIsO2()) {
      			o2AlertMessage = testDoubleLevel(sensor.getO2(), 
      					alertCriteria, alertCriteria.getMinO2Value(), alertCriteria.getMaxO2Value(), 
      					AlertCriteria.minO2, AlertCriteria.maxO2, "O2", alertCriteria.getIsNotO2Between());
      			isO2 = !o2AlertMessage.isEmpty();
      		}
      		
      		if ( !isCoverDetect && !isCoverLevel && !isWaterLevel && !isH2s && !isSo2 &&  !isCo2 && !isCh4 && !isNh3 && !isO2) {
      			continue;
      		}
      		
      		Date triggerTime = new Date(); 
      		sensorIdToTriggerTime.put(sensorConfigId, triggerTime.getTime());
      		
      		if (! alertSetting.getIsTriggered()) {
      			alertSetting.setIsTriggered(true);
      			alertSetting.setTriggerTime(triggerTime);
          		alertSettingService.updateAlertSetting(alertSetting);
      		}
      		
      		Alert alert = new Alert();
      		alert.setTagConfiguration(alertTagConfiguration);
      		alert.setTagConfigurationName(alertTagConfiguration.getName());
      		alert.setAlertSetting(alertSetting);
      		alert.setAlertSettingId(alertSetting.getId());
      		
      		alert.setCreateDateTime(triggerTime);
      		
      		DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String dateString = df.format(triggerTime);
			alert.setCreateDateTimeStr(dateString);
			
      		alert.setDeviceId(deviceData.getDeviceId());
      		alert.setGroupId(groupId);
      		alert.setSensorId(sensor.getSensorId());
      		alert.setTagConfigurationId(alertTagConfiguration.getId());
      		alert.setSiteConfigurationId(alertTagConfiguration.getSiteConfigurationId());
      		alert.setSensorConfigId(Long.valueOf(sensorConfigId));
      		
      		alert.setCoverDetect(sensor.getCoverDetect() > 0 ? "Closed" : "Open");
      		alert.setCoverLevel(sensor.getCoverLevel());
      		alert.setWaterLevel(sensor.getWaterLevel());
      		alert.setH2s(sensor.getH2s());
      		alert.setSo2(sensor.getSo2());
      		alert.setCo2(sensor.getCo2());
      		alert.setCh4(sensor.getCh4());
     		alert.setNh3(sensor.getNh3());
      		alert.setO2(sensor.getO2());
      		
      		alert.setIsCoverDetect(isCoverDetect);
      		alert.setIsCoverLevel(isCoverLevel);
      		alert.setIsWaterLevel(isWaterLevel);
      		alert.setIsH2s(isH2s);
      		alert.setIsSo2(isSo2);
      		alert.setIsCo2(isCo2);
      		alert.setIsCh4(isCh4);
      		alert.setIsNh3(isNh3);
      		alert.setIsO2(isO2);
      		
      		alert.setAlertSettingLevel(alertSetting.getLevel());
      		alert.setAlertSettingName(alertSetting.getName());
      		
      		alert.setCoverLevelAlertMessage(coverLevelAlertMessage);
      		alert.setWaterLevelAlertMessage(waterLevelAlertMessage);
      		alert.setH2sAlertMessage(h2sAlertMessage);
      		alert.setSo2AlertMessage(so2AlertMessage);
      		alert.setCo2AlertMessage(co2AlertMessage);
      		alert.setCh4AlertMessage(ch4AlertMessage);
      		alert.setNh3AlertMessage(nh3AlertMessage);
      		alert.setO2AlertMessage(o2AlertMessage);
      		alert.setCoverDetectAlertMessage(coverDetectAlertMessage);
      		newAlerts.add(alert);
    	}

    	
    	return newAlerts;
    }

	private String testIntegerLevel(int 		   sensorValue, 
									AlertCriteria alertCriteria, 
									Integer 	   minCriteriaValue, 
									Integer 	   maxCriteriaValue, 
									int 		   minConstLevel, 
									int 		   maxConstLevel, 
									String 	   valueName,
									Boolean	   isNotBetween) {
		String message = "";
		
		if ( minCriteriaValue != null && maxCriteriaValue != null) {
			if (isNotBetween) {
				if ( minConstLevel <= sensorValue && sensorValue < minCriteriaValue) {
					message = String.format("%d <= [ %s: %d ] < %d", minConstLevel, valueName, sensorValue, minCriteriaValue);
				} else if ( maxCriteriaValue < sensorValue && sensorValue <= maxConstLevel) {
					message = String.format("%d <  [%s: %d ] <= %d", maxCriteriaValue, valueName, sensorValue, maxConstLevel);
				}
			} else {
				if ( minCriteriaValue <= sensorValue && sensorValue <= maxCriteriaValue) {
					message = String.format("%d <= [ %s: %d ] <= %d", minCriteriaValue, valueName, sensorValue, maxCriteriaValue);
				}
			}
		}
		return message;
	}

	private String testDoubleLevel(double sensorValue, 
			 AlertCriteria alertCriteria, 
			 Double 	   minCriteriaValue, 
			 Double 	   maxCriteriaValue, 
			 double 	   minConstLevel, 
			 double 	   maxConstLevel, 
			 String 	   valueName,
			 Boolean	   isNotBetween) {
		String message = "";		
		if ( minCriteriaValue != null && maxCriteriaValue != null) {
			if (isNotBetween) {
				if ( minConstLevel <= sensorValue && sensorValue < minCriteriaValue) {
					message = String.format("%.1f <= [ %s: %.1f ] < %.1f", minConstLevel, valueName, sensorValue, minCriteriaValue);
				} else if ( maxCriteriaValue < sensorValue && sensorValue <= maxConstLevel) {
					message = String.format("%.1f < [ %s: %.1f ] <= %.1f", maxCriteriaValue, valueName, sensorValue, maxConstLevel);
				}
			} else {
				if ( minCriteriaValue <= sensorValue && sensorValue <= maxCriteriaValue) {
					message = String.format("%.1f <= [ %s: %.1f ] <= %.1f", minCriteriaValue, valueName, sensorValue, maxCriteriaValue);
				}
			}
		}
		return message;
	}
	
    private static BlockingQueue<DeviceData> deviceDataQueue = new LinkedBlockingQueue<DeviceData>();
    
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}
	
	static void sendToProcessAlert( DeviceData deviceData) {
		deviceDataQueue.offer(deviceData);
	}

	public TagConfigurationDao getTagConfigurationDao() {
		return tagConfigurationDao;
	}

	public void setTagConfigurationDao(TagConfigurationDao tagConfigurationDao) {
		this.tagConfigurationDao = tagConfigurationDao;
	}

	public long getAlertSettingPullIntevalInSecs() {
		return alertSettingPullIntevalInSecs;
	}

	public void setAlertSettingPullIntevalInSecs(long alertSettingPullIntevalInSecs) {
		this.alertSettingPullIntevalInSecs = alertSettingPullIntevalInSecs;
	}

	public AlertSettingService getAlertSettingService() {
		return alertSettingService;
	}

	public void setAlertSettingService(AlertSettingService alertSettingService) {
		this.alertSettingService = alertSettingService;
	}
}
