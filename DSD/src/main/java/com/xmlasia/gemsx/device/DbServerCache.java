package com.xmlasia.gemsx.device;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.ReaderIdToNameMapDao;
import com.xmlasia.gemsx.domain.ReaderIdToNameMap;
public class DbServerCache extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	
	private ReaderIdToNameMapDao readerIdToNameMapDao;
	private long serverCacheUpdateIntervalInMins;
	
	private ConcurrentHashMap<Long, String> readerIdToNameMap = new ConcurrentHashMap<Long, String>();
	private long serverPollIntervalInMs = 5000;
	
	@Override
    public void run() {
		updateReaderIdToNameMap();
		
		long currentDateTime = new Date().getTime();
		while(!(Thread.currentThread().isInterrupted())) {
			
			try {
				Thread.sleep(serverPollIntervalInMs);
				long now = new Date().getTime();
				if ( now - currentDateTime > serverCacheUpdateIntervalInMins * 60000 ) {
					updateReaderIdToNameMap();
					currentDateTime = now;
				}
				
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				 Thread.currentThread().interrupt();
			}
        }
    }

	private void updateReaderIdToNameMap() {
		List<ReaderIdToNameMap> readerIdToNameMaps = readerIdToNameMapDao.listAll();
		if ( readerIdToNameMaps != null) {
			for ( ReaderIdToNameMap readerIdToNameMap : readerIdToNameMaps) {
				
				String readerName = readerIdToNameMap.getReaderName();
				readerName = ( readerName == null ? "" : readerName);
				this.readerIdToNameMap.put(readerIdToNameMap.getReaderId(), readerName);	
			}
		}
	}
	
	public String getReaderName(long readerId) {
		String readerName = readerIdToNameMap.get(readerId);
		return readerName == null ? "" : readerName;
	}
	
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}

	public ReaderIdToNameMapDao getReaderIdToNameMapDao() {
		return readerIdToNameMapDao;
	}

	public void setReaderIdToNameMapDao(ReaderIdToNameMapDao readerIdToNameMapDao) {
		this.readerIdToNameMapDao = readerIdToNameMapDao;
	}

	public long getServerCacheUpdateIntervalInMins() {
		return serverCacheUpdateIntervalInMins;
	}

	public void setServerCacheUpdateIntervalInMins(long serverCacheUpdateIntervalInMins) {
		this.serverCacheUpdateIntervalInMins = serverCacheUpdateIntervalInMins;
	}

	public long getServerPollIntervalInMs() {
		return serverPollIntervalInMs;
	}

	public void setServerPollIntervalInMs(long serverPollIntervalInMs) {
		this.serverPollIntervalInMs = serverPollIntervalInMs;
	}
}
