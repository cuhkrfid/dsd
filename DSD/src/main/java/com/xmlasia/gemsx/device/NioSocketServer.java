package com.xmlasia.gemsx.device;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.xmlasia.gemsx.dao.DeviceToReaderDao;
import com.xmlasia.gemsx.dao.ReaderIdToNameMapDao;
import com.xmlasia.gemsx.domain.Validation;
import com.xmlasia.gemsx.service.impl.ServerConfigService;

public class NioSocketServer  extends Thread implements IServer {

	private int port;
	private String ipAddr; 
	private int readTimeOutInMins;
	private static char[] hexArray = "0123456789ABCDEF".toCharArray();
	private boolean nioSocketServerSaveRaw = false; 
	private static ConcurrentMap<Integer, AsynchronousSocketChannel> cordIdToAsyncChannel = new ConcurrentHashMap<Integer, AsynchronousSocketChannel>();
	private DeviceToReaderDao deviceToReaderDao; 
	private long rssiDataThreadSleepMs = 1000; 
	private long rssiPacketReleaseTimeoutMs = 2000; 
	private boolean viewHeartBeatInDiagnosticSensorView = false;
	private int clearReaderRssiMins = 3; 
	
	private ReaderIdToNameMapDao readerIdToNameMapDao;
	private DbServerCache dbServerCache; 
	private ServerConfigService serverConfigService;
	
    public NioSocketServer(int port,  
    					   String ipAddr, 
    					   int readTimeOut, 
    					   boolean nioSocketServerSaveRaw, 
    					   DeviceToReaderDao deviceToReaderDao, 
    					   long rssiDataThreadSleepMs, 
    					   long rssiPacketReleaseTimeoutMs, 
    					   boolean viewHeartBeatInDiagnosticSensorView, 
    					   int clearReaderRssiMins,
    					   ReaderIdToNameMapDao readerIdToNameMapDao, 
    					   DbServerCache dbServerCache, 
    					   ServerConfigService serverConfigService) {
        this.port = port;
        this.ipAddr = ipAddr;
        this.readTimeOutInMins = readTimeOut;
        this.nioSocketServerSaveRaw = nioSocketServerSaveRaw;
        this.deviceToReaderDao = deviceToReaderDao;
        this.rssiDataThreadSleepMs = rssiDataThreadSleepMs;
        this.rssiPacketReleaseTimeoutMs = rssiPacketReleaseTimeoutMs;
        this.viewHeartBeatInDiagnosticSensorView = viewHeartBeatInDiagnosticSensorView;
        this.clearReaderRssiMins = clearReaderRssiMins;
        
        this.readerIdToNameMapDao = readerIdToNameMapDao;
        this.dbServerCache = dbServerCache;
        this.serverConfigService = serverConfigService;
    }
    
	@Override
	public void startServer() {
		// TODO Auto-generated method stub
		 try
	        {
			    // Create an AsynchronousServerSocketChannel that will listen on port 5000
	            final AsynchronousServerSocketChannel listener =
	                    AsynchronousServerSocketChannel.open().bind(new InetSocketAddress(InetAddress.getByName(ipAddr),port));
	            listener.accept( null, 
	            		new NioSocketChannel(readTimeOutInMins, 
	            				nioSocketServerSaveRaw, 
	            				listener, 
	            				cordIdToAsyncChannel, 
	            				deviceToReaderDao,  
	            				rssiDataThreadSleepMs, 
	            				rssiPacketReleaseTimeoutMs, 
	            				viewHeartBeatInDiagnosticSensorView, 
	            				clearReaderRssiMins,
	            				readerIdToNameMapDao, 
	            				dbServerCache,
	            				serverConfigService ));
	        }
	        catch (IOException e)
	        {
	            e.printStackTrace();
	        }		
	}

	private static void send(Packet packet) throws Exception {
		// TODO Auto-generated method stub
		System.out.println(PacketTool.getHexString(packet.getByte()));
		AsynchronousSocketChannel ch = cordIdToAsyncChannel.get(packet.getReaderId());
		if (ch != null) {
			ch.write(ByteBuffer.wrap(packet.getByte()));
	   	} else {
	   		throw new Exception (String.format("Cannot find socket channel with readerId %d", packet.getReaderId() ));
	   	}
	}
	
	public static Validation sendEndPointConfigStatic(int command,
			int length,
			String deviceAddress,
			String readerId,
			String measurePeriod ) {
		
		Validation validateMsg = new Validation();
		validateMsg.setValidationSubject("Send Config");
		
		try {
		    Packet packet = new Packet();
	        packet.setCommand(command);
	        packet.setDeviceAddress(deviceAddress);
	        packet.setReaderId(Integer.valueOf(readerId));
	        packet.setLength(11);
	        
	        EndDeviceConfigData endDeviceConfigData = new EndDeviceConfigData();
	        endDeviceConfigData.setMeasurementPeriod(Integer.valueOf(measurePeriod));
	        packet.setData(endDeviceConfigData.getByte());
	        NioSocketServer.send(packet);
	        
	        validateMsg.setValidationMessages("Send config success");
		} catch (Exception e) {
			validateMsg.setFailedValidationMessage("Send config failed: " + e.getMessage());
		}
		return validateMsg;
	}
   
   	public static String bytesToHex(byte[] bytes ) {
	   	char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
   }
   
   	public static ByteBuffer cloneByteBuffer(ByteBuffer original) {
        ByteBuffer clone = ByteBuffer.allocate(original.capacity());
        original.rewind();//copy from the beginning
        clone.put(original);
        original.rewind();
        clone.flip();
        return clone;
   	}
   
	@Override
	public void stopServer() throws IOException {
		// TODO Auto-generated method stub
	}

	@Override
	public Validation sendEndPointConfig(int command, int length, String deviceAddress, String cordId,
			String measurePeriod) {
		// TODO Auto-generated method stub
		return null;
	}

}
