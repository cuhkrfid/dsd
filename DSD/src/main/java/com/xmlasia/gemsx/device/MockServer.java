package com.xmlasia.gemsx.device;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.xmlasia.gemsx.domain.DeviceData;
import com.xmlasia.gemsx.domain.Network;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.Validation;

public class MockServer extends Thread implements IServer {

	private boolean isStop = false; 
	
	private Network rootNodeNetwork = new Network();
	   
	
	private Random randomGenerator = new Random(); 
	private Integer minCoverLevel = 130; //0 149 
	private Integer maxCoverLevel = 160; //200

	private Integer minWaterLevel = 20; //0  24
	private Integer maxWaterLevel = 40; //100

	private Integer minH2s = 9000; 	//0  98
	private Integer maxH2s = 10000;	//100

	private Integer minSo2 = 9000;  //0     //100
	private Integer maxSo2 = 10000; //100

	private Integer minCo2 = 8030; 	//0    //98
	private Integer maxCo2 = 10000; //100

	private Integer minCh4 = 90020; //0	    //98
	private Integer maxCh4 = 99000; //100

	private Integer minNh3 = 9010;  //0    //98
	private Integer maxNh3 = 10000; //100

	private Integer minO2 = 9545;   //0    //98
	private Integer maxO2 = 10000;  //100

	private Integer minRssi = 9800;  //0   //110
	private Integer maxRssi = 12000; //120
	
	private int numberofEndDevice = 100;
	private boolean isSend = true; 
	
	private int interval = 5000;
	private int seriesInterval = 0;
	
    public MockServer(int mockServerNumOfClients, int interval, int seriesInterval) {

    	rootNodeNetwork.setDeviceAddress("0D2F");
    	rootNodeNetwork.setDeviceId(666);
    	rootNodeNetwork.setDeviceType(Network.COORDINATOR);
    	rootNodeNetwork.setStatusId(0);
    	rootNodeNetwork.setParentAddress("0D2F");
    	rootNodeNetwork.setCordId(1);
    	
    	numberofEndDevice = mockServerNumOfClients;
    	this.interval = interval;
    	this.seriesInterval = seriesInterval;
    }

    @Override
    public void run() {
    	int[] packetCounter = new int[numberofEndDevice];
    	int maxStepAdcValue = 0;
    	while (!isStop) {
			
			if ( isSend) {
				List<DeviceData> deviceDatas = new ArrayList<DeviceData>();
				for (int i = 0; i < numberofEndDevice; ++i) {
					String deviceAddress = String.format("%04d", i+1);
				
					DeviceData deviceData = new DeviceData();
					
					deviceData.setDeviceId(i+1);
					deviceData.setReaderId(i+1);
					deviceData.setTagId(i+1);
					deviceData.setDeviceAddress(deviceAddress);
					deviceData.setParentAddress("0D2F");
					
					Sensor sensor = new Sensor();
					sensor.setDeviceId(i+1);
					sensor.setSensorId(i+1); //tagId
		    		
		    		deviceData.getSensors().add(sensor);
		    		deviceDatas.add(deviceData);
		    		
		        	deviceData.setInputDate(new Date());
		        	rootNodeNetwork.setUpdatedTime(new Date());
		        	
					Double rssi = generateNextDouble(minRssi, maxRssi);
					deviceData.setRssi(rssi);
					
					packetCounter[i]++;
					deviceData.setPacketCounter(packetCounter[i]);
                    
					deviceData.setMaxStep(++maxStepAdcValue);
					deviceData.setAdcValue(++maxStepAdcValue);
					
		        	for ( Sensor mockSensorData : deviceData.getSensors()) {
	
						Integer coverLevel = generateNextInteger(minCoverLevel, maxCoverLevel);
						Integer waterLevel = generateNextInteger(minWaterLevel, maxWaterLevel);
						Double h2s = generateNextDouble(minH2s, maxH2s);
						Double so2 = generateNextDouble(minSo2, maxSo2);
						Double co2 = generateNextDouble(minCo2, maxCo2);
						Double ch4 = generateNextDouble(minCh4, maxCh4);
						Double nh3 = generateNextDouble(minNh3, maxNh3);
						Double o2 = generateNextDouble(minO2, maxO2);
						
						mockSensorData.setInputDate(new Date());
						mockSensorData.setCoverLevel(coverLevel);
						mockSensorData.setWaterLevel(waterLevel);
						mockSensorData.setH2s(h2s);
						mockSensorData.setSo2(so2);
						mockSensorData.setCo2(co2);
						mockSensorData.setCh4(ch4);
						mockSensorData.setNh3(nh3);
						mockSensorData.setO2(o2);
	
						mockSensorData.setCoverDetect(randomGenerator.nextInt() > 0 ? 1 : 0);
						mockSensorData.setWaterLevel1Detect(randomGenerator.nextInt() > 0 ? 1 : 0);
						mockSensorData.setWaterLevel2Detect(randomGenerator.nextInt() > 0 ? 1 : 0);
						mockSensorData.setAntennaOptimized(randomGenerator.nextInt() > 0 ? 1 : 0);
						mockSensorData.setIsLowBattery(Boolean.valueOf(randomGenerator.nextInt() > 0 ? true : false));	
		        	}
		        	
					try {
						Thread.sleep(seriesInterval);
					} catch (InterruptedException e) {
						return;
					}
					
		            WebClientDeviceDataEndPoint.broadcastDeviceDataToWebClients(deviceData); //function to calculation timeDiffInMs
		            DeviceDataStoreEndpoint.saveToDb(deviceData); //also save timeDiffInMs to Db
		            
		            Network tagEndPoint = new Network();
		            tagEndPoint.setDeviceAddress(deviceData.getDeviceAddress());
		            tagEndPoint.setDeviceId(deviceData.getDeviceId());
		            tagEndPoint.setDeviceType(Network.ENDDEVICE);
		            tagEndPoint.setStatusId(0);
		        	tagEndPoint.setParentAddress("0DXF");
		        	tagEndPoint.setCordId(1);
		        	tagEndPoint.setUpdatedTime(new Date());
		        	
		            NetworkStoreEndpoint.saveToDb(tagEndPoint);
		            WebClientNetworkEndPoint.broadcastNetworkToWebClients(tagEndPoint);
		            		            
		            NetworkStoreEndpoint.saveToDb(rootNodeNetwork);
		            WebClientNetworkEndPoint.broadcastNetworkToWebClients(rootNodeNetwork);
		            
		            AlertEndpoint.sendToProcessAlert(deviceData);
				}
			}
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				return;
			}
		}
    }
	 
    @Override
	public void startServer() {
		 this.start();
	}
	 
    @Override
	public void stopServer() throws IOException {
    	isStop = true;
    	this.interrupt();
    }
 
    private Integer generateNextInteger(Integer min, Integer max) {
    	int randValue = randomGenerator.nextInt(max-min+1) + min;
    	return randValue;
    }
    
    private Double generateNextDouble(Integer min, Integer max) {
    	
    	int randValue = randomGenerator.nextInt(max-min+1) + min;
    	
    	BigDecimal randDecimal = new BigDecimal(randValue);
    	randDecimal = randDecimal.divide(new BigDecimal(100));
    	
    	return randDecimal.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
    
	@Override
	public Validation sendEndPointConfig(int command,
			int length,
			String deviceAddress,
			String cordId,
			String measurePeriod ) {
		
		Validation validateMsg = new Validation();
		validateMsg.setValidationSubject("Send Config");
	    validateMsg.setValidationMessages("Send config success");
		return validateMsg;
	}
}
