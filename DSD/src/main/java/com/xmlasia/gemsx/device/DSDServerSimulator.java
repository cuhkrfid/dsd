package com.xmlasia.gemsx.device;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.xmlasia.gemsx.domain.DeviceData;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.Validation;

public class DSDServerSimulator extends Thread implements IServer {

	private ServerSocket server;
	
	private int port;
	private String ipAddr; 
	private int backLog = 50;
	
    private boolean started = false;
    
	public DSDServerSimulator(int port, int backLog, String ipAddr) {
        this.port = port;
        this.backLog = backLog;
        this.ipAddr = ipAddr;
	}
	 
	 @Override
	 public void run() {
		 try {
			 InetAddress addr = InetAddress.getByName(ipAddr);
			 
			 server = new ServerSocket(backLog, port, addr);
			 server.setReuseAddress(true);
			 while (started) {
				 final Socket socket = server.accept();
				 new Thread() {
					 public void run() {
						 try {
							 processData(socket);
						 } catch (Exception ex) {
							 ex.printStackTrace();
						 }
					 }
				 }.start();
			 }
			 server.close();
		 } catch (IOException ex) {
			 ex.printStackTrace();
		 }
	 }
	 
	 private void processData(Socket socket) throws IOException  {
		 boolean connected = true;
		 while (connected) {
			 
			InputStream is;
		    try {
		       is = socket.getInputStream();
		       byte[] buffer = new byte[2048];
		       int read;
		       if((read = is.read(buffer)) != -1) {
		           String output = new String(buffer, 0, read);
		           System.out.println(output);
		           System.out.flush();
		           
		           try {
		        	   JSONObject  sensorDataObject = new JSONObject(output);
			           String deviceAddress = sensorDataObject.getString("deviceAddress");
			           String deviceIdStr = sensorDataObject.getString("deviceId");
		        	   String coverLevelStr = sensorDataObject.getString("coverLevel");
			           String waterLevelStr = sensorDataObject.getString("waterLevel");
			           String h2sStr = sensorDataObject.getString("h2s");
			           String so2Str = sensorDataObject.getString("so2");
			           String co2Str = sensorDataObject.getString("co2");
			           String ch4Str = sensorDataObject.getString("ch4");
			           String nh3Str = sensorDataObject.getString("nh3");
			           String o2Str = sensorDataObject.getString("o2");
			           
			           DeviceData deviceData = new DeviceData();
					
			           deviceData.setDeviceId(Integer.valueOf(deviceIdStr));
			           deviceData.setDeviceAddress(deviceAddress);
			           deviceData.setParentAddress("0D2F");
			           deviceData.setInputDate(new Date());
			           
			           Sensor sensor = new Sensor();
			           sensor.setReaderId(Integer.valueOf(deviceIdStr));
			           sensor.setDeviceId(Integer.valueOf(deviceIdStr));
			           sensor.setSensorId(Integer.valueOf(deviceIdStr));
		    		   
			           sensor.setInputDate(new Date());
			           sensor.setCoverLevel(Integer.valueOf(coverLevelStr));
			           sensor.setWaterLevel(Integer.valueOf(waterLevelStr));
			           sensor.setH2s(Double.valueOf(h2sStr));
			           sensor.setSo2(Double.valueOf(so2Str));
			           sensor.setCo2(Double.valueOf(co2Str));
			           sensor.setCh4(Double.valueOf(ch4Str));
			           sensor.setNh3(Double.valueOf(nh3Str));
			           sensor.setO2(Double.valueOf(o2Str));
						
			           deviceData.getSensors().add(sensor);
			         
			           WebClientDeviceDataEndPoint.broadcastDeviceDataToWebClients(deviceData); //function to calculation timeDiffInMs
			           DeviceDataStoreEndpoint.saveToDb(deviceData); //also save timeDiffInMs to Db
			          // NetworkStoreEndpoint.saveToDb(mockNetwork);
			          // WebClientNetworkEndPoint.broadcastNetworkToWebClients(mockNetwork);
			           AlertEndpoint.sendToProcessAlert(deviceData);
		           } catch (JSONException e) {
		        	   // TODO Auto-generated catch block
		        	   e.printStackTrace();
		           }
		       };
		    }
		    catch (IOException e) {
		       System.out.println(e);
		    }
					 	
		 }
	 }
	 
	 @Override
	 public Validation sendEndPointConfig(int command, int length, String deviceAddress, String cordId,
			String measurePeriod) {
		// TODO Auto-generated method stub
		return null;
	 }
	
	 @Override
	 public void startServer() {
		 if (server == null || !server.isBound()) {
			 this.start();
		 }
		 started = true;
	 }
	 
	 @Override
	 public void stopServer() throws IOException {
		 started = false;
		 if (server != null && server.isBound()) {
			 server.close();
		 }
		 WebClientDeviceDataEndPoint.stop();
	 }

}
