package com.xmlasia.gemsx.device;

import java.nio.ByteBuffer;

public class PacketTool {

	
    public static final int getHexByte(byte[] bytes, int index) {
        return (bytes[index] & 0x00FF);
    }

    public static final int getInt(byte[] arrs) {
        String data = getHexString(arrs);
        return Integer.parseInt(data, 16);
    }

    public static final short getIntBySignedByte(byte[] arrs) {
        return (short) Integer.parseInt(getHexString(arrs), 16);
    }

    public static final int[] getIntArray(byte[] data) {
        int[] result = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            result[i] = getHexByte(data, i);
        }
        return result;
    }

    public static final byte[] getIntToHexStringByteArray(int i, int length) {
        return getHexStringToByteArray(intToHex(i), length);
    }

    public static final byte[] getHexStringToByteArray(String s, int length) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        byte[] result;
        if (data.length < length) {
            result = new byte[length];
            int i;
            for (i = 0; i < length - data.length; i++) {
                result[i] = 0x00;
            }
            for (int j = 0; i < length; i++, j++) {
                result[i] = data[j];
            }
        } else {
            result = data;
        }
        return result;
    }
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static final String getHexString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static final String getHexString(int[] arrs) {
        String hex = "";
        for (int arr : arrs) {
            if (arr < 10) {
                hex += "0" + arr;
            } else {
                if (arr < 16) {
                    hex += "0" + Integer.toHexString(arr);
                } else {
                    hex += Integer.toHexString(arr);
                }
            }
        }
        return hex;
    }

    public static final String getHexBinary(String Hex) {
        int i = Integer.parseInt(Hex);
        String Bin = Integer.toBinaryString(i);
        return Bin;
    }

    public static final int getHexBinaryInt(String Hex) {
        int i = Integer.parseInt(Hex);
        return i;
    }

    public static final double getIEEE754(int[] data) {
        String hexString = getHexString(data);
        Long i = Long.parseLong(hexString, 16);
        return Float.intBitsToFloat(i.intValue());
    }

    public static final double getDouble(byte[] data) {
        float f =  ByteBuffer.wrap(data).getFloat();
        return f;
    }

    public static final String intToHex(int arr) {
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.toHexString(arr));
        if (sb.length() % 2 != 0) {
            sb.insert(0, '0');
        }
        String hex = sb.toString();
        return hex;
    }
    
	public static double round(double i) {
		return (double) (Math.round(i * 10) / 10.0);
	}
}
