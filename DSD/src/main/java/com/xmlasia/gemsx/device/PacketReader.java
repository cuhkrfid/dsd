package com.xmlasia.gemsx.device;

import java.io.DataInputStream;
import java.io.IOException;

public class PacketReader implements IPacketReader {

	private DataInputStream dataStream;
	
	public PacketReader( DataInputStream dataStream) {
		this.dataStream = dataStream;
	}
	
	public int read( byte[] packetData, boolean isSwappedByte ) throws IOException {
		int rtn = dataStream.read(packetData);
		if ( isSwappedByte ) {
			byte[] swappedByte = byteSwap(packetData, packetData.length);
			System.arraycopy(swappedByte, 0, packetData, 0,swappedByte.length);
		}
		return rtn;
	}
	
	private byte[] byteSwap(byte[] b, int length) {
		byte[] swappedByte = new byte[length];
		for (int i = length-1; i >= 0; --i) {
			swappedByte[length-1-i] = b[i];
		}
		return swappedByte;
	}
	
}
