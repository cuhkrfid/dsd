package com.xmlasia.gemsx.device;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.DeviceDataDao;
import com.xmlasia.gemsx.dao.ReaderIdToNameMapDao;
import com.xmlasia.gemsx.dao.SensorConfigDao;
import com.xmlasia.gemsx.dao.SensorDao;
import com.xmlasia.gemsx.domain.DeviceData;
import com.xmlasia.gemsx.domain.RawHexStringDeviceData;
import com.xmlasia.gemsx.domain.ReaderIdToNameMap;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.SensorConfig;

public class DeviceDataStoreEndpoint extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	private DeviceDataDao deviceDataDao;
	private SensorDao sensorDao;
	private SensorConfigDao sensorConfigDao;
	private ReaderIdToNameMapDao readerIdToNameMapDao;
	
	@Override
    public void run() {
        while(!(Thread.currentThread().isInterrupted())) {
            try {
                final DeviceData deviceData = deviceDataQueue.take();
                this.save(deviceData);

            } catch (InterruptedException e) {
                // Set interrupted flag.
                Thread.currentThread().interrupt();
            } catch (Exception e) {
            	log.error(this.getClass().toString() + ": "+ e.getMessage());
            }
        }

        // Thread is getting ready to die, but first,
        // drain remaining elements on the queue and process them.
        final List<DeviceData> remainingObjects = new ArrayList<DeviceData>();
        deviceDataQueue.drainTo(remainingObjects);
       
        for(DeviceData deviceData : remainingObjects) {
            this.save(deviceData);
        }
    }

    private void save(final DeviceData deviceData) {
        // Do something with the complex object.
    	try {
    		
    		RawHexStringDeviceData rawHexStringDeviceData = deviceData.getRawHexStringDeviceData();
    		if ( rawHexStringDeviceData != null) {
    			deviceDataDao.create(rawHexStringDeviceData);
    			return;
    		}
    		
    		DeviceData savedDeviceData = deviceDataDao.createDeviceData(deviceData);
	    	List<Sensor> sensors = deviceData.getSensors();
	    	for ( Sensor sensor : sensors) {
	    		sensor.setDeviceDataId(savedDeviceData.getId());
	    		sensor.setInputDate(savedDeviceData.getInputDate());
	    		sensor.setReaderId(savedDeviceData.getReaderId());
	    		sensor.setDeviceId(savedDeviceData.getDeviceId());
	    		sensor.setRssi(savedDeviceData.getRssi());
	    		
	    		sensor.setTagId(savedDeviceData.getTagId());
	    		sensor.setPacketCounter(savedDeviceData.getPacketCounter());
	    		sensor.setMaxStep(savedDeviceData.getMaxStep());
	    		sensor.setAdcValue(savedDeviceData.getAdcValue());
	    		
	    		sensorDao.createDeviceData(sensor);
	    	
	    		SensorConfig sensorConfig = new SensorConfig();
	    		sensorConfig.setSensorId(sensor.getSensorId());
	    		sensorConfig.setDeviceId(deviceData.getDeviceId());
	    		sensorConfig.setDeviceAddress(deviceData.getDeviceAddress());	
	    		sensorConfig.setMeasurePeriod(deviceData.getMeasurePeriod());
	    
	    		sensorConfig.setIsCoverLevel(sensor.getIsCoverLevel());
	    		sensorConfig.setIsWaterLevel(sensor.getIsWaterLevel());
	    		sensorConfig.setIsH2s(sensor.getIsH2s());
	    		sensorConfig.setIsNh3(sensor.getIsNh3());
	    		sensorConfig.setIsSo2(sensor.getIsSo2());
	    		sensorConfig.setIsCo2(sensor.getIsCo2());
	    		sensorConfig.setIsCh4(sensor.getIsCh4());
	    		sensorConfig.setIsO2(sensor.getIsO2());
		    		
	    		sensorConfig.setAutoCreate(true);
	    		sensorConfig.setInputDate(deviceData.getInputDate());
	    		
	    		SensorConfigStoreEndpoint.saveToDb(sensorConfig);
	    	}
	    
	    	if ( deviceData.getReaderIds() != null && deviceData.getReaderIds().length() > 0 ) {
		    	String[] readerIds = deviceData.getReaderIds().split(",");
		    	for ( String readerIdStr : readerIds) {
	    	    	ReaderIdToNameMap readerIdToNameMap = new ReaderIdToNameMap();
	    	    	readerIdToNameMap.setReaderId(Long.valueOf(readerIdStr));
	    	    	readerIdToNameMapDao.createReaderIdToNameMapIfNotFound(readerIdToNameMap);	    		
		    	}
	    	}
	    	
    	} catch (Exception e) {
    		log.error("save to db error: " + e.getMessage());
    	}
    }
	
    private static BlockingQueue<DeviceData> deviceDataQueue = new LinkedBlockingQueue<DeviceData>();
    
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}

	public DeviceDataDao getDeviceDataDao() {
		return deviceDataDao;
	}

	public void setDeviceDataDao(DeviceDataDao deviceDataDao) {
		this.deviceDataDao = deviceDataDao;
	}

	public SensorDao getSensorDao() {
		return sensorDao;
	}

	public void setSensorDao(SensorDao sensorDao) {
		this.sensorDao = sensorDao;
	}	
	
	static void saveToDb( DeviceData deviceData) {
		deviceDataQueue.offer(deviceData);
	}

	public SensorConfigDao getSensorConfigDao() {
		return sensorConfigDao;
	}

	public void setSensorConfigDao(SensorConfigDao sensorConfigDao) {
		this.sensorConfigDao = sensorConfigDao;
	}

	public ReaderIdToNameMapDao getReaderIdToNameMapDao() {
		return readerIdToNameMapDao;
	}

	public void setReaderIdToNameMapDao(ReaderIdToNameMapDao readerIdToNameMapDao) {
		this.readerIdToNameMapDao = readerIdToNameMapDao;
	}
}
