package com.xmlasia.gemsx.device;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EndDeviceConfigData {
	public static final int LENGTH_MEASUREPERIOD = 2;
	private int measurementPeriod;
	
	public int getMeasurementPeriod() {
	    return measurementPeriod;
	}
	
	public void setMeasurementPeriod(int measurementPeriod) {
	    this.measurementPeriod = measurementPeriod;
	}
	
	
	public byte[] getByte() {
	    try {
	        ByteArrayOutputStream b = new ByteArrayOutputStream();
	        String s = PacketTool.intToHex(measurementPeriod);
	        b.write(PacketTool.getHexStringToByteArray(s, 2));
	        return b.toByteArray();
	    } catch (IOException ex) {
	        Logger.getLogger(EndDeviceConfigData.class.getName()).log(Level.SEVERE, null, ex);
	    }
	    return null;
	}
}
