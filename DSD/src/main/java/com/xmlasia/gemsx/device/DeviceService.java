package com.xmlasia.gemsx.device;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.DeviceToReaderDao;
import com.xmlasia.gemsx.dao.ReaderIdToNameMapDao;
import com.xmlasia.gemsx.service.impl.ServerConfigService;

public class DeviceService {

	Log log = LogFactory.getLog(this.getClass());
	IServer myserver = null;

	private int port;
	private String ipAddr; 
	private int backLog = 50;
	private String socketServerClass;
	private int nioSocketServerReadTimeoutInMin = 360;
	private boolean nioSocketServerSaveRaw = true;
	
	private int mockServerNumOfClients = 10; 
	private int mockServerMeasurePeriod = 10000;
	private int mockServerSeriesInterval = 50;
	private DeviceToReaderDao deviceToReaderDao;
	private int rssiDataThreadSleepMs = 1000;
	private int rssiPacketReleaseTimeoutMs = 2000;
	
	private boolean viewHeartBeatInDiagnosticSensorView = false;
	private int clearReaderRssiMins = 3;

	private ReaderIdToNameMapDao readerIdToNameMapDao;
	private DbServerCache dbServerCache; 
	private ServerConfigService serverConfigService;
	
	public void init() throws Exception  {

		if ( socketServerClass.equals("DSDServerSimulator"))
			myserver = new DSDServerSimulator(backLog, port, ipAddr);
		else if ( socketServerClass.equals("MockServer"))
			myserver = new MockServer(mockServerNumOfClients, mockServerMeasurePeriod, mockServerSeriesInterval);
		else 
			myserver = new NioSocketServer(port, 
										  ipAddr, 
										  nioSocketServerReadTimeoutInMin, 
										  nioSocketServerSaveRaw, 
										  getDeviceToReaderDao(), 
										  rssiDataThreadSleepMs, 
										  rssiPacketReleaseTimeoutMs, 
										  viewHeartBeatInDiagnosticSensorView, 
										  clearReaderRssiMins,
										  readerIdToNameMapDao, 
										  dbServerCache,
										  getServerConfigService() );	
			
	    myserver.startServer();
	    log.info("Start Socket server on " + port);
	}
	
	public void destroy() throws Exception {
	    log.info("Close Socket server");
	    if (myserver != null) {
	        try {
	            myserver.stopServer();
	        } catch (IOException ex) {
	            log.error(ex);
	        }
	    }
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public String getIpAddr() {
		return ipAddr;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public int getBackLog() {
		return backLog;
	}

	public void setBackLog(int backLog) {
		this.backLog = backLog;
	}
	    
	public IServer getServer() {
		return myserver;
	}

	public String getSocketServerClass() {
		return socketServerClass;
	}

	public void setSocketServerClass(String socketServerClass) {
		this.socketServerClass = socketServerClass;
	}

	public int getMockServerNumOfClients() {
		return mockServerNumOfClients;
	}

	public void setMockServerNumOfClients(int mockServerNumOfClients) {
		this.mockServerNumOfClients = mockServerNumOfClients;
	}

	public int getMockServerMeasurePeriod() {
		return mockServerMeasurePeriod;
	}

	public void setMockServerMeasurePeriod(int mockServerMeasurePeriod) {
		this.mockServerMeasurePeriod = mockServerMeasurePeriod;
	}

	public int getMockServerSeriesInterval() {
		return mockServerSeriesInterval;
	}

	public void setMockServerSeriesInterval(int mockServerSeriesInterval) {
		this.mockServerSeriesInterval = mockServerSeriesInterval;
	}

	public int getNioSocketServerReadTimeoutInMin() {
		return nioSocketServerReadTimeoutInMin;
	}

	public void setNioSocketServerReadTimeoutInMin(int nioSocketServerReadTimeoutInMin) {
		this.nioSocketServerReadTimeoutInMin = nioSocketServerReadTimeoutInMin;
	}

	public boolean getNioSocketServerSaveRaw() {
		return nioSocketServerSaveRaw;
	}

	public void setNioSocketServerSaveRaw(boolean nioSocketServerSaveRaw) {
		this.nioSocketServerSaveRaw = nioSocketServerSaveRaw;
	}

	public DeviceToReaderDao getDeviceToReaderDao() {
		return deviceToReaderDao;
	}

	public void setDeviceToReaderDao(DeviceToReaderDao deviceToReaderDao) {
		this.deviceToReaderDao = deviceToReaderDao;
	}

	public int getRssiDataThreadSleepMs() {
		return rssiDataThreadSleepMs;
	}

	public void setRssiDataThreadSleepMs(int rssiDataThreadSleepMs) {
		this.rssiDataThreadSleepMs = rssiDataThreadSleepMs;
	}

	public int getRssiPacketReleaseTimeoutMs() {
		return rssiPacketReleaseTimeoutMs;
	}

	public void setRssiPacketReleaseTimeoutMs(int rssiPacketReleaseTimeoutMs) {
		this.rssiPacketReleaseTimeoutMs = rssiPacketReleaseTimeoutMs;
	}

	public boolean isViewHeartBeatInDiagnosticSensorView() {
		return viewHeartBeatInDiagnosticSensorView;
	}

	public void setViewHeartBeatInDiagnosticSensorView(boolean viewHeartBeatInDiagnosticSensorView) {
		this.viewHeartBeatInDiagnosticSensorView = viewHeartBeatInDiagnosticSensorView;
	}

	public int getClearReaderRssiMins() {
		return clearReaderRssiMins;
	}

	public void setClearReaderRssiMins(int clearReaderRssiMins) {
		this.clearReaderRssiMins = clearReaderRssiMins;
	}

	public DbServerCache getDbServerCache() {
		return dbServerCache;
	}

	public void setDbServerCache(DbServerCache dbServerCache) {
		this.dbServerCache = dbServerCache;
	}

	public ReaderIdToNameMapDao getReaderIdToNameMapDao() {
		return readerIdToNameMapDao;
	}

	public void setReaderIdToNameMapDao(ReaderIdToNameMapDao readerIdToNameMapDao) {
		this.readerIdToNameMapDao = readerIdToNameMapDao;
	}

	public ServerConfigService getServerConfigService() {
		return serverConfigService;
	}

	public void setServerConfigService(ServerConfigService serverConfigService) {
		this.serverConfigService = serverConfigService;
	}

}
