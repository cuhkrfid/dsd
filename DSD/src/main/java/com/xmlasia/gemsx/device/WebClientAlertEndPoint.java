package com.xmlasia.gemsx.device;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import javax.inject.Singleton;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.JsonObject;
import com.xmlasia.gemsx.domain.Alert;

@ServerEndpoint(value="/alertsocket")
@Singleton
public class WebClientAlertEndPoint {
   
	static class QueueConsumer extends Thread {
		
		Log log = LogFactory.getLog(this.getClass());
		private boolean isStarted = false; 
	
		@Override
	    public void run() {
			
	        while(!(Thread.currentThread().isInterrupted())) {
	            try {
	                final Alert alert = alertQueue.take();
	                this.process(alert);
	                Thread.sleep(500);
	            } catch (InterruptedException e) {
	                // Set interrupted flag.
	                Thread.currentThread().interrupt();
	            } catch (Exception e) {
	            	log.error(this.getClass().toString() + ": "+ e.getMessage());
	            }
	        }
	
	        // Thread is getting ready to die, but first,
	        // drain remaining elements on the queue and process them.
	        final List<Alert> remainingObjects = new ArrayList<Alert>();
	        alertQueue.drainTo(remainingObjects);
	       
	        for(Alert alert : remainingObjects) {
	            this.process(alert);
	        }
	    }
	
	    private void process(final Alert alert) {
	        // Do something with the complex object.
	    	sendToWebClients(alert);
	    }
	    
	    public boolean getIsStarted() { return isStarted; }
	    public void setIsStarted(boolean started) { this.isStarted = started; }
	};
	
    static Set<Session> userSessions = Collections.synchronizedSet(new HashSet<Session>());
    static QueueConsumer queueConsumer = new WebClientAlertEndPoint.QueueConsumer();
    static BlockingQueue<Alert> alertQueue = new LinkedBlockingQueue<Alert>();
   
	static ConcurrentHashMap<String, Alert> deviceAddrToNetwork 
		= new ConcurrentHashMap<String, Alert>();
	
    /**
     * Callback hook for Connection open events. 
     * 
     * This method will be invoked when a client requests for a 
     * WebSocket connection.
     * 
     * @param userSession the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) {
    	
    	if ( ! queueConsumer.getIsStarted() ) {
    		queueConsumer.start();
    		queueConsumer.setIsStarted(true);
    	}
    	
        userSessions.add(userSession);
    }
    
    /**
     * Callback hook for Connection close events.
     * 
     * This method will be invoked when a client closes a WebSocket
     * connection.
     * 
     * @param userSession the userSession which is opened.
     */
    @OnClose
    public void onClose(Session userSession) {
        userSessions.remove(userSession);
    }
    
    /**
     * Callback hook for Message Events.
     * 
     * This method will be invoked when a client send a message.
     * 
     * @param message The text message
     * @param userSession The session of the client
     */
    @OnMessage
    public void onMessage(String message, Session userSession) {

    }
     
    private static void sendToWebClients(Alert alert) {
    	 	
	    JsonObject alertDataObject = new JsonObject();
	    	
	    alertDataObject.addProperty("msgType", "alert" );
	    alertDataObject.addProperty("id", alert.getId());
	    alertDataObject.addProperty("alertSettingId", alert.getAlertSettingId());
    	
    	DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dateTimeString = df.format(alert.getCreateDateTime());
    	alertDataObject.addProperty("createDateTimeStr", dateTimeString);
    	alertDataObject.addProperty("createDateTimeLong", alert.getCreateDateTime().getTime());
    	
    	df = new SimpleDateFormat("HH:mm:ss");
		String timeString = df.format(alert.getCreateDateTime());
    	alertDataObject.addProperty("createTimeStr", timeString);
    	
    	
    	alertDataObject.addProperty("level", alert.getAlertSetting().getLevel());
    	alertDataObject.addProperty("name", alert.getAlertSetting().getName());
    	alertDataObject.addProperty("deviceAddress", alert.getDeviceAddress());
      	alertDataObject.addProperty("deviceId", alert.getDeviceId());
      	alertDataObject.addProperty("sensorId", alert.getSensorId());
      	alertDataObject.addProperty("groupId", alert.getGroupId());
 
       	alertDataObject.addProperty("isCoverDetect", alert.getIsCoverDetect());
    	alertDataObject.addProperty("isCoverLevel", alert.getIsCoverLevel());
     	alertDataObject.addProperty("isWaterLevel", alert.getIsWaterLevel());
     	alertDataObject.addProperty("isH2s", alert.getIsH2s());
    	alertDataObject.addProperty("isSo2", alert.getIsSo2());
    	alertDataObject.addProperty("isCo2", alert.getIsCo2());    	          
    	alertDataObject.addProperty("isCh4", alert.getIsCh4());    	          
    	alertDataObject.addProperty("isNh3", alert.getIsNh3());    	          
    	alertDataObject.addProperty("isO2", alert.getIsO2());    	          

    	alertDataObject.addProperty("coverDetect", alert.getCoverDetect());
      	alertDataObject.addProperty("coverLevel", alert.getCoverLevel());
      	alertDataObject.addProperty("waterLevel", alert.getWaterLevel());
      	alertDataObject.addProperty("h2s", alert.getH2s());
      	alertDataObject.addProperty("so2", alert.getSo2());
     	alertDataObject.addProperty("co2", alert.getCo2());
     	alertDataObject.addProperty("ch4", alert.getCh4());
     	alertDataObject.addProperty("nh3", alert.getNh3());
     	alertDataObject.addProperty("o2", alert.getO2());
     	
    	alertDataObject.addProperty("alertSettingLevel", alert.getAlertSettingLevel());
    	alertDataObject.addProperty("alertSettingName", alert.getAlertSettingName());	          
    	alertDataObject.addProperty("tagConfigurationId", alert.getTagConfigurationId());
    	alertDataObject.addProperty("siteConfigurationId", alert.getSiteConfigurationId());
    	
    	alertDataObject.addProperty("tagConfigurationName", alert.getTagConfiguration().getName());
        
    	alertDataObject.addProperty("coverLevelAlertMessage", alert.getCoverLevelAlertMessage());
    	alertDataObject.addProperty("waterLevelAlertMessage", alert.getWaterLevelAlertMessage());
    	alertDataObject.addProperty("co2AlertMessage", alert.getCo2AlertMessage());
    	alertDataObject.addProperty("h2sAlertMessage", alert.getH2sAlertMessage());
    	alertDataObject.addProperty("so2AlertMessage", alert.getSo2AlertMessage());
    	alertDataObject.addProperty("ch4AlertMessage", alert.getCh4AlertMessage());
    	alertDataObject.addProperty("nh3AlertMessage", alert.getNh3AlertMessage());
    	alertDataObject.addProperty("o2AlertMessage", alert.getO2AlertMessage());
    	alertDataObject.addProperty("mobileAlertIcon", alert.getMobileAlertIcon());
        
    	
    	String alertMessage = Alert.createMessage(alert, System.lineSeparator());
    	if ( alertMessage != null && alertMessage.length() > 0 ) {
    		alertDataObject.addProperty("messageHtml", alertMessage);    
    	}		
    
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
    	
        String message = alertDataObject.toString();       
    	for (Session session : userSessions) {
    		String queryString = session.getQueryString();
    		session.getAsyncRemote().sendText(message);
    	} 
    }
	
    
    public static void sendAlert(Alert alert) {
    	if ( ! queueConsumer.getIsStarted() )
    		return;

    	alertQueue.offer(alert);
    }

    public static void stop() {
    	queueConsumer.interrupt();
    }
}