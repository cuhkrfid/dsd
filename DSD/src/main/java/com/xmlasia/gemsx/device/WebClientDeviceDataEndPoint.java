/**
 * ChatServerEndPoint.java
 * http://programmingforliving.com
 */
package com.xmlasia.gemsx.device;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import javax.inject.Singleton;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.xmlasia.gemsx.domain.CurrentUserView;
import com.xmlasia.gemsx.domain.DeviceData;
import com.xmlasia.gemsx.domain.Sensor;
import com.xmlasia.gemsx.domain.TagConfiguration;

/**
 * ChatServer
 * @author Jiji_Sasidharan
 */
@ServerEndpoint(value="/chat")
@Singleton
public class WebClientDeviceDataEndPoint {
   
	
	static class QueueConsumer extends Thread {
		
		Log log = LogFactory.getLog(this.getClass());
		private boolean isStarted = false; 
	
		@Override
	    public void run() {
			
	        while(!(Thread.currentThread().isInterrupted())) {
	            try {
	                final DeviceData deviceData = deviceDataQueue.take();
	                process(deviceData);
	                Thread.sleep(500);
	
	            } catch (InterruptedException e) {
	                // Set interrupted flag.
	                Thread.currentThread().interrupt();
	            } catch (Exception e) {
	            	log.error(this.getClass().toString() + ": "+ e.getMessage());
	            }
	        }
	
	        // Thread is getting ready to die, but first,
	        // drain remaining elements on the queue and process them.
	        final List<DeviceData> remainingObjects = new ArrayList<DeviceData>();
	        deviceDataQueue.drainTo(remainingObjects);
	       
	        for(DeviceData deviceData : remainingObjects) {
	            process(deviceData);
	        }
	    }
	
	    private void process(final DeviceData deviceData) {
	        // Do something with the complex object.
	    	sendToWebClients(deviceData);
	    }
	    
	    public boolean getIsStarted() { return isStarted; }
	    public void setIsStarted(boolean started) { this.isStarted = started; }
	};
	
    static Set<Session> userSessions = Collections.synchronizedSet(new HashSet<Session>());
    static QueueConsumer queueConsumer = new WebClientDeviceDataEndPoint.QueueConsumer();
    static BlockingQueue<DeviceData> deviceDataQueue = new LinkedBlockingQueue<DeviceData>();
	static ConcurrentHashMap<String, Set<CurrentUserView>> currentUserViews 
		= new ConcurrentHashMap<String, Set<CurrentUserView>>();

	static ConcurrentHashMap<String, DeviceData> deviceAddrToDeviceData 
		= new ConcurrentHashMap<String, DeviceData>();
	
    /**
     * Callback hook for Connection open events. 
     * 
     * This method will be invoked when a client requests for a 
     * WebSocket connection.
     * 
     * @param userSession the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) {
    	if ( ! queueConsumer.getIsStarted() ) {
    		queueConsumer.setIsStarted(true);
    		queueConsumer.start();
    	}
        userSessions.add(userSession);
    }
    
    /**
     * Callback hook for Connection close events.
     * 
     * This method will be invoked when a client closes a WebSocket
     * connection.
     * 
     * @param userSession the userSession which is opened.
     */
    @OnClose
    public void onClose(Session userSession) {
        userSessions.remove(userSession);
    }
    
    /**
     * Callback hook for Message Events.
     * 
     * This method will be invoked when a client send a message.
     * 
     * @param message The text message
     * @param userSession The session of the client
     */
    @OnMessage
    public void onMessage(String message, Session userSession) {
//        for (Session session : userSessions) {
//            session.getAsyncRemote().sendText(message);
//        }
    }
    
	private static boolean viewAllSnr = false;   
    public static void setViewAllSnr(boolean viewAllSnrBool) {
		viewAllSnr = viewAllSnrBool;
	}
    
    public static boolean getViewAllSnr( ) {
		//return viewAllSnr;
		return true;
	}
	
    private static void sendToWebClients(DeviceData deviceData) {
    	Map<Integer,JsonObject> sensorIdToJson = new HashMap<Integer,JsonObject>();
    	List<Sensor> sensors = deviceData.getSensors();
    	if ( sensors != null) {
      		//construct Json object
      		for (Sensor sensor : sensors) {
      			
      	    	JsonObject sensorDataObject = new JsonObject();
      	    	sensorDataObject.addProperty("msgType", "deviceData" );
      	    	sensorDataObject.addProperty("date", deviceData.getInputDate().getTime());
      	    	
				DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String dateString = df.format(deviceData.getInputDate());
				sensorDataObject.addProperty("dateString", dateString);
				
				DateFormat df2 = new SimpleDateFormat("HH:mm:ss");
				String dateString2 = df2.format(deviceData.getInputDate());
				sensorDataObject.addProperty("dateString2", dateString2);
				
      	    	sensorDataObject.addProperty("deviceId", deviceData.getTagId());
      	    	sensorDataObject.addProperty("sensorId", sensor.getSensorId());

      	    	sensorDataObject.addProperty("coverDetectStr", sensor.getCoverDetectStr());
      	    	sensorDataObject.addProperty("waterLevel1DetectStr", sensor.getWaterLevel1DetectStr());
      	    	sensorDataObject.addProperty("waterLevel2DetectStr", sensor.getWaterLevel2DetectStr());
      	    	sensorDataObject.addProperty("antennaOptimizedStr", sensor.getAntennaOptimizedStr());
      	    	sensorDataObject.addProperty("batteryStr", sensor.getBattery());
      	    	
      	    	sensorDataObject.addProperty("isCoverLevel", sensor.getIsCoverLevel());
      	    	sensorDataObject.addProperty("isWaterLevel", sensor.getIsWaterLevel());
      	    	sensorDataObject.addProperty("isH2s", sensor.getIsH2s());
      	    	sensorDataObject.addProperty("isSo2", sensor.getIsSo2());
     	    	sensorDataObject.addProperty("isCo2", sensor.getIsCo2());
      	    	sensorDataObject.addProperty("isCh4", sensor.getIsCh4());
      	    	sensorDataObject.addProperty("isNh3", sensor.getIsNh3());
      	    	sensorDataObject.addProperty("isO2", sensor.getIsO2());
 
      	    	//if ( sensor.getIsCoverLevel() ) {
      	    		sensorDataObject.addProperty("coverLevel", sensor.getCoverLevel());
      	    	//} else {
      	    	//	sensorDataObject.addProperty("coverLevel", "");
      	    	//}
      	    	
//      	    	if ( sensor.getIsWaterLevel() ) {
      	    		sensorDataObject.addProperty("waterLevel", sensor.getWaterLevel());
      	    		int waterLevelFromZero2 = -1 * sensor.getWaterLevel();
      	    		sensorDataObject.addProperty("waterLevelFromZero2", waterLevelFromZero2 );
//      	    	} else {
//      	    		sensorDataObject.addProperty("waterLevel", "");
//      	    	}
//      	    	
//      	    	if ( sensor.getIsH2s() ) {
      	    		sensorDataObject.addProperty("h2s", sensor.getH2s());
//      	    	} else {
//      	    		sensorDataObject.addProperty("h2s", "");
//      	    	}
      	    	
//	      	  	if ( sensor.getIsSo2() ) {
	      	    	sensorDataObject.addProperty("so2", sensor.getSo2());
//	  	    	} else {
//	      	    	sensorDataObject.addProperty("so2",  "");
//	  	    	}
  	    	
//	      	  	if ( sensor.getIsCo2() ) {
	      	    	sensorDataObject.addProperty("co2", sensor.getCo2());	
//	      	  	} else {
//	      	    	sensorDataObject.addProperty("co2", "");	    	
//	      	  	}
//	      	  	
//	      	  	if ( sensor.getIsCh4() ) {
	      	    	sensorDataObject.addProperty("ch4", sensor.getCh4());   	    	
//	      	  	} else {
//	      	    	sensorDataObject.addProperty("ch4", "");   		
//	      	  	}
	      	  	
//	      	  	if ( sensor.getIsNh3() ) {
	      	    	sensorDataObject.addProperty("nh3", sensor.getNh3());   	    	
//	      	  	} else {
//	      	    	sensorDataObject.addProperty("nh3", "");   		
//	      	  	}
	      	  	
//	      	  	if ( sensor.getIsO2() ) {
	      	    	sensorDataObject.addProperty("o2", sensor.getO2());   	    	
//	      	  	} else {
//	      	    	sensorDataObject.addProperty("o2", "");   		
//	      	  	}
	      	  	
	      	  	
      	    	sensorDataObject.addProperty("parentAddress", deviceData.getParentAddress());
      	    	sensorDataObject.addProperty("deviceAddress", deviceData.getDeviceAddress());
      	    	sensorDataObject.addProperty("readerId", deviceData.getReaderId());
      	    	sensorDataObject.addProperty("readerIds", deviceData.getReaderIds());
      	    	sensorDataObject.addProperty("rssiStr", deviceData.getRssiStr());
      	    	sensorDataObject.addProperty("readerRssi", deviceData.getReaderRssi());
      	    	sensorDataObject.addProperty("readerNameRssi", deviceData.getReaderNameRssi());
     	    	sensorDataObject.addProperty("readerRssiSnr", deviceData.getReaderRssiSnr());
     	    	sensorDataObject.addProperty("readerNameRssiSnr", deviceData.getReaderNameRssiSnr());
     	    	      	    	
      	    	sensorDataObject.addProperty("cordId", deviceData.getTagId());
      	    	sensorDataObject.addProperty("numofSensors", deviceData.getNumberOfSensor());
      	    	sensorDataObject.addProperty("measurePeriod", deviceData.getMeasurePeriod());
             	   
      	    	sensorDataObject.addProperty("timeDiffInMsStr", deviceData.getTimeDiffInMsStr());
      	    	sensorDataObject.addProperty("timeDiffMaxInMsStr", deviceData.getMaxTimeDiffInMsStr());
          	    
      	    	
    	    	sensorDataObject.addProperty("coverDetect", sensor.getCoverDetect());
    	    	sensorDataObject.addProperty("antennaOptimized", sensor.getAntennaOptimized());
    	    	sensorDataObject.addProperty("waterLevel1Detect", sensor.getWaterLevel1Detect());
    	    	sensorDataObject.addProperty("waterLevel2Detect", sensor.getWaterLevel2Detect());
    	    	sensorDataObject.addProperty("battery", sensor.getIsLowBattery() == true ? 0 : 1);
    	    	
    	    	sensorDataObject.addProperty("rssi", deviceData.getRssi());
      	    	sensorDataObject.addProperty("packetCounter", deviceData.getPacketCounter());
      	    	
      	    	sensorDataObject.addProperty("userDefine", deviceData.getUserDefine());
      	    	sensorDataObject.addProperty("maxStep", deviceData.getMaxStep());
      	    	sensorDataObject.addProperty("timeSlot", deviceData.getTimeSlot());
      	    	sensorDataObject.addProperty("adcValue", deviceData.getAdcValue());
      	    	
      	    	sensorIdToJson.put(sensor.getSensorId(), sensorDataObject); 
      		}
      	}
    	
    	
       for (Session session : userSessions) {
    		 JsonArray jdataList = getSessionResultToSend(deviceData, sensorIdToJson, session, true);
    		 if ( jdataList != null && jdataList.size() > 0) {	
    			 String message = jdataList.toString();
    			 session.getAsyncRemote().sendText(message);	
    		 }
    	}    
    }

    //filter in only the deviceId that the user wish to see
	private static JsonArray getSessionResultToSend(DeviceData deviceData, Map<Integer,JsonObject> sensorIdToJson, Session session, boolean ignoreUserName) {
		
		JsonArray jdataList = new JsonArray();
		Set<CurrentUserView> userView = new HashSet<CurrentUserView>();
		String userName = "";

		if ( ! ignoreUserName ) {
			Principal principal = session.getUserPrincipal();
			if (principal == null) {
				String queryString = session.getQueryString();
				String[] queryStringEntries = queryString.split("=");
				if ( queryStringEntries.length < 2 || !queryStringEntries[0].equals("userName")) {
					return null;
				}
				userName = queryStringEntries[1];
			} else {
				userName = principal.getName();
			} 
			userView = currentUserViews.get(userName);
			if (userView == null && !ignoreUserName)
				return null;
		}
			
		boolean addAll =  false;
		if ( !ignoreUserName) {
			try {
				for ( CurrentUserView deviceView : userView) {
					if (deviceView.getDeviceId() == -1 ) {
						addAll = true;
						break;
					}
					if (deviceView.getDeviceId() == deviceData.getDeviceId() )  { 
						JsonObject jsonObject = sensorIdToJson.get(deviceView.getSensorId());
						if ( jsonObject != null)
							jdataList.add(jsonObject);
					}
				}
				
			} catch (Exception e) {
			}
		}
		
		if ( addAll || ignoreUserName ) {
			jdataList = new JsonArray();
			for ( Map.Entry<Integer,JsonObject> entry : sensorIdToJson.entrySet()  ) {
				jdataList.add(entry.getValue());
			}
		}
		return jdataList;
	}
    public static void resetMaxTimeDiff(String deviceAddr) {
    	DeviceData lastReceiveDeviceData = deviceAddrToDeviceData.get(deviceAddr);
    	if ( lastReceiveDeviceData != null) {
    		lastReceiveDeviceData.setMaxTimeDiffInMsStr("N/A");
    	}
    }
	
    public static void broadcastDeviceDataToWebClients(DeviceData deviceData) {
    	
    	if ( ! queueConsumer.getIsStarted() )
    		return;
    	
     	DeviceData lastReceiveDeviceData = deviceAddrToDeviceData.get(deviceData.getDeviceAddress());
    	if ( lastReceiveDeviceData != null) {
    		long timeDiffInMs = deviceData.getInputDate().getTime() - lastReceiveDeviceData.getInputDate().getTime();
    		deviceData.setTimeDiffInMs(timeDiffInMs);
    		
    		String timeDiffInMsStr =  String.valueOf(timeDiffInMs);
    		deviceData.setTimeDiffInMsStr(timeDiffInMsStr);

    		lastReceiveDeviceData.setInputDate(deviceData.getInputDate());
    		
    		String lastMaxTimeDiffStr = lastReceiveDeviceData.getMaxTimeDiffInMsStr();
    		if ( lastMaxTimeDiffStr != null && !lastMaxTimeDiffStr.equals("N/A")) {
    			long lastMaxTimeDiff = Long.valueOf(lastMaxTimeDiffStr);
    			if ( lastMaxTimeDiff < timeDiffInMs) {
    				lastReceiveDeviceData.setMaxTimeDiffInMsStr(String.valueOf(timeDiffInMs));
    			}
    		} else {
    			lastReceiveDeviceData.setMaxTimeDiffInMsStr(timeDiffInMsStr);
    		}
    		deviceData.setMaxTimeDiffInMsStr(lastReceiveDeviceData.getMaxTimeDiffInMsStr());
    		
     	} else {
    		deviceData.setTimeDiffInMsStr("N/A");
    		deviceData.setMaxTimeDiffInMsStr("N/A");
         	deviceAddrToDeviceData.put(deviceData.getDeviceAddress(), deviceData);
    	}

    	deviceDataQueue.offer(deviceData);
    }
    
    public static void updateCurrentUserView(String userName, List<TagConfiguration> tagConfigurationToView) {
    	Set<CurrentUserView> userView = currentUserViews.get(userName);
    	if ( tagConfigurationToView == null || tagConfigurationToView.size() == 0)
    		return;

    	if (userView == null) {
    		userView = new HashSet<CurrentUserView>();
    	} else {
    		userView.clear();
    	}

    	for (TagConfiguration tagConfig : tagConfigurationToView) {
    		CurrentUserView deviceView = new CurrentUserView();
    		int deviceId = tagConfig.getDeviceId();
        	//int sensorId = tagConfig.getSensorId();	
    	
        	deviceView.setDeviceId(-1);
        	//deviceView.setSensorId(sensorId);
        	
        	userView.add(deviceView);
    	}
    	    	
     	currentUserViews.put(userName, userView);
    }
    
    
    public static void updateCurrentUserView(String userName, int deviceId, int sensorId) {
    	Set<CurrentUserView> userView = currentUserViews.get(userName);
   
    	if (userView == null) {
    		userView = new HashSet<CurrentUserView>();
    	} else {
    		userView.clear();
    	}

    	CurrentUserView deviceView = new CurrentUserView();
    	deviceView.setDeviceId(-1);
        //deviceView.setSensorId(-1);
        userView.add(deviceView);
    	    	
     	currentUserViews.put(userName, userView);
    }
    
    public static void stop() {
    	queueConsumer.interrupt();
    }
}
