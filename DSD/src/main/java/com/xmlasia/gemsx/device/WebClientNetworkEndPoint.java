package com.xmlasia.gemsx.device;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import javax.inject.Singleton;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.xmlasia.gemsx.domain.Network;

/**
 * ChatServer
 * @author Jiji_Sasidharan
 */
@ServerEndpoint(value="/networksocket")
@Singleton
public class WebClientNetworkEndPoint {
   
	static class QueueConsumer extends Thread {
		
		Log log = LogFactory.getLog(this.getClass());
		private boolean isStarted = false; 
	
		@Override
	    public void run() {
			
	        while(!(Thread.currentThread().isInterrupted())) {
	            try {
	                final Network network = networkQueue.take();
	                this.process(network);
	                Thread.sleep(500);
	
	            } catch (InterruptedException e) {
	                // Set interrupted flag.
	                Thread.currentThread().interrupt();
	            } catch (Exception e) {
	            	log.error(this.getClass().toString() + ": "+ e.getMessage());
	            }
	        }
	
	        // Thread is getting ready to die, but first,
	        // drain remaining elements on the queue and process them.
	        final List<Network> remainingObjects = new ArrayList<Network>();
	        networkQueue.drainTo(remainingObjects);
	       
	        for(Network network : remainingObjects) {
	            this.process(network);
	        }
	    }
	
	    private void process(final Network network) {
	        // Do something with the complex object.
	    	sendToWebClients(network);
	    }
	    
	    public boolean getIsStarted() { return isStarted; }
	    public void setIsStarted(boolean started) { this.isStarted = started; }
	};
	
    static Set<Session> userSessions = Collections.synchronizedSet(new HashSet<Session>());
    static QueueConsumer queueConsumer = new WebClientNetworkEndPoint.QueueConsumer();
    static BlockingQueue<Network> networkQueue = new LinkedBlockingQueue<Network>();
//	static ConcurrentHashMap<String, Set<CurrentUserView>> currentUserViews 
//		= new ConcurrentHashMap<String, Set<CurrentUserView>>();

	static ConcurrentHashMap<String, Network> deviceAddrToNetwork 
		= new ConcurrentHashMap<String, Network>();
	
    /**
     * Callback hook for Connection open events. 
     * 
     * This method will be invoked when a client requests for a 
     * WebSocket connection.
     * 
     * @param userSession the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) {
    	if ( ! queueConsumer.getIsStarted() ) {
    		queueConsumer.setIsStarted(true);
    		queueConsumer.start();
    	}
    	
        userSessions.add(userSession);
    }
    
    /**
     * Callback hook for Connection close events.
     * 
     * This method will be invoked when a client closes a WebSocket
     * connection.
     * 
     * @param userSession the userSession which is opened.
     */
    @OnClose
    public void onClose(Session userSession) {
        userSessions.remove(userSession);
    }
    
    /**
     * Callback hook for Message Events.
     * 
     * This method will be invoked when a client send a message.
     * 
     * @param message The text message
     * @param userSession The session of the client
     */
    @OnMessage
    public void onMessage(String message, Session userSession) {
//        for (Session session : userSessions) {
//            session.getAsyncRemote().sendText(message);
//        }
    }
    
   
    
    private static void sendToWebClients(Network network) {
    	
     	JsonArray jdataList = new JsonArray();
     	
	    JsonObject networkDataObject = new JsonObject();
	    networkDataObject.addProperty("msgType", "network" );
    	networkDataObject.addProperty("date", network.getUpdatedTime().getTime());
    	
    	DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dateString = df.format(network.getUpdatedTime());
		networkDataObject.addProperty("dateString", dateString);
    	networkDataObject.addProperty("deviceId", network.getDeviceId() != null ? String.valueOf(network.getDeviceId()) : "N/A" );
		networkDataObject.addProperty("routerId", network.getRouterId() != null ? String.valueOf(network.getRouterId()) : "N/A" );
    	networkDataObject.addProperty("deviceIdStr", network.getDeviceId() != null ? String.valueOf(network.getDeviceId()) : "N/A" );
		networkDataObject.addProperty("routerIdStr", network.getRouterId() != null ? String.valueOf(network.getRouterId()) : "N/A" );
		networkDataObject.addProperty("cordId", network.getCordId());
    	networkDataObject.addProperty("deviceTypeName", network.getDeviceTypeName());
		networkDataObject.addProperty("deviceType", network.getDeviceType());
		networkDataObject.addProperty("parentAddress", network.getParentAddress());
    	networkDataObject.addProperty("deviceAddress", network.getDeviceAddress());

    	networkDataObject.addProperty("timeDiffInMsStr", network.getTimeDiffInMsStr());
    	networkDataObject.addProperty("timeDiffMaxInMsStr", network.getTimeDiffMaxInMsStr());
  	    
    	jdataList.add(networkDataObject);
    	
        String message = jdataList.toString();       
    	for (Session session : userSessions) {
    		session.getAsyncRemote().sendText(message);
    	} 
    }
	
    
    public static void broadcastNetworkToWebClients(Network network) {
    	
    	if ( ! queueConsumer.getIsStarted() )
    		return;
    	
     	Network lastReceiveNetwork = deviceAddrToNetwork.get(network.getDeviceAddress());
    	if ( lastReceiveNetwork != null) {
    		long timeDiffInMs = network.getUpdatedTime().getTime() - lastReceiveNetwork.getUpdatedTime().getTime();
    
    		String timeDiffInMsStr =  String.valueOf(timeDiffInMs);
    		network.setTimeDiffInMsStr(timeDiffInMsStr);
    	    
    		lastReceiveNetwork.setUpdatedTime(network.getUpdatedTime());
    		
    		String lastMaxTimeDiffStr = lastReceiveNetwork.getTimeDiffMaxInMsStr();
    		if ( lastMaxTimeDiffStr != null && !lastMaxTimeDiffStr.equals("N/A")) {
    			long lastMaxTimeDiff = Long.valueOf(lastMaxTimeDiffStr);
    			if ( lastMaxTimeDiff < timeDiffInMs) {
    				lastReceiveNetwork.setTimeDiffMaxInMsStr(String.valueOf(timeDiffInMs));
    			}
    		} else {
    			lastReceiveNetwork.setTimeDiffMaxInMsStr(timeDiffInMsStr);
    		}
    		network.setTimeDiffMaxInMsStr(lastReceiveNetwork.getTimeDiffMaxInMsStr());
    		
    	} else {
    		network.setTimeDiffInMsStr("N/A");
    		network.setTimeDiffMaxInMsStr("N/A");
    		deviceAddrToNetwork.put(network.getDeviceAddress(), network);
        }
     	networkQueue.offer(network);
    }

    public static void stop() {
    	queueConsumer.interrupt();
    }
    
    public static void resetMaxTimeDiff(String deviceAddr) {
    	Network lastReceiveNetwork = deviceAddrToNetwork.get(deviceAddr);
    	if ( lastReceiveNetwork != null) {
    		lastReceiveNetwork.setTimeDiffMaxInMsStr("N/A");
    	}
    }
}
