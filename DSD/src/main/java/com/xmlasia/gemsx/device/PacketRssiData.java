package com.xmlasia.gemsx.device;

import java.util.Date;

public class PacketRssiData {

	private Long tagId;
	private Long readerId; 
	private String rssi = null; 
	private IPacketReader packetReader;
	private Date packetReceiveDate;
	private int seqNum;
	private String key = "";
	private String readerIdStr = null;
	private String readerRssi = null;
	private String readerNameRssi = null;
	private String readerRssiSnr = null;
	private String readerNameRssiSnr = null;
	
	
	public PacketRssiData(Long tagId, Long readerId, String rssi, String readerRssi, String readerNameRssi, String readerRssiSnr, String readerNameRssiSnr, IPacketReader packetReader, Date packetReceiveDate, int seqNum) {
		this.tagId = tagId;
		this.readerId = readerId;
		this.readerIdStr = readerId.toString();
		
		this.rssi = rssi;
		this.readerRssi = readerRssi;
		this.readerNameRssi = readerNameRssi;

		this.readerRssiSnr = readerRssiSnr;
		this.readerNameRssiSnr = readerNameRssiSnr;

		this.packetReader = packetReader;
		this.seqNum = seqNum;
		this.setPacketReceiveDate(packetReceiveDate);
		this.setKey(generateKey(tagId.intValue(), seqNum));
	}
	
	public static String generateKey(int tagId, int seqNum ) {
		return String.valueOf(tagId) + "_" + String.valueOf(seqNum);
	}
	
	public Long getReaderId() {
		return readerId;
	}
	public void setReaderId(Long readerId) {
		this.readerId = readerId;
	}
	public String getRssi() {
		return rssi;
	}
	public void setRssi(String rssi) {
		if (this.rssi != null && this.rssi.length() > 0) {
			this.rssi += ("," + rssi);
		} else {
			this.rssi = rssi;
		}
	}
	
	public IPacketReader getPacketReader() {
		return packetReader;
	}
	public void setPacketReader(IPacketReader packetReader) {
		this.packetReader = packetReader;
	}

	public Date getPacketReceiveDate() {
		return packetReceiveDate;
	}

	public void setPacketReceiveDate(Date packetReceiveDate) {
		this.packetReceiveDate = packetReceiveDate;
	}

	public int getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getReaderRssi() {
		return readerRssi;
	}

	public void setReaderRssi(String readerRssi) {
		if (this.readerRssi != null && this.readerRssi.length() > 0) {
			this.readerRssi += ("," + readerRssi);
		} else {
			this.readerRssi = readerRssi;
		}
	}
	
	public String getReaderNameRssi() {
		return readerNameRssi;
	}

	public void setReaderNameRssi(String readerNameRssi) {
		if (this.readerNameRssi != null && this.readerNameRssi.length() > 0) {
			this.readerNameRssi += ("," + readerNameRssi);
		} else {
			this.readerNameRssi = readerNameRssi;
		}
	}
	
	public String getReaderRssiSnr() {
		return readerRssiSnr;
	}

	public void setReaderRssiSnr(String readerRssiSnr) {
		if (this.readerRssiSnr != null && this.readerRssiSnr.length() > 0) {
			this.readerRssiSnr += (", " + readerRssiSnr);
		} else {
			this.readerRssiSnr = readerRssiSnr;
		}
	}

	public String getReaderNameRssiSnr() {
		return readerNameRssiSnr;
	}

	public void setReaderNameRssiSnr(String readerNameRssiSnr) {
		if (this.readerNameRssiSnr != null && this.readerNameRssiSnr.length() > 0) {
			this.readerNameRssiSnr += (", " + readerNameRssiSnr);
		} else {
			this.readerNameRssiSnr = readerNameRssiSnr;
		}
	}
	
	public String getReaderIdStr() {
		return readerIdStr;
	}

	public void setReaderIdStr(String readerIdStr) {
		if (this.readerIdStr != null && this.readerIdStr.length() > 0) {
			this.readerIdStr += ("," + readerIdStr);
		} else {
			this.readerIdStr = readerIdStr;
		}
	}
}
