package com.xmlasia.gemsx.device;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.AlertDao;
import com.xmlasia.gemsx.domain.Alert;

public class AlertStoreEndpoint extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	private AlertDao alertDao;
	
	
	@Override
    public void run() {
        while(!(Thread.currentThread().isInterrupted())) {
            try {
                final Alert newAlert = alertQueue.take();
                this.save(newAlert);

            } catch (InterruptedException e) {
                // Set interrupted flag.
                Thread.currentThread().interrupt();
            } catch (Exception e) {
            	log.error(this.getClass().toString() + ": "+ e.getMessage());
            }
        }

        // Thread is getting ready to die, but first,
        // drain remaining elements on the queue and process them.
        final List<Alert> remainingObjects = new ArrayList<Alert>();
        alertQueue.drainTo(remainingObjects);
       
        for(Alert newAlert : remainingObjects) {
            this.save(newAlert);
        }
    }

    private void save(final Alert newAlert) {
        // Do something with the complex object.
    	try {
    		alertDao.createAlert(newAlert);
    	} catch (Exception e) {
    		log.error(this.getClass().toString() + ": "+ e.getMessage());
    	}
    }
	
    private static BlockingQueue<Alert> alertQueue = new LinkedBlockingQueue<Alert>();
    
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}

	public AlertDao getAlertDao() {
		return alertDao;
	}

	public void setAlertDao(AlertDao alertDao) {
		this.alertDao = alertDao;
	}
	
	static void processAlert( Alert newAlert) {
		alertQueue.offer(newAlert);
	}
}
