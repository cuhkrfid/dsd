package com.xmlasia.gemsx.device;

import java.io.IOException;
import java.nio.ByteBuffer;

public class NioPacketReader implements IPacketReader {

	private ByteBuffer byteBuffer;
	
	public NioPacketReader(ByteBuffer byteBuffer) {
		this.byteBuffer = byteBuffer;
	}
	
	@Override
	public int read(byte[] packetData, boolean isSwappedByte) throws IOException {
        byteBuffer.get( packetData, 0, packetData.length );
        if ( isSwappedByte ) {
			byte[] swappedByte = byteSwap(packetData, packetData.length);
			System.arraycopy(swappedByte, 0, packetData, 0,swappedByte.length);
		}
		return packetData.length;
	}

	private byte[] byteSwap(byte[] b, int length) {
		byte[] swappedByte = new byte[length];
		for (int i = length-1; i >= 0; --i) {
			swappedByte[length-1-i] = b[i];
		}
		return swappedByte;
	}
}
