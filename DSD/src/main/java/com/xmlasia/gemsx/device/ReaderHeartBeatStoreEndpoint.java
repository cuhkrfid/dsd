package com.xmlasia.gemsx.device;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.ReaderHeartBeatDao;
import com.xmlasia.gemsx.dao.ReaderIdToNameMapDao;
import com.xmlasia.gemsx.domain.ReaderHeartBeat;
import com.xmlasia.gemsx.domain.ReaderIdToNameMap;

public class ReaderHeartBeatStoreEndpoint extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	private ReaderHeartBeatDao readerHeartBeatDao;
	private ReaderIdToNameMapDao readerIdToNameMapDao;
	
	@Override
    public void run() {
        
		while(!(Thread.currentThread().isInterrupted())) {
            try {
                final ReaderHeartBeat readerHeartBeat = readerHeartBeatQueue.take();
                this.save(readerHeartBeat);

            } catch (InterruptedException e) {
                // Set interrupted flag.
                Thread.currentThread().interrupt();
            } catch (Exception e) {
            	log.error(this.getClass().toString() + ": "+ e.getMessage());
            }
        }

        // Thread is getting ready to die, but first,
        // drain remaining elements on the queue and process them.
        final List<ReaderHeartBeat> remainingObjects = new ArrayList<ReaderHeartBeat>();
        readerHeartBeatQueue.drainTo(remainingObjects);
       
        for(ReaderHeartBeat readerHeartBeat : remainingObjects) {
            this.save(readerHeartBeat);
        }
    }

    private void save(final ReaderHeartBeat readerHeartBeat) {
        try {
    		readerHeartBeatDao.create(readerHeartBeat);
    		
   	    	ReaderIdToNameMap readerIdToNameMap = new ReaderIdToNameMap();
   	    	readerIdToNameMap.setReaderId(readerHeartBeat.getReaderId());
   	    	readerIdToNameMapDao.createReaderIdToNameMapIfNotFound(readerIdToNameMap);	    		
	    	
	    } catch (Exception e) {
    		log.error("save to db error: " + e.getMessage());
    	}
    }
	
    private static BlockingQueue<ReaderHeartBeat> readerHeartBeatQueue = new LinkedBlockingQueue<ReaderHeartBeat>();
    
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}

	public ReaderHeartBeatDao getReaderHeartBeatDao() {
		return readerHeartBeatDao;
	}

	public void setReaderHeartBeatDao(ReaderHeartBeatDao readerHeartBeatDao) {
		this.readerHeartBeatDao = readerHeartBeatDao;
	}

	static void saveToDb( ReaderHeartBeat readerHeartBeat) {
		
		readerHeartBeatQueue.offer(readerHeartBeat);
	}

	public ReaderIdToNameMapDao getReaderIdToNameMapDao() {
		return readerIdToNameMapDao;
	}

	public void setReaderIdToNameMapDao(ReaderIdToNameMapDao readerIdToNameMapDao) {
		this.readerIdToNameMapDao = readerIdToNameMapDao;
	}
	
	
}
