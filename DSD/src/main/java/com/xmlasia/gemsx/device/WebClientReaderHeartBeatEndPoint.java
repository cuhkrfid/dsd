package com.xmlasia.gemsx.device;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.inject.Singleton;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.JsonObject;
import com.xmlasia.gemsx.domain.ReaderHeartBeat;

/**
 * ChatServer
 * @author Jiji_Sasidharan
 */
@ServerEndpoint(value="/readerHeartBeat")
@Singleton
public class WebClientReaderHeartBeatEndPoint {

	static class QueueConsumer extends Thread {
		
		Log log = LogFactory.getLog(this.getClass());
		private boolean isStarted = false; 
	
		@Override
	    public void run() {
			
	        while(!(Thread.currentThread().isInterrupted())) {
	            try {
	                final ReaderHeartBeat readerHeartBeat = readerHeartBeatQueue.take();
	                this.process(readerHeartBeat);
	                Thread.sleep(500);
	
	            } catch (InterruptedException e) {
	                // Set interrupted flag.
	                Thread.currentThread().interrupt();
	            } catch (Exception e) {
	            	log.error(this.getClass().toString() + ": "+ e.getMessage());
	            }
	        }
	
	        // Thread is getting ready to die, but first,
	        // drain remaining elements on the queue and process them.
	        final List<ReaderHeartBeat> remainingObjects = new ArrayList<ReaderHeartBeat>();
	        readerHeartBeatQueue.drainTo(remainingObjects);
	       
	        for(ReaderHeartBeat readerHeartBeat : remainingObjects) {
	            this.process(readerHeartBeat);
	        }
	    }
	
	    private void process(final ReaderHeartBeat readerHeartBeat) {
	        // Do something with the complex object.
	    	sendToWebClients(readerHeartBeat);
	    }
	    
	    public boolean getIsStarted() { return isStarted; }
	    public void setIsStarted(boolean started) { this.isStarted = started; }
	};
	
    static Set<Session> userSessions = Collections.synchronizedSet(new HashSet<Session>());
    static QueueConsumer queueConsumer = new WebClientReaderHeartBeatEndPoint.QueueConsumer();
    static BlockingQueue<ReaderHeartBeat> readerHeartBeatQueue = new LinkedBlockingQueue<ReaderHeartBeat>();
	
    /**
     * Callback hook for Connection open events. 
     * 
     * This method will be invoked when a client requests for a 
     * WebSocket connection.
     * 
     * @param userSession the userSession which is opened.
     */
    @OnOpen
    public void onOpen(Session userSession) {
    	if ( ! queueConsumer.getIsStarted() ) {
    		queueConsumer.setIsStarted(true);
    		queueConsumer.start();
    	}
    	
        userSessions.add(userSession);
    }
    
    /**
     * Callback hook for Connection close events.
     * 
     * This method will be invoked when a client closes a WebSocket
     * connection.
     * 
     * @param userSession the userSession which is opened.
     */
    @OnClose
    public void onClose(Session userSession) {
        userSessions.remove(userSession);
    }
    
    /**
     * Callback hook for Message Events.
     * 
     * This method will be invoked when a client send a message.
     * 
     * @param message The text message
     * @param userSession The session of the client
     */
    @OnMessage
    public void onMessage(String message, Session userSession) {
//        for (Session session : userSessions) {
//            session.getAsyncRemote().sendText(message);
//        }
    }
    
   
    
    private static void sendToWebClients(ReaderHeartBeat readerHeartBeat) {
    	
        JsonObject readerHeartBeatDataObject = new JsonObject();
	    readerHeartBeatDataObject.addProperty("msgType", "readerHeartBeat" );
    	
	    DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dateString = df.format(readerHeartBeat.getInputDate());
		readerHeartBeatDataObject.addProperty("dateString2", dateString);
		readerHeartBeatDataObject.addProperty("readerId", readerHeartBeat.getReaderId());
		readerHeartBeatDataObject.addProperty("readerName", readerHeartBeat.getReaderName());
		readerHeartBeatDataObject.addProperty("rssi", readerHeartBeat.getRssi());
		readerHeartBeatDataObject.addProperty("packetCounter", readerHeartBeat.getPacketCounter());			
		
		String message = readerHeartBeatDataObject.toString();       
    	for (Session session : userSessions) {
    		session.getAsyncRemote().sendText(message);
    	} 
    }
	
    public static void broadcastReaderHeartBeatToWebClients(ReaderHeartBeat readerHeartBeat) {
    	
    	if ( ! queueConsumer.getIsStarted() )
    		return;
    	
     	readerHeartBeatQueue.offer(readerHeartBeat);
    }

    public static void stop() {
    	queueConsumer.interrupt();
    }
    
}
