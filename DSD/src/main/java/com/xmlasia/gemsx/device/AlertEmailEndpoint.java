package com.xmlasia.gemsx.device;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;

import com.xmlasia.gemsx.domain.Alert;
import com.xmlasia.gemsx.domain.AlertSetting;

public class AlertEmailEndpoint extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	
	private String fromEmail; 
	private String fromPassword;
	private String hostSmtp;
	private int hostSmtpPort;
	
	@Override
    public void run() {
		
        while(!(Thread.currentThread().isInterrupted())) {
            try {
                final Alert newAlert = alertQueue.take();
                this.sendEmail(newAlert);

            } catch (InterruptedException e) {
                // Set interrupted flag.
                Thread.currentThread().interrupt();
            } catch (Exception e) {
            	log.error(this.getClass().toString() + ": "+ e.getMessage());
            }
        }

        // Thread is getting ready to die, but first,
        // drain remaining elements on the queue and process them.
        final List<Alert> remainingObjects = new ArrayList<Alert>();
        alertQueue.drainTo(remainingObjects);
    }

    private void sendEmail(final Alert newAlert) {
        // Do something with the complex object.
    	try {
			Email email = new SimpleEmail();
			email.setHostName(hostSmtp);
			email.setSmtpPort(hostSmtpPort);
			email.setAuthenticator(new DefaultAuthenticator(fromEmail, fromPassword));
			email.setSSLOnConnect(true);
			email.setFrom(fromEmail);

			AlertSetting alertSetting = newAlert.getAlertSetting();
			
			String subject = "DSD Environment Monitoring System [ALERT]: " + alertSetting.getName() + ", Level: " + alertSetting.getLevel();
			email.setSubject(subject);
			
			String alertMessage = "";
			alertMessage += String.format("Tag Configuration: %s %s", newAlert.getTagConfiguration().getName(), System.lineSeparator());
			alertMessage += String.format("Sensor Id: %d %s", newAlert.getSensorConfigId(), System.lineSeparator());
			alertMessage += String.format("Alert Setting Name: %s %s", alertSetting.getName(), System.lineSeparator());
			alertMessage += String.format("Alert Setting Level:%s %s", alertSetting.getLevel(), System.lineSeparator());
			alertMessage += String.format("Alert Trigger Time: %s %s", newAlert.getCreateDateTimeStr(), System.lineSeparator());
			alertMessage += Alert.createMessage(newAlert, System.lineSeparator());
			email.setMsg( alertMessage );
			
			String emails = newAlert.getAlertSetting().getEmails();
			if ( emails != null && emails.length() > 0 ) {
				String[] emailReceipts = emails.split(";");
				for (String emailReceipt : emailReceipts) {
					try {
						email.addTo(emailReceipt);
					} catch (Exception e) {
						
					}
				}
			}
			email.send();
    		
    	} catch (Exception e) {
    		log.error(this.getClass().toString() + ": "+ e.getMessage());
    	}
    }
	
    private static BlockingQueue<Alert> alertQueue = new LinkedBlockingQueue<Alert>();
    
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}

	static void processAlert( Alert newAlert) {
		if ( newAlert.getAlertSetting().getIsSendEmail() 
			&& newAlert.getAlertSetting().getEmails() != null
			&& newAlert.getAlertSetting().getEmails().length() > 0 ) {
			alertQueue.offer(newAlert);
		}
	}

	public String getFromPassword() {
		return fromPassword;
	}

	public void setFromPassword(String fromPassword) {
		this.fromPassword = fromPassword;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public String getHostSmtp() {
		return hostSmtp;
	}

	public void setHostSmtp(String hostSmtp) {
		this.hostSmtp = hostSmtp;
	}

	public int getHostSmtpPort() {
		return hostSmtpPort;
	}

	public void setHostSmtpPort(int hostSmtpPort) {
		this.hostSmtpPort = hostSmtpPort;
	}
	
}
