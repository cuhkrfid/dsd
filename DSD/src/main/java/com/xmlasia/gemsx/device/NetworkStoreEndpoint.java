package com.xmlasia.gemsx.device;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.xmlasia.gemsx.dao.NetworkDao;
import com.xmlasia.gemsx.domain.Network;

public class NetworkStoreEndpoint extends Thread {

	Log log = LogFactory.getLog(this.getClass());
	private NetworkDao networkDao;
	
	@Override
    public void run() {
	    //networkDao.test1();
        
		while(!(Thread.currentThread().isInterrupted())) {
            try {
                final Network network = networkQueue.take();
                this.save(network);

            } catch (InterruptedException e) {
                // Set interrupted flag.
                Thread.currentThread().interrupt();
            } catch (Exception e) {
            	log.error(this.getClass().toString() + ": "+ e.getMessage());
            }
        }

        // Thread is getting ready to die, but first,
        // drain remaining elements on the queue and process them.
        final List<Network> remainingObjects = new ArrayList<Network>();
        networkQueue.drainTo(remainingObjects);
       
        for(Network network : remainingObjects) {
            this.save(network);
        }
    }

    private void save(final Network network) {
        try {
    		networkDao.createNewOrUpdate(network);
	    } catch (Exception e) {
    		log.error("save to db error: " + e.getMessage());
    	}
    }
	
    private static BlockingQueue<Network> networkQueue = new LinkedBlockingQueue<Network>();
    
	public void init() throws Exception  {
		start();
	}
	
	public void destroy() {
		interrupt();
	}

	public NetworkDao getNetworkDao() {
		return networkDao;
	}

	public void setNetworkDao(NetworkDao networkDao) {
		this.networkDao = networkDao;
	}

	static void saveToDb( Network network) {
		networkQueue.offer(network);
	}
}