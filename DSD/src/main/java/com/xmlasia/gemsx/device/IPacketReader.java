package com.xmlasia.gemsx.device;

import java.io.IOException;

public interface IPacketReader {

	public int read( byte[] packetData, boolean isSwappedByte ) throws IOException;
}
