package com.xmlasia.gemsx.security;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.xmlasia.gemsx.dao.GroupDao;
import com.xmlasia.gemsx.domain.Group;
import com.xmlasia.gemsx.domain.User;
import com.xmlasia.gemsx.service.UserService;


public class LdapAndDaoAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter 
{
    private static final Logger log = Logger.getLogger(LdapAndDaoAuthenticationProcessingFilter.class);
       
	public final static String SPRING_SECURITY_FORM_DOMAIN_KEY = "j_domain";

	private String domainParameter = SPRING_SECURITY_FORM_DOMAIN_KEY;
	private UserService userService;
	private GroupDao groupDao;
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request,
			HttpServletResponse response) throws AuthenticationException {
		if (!"POST".equals(request.getMethod())) {  
	        throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());  
	    }  
	
	    String username = obtainUsername(request);  
	    String password = obtainPassword(request);  
	  
	    if (username == null) {  
	        username = "";  
	    }  
	  
	    if (password == null) {  
	        password = "";  
	    }  
	
	    username = username.trim();  

	    UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
	    setDetails(request, authRequest);  
	    
	    Authentication auth = null;
	    try{	  
	    	User user  = userService.findByLoginId(username);
	    	if ( user == null) {
	    		throw new AuthenticationServiceException("Unable to find username: " + username);  
	    	}
	    	
	    	List<Group> groups = groupDao.searchByCriteria(String.valueOf(user.getGroupId()), null, null, null);
	    	if ( groups == null || groups.size() == 0) {
	    		throw new AuthenticationServiceException("Unable to find group for username: " + username);  
	    	}
	    	if ( groups.get(0).getActive() == false) {
	    		throw new AuthenticationServiceException(String.format("Group %s for username: %s is inactive", groups.get(0).getName(), username));  
	    	}
	    	
	    	auth = this.getAuthenticationManager().authenticate(authRequest);
	     	
	    } catch (AuthenticationException ex){
	    	String msg = "Failed to authenticate user = " + username + ":";
	    	log.info(msg + ex.getMessage());
	    	throw ex;
	    }
	   	    	   	    
	    return auth;
	}    	
	
	protected String obtainDomain(HttpServletRequest request) {
        return request.getParameter(domainParameter);
    }

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GroupDao getGroupDao() {
		return groupDao;
	}

	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}


}
