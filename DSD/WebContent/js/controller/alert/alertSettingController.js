angular.module('dsd').controller('alertSettingViewCtrl', ['$scope', '$translate','$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', '$window',
 function ($scope, $translate, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, $window) {
	$scope.rowHeight = 60; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 4;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows);
	
	$scope.section="";
	$scope.users=[
	];
	$scope.roles=[
	];
	$scope.modules = [];
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.userTotalItems = 100;
	$scope.userCurrentPage = 1;
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
		  
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	
	$scope.getTableStyle= function() {
        return {
            height: (($scope.numberOfRows+1)* $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight ) + 30 +  "px"
        };
    };
	
	
	$scope.changeLanguage=function(locale) {
	};

    
    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });
 
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.persons = {currentPage : 1};

	//User Maintenance
	$scope.openAlertSetting = function (alertSettingId) {
		var path = 'SystemConfiguration/TagConfiguration/Edit/';
		path += (alertSettingId+"");
		$location.path(path);
	};
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
   			{field:'name', displayName: $translate.instant('ALERT_NAME'), width: 160,  headerCellClass: 'uiGridthemeHeaderColor',
	 			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><a>{{row.entity.name}}</a></div>'},
	 		{field:'level', displayName: $translate.instant('ALERT_LEVEL'), width: 90,  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><a>{{row.entity.level}}</a></div>'},
		 	{field:'fromDateStr',  width: 120, displayName: $translate.instant('START_DATE'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><a>{{row.entity.fromDateStr}}</a></div>'},
			{field:'toDateStr',  width: 120, displayName: $translate.instant('END_DATE'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><a>{{row.entity.toDateStr}}</a></div>'},							
			{field:'siteConfiguration.name',  width: 160, displayName: $translate.instant('SITE_NAME'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><a>{{row.entity.siteConfiguration.name}}</a></div>'},	
			{field:'coverLevelHtml',  width: 160, displayName: $translate.instant('COVER_LEVEL'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><div ng-repeat="item in row.entity.coverLevelHtml">{{item}}</div></div>'},							
			{field:'waterLevelHtml',  width: 160, displayName: $translate.instant('WATER_LEVEL'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><div ng-repeat="item in row.entity.waterLevelHtml">{{item}}</div></div>'},								
			{field:'h2sHtml',  width: 160, displayName: $translate.instant('H2S'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><div ng-repeat="item in row.entity.h2sHtml">{{item}}</div></div>'},					 		
			{field:'so2Html',  width: 160, displayName: $translate.instant('SO2'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><div ng-repeat="item in row.entity.so2Html">{{item}}</div><div>'},			
			{field:'co2Html',  width: 160, displayName: $translate.instant('CO2'), headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><div ng-repeat="item in row.entity.co2Html">{{item}}</div></div>'},	
			{field:'ch4Html',  width: 160, displayName: $translate.instant('CH4'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><div ng-repeat="item in row.entity.ch4Html">{{item}}</div></div>'},	
			{field:'nh3Html',  width: 160, displayName: $translate.instant('NH3'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><div ng-repeat="item in row.entity.nh3Html">{{item}}</div></div>'},	
			{field:'o2Html',  width: 160, displayName: $translate.instant('O2'), headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><div ng-repeat="item in row.entity.o2Html">{{item}}</div></div>'},	
			{field:'emailsHtml',  width: 100, displayName: $translate.instant('SEND_EMAIL'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div ng-repeat="item in row.entity.emailsHtml">{{item}}</div>'},
			{field:'repeatAlertInSecs',  width: 100, displayName: $translate.instant('REPEAT_ALERT_IN_SECS'), headerCellClass: 'uiGridthemeHeaderColor',
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertSetting(row.entity.id)"><a>{{row.entity.repeatAlertInSecs}}</a></div>'}	
			];
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.tagConfigurationTotalItems = 100;
	$scope.tagConfigurationCurrentPage = 1;
	$scope.tagConfigurations=[];
	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		useExternalFiltering: false,
		multiSelect: false,
		enableRowSelection: true,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		rowHeight: $scope.rowHeight,
		enableRowHeaderSelection: false,
		enableColumnResizing : true,
		paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
		paginationPageSize: $scope.numberOfRowsInt,	 
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {

		    });
		},
		columnDefs: $scope.columnDefs
		};
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.openAlertSetting = function (alertSettingId) {
		 $http({
	   		url: appMainService.getDomainAddress() + '/rest/alertSetting/getAlertSetting/',
	   		method: 'GET',
         params: {
             id: alertSettingId,
             name: null,
             level: null,
             useDateRange: null,
             fromDate: null,
             toDate: null,
             deviceAddress: null,
             deviceId : null,
             groupId : null,
             isSendEmail: null,
            
             isCoverLevel: null,
             isWaterLevel: null,
             isH2s: null,
             isSo2: null,
             isCo2: null,
             isCh4: null,
             isNh3: null,
             isO2: null
            }
		}).then(function (response) {
			if (response.data == null || response.data.length == 0 ) {
				return; 
			}
			var data = response.data[0];
			appMainService.setAlertSetting(data);
			var path = 'Alert/AlertSetting/New/';
			$location.path(path); 
				
	   	},function (response){
	    	alert("Failed to search data, status=" + response.status);
	   	});
	};
    
	 $scope.getAllAlertSetting = function () {
		 $http({
		   		url: appMainService.getDomainAddress() + '/rest/alertSetting/getAlertSetting/',
		   		method: 'GET',
	            params: {
	                id: null,
	                name: null,
	                level: null,
	                useDateRange: null,
	                fromDate: null,
	                toDate: null,
	                deviceAddress: null,
	                deviceId : null,
	                groupId : null,
	                isSendEmail: null,
	                
	                isCoverLevel: null,
	                isWaterLevel: null,
	                isH2s: null,
	                isSo2: null,
	                isCo2: null,
	                isCh4: null,
	                isNh3: null,
	                isO2: null
	               }
		 }).then(function (response) {
		 	$scope.gridOptions.data = response.data;
	   	   	},function (response){
		    	alert("Failed to search data, status=" + response.status);
	   	 });
	 };
	 $scope.getAllAlertSetting();
	 

	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
}]);


angular.module('dsd').controller('alertSettingCreateCtrl', ['$rootScope','$scope','$translate', '$interval', '$timeout','$routeParams', '$http', '$location' , 'uiGridConstants', 'appMainService', 'ngDialog',
                                                         function ($rootScope, $scope, $translate, $interval, $timeout, $routeParams, $http, $location, uiGridConstants, appMainService, ngDialog){
    
	
	$scope.setColumnDefsSensorGrid = function() {
		var columnDefs = [
  			{field:'delete', displayName: $translate.instant('DELETE'), width: 70,
  				cellTemplate: '<div class="ui-grid-cell-contents diagnosticSensorViewFont" ng-class="col.colIndex()" ng-click="grid.appScope.editAlertSettingSensorConfig(row.entity.tagConfiguration)"><img src="img/normal/delete.png" width="24" height="24" ng-click="grid.appScope.deleteAlertSettingSensorConfig(row.entity)"></img></div>'},
	 		{field:'id', displayName: $translate.instant('TAG_ID'), width : 150,
		    	cellTemplate: '<div class="ui-grid-cell-contents diagnosticSensorViewFont" ng-class="col.colIndex()" ng-click="grid.appScope.editAlertSettingSensorConfig(row.entity.tagConfiguration)"><a>{{row.entity.tagConfiguration.name}}</a></div>'},
		    {field:'sensorType', displayName: $translate.instant('SENSOR_TYPE'), width : 300, 
		    	cellTemplate: '<div class="ui-grid-cell-contents diagnosticSensorViewFont " ng-class="col.colIndex()" ng-click="grid.appScope.editAlertSettingSensorConfig(row.entity.tagConfiguration)"><a>{{row.entity.sensorType}}</a></div>'},
			{field:'config', displayName: $translate.instant('CONFIG'), enableFiltering : false,
				cellTemplate: '<div><button tooltip-trigger="mouseenter" tooltip-placement="bottom" tooltip-append-to-body="true" tooltip-popup-delay="0" uib-tooltip="{{\'TAG_CONFIGURATION\'|translate}}" class="mainContent-btn" ng-click="grid.appScope.openTagConfiguration(row.entity.tagConfiguration)" ng-if="grid.appScope.currentPermission[\'SYSTEM_CONFIGURATION_TAG_CONFIGURATION\']!=3"><img src="img/shortCut/tagConfiguration.png" width="24" height="24"/></button></div>'}
		    	
		];
		$scope.columnDefsSensor = columnDefs;
	};
	$scope.setColumnDefsSensorGrid();
	
	$scope.sensorGridOptions={
		//data: $scope.tagConfiguration.sensorConfigs,           
		rowHeight: 30,
		minRowsToShow: 5,
		enableFiltering: false,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		multiSelect: false,
		enableColumnResizing : true,
		onRegisterApi: function(gridApi){
	        $scope.sensorGridApi = gridApi;
	    },
		columnDefs: $scope.columnDefsSensor
	};
	
	$scope.getSensorTableStyle= function() {
		var length = $scope.sensorGridOptions.data.length; 
        return {
            height: (8 * $scope.sensorGridOptions.rowHeight  ) + "px"
        };
    };
    
    $scope.getSensorTypeHtml = function(value) {
		 
		var sensorTypeHtmlMsg = "";
		if ( value.isCoverLevel)
			sensorTypeHtmlMsg += "Cover level, ";
		if ( value.isWaterLevel)
			sensorTypeHtmlMsg += "Water level, ";
		if ( value.isH2s)
			sensorTypeHtmlMsg += "h2s, ";
		if ( value.isSo2)
			sensorTypeHtmlMsg += "so2, ";
		if ( value.isCo2)
			sensorTypeHtmlMsg += "co2, ";
		if ( value.isCh4)
			sensorTypeHtmlMsg += "ch4, ";
		if ( value.isNh3)
			sensorTypeHtmlMsg += "nh3, ";
		if ( value.isO2)
			sensorTypeHtmlMsg += "o2, ";
		
		sensorTypeHtmlMsg = sensorTypeHtmlMsg.substr(0,sensorTypeHtmlMsg.length-2);
		return sensorTypeHtmlMsg;
    };
    
    $scope.createAlertSettingOk = function () {
 		
    	var validationObject = new Object(); 
		if ($scope.alertSetting.id == 0) {
			validationObject.validationSubject = "New Alert Setting ";
		} else {
			validationObject.validationSubject = "Update Alert Setting ";
		}

    	if ($scope.alertSetting.alertSettingSensorConfigs.length == 0) {
    		validationObject.validationMessages = "Please add at least one tag configuration in the alert setting";
    		appMainService.displayMessageResult(validationObject, true);
    		return;    		
    	}
    	
		if (!$scope.alertSetting.useDateRange) {
			$scope.alertSetting.fromDate = null;
			$scope.alertSetting.toDate = null;
		}

		if (!$scope.alertSetting.isRepeatAlert) {
			$scope.alertSetting.repeatAlertInSecs = null;
		}
		
		if (!$scope.alertSetting.isSendEmail) {
			$scope.alertSetting.emails = null;
		}
		
    	var fd = new FormData();
    	
    	fd.append('name', $scope.alertSetting.name);
    	fd.append('level', $scope.alertSetting.level);
     	fd.append('useDateRange', $scope.alertSetting.useDateRange);
    	
     	if ($scope.alertSetting.useDateRange) {
     		fd.append('fromDate', $scope.alertSetting.fromDate.toString());
     		fd.append('toDate', $scope.alertSetting.toDate.toString());       
     	} else {
     		fd.append('fromDate', null);
     		fd.append('toDate', null);        		
     	}
     	
    	fd.append('deviceAddress', $scope.alertSetting.deviceAddress);
     	fd.append('deviceId', $scope.alertSetting.deviceId);
     	fd.append('siteId', $scope.alertSetting.site.id);
     	fd.append('groupId', $scope.alertSetting.groupId);
     
     	fd.append('useSiteId', $scope.alertSetting.useSiteId);
     	fd.append('useGroupId', $scope.alertSetting.useGroupId);
     	
     	fd.append('isSendEmail', $scope.alertSetting.isSendEmail);
      	fd.append('emails', $scope.alertSetting.emails);
        
     	fd.append('isRepeatAlert', $scope.alertSetting.isRepeatAlert);
      	fd.append('repeatAlertInSecs', $scope.alertSetting.repeatAlertInSecs);
      	fd.append('alertCriteriaId', $scope.alertSetting.alertCriteria.id);
    	fd.append('alertSettingSensorConfigs', $scope.generateAlertSettingSensorConfigsJson($scope.alertSetting.alertSettingSensorConfigs));
      	
      	if ( $scope.alertSetting.id == 0) {
	 		$http.post(appMainService.getDomainAddress() + '/rest/alertSetting/addAlertSetting', fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        }).then(function (response) {
	        	appMainService.displayMessageResult(response.data, true);
	        	$location.path('Alert/AlertSetting');
		    },function (response){
				  alert("Failed to add alert setting, status=" + response.status);
		    });
      	} else {
      		fd.append('id', $scope.alertSetting.id);
     		$http.post(appMainService.getDomainAddress() + '/rest/alertSetting/updateAlertSetting', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (response) {
            	appMainService.displayMessageResult(response.data, true);
            	$location.path('Alert/AlertSetting');
    	    },function (response){
    			  alert("Failed to add alert setting, status=" + response.status);
    	    });      		
      	}
	};

	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
	
	$scope.checkUseSiteId = function () {
		$scope.alertSetting.useGroupId = !$scope.alertSetting.useSiteId;
		if ( $scope.alertSetting.useGroupId == false) {
			$scope.alertSetting.groupId = "";
		}
		
		if ( $scope.alertSetting.useSiteId == false) {
			$scope.currentSensor = null;
			$scope.validateTagOnCriteria(null,null);
		}
	};
	
	
	$scope.checkUseGroupId = function () {
		$scope.alertSetting.useSiteId = !$scope.alertSetting.useGroupId;
		if ( $scope.alertSetting.useSiteId == false) {
			$scope.alertSetting.site = null;
			$scope.currentSensor = null;
			$scope.validateTagOnCriteria(null,null);
		}
	};
	
	$scope.removeAlertSetting = function (alertSettingId){
		$http({
            url: appMainService.getDomainAddress() + '/rest/alertSetting/deleteData/',
            method: 'DELETE',
            params: {
            	id: alertSettingId
            }
        }).then(function (response) {
        	appMainService.displayMessageResult(response.data, true);
      	    $location.path('Alert/AlertSetting');
        },function (response){
  		  alert("Failed to delete alert setting, status=" + response.status);
        });
	};
	
	$scope.setFromDate = function(date) {
		if ( $scope.alertSetting != null)
			$scope.alertSetting.fromDate = new Date(date).getTime().toString();
	};
	
	$scope.setToDate = function(date) {
		if ( $scope.alertSetting != null)
			$scope.alertSetting.toDate = new Date(date).getTime().toString();
	}
	
	
	$scope.getTagConfiguration = function(site, selectedAlertSettingSensorConfigs) {
    	$http({
    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
    		method: 'GET',
            params: {
            id: null, 
            name: null, 
            deviceId : null,
            deviceAddress : null,
            groupId : null,
            description: null,
            siteName : null,
            siteId : site.id,
           }
    	}).then(function (response) {
    		$scope.tagConfigurations = response.data;
   			scrollToMarkers( $scope.tagConfigurations, site);
   			var isFound = false;
   			
   			if ( selectedAlertSettingSensorConfigs != null && selectedAlertSettingSensorConfigs.length > 0) {
   				for (var j = 0; j < selectedAlertSettingSensorConfigs.length; ++j) {
					var tagConfiguration = selectedAlertSettingSensorConfigs[j].tagConfiguration;
					isFound |= selectMarker(tagConfiguration);
				}
   			}
   			if ( isFound == false ) {
   				$scope.alertSetting.alertSettingSensorConfigs = [];
   				$scope.sensorGridOptions.data = $scope.alertSetting.alertSettingSensorConfigs;		
   			}
   			
   		},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
    };
	
	$scope.getAllGroupId = function() {
		$http({  
			url: appMainService.getDomainAddress() + '/rest/alertSetting/getAllGroupId/',
			method: 'GET'
		}).then(function (response) {
			$scope.groupIds = response.data;
			for (var i = 0; i <  $scope.groupIds.length; ++i) {
				if ( $scope.groupIds[i] ==  $scope.alertSetting.groupId) {
					$scope.alertSetting.groupId = $scope.groupIds[i];
					break;
				}
			}
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});	
	};
	
	
	$scope.getAllAlertCriteria = function () {
		 $http({
		   		url: appMainService.getDomainAddress() + '/rest/alertCriteria/getAlertCriteria/',
		   		method: 'GET',
	            params: {
	                id: null,
	                name: null,
	                isCoverLevel: null,
	                isWaterLevel: null,
	                isH2s: null,
	                isSo2: null,
	                isCo2: null,
	                isCh4: null,
	                isNh3: null,
	                isO2: null
	            }
		 }).then(function (response) {
		 	$scope.alertCriterias = response.data;
			 	if ($scope.alertSetting.alertCriteria != null) {
			 		for (var i = 0; i < $scope.alertCriterias.length; ++i) {
			 			if ( $scope.alertCriterias[i].id == $scope.alertSetting.alertCriteria.id ) {
			 				$scope.alertSetting.alertCriteria = $scope.alertCriterias[i];
			 				$scope.validateTagOnCriteria($scope.currentSensor, $scope.alertSetting.alertCriteria);
			 				break;
			 			}
			 		}
			 	}
	   	   	},function (response){
		    	alert("Failed to search data, status=" + response.status);
	   	 });
	 };
	 
	 $scope.openAlertCriteria = function(alertCriteria) {
		 appMainService.setAlertCriteria(alertCriteria);
		 appMainService.setAlertSetting($scope.alertSetting);
		 appMainService.setStartDate($scope.alertSetting.fromDate);
		 appMainService.setEndDate($scope.alertSetting.toDate);
		 
		 
		 var path = 'Alert/AlertCriteria/New/';
  		 $location.path(path); 
	 };
	 
	 $scope.selectSite = function(site, alertSettingSensorConfigs) {
		 if (typeof site != "undefined") {
			 $scope.getTagConfiguration(site, alertSettingSensorConfigs);
		 } else {
			 $scope.currentSensor = null;
			 $scope.validateTagOnCriteria(null,null);
		 }
	 };
	 
	 $scope.onChangeCriteria = function(alertCriteria) {
		 $scope.validateTagOnCriteria($scope.currentSelectedConfiguration, alertCriteria);
	 }
	 
	 $scope.validateTagOnCriteria = function(currentSensor, alertCriteria) {
		 if (alertCriteria == null || currentSensor == null) {
			 $scope.coverLevelImagePath = "img/basic/question.png";
			 $scope.waterLevelImagePath = "img/basic/question.png";
			 $scope.h2sImagePath = "img/basic/question.png";
			 $scope.so2ImagePath = "img/basic/question.png";
			 $scope.co2ImagePath = "img/basic/question.png";
			 $scope.ch4ImagePath = "img/basic/question.png";
			 $scope.nh3ImagePath = "img/basic/question.png";
			 $scope.o2ImagePath = "img/basic/question.png";
			 
			 return;
		 }
		 
		 if (currentSensor.isCoverLevel != alertCriteria.isCoverLevel) {
			 $scope.coverLevelImagePath = "img/basic/Delete-48.png"; 
		 } else {
			 $scope.coverLevelImagePath = "img/basic/Checkmark-48.png"
		 }
		 
		 if (currentSensor.isWaterLevel != alertCriteria.isWaterLevel) {
			 $scope.waterLevelImagePath = "img/basic/Delete-48.png"; 
		 } else {
			 $scope.waterLevelImagePath = "img/basic/Checkmark-48.png"
		 }
		 
		 if (currentSensor.isH2s != alertCriteria.isH2s) {
			 $scope.h2sImagePath = "img/basic/Delete-48.png"; 
		 } else {
			 $scope.h2sImagePath = "img/basic/Checkmark-48.png"
		 }
		 
		 if (currentSensor.isSo2 != alertCriteria.isSo2) {
			 $scope.so2ImagePath = "img/basic/Delete-48.png"; 
		 } else {
			 $scope.so2ImagePath = "img/basic/Checkmark-48.png"
		 }
		 
		 if (currentSensor.isCo2 != alertCriteria.isCo2) {
			 $scope.co2ImagePath = "img/basic/Delete-48.png"; 
		 } else {
			 $scope.co2ImagePath = "img/basic/Checkmark-48.png"
		 }
		 
		 if (currentSensor.isCh4 != alertCriteria.isCh4) {
			 $scope.ch4ImagePath = "img/basic/Delete-48.png"; 
		 } else {
			 $scope.ch4ImagePath = "img/basic/Checkmark-48.png"
		 }
		 
		 if (currentSensor.isNh3 != alertCriteria.isNh3) {
			 $scope.nh3ImagePath = "img/basic/Delete-48.png"; 
		 } else {
			 $scope.nh3ImagePath = "img/basic/Checkmark-48.png"
		 }
		 
		 if (currentSensor.isO2 != alertCriteria.isO2) {
			 $scope.o2ImagePath = "img/basic/Delete-48.png"; 
		 } else {
			 $scope.o2ImagePath = "img/basic/Checkmark-48.png"
		 }	
	 };
	 
	 $scope.addOrUpdateAlertSettingSensorConfig = function(tagConfiguration) {
		 		 
		 if (tagConfiguration.sensorConfigs == null || tagConfiguration.sensorConfigs.length == 0)
			 return;
		 
		 var isAlertSettingSensorConfigExists = false; 
		 
		 for (var i = 0; i < $scope.alertSetting.alertSettingSensorConfigs.length; ++i) {
			 if ( $scope.alertSetting.alertSettingSensorConfigs[i].tagConfigurationId == tagConfiguration.id ) {
				 isAlertSettingSensorConfigExists = true;
				 break;
			 }
		 }
		 
		 if ( !isAlertSettingSensorConfigExists ) {
			 var alertSettingSensorConfig = new Object(); 
			 alertSettingSensorConfig.id = 0; 
			 
			 alertSettingSensorConfig.alertSettingId = $scope.alertSetting.id;
			 alertSettingSensorConfig.alertSetting = $scope.alertSetting;
			 
			 alertSettingSensorConfig.tagConfigurationId = tagConfiguration.id;
			 alertSettingSensorConfig.tagConfiguration = tagConfiguration;
			 
			 alertSettingSensorConfig.sensorType = $scope.getSensorTypeHtml(tagConfiguration.sensorConfigs[0]);
			 
			 $scope.alertSetting.alertSettingSensorConfigs.push(alertSettingSensorConfig);
			 $scope.sensorGridOptions.data = $scope.alertSetting.alertSettingSensorConfigs;		
			 selectMarker(tagConfiguration);
			 
		 } else {
			 for (var i = 0; i < $scope.sensorGridOptions.data.length; ++i) {
				 if ( $scope.sensorGridOptions.data[i].tagConfiguration.id == tagConfiguration.id) {
					if ($scope.currentSelectedConfiguration == null || $scope.currentSelectedConfiguration.id != tagConfiguration.id ) {
					 	$scope.sensorGridApi.selection.selectRow(i);
	   					$scope.sensorGridApi.selection.toggleRowSelection($scope.sensorGridOptions.data[i]);
	   					$scope.currentSelectedConfiguration = tagConfiguration;
	   					
	   					highlightMarker(tagConfiguration);
	   					$scope.filterAlertHistoryByTagConfiguration(tagConfiguration);
	   					
					} else {
						//$scope.clearCurrentSelection();
					}
	   				$scope.$apply();		 	   			
	 	   			break;
	   			}
	   		}
		 }
		 $scope.validateTagOnCriteria(tagConfiguration, $scope.alertSetting.alertCriteria);
	 };
	  
	 $scope.clearCurrentSelection = function() {
		 $scope.sensorGridApi.selection.clearSelectedRows()
		 $scope.currentSelectedConfiguration = null;
		
		 unSelectCurrentMarker();
		 $scope.gridOptionsAlertHistory.data = $scope.alertHistoryAll;
		 
		 $scope.validateTagOnCriteria($scope.currentSelectedConfiguration, $scope.alertSetting.alertCriteria);
	 };
	 
	 $scope.filterAlertHistoryByTagConfiguration = function(tagConfiguration) {
		 $scope.gridOptionsAlertHistory.data = [];
		 for (var i=0; i<$scope.alertHistoryAll.length; ++i) {
			 var alert = $scope.alertHistoryAll[i];
			 if (alert.tagConfigurationId == tagConfiguration.id) {
				 $scope.gridOptionsAlertHistory.data.push(alert);
			 }
		 }
	 };
	 
	 $scope.editAlertSettingSensorConfig = function(tagConfiguration) {
		 $scope.currentSelectedConfiguration = tagConfiguration;
		 highlightMarker(tagConfiguration);
		 $scope.filterAlertHistoryByTagConfiguration(tagConfiguration);
		 $scope.validateTagOnCriteria(tagConfiguration, $scope.alertSetting.alertCriteria);
	 };
	 
	 $scope.deleteAlertSettingSensorConfig = function (alertSettingSensorConfig) {
		 for (var i = 0; i < $scope.alertSetting.alertSettingSensorConfigs.length; ++i) {
			 if ( $scope.alertSetting.alertSettingSensorConfigs[i] == alertSettingSensorConfig) {
				 if (  $scope.alertSetting.alertSettingSensorConfigs[i].tagConfigurationId == $scope.currentSelectedConfiguration.id) {
					 $scope.clearCurrentSelection();
				 }
				 
				 $scope.alertSetting.alertSettingSensorConfigs.splice(i,1);
				 deleteMarker(alertSettingSensorConfig.tagConfiguration);
				 break;
			 }
		 }
		 $scope.validateTagOnCriteria($scope.currentSelectedConfiguration, $scope.alertSetting.alertCriteria);
	 };
	 
	$scope.generateAlertSettingSensorConfigsJson = function (alertSettingSensorConfigs) {
		var alertSettingSensorConfigsJson = '{"alertSettingSensorConfigs":[';
		
		for (var i = 0; i < alertSettingSensorConfigs.length; ++i) {
 			
			var id = '"id":"' + alertSettingSensorConfigs[i].id + '"'; 
 			var alertSettingId = '"alertSettingId":"' + alertSettingSensorConfigs[i].alertSettingId + '"'; 
			var tagConfigurationId = '"tagConfigurationId":"' + alertSettingSensorConfigs[i].tagConfigurationId + '"'; 
			
			var alertSettingSensorConfig = '{' + id + ',' + alertSettingId + ',' + tagConfigurationId + '}';

			if ( i != alertSettingSensorConfigs.length - 1 ) {
				alertSettingSensorConfig += ','
			}
			alertSettingSensorConfigsJson += alertSettingSensorConfig;
		}
		alertSettingSensorConfigsJson += ']}';
		return alertSettingSensorConfigsJson;
	};

	$scope.currentSelectedConfiguration = null;
	$scope.subName = "NEW_RECORD";
	$scope.tagConfigurations = [];
	$scope.alertSetting = appMainService.getAlertSetting();
	if ($scope.alertSetting != null) {
		if ($scope.alertSetting.id > 0) {
			$scope.subName = "EDIT_RECORD";
			for ( var i = 0; i < $scope.alertSetting.alertSettingSensorConfigs.length; ++i) {
				var tagConfiguration = $scope.alertSetting.alertSettingSensorConfigs[i].tagConfiguration;
		
				$scope.alertSetting.alertSettingSensorConfigs[i].sensorType = $scope.getSensorTypeHtml(tagConfiguration.sensorConfigs[0]);
			}
			$scope.sensorGridOptions.data = $scope.alertSetting.alertSettingSensorConfigs;	
		} else {
			$scope.alertSetting.fromDate = appMainService.getStartDate();
			$scope.alertSetting.toDate = appMainService.getEndDate();
		}
	} else {
		$scope.alertSetting = new Object();
		$scope.alertSetting.id = 0;
		$scope.alertSetting.name = "";
		$scope.alertSetting.level = "";
		$scope.alertSetting.useDateRange = false;
	
		var time =  new Date().getTime();
		$scope.alertSetting.fromDate = appMainService.getStartDate();
		$scope.alertSetting.toDate = appMainService.getEndDate();
		
		$scope.alertSetting.deviceAddress = '';
		$scope.alertSetting.deviceId = '';
		$scope.alertSetting.site = null;
		$scope.alertSetting.useSiteId = true;
		$scope.alertSetting.siteId = 0;
		
		$scope.alertSetting.groupId = ""; 
		$scope.alertSetting.useGroupId = false;
		
		$scope.alertSetting.isSendEmail = false;
		$scope.alertSetting.emails = "";

	    $scope.alertSetting.isRepeatAlert = false;
	    $scope.alertSetting.repeatAlertInSecs = 300;

		$scope.alertSetting.alertCriteria = null;
		$scope.alertSetting.alertCriteriaId = 0;
		$scope.alertSetting.alertSettingSensorConfigs = [];
		
	}

	$scope.alertLevels = ['Critical','High','Medium','Low'], 
	
	$scope.comparator = [">", ">=", "==", "<=", '<'];
    
	var tagConfiguration = appMainService.getTagConfiguration();
	if ( tagConfiguration != null) {
		$scope.alertSetting.deviceAddress = tagConfiguration.deviceAddress;
		$scope.alertSetting.deviceId = tagConfiguration.deviceId;
		$scope.alertSetting.groupId = tagConfiguration.groupId;
	}
	
	$scope.coverLevelImagePath = "img/basic/question.png";
	$scope.waterLevelImagePath = "img/basic/question.png";
	$scope.h2sImagePath = "img/basic/question.png";
	$scope.so2ImagePath = "img/basic/question.png";
	$scope.co2ImagePath = "img/basic/question.png";
	$scope.ch4ImagePath = "img/basic/question.png";
	$scope.nh3ImagePath = "img/basic/question.png";
	$scope.o2ImagePath = "img/basic/question.png";
	 
	$scope.getAllGroupId();
	$scope.getAllAlertCriteria();
	 
	$scope.$on('updateLanguageInUiGrid', function() {
		$scope.setColumnDefsSensorGrid();  
		$scope.sensorGridOptions.columnDefs = $scope.columnDefsSensor;
		
		$scope.setColumnDefsAlertHistory();
		$scope.gridOptionsAlertHistory.columnDefs = $scope.columnDefsAlertHistory;
	});
	
	$scope.historyLevels = ['Last 24 hours','Last 7 days','Last month','Last 3 months'], 
	$scope.currentHistorySelection = $scope.historyLevels[0];
	$scope.realTime = true;
	
	$scope.setColumnDefsAlertHistory = function() {
		var columnDefsAlertHistory = [
  			{field:'info', width: 30, displayName: '',	enableFiltering : false,	 
				cellTemplate: '<img ng-src="{{grid.appScope.getIcon(row.entity)}}" width="24" height="24"></img>'},
		    {field:'createDateTimeStr',  width: 140, displayName: $translate.instant('DATE'),  enableFiltering : false,
//					  sort: {
//						  direction: uiGridConstants.DESC,
//						  priority: 0,
//					  },
			  cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.createDateTimeStr}}</a></div>'},
		    {field:'alertSettingLevel',  width: 120, displayName: $translate.instant('ALERT_LEVEL'), 
			       cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.alertSettingLevel}}</a></div>'},
		    {field:'alertSettingName',  width: 160, displayName: $translate.instant('ALERT_NAME'), 
			       cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.alertSettingName}}</a></div>'},
			{field:'tagConfigurationName',  width: 180, displayName: $translate.instant('TAG_CONFIGURATION'),  enableFiltering : true,
			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.navigateToMarker(row.entity)"><a>{{row.entity.tagConfigurationName}}</a></div>'},
			{field:'coverLevel',  width: 110, displayName: $translate.instant('COVER_LEVEL'), 
			 	cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
				 	if (row.entity.isCoverLevel == true) {
				 		return 'redCellColor';
				 	}
			 	},
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.coverLevel}}</a></div>'},
			
		 	{field:'waterLevel',  width: 115, displayName: $translate.instant('WATER_LEVEL'), 
				 cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
				 	if (row.entity.isWaterLevel == true) {
				 		return 'redCellColor';
				 	}
				},			 		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.waterLevel}}</a></div>'},
			
		 	{field:'h2s',  width: 80, displayName: $translate.instant('H2S'), 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isH2s == true) {
				 		return 'redCellColor';
				 	}
				},
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.h2s}}</a></div>'},
			
			{field:'so2',  width: 80, displayName: $translate.instant('SO2'), 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isSo2 == true) {
				 		return 'redCellColor';
				 	}
				},			 		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.so2}}</a></div>'},
		 	
			{field:'co2',  width: 60, displayName: $translate.instant('CO2'), 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isCo2 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.co2}}</a></div>'},		
			 	
			{field:'ch4',  width: 60, displayName: $translate.instant('CH4'), 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isCh4 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.ch4}}</a></div>'},	
			{field:'nh3',  width: 70, displayName: $translate.instant('NH3'), 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isNh3 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.nh3}}</a></div>'},	
			{field:'o2',  width: 60, displayName: $translate.instant('O2'), 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isO2 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.o2}}</a></div>'},	
		 ];	
		 $scope.columnDefsAlertHistory = columnDefsAlertHistory;
	};
	$scope.setColumnDefsAlertHistory();
	
	$scope.gridOptionsAlertHistory={
		minRowsToShow: 30,
		enableFiltering: true,
		multiSelect: false,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: true,
		enableVerticalScrollbar: 0,
		rowHeight: 30,
		enableColumnResizing : true,
		exporterMenuPdf : false,
		enableGridMenu: true,
	    enableSelectAll: true,
	    exporterCsvFilename: 'alertHistoryViewFile.csv',
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	
	    
		onRegisterApi: function(gridApi){
			$scope.alertHistoryGridApi = gridApi;
			$scope.alertHistoryGridApi.core.on.filterChanged( $scope, function() {
				var grid = this.grid;
				
				var level = grid.columns.filter(function (item){
					return item.field == "level";
				})[0].filters[0].term;
				
				var name = grid.columns.filter(function (item){
					return item.field == "name";
				})[0].filters[0].term;
				
				var tagId = grid.columns.filter(function (item){
					return item.field == "tagId";
				})[0].filters[0].term;
					
				var sensorId = grid.columns.filter(function (item){
					return item.field == "sensorId";
				})[0].filters[0].term;
				
		    });
			$scope.alertHistoryGridApi.core.notifyDataChange( uiGridConstants.dataChange.OPTIONS);
		},
		columnDefs: $scope.columnDefsAlertHistory
	};
	
    $scope.startPage = function() {
		$http({
			url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
			method: 'GET',
	        params: {
	        id: null, 
	        name: null, 
	        description: null,
	        defaultSite: null
	       }
		}).then(function (response) {
			$scope.siteConfigurations = response.data;
			if ( $scope.siteConfigurations.length > 0 ) {
				for (var i = 0; i < $scope.siteConfigurations.length; ++i) {
					if ( $scope.alertSetting.siteId == $scope.siteConfigurations[i].id ) {
						$scope.alertSetting.site = $scope.siteConfigurations[i];
						$scope.selectSite($scope.alertSetting.site, $scope.alertSetting.alertSettingSensorConfigs);
						break;
					}
				}
			}
		},function (response){
	 		alert("Failed to find site configuration list, status=" + response.status);
		});
    };
    
	$scope.getIcon = function (alert) {
		if (alert.alertSettingLevel == "Critical") {
			return "img/basic/criticalalert2.png";
		}
		if (alert.alertSettingLevel == "High") {
			return "img/basic/highalert2.png";
		}
		if (alert.alertSettingLevel == "Medium") {
			return "img/basic/mediumalert2.png";
		}
		return "img/basic/lowalert2.png";
		
	};
	
	$scope.alertHistoryAll = [];
	$scope.searchAlertData = function(startDate, endDate, alertSettingId ) {
		$scope.searching = true;
		$http({  
			url: appMainService.getDomainAddress() + '/rest/alert/getAlert/',
			method: 'GET',
	        params: {
	        	fromDate: startDate,
	        	toDate: endDate,
		        level: "All",
		        alertSettingId: alertSettingId,
		        desc: true
	       }
		}).then(function (response) {
			if (response.data.length > 0) {
				$scope.gridOptionsAlertHistory.data = response.data;
				$scope.alertHistoryAll = response.data;
			}
			$scope.searching = false;
		},function (response){
			$scope.searching = false;
	 		alert("Failed to search data, status=" + response.status);
		});	
	};
	
    $scope.onChangeHistory = function (currentHistorySelection) {
     	var currentDate = new Date();
    	if ( currentHistorySelection == $scope.historyLevels[0] ) {
     		var currentTime = currentDate.getTime();
    		var fromTime = currentTime - (24*60*60*1000);
    		$scope.searchAlertData(fromTime, currentTime, $scope.alertSetting.id);
    	} 
    	if ( currentHistorySelection == $scope.historyLevels[1] ) {
     		var currentTime = currentDate.getTime();
    		var fromTime = currentTime - (7*24*60*60*1000);
    		$scope.searchAlertData(fromTime, currentTime, $scope.alertSetting.id);
    	}
    	if ( currentHistorySelection == $scope.historyLevels[2] ) {
     		var currentTime = currentDate.getTime();
    		var fromTime = currentTime - (31*24*60*60*1000);
    		$scope.searchAlertData(fromTime, currentTime, $scope.alertSetting.id);
    	}
    	if ( currentHistorySelection == $scope.historyLevels[3] ) {
     		var currentTime = currentDate.getTime();
    		var fromTime = currentTime - (3*31*24*60*60*1000);
    		$scope.searchAlertData(fromTime, currentTime, $scope.alertSetting.id);
    	}
    	
    };
    $scope.onChangeHistory($scope.currentHistorySelection);
    
	$scope.openAlert = function(alert) {
		
		var path = 'view/alert/alertDialogView.html'; 
	
		$scope.formData = new FormData();
		$scope.formData.alert = alert;
		
		ngDialog.openConfirm({template: path,
			scope: $scope //Pass the scope object if you need to access in the template
		}).then(
			//Update
			function(value) {
				if ( value == true) {
					appMainService.setDashBoardSelectedTagConfigurationId($scope.formData.alert.tagConfigurationId);
					$location.path("/DashBoard");
					return;
				} else {
					$scope.openAlertSetting($scope.formData.alert.alertSettingId);
				}
			},
			//Cancel
			function(value) {
			}		
		);				 		
	};
	
	$scope.navigateToMarker = function(alert) {
		$scope.editAlertSettingSensorConfig(alert.tagConfiguration);
		$scope.addOrUpdateAlertSettingSensorConfig(alert.tagConfiguration);
	};
	
	$scope.toggleRealTime = function() {
		$scope.realTime = !$scope.realTime;
	};

	$scope.$on('receiveAlert', function(event, alert) {
		
		if ( alert.alertSettingId == $scope.alertSetting.id) {
    		if ($scope.currentSelectedConfiguration != null ) {
    			$scope.alertHistoryAll.unshift(alert);
    			if( $scope.currentSelectedConfiguration.id == alert.tagConfigurationId) {
    				$scope.gridOptionsAlertHistory.data.unshift(alert);
    			}
    		} else {
    			$scope.gridOptionsAlertHistory.data.unshift(alert);
    		}
    	} 
	});
	$rootScope.connectWebSocketData();
	
	$scope.openTagConfiguration = function (tagConfiguration) {
		appMainService.setTagConfiguration(tagConfiguration);
		appMainService.setAlertSetting($scope.alertSetting);
		var path = 'SystemConfiguration/TagConfiguration/New/';
		$location.path(path);		
	};
 }]);


