angular.module('dsd').controller('alertGroupViewCtrl', ['$rootScope','$scope', '$translate','$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants',  'appMainService', 'ngDialog', '$window',
 function ($rootScope, $scope,$translate,$uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, ngDialog, $window) {
	$scope.selectedSite = null;
	$scope.siteConfigurations = [];
	$http({
		url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
		method: 'GET',
        params: {
        id: null, 
        name: null, 
        description: null,
        defaultSite: null
       }
	}).then(function (response) {
		$scope.siteConfigurations = response.data;
	},function (response){
 		alert("Failed to find site configuration list, status=" + response.status);
	});
	
	$scope.rowHeight = 30; 
	$scope.numberOfRows = ($window.innerHeight - 650) / $scope.rowHeight - 1;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows) - 1;
	
	$scope.alertLevels = ["All", "Critical", "High", "Medium", "Low"];
	$scope.alertLevel = "All";	
		
	$scope.searching = false;
	
	$scope.startDate = appMainService.getStartDate();
	$scope.endDate = appMainService.getEndDate();
	
	$scope.realTime = false;	
	$scope.alertIcon = "criticalalert.png";
	$scope.tagConfigurations=[];
	
	$scope.allTagConfiguration = new Object();
	$scope.allTagConfiguration.id = 0;
	$scope.allTagConfiguration.name = "All";
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
  			{field:'info', width: 30, displayName: '',	enableFiltering : false, headerCellClass: 'uiGridthemeHeaderColor',	 
				cellTemplate: '<img ng-src="{{grid.appScope.getIcon(row.entity)}}" width="24" height="24"></img>'},
		    {field:'createDateTimeStr',  width: 140, displayName: $translate.instant('DATE'),  enableFiltering : false, headerCellClass: 'uiGridthemeHeaderColor',
//					  sort: {
//						  direction: uiGridConstants.DESC,
//						  priority: 0,
//					  },
			  cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.createDateTimeStr}}</a></div>'},
		    {field:'alertSettingLevel',  width: 110, displayName: $translate.instant('ALERT_LEVEL'),  headerCellClass: 'uiGridthemeHeaderColor',
			       cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.alertSettingLevel}}</a></div>'},
		    {field:'alertSettingName',  width: 150, displayName: $translate.instant('ALERT_NAME'),   headerCellClass: 'uiGridthemeHeaderColor',
			       cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.alertSettingName}}</a></div>'},
			{field:'tagConfigurationName',  width: 160, displayName: $translate.instant('TAG_CONFIGURATION'),  headerCellClass: 'uiGridthemeHeaderColor',
				   cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.showTagGroupConfiguration(row.entity)"><a>{{row.entity.tagConfigurationName}}</a></div>'},
			{field:'deviceId',  width: 90, displayName: $translate.instant('DEVICE_ID'),  enableFiltering : true,  headerCellClass: 'uiGridthemeHeaderColor',
			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.deviceId}}</a></div>'},
			{field:'coverDetect',  width: 160, displayName: $translate.instant('COVER_DETECT'),  headerCellClass: 'uiGridthemeHeaderColor',
				 	cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					 	if (row.entity.isCoverDetect == true) {
					 		return 'redCellColor';
					 	}
				 	},
				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.coverDetect}}</a></div>'},
			{field:'coverLevel',  width: 110, displayName: $translate.instant('COVER_LEVEL'),  headerCellClass: 'uiGridthemeHeaderColor',
			 	cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
				 	if (row.entity.isCoverLevel == true) {
				 		return 'redCellColor';
				 	}
			 	},
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.coverLevel}}</a></div>'},
			
		 	{field:'waterLevel',  width: 120, displayName: $translate.instant('WATER_LEVEL'),  headerCellClass: 'uiGridthemeHeaderColor',
				 cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
				 	if (row.entity.isWaterLevel == true) {
				 		return 'redCellColor';
				 	}
				},			 		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.waterLevel}}</a></div>'},
			
		 	{field:'h2s',  width: 80, displayName: $translate.instant('H2S'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isH2s == true) {
				 		return 'redCellColor';
				 	}
				},
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.h2s}}</a></div>'},
			
			{field:'so2',  width: 80, displayName: $translate.instant('SO2'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isSo2 == true) {
				 		return 'redCellColor';
				 	}
				},			 		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.so2}}</a></div>'},
		 	
			{field:'co2',  width: 60, displayName: $translate.instant('CO2'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isCo2 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.co2}}</a></div>'},		
			 	
			{field:'ch4',  width: 60, displayName: $translate.instant('CH4'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isCh4 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.ch4}}</a></div>'},	
			{field:'nh3',  width: 70, displayName: $translate.instant('NH3'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isNh3 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.nh3}}</a></div>'},	
			{field:'o2',  width: 60, displayName: $translate.instant('O2'),  headerCellClass: 'uiGridthemeHeaderColor',
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isO2 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.o2}}</a></div>'},	
			{field:'details', displayName: $translate.instant('DETAILS'), enableFiltering : false, headerCellClass: 'uiGridthemeHeaderColor',
					cellTemplate: '<button tooltip-trigger="mouseenter" tooltip-placement="bottom" tooltip-append-to-body="true" tooltip-popup-delay="0" uib-tooltip="{{\'ALERT_RULE_SETTING\'|translate}}" class="mainContent-btn" ng-click="grid.appScope.openAlertSetting(row.entity.alertSettingId)"><img src="img/shortCut/alertRuleSettings.png" width="24" height="24"/></button>'},
						
			 		
		 ];	
		 $scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.getTableStyle= function() {
        return {
            height: (($scope.numberOfRows +  1)* $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight ) + 30 + "px"
        };
    };
    
	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: false,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		rowHeight: $scope.rowHeight,
		enableColumnResizing : true,
		exporterMenuPdf : false,
		enableGridMenu: true,
	    enableSelectAll: true,
	    exporterCsvFilename: 'alertHistoryViewFile.csv',
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,
	    onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {
				var grid = this.grid;
				
				var level = grid.columns.filter(function (item){
					return item.field == "level";
				})[0].filters[0].term;
				
				var name = grid.columns.filter(function (item){
					return item.field == "name";
				})[0].filters[0].term;
				
				var tagId = grid.columns.filter(function (item){
					return item.field == "tagId";
				})[0].filters[0].term;
					
				var sensorId = grid.columns.filter(function (item){
					return item.field == "sensorId";
				})[0].filters[0].term;
				
		    });
			$scope.tagConfigurationGridApi.core.notifyDataChange( uiGridConstants.dataChange.OPTIONS);
		},
		columnDefs: $scope.columnDefs
	};
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.getIcon = function (alert) {
		if (alert.alertSettingLevel == "Critical") {
			return "img/basic/criticalalert2.png";
		}
		if (alert.alertSettingLevel == "High") {
			return "img/basic/highalert2.png";
		}
		if (alert.alertSettingLevel == "Medium") {
			return "img/basic/mediumalert2.png";
		}
		return "img/basic/lowalert2.png";
		
	};
	
	$scope.onAlertLocation = function() {
		$rootScope.onAlertLocation();
	};
	
	$scope.openAlert = function(alert) {
		
		var path = 'view/alert/alertDialogView.html'; 
	
		$scope.formData = new FormData();
		$scope.formData.alert = alert;
		
		ngDialog.openConfirm({template: path,
			scope: $scope
		}).then(
			//Update
			function(value) {
				if ( value == true) {
					appMainService.setDashBoardSelectedTagConfigurationId($scope.formData.alert.tagConfigurationId);
					$location.path("/DashBoard");
					return;
				} else {
					$scope.openAlertSetting($scope.formData.alert.alertSettingId);
				}
			},
			//Cancel
			function(value) {
			}		
		);				 		
	};
	
	$scope.openAlertSetting = function(alertSettingId) {
		$http({
	   		url: appMainService.getDomainAddress() + '/rest/alertSetting/getAlertSetting/',
	   		method: 'GET',
         params: {
             id: alertSettingId,
             name: null,
             level: null,
             useDateRange: null,
             fromDate: null,
             toDate: null,
             deviceAddress: null,
             deviceId : null,
             groupId : null,
             isSendEmail: null,
             isCoverLevel: null,
             isWaterLevel: null,
             isH2s: null,
             isSo2: null,
             isCo2: null,
             isCh4: null,
             isNh3: null,
             isO2: null
            }
		}).then(function (response) {
			if (response.data == null || response.data.length == 0 ) {
				return; 
			}
			var data = response.data[0];
			
			appMainService.setAlertSettingHistoryData($scope.gridOptions.data);
			appMainService.setAlertSetting(data);
			appMainService.setStartDate($scope.startDate);
			appMainService.setEndDate($scope.endDate);
			 
			var path = 'Alert/AlertSetting/New/';
			$location.path(path); 
				
	   	},function (response){
	    	alert("Failed to search data, status=" + response.status);
	   	});
	}
	
	$scope.setFromDate = function(date) {
		$scope.startDate = new Date(date).getTime().toString();
	};
	
	$scope.setToDate = function(date) {
		$scope.endDate = new Date(date).getTime().toString();
	}

	$scope.toggleRealTime = function() {
	};
	
	$scope.$on('receiveAlert', function(event, alertData) {
		if ($scope.realTime == false) {
			return;
		}
		alertData.isRead = false;
    	if ($scope.selectedSite != null  && $scope.selectedSite.id == alertData.siteConfigurationId) {
    		$scope.gridOptions.data.unshift(alertData);
    	} 
    	
	});
	

	$scope.userCancel = function () {
		window.history.back();
	};
	
	//$scope.gridOptions.data = appMainService.getAlertSettingHistoryData();
	$rootScope.connectWebSocketData();
	

	$scope.mouseOverAlert = null;
	$scope.setMouseOverAlert = function(alertObject) {
		$scope.mouseOverAlert = alertObject;
	};
	
	$scope.getMouseOverAlert = function() {
		var mouseOverAlert = $scope.mouseOverAlert;
		 $scope.mouseOverAlert = null;
		return mouseOverAlert;
	};	
	 
	$scope.streetAlertMap = [];
	$scope.street = [];
	$scope.searchAlertData = function(siteConfiguration) {
		
		$scope.realTime = false;
		$scope.searching = true;
		$scope.isLoading = true;
		
		
		$http({  
			url: appMainService.getDomainAddress() + '/rest/alert/getAlert/',
			method: 'GET',
	        params: {
	        	fromDate: $scope.startDate.toString(),
	        	toDate: $scope.endDate.toString(),
		        level: null,
		        alertSettingId: null,
		        tagConfigurationId: null,
		        siteConfigurationId: siteConfiguration.id,
		        desc: true
	       }
		}).then(function (response) {
			$scope.isLoading = false;
			$scope.realTime = true;
			$scope.gridOptions.data = response.data;
			$scope.searching = false;
		},function (response){
			$scope.searching = false;
			$scope.isLoading = false;
			$scope.loadingMessage = $translate.instant("Failed to search data, status=" + response.status);
		});	
	};
	
	 $scope.getTagConfiguration = function(siteConfiguration) {
		clearAllPolyLines();
		 
   		$http({
    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
    		method: 'GET',
            params: {
            id: null, 
            name: null, 
            deviceId : null,
            deviceAddress : null,
            groupId : null,
            description: null,
            siteName : null,
            siteId : siteConfiguration.id,
           }
    	}).then(function (response) {
    		$scope.tagConfigurations = response.data;
   			scrollToMarkers( $scope.tagConfigurations, siteConfiguration);
   			$scope.searchGroupTagConfiguration(siteConfiguration);
   		},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
	 };
	    

	$scope.searchGroupTagConfiguration = function(selectedSite) {

		$http({
    		url: appMainService.getDomainAddress() + '/rest/groupTagConfiguration/searchData/',
    		method: 'GET',
    		params: {
    			id : null,
    			//fromDate : startDate.getTime().toString(),
    			//toDate : date.getTime().toString(),
	        	fromDate: $scope.startDate.toString(),
	        	toDate: $scope.endDate.toString(),
    			siteConfigurationId : selectedSite.id,
    		}
        }).then(function (response) {
        	var groupTagConfigurations = response.data;
        	for ( var i = 0; i < groupTagConfigurations.length; ++i) {
        		var groupTagConfiguration = groupTagConfigurations[i];
        		for (var j = 0; j < groupTagConfiguration.tagConfigurations.length; ++j ) {
        			var tagConfiguration =  groupTagConfiguration.tagConfigurations[j];
        			selectMarker(tagConfiguration);
        		}
        		
        		var polyLines = groupTagConfiguration.polylines;
				var lineNo = -1;
				var tagConfigurationStrs = "";
				var coordinates = [];
				for (var j=0; j < polyLines.length; ++j ) {
					var polyLine = polyLines[j];
					if (polyLine.lineNo != lineNo && lineNo != -1 ) {
						if (coordinates.length > 0) {
							addPolyline(coordinates, tagConfigurationStrs);
						}
						coordinates = [];
					}  
					lineNo = polyLine.lineNo;
					coordinates.push({lat: polyLine.lat, lng: polyLine.lng });
					tagConfigurationStrs = polyLine.tagConfigurationIdsStr;
				}
				
				if (coordinates.length > 0) {
					addPolyline(coordinates, tagConfigurationStrs);
				}
        	}

    	},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
	};
	
	$scope.showTagGroupConfiguration = function(alert) {
		highlightMarker(alert.tagConfiguration);
		highlightPolyLine(alert.tagConfiguration);
	}
	
	$scope.searchReverseGeoCode = function(lat,lng) {
		 var latlng = lat + "," + lng;
		 $http({
	   		url: 'http://maps.googleapis.com/maps/api/geocode/json',
	   		method: 'GET',
	   		params: {
	    	latlng: latlng,
	    	sensor: true
	   		}
		 }).then(function (response) {
			 var response = response.data;
			 if (response.results.length > 0) {
				 alert(JSON.stringify(response.results[0]));
			 
			 }
		   	},function (response){
		    	alert("Failed to search data, status=" + response.status);
		 });
	}
}]);