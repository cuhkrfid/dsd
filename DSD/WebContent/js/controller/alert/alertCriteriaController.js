angular.module('dsd').controller('alertCriteriaViewCtrl', ['$scope', '$translate','$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', '$window',
 function ($scope, $translate, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, $window) {

	$scope.rowHeight = 60; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 4;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows);
	
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
		  
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	$scope.getTableStyle= function() {
        return {
            height: (($scope.numberOfRows +  1)* $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight ) + 30 + "px"
        };
    };

	//User Maintenance
	$scope.openAlertCriteria = function (alertSettingId) {
		var path = 'SystemConfiguration/TagConfiguration/Edit/';
		path += (alertSettingId+"");
		$location.path(path);
	};
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
		{field:'name', displayName: $translate.instant('ALERT_CRITERIA_NAME'), width: 200,  headerCellClass: 'uiGridthemeHeaderColor',
 			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><a>{{row.entity.name}}</a></div>'},
 		{field:'coverDetect',  width: 120, displayName: $translate.instant('COVER_DETECT'),  headerCellClass: 'uiGridthemeHeaderColor',
 			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><a>{{row.entity.coverDetectOpenOrCloseStr}}</a></div>'},	
 		{field:'coverLevelHtml',  width: 200, displayName: $translate.instant('COVER_LEVEL'),  headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><div ng-repeat="item in row.entity.coverLevelHtml">{{item}}</div></div>'},							
		{field:'waterLevelHtml',  width: 200, displayName: $translate.instant('WATER_LEVEL'),  headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><div ng-repeat="item in row.entity.waterLevelHtml">{{item}}</div></div>'},								
		{field:'h2sHtml',  width: 200, displayName: $translate.instant('H2S'),  headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><div ng-repeat="item in row.entity.h2sHtml">{{item}}</div></div>'},					 		
		{field:'so2Html',  width: 200, displayName: $translate.instant('SO2'),  headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><div ng-repeat="item in row.entity.so2Html">{{item}}</div></div>'},			
		{field:'co2Html',  width: 200, displayName: $translate.instant('CO2'),  headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><div ng-repeat="item in row.entity.co2Html">{{item}}</div></div>'},
		{field:'ch4Html',  width: 200, displayName: $translate.instant('CH4'),  headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><div ng-repeat="item in row.entity.ch4Html">{{item}}</div></div>'},
		{field:'nh3Html',  width: 200, displayName: $translate.instant('NH3'), headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><div ng-repeat="item in row.entity.nh3Html">{{item}}</div></div>'},
		{field:'o2Html',  width: 200, displayName: $translate.instant('O2'),  headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openAlertCriteria(row.entity)"><div ng-repeat="item in row.entity.o2Html">{{item}}</div></div>'}
		];
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	

	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		useExternalFiltering: false,
		multiSelect: false,
		enableRowSelection: true,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		rowHeight: $scope.rowHeight,
		enableRowHeaderSelection: false,
		enableColumnResizing : true,
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	 
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {

		    });
		},
		columnDefs: $scope.columnDefs
		};
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.openAlertCriteria = function (alertCriteria) {
		appMainService.setAlertCriteria(alertCriteria);
		var path = 'Alert/AlertCriteria/New/';
		$location.path(path); 
	};
    
	 $scope.getAllAlertCriteria = function () {
		 $http({
		   		url: appMainService.getDomainAddress() + '/rest/alertCriteria/getAlertCriteria/',
		   		method: 'GET',
	            params: {
	                id: null,
	                name: null,
	                isCoverLevel: null,
	                isWaterLevel: null,
	                isH2s: null,
	                isSo2: null,
	                isCo2: null,
	                isCh4: null,
	                isNh3: null,
	                isO2: null
	            }
		 }).then(function (response) {
		 	$scope.gridOptions.data = response.data;
	   	   	},function (response){
		    	alert("Failed to search data, status=" + response.status);
	   	 });
	 };
	 $scope.getAllAlertCriteria();
	 

	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
}]);


angular.module('dsd').controller('alertCriteriaCreateCtrl', ['$scope','$translate', '$interval', '$timeout','$routeParams', '$http', '$location' , 'appMainService',
                                                         function ($scope, $translate, $interval, $timeout, $routeParams, $http, $location, appMainService){
    
	$scope.subName = "NEW_RECORD";
	
	$scope.alarmCriteria = appMainService.getAlertCriteria();
	if ($scope.alarmCriteria != null) {
		$scope.subName = "EDIT_RECORD";
	} else {
		$scope.alarmCriteria = new Object();
		$scope.alarmCriteria.id = 0;
		$scope.alarmCriteria.name = "";
	
	    $scope.alarmCriteria.isCoverLevel = false;
	    $scope.alarmCriteria.coverLevelCriteria = "";
	  
	    $scope.alarmCriteria.isWaterLevel = false;
	    $scope.alarmCriteria.waterLevelCriteria = "";
	    
	    $scope.alarmCriteria.isH2s = false;
	    $scope.alarmCriteria.h2sCriteria = "";
	  
	    $scope.alarmCriteria.isSo2 = false;
	    $scope.alarmCriteria.so2Criteria = "";
	  
	    $scope.alarmCriteria.isCo2 = false;
	    $scope.alarmCriteria.co2Criteria = "";
	
	    $scope.alarmCriteria.isCh4 = false;
	    $scope.alarmCriteria.ch4Criteria = "";
	    
	    $scope.alarmCriteria.isNh3 = false;
	    $scope.alarmCriteria.nh3Criteria = "";
	    
	    $scope.alarmCriteria.isO2 = false;
	    $scope.alarmCriteria.o2Criteria = "";
	    
		$scope.alarmCriteria.minCoverLevelValue = 0;
		$scope.alarmCriteria.maxCoverLevelValue = 200;
		$scope.alarmCriteria.isNotCoverLevelBetween = false;

		$scope.alarmCriteria.minWaterLevelValue = 0;
		$scope.alarmCriteria.maxWaterLevelValue = 200;
		$scope.alarmCriteria.isNotWaterLevelBetween = false;
		
		$scope.alarmCriteria.minH2sValue = -10.0;
		$scope.alarmCriteria.maxH2sValue = 100.0;
		$scope.alarmCriteria.isNotH2sBetween = false;
			
		$scope.alarmCriteria.minSo2Value = -10.0;
		$scope.alarmCriteria.maxSo2Value = 100.0;
		$scope.alarmCriteria.isNotSo2Between = false;

		$scope.alarmCriteria.minCo2Value = -10.0;
		$scope.alarmCriteria.maxCo2Value = 10.0;
		$scope.alarmCriteria.isNotCo2Between = false;

		$scope.alarmCriteria.minCh4Value = -50.0;
		$scope.alarmCriteria.maxCh4Value = 50.0;
		$scope.alarmCriteria.isNotCh4Between = false;
		
		$scope.alarmCriteria.minNh3Value = -50.0;
		$scope.alarmCriteria.maxNh3Value = 50.0;
		$scope.alarmCriteria.isNotNh3Between = false;

		$scope.alarmCriteria.minO2Value = -10.0;
		$scope.alarmCriteria.maxO2Value = 10.0;
		$scope.alarmCriteria.isNotO2Between = false;

		$scope.alarmCriteria.isCoverDetect = false;
		$scope.alarmCriteria.coverDetect = false;
		$scope.alarmCriteria.coverDetectOpenOrCloseStr = "--"; 
	};
	
	$scope.toggleIsCoverDetect = function() {
		if ($scope.alarmCriteria.isCoverDetect == true ) {
			if ($scope.alarmCriteria.coverDetect == true ) {
				$scope.alarmCriteria.coverDetectOpenOrCloseStr = $translate.instant("CLOSED");
			} else {
				$scope.alarmCriteria.coverDetectOpenOrCloseStr = $translate.instant("OPEN");
			}
		} else {
			$scope.alarmCriteria.coverDetectOpenOrCloseStr = "--"; 
		}
	};
	
	$scope.toggleCoverDetect = function() {
		if ($scope.alarmCriteria.coverDetect == true ) {
			$scope.alarmCriteria.coverDetectOpenOrCloseStr = $translate.instant("CLOSED");
		} else {
			$scope.alarmCriteria.coverDetectOpenOrCloseStr = $translate.instant("OPEN");
		}
	};
	
    $scope.createAlertCriteriaOk = function () {
 		
		if (!$scope.alarmCriteria.isCoverLevel) {
			$scope.alarmCriteria.coverLevelCriteria = "";
		}

		if (!$scope.alarmCriteria.isWaterLevel) {
			$scope.alarmCriteria.waterLevelCriteria = "";
		}

		if (!$scope.alarmCriteria.isH2s) {
			$scope.alarmCriteria.h2sCriteria = "";
		}

		if (!$scope.alarmCriteria.isSo2) {
			$scope.alarmCriteria.so2Criteria = "";
		}

		if (!$scope.alarmCriteria.isCo2) {
			$scope.alarmCriteria.co2Criteria = "";
		}
		
		if (!$scope.alarmCriteria.isCh4) {
			$scope.alarmCriteria.ch4Criteria = "";
		}
		
		if (!$scope.alarmCriteria.isNh3) {
			$scope.alarmCriteria.nh3Criteria = "";
		}
		
		if (!$scope.alarmCriteria.isO2) {
			$scope.alarmCriteria.o2Criteria = "";
		}
		
		var fd = new FormData();
    	
    	fd.append('name', $scope.alarmCriteria.name);
    	fd.append('level', $scope.alarmCriteria.level);
        
     	fd.append('isCoverLevel', $scope.alarmCriteria.isCoverLevel);
     	fd.append('coverLevelCriteria', $scope.alarmCriteria.coverLevelCriteria);
     
    	fd.append('isWaterLevel', $scope.alarmCriteria.isWaterLevel);
     	fd.append('waterLevelCriteria', $scope.alarmCriteria.waterLevelCriteria);
     
    	fd.append('isH2s', $scope.alarmCriteria.isH2s);
     	fd.append('h2sCriteria', $scope.alarmCriteria.h2sCriteria);
      	
    	fd.append('isSo2', $scope.alarmCriteria.isSo2);
     	fd.append('so2Criteria', $scope.alarmCriteria.so2Criteria);
    
    	fd.append('isCo2', $scope.alarmCriteria.isCo2);
     	fd.append('co2Criteria', $scope.alarmCriteria.co2Criteria);
      	
    	fd.append('isCh4', $scope.alarmCriteria.isCh4);
     	fd.append('nh3Criteria', $scope.alarmCriteria.nh3Criteria);

    	fd.append('isNh3', $scope.alarmCriteria.isNh3);
     	fd.append('ch4Criteria', $scope.alarmCriteria.ch4Criteria);
     	
    	fd.append('isO2', $scope.alarmCriteria.isO2);
     	fd.append('o2Criteria', $scope.alarmCriteria.o2Criteria);
     	
     	fd.append('isNotCoverLevelBetween', $scope.alarmCriteria.isNotCoverLevelBetween);
      	fd.append('isNotWaterLevelBetween', $scope.alarmCriteria.isNotWaterLevelBetween);
      	fd.append('isNotH2sBetween', $scope.alarmCriteria.isNotH2sBetween);
      	fd.append('isNotSo2Between', $scope.alarmCriteria.isNotSo2Between);
      	
      	
     	fd.append('isNotCo2Between', $scope.alarmCriteria.isNotCo2Between);
    	fd.append('isNotCh4Between', $scope.alarmCriteria.isNotCh4Between);
      	fd.append('isNotNh3Between', $scope.alarmCriteria.isNotNh3Between);
      	fd.append('isNotO2Between', $scope.alarmCriteria.isNotO2Between);
      	
      	fd.append('maxCoverLevelValue', $scope.alarmCriteria.maxCoverLevelValue);
      	fd.append('maxWaterLevelValue', $scope.alarmCriteria.maxWaterLevelValue);
      	fd.append('maxH2sValue', $scope.alarmCriteria.maxH2sValue);
      	fd.append('maxSo2Value', $scope.alarmCriteria.maxSo2Value);
      	fd.append('maxCo2Value', $scope.alarmCriteria.maxCo2Value);
      	fd.append('maxCh4Value', $scope.alarmCriteria.maxCh4Value);
      	fd.append('maxNh3Value', $scope.alarmCriteria.maxNh3Value);
      	fd.append('maxO2Value', $scope.alarmCriteria.maxO2Value);
      	
      	fd.append('minCoverLevelValue', $scope.alarmCriteria.minCoverLevelValue);
      	fd.append('minWaterLevelValue', $scope.alarmCriteria.minWaterLevelValue);
      	fd.append('minH2sValue', $scope.alarmCriteria.minH2sValue);
      	fd.append('minSo2Value', $scope.alarmCriteria.minSo2Value);
      	fd.append('minCo2Value', $scope.alarmCriteria.minCo2Value);
      	fd.append('minCh4Value', $scope.alarmCriteria.minCh4Value);
      	fd.append('minNh3Value', $scope.alarmCriteria.minNh3Value);
      	fd.append('minO2Value', $scope.alarmCriteria.minO2Value);

      	fd.append('isCoverDetect', $scope.alarmCriteria.isCoverDetect);
      	fd.append('coverDetect', $scope.alarmCriteria.coverDetect);
      	fd.append('coverDetectOpenOrCloseStr', $scope.alarmCriteria.coverDetectOpenOrCloseStr);
      	
      	
      	if ( $scope.alarmCriteria.id == 0) {
	 		$http.post(appMainService.getDomainAddress() + '/rest/alertCriteria/addAlertCriteria', fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        }).then(function (response) {
	        	appMainService.displayMessageResult(response.data, true);
	        	$location.path('Alert/AlertCriteria');
		    },function (response){
				  alert("Failed to add alert setting, status=" + response.status);
		    });
      	} else {
      		fd.append('id', $scope.alarmCriteria.id);
     		$http.post(appMainService.getDomainAddress() + '/rest/alertCriteria/updateAlertCriteria', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function (response) {
            	appMainService.displayMessageResult(response.data, true);
          	    $location.path('Alert/AlertCriteria');
    	    },function (response){
    			  alert("Failed to add alert setting, status=" + response.status);
    	    });      		
      	}
	};

	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
	
	$scope.removeAlertCriteria = function (alarmCriteriaId){
		$http({
            url: appMainService.getDomainAddress() + '/rest/alertCriteria/deleteData/',
            method: 'DELETE',
            params: {
            	id: alarmCriteriaId
            }
        }).then(function (response) {
        	appMainService.displayMessageResult(response.data, true);
      	    $location.path('Alert/AlertCriteria');
        },function (response){
  		  alert("Failed to delete alert setting, status=" + response.status);
        });
	};
	
	$scope.toggleFlag = function() {
		if (!$scope.alarmCriteria.isCoverLevel ) {
			$scope.alarmCriteria.isNotCoverLevelBetween = false;
		} else {
//			$scope.alarmCriteria.isNotCoverLevelBetween = true
		}
		
		if (!$scope.alarmCriteria.isWaterLevel ) {
			$scope.alarmCriteria.isNotWaterLevelBetween = false;
		} else {
//			$scope.alarmCriteria.isNotWaterLevelBetween = true;
			
		}
		
		if (!$scope.alarmCriteria.isH2s ) {
			$scope.alarmCriteria.isNotH2sBetween = false;
		} else {
//			$scope.alarmCriteria.isNotH2sBetween = true;			
		}
		
		if (!$scope.alarmCriteria.isSo2  ) {
			$scope.alarmCriteria.isNotSo2Between = false;
		} else {
//			$scope.alarmCriteria.isNotSo2Between = true
		}
		
		if (!$scope.alarmCriteria.isCo2  ) {
			$scope.alarmCriteria.isNotCo2Between = false;
		} else {
//			$scope.alarmCriteria.isNotCo2Between = true;
			
		}
		
		if (!$scope.alarmCriteria.isCh4  ) {
			$scope.alarmCriteria.isNotCh4Between = false;
		} else {
//			$scope.alarmCriteria.isNotCh4Between = true;			
		}
		
		if (!$scope.alarmCriteria.isNh3  ) {
			$scope.alarmCriteria.isNotNh3Between = false;
		} else {
//			$scope.alarmCriteria.isNotNh3Between = true;
			
		}
		
		if (!$scope.alarmCriteria.isO2  ) {
			$scope.alarmCriteria.isNotO2Between = false;
		} else {
//			$scope.alarmCriteria.isNotO2Between = true;			
		}
	}
	
 }]);


