angular.module('dsd').controller('networkChartViewCtrl', ['$scope', '$translate','$interval', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', 'ngDialog',
 function ($scope, $translate, $interval, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, ngDialog) {
	
	 $scope.chartDataSource = [];
	 $scope.getAllNetwork = function () {
		 $http({
		   		url: appMainService.getDomainAddress() + '/rest/networkView/getChartData/',
		   		method: 'GET',
		 }).then(function (response) {
		 	$scope.chartDataSource = response.data;
		 	
		 	$("#networkChart").getOrgChart({	
				scale: 0.4,
				editable: false,
				color: "black",		
				theme: "deborah",
				clickEvent: function( sender, args ) {
					return false; //if you want to cancel the event
				},
				dataSource: $scope.chartDataSource

			  });
			  
		 },function (response){
			 alert("Failed to search data, status=" + response.status);
	   	 });
	 };
	 $scope.getAllNetwork();
	 
	 $interval($scope.getAllNetwork, 60000);
	    
	 $scope.openDeviceConfig = function(id, deviceType) {
	   	  	
		 $scope.deviceToConfig = null;
		 $http({
    		url: appMainService.getDomainAddress() + '/rest/networkView/searchNetwork/',
    		method: 'GET',
	            params: {
	            	id: id, 
	            	deviceType : deviceType
           }
		 }).then(function (response) {
			 if ( response.data != null ) {
				 $scope.deviceToConfig = response.data;
				 $scope.openConfigDialog($scope.deviceToConfig);
			 } else {
				 alert("No data found");
				 return;
			 }
		 },function (response){
     		alert("Failed to search data, status=" + response.status);
     		return;
		 });
	 }

	 $scope.openConfigDialog = function(deviceToConfig) {
		var path = null; 
		
		if (deviceToConfig.deviceType == 0)
			path = 'view/diagnostic/configTagView.html'; 
		else if (deviceToConfig.deviceType == 1)
			path = 'view/diagnostic/configRouterView.html'; 
		else
			return;
		
		$scope.formData = new FormData();
		$scope.formData.measurePeriod = ( $scope.deviceToConfig.timeDiffInMsStr == "N/A" || $scope.deviceToConfig.timeDiffInMsStr == null ) ? 5 : Math.round($scope.deviceToConfig.timeDiffInMsStr / 1000);
		
		if (deviceToConfig.deviceType == 0) {
			$http({
	    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/getMeasurementPeriod/',
	    		method: 'GET',
		            params: {
		            	deviceIdStr : deviceToConfig.deviceId,
		            }
			 }).then(function (response) {
				 if ( response.data != null && response.data > 0 ) {
					 $scope.formData.measurePeriod = response.data;
				 } 
			 },function (response){
	     		alert("Failed to search data, status=" + response.status);
	     		return;
			 });
		} 
		
		$scope.formData.connected = deviceToConfig.connected;
		$scope.formData.networkId = deviceToConfig.id; 
		
		ngDialog.openConfirm({template: path,
			scope: $scope //Pass the scope object if you need to access in the template
		}).then(
			//Update
			function(value) {
				
				var fd = new FormData();
				fd.append('command',4); 
		 		fd.append('length', 11); 
		 		fd.append('deviceAddress', deviceToConfig.deviceAddress); 
		 	 	fd.append('cordId', deviceToConfig.cordId); 
		 		fd.append('measurePeriod', $scope.formData.measurePeriod); 
		 	 	
		 		$http.post(appMainService.getDomainAddress() + '/rest/diagnosticView/sendConfigToTag', fd, {
		            transformRequest: angular.identity,
		            headers: {'Content-Type': undefined}
		        }).then(function (response) {
		        	appMainService.displayMessageResult(response.data, true);
		        },function (response){
					alert(response.data);
			    });
		 	},
			//Cancel
			function(value) {
		 		if ( value === "Delete" ) {
	 				$http({
			    		url: appMainService.getDomainAddress() + '/rest/networkView/deleteNetwork/',
			    		method: 'DELETE',
			    		params: {
				    	   id: $scope.formData.networkId
			           	}
					 }).then(function (response) {
						$scope.chartDataSource = response.data;
						$("#networkChart").getOrgChart({	
							scale: 0.4,
							editable: false,
							color: "black",		
							theme: "deborah",
							clickEvent: function( sender, args ) {
								return false; //if you want to cancel the event
							},
							dataSource: $scope.chartDataSource
						});
						
					 },function (response){
						 alert("Failed to delete data, status=" + response.status);
						 return;
					});
		 			
		 		}
			}		
		);				 
	 }

	  
 }]);


