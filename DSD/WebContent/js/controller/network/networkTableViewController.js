angular.module('dsd').controller('networkTableViewCtrl', ['$rootScope', '$scope', '$translate', '$interval', '$routeParams', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', 'ngDialog', '$window',
 function ($rootScope, $scope, $translate, $interval, $routeParams,$uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, ngDialog, $window) {
	$scope.rowHeight = 30; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 7;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows) - 1;
	
	$scope.deviceToConfig = null;
	$scope.realTime = true;	
	
	$scope.resetTimeDiff = function (deviceToConfig) {
		
	 	$http({
	   		url: appMainService.getDomainAddress() + '/rest/networkView/resetMaxTimeDiff/',
	   		method: 'GET',
	          params: {
	        	  deviceAddress: deviceToConfig.deviceAddress
		      }
	   	}).then(function (response) {
	   		deviceToConfig.timeDiffMaxInMsStr = "N/A";
	   	},function (response){
	    	alert("Failed to search data, status=" + response.status);
	   	});
	}
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
      		{field:'date',  width: 120, displayName: $translate.instant('DATE'), enableFiltering : false, 
 		           cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.dateString\'>{{row.entity.dateString}}</span></div>'},
 			{field:'deviceTypeName',  width: 90, displayName: $translate.instant('DEVICE_TYPE'),  enableFiltering : true,
 				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.deviceTypeName\'>{{row.entity.deviceTypeName}}</span></div>'},			 
 		    {field:'cordId',  width: 90, displayName: $translate.instant('CORD_ID'),  enableFiltering : true,
 				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.cordId\'>{{row.entity.cordId}}</span></div>',
				 	filter: { condition: uiGridConstants.filter.EXACT } },
 			{field:'routerIdStr',  width: 90, displayName: $translate.instant('ROUTER_ID'),  enableFiltering : true,
 				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.routerIdStr\'>{{row.entity.routerIdStr}}</span></div>',
				 	filter: { condition: uiGridConstants.filter.EXACT } },
 			{field:'deviceIdStr',  width: 90, displayName: $translate.instant('DEVICE_ID'),  enableFiltering : true,
 				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.deviceIdStr\'>{{row.entity.deviceIdStr}}</span></div>',
				 	filter: { condition: uiGridConstants.filter.EXACT } },
 			{field:'parentAddress',  width: 90, displayName: $translate.instant('PARENT_ADDR'),  enableFiltering : true,
 				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.parentAddress\'>{{row.entity.parentAddress}}</span></div>'},			 
 			{field:'deviceAddress',  width: 90, displayName: $translate.instant('DEVICE_ADDR'),  enableFiltering : true,
 				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.deviceAddress\'>{{row.entity.deviceAddress}}</span></div>'},
		 	{field:'timeDiffInMs',  width: 120, displayName: $translate.instant('TIME_DIFF'),  enableFiltering : true,
 				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.timeDiffInMsStr\'>{{row.entity.timeDiffInMsStr}}</span></div>'},
 			{field:'timeDiffMaxInMsStr',  width: 95, displayName: $translate.instant('MAX_TIME_DIFF'),  enableFiltering : true,
 					cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.timeDiffMaxInMsStr\'>{{row.entity.timeDiffMaxInMsStr}}</span></div>'},
			{field:'details', displayName: $translate.instant('DETAILS'), enableFiltering : false,
 					cellTemplate: '<button class="mainContent-btn" ng-if="grid.appScope.currentPermission[\'NETWORK_TABLE_VIEW\']!=3" ng-click="grid.appScope.openConfigDevice(row.entity)" ng-style="grid.appScope.getConfigButtonStyle(row.entity)">{{\'SEND_TO_DEVICE\'|translate}}</button>&nbsp;<button tooltip-trigger="mouseenter" tooltip-placement="bottom" tooltip-append-to-body="true" tooltip-popup-delay="0" uib-tooltip="{{\'RESET_MAX_TIME_DIFF\'|translate}}"  class="mainContent-btn" ng-click="grid.appScope.resetTimeDiff(row.entity)"><img src="img/shortCut/diagnosticResetMaxTimeDiff.png" width="24" height="24"/></button>'},
 		 ];
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.$on('updateLanguageInUiGrid', function() {
		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		rowHeight: $scope.rowHeight,
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	  		
		enableColumnResizing : true,
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {
			
		    });
		},
		columnDefs: $scope.columnDefs
	 };
	
	 $scope.getAllNetwork = function () {
		 $http({
		   		url: appMainService.getDomainAddress() + '/rest/networkView/getTableData/',
		   		method: 'GET',
		 }).then(function (response) {
		 	$scope.gridOptions.data = response.data;
	   	   	},function (response){
		    	alert("Failed to search data, status=" + response.status);
	   	 });
	 };
	 $scope.getAllNetwork();
	   	
	$scope.getConfigButtonStyle = function( network ) {
		if ( network.deviceType == 2) {
			return {'visibility': 'hidden'};
		}
		return {'visibility': 'visible'};
	};
	
	$scope.gridOptions.data = [];

	$scope.getTableStyle= function() {
        return {
            height: (($scope.numberOfRows +  2)* $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight ) + "px"
        };
    };
	
	$scope.$on('receiveNetwork', function(event, networks) {
		if ($scope.realTime == false) {
			return;
		}
		
		for (var i = 0; i < networks.length; ++i) {
			var networkData = networks[i];
			var isFound = false;
	   		
	   		for (var i = 0; i < $scope.gridOptions.data.length; ++i) {
	   			if ( $scope.gridOptions.data[i].deviceAddress == networkData.deviceAddress &&
	   				 $scope.gridOptions.data[i].deviceType == networkData.deviceType) {
	   				$scope.gridOptions.data[i] = networkData;
	   				isFound = true;
	   				break;
	   			}
	   		}
	   		
	   		if (!isFound) {
	   			$scope.gridOptions.data.push(networkData);
	   		}
		}
	});
	$rootScope.connectWebSocketData();

	$scope.userCancel = function () {
		window.history.back();
	};
	
	$scope.openConfigDevice = function (row) {
		
		$scope.deviceToConfig = row;
		//json name attribute translation
		$scope.deviceToConfig.cordId = row.cordId;
		$scope.deviceToConfig.parentAddress = row.parentAddress;
		
		$scope.formData = new FormData();
		$scope.formData.measurePeriod = ( $scope.deviceToConfig.timeDiffInMsStr == "N/A" || $scope.deviceToConfig.timeDiffInMsStr == null ) ? 5 : Math.round($scope.deviceToConfig.timeDiffInMsStr / 1000);
		
		if ($scope.deviceToConfig.deviceType == 0) {
			$http({
	    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/getMeasurementPeriod/',
	    		method: 'GET',
		            params: {
		            	deviceIdStr : $scope.deviceToConfig.deviceId,
		            }
			 }).then(function (response) {
				 if ( response.data != null && response.data > 0 ) {
					 $scope.formData.measurePeriod = response.data;
				 } 
			 },function (response){
	     		alert("Failed to search data, status=" + response.status);
	     		return;
			 });
		} 
		
		var path = null; 
		if (row.deviceType == 0)
			path = 'view/diagnostic/configTagView.html'; 
		else if (row.deviceType == 1)
			path = 'view/diagnostic/configRouterView.html'; 
		else
			return;
		
		ngDialog.openConfirm({template: path,
			scope: $scope //Pass the scope object if you need to access in the template
		}).then(
			//Update
			function(value) {
				
				var fd = new FormData();
		 		fd.append('command',4); 
		 		fd.append('length', 11); 
		 		fd.append('deviceAddress', $scope.deviceToConfig.deviceAddress); 
		 	 	fd.append('cordId', $scope.deviceToConfig.cordId); 
		 		fd.append('measurePeriod', $scope.formData.measurePeriod); 
		 	 	
		 		$http.post(appMainService.getDomainAddress() + '/rest/diagnosticView/sendConfigToTag', fd, {
		            transformRequest: angular.identity,
		            headers: {'Content-Type': undefined}
		        }).then(function (response) {
		        	appMainService.displayMessageResult(response.data, true);
		        },function (response){
					alert(response.data);
			    });
		 	},
			//Cancel
			function(value) {
			}		
		);		
	};
}]);