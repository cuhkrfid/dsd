angular.module('dsd').controller('networkReaderHeartBeatViewCtrl', ['$rootScope', '$scope', '$translate','$routeParams', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', '$window', 'ngDialog', '$timeout',
 function ($rootScope, $scope, $translate, $routeParams,$uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, $window, ngDialog, $timeout) {

	$scope.rowHeight = 30; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 7;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows) - 1;
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
            {field:'dateString2',  width: 180, displayName: $translate.instant('TIME'), headerCellClass: 'uiGridthemeHeaderColor', 
 		           cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'"><a >{{row.entity.dateString2}}</a></div>'},
 		 	{field:'readerId',  width: 100, displayName: $translate.instant('READER_ID'), headerCellClass: 'uiGridthemeHeaderColor',
 	 			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a >{{row.entity.readerId}}</a></div>',
				 	filter: { condition: uiGridConstants.filter.EXACT } },
			{field:'readerName',  width: 200, displayName: $translate.instant('READER_NAME'), headerCellClass: 'uiGridthemeHeaderColor',
	 	 		 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a >{{row.entity.readerName}}</a></div>' },
 			{field:'rssi',  width: 100, displayName: $translate.instant('RSSI'), headerCellClass: 'uiGridthemeHeaderColor',
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'"  data-toggle="tooltip" title="dBm"><a >{{row.entity.rssi}}</a></div>'},
		    {field:'packetCounter',  width: 100, displayName: $translate.instant('PACKET_COUNTER'),  headerCellClass: 'uiGridthemeHeaderColor',
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a >{{row.entity.packetCounter}}</a></div>'} 	 
 	 	 ];		
		$scope.columnDefs = columnDefs;
	};
	
	$scope.setColumnDefs();

	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: true,
		enableVerticalScrollbar: 0,
		rowHeight: $scope.rowHeight,
		
		exporterMenuPdf : false,
		enableGridMenu: true,
	    enableSelectAll: true,
	    exporterCsvFilename: 'networkReaderHeartBeatFile.csv',
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	 
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {

		    });
		},
		columnDefs: $scope.columnDefs
	};
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.toggleRealTime = function() {

	};
	
	$scope.getTableStyle= function() {
        return {
            height: (($scope.numberOfRows +  2)* $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight ) + "px"
        };
    };
    
	
	$scope.realTime = false;
	$scope.searchReaderHeartBeat = function() {
		$scope.realTime = false;
		$scope.isLoading = true;
		
		$scope.loadingMessage = $translate.instant('LOADING_PLEASE_WAIT');
		
		ngDialog.open({
			scope: $scope,
			className: "ngdialog-theme-loading", 
			template: "view/loadingDialog.html",
            showClose: false,
            closeByEscape : false
		});
		
		var now = new Date().getTime();
		var fromDateLong =  now - 8*24*60*60*1000;
		$http({  
			url: appMainService.getDomainAddress() + '/rest/networkView/searchReaderHeartBeat/',
			method: 'GET',
	        params: {
	        	 readerId : null,
	        	 dateFrom : fromDateLong.toString(),
	        	 dateTo: now.toString(),
	        	 fromRssi: null,
	        	 toRssi: null,
	        	 desc: true
	        }
		}).then(function (response) {
			$scope.isLoading = false;
			$scope.realTime = true;
			ngDialog.close();
			if (response.data.length > 0) {
				$scope.gridOptions.data = response.data;
				ngDialog.close();
			} else {
				$scope.loadingMessage = $translate.instant('NO_DATA_FOUND');
			}

		},function (response){
			$scope.isLoading = false;
			$scope.loadingMessage = ("Failed to search data, status=" + response.status);	
		});	
	};
	
	$timeout($scope.searchReaderHeartBeat, 0);
	
	
	$scope.$on('receiveReaderHeartBeat', function(event, readerHeartBeat) {
		if ( $scope.realTime == false ) {
			return;
		}
		
		readerHeartBeat.isRead = false;
	 	$scope.gridOptions.data.unshift(readerHeartBeat);
	 	//remove last element if exceeded 5000
	 	if ( $scope.gridOptions.data.length >= 5000) {
	 		$scope.gridOptions.data.splice( $scope.gridOptions.data.length-1,1);
	 	}
 	 	$scope.$apply();   			
	});
	
	$rootScope.connectWebSocketData();
	
	$scope.userCancel = function () {
		window.history.back();
	};
}]);

	
