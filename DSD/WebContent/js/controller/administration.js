angular.module('dsd').controller('userCreateCtrl', ['$scope', '$translate', '$http', '$location' , 'appMainService',
    function ($scope, $translate, $http, $location, appMainService){
		
		$scope.fileName = '';
		$scope.newImage = null; 
		$scope.newPassword = "";
		$scope.sensorConfigTitle = 'NEW_RECORD';
		
		var user = appMainService.getUser()
		if (user == null) {
			$scope.user = new Object();
			$scope.user.email = "";
			$scope.user.phone = "";
			$scope.user.title = "";
			$scope.user.department = "";
		} else {
			$scope.sensorConfigTitle = 'EDIT_RECORD';
			$scope.user = user;
		}
		
//		document.getElementById('file').onchange = function(e) {
//			$scope.file = e.srcElement.files[0];
//				
//			var fullPath = document.getElementById('file').value;
//			if (fullPath) {
//			    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
//			    var filename = fullPath.substring(startIndex);
//			    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
//			    	$scope.fileName = filename.substring(1);
//			    }
//			}
//		};
	
		$scope.deleteImage = function() {
			$scope.fileName = '';
			$scope.newImage = null;
		}
		
		$scope.createUserOk = function () {
			
			if ( $scope.user.id > 0) {
				$scope.editUserOk();
				return;
			}
			
			var fd = new FormData();
			fd.append('name', $scope.user.name); 
	 		fd.append('userName', $scope.user.username); 
	 		fd.append('email', $scope.user.email); 
	 		fd.append('password', Sha256.hash($scope.password));
	 		
	 		fd.append('phone', $scope.user.phone);
	 		fd.append('fax', $scope.user.fax);
	 		fd.append('title', $scope.user.title);
	 		fd.append('active', $scope.user.active);
	 		fd.append('department', $scope.user.department);
	 		fd.append('groupId', $scope.user.groupId);
	 		
	 		if ( $scope.fileName == null) {
	 			fd.append('fileName', null);
	 			fd.append('file', null);
	 		} else {
	 			fd.append('fileName', $scope.fileName);
	 		 	fd.append('file', $scope.newImage);
	 		} 

		    $http.post(appMainService.getDomainAddress() + '/rest/user/addData', fd, {
		    	transformRequest: angular.identity,
            	headers: {'Content-Type': undefined}
		    }).then(function (response) {
				appMainService.displayMessageResult(response.data, true);
			  $location.path('Admin/UserMaintenance');
		    },function (response){
				  alert("Failed to add user, status=" + response.status);
		    });
		};
					
		$scope.editUserOk = function () {
			
			var fd = new FormData();
			
        	fd.append('id', $scope.user.id); 
			fd.append('name', $scope.user.name); 
	 		fd.append('userName', $scope.user.username); 
	 		fd.append('email', $scope.user.email); 
	 		
	 		fd.append('password', Sha256.hash($scope.password));
	 		
	 		fd.append('phone', $scope.user.phone);
	 		fd.append('fax', $scope.user.fax);
	 		fd.append('title', $scope.user.title);
	 		fd.append('active', $scope.user.active);
	 		fd.append('department', $scope.user.department);
	 		fd.append('groupId', $scope.user.groupId);
	 		
	 		if ( $scope.fileName == null) {
	 			fd.append('fileName', null);
	 			fd.append('file', null);
	 		} else {
	 			fd.append('fileName', $scope.fileName);
	 		 	fd.append('file', $scope.newImage);
	 		} 

		    $http.post(appMainService.getDomainAddress() + '/rest/user/updateData', fd, {
		    	transformRequest: angular.identity,
            	headers: {'Content-Type': undefined}
		    }).then(function (response) {
				appMainService.displayMessageResult(response.data, true);
	  		  $location.path('Admin/UserMaintenance');
	        },function (response){
	  		  alert("Failed to edit user, status=" + response.status);
	        });
		};
		
		$scope.userCancel = function () {
			window.history.back();
		};
		
		$scope.removeUser = function (userId){
			$http({
	            url: appMainService.getDomainAddress() + '/rest/user/deleteData/',
	            method: 'DELETE',
	            params: {
	            	id: userId
	            }
	        }).then(function (response) {
	        	appMainService.displayMessageResult(response.data, true);
				$location.path('Admin/UserMaintenance/');
	        },function (response){
	  		  	alert("Failed to delete user (ID = "+userId+"), status=" + response.status);
	        });
		};
		
		$http({
            url: appMainService.getDomainAddress() + '/rest/user/searchGroup/',
            method: 'GET',
            params: {
            	id: null,
            	name: null,
    			description: null,
    			active: true
            }
        }).then(function (response) {
    		$scope.groupList = response.data;
    		$http({
                url: appMainService.getDomainAddress() + '/rest/user/getAllPermission/',
                method: 'GET',
            }).then(function (response) {
        		$scope.permissionList = response.data;
            },function (response){
      		  alert("Failed to find permission list, status=" + response.status);
            });
        },function (response){
  		  alert("Failed to find group list, status=" + response.status);
        });
		
	}
]);


angular.module('dsd').controller('userRoleCtrl', ['$scope', '$routeParams', '$http', '$location' , 'appMainService',
    function ($scope, $routeParams, $http, $location, appMainService){
		$scope.editUserId = $routeParams.userId;
		$scope.assignAllRole = function (){
			$scope.assignedRoles = $scope.assignedRoles.concat($scope.unassignedRoles);
			$scope.unassignedRoles = [];
		};
		
		$scope.assignRole = function (){
			var gridApi = $scope.userUnassignedRolesGridApi;
			var selected = gridApi.selection.getSelectedRows();
			$scope.assignedRoles = $scope.assignedRoles.concat(selected);
			$scope.unassignedRoles = $scope.unassignedRoles.filter(
				function(item) {
					return selected.indexOf(item) === -1;
				}
			);
		};
		
		$scope.unassignAllRole = function (){
			$scope.unassignedRoles = $scope.unassignedRoles.concat($scope.assignedRoles);
			$scope.assignedRoles = [];
		};
		
		$scope.unassignRole = function (){
			var gridApi = $scope.userAssignedRolesGridApi;
			var selected = gridApi.selection.getSelectedRows();
			$scope.unassignedRoles = $scope.unassignedRoles.concat(selected);
			$scope.assignedRoles = $scope.assignedRoles.filter(
				function(item) {
					return selected.indexOf(item) === -1;
				}
			);
		};
		
		$scope.editUserRoleOk = function () {
			$http({
	            url: appMainService.getDomainAddress() + '/rest/user/updateUserRole',
	            method: 'PUT',
	            params: {
	            	id: $scope.editUserId,
	            	roleIdList: $scope.assignedRoles.map(function(a) {return a.id;}),
	            }
	        }).then(function (response) {
	    	  alert("Edit user-role success");
	    	  $scope.userEditCancel($scope.editUserId);
	        },function (response){
	  		  alert("Failed to edit user-role, status=" + response.status);
	        });
		};
		
		$http({
		    url: appMainService.getDomainAddress() + '/rest/user/findUserRoleById/',
		    method: 'GET',
		    params: {
		    	id: $routeParams.userId
		    }
		}).then(function (response) {
    		$scope.unassignedRoles = response.data.unassignedRoles;
    		$scope.assignedRoles = response.data.assignedRoles;
		},function (response){
			  alert("Failed to find user role (User ID = "+$scope.editUserId+"), status=" + response.status);
		});
		$scope.userRoleUnassignedGridOption = {
			data:'unassignedRoles',
			enableRowSelection: true,
			multiSelect: true,
//			enableRowHeaderSelection: false,
			rowHeight: 37,
			minRowsToShow: 10,
			enableFiltering: false,
			onRegisterApi: function(gridApi){
		        $scope.userUnassignedRolesGridApi = gridApi;
		    },
			columnDefs: [
			    {field:'chiName', displayName: 'Chinese Name'},
			    {field:'engName', displayName: 'English Name'}//,
//				{field:'deleteBtn', displayName: ' ', cellTemplate: '<button type="button" class="btn btn-danger" ng-click="grid.appScope.removeUserRole(row)">Delete</button>'},
			]
		};
		$scope.userRoleAssignedGridOption = {
			data:'assignedRoles',
			enableRowSelection: true,
			multiSelect: true,
//			enableRowHeaderSelection: false,
			rowHeight: 37,
			minRowsToShow: 10,
			enableFiltering: false,
			onRegisterApi: function(gridApi){
		        $scope.userAssignedRolesGridApi = gridApi;
		    },
			columnDefs: [
			    {field:'chiName', displayName: 'Chinese Name'},
			    {field:'engName', displayName: 'English Name'}//,
//				{field:'deleteBtn', displayName: ' ', cellTemplate: '<button type="button" class="btn btn-danger" ng-click="grid.appScope.removeUserRole(row)">Delete</button>'},
			]
		};
	}
]);

angular.module('dsd').controller('userGroupCtrl', ['$scope', '$routeParams', '$http', '$location' , 'appMainService',
    function ($scope, $routeParams, $http, $location, appMainService){
		$scope.editUserId = $routeParams.userId;
		$scope.assignAllGroup = function (){
			$scope.assignedGroups = $scope.assignedGroups.concat($scope.unassignedGroups);
			$scope.unassignedGroups = [];
		};
		
		$scope.assignGroup = function (){
			var gridApi = $scope.userUnassignedGroupsGridApi;
			var selected = gridApi.selection.getSelectedRows();
			$scope.assignedGroups = $scope.assignedGroups.concat(selected);
			$scope.unassignedGroups = $scope.unassignedGroups.filter(
				function(item) {
					return selected.indexOf(item) === -1;
				}
			);
		};
		
		$scope.unassignAllGroup = function (){
			$scope.unassignedGroups = $scope.unassignedGroups.concat($scope.assignedGroups);
			$scope.assignedGroups = [];
		};
		
		$scope.unassignGroup = function (){
			var gridApi = $scope.userAssignedGroupsGridApi;
			var selected = gridApi.selection.getSelectedRows();
			$scope.unassignedGroups = $scope.unassignedGroups.concat(selected);
			$scope.assignedGroups = $scope.assignedGroups.filter(
				function(item) {
					return selected.indexOf(item) === -1;
				}
			);
		};
		
		$scope.editUserGroupOk = function () {
			$http({
	            url: appMainService.getDomainAddress() + '/rest/user/updateUserGroup',
	            method: 'PUT',
	            params: {
	            	id: $scope.editUserId,
	            	groupIdList: $scope.assignedGroups.map(function(a) {return a.id;}),
	            }
	        }).then(function (response) {
	    	  alert("Edit user-group success");
	    	  $scope.userEditCancel($scope.editUserId);
	        },function (response){
	  		  alert("Failed to edit user-group, status=" + response.status);
	        });
		};
		
		$http({
		    url: appMainService.getDomainAddress() + '/rest/user/findUserGroupById/',
		    method: 'GET',
		    params: {
		    	id: $routeParams.userId
		    }
		}).then(function (response) {
    		$scope.unassignedGroups = response.data.unassignedGroups;
    		$scope.assignedGroups = response.data.assignedGroups;
		},function (response){
			  alert("Failed to find user group (User ID = "+$scope.editUserId+"), status=" + response.status);
		});
		$scope.userGroupUnassignedGridOption = {
			data:'unassignedGroups',
			enableRowSelection: true,
			multiSelect: true,
			rowHeight: 37,
			minRowsToShow: 10,
			enableFiltering: false,
			onRegisterApi: function(gridApi){
		        $scope.userUnassignedGroupsGridApi = gridApi;
		    },
			columnDefs: [
			    {field:'chiName', displayName: 'Chinese Name'},
			    {field:'engName', displayName: 'English Name'}
			]
		};
		$scope.userGroupAssignedGridOption = {
			data:'assignedGroups',
			enableRowSelection: true,
			multiSelect: true,
			rowHeight: 37,
			minRowsToShow: 10,
			enableFiltering: false,
			onRegisterApi: function(gridApi){
		        $scope.userAssignedGroupsGridApi = gridApi;
		    },
			columnDefs: [
			    {field:'chiName', displayName: 'Chinese Name'},
			    {field:'engName', displayName: 'English Name'}
			]
		};
	}
]);

angular.module('dsd').controller('userCtrl', ['$translate','$scope', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService',
                                                function ($translate, $scope, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService) {
                                                	
	$scope.section="";
	$scope.users=[
	];
	$scope.roles=[
	];
	$scope.modules = [];
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.userTotalItems = 100;
	$scope.userCurrentPage = 1;
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="#">Impersonate </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
	
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	$scope.getTableStyle= function() {
        var marginHeight = 50;
        var length = $scope.users.length; 
        return {
            height: (35 * $scope.userGridOption.rowHeight + $scope.userGridOption.headerRowHeight + marginHeight ) + "px"
        };
    };
    
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
		    {field:'username', displayName: $translate.instant('TXT_USERNAME') ,  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openUser(row.entity)"><a>{{row.entity.username}}</a></div>'},
		    {field:'name', displayName: $translate.instant('NAME'),  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openUser(row.entity)"><a>{{row.entity.name}}</a></div>'},
		    {field:'phone', displayName:$translate.instant('PHONE'),  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openUser(row.entity)"><a>{{row.entity.phone}}</a></div>'},
			{field:'email', displayName: $translate.instant('EMAIL'),  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openUser(row.entity)"><a>{{row.entity.email}}</a></div>'},
		    {field:'department', displayName: $translate.instant('DEPARTMENT'),  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openUser(row.entity)"><a>{{row.entity.department}}</a></div>'},
		    {field:'title', displayName:$translate.instant('TITLE'),  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openUser(row.entity)"><a>{{row.entity.title}}</a></div>'},
		    {field:'group', displayName:$translate.instant('GROUPS'),  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openUser(row.entity)"><a>{{row.entity.group.name}}</a></div>'},
/*		    {field:'permission', displayName:$translate.instant('PERMISSION'), cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openUser(row.entity)"><a>{{row.entity.permission.name}}</a></div>'},
*/		    {field:'active', displayName:$translate.instant('ACTIVE'),  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openUser(row.entity)"><a>{{row.entity.active}}</a></div>'},			
		];	
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.userGridOption={
		data:'users', 
		minRowsToShow: 30,
		enableFiltering: true,
	    useExternalFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableHorizontalScrollbar: true,
		enableVerticalScrollbar: 0,
		enableRowHeaderSelection: false,
		enableColumnResizing : true,
		onRegisterApi: function(gridApi){
	        $scope.userGridApi = gridApi;
	        $scope.userGridApi.core.on.filterChanged( $scope, function() {
	            var grid = this.grid;
	            var id = grid.columns.filter(function (item){
	            	return item.field == "id";
	            })[0].filters[0].term;
	            var username = grid.columns.filter(function (item){
	            	return item.field == "username";
	            })[0].filters[0].term;
	            var chiName = grid.columns.filter(function (item){
	            	return item.field == "chiName";
	            })[0].filters[0].term;
	            var engName = grid.columns.filter(function (item){
	            	return item.field == "engName";
	            })[0].filters[0].term;
	            var email = grid.columns.filter(function (item){
	            	return item.field == "email";
	            })[0].filters[0].term;
	            var dept = grid.columns.filter(function (item){
	            	return item.field == "department";
	            })[0].filters[0].term;
	            var password = grid.columns.filter(function (item){
	            	return item.field == "password";
	            })[0].filters[0].term;
	            var phone = grid.columns.filter(function (item){
	            	return item.field == "phone";
	            })[0].filters[0].term;
	            var notification = grid.columns.filter(function (item){
	            	return item.field == "notification";
	            })[0].filters[0].term;	    
	            var title = grid.columns.filter(function (item){
	            	return item.field == "title";
	            })[0].filters[0].term;	
	            var pwdResetAfterLogin = grid.columns.filter(function (item){
	            	return item.field == "pwdResetAfterLogin";
	            })[0].filters[0].term;	
	            var active = grid.columns.filter(function (item){
	            	return item.field == "active";
	            })[0].filters[0].term;	
	            
	            $scope.searchUser(id, chiName, engName, username, email, dept, password, phone, notification, fax, title, pwdResetAfterLogin,active);
	        });
	    },
		columnDefs: $scope.columnDefs
	};

	
	$scope.$on('updateLanguageInUiGrid', function() {
		$scope.setColumnDefs();  
		$scope.userGridOption.columnDefs = $scope.columnDefs;
	});
	
	$scope.roleGridOption={
		data:'roles',
		minRowsToShow: 15,
		enableFiltering: true,
		columnDefs: [
		    {field:'roleId'},
		    {field:'roleName'},
		    {field:'roleCode'},
		    {field:'isDeleted'}
		]
	};
	
	$scope.getStaticData = function (dataName){
		$http({
			url: appMainService.getDomainAddress() + '/rest/staticData/searchData/',
			method: 'GET',
			params: {
				dataName:dataName
			}
		}).then(function (response) {
			if (response.data.length > 0){
				$scope.staticData[dataName] = response.data[0];
			}
		},function (response){
			alert("Failed to search data, status=" + response.status);
		});
	};
	$scope.getStaticData("mainTitle");
	$scope.getStaticData("welcomeMessage");
	$scope.toggleFiltering = function(){
	    $scope.userGridOption.enableFiltering = !$scope.userGridOption.enableFiltering;
	    $scope.userGridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN ); 
	};
	
	$scope.toggleCompanyFiltering = function(){
		$scope.companyGridOption.enableFiltering = !$scope.companyGridOption.enableFiltering;
	    $scope.companyGridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	 
	};
	
    $scope.searchUser = function (searchId, searchChiName, searchEngName, searchLoginId, searchEmail, searchDept, 
    		searchPassword, searchPhone, searchNotification, searchFax, searchTitle, searchPwdResetAfterLogin, searchActive ) {
        $http({
            url: appMainService.getDomainAddress() + '/rest/user/searchData/',
            method: 'GET',
            params: {
            	id: searchId, 
            	chiName: searchChiName, 
            	engName: searchEngName, 
            	userName: searchLoginId, 
            	email: searchEmail, 
            	dept: searchDept,
            	password: searchPassword,
            	phone: searchPhone,
            	notification : searchNotification, 
            	fax : searchFax,
            	title : searchTitle,
            	pwdResetAfterLogin : searchPwdResetAfterLogin,
            	active : searchActive
            	
            }
        }).then(function (response) {
            $scope.users = response.data;
//                                                        	$scope.userTotalItems = response.data.length;
        },function (response){
  		  alert("Failed to search data, status=" + response.status);
        });
    };
    
    $scope.resetUser = function () {
    	$scope.searchId = "";
    	$scope.searchChiName = "";
    	$scope.searchEngName = "";
    	$scope.searchLoginId = "";
    	$scope.searchEmail = "";
    };
    
    //System Settings
    $scope.showSystemSetting = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'view/systemSettingView.html',
			controller: 'SystemSettingCtrl',
			size: 'lg',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
    
    
    
    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });
 
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.persons = {currentPage : 1};
	
	//User Maintenance
	$scope.openUser = function (user) {
		appMainService.setUser(user);
		var path = 'Admin/UserMaintenance/New/';
		$location.path(path);
	};

	$scope.setShowUserMain = function (){
		$scope.showUserMain = true;
		$scope.showRoleMain = false;
		$scope.searchUser(null,null,null,null,null,null,null,null,null,null,null);
	};
	
	//Role Maintanence
	$scope.openRole = function (x) {
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'editRoleWindow.html',
			controller: 'EditRoleWindowCtrl',
			resolve: {
				x: function (){
					return x;
				}
			}
		});
	};

	$scope.newRole = function (){
		var roles = $scope.roles;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'newRoleWindow.html',
			controller: 'NewRoleWindowCtrl',
			resolve: {
				roles: function (){
					return roles;
				}
			}
		});
	};
	$scope.removeRole = function (index){
		$scope.roles.splice(index, 1);
	};

	$scope.setShowRoleMain = function (){
		$scope.showUserMain = false;
		$scope.showRoleMain = true;
	};
	
	$scope.companyTotalItems = 100;
	$scope.companyCurrentPage = 1;
	$scope.companies=[];
	$scope.companyGridOption={
			data:'companies', 
			rowHeight: 37,
			minRowsToShow: 15,
			enableFiltering: true,
			useExternalFiltering: true,
			columnDefs: [
			    {field:'id', width: 30,
			 		headerCellTemplate: '<a href="javascript:void(0);" ng-click="grid.appScope.toggleCompanyFiltering()"><img src="img/basic/Filter-48.png" width="24" height="24" ></img></a>', 
			 		cellTemplate: '<img src="img/basic/Info-48.png" width="24" height="24"></img>'
			 	},
			    
			    {field:'chiName', displayName: 'Chinese Name', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openCompany(row.entity.id)"><a>{{row.entity.chiName}}</a></div>'},
			    {field:'engName', displayName: 'English Name', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openCompany(row.entity.id)"><a>{{row.entity.engName}}</a></div>'},
			    {field:'address', displayName:'Address', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openCompany(row.entity.id)"><a>{{row.entity.address}}</a></div>'},
			    {field:'telNo', displayName:'Tel No.', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openCompany(row.entity.id)"><a>{{row.entity.telNo}}</a></div>'},
			    {field:'faxNo', displayName:'Fax No.', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openCompany(row.entity.id)"><a>{{row.entity.faxNo}}</a></div>'},
			    {field:'deleteBtn', enableFiltering: false, displayName: ' ', cellTemplate: '<a href="javascript:void(0);" ng-click="grid.appScope.removeCompany(row)"><img src="img/basic/Cancel-48.png" width="24" height="24"></img></a>'},
			],
			onRegisterApi: function(gridApi){
		        $scope.companyGridApi = gridApi;
		        $scope.companyGridApi.core.on.filterChanged( $scope, function() {
		            var grid = this.grid;
		            var id = grid.columns[0].filters[0].term;
		            var chiName = grid.columns[1].filters[0].term;
		            var engName = grid.columns[2].filters[0].term;
		            var address = grid.columns[3].filters[0].term;
		            var telNo = grid.columns[4].filters[0].term;
		            var faxNo = grid.columns[5].filters[0].term;
		            
		            $scope.searchCompany(id, chiName, engName, address, telNo, faxNo);
		        });
		    },
	};
	$scope.openCompany = function (companyId) {
		var path = 'Admin/CompanyMaintenance/Edit/';
		path += (companyId+"");
		$location.path(path);
	};
	$scope.newCompany = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'newCompanyWindow.html',
			controller: 'NewCompanyWindowCtrl',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
	$scope.removeCompany = function (row){
		var entity = row.entity;
		var id = entity.id;
		$http({
            url: appMainService.getDomainAddress() + '/rest/user/deleteCompany/',
            method: 'PUT',
            params: {
            	id: id
            }
        }).then(function (response) {
      	    alert("Delete company success");
      	    $scope.searchCompany(null,null,null,null,null,null);
        },function (response){
  		  alert("Failed to delete company, status=" + response.status);
        });
	};
    $scope.searchCompany = function (searchId, searchChiName, searchEngName, searchAddress, searchTelNo, searchFaxNo) {
        $http({
            url: appMainService.getDomainAddress() + '/rest/user/searchCompany/',
            method: 'GET',
            params: {
            	id: searchId, 
            	chiName: searchChiName, 
            	engName: searchEngName, 
            	address: searchAddress, 
            	telNo: searchTelNo,
            	faxNo: searchFaxNo
            }
        }).then(function (response) {
            $scope.companies = response.data;
//                                                        	$scope.userTotalItems = response.data.length;
        },function (response){
  		  alert("Failed to search data, status=" + response.status);
        });
    };
	$scope.newSearchCompany = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'newSearchCompanyWindow.html',
			controller: 'NewSearchCompanyWindowCtrl',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
	
	$scope.userCancel = function () {
		window.history.back();
	};
	
	$scope.userEditCancel = function (userId){
		$scope.openUser(userId);
	};
	
}]);

