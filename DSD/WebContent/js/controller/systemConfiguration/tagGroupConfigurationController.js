angular.module('dsd').controller('tagGroupConfigurationCtrl', ['$scope', '$translate', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', '$window',
 function ($scope, $translate, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, $window) {
	$scope.rowHeight = 120; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 2;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows);
	
	$scope.section="";
	$scope.users=[
	];
	$scope.roles=[
	];
	$scope.modules = [];
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.userTotalItems = 100;
	$scope.userCurrentPage = 1;
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
		  
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	$scope.getTableStyle= function() {
        return {
        	height: (($scope.numberOfRows +  1)* $scope.gridOptions.rowHeight  ) + "px"
        };
    };
	
	
	$scope.changeLanguage=function(locale) {
		alert(locale);
	};
	
	$scope.toggleFiltering = function(){
		$scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
	    $scope.tagConfigurationGridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	 
	};

	
    //System Settings
    $scope.showSystemSetting = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'view/systemSettingView.html',
			controller: 'SystemSettingCtrl',
			size: 'lg',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
    
    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });
 
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.persons = {currentPage : 1};
	
	

	$scope.removeTagConfiguration = function (row){
		var path = 'SystemConfiguration/TagConfiguration/Delete/';
		path += (row.entity.id+"");
		$location.path(path);
	};
	
	$scope.tagConfigurationTotalItems = 100;
	$scope.tagConfigurationCurrentPage = 1;
	$scope.tagConfigurations=[];
	
	$scope.siteConfigurations = [];
	$scope.selectedSite = appMainService.getSiteConfiguration();
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
			{field:'name', displayName: $translate.instant('GROUP_NAME'), width: 250, headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openGroupTagConfiguration(row.entity)"><a>{{row.entity.name}}</a></div>'},
			{field:'tagConfigurationHtml', displayName:$translate.instant('TAG_NAME'), width: 250, headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" style="overflow-y:scroll;height:120px;" ng-click="grid.appScope.openGroupTagGroupConfiguration(row.entity)"><div ng-repeat="item in row.entity.tagConfigurations">{{item.name}}</div></div>'}
		];	
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.$on('updateLanguageInUiGrid', function() {
		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: false,
		enableRowSelection: true,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		rowHeight: $scope.rowHeight,
		enableRowHeaderSelection: false,
		enableColumnResizing : true,
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {
		    });
		},
		columnDefs: $scope.columnDefs
	};
	
	$scope.getTableStyle= function() {
        return {
        	height: (($scope.numberOfRows +  1)* $scope.gridOptions.rowHeight  ) + "px"
        };
    };
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	$scope.configureAlert = function (tagConfiguration) {
		var path = '/Alert/AlertSetting/New/';
		appMainService.setTagConfiguration(tagConfiguration);
		$location.path(path);    		
	};
	
	$scope.openGroupTagGroupConfiguration = function (groupTagGroupConfiguration) {
		var path = 'SystemConfiguration/TagConfigurationGroupView/New/';
		appMainService.setGroupTagConfiguration(groupTagGroupConfiguration);
		$location.path(path);    		
	};
	
	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
	$scope.validating = false; 
	

	
	$scope.searchGroupTagConfiguration = function(selectedSite) {

		if ( selectedSite == null)
			return; 
		 
	    var date = new Date();
		var startDate = new Date(date.getTime() - 24*60*60*1000);
		$http({
    		url: appMainService.getDomainAddress() + '/rest/groupTagConfiguration/searchData/',
    		method: 'GET',
    		params: {
    			id : null,
    			fromDate : startDate.getTime().toString(),
    			toDate : date.getTime().toString(),
    			siteConfigurationId : selectedSite.id,
    		}
        }).then(function (response) {
    		$scope.gridOptions.data = response.data;
    	},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
	};

	
	$http({
		url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
		method: 'GET',
        params: {
        id: null, 
        name: null, 
        description: null,
        defaultSite: null
       }
	}).then(function (response) {
		$scope.siteConfigurations = response.data;
		if ($scope.selectedSite != null) {
			for (var i = 0; i < $scope.siteConfigurations.length; ++i) {
				if ( $scope.siteConfigurations[i].id == $scope.selectedSite.id ) {
					$scope.selectedSite = $scope.siteConfigurations[i];
					$scope.validateTagConfiguration($scope.selectedSite);
					break;
				}
			}
		} else {
			if ($scope.siteConfigurations.length > 0) {
				$scope.selectedSite = $scope.siteConfigurations[0];
				$scope.searchGroupTagConfiguration($scope.selectedSite);
			}
		}
		
	},function (response){
 		alert("Failed to find site configuration list, status=" + response.status);
	});
	
	$scope.searchTag = function (siteId) {
		$scope.validateTagConfiguration(siteId);
	}; 
}]);


angular.module('dsd').controller('tagGroupConfigurationCreateCtrl', ['$scope', '$translate','$interval', '$timeout','$routeParams', '$http', '$location' , 'appMainService','ngDialog', '$window',
                                                         function ($scope, $translate, $interval, $timeout, $routeParams, $http, $location, appMainService, ngDialog, $window){

	$scope.rowHeight = 30; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 8;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows);
	
	$scope.siteConfigurations = [];
	$scope.tagConfigurations = []
	$scope.markers = [];
	
	$scope.fileName = '';
	$scope.image = null; 
	
	$scope.removeTagConfigurationByLatLng = function(latLng) {
		
	    //var obj = latLng.toString().replace("(", "").replace(")", "").replace(" ", "").split(",");
	    var lat = latLng.lat();
	    var lng = latLng.lng();
	   
	    for (var i = 0; i < $scope.groupTagConfiguration.tagConfigurations.length; ++i ) {
			var tagConfiguration = $scope.groupTagConfiguration.tagConfigurations[i];
			if (tagConfiguration.lat == lat && tagConfiguration.lon == lng) {
				$scope.groupTagConfiguration.tagConfigurations.splice(i,1);
				deleteMarker(tagConfiguration);
				return;
			}
		}
	};

	$scope.getCoord4DecimalPlaces = function(value) {
	    var valueArray = value.toString().split(".");
	    var valueDecimal = valueArray[1].substring(0, 4);
	    return valueArray[0] + "." + valueDecimal;
	};
	
	$scope.checkIfTagConfigurationExistInGroup = function(tagConfig) {
		for (var i = 0; i < $scope.groupTagConfiguration.tagConfigurations.length; ++i ) {
			var tagConfiguration = $scope.groupTagConfiguration.tagConfigurations[i];
			if (tagConfiguration.id == tagConfig.id) {
				return true;
			}
		}
		return false;
	};
	

	$scope.addTagConfigurationByLatLng = function(latLng) {

	    var lat = $scope.getCoord4DecimalPlaces( latLng.lat() ); 
	    var lng = $scope.getCoord4DecimalPlaces( latLng.lng() );
	    
	    for (var i = 0; i < $scope.tagConfigurations.length; ++i ) {
			var tagConfiguration = $scope.tagConfigurations[i];
			var tagLat = $scope.getCoord4DecimalPlaces(tagConfiguration.lat);
			var tagLng = $scope.getCoord4DecimalPlaces(tagConfiguration.lon);
			
			if (tagLat == lat && tagLng == lng ) {
				$scope.addToGroupTagConfiguration(tagConfiguration);
				return tagConfiguration.id;
			}
		}
	    return null;
	};
	
	$scope.removeTagConfigurationByLatLng = function(latLng, tagConfigurationId) {
	    var lat = $scope.getCoord4DecimalPlaces( latLng.lat() ); 
	    var lng = $scope.getCoord4DecimalPlaces( latLng.lng() );
		
		for (var i = 0; i < $scope.groupTagConfiguration.tagConfigurations.length; ++i ) {
			var tagConfiguration = $scope.groupTagConfiguration.tagConfigurations[i];
			if (tagConfiguration.id == tagConfigurationId) {
				var tagLat = $scope.getCoord4DecimalPlaces(tagConfiguration.lat);
				var tagLng = $scope.getCoord4DecimalPlaces(tagConfiguration.lon);
				if (tagLat != lat || tagLng != lng ) {
					$scope.groupTagConfiguration.tagConfigurations.splice(i,1);
					deleteMarker(tagConfiguration);
					return tagConfigurationId;
				}
			}
		}
		return null;
	};
	
	$scope.startPage = function() {
    	$scope.groupTagConfiguration = appMainService.getGroupTagConfiguration();
    	if ( $scope.groupTagConfiguration == null ) {
    		$scope.groupTagConfiguration = new Object();
    		$scope.groupTagConfiguration.id = 0;
    		$scope.groupTagConfiguration.siteConfiguration = null;
    		$scope.groupTagConfiguration.name = "";
    		$scope.groupTagConfiguration.tagConfigurations = [];
    		$scope.subName = "NEW_RECORD";
    		
    	} else {
    		$scope.subName = "EDIT_RECORD";
    	}
    	$scope.tagConfigurationGridOptions.data = $scope.groupTagConfiguration.tagConfigurations;
    	
    	$http({
			url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
			method: 'GET',
	        params: {
	        id: null, 
	        name: null, 
	        description: null,
	        defaultSite: null
	       }
		}).then(function (response) {
			$scope.siteConfigurations = response.data;
			if ( $scope.siteConfigurations.length > 0 && $scope.groupTagConfiguration.siteConfiguration != null ) {
				for (var i = 0; i < $scope.siteConfigurations.length; ++i) {
					if ( $scope.groupTagConfiguration.siteConfiguration.id == $scope.siteConfigurations[i].id ) {
						$scope.groupTagConfiguration.siteConfiguration = $scope.siteConfigurations[i];
						$scope.getTagConfiguration($scope.groupTagConfiguration);
						break;
					}
				}
			}
			
		},function (response){
	 		alert("Failed to find site configuration list, status=" + response.status);
		});
    };

    $scope.getDeleteKeyWord = function () {
    	
    	return $translate.instant('DELETE');
    };
    
    $scope.getCreateOrCompletePipleTitle = function() {
    	return $scope.createOrCompletePipleTitle;
    };
    
    
    $scope.createOrCompletePipleTitle = 'ADD_LINE';
    $scope.tagConfigurationIdsStrToPolyLine = "";
    $scope.tagConfigurationIdsStrToPolyLineArray = [];
    
    $scope.getCompletePipe = function() {
    	return $scope.createOrCompletePipleTitle == 'COMPLETE_LINE';
    }
    
    
    $scope.createOrCompletePiple = function() {
    	if( $scope.createOrCompletePipleTitle === 'ADD_LINE') {
    		var coordinates = [];
        	addPolyline(coordinates);
    		$scope.createOrCompletePipleTitle = 'COMPLETE_LINE';
    		
    	} else {
    		
    		$scope.tagConfigurationIdsStrToPolyLineArray.push($scope.tagConfigurationIdsStrToPolyLine);
    		$scope.tagConfigurationIdsStrToPolyLine = "";
    		
    		//completed Pipe
    		$scope.createOrCompletePipleTitle = 'ADD_LINE';
    	}
    };

    
    $scope.getTagConfiguration = function(groupTagConfiguration) {
    	
   		$http({
    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
    		method: 'GET',
            params: {
            id: null, 
            name: null, 
            deviceId : null,
            deviceAddress : null,
            groupId : null,
            description: null,
            siteName : null,
            siteId : groupTagConfiguration.siteConfiguration.id,
           }
    	}).then(function (response) {
    		$scope.tagConfigurations = response.data;
   			scrollToMarkers( $scope.tagConfigurations, groupTagConfiguration.siteConfiguration);
   			if ( groupTagConfiguration.id > 0) {
   				for (var i = 0; i < groupTagConfiguration.tagConfigurations.length; ++i ) {
					var tagConfiguration = groupTagConfiguration.tagConfigurations[i];
					selectMarker(tagConfiguration);
				}
				
				var polyLines = groupTagConfiguration.polylines;
				var lineNo = -1;
				var coordinates = [];
				for (var j=0; j < polyLines.length; ++j ) {
					var polyLine = polyLines[j];
					if (polyLine.lineNo != lineNo && lineNo != -1 ) {
						if (coordinates.length > 0) {
							addPolyline(coordinates);
						}
						coordinates = [];
					}  
					lineNo = polyLine.lineNo;
					coordinates.push({lat: polyLine.lat, lng: polyLine.lng });
				}
				
				if (coordinates.length > 0) {
					addPolyline(coordinates);
				}
   			}
   			
   		},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
    };
    
     $scope.getGroupTagConfiguration = function() {
    	 return  $scope.groupTagConfiguration;
     };
    
     $scope.getPolyLineCoordinates = function()  {
    	
    	 var polyLineCoordinates = [];
    	 
    	 for (var i = 0; i < $scope.groupTagConfiguration.tagConfigurations.length; ++i ) {
    		 var tagConfiguration = $scope.groupTagConfiguration.tagConfigurations[i];
    		 polyLineCoordinates.push({ lat: tagConfiguration.lat, lng:  tagConfiguration.lon });
    	 }
    	 return polyLineCoordinates;
     };
    

	 $scope.addToGroupTagConfiguration = function(tagConfiguration) {
		 var isFound = false;
		 for (var i = 0; i < $scope.groupTagConfiguration.tagConfigurations.length; ++i ) {
			 if (  $scope.groupTagConfiguration.tagConfigurations[i].id == tagConfiguration.id ) {
				 isFound = true;
				 break;
			 }
			  
		 }
		 if (isFound == false) {
			 $scope.groupTagConfiguration.tagConfigurations.push(tagConfiguration)
		
			 if ( $scope.tagConfigurationIdsStrToPolyLine.length == 0) {
				 $scope.tagConfigurationIdsStrToPolyLine = tagConfiguration.id; 
			 } else {
				 $scope.tagConfigurationIdsStrToPolyLine += ("AND" + tagConfiguration.id);
			 } 
			 
			 selectMarker(tagConfiguration);
		 }
	 };
	  
	 $scope.clearCurrentSelection = function() {
		 $scope.sensorGridApi.selection.clearSelectedRows()
		 $scope.currentSelectedConfiguration = null;
		
		 unSelectCurrentMarker();
		 $scope.gridOptionsAlertHistory.data = $scope.alertHistoryAll;
		 
		 $scope.validateTagOnCriteria($scope.currentSelectedConfiguration, $scope.alertSetting.alertCriteria); 
	 };
    
	$scope.setColumnDefs = function() {
		var columnDefs = [
 		    {field:'name', displayName: $translate.instant('TAG_CONFIGURATION'), width : 250, 
		    	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.selectTagConfiguration(row.entity)"><a>{{row.entity.name}}</a></div>'},
		];
		
		$scope.tagConfigurationGridOptions = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.selectTagConfiguration = function(tagConfiguration) {
		highlightMarker(tagConfiguration);
	}
	
	
	$scope.$on('updateLanguageInUiGrid', function() {
		$scope.setColumnDefs();  
		$scope.tagConfigurationGridOptions.columnDefs = $scope.tagConfigurationGridOptions;
	});

	$scope.tagConfigurationGridOptions={
		//data: $scope.tagConfiguration.sensorConfigs,           
		rowHeight: 30,
		minRowsToShow: 5,
		enableFiltering: false,
		enableRowSelection: true,
		onRegisterApi: function(gridApi){
	        $scope.sensorGridApi = gridApi;
	    },
		columnDefs: $scope.tagConfigurationGridOptions

	};


	$scope.onFileChange = function(e) {
		$scope.file = e.srcElement.files[0];
		$scope.image = $scope.file;
		
		var fullPath = document.getElementById('file').value;
		if (fullPath) {
		    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
		    var filename = fullPath.substring(startIndex);
		    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
		    	$scope.fileName = filename.substring(1);
		    	document.getElementById("hiddenImage").style.display = "none";
		    }
		}
	};

	$scope.deleteImage = function() {
		$scope.fileName = $scope.tagConfiguration.id == 0 ?  '' : '<deleted_@>';
		$scope.image = null;
		$scope.tagConfiguration.imageUrlPath = "";
		document.getElementById("hiddenImage").style.display = "none";
	}
	
	$scope.isSensorTypeSelected = false;
	$scope.toggleSensorType = function(coverLevel, waterLevel, h2s, so2, co2, ch4, nh3, o2) {
		$scope.isSensorTypeSelected = (coverLevel || waterLevel || h2s || so2 || co2 || ch4 || nh3 || o2);
	};
  
	$scope.loadMap = function(siteConfiguration, clearMarker) {
    	if(clearMarker) {
    		clearMarkers();
    		$scope.tagConfiguration.lat = null;
    	    $scope.tagConfiguration.lon = null;
    	    $scope.tagConfiguration.zoomLevel = null;
    	}
    	panZoomMap(siteConfiguration);
    }
    
    $scope.createTagGroupConfigurationOk = function () {
		var validationObject = new Object(); 


		if ($scope.groupTagConfiguration.id == 0) {
			validationObject.validationSubject = "Create Group Tag Configuration ";
		} else {
			validationObject.validationSubject = "Update Group Tag Configuration ";
		}

    	if ($scope.groupTagConfiguration.tagConfigurations.length == 0) {
    		validationObject.validationMessages = "Please add a tag to the group configuration";
    		appMainService.displayMessageResult(validationObject, true);
    		return;
 		}
		
		var polyArray = getPolyArray();
		
		var polyLineStringArray = "";
		for (var i = 0; i < polyArray.length; ++i) {
			var polyLine = polyArray[i];
			var polyLineString = $scope.generatePolyLineJson(polyLine, i);
			polyLineStringArray += polyLineString;
			if ( i != polyArray.length - 1 ) {
				polyLineStringArray += '&&'
			}
		}
		
		var tagConfigurationStringArray = $scope.generateTagConfigurationString($scope.groupTagConfiguration.tagConfigurations);
		
		var fd = new FormData();
    	fd.append('id', $scope.groupTagConfiguration.id);
    	fd.append('name', $scope.groupTagConfiguration.name); 
    	fd.append('siteId', $scope.groupTagConfiguration.siteConfiguration.id);
		fd.append('tagConfigurationIds', tagConfigurationStringArray);
		fd.append('polyLine', polyLineStringArray);
 		$http.post(appMainService.getDomainAddress() + '/rest/groupTagConfiguration/addData', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
    		appMainService.displayMessageResult(response.data, true);
        	$location.path('SystemConfiguration/TagConfigurationGroupView');
    		//$scope.userCancel();
        },function (response){
			  alert("Failed to add tag configuration, status=" + response.status);
	    });
	};

	$scope.removeTagGroupConfiguration = function (){
		$http({
            url: appMainService.getDomainAddress() + '/rest/tagConfiguration/deleteData/',
            method: 'DELETE',
            params: {
            	id: $scope.tagConfiguration.id
            }
        }).then(function (response) {
    		appMainService.displayMessageResult(response.data, true);
      	    $location.path('SystemConfiguration/TagConfiguration');
        },function (response){
  		  alert("Failed to delete tag configuration, status=" + response.status);
        });
	};
	
	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};



	$scope.getSensorTableStyle= function() {
        return {
            height: ($scope.numberOfRowsInt * $scope.rowHeight  ) + "px"
        };
    };
    
	
	$scope.generatePolyLineJson = function (polyLine, index) {
		
		var pathArray = polyLine.getPath().getArray();
		
		var polyLineJson = '{"polyLine":[';
		for (var i = 0; i < pathArray.length; ++i) {
			
			var latLng = pathArray[i];
			var lat = latLng.lat();
			var lng = latLng.lng();
			
 			var latValue = '"lat":"' + lat + '"'; 
 			var lngValue = '"lng":"' + lng + '"'; 
 			var tagConfigurationIds = '"tagConfigurationIds":"' + $scope.tagConfigurationIdsStrToPolyLineArray[index] + '"'; 
 				
 				
			var coordinates = '{' + latValue + ',' + lngValue + ',' + tagConfigurationIds + '}';

			if ( i != pathArray.length - 1 ) {
				coordinates += ','
			}
			polyLineJson += coordinates;
		}
		polyLineJson += ']}';
		return polyLineJson;
	};
	
	$scope.generateTagConfigurationString = function(tagConfigurations) {
		
		var tagConfigurationsStr = "";
		for (var i = 0; i < tagConfigurations.length; ++i) {
			var tagConfigurationId = tagConfigurations[i].id;
 			tagConfigurationsStr += tagConfigurationId;
			if ( i != tagConfigurations.length - 1 ) {
				tagConfigurationsStr += ','
			}
		}
		return tagConfigurationsStr;
	}
	
	$scope.setMarkerPosition = function(location, zoomLevel) {
		$scope.tagConfiguration.lat = location.lat();
	    $scope.tagConfiguration.lon = location.lng();
	    $scope.tagConfiguration.zoomLevel = zoomLevel;
 	};
 	
 	$scope.toggleFlag = function (readerInSequence, readerWithHighestRssi, assignToSpecificReaders ) {
 		if ( readerInSequence == true) {
 			$scope.tagConfiguration.readerWithHighestRssi = false;
	 		$scope.tagConfiguration.assignToSpecificReaders = false;
 		}
 		
 		if ( readerWithHighestRssi == true) {
	 		$scope.tagConfiguration.readerInSequence = false;
 			$scope.tagConfiguration.assignToSpecificReaders = false;
 		}
 		
 		if ( assignToSpecificReaders == true) {
	 		$scope.tagConfiguration.readerInSequence = false;
 			$scope.tagConfiguration.readerWithHighestRssi = false;
 		}

 		if ( $scope.tagConfiguration.readerInSequence == false 
 				&& $scope.tagConfiguration.readerWithHighestRssi == false 
 				&& $scope.tagConfiguration.assignToSpecificReaders == false) {
	 		$scope.tagConfiguration.readerInSequence = true;
 			$scope.tagConfiguration.readerWithHighestRssi = false;
	 		$scope.tagConfiguration.assignToSpecificReaders = false;
 		}
 	};
 }]);



