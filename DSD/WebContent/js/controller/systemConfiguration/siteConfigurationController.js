angular.module('dsd').controller('siteConfigurationCtrl', ['$scope', '$translate', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', '$window',
 function ($scope, $translate, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, $window) {
	$scope.rowHeight = 120; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 2;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows);
	
	$scope.section="";
	$scope.users=[
	];
	$scope.roles=[
	];
	$scope.modules = [];
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.userTotalItems = 100;
	$scope.userCurrentPage = 1;
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
		  
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	
	$scope.getTableStyle= function() {
        return {
        	height: (($scope.numberOfRows +  1)* $scope.gridOptions.rowHeight  ) + "px"
        };
    };
	
	
	
	$scope.changeLanguage=function(locale) {
		alert(locale);
	};
	
	$scope.toggleFiltering = function(){
		$scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
	    $scope.siteConfigurationGridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	 
	};

    //System Settings
    $scope.showSystemSetting = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'view/systemSettingView.html',
			controller: 'SystemSettingCtrl',
			size: 'lg',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
    
    
    
    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });
 
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.persons = {currentPage : 1};
	
	$scope.siteConfigurationTotalItems = 100;
	$scope.siteConfigurationCurrentPage = 1;
	$scope.siteConfigurations=[];
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
 			{field:'name', width: 200, displayName: $translate.instant('SITE_NAME'),  
 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openSiteConfiguration(row.entity)"><a>{{row.entity.name}}</a></div>'},
 			{field:'description', width: 400, displayName: $translate.instant('DESCRIPTION'), 
 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openSiteConfiguration(row.entity)"><a>{{row.entity.description}}</a></div>'},
/*		 	{field:'siteMap', width: 300, displayName: $translate.instant('SITE_MAP'),   enableFiltering : false,
					cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openSiteConfiguration(row.entity)"><img style="margin-left: auto;margin-right: auto; display: block;" ng-src="{{row.entity.imageUrlPath}}" height="120"></div>'},*/
		];
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
		
    
	$scope.gridOptions={
		data:'siteConfigurations', 
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		useExternalFiltering: true,
		multiSelect: false,
		enableRowSelection: true,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		rowHeight: $scope.rowHeight,
		enableRowHeaderSelection: false,
		enableColumnResizing : true,
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	  
		onRegisterApi: function(gridApi){
			$scope.siteConfigurationGridApi = gridApi;
			$scope.siteConfigurationGridApi.core.on.filterChanged( $scope, function() {
				var grid = this.grid;
				var id = grid.columns.filter(function (item){
					return item.field == "id";
				})[0].filters[0].term;
				var name = grid.columns.filter(function (item){
					return item.field == "name";
				})[0].filters[0].term;
				var description = grid.columns.filter(function (item){
					return item.field == "description";
				})[0].filters[0].term;
							
				$scope.searchSiteConfiguration(id, name, description);
		    });
		},
		columnDefs: $scope.columnDefs
	};
	
	$scope.openSiteConfiguration = function (siteConfiguration) {
		appMainService.setSiteConfiguration(siteConfiguration);
		$location.path('SystemConfiguration/SiteConfiguration/New/');    		
	};
	
    $scope.searchSiteConfiguration = function (searchId, name, description) {
    	$http({
    		url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
    		method: 'GET',
            params: {
            id: searchId, 
            name: name, 
            description: description,
            defaultSite: null
           }
    	}).then(function (response) {
    		$scope.siteConfigurations = response.data;
    	},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
    };

	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
}]);

angular.module('dsd').controller('siteConfigurationCreateCtrl', ['$scope', '$routeParams', '$http', '$location' , 'appMainService',
                                                           function ($scope, $routeParams, $http, $location, appMainService){
     
	$scope.fileName = '';
	$scope.image = null; 
	$scope.subName = "NEW_RECORD";
	
	$scope.siteConfiguration = appMainService.getSiteConfiguration(); 
	if ($scope.siteConfiguration != null) {
		$scope.subName = "EDIT_RECORD";
		$scope.siteConfiguration.oldDefaultSite = $scope.siteConfiguration.defaultSite;
	} else {
		$scope.siteConfiguration = new Object();
		$scope.siteConfiguration.id = 0;
		$scope.siteConfiguration.description = "";
		$scope.siteConfiguration.lat = 22.2855200;
		$scope.siteConfiguration.lon = 114.1576900;
		$scope.siteConfiguration.zoomLevel = 11;
	}
		
	$scope.getSiteConfiguration = function () {
		return $scope.siteConfiguration;
	}
	
 	$scope.createSiteConfigurationOk = function () {

 		setSiteConfiguration($scope.siteConfiguration);
 		
 		var fd = new FormData();

 		if ($scope.subName == "EDIT_RECORD")
 			fd.append('id', $scope.siteConfiguration.id); 
 		
 		fd.append('name', $scope.siteConfiguration.name); 
 		fd.append('description', $scope.siteConfiguration.description); 
 		fd.append('defaultSite', $scope.siteConfiguration.defaultSite); 
		fd.append('lat', $scope.siteConfiguration.lat); 
		fd.append('lon', $scope.siteConfiguration.lon); 
		fd.append('zoomLevel', $scope.siteConfiguration.zoomLevel); 
 		
 		if ( $scope.fileName == null) {
 			fd.append('fileName', null);
 			fd.append('file', null);
 		} else {
 			fd.append('fileName', $scope.fileName);
 		 	fd.append('file', $scope.image);
 		} 
 		
 		if ($scope.subName == "EDIT_RECORD") {
	 		$http.post(appMainService.getDomainAddress() + '/rest/siteConfiguration/updateData', fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        }).then(function (response) {
	      	    appMainService.displayMessageResult(response.data, true);
	      	    $location.path('SystemConfiguration/SiteConfiguration');
		    },function (response){
				  alert("Failed to add site configuration, status=" + response.status);
		    });
 		} else {
	 		$http.post(appMainService.getDomainAddress() + '/rest/siteConfiguration/addData', fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        }).then(function (response) {
	      	    appMainService.displayMessageResult(response.data, true);
	      	    $location.path('SystemConfiguration/SiteConfiguration');
		    },function (response){
				  alert("Failed to add site configuration, status=" + response.status);
		    });			
 		}
	};

	$scope.removeSiteConfiguration = function (){
		$http({
            url: appMainService.getDomainAddress() + '/rest/siteConfiguration/deleteData/',
            method: 'DELETE',
            params: {
            	id: $scope.siteConfiguration.id
            }
        }).then(function (response) {
      	    appMainService.displayMessageResult(response.data, true);
      	    $location.path('SystemConfiguration/SiteConfiguration');
        },function (response){
  		  	alert("Failed to delete site configuration, status=" + response.status);
        });
	};
	
	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
 }]);


