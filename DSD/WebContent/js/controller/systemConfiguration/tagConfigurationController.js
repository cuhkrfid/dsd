angular.module('dsd').controller('tagConfigurationCtrl', ['$scope', '$translate', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', '$window',
 function ($scope, $translate, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, $window) {
	$scope.rowHeight = 120; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 2;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows);
	
	$scope.section="";
	$scope.users=[
	];
	$scope.roles=[
	];
	$scope.modules = [];
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.userTotalItems = 100;
	$scope.userCurrentPage = 1;
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
		  
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	$scope.getTableStyle= function() {
        return {
        	height: (($scope.numberOfRows +  1)* $scope.gridOptions.rowHeight  ) + "px"
        };
    };
	
	
	$scope.changeLanguage=function(locale) {
		alert(locale);
	};
	
	$scope.toggleFiltering = function(){
		$scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
	    $scope.tagConfigurationGridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	 
	};

	
    //System Settings
    $scope.showSystemSetting = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'view/systemSettingView.html',
			controller: 'SystemSettingCtrl',
			size: 'lg',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
    
    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });
 
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.persons = {currentPage : 1};
	
	

	$scope.removeTagConfiguration = function (row){
		var path = 'SystemConfiguration/TagConfiguration/Delete/';
		path += (row.entity.id+"");
		$location.path(path);
	};
	
	$scope.tagConfigurationTotalItems = 100;
	$scope.tagConfigurationCurrentPage = 1;
	$scope.tagConfigurations=[];
	
	$scope.siteConfigurations = [];
	$scope.selectedSite = appMainService.getSiteConfiguration();
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
			{field:'name', displayName: $translate.instant('TAG_NAME'), width: 160, headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openTagConfiguration(row.entity)"><a>{{row.entity.name}}</a></div>'},
			{field:'deviceId',  width: 80, displayName: $translate.instant('DEVICE_ID'), headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openTagConfiguration(row.entity)"><a>{{row.entity.deviceId}}</a></div>'},
			{field:'sensorTypeHtml', displayName:$translate.instant('SENSOR_TYPE'), width: 200, headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" style="overflow-y:scroll;height:120px;" ng-click="grid.appScope.openTagConfiguration(row.entity)"><div ng-repeat="item in row.entity.sensorTypeHtml">{{item}}</div></div>'},
			{field:'validateTagResult', displayName:$translate.instant('VALIDATE_TAG_RESULT'), width: 140, enableFiltering : false, headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openTagConfiguration(row.entity)"><img ng-src="{{row.entity.validateTagImagePath}}" width="48" height="48" ng-click="grid.appScope.deleteSensorConfig(row.entity.id)"></img></div>'},
			{field:'deviceImage', displayName: $translate.instant('IMAGE'), enableFiltering : false,  width: 200, headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" style="overflow-y:scroll;height:120px;" ng-click="grid.appScope.openTagConfiguration(row.entity)"><img ng-src="{{row.entity.imageUrlPath}}" ng-if="row.entity.imageUrlPath!=null" height="110"></div>'},
			{field:'siteConfiguration.name', displayName: $translate.instant('SITE_NAME'), width: 160, headerCellClass: 'uiGridthemeHeaderColor',
			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openTagConfiguration(row.entity)"><a>{{row.entity.siteConfiguration.name}}</a></div>'},
//			{field:'siteMap', displayName: $translate.instant('SITE_MAP'),  enableFiltering : false,  width: 160,
//			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openTagConfiguration(row.entity)"><img ng-src="{{row.entity.siteConfiguration.imageUrlPath}}" width="160"></div>'}
		];	
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.$on('updateLanguageInUiGrid', function() {
		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: false,
		enableRowSelection: true,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		rowHeight: $scope.rowHeight,
		enableRowHeaderSelection: false,
		enableColumnResizing : true,
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {
		    });
		},
		columnDefs: $scope.columnDefs
	};
	
	$scope.getTableStyle= function() {
        return {
        	height: (($scope.numberOfRows +  1)* $scope.gridOptions.rowHeight  ) + "px"
        };
    };
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	$scope.configureAlert = function (tagConfiguration) {
		var path = '/Alert/AlertSetting/New/';
		appMainService.setTagConfiguration(tagConfiguration);
		$location.path(path);    		
	};
	
	$scope.openTagConfiguration = function (tagConfiguration) {
		var path = 'SystemConfiguration/TagConfiguration/New/';
		appMainService.setTagConfiguration(tagConfiguration);
		appMainService.setSiteConfiguration($scope.selectedSite);
		$location.path(path);    		
	};
	
	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
	$scope.validating = false; 
	
	$scope.validateTagConfiguration = function(selectedSite) {
		$scope.validating = true; 
		$http({
    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/validateTagConfiguration/',
    		method: 'GET',
    		params: {
    			siteId : selectedSite.id
    		}
        }).then(function (response) {
    		$scope.tagConfigurations = response.data;
    		$scope.gridOptions.data = $scope.tagConfigurations;
    		
    		
    	},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
		$scope.validating = false; 
	};
	
	$http({
		url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
		method: 'GET',
		headers : { 'Content-Type' : 'application/json',    
		      		'Authorization': 'Bearer myToken1234' },
	    params: {
        id: null, 
        name: null, 
        description: null,
        defaultSite: null
       }
	}).then(function (response) {
		$scope.siteConfigurations = response.data;
		if ($scope.selectedSite != null) {
			for (var i = 0; i < $scope.siteConfigurations.length; ++i) {
				if ( $scope.siteConfigurations[i].id == $scope.selectedSite.id ) {
					$scope.selectedSite = $scope.siteConfigurations[i];
					$scope.validateTagConfiguration($scope.selectedSite);
					break;
				}
			}
		} else {
			if ($scope.siteConfigurations.length > 0) {
				$scope.selectedSite = $scope.siteConfigurations[0];
				$scope.validateTagConfiguration($scope.selectedSite);
			}
		}
		
	},function (response){
 		alert("Failed to find site configuration list, status=" + response.status);
	});
	
	$scope.searchTag = function (siteId) {
		$scope.validateTagConfiguration(siteId);
	}; 
}]);


angular.module('dsd').controller('tagConfigurationCreateCtrl', ['$scope', '$translate','$interval', '$timeout','$routeParams', '$http', '$location' , 'appMainService','ngDialog',
                                                         function ($scope, $translate, $interval, $timeout, $routeParams, $http, $location, appMainService, ngDialog){

	$scope.siteConfigurations = [];
	$scope.markers = [];
	
	$scope.fileName = '';
	$scope.image = null; 
	$scope.loadSiteConfigurations = function() {
		$http({
			url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
			method: 'GET',
	        params: {
	        id: null, 
	        name: null, 
	        description: null,
            defaultSite: null
	       }
		}).then(function (response) {
			$scope.siteConfigurations = response.data;
			for (var i = 0; i < $scope.siteConfigurations.length; ++i) {
				if ( $scope.siteConfigurations[i].id == $scope.tagConfiguration.siteConfigurationId) {
					$scope.tagConfiguration.siteConfiguration = $scope.siteConfigurations[i];
					$scope.loadMap($scope.siteConfigurations[i], false);
					break;
				}
			}
		},function (response){
	 		alert("Failed to find site configuration list, status=" + response.status);
		});
	}; 
	

	$scope.setColumnDefs = function() {
		var columnDefs = [
  			{field:'delete', displayName: $translate.instant('DELETE'), width: 90,
		 			cellTemplate: '<img src="img/normal/delete.png" width="24" height="24" ng-click="grid.appScope.deleteSensorConfig(row.entity.sensorId)"></img>'},
		    {field:'sensorType', displayName: $translate.instant('SENSOR_TYPE'), width : 280, 
		    	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.editSensorConfig(row.entity.sensorId)"><a>{{row.entity.sensorType}}</a></div>'},
		];
		
		var columnDefs2 = [
   	   	    {field:'action', displayName: $translate.instant('CORRECT'), width : 80, 
  		    	cellTemplate: '<img src="img/basic/Checkmark-48.png" width="24" height="24" ng-click="grid.appScope.correctAction(row.entity)"></img>'},
		    {field:'message', displayName: $translate.instant('TAG_VALIDATION_MESSAGE'), width : 300, 
  		    	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a>{{row.entity.message}}</a></div>'},
  		];
		
		var columnDefs3 = [
  	   	    {field:'sequenceNumber', displayName: $translate.instant('SEQUENCE_NO'), width : 120, 
 		    	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a>{{row.entity.message}}</a></div>'},
 		    {field:'readerId', displayName: $translate.instant('READER_ID'), width : 120, 
 		    	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a>{{row.entity.message}}</a></div>'},
 		];
		
		$scope.sensorGridOptions = columnDefs;
		$scope.validationGridOptions = columnDefs2; 
		$scope.readerGridOptions = columnDefs3;
		
	};
	$scope.setColumnDefs();
	
	$scope.correctAction = function(validationMessage) {
		//alert(validationMessage.messageType + ":" + validationMessage.sensorAction + ":" + validationMessage.sensorId);
		if ( validationMessage.messageType == "deviceIdNotFound" ||  validationMessage.messageType == "deviceIdTimeout") {
			$scope.tagConfiguration.deviceId = "";
		}
		
		if ( validationMessage.messageType == "sensorIdNotFound") {
			if (validationMessage.sensorAction == "remove") {
				for (var i = 0; i < $scope.sensorGridOptions.data.length; ++i) {
					if ( $scope.sensorGridOptions.data[i].sensorId == validationMessage.sensorId ) {
						$scope.sensorGridOptions.data.splice(i,1);
						break;
					}
				}
				
			} else if (validationMessage.sensorAction == "add") {
				var sensorConfig = new Object();
				sensorConfig.id = 0;
				sensorConfig.sensorId = validationMessage.sensorId;
				
				sensorConfig.isCoverLevel = validationMessage.isCoverLevel; 
				sensorConfig.isWaterLevel = validationMessage.isWaterLevel;
				sensorConfig.isH2s = validationMessage.isH2s;
				sensorConfig.isSo2 = validationMessage.isSo2;
				sensorConfig.isCo2 = validationMessage.isCo2;
				sensorConfig.isCh4 = validationMessage.isCh4;
				sensorConfig.isNh3 = validationMessage.isNh3;
				sensorConfig.isO2 = validationMessage.isO2;
				
				var sensorTypeHtmlMsg = "";
				if ( sensorConfig.isCoverLevel)
					sensorTypeHtmlMsg += "Cover level, ";
				if ( sensorConfig.isWaterLevel)
					sensorTypeHtmlMsg += "Water level, ";
				if ( sensorConfig.isH2s)
					sensorTypeHtmlMsg += "h2s, ";
				if ( sensorConfig.isSo2)
					sensorTypeHtmlMsg += "so2, ";
				if ( sensorConfig.isCo2)
					sensorTypeHtmlMsg += "co2, ";
				if ( sensorConfig.isCh4)
					sensorTypeHtmlMsg += "ch4, ";
				if ( sensorConfig.isNh3)
					sensorTypeHtmlMsg += "nh3, ";
				if ( sensorConfig.isO2)
					sensorTypeHtmlMsg += "o2, ";
				
    			sensorConfig.sensorType  = sensorTypeHtmlMsg.substr(0,sensorTypeHtmlMsg.length-2);
    			$scope.sensorGridOptions.data.push(sensorConfig);
			}
		}
		if ( validationMessage.messageType == "sensorTypeNotFound") {
			if (validationMessage.sensorAction == "update") {
				for (var i = 0; i < $scope.sensorGridOptions.data.length; ++i) {
					if ( $scope.sensorGridOptions.data[i].sensorId == validationMessage.sensorId ) {
						var sensorConfig = $scope.sensorGridOptions.data[i];

						if ( validationMessage.isCoverLevel)
							sensorConfig.isCoverLevel = !sensorConfig.isCoverLevel;
						if ( validationMessage.isWaterLevel)
							sensorConfig.isWaterLevel = !sensorConfig.isWaterLevel;
						if ( validationMessage.isH2s)
							sensorConfig.isH2s = !sensorConfig.isH2s;
						if ( validationMessage.isSo2)
							sensorConfig.isSo2 = !sensorConfig.isSo2;
						if ( validationMessage.isCo2)
							sensorConfig.isCo2 = !sensorConfig.isCo2;
						if ( validationMessage.isCh4)
							sensorConfig.isCh4 = !sensorConfig.isCh4;
						if ( validationMessage.isNh3)
							sensorConfig.isNh3 = !sensorConfig.isNh3;
						if ( validationMessage.isO2)
							sensorConfig.isO2 = !sensorConfig.isO2;
						
						var sensorTypeHtmlMsg = "";
						if ( sensorConfig.isCoverLevel)
							sensorTypeHtmlMsg += "Cover level, ";
						if ( sensorConfig.isWaterLevel)
							sensorTypeHtmlMsg += "Water level, ";
						if ( sensorConfig.isH2s)
							sensorTypeHtmlMsg += "h2s, ";
						if ( sensorConfig.isSo2)
							sensorTypeHtmlMsg += "so2, ";
						if ( sensorConfig.isCo2)
							sensorTypeHtmlMsg += "co2, ";
						if ( sensorConfig.isCh4)
							sensorTypeHtmlMsg += "ch4, ";
						if ( sensorConfig.isNh3)
							sensorTypeHtmlMsg += "nh3, ";
						if ( sensorConfig.isO2)
							sensorTypeHtmlMsg += "o2, ";
						
						sensorConfig.sensorType  = sensorTypeHtmlMsg.substr(0,sensorTypeHtmlMsg.length-2);
						break;
					}
				}
				
			}
		} 
		
		for (var i = 0; i < $scope.validationGridOptions.data.length; ++i) {
			if ( $scope.validationGridOptions.data[i].id == validationMessage.id ) {
				$scope.validationGridOptions.data.splice(i,1);
				break;
			}
		}
	};
	
	$scope.$on('updateLanguageInUiGrid', function() {
		$scope.setColumnDefs();  
		$scope.sensorGridOptions.columnDefs = $scope.sensorGridOptions;
		$scope.validationGridOptions.columnDefs = $scope.validationGridOptions;
		$scope.readerGridOptions.columnDefs = $scope.readerGridOptions;
	});

	$scope.sensorGridOptions={
		//data: $scope.tagConfiguration.sensorConfigs,           
		rowHeight: 30,
		minRowsToShow: 5,
		enableFiltering: false,
		enableRowSelection: true,
		onRegisterApi: function(gridApi){
	        $scope.sensorGridApi = gridApi;
	    },
		columnDefs: $scope.sensorGridOptions
	};
	
	$scope.readerGridOptions={
		//data: $scope.tagConfiguration.sensorConfigs,           
		rowHeight: 30,
		minRowsToShow: 5,
		enableFiltering: false,
		enableRowSelection: true,
		enableHorizontalScrollbar: 0,
		enableVerticalScrollbar: 0,
		onRegisterApi: function(gridApi){
	        $scope.sensorGridApi = gridApi;
	    },
		columnDefs: $scope.readerGridOptions
	};
	$scope.getReaderTableStyle= function() {
        return {
            height: (5 * $scope.readerGridOptions.rowHeight  ) + "px"
        };
    };
	
	$scope.tagConfiguration = appMainService.getTagConfiguration(); 
	if ($scope.tagConfiguration != null) {
		$http({
    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
    		method: 'GET',
            params: {
            id: null, 
            name: null, 
            deviceId : $scope.tagConfiguration.deviceId,
            deviceAddress : null,
            groupId : null,
            description: null,
            siteName : null,
            siteId : null,
           }
    	}).then(function (response) {
    		if (response.data != null && response.data.length > 0) {
    			$scope.tagConfiguration = response.data[0];
    		}
    		for (var i = 0; i < $scope.tagConfiguration.sensorConfigs.length; ++i) {
    			var sensorTypeHtmlMsg = "";
    			var value = $scope.tagConfiguration.sensorConfigs[i];

    			if ( value.isCoverLevel)
					sensorTypeHtmlMsg += "Cover level, ";
				if ( value.isWaterLevel)
					sensorTypeHtmlMsg += "Water level, ";
				if ( value.isH2s)
					sensorTypeHtmlMsg += "h2s, ";
				if ( value.isSo2)
					sensorTypeHtmlMsg += "so2, ";
				if ( value.isCo2)
					sensorTypeHtmlMsg += "co2, ";
				if ( value.isCh4)
					sensorTypeHtmlMsg += "ch4, ";
				if ( value.isNh3)
					sensorTypeHtmlMsg += "nh3, ";
				if ( value.isO2)
					sensorTypeHtmlMsg += "o2, ";

    			value.sensorType  = sensorTypeHtmlMsg.substr(0,sensorTypeHtmlMsg.length-2);
    		}
    		$scope.sensorGridOptions.data = $scope.tagConfiguration.sensorConfigs;
    		$scope.validationGridOptions.data = $scope.tagConfiguration.validationMessageObjects;
    		
    		$scope.subName = $scope.tagConfiguration.id == 0 ?  "NEW_RECORD" : "EDIT_RECORD";
    		$scope.loadSiteConfigurations();
    		
    	},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
			
	} else {
		$scope.tagConfiguration = new Object();
		
		$scope.tagConfiguration.id = 0;
		$scope.tagConfiguration.measurePeriod = 5;
		$scope.tagConfiguration.sensorConfigs = [];
		$scope.tagConfiguration.vibrationThreshold = 1;
		$scope.tagConfiguration.vibration = false;
		$scope.tagConfiguration.siteConfigurationId = 0;
		$scope.tagConfiguration.lat = null;
		$scope.tagConfiguration.lon = null;
		$scope.tagConfiguration.zoomLevel = null;
		$scope.tagConfiguration.readerInSequence = true;
		$scope.tagConfiguration.readerInSequenceInMs = 2000;
		$scope.tagConfiguration.readerWithHighestRssi = false; 
	    $scope.tagConfiguration.readerWithHighestRssiInMs = 2000; 
	    $scope.tagConfiguration.assignToSpecificReaders = false;
		$scope.tagConfiguration.assignToSpecificReadersInMs = 2000; 
		//$scope.tagConfiguration.assignToSpecificReadersConfigs = [];
		$scope.tagConfiguration.description = "";
		$scope.tagConfiguration.groupId = 1;
		$scope.subName = "NEW_RECORD";
		$scope.tagConfiguration.createDateTime = null;
		$scope.loadSiteConfigurations();
	}
	
	$scope.onFileChange = function(e) {
		$scope.file = e.srcElement.files[0];
		$scope.image = $scope.file;
		
		var fullPath = document.getElementById('file').value;
		if (fullPath) {
		    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
		    var filename = fullPath.substring(startIndex);
		    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
		    	$scope.fileName = filename.substring(1);
		    	document.getElementById("hiddenImage").style.display = "none";
		    }
		}
	};

	$scope.deleteImage = function() {
		$scope.fileName = $scope.tagConfiguration.id == 0 ?  '' : '<deleted_@>';
		$scope.image = null;
		$scope.tagConfiguration.imageUrlPath = "";
		document.getElementById("hiddenImage").style.display = "none";
	}
	
	$scope.isSensorTypeSelected = false;
	$scope.toggleSensorType = function(coverLevel, waterLevel, h2s, so2, co2, ch4, nh3, o2) {
		$scope.isSensorTypeSelected = (coverLevel || waterLevel || h2s || so2 || co2 || ch4 || nh3 || o2);
	};
  
	$scope.loadMap = function(siteConfiguration, clearMarker) {
    	if(clearMarker) {
    		clearMarkers();
    		$scope.tagConfiguration.lat = null;
    	    $scope.tagConfiguration.lon = null;
    	    $scope.tagConfiguration.zoomLevel = null;
    	}
    	panZoomMap(siteConfiguration);
    }
    
	$scope.setFromDate = function(date) {
		$scope.tagConfiguration.createDateTime = new Date(date).toString();
	};
	
    $scope.createTagConfigurationOk = function () {
		var validationObject = new Object(); 
		if ($scope.tagConfiguration.id == 0) {
			validationObject.validationSubject = "Create Tag Configuration ";
		} else {
			validationObject.validationSubject = "Update Tag Configuration ";
			
		}

    	if ($scope.tagConfiguration.lat == null || $scope.tagConfiguration.lon == null || $scope.tagConfiguration.zoomLevel == null) {
    		validationObject.validationMessages = "Please select a location for the tag";
    		appMainService.displayMessageResult(validationObject, true);
    		return;
 		}

    	if ($scope.tagConfiguration.sensorConfigs.length == 0) {
    		validationObject.validationMessages = "Please add at least one sensor in the tag";
    		appMainService.displayMessageResult(validationObject, true);
    		return;    		
    	}
    
    	var fd = new FormData();
    	fd.append('id', $scope.tagConfiguration.id);
    	fd.append('deviceAddress', $scope.tagConfiguration.deviceAddress);
 		fd.append('deviceId', $scope.tagConfiguration.deviceId); 
 		fd.append('name', $scope.tagConfiguration.name); 
 	 	fd.append('description', $scope.tagConfiguration.description); 
 	 	fd.append('groupId', $scope.tagConfiguration.groupId); 
 		fd.append('measurementPeriod', $scope.tagConfiguration.measurePeriod); 
 		fd.append('siteId', $scope.tagConfiguration.siteConfiguration.id); 
 		
 		fd.append('lat', $scope.tagConfiguration.lat); 
 		fd.append('lon', $scope.tagConfiguration.lon); 
 		fd.append('zoomLevel', $scope.tagConfiguration.zoomLevel); 
 	 	
 		fd.append('readerInSequence', $scope.tagConfiguration.readerInSequence); 
 		fd.append('readerInSequenceInMs', $scope.tagConfiguration.readerInSequenceInMs); 
 		fd.append('readerWithHighestRssi', $scope.tagConfiguration.readerWithHighestRssi); 
 		fd.append('readerWithHighestRssiInMs', $scope.tagConfiguration.readerWithHighestRssiInMs); 
 		fd.append('assignToSpecificReaders', $scope.tagConfiguration.assignToSpecificReaders); 
 		fd.append('assignToSpecificReadersInMs', $scope.tagConfiguration.assignToSpecificReadersInMs); 
 		fd.append('assignToSpecificReadersConfigs', $scope.tagConfiguration.assignToSpecificReadersConfigs); 
 		
 		if ( $scope.fileName == null) {
 			fd.append('fileName', null);
 			fd.append('file', null);
 			fd.append('imageUriBase64', null);
 		} else {
 			fd.append('fileName', $scope.fileName);
 		 	fd.append('file', $scope.image);
 			fd.append('imageUriBase64', null);
 		} 
 	
 		fd.append('manHoleId', $scope.tagConfiguration.manHoleId); 
 		fd.append('manHoleHeight', $scope.tagConfiguration.manHoleHeight); 
 
		if ( $scope.tagConfiguration.createDateTime != null) {
 			var id = $scope.tagConfiguration.createDateTime.indexOf('GMT');
 			if ( id >= 0) {
 				$scope.tagConfiguration.createDateTime = $scope.tagConfiguration.createDateTime.substring(0, id-1);
 			}
 			fd.append('createDateTime', $scope.tagConfiguration.createDateTime.toString());
 		}
 		
 		var sensorConfigsStr = $scope.generateSensorConfigsJson($scope.tagConfiguration.sensorConfigs, $scope.tagConfiguration.deviceId);
 		fd.append('sensorConfigs', sensorConfigsStr);
 		
 		$http.post(appMainService.getDomainAddress() + '/rest/tagConfiguration/addData', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
    		appMainService.displayMessageResult(response.data, true);
        	$location.path('SystemConfiguration/TagConfiguration');
    		//$scope.userCancel();
        },function (response){
			  alert("Failed to add tag configuration, status=" + response.status);
	    });
	};

	$scope.removeTagConfiguration = function (){
		$http({
            url: appMainService.getDomainAddress() + '/rest/tagConfiguration/deleteData/',
            method: 'DELETE',
            params: {
            	id: $scope.tagConfiguration.id
            }
        }).then(function (response) {
    		appMainService.displayMessageResult(response.data, true);
      	    $location.path('SystemConfiguration/TagConfiguration');
        },function (response){
  		  alert("Failed to delete tag configuration, status=" + response.status);
        });
	};
	
	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};


	$scope.getValidationTableStyle= function() {
        var length = $scope.validationGridOptions.data.length; 
        return {
            height: (6 * $scope.validationGridOptions.rowHeight  ) + "px"
        };
    };
    
	$scope.validationGridOptions={
			//data: $scope.tagConfiguration.sensorConfigs,           
			rowHeight: 30,
			minRowsToShow: 5,
			enableFiltering: false,
			enableRowSelection: true,
			onRegisterApi: function(gridApi){
		        $scope.validationGridApi = gridApi;
		    },
			columnDefs: $scope.validationGridOptions
	};
	
	$scope.getSensorTableStyle= function() {
        var length = $scope.sensorGridOptions.data.length; 
        return {
            height: (5 * $scope.sensorGridOptions.rowHeight  ) + "px"
        };
    };
    
	$scope.addSensor = function() {
		$scope.sensorConfigTitle = 'NEW_RECORD';
		$scope.formData = new FormData();
		$scope.toggleSensorType(false, false, false, false, false, false, false, false);
		ngDialog.openConfirm({template: 'view/systemConfiguration/tagConfiguration/sensorConfigView.html',
			scope: $scope //Pass the scope object if you need to access in the template
		}).then(
			//Update
			function(value) {
				$scope.sensorDataId++;
						
				var sensorTypeHtmlMsg = "";
				if ( value.isCoverLevel)
					sensorTypeHtmlMsg += "Cover level, ";
				if ( value.isWaterLevel)
					sensorTypeHtmlMsg += "Water level, ";
				if ( value.isH2s)
					sensorTypeHtmlMsg += "h2s, ";
				if ( value.isSo2)
					sensorTypeHtmlMsg += "so2, ";
				if ( value.isCo2)
					sensorTypeHtmlMsg += "co2, ";
				if ( value.isCh4)
					sensorTypeHtmlMsg += "ch4, ";
				if ( value.isNh3)
					sensorTypeHtmlMsg += "nh3, ";
				if ( value.isO2)
					sensorTypeHtmlMsg += "o2, ";
				
				sensorTypeHtmlMsg = sensorTypeHtmlMsg.substr(0,sensorTypeHtmlMsg.length-2);
				
				var newSensorConfig = { 
						id : "new_" + $scope.sensorDataId,
						sensorId : $scope.tagConfiguration.deviceId,
						isCoverLevel : value.isCoverLevel,
						isWaterLevel : value.isWaterLevel,
						isH2s : value.isH2s,
						isSo2 : value.isSo2,
						isCo2 : value.isCo2,
						isCh4 : value.isCh4,
						isNh3 : value.isNh3,
						isO2 : value.isO2,
						sensorType : sensorTypeHtmlMsg
				};
				$scope.tagConfiguration.sensorConfigs.push(newSensorConfig);
				$scope.sensorGridOptions.data = $scope.tagConfiguration.sensorConfigs; 
			},
			//Cancel
			function(value) {
			}		
		);
	};
	
	$scope.deleteSensorConfig = function(sensorId) {
		for (var i = 0; i < $scope.tagConfiguration.sensorConfigs.length; ++i) {
			var sensorConfig = $scope.tagConfiguration.sensorConfigs[i];
			if (sensorConfig.sensorId == sensorId) {
				$scope.tagConfiguration.sensorConfigs.splice(i,1);
			}
		}
	};
	
	$scope.editSensorConfig = function(sensorId) {
		for (var i = 0; i < $scope.tagConfiguration.sensorConfigs.length; ++i) {
			var sensorConfig = $scope.tagConfiguration.sensorConfigs[i];
			
			if (sensorConfig.sensorId == sensorId) {
				$scope.sensorConfigTitle = 'EDIT_RECORD';
				
				$scope.formData = new FormData();
				$scope.formData.sensorId = sensorConfig.sensorId;
				$scope.formData.isCoverLevel = sensorConfig.isCoverLevel;
				$scope.formData.isWaterLevel = sensorConfig.isWaterLevel;
				$scope.formData.isH2s = sensorConfig.isH2s;
				$scope.formData.isSo2 = sensorConfig.isSo2;
				$scope.formData.isCo2 = sensorConfig.isCo2;
				$scope.formData.isCh4 = sensorConfig.isCh4;
				$scope.formData.isNh3 = sensorConfig.isNh3;
				$scope.formData.isO2 = sensorConfig.isO2;
				
				$scope.toggleSensorType(sensorConfig.isCoverLevel, sensorConfig.isWaterLevel, sensorConfig.isH2s, sensorConfig.isSo2, sensorConfig.isCo2, sensorConfig.isCh4, sensorConfig.isNh3, sensorConfig.isO2);
				
				ngDialog.openConfirm({template: 'view/systemConfiguration/tagConfiguration/sensorConfigView.html',
					scope: $scope //Pass the scope object if you need to access in the template
				}).then(
					//Update
					function(value) {
						var sensorTypeHtmlMsg = "";
						if ( value.isCoverLevel)
							sensorTypeHtmlMsg += "Cover level, ";
						if ( value.isWaterLevel)
							sensorTypeHtmlMsg += "Water level, ";
						if ( value.isH2s)
							sensorTypeHtmlMsg += "h2s, ";
						if ( value.isSo2)
							sensorTypeHtmlMsg += "so2, ";
						if ( value.isCo2)
							sensorTypeHtmlMsg += "co2, ";
						if ( value.isCh4)
							sensorTypeHtmlMsg += "ch4, ";
						if ( value.isNh3)
							sensorTypeHtmlMsg += "nh3, ";
						if ( value.isO2)
							sensorTypeHtmlMsg += "o2, ";
						
						sensorTypeHtmlMsg = sensorTypeHtmlMsg.substr(0,sensorTypeHtmlMsg.length-2);
						
						var sensorDataId = sensorConfig.id;
						
						var editSensorConfig = {
								id : sensorDataId,
								sensorId : value.sensorId,
								isCoverLevel : value.isCoverLevel,
								isWaterLevel : value.isWaterLevel,
								isH2s : value.isH2s,
								isSo2 : value.isSo2,
								isCo2 : value.isCo2,
								isCh4 : value.isCh4,
								isNh3 : value.isNh3,
								isO2 : value.isO2,
								sensorType : sensorTypeHtmlMsg
						};
						$scope.tagConfiguration.sensorConfigs[i] = editSensorConfig;						
					},
					//Cancel
					function(value) {
					}		
				);
				return;
			}
		}
	};
	
	$scope.generateSensorConfigsJson = function (sensorConfigs, deviceId) {
		var sensorConfigsJson = '{"sensorConfigs":[';
		for (var i = 0; i < sensorConfigs.length; ++i) {
 			var id = '"id":"' + sensorConfigs[i].id + '"'; 
 			var sensorId = '"sensorId":"' + deviceId + '"'; 
			var isCoverLevel = '"isCoverLevel":"' + sensorConfigs[i].isCoverLevel + '"'; 
			var isWaterLevel = '"isWaterLevel":"' + sensorConfigs[i].isWaterLevel + '"'; 
			var isH2s = '"isH2s":"' + sensorConfigs[i].isH2s + '"'; 
			var isSo2 = '"isSo2":"' + sensorConfigs[i].isSo2 + '"'; 
			var isCo2 = '"isCo2":"' + sensorConfigs[i].isCo2 + '"'; 
			var isCh4 = '"isCh4":"' + sensorConfigs[i].isCh4 + '"'; 
			var isNh3 = '"isNh3":"' + sensorConfigs[i].isNh3 + '"'; 
			var isO2 = '"isO2":"' + sensorConfigs[i].isO2 + '"'; 			
			
			var sensorConfig = '{' + id + ',' + sensorId + ',' + isCoverLevel + ',' + isWaterLevel + ',' + isH2s + ',' + isSo2 + ',' + isCo2 + ',' + isCh4 + ',' + isNh3 + ',' + isO2 + '}';

			if ( i != sensorConfigs.length - 1 ) {
				sensorConfig += ','
			}
			sensorConfigsJson += sensorConfig;
		}
		sensorConfigsJson += ']}';
		return sensorConfigsJson;
	};
	
	$scope.setMarkerPosition = function(location, zoomLevel) {
		$scope.tagConfiguration.lat = location.lat();
	    $scope.tagConfiguration.lon = location.lng();
	    $scope.tagConfiguration.zoomLevel = zoomLevel;
 	};
 	
 	$scope.getTagConfiguration = function() {
 		return $scope.tagConfiguration;
 	};
 	
 	$scope.toggleFlag = function (readerInSequence, readerWithHighestRssi, assignToSpecificReaders ) {
 		if ( readerInSequence == true) {
 			$scope.tagConfiguration.readerWithHighestRssi = false;
	 		$scope.tagConfiguration.assignToSpecificReaders = false;
 		}
 		
 		if ( readerWithHighestRssi == true) {
	 		$scope.tagConfiguration.readerInSequence = false;
 			$scope.tagConfiguration.assignToSpecificReaders = false;
 		}
 		
 		if ( assignToSpecificReaders == true) {
	 		$scope.tagConfiguration.readerInSequence = false;
 			$scope.tagConfiguration.readerWithHighestRssi = false;
 		}

 		if ( $scope.tagConfiguration.readerInSequence == false 
 				&& $scope.tagConfiguration.readerWithHighestRssi == false 
 				&& $scope.tagConfiguration.assignToSpecificReaders == false) {
	 		$scope.tagConfiguration.readerInSequence = true;
 			$scope.tagConfiguration.readerWithHighestRssi = false;
	 		$scope.tagConfiguration.assignToSpecificReaders = false;
 		}
 	};
 }]);


angular.module('dsd').controller('tagConfigurationValidateCtrl', ['$scope', '$translate','$interval', '$timeout','$routeParams', '$http', '$location' , 'appMainService','ngDialog',
                                                                  function ($scope, $translate, $interval, $timeout, $routeParams, $http, $location, appMainService, ngDialog){
}]);
