var app = angular.module('dsd', ['ngFileUpload','ngRoute', 'ngAnimate','ui.grid.autoResize','ui.bootstrap','ui.grid', 'ui.grid.selection','ngSanitize','chart.js','pascalprecht.translate', 'ngDialog','openlayers-directive','ui.grid.grouping','ui.grid.exporter','ui-rangeSlider','ui.grid.edit', 'ui.select','ui.grid.resizeColumns','ui.grid.pagination','uiSwitch']);

app.filter('waterLevelFilter', function () {
    return function (value) {
      return -1 * value;
    };
});

app.directive('datetimez', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.datetimepicker({
        	  format:'YYYY-MM-DD HH:mm:ss', 
        	  defaultDate: new Date()
          }).on('dp.change', function(e) {
              var outputDate = new Date(e.date);
              var n = outputDate.getTime();
              ngModelCtrl.$setViewValue(n);
        	  scope.$apply();
          });
        }
    };
});

app.directive('networkChartClicklistener', function () {
	  
	  return {
		scope: { someCtrlFn: '&callbackFn' },
		  
	    link: function (scope, element, attrs) {
	      var onButtonClick = function (event, sender, args) {
	    	  var id = args.data['Name'].split(":")[1];
	    	  var deviceType = args.data['Name'].split(":")[0];
	    	  scope.someCtrlFn({arg1: id, arg2: deviceType});
	      };
	          
	      var button = angular.element(document.querySelector('#networkChart'));
	      button.on('clickEvent', onButtonClick);
	      scope.$on('$destroy', function () {
	     
	      });
	    }
	  }
});


app.directive('timepicker', [
 	function() {
    var link;
    link = function(scope, element, attr, ngModel) {
        element = $(element);
        element.datetimepicker({
            format: 'YYYY-MM-DD HH:mm.ss',
          	defaultDate: scope.date
      	});
        element.on('dp.change', function(event) {
            scope.$apply(function() {
            	//scope.$setViewValue(event.date);
            	scope.someCtrlFn({arg1: event.date});
            });
        });
    };
    return {
	    restrict: 'A',
	    scope: {
	    	date: '=ngModel',
	    	someCtrlFn: '&callbackFn'
  		    },
  		    link: link
  	    };
  	}
]);

app.directive('animateOnChange', function($animate,$timeout) {
  return function(scope, elem, attr) {
      scope.$watch(attr.animateOnChange, function(nv,ov) {
        if (nv!=ov) {
          var c = nv > ov?'change-up':'change';
          $animate.addClass(elem,c).then(function() {
            $timeout(function() {$animate.removeClass(elem,c)}, 1000);
          });
        }
      })  
  }  
});
	
app.directive('uiSelectWrap', function($document, uiGridEditConstants) {
	return function link($scope, $elm, $attr) {
	    $document.on('click', docClick);
	    
	    function docClick(evt) {
	      if ($(evt.target).closest('.ui-select-container').size() === 0) {
	        $scope.$emit(uiGridEditConstants.events.END_CELL_EDIT);
	        $document.off('click', docClick);
	      }
	    }
	}; 
});


angular.module('dsd').config(['$routeProvider', function($routeProvider){
	$routeProvider
	 	 // Administration: User
		 .when('/Admin/UserMaintenance', {
	         templateUrl: 'view/administration/user/listView.html',
	         controller: 'userCtrl'
	     })	 
	     .when('/Admin/UserMaintenance/New', {
		     templateUrl: 'view/administration/user/createView.html',
		     controller: 'userCreateCtrl'
		 })
		 .when('/Admin/UserMaintenance/Edit/UserRole/:userId', {
		     templateUrl: 'view/administration/user/userRoleView.html',
		     controller: 'userRoleCtrl'
		 })
		 .when('/Admin/UserMaintenance/Edit/UserGroup/:userId', {
		     templateUrl: 'view/administration/user/userGroupView.html',
		     controller: 'userGroupCtrl'
		 })	
		 .when('/Admin/GroupMaintenance', {
	         templateUrl: 'view/administration/group/listGroupView.html',
	         controller: 'groupCtrl'
	     })	 
	     .when('/Admin/GroupMaintenance/New', {
		     templateUrl: 'view/administration/group/createGroupView.html',
		     controller: 'groupCreateCtrl'
		 })
	     
	     //View
	     .when('/View/FloorPlanView', {
	         templateUrl: 'view/view/floorPlanView.html',
	         controller: 'floorPlanViewCtrl'
	     })
	     .when('/View/SensorView', {
	         templateUrl: 'view/view/tagSensorView.html',
	         controller: 'tagSensorViewCtrl'
	     })
	     .when('/View/TableView', {
	         templateUrl: 'view/view/tableView.html',
	         controller: 'tableViewCtrl'
	     })
	     .when('/View/ChartView', {
	         templateUrl: 'view/view/chartView.html',
	         controller: 'chartViewCtrl'
	     })
	     .when('/View/ChartView/:tagConfigurationId', {
	         templateUrl: 'view/view/chartView.html',
	         controller: 'chartViewCtrl',
	     })
		 .when('/View/TagConfigurationSensorData/:tagConfigurationId', {
		     templateUrl: 'view/view/tagSensorView.html',
		     controller: 'tagSensorViewCtrl'
		 })	     	     
		 
		 .when('/Diagnostic/DiagnosticSensorView', {
	         templateUrl: 'view/diagnostic/diagnosticSensorView.html',
	         controller: 'diagnosticSensorViewCtrl'
	     })
		 .when('/Diagnostic/DiagnosticTableView', {
	         templateUrl: 'view/diagnostic/diagnosticTableView.html',
	         controller: 'diagnosticTableViewCtrl'
	     })
	     .when('/Diagnostic/DiagnosticChartView', {
	         templateUrl: 'view/view/chartView.html',
	         controller: 'chartViewCtrl'
	     })
	     .when('/Alert/AlertHistory', {
	         templateUrl: 'view/alert/alertHistoryView.html',
	         controller: 'alertHistoryViewCtrl'
	     })
	     .when('/Alert/AlertGroupView', {
	         templateUrl: 'view/alert/alertGroupView.html',
	         controller: 'alertGroupViewCtrl'
	     })	     
		 .when('/Alert/AlertSetting', {
	         templateUrl: 'view/alert/listAlertSettingView.html',
	         controller: 'alertSettingViewCtrl'
	     })
	     .when('/Alert/AlertSetting/New', {
	         templateUrl: 'view/alert/createAlertSettingView.html',
	         controller: 'alertSettingCreateCtrl'
	     })
	     .when('/Alert/AlertCriteria', {
	         templateUrl: 'view/alert/listAlertCriteriaView.html',
	         controller: 'alertCriteriaViewCtrl'
	     })
	     .when('/Alert/AlertCriteria/New', {
	         templateUrl: 'view/alert/createAlertCriteriaView.html',
	         controller: 'alertCriteriaCreateCtrl'
	     })
	    	     
	     
	     //System Configuration
	     .when('/SystemConfiguration/SiteConfiguration', {
		     templateUrl: 'view/systemConfiguration/siteConfiguration/listSiteConfigurationView.html',
		     controller: 'siteConfigurationCtrl'
	     })
	     .when('/SystemConfiguration/SiteConfiguration/New', {
		     templateUrl: 'view/systemConfiguration/siteConfiguration/createSiteConfigurationView.html',
		     controller: 'siteConfigurationCreateCtrl'
	     })	     
		 
		 .when('/SystemConfiguration/TagConfiguration', {
		     templateUrl: 'view/systemConfiguration/tagConfiguration/listTagConfigurationView.html',
		     controller: 'tagConfigurationCtrl'
	     })
	     .when('/SystemConfiguration/TagConfiguration/New', {
		     templateUrl: 'view/systemConfiguration/tagConfiguration/createTagConfigurationView.html',
		     controller: 'tagConfigurationCreateCtrl'
	     })	     
	     .when('/SystemConfiguration/TagConfigurationGroupView', {
		     templateUrl: 'view/systemConfiguration/tagConfiguration/listTagGroupConfigurationView.html',
		     controller: 'tagGroupConfigurationCtrl'
	     })
	     .when('/SystemConfiguration/TagConfigurationGroupView/New', {
		     templateUrl: 'view/systemConfiguration/tagConfiguration/createTagGroupConfigurationView.html',
		     controller: 'tagGroupConfigurationCreateCtrl'
	     })
	     .when('/SystemConfiguration/ValidateTagConfiguration', {
		     templateUrl: 'view/systemConfiguration/tagConfiguration/validateTagConfigurationView.html',
		     controller: 'tagConfigurationValidateCtrl'
		 })

		 .when('/Network/TableView', {
		     templateUrl: 'view/network/networkTableView.html',
		     controller: 'networkTableViewCtrl'
	     })
		 .when('/Network/ChartView', {
		     templateUrl: 'view/network/networkChartView.html',
		     controller: 'networkChartViewCtrl'
	     })
	     .when('/Network/ReaderHeartBeatView', {
	         templateUrl: 'view/network/networkReaderHeartBeatView.html',
	         controller: 'networkReaderHeartBeatViewCtrl'
	     })
	     
	     .when('/SystemDefinition/ApplicationMaintenance/New', {
		     templateUrl: 'view/system-definition/application/createView.html',
		     controller: 'applicationCreateCtrl'
	     })
	     .when('/SystemDefinition/ApplicationMaintenance/Edit/:applicationId', {
		     templateUrl: 'view/system-definition/application/editView.html',
		     controller: 'applicationEditCtrl'
		 })
		 .when('/SystemDefinition/ApplicationExternalLink', {
			 templateUrl: 'view/system-definition/application/linkView.html',
			 controller: 'mainCtrl'
		 })
	     
	     // Homepage to list menu
	     .when('/ListMenu/:moduleId', {
	         templateUrl: 'listmenu.html',
	         controller: 'moduleDetailsCtrl'
	     })
	     
	     // Others
	     .when('/DashBoard', {
	         templateUrl: 'dashBoardView.html',
	         controller: 'dashBoardViewCtrl'
	     })
	      .when('/', {
	         templateUrl: 'dashBoardView.html',
	         controller: 'dashBoardViewCtrl'
	     })
	     .otherwise({redirectTo:"/"});
}]);

app.directive('dynamic', function ($compile) {
	  return {
	    restrict: 'A',
	    replace: true,
	    link: function (scope, ele, attrs) {
	      scope.$watch(attrs.dynamic, function(html) {
	        ele.html(html);
	        $compile(ele.contents())(scope);
	      });
	    }
	  };
})


app.factory('appMainService', function($rootScope, ngDialog) {
  var sharedService = {};

  sharedService.message = '';
  sharedService.login = false; 
  
  sharedService.tableViewSiteId = 0;
  sharedService.tableViewGroupId = 0;
 
  sharedService.diagnosticChartViewDeviceId = null;
  sharedService.diagnosticChartViewSensorId = null;
  
  sharedService.siteConfiguration = null;
  sharedService.tagConfiguration = null;
  sharedService.alertSetting = null;
  sharedService.diagnosticTableViewData = [];
  sharedService.diagnosticSensorViewData = [];
  sharedService.alertSettingHistoryData = [];
  sharedService.user = null; 
  sharedService.langKey = null;
  sharedService.startDate = null;
  sharedService.endDate = null;
  sharedService.alertCriteria = null;
  sharedService.group = null;
  sharedService.tableView = [];
  sharedService.startCheckSessionTimeout = false; 
  sharedService.domainAddress = "/DSD"; 
  sharedService.dashBoardSelectedTagConfigurationId = 0;
  sharedService.loginLang = null; 
  sharedService.searchAlertData = null; 
  sharedService.groupTagConfiguration = null;
  
//  sharedService.dashBoardSelectedTagConfigurationForAlertViewId = 0;
//  sharedService.dashBoardSelectedTagConfigurationForMapViewId = 0;
  
  sharedService.prepForBroadcast = function(msg) {
    this.message = msg;
    this.broadcastItem(msg);
  };

  sharedService.broadcastItem = function(msg) {
	  if ('menuUpdatedInModuleEvent' === msg ||
			  'updateLanguageInUiGrid' === msg) {
		  $rootScope.$broadcast(msg);
	  }
	  else
		  $rootScope.$broadcast('handleGeneralBroadcast');
  };

  sharedService.broadcastItem2 = function(msg, locale) {
	  $rootScope.$broadcast(msg, locale);
  };
  
  sharedService.setLogin = function(data) {
	  this.login = data;
  };
  
  sharedService.getLogin = function () {
	  return this.login;
  };
  
  sharedService.setTableViewSiteId = function(data) {
	  this.tableViewSiteId = data;
  };
  sharedService.getTableViewSiteId = function () {
	  return this.tableViewSiteId;
  };

  sharedService.setTableViewGroupId = function(data) {
	  this.tableViewGroupId = data;
  };
  sharedService.getTableViewGroupId = function () {
	  return this.tableViewGroupId;
  };  
  
  sharedService.setDiagnosticChartViewDeviceId = function(data) {
	  this.diagnosticChartViewDeviceId = data;
  };
  sharedService.getDiagnosticChartViewDeviceId = function () {
	  return this.diagnosticChartViewDeviceId;
  };

  sharedService.setDiagnosticChartViewSensorId = function(data) {
	  this.diagnosticChartViewSensorId = data;
  };
  sharedService.getDiagnosticChartViewSensorId = function () {
	  return this.diagnosticChartViewSensorId;
  };    

  sharedService.setTagConfiguration = function(data) {
	  this.tagConfiguration = data;
  };
  sharedService.getTagConfiguration = function () {
	  var rtn = this.tagConfiguration;
	  this.tagConfiguration = null;
	  return rtn;
  };    

  sharedService.setSiteConfiguration = function(data) {
	  this.siteConfiguration = data;
  };
  sharedService.getSiteConfiguration = function () {
	  var rtn = this.siteConfiguration;
	  this.siteConfiguration = null;
	  return rtn;
  };    
  
  sharedService.setAlertSetting = function(alertSetting) {
	  this.alertSetting = alertSetting;
  }
  sharedService.getAlertSetting = function() {
	  var alertSetting =  this.alertSetting;
	  this.alertSetting = null;
	  return alertSetting;
  }

  sharedService.getUser = function () {
	  var user = this.user;
	  this.user = null;
	  return user;
  }

  sharedService.setUser = function (user) {
	  this.user = user;
  }
  
  sharedService.getGroup = function () {
	  var group = this.group;
	  this.group = null;
	  return group;
  }

  sharedService.setGroup = function (group) {
	  this.group = group;
  }

  sharedService.setStartDate = function(fromDate) {
	  if ( fromDate == null)
		  return; 
	  
	  this.startDate = fromDate.toString();
  }
  sharedService.getStartDate = function() {
	  if ( this.startDate == null) {
		  return ( new Date().getTime() - 24*60*60*1000); //1 day
	  }
   	  var fromDate = new Date(parseInt(this.startDate));
	  return fromDate.getTime();
  }

  sharedService.setEndDate = function(toDate) {
	  if ( toDate == null)
		  return; 
	  
	  this.endDate = toDate.toString();
  }
  sharedService.getEndDate = function() {
	  if ( this.endDate == null)
		  return new Date().getTime();
	  
	  var toDate = new Date(parseInt(this.endDate));
	  return toDate.getTime();
  }
  
  sharedService.setAlertCriteria = function(alertCriteria) {
	  this.alertCriteria = alertCriteria;
  }

  sharedService.getAlertCriteria = function() {
	  var alertCriteria =  this.alertCriteria;
	  this.alertCriteria = null;
	  return alertCriteria;
  }

  sharedService.setTableView = function(tableView) {
	  this.tableView = tableView;
  }
  
  sharedService.getTableView = function() {
	  var tableView = this.tableView;
	  this.tableView = [];
	  return tableView;
  }
  
  sharedService.getDomainAddress = function () {
	  return this.domainAddress;
  };
  
  sharedService.getLoginLang = function() {
	  var loginLang = localStorage.getItem("loginLang");
	  if (loginLang != "" && loginLang != null ) {
		  return loginLang;
	  }
	  return null;
  }
  
  sharedService.setLoginLang = function(loginLang) {
	  this.loginLang = loginLang;
  }
  
  sharedService.setSearchAlertData = function(searchAlertData) {
	  this.searchAlertData = searchAlertData;
  }
  
  sharedService.getSearchAlertData = function() {
	  var searchAlertData = this.searchAlertData;
	  this.searchAlertData = null;
	  return searchAlertData;
  } 
  
  sharedService.reset = function () {
	  this.tableViewSiteId = 0;
	  this.tableViewGroupId = 0;
	  this.diagnosticChartViewDeviceId = null;
	  this.diagnosticChartViewSensorId = null;
	  this.tagConfiguration = null;
	  this.alertSetting = null;
	  this.siteConfiguration = null;
	  this.user = null;
	  this.langKey = null;
	  this.alertCriteria = null;
	  this.startDate = null;
	  this.endDate = null;
	  this.alertSettingHistoryData = [];
	  this.group = null;
	  this.tableView = [];
//	  this.dashBoardSelectedTagConfigurationId = 0;
//	  this.dashBoardSelectedTagConfigurationForAlertViewId = 0;
//	  this.dashBoardSelectedTagConfigurationForMapViewId = 0;
  };  
  
  sharedService.displayMessageResult = function(validationObject, displayPassValidation) {
	  $rootScope.validationObject = validationObject;
	 // $rootScope.validationObject.validationMessages = validationObject.validationMessages.replace(";","\n");
	  var useCssClass; 
	  if (validationObject.isValid) {
		  if (displayPassValidation)	  
			  useCssClass = 'ngdialog-theme-validationSuccess';
		  else
			  return;
	  }
	  else
		  useCssClass = 'ngdialog-theme-validationFailed';
	  
	  ngDialog.openConfirm({
		  className: useCssClass ,
		  template: 'view/validationMessageView.html',
		  scope: $rootScope //Pass the scope object if you need to access in the template
	  }).then(
		 //Update
		function(value) {
		},
		//Cancel
		function(value) {
		}		
	  );		
  };
  
  sharedService.displaySessionTimeoutResult = function(validationObject) {
	  $rootScope.validationObject = validationObject;
	 // $rootScope.validationObject.validationMessages = validationObject.validationMessages.replace(";","\n");
	  var useCssClass; 
	  if (validationObject.isValid) {
		  return;
	  }
	  else
		  useCssClass = 'ngdialog-theme-validationFailed';

	  ngDialog.openConfirm({
		  className: useCssClass ,
		  template: 'view/validationSessionView.html',
		  scope: $rootScope //Pass the scope object if you need to access in the template
	  }).then(
		 //Update
		function(value) {
			$rootScope.alreadyTimeoutDisplay = false;
		},
		//Cancel
		function(value) {
			$rootScope.alreadyTimeoutDisplay = false;
			$rootScope.$location.path('logout');
		}		
	  );		
  };
  
  sharedService.setDiagnosticTableViewData = function(diagnosticTableViewData) {
	  this.diagnosticTableViewData = diagnosticTableViewData;
  };
  sharedService.getDiagnosticTableViewData = function() {
	  return this.diagnosticTableViewData;
  };

  sharedService.setDiagnosticSensorViewData = function(diagnosticSensorViewData) {
	  this.diagnosticSensorViewData = diagnosticSensorViewData;
  };
  sharedService.getDiagnosticSensorViewData = function() {
	  return this.diagnosticSensorViewData;
  };
    
  sharedService.setAlertSettingHistoryData = function (alertSettingHistoryData) {
	  this.alertSettingHistoryData = alertSettingHistoryData;
  };
  sharedService.getAlertSettingHistoryData = function () {
	  return this.alertSettingHistoryData;
  };
  
  sharedService.setLangKey = function(langKey) {
	  this.langKey = langKey;
  };
  
  sharedService.getLangKey = function() {
	  return this.langKey;
  };
  
  sharedService.setStartCheckSessionTimeout = function (startCheckSessionTimeout) {
	  this.startCheckSessionTimeout = startCheckSessionTimeout;
  };
  sharedService.getStartCheckSessionTimeout = function (startCheckSessionTimeout) {
	  return this.startCheckSessionTimeout;
  };
  
  sharedService.setDashBoardSelectedTagConfigurationId = function(tagConfiguration) {
	  this.dashBoardSelectedTagConfigurationId = tagConfiguration;
  };
  sharedService.getDashBoardSelectedTagConfigurationId = function() {
	  var tagConfigurationId = this.dashBoardSelectedTagConfigurationId;
	  this.dashBoardSelectedTagConfigurationId = 0;
	  return tagConfigurationId;
  };
  
  sharedService.setGroupTagConfiguration = function(groupTagConfiguration) {
	  this.groupTagConfiguration = groupTagConfiguration;
  };
  
  sharedService.getGroupTagConfiguration = function() {
	  var groupTagConfiguration = this.groupTagConfiguration;
	  this.groupTagConfiguration = null;
	  return groupTagConfiguration;
  };
  
//  sharedService.setDashBoardSelectedTagConfigurationForAlertViewId = function(tagConfiguration) {
//	  this.dashBoardSelectedTagConfigurationForAlertView = tagConfiguration;
//  };
//  sharedService.getDashBoardSelectedTagConfigurationForAlertViewId = function () {
//	  var tagConfigurationId = this.dashBoardSelectedTagConfigurationForAlertViewId;
//	  this.dashBoardSelectedTagConfigurationForAlertViewId = 0;
//	  return tagConfigurationId;
//  };
//  sharedService.setDashBoardSelectedTagConfigurationForMapViewId = function (tagConfiguration) {
//	  this.dashBoardSelectedTagConfigurationForMapView = tagConfiguration;
//  };
//  sharedService.getDashBoardSelectedTagConfigurationForMapViewId = function () {
//	  var tagConfigurationId = this.dashBoardSelectedTagConfigurationForMapViewId;
//	  this.dashBoardSelectedTagConfigurationForMapViewId = 0;
//	  return tagConfigurationId;
//  };
  
  $rootScope.webSocketClient = null;
  
  $rootScope.connectWebSocketData = function () {
	  if ($rootScope.webSocketClient != null) {
		  return;
	  }
	  
	  $rootScope.webSocketIpAddr = "137.189.35.35";
	  $rootScope.webSocketPort="32";
	  
	  //$rootScope.webSocketIpAddr = "127.0.0.1"; 
	  //$rootScope.webSocketPort="9000"; 
	  	
	  $rootScope.endPointSocketUrl = "ws://" + $rootScope.webSocketIpAddr + ":" +  $rootScope.webSocketPort;
	  $rootScope.webSocketClient = new WebSocket($rootScope.endPointSocketUrl);
	  $rootScope.userName = "admin";
	  $rootScope.webSocketClient.onmessage = function (event) {
		   
		   var jsonObj = JSON.parse(event.data);
		   if ( typeof jsonObj.msgType == "undefined" && jsonObj[0].msgType === "deviceData") {
			  
			   var sensorDatas = [];
		 	   angular.forEach(jsonObj, function(data, index){
		 		   var sensorData = data;
		 		   sensorData.deviceId = data.deviceId;
		 		   sensorDatas.push(sensorData);
		 	   });
		 	   
		 	   $rootScope.$broadcast('receiveSensorData', sensorDatas);
		 	 
		   } else if ( typeof jsonObj.msgType == "undefined" && jsonObj[0].msgType === "network") {
			
			   console.log('receiveNetwork');
			   $rootScope.$broadcast('receiveNetwork', jsonObj);
		   
		   } else if (jsonObj.msgType === "alert") {
			   
			   jsonObj.tagConfiguration = new Object();
			   jsonObj.tagConfiguration.id = jsonObj.tagConfigurationId; 
			   jsonObj.tagConfiguration.name = jsonObj.tagConfigurationName;
		       
			   $rootScope.$broadcast('receiveAlert', jsonObj);
			   
		   } else if (jsonObj.msgType === "readerHeartBeat") {
			   console.log("readerHeartBeat");
			   $rootScope.$broadcast('receiveReaderHeartBeat', jsonObj);
		   }
	  };
  };
 
  $rootScope.disconnectWebSocketData = function () {
	   if ($rootScope.webSocketClient != null) {
		   $rootScope.webSocketClient.close();
		   $rootScope.webSocketClient = null;
	   }
  };
  
  return sharedService;
});


app.config(function ($translateProvider, ngDialogProvider) {

    ngDialogProvider.setDefaults({
        showClose: false
    });
    
  $translateProvider.translations('en', {
	  "SYSTEM_TITLE" : "Welcome to DEPP System",
	  "TXT_FORGET_EMAIL": "Forget Password?",
	  "STREET_NAME" : "Street Name",
	  "TXT_LOGIN":"Login",
	  "TXT_PASSWORD" :"Password",
	  "TXT_USERNAME" :"Username",
	  "TXT_HOMEPAGE": "Homepage",
	  "MSG_LOGIN_FAIL":"Incorrect user id or password",
	  "TXT_MAINTITLE" : "DSD Monitoring System",
	  "VIEW" : "View",
	  "FLOOR_PLAN_VIEW" : "Map View",
	  "TABLE_VIEW" : "Table View",
	  "CHART_VIEW" : "Chart View",
	  "DIAGNOSTIC" : "Diagnostic",
	  "DIAGNOSTIC_TABLE_VIEW" : "Diagnostic Table View",
	  "SENSOR_VIEW" : "Sensor View",
	  "ALERT" : "Alert",
	  "ALERT_DETAILS" : "Alert Details",
	  "ALERT_GROUP" : "Alert Group",
	  "ALERT_HISTORY" : "Alert History",
	  "ALERT_RULE_SETTING" : "Alert Rule Settings",
	  "SYSTEM_CONFIGURATION" : "System",
	  "SITE_CONFIGURATION" : "Site Configuration",
	  "SITE_ID" : "Site Id",
	  "SITE" : "Site",
	  "TAG_CONFIGURATION" : "Tag Configuration",
	  "TAG_CONFIGURATION_GROUP" : "Tag Group",
	  "TAG_GROUP_CONFIGURATION" : "Tag Group Configuration",
	  "NETWORK" : "Network",
	  "NETWORK_DIAGRAM" : "Diagram View",
	  "ADMINISTRATION" : "Administration",
	  "USERS" : "Users",
	  "WELCOME" : "Welcome",
	  "ITEMS" : "Items",
	  "SELECT_SITE" : "Select site",
	  "SELECT_A_TAG" : "Select a tag",
	  "SELECT_TAG" : "Select tag",
	  "GROUP_ID" : "Group Id",
	  "TAG_NAME" : "Tag name",
	  "LOGOUT" : "Logout",
	  "PROFILE" : "User profile",
	  "DATE" : "Date",
	  "SENSOR_ID" : "Sensor Id",
	  "SENSOR_TYPE" : "Sensor type",
	  "TEMPERATURE" : "Temperature",
	  "TEMP" : "Temp.",
	  "HUMIDITY" : "Humidity",
	  "VIBRATION" : "Vibration",
	  "LUX" : "LUX",
	  "UV" : "UV",
	  "PERIOD_S" : "Period (s)",
	  "MEASURE_PERIOD" : "Measure Period (s)",
	  "TIME_DIFF" : "Time Diff.(Ms)",
	  "MAX_TIME_DIFF" : "Max Diff.(Ms)",
	  "DETAILS" : "Details",
	  "SENSOR_DATA" : "Sensor Data",
	  "DEVICE_ID" : "Tag Id",
	  "DEVICE_ADDR" : "Device Addr",
	  "DESCRIPTION" : "Description",
	  "START_DATE" : "Start date",
	  "END_DATE" : "End date",
	  "SENSOR_TYPE_IS_REQUIRED" : "Sensor type is required",
	  "RECEIVE_REALTIME_UPDATES_FROM_SERVER" : "Receive real time updates from Server",
	  "SEARCH" : "Search",
	  "PARENT_ADDR" : "Parent Addr",
	  "READER_ID" : "Reader Id",
	  "CORD_ID" : "Reader Id",
	  "NOF_SENSORS" : "# Sensors",
	  "RESET_MAX_TIME_DIFF" : "Reset max. time diff",
	  "SEND_TO_DEVICE" : "Send to device",
	  "SELECT" : "Select ",
	  "CONNECT" : "Connect",
	  "CHART_X_AXIS_COVERAGE_MINS" : "Chart x-axis coverage(mins)",
	  "ALERT_LEVEL" : "Alert level",
	  "ALERT_MESSAGE" : "Alert message",
	  "LEVEL" : "Level",
	  "ALERT_NAME" : "Alert name",
	  "USE_DATE_RANGE" : "Date range",
	  "SEND_EMAIL" : "Send email",
	  "EMAIL" : "Email",
	  "REPEAT_ALERT_IN_SECS" : "Repeat (secs)",
	  "NEW_RECORD" : "New record",
	  "EDIT_RECORD" : "Edit record",
	  "SAVE" : "Save",
	  "CANCEL" : "Cancel",
	  "CRITICAL_ALERT" : "Critical",
	  "HIGH_ALERT" : "High",
	  "MEDIUM_ALERT" : "Medium",
	  "LOW_ALERT" : "Low",
	  "SET" : "Set ",
	  "EMAIL_RECEIPT_BY_COMMA" : "Email recipients (separated by semicolon)",
	  "ALERT_LEVEL_IS_REQUIRED" : "Alert level is required",
	  "ALERT_NAME_IS_REQUIRED" : "Alert name is required",
	  "DEVICE_ADDR_IS_REQUIRED" : "Device address is required",
	  "DEVICE_ID_IS_REQUIRED" : "Tag id is required",
	  "NAME_IS_REQUIRED" : "Name is required",
	  "TAG_NAME_IS_REQUIRED" : "Tag name is required",
	  "GROUP_ID_IS_REQUIRED" : "Group Id is required",
	  "SENSOR_ID_IS_REQUIRED" : "Sensor Id is required",
	  "SITE_IS_REQUIRED" : "Site is required",
	  "MEASURE_PERIOD_IS_REQUIRED" : "Measurement period is required",
	  "DISCONNECT" : "Disconnect",
	  "DELETE" : "Delete",
	  "NAME" : "Name",
	  "SITE_MAP" : "Site map",
	  "MAP" : "Map",
	  "SELECT" : "Select",
	  "REMOVE" : "Remove",
	  "MAP_IS_REQUIRED" : "Map is required",
	  "DEVICE_IMAGE" : "Device image",
	  "SITE_NAME" : "Site name",
	  "TAG_ID" : "Tag Id",
	  "CONFIGURE_ALERT" : "Configure alert",
	  "NETWORK_TABLE_VIEW" : "Network Table View",
	  "DEVICE_TYPE" : "Device type",
	  "ROUTER_ID" : "Router Id",
	  "CHINAME" : "Chinese name",
	  "ENGNAME" : "English name",
	  "EMAIL" : "Email",
	  "DEPARTMENT" : "Department",
	  "PHONE" : "Phone",
	  "NOTIFICATION" : "Notification",
	  "TITLE" : "Title",
	  "ACTIVE" : "Active",
	  "ADD_SENSOR" : "Add Sensor",
	  "IMAGE" : "Image",
	  "SENSOR_CONFIGURATION" : "Sensor configuration",
	  "ROUTER_CONFIGURATION" : "Router configuration",
	  "SEND_TO_ROUTER" : "Send to router",
	  "HEART_BEAT_IN_SECS" : "Heart beat in secs",
	  "DEFAULT_LANGUAGE" : "Default language",
	  "REAL_TIME" : "Real time",
	  "DEFAULT_SITE" : "Default site",
	  "PASSWORD" : "Password",
	  "GROUPS" : "Groups",
	  "LOGIN_ID" : "Login Id",
	  "LOGIN_ID_IS_REQUIRED": "Login Id is required",
	  "NAME_IS_REQUIRED" : "Name is required",
	  "PASSWORD_IS_REQUIRED" : "Password is required",
	  "GROUP_IS_REQUIRED" : "Group is required",
	  "PERMISSION" : "Permission",
	  "PERMISSION_IS_REQUIRED" : "Permission is required",
	  "VALIDATE_TAG_CONFIGURATION" : "Validate Tag Configuration",
	  "VALIDATE_TAG_RESULT" : "Validation Result",
	  "TAG_VALIDATION_MESSAGE" : "Tag validation message",
	  "DELETE_FROM_NETWORK" : "Delete from network",
	  "USE_GROUP_ID" : "Group Id",
	  "USE_SENSOR_ID" : "Sensor Id",
	  "CORRECT" : "Correct",
	  "DIAGNOSTIC_SENSOR_VIEW" : "Diagnostic Sensor View",
	  "TAG_SENSOR_VIEW" : "Tag Sensor View",
	  "DIAGNOSTIC_CHART_VIEW" : "Diagnostic Chart View",
	  "SELECT_DEVICE_ID" : "Select Device Id",
	  "SELECT_SENSOR_ID" : "Select Sensor Id",
	  "USE_OUT_OF_RANGE_TRIGGER_ALERT" : "Trigger alert if out of range",
	  "ALERT_CRITERIA" : "Alert criteria",
	  "ALERT_CRITERIA_NAME" : "Criteria name",
	  "ALERT_CRITERIA_IS_REQUIRED" : "Alert Criteria is required",
	  "ALERT_CRITERIA_NAME_IS_REQUIRED" : "Criteria name is required",
	  "EDIT_ALERT_CRITERIA" : "Edit alert criteria",
	  "SELECT_SENSOR_TYPE" : "Select sensor type",
	  "ENGLISH" : "English",
	  "SIMPLIFIED_CHINESE" : "Simplified Chinese",
	  "CONFIRM_PASSWORD" : "Confirm password",
	  "UPDATE_MEASURE_PERIOD" : "Update measure period",
	  "UPDATE_HEART_BEAT" : "Update heart beat",
	  "GROUP_NAME" : "Group Name",
	  "GROUP_NAME_IS_REQUIRED" : "Group name is required",
	  "ASSIGNED_FUNCTION" : "Assigned function",
	  "FUNCTION" : "Function",
	  "MENU" : "Menu",
	  "ALL" : "All",
	  "MODIFY" : "Modify",
	  "READONLY" : "Read Only",
	  "COVER_LEVEL" : "Cover level",
	  "WATER_LEVEL" : "Water level",
	  "H2S" : "h2s",
	  "SO2" : "so2",
	  "CO2" : "co2",
	  "CH4" : "ch4",
	  "NH3" : "nh3",
	  "O2" : "o2",
	  "RSSI" : "Rssi",
	  "PACKET_COUNTER" : "Seq.no.",
	  "REFRESH" : "Refresh",
	  "CONFIG" : "Config",
	  "GROUP" : "Group",
	  "TIME" : "Time",
	  "LOW_BATTERY" : "Low battery",
	  "Y-AXIS-MIN" : "Y-axis min. value",
	  "Y-AXIS-MAX" : "Y-axis max. value",
	  "COVER_DETECT" : "Cover Detect",
	  "WATER_LEVEL_1_DETECT" : "Water L1",
	  "WATER_LEVEL_2_DETECT" : "Water L2",
 	  "ANTENNA_OPTIMIZED" : "Ant. Opt'ed",
 	  "MAX_STEP" : "Max Step",
 	  "USER_DEFINE" : "User Define",
 	  "ADC_VALUE" : "Adc Value",
 	  "BATTERY" : "Battery",
 	  "SELECT_VIEW_TYPE" : "Select view type",
 	  "REAL_TIME_CHART_VIEW" : "Real-time Chart View",
 	  "MINUTE" : "Minute",
 	  "HOUR" : "Hour",
 	  "DAY" : "Day",
 	  "WEEK" : "Week",
 	  "MONTH" : "Month",
 	  "YEAR" : "Year",
 	  "ALL" : "All",
 	  "OPEN_ALERT_SETTING" : "Open Alert Setting",
 	  "CLOSE" : "Close",
 	  "LOADING" : "Loading",
 	  "MINS_AGO" : "mins ago",
 	  "HOURS_AGO" : "hrs ago",
 	  "OPEN_TAG_LOCATION" : "Open Tag Location",
 	  "SENSOR_DATA_OPTIONS" : "Sensor Data Options",
 	  "READER_WITH_HIGHEST_RSSI" : "Reader with highest Rssi",
 	  "ASSIGN_TO_SPECIFIC_READERS" : "Assign to specific readers",
 	  "UP" : "Up",
 	  "DOWN" : "Down",
 	  "SEQUENCE_NO" : "Seq No.",
 	  "LATEST_DATA_FROM_READERS" : "Latest data from readers",
 	  "TAG_LOCATION" : "Tag Location",
 	  "LOADING_PLEASE_WAIT" : "Loading, please wait",
 	  "NO_DATA_FOUND" : "No data found",
 	  "EXPORT_TO_CSV" : "Export to csv file",
 	  "VERSION" : "Version",
 	  "READER_RSSI" : "Reader:Rssi",
 	  "READER_NAME_RSSI" : "Name:Rssi",
	  "READER_RSSI_SNR" : "Reader:Rssi|Snr",
 	  "READER_NAME_RSSI_SNR" : "Name:Rssi|Snr",
 	  "READER_HEART_BEAT_VIEW" : "Reader Heart Beat",
 	  "OPEN" : "Open",
 	  "CLOSED" : "Closed",
 	  "ADD_PIPE" : "Add Pipe",
 	  "EDIT_PIPE" : "Edit Pipe",
 	  "ADD_POINT" : "Add Point",
 	  "DELETE_POINT" : "Delete Point",
 	  "ADD_LINE" : "Add Line",
 	  "COMPLETE_LINE" : "Complete Line",
 	  "IMPORT_LINE_FROM_CSV_FILE" : "Import Lines from csv file",
 	  "READER_NAME" : "Reader Name",
 	  "WATER_LEVEL_CHART_TYPE" : "Water Level Chart Type",
 	  "FROM_TOP" : "From Top",
 	  "FROM_BOTTOM" : "From Bottom",
	  "MAN_HOLE_ID" : "Manhole Id",
	  "MAN_HOLE_HEIGHT" : "Manhole Height",
	  "HEIGHT" : "Height"
  });
  
  $translateProvider.translations('zh', {
	  "SYSTEM_TITLE" : "Welcome to DEPP System",
	  "STREET_NAME" : "街道名称",
	  "TXT_FORGET_EMAIL": "忘記密碼?",
	  "TXT_LOGIN":	"登入",
	  "TXT_PASSWORD" :"密碼",
	  "TXT_USERNAME" :"用戶",
	  "TXT_HOMEPAGE": "首页",
	  "MSG_LOGIN_FAIL": "不正确的用户名和密码!!!",
	  "TXT_MAINTITLE" : "DSD监测系统",
	  "VIEW" : "图示",
	  "FLOOR_PLAN_VIEW" : "地图",
	  "TABLE_VIEW" : "方格表示",
	  "CHART_VIEW" : "统计图",
	  "DIAGNOSTIC" : "测试",
      "DIAGNOSTIC_TABLE_VIEW" : "测试方格表示",
	  "SENSOR_VIEW" : "传感器数据",
	  "ALERT" : "警报",
	  "ALERT_DETAILS" : "警报细节",
	  "ALERT_GROUP" : "警报群组",
	  "ALERT_HISTORY" : "过往警报",
	  "ALERT_RULE_SETTING" : "警报设定", 
	  "SYSTEM_CONFIGURATION" : "系统设定",
	  "SITE_CONFIGURATION" : "设置所在地",
	  "SITE_ID" : "所在地Id",
	  "SITE" : "所在地",
	  "TAG_CONFIGURATION" : "标签设定",
	  "TAG_CONFIGURATION_GROUP" : "标签群组",
	  "TAG_GROUP_CONFIGURATION" : "标签群组设定",
	  "NETWORK" : "网络状态",
	  "NETWORK_DIAGRAM" : "网络图",
	  "ADMINISTRATION" : "管理员设定",
	  "USERS" : "用户",
	  "WELCOME" : "欢迎",
	  "ITEMS" : "项目",
	  "SELECT_SITE" : "选择所在地",
	  "SELECT_A_TAG" : "选择标签",
	  "SELECT_TAG" : "选择标签",
	  "GROUP_ID" : "群组Id",
	  "TAG_NAME" : "标签名称",
	  "LOGOUT" : "登出",
	  "PROFILE" : "用户设定",
	  "DATE" : "日期",
	  "SENSOR_ID" : "传感器Id",
	  "SENSOR_TYPE" : "传感器类型",
	  "TEMPERATURE" : "温度",
	  "TEMP" : "温度",
	  "HUMIDITY" : "湿度",
	  "VIBRATION" : "振动",
	  "LUX" : "光度",
	  "UV" : "紫外线",
	  "PERIOD_S" : "间隔(s)",
	  "MEASURE_PERIOD" : "测量间隔 (s)",
	  "TIME_DIFF" : "时间间隔 (ms)",
	  "MAX_TIME_DIFF" : "最大间隔(ms)",
	  "DETAILS" : "进阶",
	  "SENSOR_DATA" : "传感器数据",
	  "DEVICE_ID" : "終端Id",
	  "DEVICE_ADDR" : "終端地址",
	  "DESCRIPTION" : "说明",
	  "START_DATE" : "开始日期",
	  "END_DATE" : "完结日期",
	  "SENSOR_TYPE_IS_REQUIRED" : "输入传感器类型",
	  "RECEIVE_REALTIME_UPDATES_FROM_SERVER" : "接收即时数据",   
	  "SEARCH" : "搜索",
	  "PARENT_ADDR" : "終端祖先地址",
	  "READER_ID" : "接收器Id",
	  "CORD_ID" : "协调机Id",
	  "NOF_SENSORS" : "传感器数目",
	  "RESET_MAX_TIME_DIFF" : "重置最大时间间隔",
	  "SEND_TO_DEVICE" : "传送到終端",
	  "SELECT" : "选择",
	  "CONNECT" : "连接",
	  "CHART_X_AXIS_COVERAGE_MINS" : "统计图 x-轴覆盖范围(分钟)",
	  "ALERT_LEVEL" : "警报级别",
	  "ALERT_MESSAGE" : "警报讯息",
	  "LEVEL" : "级别",
	  "ALERT_NAME" : "警报名称",
	  "USE_DATE_RANGE" : "日期间隔",
	  "SEND_EMAIL" : "发送电邮",
	  "EMAIL" : "电邮",
	  "REPEAT_ALERT_IN_SECS" : "重复(s)",
	  "NEW_RECORD" : "新档案",
	  "EDIT_RECORD" : "更新档案",
	  "SAVE" : "保存",
	  "CANCEL" : "取消",
	  "CRITICAL_ALERT" : "严重",
	  "HIGH_ALERT" : "高",
	  "MEDIUM_ALERT" : "中",
	  "LOW_ALERT" : "低",
	  "SET" : "设定",
	  "EMAIL_RECEIPT_BY_COMMA" : "电子邮件收件人 (用 ; 隔开)",
	  "ALERT_LEVEL_IS_REQUIRED" : "输入警报级别",
	  "ALERT_NAME_IS_REQUIRED" : "输入警报名称",
	  "DEVICE_ADDR_IS_REQUIRED" : "输入終端地址",
	  "DEVICE_ID_IS_REQUIRED" : "输入标签Id",
	  "NAME_IS_REQUIRED" : "输入名称",
	  "GROUP_ID_IS_REQUIRED" : "输入群组Id",
	  "SENSOR_ID_IS_REQUIRED" : "输入传感器Id",
	  "SITE_IS_REQUIRED" : "输入所在地",
	  "MEASURE_PERIOD_IS_REQUIRED" : "输入测量间隔",
	  "TAG_NAME_IS_REQUIRED" : "输入标签名称",
	  "DISCONNECT" : "断开",
	  "DELETE" : "刪除",
	  "NAME" : "名称",
	  "SITE_MAP" : "所在地地图",
	  "MAP" : "地图",
	  "SELECT" : "选择",
	  "REMOVE" : "删除",
	  "MAP_IS_REQUIRED" : "选择地图",
	  "DEVICE_IMAGE" : "終端图像",
	  "SITE_NAME" : "所在地名称",
	  "TAG_ID" : "标签Id",
	  "CONFIGURE_ALERT" : "警报设定",
	  "NETWORK_TABLE_VIEW" : "网络方格表示",
	  "DEVICE_TYPE" : "終端类别",
	  "ROUTER_ID" : "路由器Id",
	  "CHINAME" : "中文名称",
	  "ENGNAME" : "英文名称",
	  "EMAIL" : "电邮",
	  "DEPARTMENT" : "部门",
	  "PHONE" : "电话",
	  "NOTIFICATION" : "通报",
	  "TITLE" : "职位",
	  "ACTIVE" : "激活",
	  "ADD_SENSOR" : "加传感器",
	  "IMAGE" : "图像",
	  "SENSOR_CONFIGURATION" : "传感器设定",
	  "ROUTER_CONFIGURATION" : "路由器设定",
	  "SEND_TO_ROUTER" : "传送到路由器",
	  "HEART_BEAT_IN_SECS" : "心跳间隔 (秒)",
	  "DEFAULT_LANGUAGE" : "预设语言",
	  "REAL_TIME" : "即时数据",
	  "DEFAULT_SITE" : "预设所在地",
	  "PASSWORD" : "密码",
	  "GROUPS" : "群组",
	  "LOGIN_ID" : "登入Id",
	  "LOGIN_ID_IS_REQUIRED": " 输入登入Id",
	  "NAME_IS_REQUIRED" : "输入名称",
	  "PASSWORD_IS_REQUIRED" : "输入密码",
	  "GROUP_IS_REQUIRED" : "输入群组",
	  "PERMISSION" : "权限",
	  "PERMISSION_IS_REQUIRED" : "输入权限",
	  "VALIDATE_TAG_CONFIGURATION" : "确认标签设定",
	  "VALIDATE_TAG_RESULT" : "标签确认结果",
	  "TAG_VALIDATION_MESSAGE" : "标签确认讯息",
	  "DELETE_FROM_NETWORK" : "刪除网络終端",
	  "USE_GROUP_ID" : "群组Id",
	  "USE_SENSOR_ID" : "传感器Id",
	  "CORRECT" : "更正",
	  "DIAGNOSTIC_SENSOR_VIEW" : "测试传感器数据",
	  "TAG_SENSOR_VIEW" : "传感器数据",
	  "DIAGNOSTIC_CHART_VIEW" : "测试统计图",
	  "SELECT_DEVICE_ID" : "选择終端Id",
	  "SELECT_SENSOR_ID" : "选择传感器Id",
	  "USE_OUT_OF_RANGE_TRIGGER_ALERT" : "超出指定范围触发警报",
	  "ALERT_CRITERIA" : "警报條件",
	  "ALERT_CRITERIA_NAME" : "警报條件名称",
	  "ALERT_CRITERIA_IS_REQUIRED" : "输入警报條件",	  
	  "ALERT_CRITERIA_NAME_IS_REQUIRED" : "输入警报條件名称",
	  "EDIT_ALERT_CRITERIA" : "警报條件设定",
	  "SELECT_SENSOR_TYPE" : "选择传感器类型",
	  "ENGLISH" : "英语",
	  "SIMPLIFIED_CHINESE" : "简体中文",
	  "CONFIRM_PASSWORD" : "确认密码",
	  "UPDATE_MEASURE_PERIOD" : "更新测量间隔",
	  "UPDATE_HEART_BEAT" : "更新心跳间隔",
	  "GROUP_NAME" : "群组名称",
	  "GROUP_NAME_IS_REQUIRED" : "输入群组名称",
	  "ASSIGNED_FUNCTION" : "分配功能",
	  "FUNCTION" : "功能",
	  "MENU" : "列表",
	  "ALL" : "全部",
	  "MODIFY" : "更改",
	  "READONLY" : "阅读",
	  "COVER_LEVEL" : "渠蓋位置",
	  "WATER_LEVEL" : "水位位置",
	  "H2S" : "硫化氢",
	  "SO2" : "二氧化硫",
	  "CO2" : "二氧化碳",
	  "CH4" : "甲烷",
	  "NH3" : "氨气",
	  "O2" : "氧气",
	  "RSSI" : "Rssi",
	  "PACKET_COUNTER" : "顺序号",
	  "REFRESH" : "更新",
	  "CONFIG" : "设定",
	  "GROUP" : "群组",
	  "TIME" : "时间",
	  "LOW_BATTERY" : "电池量不",
	  "Y-AXIS-MIN" : "Y-axis 最細值",
	  "Y-AXIS-MAX" : "Y-axis 最大值",
	  "COVER_DETECT" : "渠蓋检测",
	  "WATER_LEVEL_1_DETECT" : "水位测试1",
	  "WATER_LEVEL_2_DETECT" : "水位测试2",
 	  "ANTENNA_OPTIMIZED" : "优化天线",
 	  "MAX_STEP" : "Max Step",
 	  "USER_DEFINE" : "User Define",
 	  "ADC_VALUE" : "Adc Value",
 	  "BATTERY" : "电池",
 	  "SELECT_VIEW_TYPE" : "选择视图类型",
 	  "REAL_TIME_CHART_VIEW" : "即时数据统计图",
 	  "MINUTE" : "分钟",
 	  "HOUR" : "小时",
 	  "DAY" : "天",
 	  "WEEK" : "周",
 	  "MONTH" : "月",
 	  "YEAR" : "年",
 	  "ALL" : "全陪",
 	  "OPEN_ALERT_SETTING" : "警报设定",
 	  "CLOSE" : "关闭",
 	  "LOADING" : "下载中",
 	  "MINS_AGO" : "分钟前",
 	  "HOURS_AGO" : "小时前",
 	  "OPEN_TAG_LOCATION" : "打开标签位置",
 	  "SENSOR_DATA_OPTIONS" : "传感器数据设定",
 	  "READER_WITH_HIGHEST_RSSI" : "接收器最高值Rssi",
 	  "ASSIGN_TO_SPECIFIC_READERS" : "分配给特定接收器",
 	  "UP" : "上",
 	  "DOWN" : "下",
 	  "SEQUENCE_NO" : "序列号",
 	  "LATEST_DATA_FROM_READERS" : "接收器最后数据",
 	  "TAG_LOCATION" : "标签位置",
 	  "LOADING_PLEASE_WAIT" : "下载中, 请稍候",
 	  "NO_DATA_FOUND" : "找不到数据",
 	  "EXPORT_TO_CSV" : "导出到csv文件",
 	  "VERSION" : "版本",
 	  "READER_RSSI" : "接收器:Rssi",
 	  "READER_NAME_RSSI" : "Name:Rssi",
	  "READER_RSSI_SNR" : "Reader:Rssi|Snr",
	  "READER_NAME_RSSI_SNR" : "Name:Rssi|Snr",
 	  "READER_HEART_BEAT_VIEW" : "接收器心跳",
 	  "OPEN" : "开",
 	  "CLOSED" : "关",
 	  "ADD_PIPE" : "增加管道",
 	  "EDIT_PIPE" : "更新管道",
 	  "ADD_POINT" : "增加点",
 	  "DELETE_POINT" : "删除点",
 	  "ADD_LINE" : "增加线",
 	  "COMPLETE_LINE" : "完成线",
 	  "IMPORT_LINE_FROM_CSV_FILE" : "导入csv文件",
 	  "READER_NAME" : "Reader Name",
 	  "WATER_LEVEL_CHART_TYPE" : "水位图类型",
 	  "FROM_TOP" : "从顶部",
 	  "FROM_BOTTOM" : "从底部",
	  "MAN_HOLE_ID" : "沙井Id",
	  "MAN_HOLE_HEIGHT" : "沙井高度",
	  "HEIGHT" : "高度"
  });
	
//  $translateProvider.useStaticFilesLoader({
//    prefix: 'data/locale-',
//    suffix: '.json'
//  })
  
  $translateProvider.preferredLanguage('en');
});

//Only for Main Page function
angular.module('dsd').controller('mainCtrl', ['$rootScope','$interval','$translate','$scope', '$uibModal', '$log', '$http', '$sce', '$location', '$compile', 'uiGridConstants', 'appMainService', 'ngDialog', '$route',
                                                function ($rootScope,$interval,$translate, $scope, $uibModal, $log, $http, $sce, $location, $compile, uiGridConstants, appMainService, ngDialog, $route) {

	$scope.updateSystemCurrentTime = function () {
		var now = new Date();
		$scope.systemCurrentTime = now.format().toString();
	};
    $interval($scope.updateSystemCurrentTime, 1000);

    $scope.alreadyTimeoutDisplay = false;
    
    $scope.checkUserSessionTimeout = function () {
    	
	   if (  $scope.alreadyTimeoutDisplay )
			return; 
	   
	   $http({
	       url: appMainService.getDomainAddress() + '/rest/user/getCheckSessionTimeout/',
	       method: 'GET',
	   }).then(function (response) {
		   if ( !response.data.isValid && !$scope.alreadyTimeoutDisplay) {
			   $scope.alreadyTimeoutDisplay = !response.data.isValid;
			   appMainService.displaySessionTimeoutResult(response.data);
		   } 
	   },function (response){
			alert("Failed to check session, status=" + response.status);
	   });
	};
	if ( appMainService.getStartCheckSessionTimeout() == false) {
		$interval($scope.checkUserSessionTimeout, 60000);
		appMainService.setStartCheckSessionTimeout(true);
	}
	
	$scope.displaySessionTimeoutError = function () {
		if ( $scope.alreadyTimeoutDisplay || ! $location.search().isLogin )
 			return; 
    	
    	var validationObject = {}; 
    	validationObject.isValid = false;
    	validationObject.validationMessages = "User session has expired.  Please login again";
    	validationObject.validationSubject = "User session";
    	
    	appMainService.displaySessionTimeoutResult(response.data);
	};
	

	
	$scope.sendMessage = function () {
	    var user = document.getElementById("userName").value.trim();
	    if (user === "")
	        alert ("Please enter your name!");
	    
	    var inputElement = document.getElementById("messageInput");
	    var message = inputElement.value.trim();
	    if (message !== "") {
	        var jsonObj = {"user" : user, "message" : message};
	        chatClient.send(JSON.stringify(jsonObj));
	        inputElement.value = "";
	    }
	    inputElement.focus();
	}
	
	if ($location.search().isLogin && !appMainService.getLogin() ) {
	   appMainService.setLogin(true);
       $rootScope.connectWebSocketData();
       
	   $http({
	       url: appMainService.getDomainAddress() + '/rest/user/getLoginUser/',
	       method: 'GET',
	   }).then(function (response) {
		   $scope.currentUser = response.data;
	       $scope.currentPermission = JSON.parse($scope.currentUser.permissionMap);
	       $scope.displayPassword = "";
	       
	       if (appMainService.getLoginLang() == null ) {
	    	   $scope.changeLanguage($scope.currentUser.userProfile.locale); 
	       } else {
	    	   $scope.changeLanguage(appMainService.getLoginLang()); 
	       }
	       
	       if ($scope.currentUser.pwdResetAfterLogin) { 
	         	$scope.editUserLoginId = $scope.currentUser.username;
	           ngDialog.openConfirm({template: 'view/administration/user/userPasswordView.html',
		        	scope: $scope //Pass the scope object if you need to access in the template
		        }).then(
		       		//Update
		       		function(value) {
		       			if (value.displayPassword != "") {
		       				$http({
		        		        url: appMainService.getDomainAddress() + '/rest/user/updateUserPassword/',
		        		        method: 'PUT',
		        		        params: {
		        		        	id: $scope.currentUser.id,
		        		        	password: Sha256.hash(value.displayPassword)
		        			        }
		        		    }).then(function (response) {
		        		    	alert("Edit user password success");
		        		    },function (response){
		        		    	alert("Failed to edit user password, status=" + response.status);
		        		    });
		        		}		
		        	},
		       		//Cancel
		       		function(value) {
		      		}		
		       	);
	       }     
	   },function (response){
		   alert("Failed to get login user, status=" + response.status);
	   });
	}
    
	$scope.section="";
	$scope.users=[];
	$scope.roles=[];
	$scope.modules = [];
	$scope.applications = [];
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.userTotalItems = 100;
	$scope.userCurrentPage = 1;
	
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
	$scope.lang = "en";
	
	$scope.currentLanguage = "EN";
	
	$scope.TAG_NAME = "";
	
	$scope.changeLanguage = function (langKey) {
		if (langKey == "en")
			$scope.currentLanguage = "中文";
		else
			$scope.currentLanguage = "EN";
		
		$translate.use(langKey);
	    $scope.lang = langKey;
	    appMainService.broadcastItem2("updateLanguageInUiGrid", langKey);
	};
	
	$scope.languageToggle = function (langKey) {
			
		if ("en" == langKey ) {
			langKey = "zh";
		    $scope.currentLanguage = "EN";
		}
	    else {
	    	langKey = "en";
	    	$scope.currentLanguage = "中文";
	    }
		appMainService.setLangKey(langKey);
	    $scope.changeLanguage(langKey);
	    $scope.currentUser.userProfile.locale = langKey;
	};
		  
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	$scope.getTableStyle= function() {
        var marginHeight = 50;
        var length = $scope.users.length; 
        return {
            height: (length * $scope.userGridOption.rowHeight + $scope.userGridOption.headerRowHeight + marginHeight ) + "px"
        };
    };
	
	
	$scope.getStaticData = function (dataName){
		$http({
			url: appMainService.getDomainAddress() + '/rest/staticData/searchData/',
			method: 'GET',
			params: {
				dataName:dataName
			}
		}).then(function (response) {
			if (response.data.length > 0){
				$scope.staticData[dataName] = response.data[0];
			}
		},function (response){
			alert("Failed to search data, status=" + response.status);
		});
	};
	$scope.getStaticData("mainTitle");
	$scope.getStaticData("welcomeMessage");
	
    $scope.searchUser = function (searchId, searchChiName, searchEngName, searchLoginId, searchEmail, searchDept) { 	
    	$http({
            url: appMainService.getDomainAddress() + '/rest/user/searchData/',
            method: 'GET',
            params: {
            	id: searchId, 
            	chiName: searchChiName, 
            	engName: searchEngName, 
            	userName: searchLoginId, 
            	email: searchEmail, 
            	dept: searchDept
            }
        }).then(function (response) {
            $scope.users = response.data;
            //  $scope.userTotalItems = response.data.length;
        },function (response){
        	alert("Failed to search data, status=" + response.status);
        });
    };
    
    $scope.searchMenu = function (searchText){
    	$http({
            url: appMainService.getDomainAddress() + '/rest/user/getModuleAndMenus/',
            method: 'GET',
            params: {
            	searchText: searchText
            }
        }).then(function (response) {
            $scope.modules = response.data;
        },function (response){
        	$scope.displaySessionTimeoutError();
        });
    };
    
    $scope.loadApplicationLink = function () {
    	
    	$http({
    		url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
    		method: 'GET',
            params: {
            id: null, 
            name: null, 
            description: null,
            defaultSite: null
           }
    	}).then(function (response) {
    		var stringBuffer = "";
	    	stringBuffer = stringBuffer.concat("<table class=\"app\">");
	    	$scope.applications = response.data;
    	},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
	}

    $scope.refresh = function () {
    	$scope.searchMenu(null);
    	$http({
            url: appMainService.getDomainAddress() + '/rest/user/getLoginUser/',
            method: 'GET',
        }).then(function (response) {
            $scope.currentUser = response.data;       
            
 	       if (appMainService.getLoginLang() == null ) {
	    	   $scope.changeLanguage($scope.currentUser.userProfile.locale); 
	       } else {
	    	   $scope.changeLanguage(appMainService.getLoginLang()); 
	       }
 	        
            $scope.currentPermission = JSON.parse($scope.currentUser.permissionMap);
        },function (response){
        	$scope.displaySessionTimeoutError();
        });
    };
    $scope.refresh();
   	
    $scope.resetUser = function () {
    	$scope.searchId = "";
    	$scope.searchChiName = "";
    	$scope.searchEngName = "";
    	$scope.searchLoginId = "";
    	$scope.searchEmail = "";
    };
    
    //User Settings
    $scope.showUserSetting = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'view/userSettingView.html',
			controller: 'UserSettingCtrl',
			size: 'lg',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
	
	$scope.navigateToLink = function(fullLink) {
		$scope.httpLink = {
			url: fullLink
		};
		$scope.linkUrl = $sce.trustAsResourceUrl($scope.httpLink.url);
		$location.path('SystemDefinition/ApplicationExternalLink/');
	}
		
	
	$scope.loadApplicationLink();
	$scope.$on('handleGeneralBroadcast', function() {
		$scope.loadApplicationLink();
    });   
	$scope.$on('menuUpdatedInModuleEvent', function() {
		$scope.refresh();
    });   
	
	
	// Application lists
	$scope.applicationList= {URL: 'view/applicationList.html'};
	$scope.userButtonList= {URL: 'view/userButtonList.html'};
	$scope.userAlertList= {URL: 'view/userAlertList.html'};
	
	$scope.userViewList= {URL: 'view/userViewList.html'};

	
	$scope.navigateToMapView = function(siteConfig) {
		appMainService.setSiteConfiguration(siteConfig);
		$location.path('View/FloorPlanView/');
	};
	
	$scope.cancel = function () {
		window.history.back();
	};
	
  	
  	$scope.runModule = function (moduleId) {
		var path = 'ListMenu/';
		path += (moduleId+"");
		$location.path(path);    		
	};
	
	$scope.resetHistory = function() {
		appMainService.reset();
	};
	
	$scope.updateUserProfile = function() {
				
		$scope.languageOptions = [{locale: 'en', label: $translate.instant('ENGLISH')}, { locale: 'zh',label: $translate.instant('SIMPLIFIED_CHINESE')}];	
		$scope.formData = new FormData();
		$scope.editUserLoginId = $scope.currentUser.username;
		//get the user profile again
		$http({
		    url: appMainService.getDomainAddress() + '/rest/user/findUserById/',
		    method: 'GET',
		    params: {
		    	id: $scope.currentUser.id
		    }
		}).then(function (response) {
			$scope.currentUser.userProfile = response.data.userProfile;
			if ($scope.currentUser.userProfile.locale == 'en')
				$scope.formData.language = $scope.languageOptions[0];
			if ($scope.currentUser.userProfile.locale == 'zh')
				$scope.formData.language = $scope.languageOptions[1];
			
			ngDialog.openConfirm({template: 'view/administration/user/userProfileView.html',
				scope: $scope //Pass the scope object if you need to access in the template
			}).then(
				//Update
				function(value) {
					if (value.language.locale != null && value.language.locale != '') {
						
						$scope.currentUser.userProfile.locale = value.language.locale;
						$http({
					        url: appMainService.getDomainAddress() + '/rest/user/updateUserProfile/',
					        method: 'PUT',
					        params: {
					        	userId: $scope.currentUser.id,
					        	userProfileId: $scope.currentUser.userProfile.id,
					        	locale: $scope.currentUser.userProfile.locale
					        }
					    }).then(function (response) {
					    	$scope.changeLanguage($scope.currentUser.userProfile.locale);
					      	alert("Edit user profile success");
					    },function (response){
					    	alert("Failed to edit user profile, status=" + response.status);
					    });
					}		
				},
				//Cancel
				function(value) {
				}		
			);
		},function (response){
			  alert("Failed to get user profile with user id (ID = "+$scope.currentUser.id+"), status=" + response.status);
		});
	};
	
	$http({
		url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
		method: 'GET',
        params: {
        id: null, 
        name: null, 
        description: null,
        defaultSite: true
       }
	}).then(function (response) {
    	var siteConfigurations = response.data;
    	if (siteConfigurations != null && siteConfigurations.length > 0) {
    		$scope.navigateToMapView(siteConfigurations[0]);
    	}
	},function (response){
 		alert("Failed to search data, status=" + response.status);
	});
	
	$rootScope.sideMenuOn = false;

	$scope.sideMenuOn = function() {
		$rootScope.sideMenuOn = true;	
		$rootScope.displayDashBoardChartView(true);
	};
	
	$scope.sideMenuOff = function() {
		$rootScope.sideMenuOn = false;
		$rootScope.displayDashBoardChartView(false);
	};
	
	$scope.selectDashBoardChartView = function() {
		 $scope.isOpen = false;  
	};
	
	$scope.getIcon = function (alert) {
		if (alert.alertSettingLevel == "Critical") {
			return "img/basic/criticalalert2.png";
		}
		if (alert.alertSettingLevel == "High") {
			return "img/basic/highalert2.png";
		}
		if (alert.alertSettingLevel == "Medium") {
			return "img/basic/mediumalert2.png";
		}
		return "img/basic/lowalert2.png";
		
	};
	
	$rootScope.realTimeAlerts = [];
	$scope.$on('receiveAlert', function(event, alert) {

		if ( $rootScope.realTimeAlerts.length >= 100) 
			return; 
		
		var dateLong = new Date().getTime();

		alert.dateLong = dateLong;
		alert.receiveMinsStr ="0 " + $translate.instant('MINS_AGO');
		alert.isRead = false;
		alert.imagePath = $scope.getIcon(alert);
		alert.index = $rootScope.realTimeAlerts.length;
		
		$rootScope.realTimeAlerts.push(alert);
	});
	
	$rootScope.calculateTimeInAlerts = function() {
		var nowLong = new Date().getTime();
		for (var i = 0; i < $rootScope.realTimeAlerts.length; ++i ) {
			var alert = $rootScope.realTimeAlerts[i];
			var minsAgo =  Math.round( (nowLong - alert.dateLong) / 60000 );
			if ( minsAgo > 60 ) {
				var hrsAgo = Math.round( minsAgo / 60 );
				alert.receiveMinsStr = hrsAgo + " " + $translate.instant('HOURS_AGO');
			} else {
				alert.receiveMinsStr = minsAgo + " " + $translate.instant('MINS_AGO');
			}
		}
	}
	$interval($rootScope.calculateTimeInAlerts, 60000);
	
	$rootScope.openAlertSetting = function(alertSettingId) {
		$http({
	   		url: appMainService.getDomainAddress() + '/rest/alertSetting/getAlertSetting/',
	   		method: 'GET',
         params: {
             id: alertSettingId,
             name: null,
             level: null,
             useDateRange: null,
             fromDate: null,
             toDate: null,
             deviceAddress: null,
             deviceId : null,
             groupId : null,
             isSendEmail: null,
             isCoverLevel: null,
             isWaterLevel: null,
             isH2s: null,
             isSo2: null,
             isCo2: null,
             isCh4: null,
             isNh3: null,
             isO2: null
            }
		}).then(function (response) {
			if (response.data == null || response.data.length == 0 ) {
				return; 
			}
			var data = response.data[0];
			appMainService.setAlertSetting(data);
			 
			var path = 'Alert/AlertSetting/New/';
			$location.path(path); 
				
	   	},function (response){
	    	alert("Failed to search data, status=" + response.status);
	   	});
	}
	
	$rootScope.onRealTimeAlert = function(alert) {
		alert.isRead = true;
		
		var path = 'view/alert/alertDialogView.html'; 
	
		$rootScope.formData = new FormData();
		$rootScope.formData.alert = alert;
		
		ngDialog.openConfirm({template: path,
			scope: $rootScope //Pass the scope object if you need to access in the template
		}).then(
			//Update
			function(value) {
				if ( value == true) {
					appMainService.setDashBoardSelectedTagConfigurationId($rootScope.formData.alert.tagConfigurationId);
					if ($location.path() === "/DashBoard") {
						$route.reload();
					} else {
						$location.path("/DashBoard");
					}
					return;
				} else {
					$rootScope.openAlertSetting($scope.formData.alert.alertSettingId);
				}
			},
			//Cancel
			function(value) {
			}		
		);	
	};
	
	$rootScope.deleteRealTimeAlert = function(alert) {
		$rootScope.realTimeAlerts.splice(alert.index, 1);
	}
	
	$rootScope.goToAlertHistory = function(alert) {
		appMainService.setSearchAlertData(true);
		
		$location.path("Alert/AlertHistory");
	}
	
	$rootScope.displayDashBoardChartView  = function (sideMenuOnOrOff) {
		$rootScope.$broadcast('dashBoardViewToggle', sideMenuOnOrOff);
	}
}]);

//Only for Main Page function
angular.module('dsd').controller('loginCtrl', ['$rootScope','$interval','$translate','$scope', '$uibModal', '$log', '$http', '$sce', '$location', '$compile', 'uiGridConstants', 'appMainService', 'ngDialog',
                                                function ($rootScope,$interval,$translate, $scope, $uibModal, $log, $http, $sce, $location, $compile, uiGridConstants, appMainService, ngDialog) {
	
	$rootScope.disconnectWebSocketData();

	$scope.error = "";
	if ($location.search().isAuthenticationError ) {
		$scope.error = "MSG_LOGIN_FAIL";
		$location.url($location.path());
	}
	
	$scope.changeLanguage = function (langKey) {
			if (langKey == "en")
			$scope.currentLanguage = "中文";
		else
			$scope.currentLanguage = "EN";
		
		localStorage.setItem("loginLang", langKey);
			
		$translate.use(langKey);
		appMainService.broadcastItem2("updateLanguageInUiGrid", langKey);
	};
	
	var loginLang = localStorage.getItem("loginLang");
	if (loginLang != "" && loginLang != null ) {
		$scope.changeLanguage(loginLang);
	}
	
}]);
