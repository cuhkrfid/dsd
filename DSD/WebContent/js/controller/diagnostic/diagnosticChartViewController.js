angular.module('dsd').controller('diagnosticChartViewCtrl', ['$rootScope', '$scope', '$routeParams','$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants','appMainService',
 function ($rootScope, $scope, $routeParams, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService) {
	
	$scope.yAxisMinValue = -100;
    $scope.yAxisMaxValue = 100;
    
	var time = new Date().getTime();
	$scope.startDate = time;
	$scope.endDate = time;
	
	$scope.colors = ['#FF0000', '#0000FF', '#008000', '#FFA500', '#00CED1', '#D100CE', '#834FC3', '#C34F8F', '#8000ff'];
	
	$scope.editCoverLevel = false;
	$scope.editWaterLevel = false;
	$scope.editH2s = false;
	$scope.editSo2 = false;
	$scope.editCo2 = false;
	$scope.editCh4 = false;
	$scope.editNh3 = false;
	$scope.editO2 = false;
	
	$scope.connected = false;
	$scope.connectButtonText = "Connect";
	$scope.chartData = [];
	

	$scope.chartIntervalInMins = 5;
	$scope.chartLength = 60;   //5 mins x 60 secs / 5 secs
	
	$scope.deviceIdConfigurations = []; 
	$scope.sensorConfigurations = [];
	
	$scope.selectDeviceId = 0;
	$scope.selectSensor = null;

	$scope.getSensor = function(id) {
		$http({  
			url: appMainService.getDomainAddress() + '/rest/alertSetting/getSensor/',
			method: 'GET',
		    params: {
		    	id: id,
	        }
		}).then(function (response) {
			$scope.selectSensor= response.data;
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});	
	};
	
	var selectedTag = appMainService.getTagConfiguration();
	if ( selectedTag != null) {
		$scope.editCoverLevel = selectedTag.isCoverLevel;
		$scope.editWaterLevel = selectedTag.isWaterLevel;
		$scope.editH2s = selectedTag.isH2s;
		$scope.editSo2 = selectedTag.isSo2;
		$scope.editCo2 = selectedTag.isCo2;		
		$scope.editCh4 = selectedTag.isCh4;
		$scope.editNh3 = selectedTag.isNh3;		
		$scope.editO2 = selectedTag.isO2;
		$scope.editRssi = false;
		
		$scope.selectDeviceId = selectedTag.deviceId;
		$scope.getSensor(selectedTag.sensorId);
	}
	
	$scope.onDeviceIdChanged = function(selectDeviceId) {
		$scope.getAllSensorId( selectDeviceId, false);
	};
	
	$scope.updateNumOfSensorSelected = function() {
		
		$scope.numOfSensorSelected = 0;
		if ( $scope.editCoverLevel ) 
			++$scope.numOfSensorSelected;
		
		if ( $scope.editWaterLevel ) 
			++$scope.numOfSensorSelected;
		
		if ( $scope.editH2s ) 
			++$scope.numOfSensorSelected;
		
		if ( $scope.editSo2 ) 
			++$scope.numOfSensorSelected;
		
		if ( $scope.editCo2 ) 
			++$scope.numOfSensorSelected;		

		if ( $scope.editCh4 ) 
			++$scope.numOfSensorSelected;	
		
		if ( $scope.editNh3 ) 
			++$scope.numOfSensorSelected;	
	
		if ( $scope.editO2 ) 
			++$scope.numOfSensorSelected;	
		
		if ( $scope.editRssi ) 
			++$scope.numOfSensorSelected;	
	};
	$scope.updateNumOfSensorSelected();
	
	$scope.onSensorChanged = function(selectSensor) {
		if ( selectSensor == null)
			return;
		
		$scope.editCoverLevel = selectSensor.isCoverLevel;
		$scope.editWaterLevel = selectSensor.isWaterLevel;
		$scope.editH2s = selectSensor.isH2s;
		$scope.editSo2 = selectSensor.isSo2;
		$scope.editCo2 = selectSensor.isCo2;		
		$scope.editCh4 = selectSensor.isCh4;
		$scope.editNh3 = selectSensor.isNh3;		
		$scope.editO2 = selectSensor.isO2;
		$scope.editRssi = true;

	};	
	
	$scope.getAllDeviceId = function() {
		var date = new Date();
		var startDate = new Date(date.getTime() - 24*60*60*1000);
		$http({  
			url: appMainService.getDomainAddress() + '/rest/diagnosticView/getAllDeviceId/',
			params: {
				dateFrom: startDate.getTime().toString(),
				dateTo: date.getTime().toString(),
			},
			method: 'GET',
		}).then(function (response) {
			$scope.deviceIdConfigurations = response.data;
			for (var i = 0; i <  $scope.deviceIdConfigurations.length; ++i) {
				if ( $scope.deviceIdConfigurations[i] == appMainService.getDiagnosticChartViewDeviceId()) {
					$scope.selectDeviceId = $scope.deviceIdConfigurations[i];
					$scope.onDeviceIdChanged($scope.selectDeviceId);
					break;
				}
			}
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});	
	};
	$scope.getAllDeviceId(); 
	
	$scope.getAllSensorId = function(deviceId, select) {
		$http({  
			url: appMainService.getDomainAddress() + '/rest/diagnosticView/getAllSensor/',
			method: 'GET',
	        params: {
		        deviceId: deviceId
	       }
		}).then(function (response) {
			$scope.sensorConfigurations = response.data;
			if (!select)
				return;
			
			for (var i = 0; i <  $scope.sensorConfigurations.length; ++i) {
				if ( $scope.sensorConfigurations[i].sensorId == appMainService.getDiagnosticChartViewSensorId()) {
					$scope.selectSensor = $scope.sensorConfigurations[i];
					break;
				}
			}
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});	
	}
	$scope.getAllSensorId( $scope.selectDeviceId, true);

	
	$scope.connectData = function(deviceId, sensorId) {
		if (deviceId == null || sensorId == null) {
			return;
		}
		
		if ($scope.connected) {
			$scope.connectButtonText = "Connect";
			$scope.connected = false;
			return;
		}
		$http({  
			url: appMainService.getDomainAddress() + '/rest/diagnosticView/webSocketViewDataByDeviceSensor/',
			method: 'GET',
	        params: {
		        deviceId: deviceId,
		        sensorId: sensorId
	       }
		}).then(function (response) {
			$scope.chartData = [];
			$scope.connected = true;
			$scope.connectButtonText = "Disconnect";
			$scope.drawChart();		
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});	
	};


	$scope.initChartSettings = function (chart) {
	   
		var categoryAxesSettings = new AmCharts.CategoryAxesSettings();
	     categoryAxesSettings.dashLength = 5;
	     categoryAxesSettings.minPeriod = "ss";
	        
	     categoryAxesSettings.maxSeries= $scope.chartIntervalInMins*60/5,
	     chart.categoryAxesSettings = categoryAxesSettings;
	
	     var valueAxesSettings = new AmCharts.ValueAxesSettings();
	     valueAxesSettings.dashLength = 5;
	     valueAxesSettings.inside = false;
	     chart.valueAxesSettings = valueAxesSettings;
	
	     
	     var chartCursorSettings = new AmCharts.ChartCursorSettings();
	     chartCursorSettings.valueBalloonsEnabled = true;
	     chart.chartCursorSettings = chartCursorSettings;
	
	     var periodSelector = new AmCharts.PeriodSelector();
	     periodSelector.periods = [{
	         period: "DD",
	         count: 1,
	         label: "1 day"
	     }, {
	         period: "DD",
	         selected: true,
	         count: 5,
	         label: "5 days"
	     }, {
	         period: "MM",
	         count: 1,
	         label: "1 month"
	     }, {
	         period: "YYYY",
	         count: 1,
	         label: "1 year"
	     }, {
	         period: "YTD",
	         label: "YTD"
	     }, {
	         period: "MAX",
	         label: "MAX"
	     }];
	     chart.periodSelector = periodSelector;
	}

	$scope.toggleRealTime = function() {
	};
	
	$scope.drawChart = function() {
		
		 var fieldMappings = [];
		
	     $scope.chart = new AmCharts.AmStockChart();
	     $scope.chart.pathToImages = "./js/amcharts/images/";
	     $scope.chart.export =  {
	          "enabled": true,
	          "position": "bottom-right",
	          "menu": [
	            {
		            "format": "JPG",
		            "label": "Save as JPG",
		            "title": "Export chart to JPG"}, 
	            {   "format": "PNG",
		            "label": "Save as PNG",
		            "title": "Export chart to PNG"},
		        {   "format": "CSV",
			        "label": "Save as CSV",
			        "title": "Export chart to CSV"}
		       ]}; 
	     
//	     $scope.chart.export =  {
//	    	"enabled": true,
//	        "position": "top-right"
//	     }; 
	     
	     $scope.dataSet = new AmCharts.DataSet();
	     $scope.dataSet.title = "Sensor values"
	     $scope.dataSet.dataProvider = $scope.chartData;

	     
	     $scope.dataSet.fieldMappings = [];
	     if ( $scope.editCoverLevel ) {	
	    	 $scope.dataSet.fieldMappings.push({fromField: "coverLevel", toField: "coverLevel" });
		 }
		 if ( $scope.editWaterLevel ) {		 
			 $scope.dataSet.fieldMappings.push({fromField: "waterLevel", toField: "waterLevel" });
		 }
	     if ( $scope.editH2s ) {	
	     	$scope.dataSet.fieldMappings.push({fromField: "h2s", toField: "h2s" });
	     }
	     if ( $scope.editSo2 ) {		 
	    	 $scope.dataSet.fieldMappings.push({fromField: "so2", toField: "so2" });
	     }
	     if ( $scope.editCo2 ) {		 	 
	    	 $scope.dataSet.fieldMappings.push({fromField: "co2", toField: "co2" });
	     }
	     if ( $scope.editCh4 ) {		 	 
	    	 $scope.dataSet.fieldMappings.push({fromField: "ch4", toField: "ch4" });
	     }
	     if ( $scope.editNh3 ) {		 	 
	    	 $scope.dataSet.fieldMappings.push({fromField: "nh3", toField: "nh3" });
	     }
	     if ( $scope.editO2 ) {		 	 
	    	 $scope.dataSet.fieldMappings.push({fromField: "o2", toField: "o2" });
	     }
	     if ( $scope.editRssi ) {		 	 
	    	 $scope.dataSet.fieldMappings.push({fromField: "rssi", toField: "rssi" });
	     }
	     	     
	     
	     $scope.dataSet.categoryField = "date";
	     $scope.chart.dataSets = [$scope.dataSet];
	
	     var stockPanel = new AmCharts.StockPanel();
	     stockPanel.title = "Data set";
	     stockPanel.showCategoryAxis = true;
	     stockPanel.percentHeight = 60;
	     stockPanel.autoMargins=true;
	     var valueAxis0 = null
	     if ( $scope.editCoverLevel ) {
	    	 
	    	 var valueAxis = new AmCharts.ValueAxis();
	    	 valueAxis.position = "left"; 
	    	 valueAxis.axisColor = $scope.colors[0];
		     valueAxis.color = $scope.colors[0];
		     valueAxis.axisThickness = 1;
		     valueAxis.offset = 0;
		     stockPanel.addValueAxis(valueAxis);
		     valueAxis0 = valueAxis;
	     }
	
	     var valueAxis1 = null;
	     if ( $scope.editWaterLevel ) {	 
	     	var valueAxis = new AmCharts.ValueAxis();
		     valueAxis.position = "right"; // this line makes the axis to appear on the right
		     valueAxis.axisColor = $scope.colors[1];
		     valueAxis.color = $scope.colors[1];
		     valueAxis.axisThickness = 1;
		     valueAxis.offset = 0;
		     if ( $scope.numOfSensorSelected == 1) {
		    	 valueAxis.minimum = $scope.yAxisMinValue;
		    	 valueAxis.maximum = $scope.yAxisMaxValue;
		     } 
		     stockPanel.addValueAxis(valueAxis);
		     valueAxis1 = valueAxis;
	     }
	     
	     var valueAxis2 = null;
	     if ( $scope.editH2s ) {		  
	     	var valueAxis = new AmCharts.ValueAxis();
		     //valueAxis.position = "left"; // this line makes the axis to appear on the right
		     valueAxis.axisColor = $scope.colors[2];
		     valueAxis.color = $scope.colors[2];
		     valueAxis.axisThickness = 1;
		     valueAxis.offset = 30;
		     if ( $scope.numOfSensorSelected == 1) {
		    	 valueAxis.minimum = $scope.yAxisMinValue;
		    	 valueAxis.maximum = $scope.yAxisMaxValue;
		     } 
		     stockPanel.addValueAxis(valueAxis);
		     valueAxis2 = valueAxis;
	     }

	     var valueAxis3 = null;
	     if ( $scope.editSo2 ) {		 
	     	 var valueAxis = new AmCharts.ValueAxis();
		     valueAxis.position = "right"; // this line makes the axis to appear on the right
		     valueAxis.axisColor = $scope.colors[3];
		     valueAxis.color = $scope.colors[3];
		     valueAxis.axisThickness = 1;
		     valueAxis.offset = 30;
		     if ( $scope.numOfSensorSelected == 1) {
		    	 valueAxis.minimum = $scope.yAxisMinValue;
		    	 valueAxis.maximum = $scope.yAxisMaxValue;
		     } 
		     stockPanel.addValueAxis(valueAxis);
		     valueAxis3 = valueAxis;
	     }

	     var valueAxis4 = null;
	     if ( $scope.editCo2 ) { 
		     var valueAxis = new AmCharts.ValueAxis();
		     //valueAxis.position = "left"; // this line makes the axis to appear on the right
		     valueAxis.axisColor = $scope.colors[4];
		     valueAxis.color = $scope.colors[4];
		     valueAxis.axisThickness = 1;
		     valueAxis.offset = 60;
		     if ( $scope.numOfSensorSelected == 1) {
		    	 valueAxis.minimum = $scope.yAxisMinValue;
		    	 valueAxis.maximum = $scope.yAxisMaxValue;
		     } 
		     stockPanel.addValueAxis(valueAxis);
		     valueAxis4 = valueAxis;
	     }
	     
	     var valueAxis5 = null;
	     if ( $scope.editCh4 ) {		 
	     	 var valueAxis = new AmCharts.ValueAxis();
		     valueAxis.position = "right"; // this line makes the axis to appear on the right
		     valueAxis.axisColor = $scope.colors[5];
		     valueAxis.color = $scope.colors[5];
		     valueAxis.axisThickness = 1;
		     valueAxis.offset = 60;
		     if ( $scope.numOfSensorSelected == 1) {
		    	 valueAxis.minimum = $scope.yAxisMinValue;
		    	 valueAxis.maximum = $scope.yAxisMaxValue;
		     } 
		     stockPanel.addValueAxis(valueAxis);
		     valueAxis5 = valueAxis;
	     }
	     
	     var valueAxis6 = null;
	     if ( $scope.editNh3 ) { 
		     var valueAxis = new AmCharts.ValueAxis();
		     //valueAxis.position = "left"; // this line makes the axis to appear on the right
		     valueAxis.axisColor = $scope.colors[6];
		     valueAxis.color = $scope.colors[6];
		     valueAxis.axisThickness = 1;
		     valueAxis.offset = 90;
		     if ( $scope.numOfSensorSelected == 1) {
		    	 valueAxis.minimum = $scope.yAxisMinValue;
		    	 valueAxis.maximum = $scope.yAxisMaxValue;
		     } 
		     stockPanel.addValueAxis(valueAxis);
		     valueAxis6 = valueAxis;
	     }

	     var valueAxis7 = null;
	     if ( $scope.editO2 ) { 
		     var valueAxis = new AmCharts.ValueAxis();
		     valueAxis.position = "right"; // this line makes the axis to appear on the right
		     valueAxis.axisColor = $scope.colors[7];
		     valueAxis.color = $scope.colors[7];
		     valueAxis.axisThickness = 1;
		     valueAxis.offset = 90;
		     if ( $scope.numOfSensorSelected == 1) {
		    	 valueAxis.minimum = $scope.yAxisMinValue;
		    	 valueAxis.maximum = $scope.yAxisMaxValue;
		     } 
		     stockPanel.addValueAxis(valueAxis);
		     valueAxis7 = valueAxis;
	     }
	     
	     var valueAxis8 = null;
	     if ( $scope.editRssi ) { 
		     var valueAxis = new AmCharts.ValueAxis();
		     valueAxis.position = "left"; // this line makes the axis to appear on the right
		     valueAxis.axisColor = $scope.colors[8];
		     valueAxis.color = $scope.colors[8];
		     valueAxis.axisThickness = 1;
		     valueAxis.offset = 120;
		     if ( $scope.numOfSensorSelected == 1) {
		    	 valueAxis.minimum = $scope.yAxisMinValue;
		    	 valueAxis.maximum = $scope.yAxisMaxValue;
		     } 
		     stockPanel.addValueAxis(valueAxis);
		     valueAxis8 = valueAxis;
	     }
	     
	     $scope.chart.panels = [stockPanel];
	 	
	     var legend = new AmCharts.StockLegend();
	     stockPanel.stockLegend = legend;
	
	     var panelsSettings = new AmCharts.PanelsSettings();
	     $scope.chart.panelsSettings = panelsSettings;
	
	     if ( $scope.editCoverLevel ) {
		     var graph = new AmCharts.StockGraph();
		     graph.valueField = "coverLevel";
		     graph.title = "Cover level";
		     graph.fillAlphas = 0;
		     graph.bullet = "square";
		     graph.bulletSize=1;
		     graph.balloonText = "[[title]]: <b>[[value]]</b>";
		     graph.valueAxis = valueAxis0;
		     graph.lineThickness = 1;
		     graph.useDataSetColors = false;
		     graph.lineColor = $scope.colors[0];
		     stockPanel.addStockGraph(graph);
	     }
	     
	     if ( $scope.editWaterLevel ) {
		     var graph = new AmCharts.StockGraph();
		     graph.valueField = "waterLevel";
		     graph.title = "Water level";
		     graph.fillAlphas = 0;
		     graph.bullet = "square";
		     graph.bulletSize=1;
		     graph.bulletBorderAlpha = 1;
		     graph.balloonText = "[[title]]: <b>[[value]]</b>";
		     graph.valueAxis = valueAxis1;
		     graph.lineThickness = 1;
		     graph.useDataSetColors = false;
		     graph.lineColor = $scope.colors[1];
		     stockPanel.addStockGraph(graph);
	     }
	    
	     if ( $scope.editH2s ) {
		     var graph = new AmCharts.StockGraph();
		     graph.valueField = "h2s";
		     graph.title = "H2s";
		     graph.fillAlphas = 0;
		     graph.bullet = "square";
		     graph.bulletSize=1;
		     graph.bulletBorderAlpha = 1;
		     graph.balloonText = "[[title]]: <b>[[value]]</b>";
		     graph.valueAxis = valueAxis2;
		     graph.lineThickness = 1;
		     graph.useDataSetColors = false;
		     graph.lineColor = $scope.colors[2];
		     stockPanel.addStockGraph(graph);
	     }
	     
	     if ( $scope.editSo2 ) {
		     var graph = new AmCharts.StockGraph();
		     graph.valueField = "so2";
		     graph.title = "SO2";
		     graph.fillAlphas = 0;
		     graph.bullet = "square";
		     graph.bulletSize=1;
		     graph.bulletBorderAlpha = 1;
		     graph.balloonText = "[[title]]: <b>[[value]]</b>";
		     graph.valueAxis = valueAxis3;
		     graph.lineThickness = 1;
		     graph.useDataSetColors = false;
		     graph.lineColor = $scope.colors[3];
		     stockPanel.addStockGraph(graph);
	     }
	     
	     if ( $scope.editCo2 ) {
		     var graph = new AmCharts.StockGraph();
		     graph.valueField = "co2";
		     graph.title = "Co2";
		     graph.fillAlphas = 0;
		     graph.bullet = "square";
		     graph.bulletSize=1;
		     graph.bulletBorderAlpha = 1;
		     graph.balloonText = "[[title]]: <b>[[value]]</b>";
		     graph.valueAxis = valueAxis4;
		     graph.lineThickness = 1;
		     graph.useDataSetColors = false;
		     graph.lineColor = $scope.colors[4];
		     stockPanel.addStockGraph(graph);
	     }
	     
	     if ( $scope.editCh4 ) {
		     var graph = new AmCharts.StockGraph();
		     graph.valueField = "ch4";
		     graph.title = "Ch4";
		     graph.fillAlphas = 0;
		     graph.bullet = "square";
		     graph.bulletSize=1;
		     graph.bulletBorderAlpha = 1;
		     graph.balloonText = "[[title]]: <b>[[value]]</b>";
		     graph.valueAxis = valueAxis5;
		     graph.lineThickness = 1;
		     graph.useDataSetColors = false;
		     graph.lineColor = $scope.colors[5];
		     stockPanel.addStockGraph(graph);
	     }
	     
	     if ( $scope.editNh3 ) {
		     var graph = new AmCharts.StockGraph();
		     graph.valueField = "nh3";
		     graph.title = "Nh3";
		     graph.fillAlphas = 0;
		     graph.bullet = "square";
		     graph.bulletSize=1;
		     graph.bulletBorderAlpha = 1;
		     graph.balloonText = "[[title]]: <b>[[value]]</b>";
		     graph.valueAxis = valueAxis6;
		     graph.lineThickness = 1;
		     graph.useDataSetColors = false;
		     graph.lineColor = $scope.colors[6];
		     stockPanel.addStockGraph(graph);
	     }
	     
	     if ( $scope.editO2 ) {
		     var graph = new AmCharts.StockGraph();
		     graph.valueField = "o2";
		     graph.title = "O2";
		     graph.fillAlphas = 0;
		     graph.bullet = "square";
		     graph.bulletSize=1;
		     graph.bulletBorderAlpha = 1;
		     graph.balloonText = "[[title]]: <b>[[value]]</b>";
		     graph.valueAxis = valueAxis7;
		     graph.lineThickness = 1;
		     graph.useDataSetColors = false;
		     graph.lineColor = $scope.colors[7];
		     stockPanel.addStockGraph(graph);
	     }
	     
	     if ( $scope.editRssi ) {
		     var graph = new AmCharts.StockGraph();
		     graph.valueField = "rssi";
		     graph.title = "Rssi";
		     graph.fillAlphas = 0;
		     graph.bullet = "square";
		     graph.bulletSize=1;
		     graph.bulletBorderAlpha = 1;
		     graph.balloonText = "[[title]]: <b>[[value]]</b>";
		     graph.valueAxis = valueAxis8;
		     graph.lineThickness = 1;
		     graph.useDataSetColors = false;
		     graph.lineColor = $scope.colors[8];
		     stockPanel.addStockGraph(graph);
	     }
	     
	     $scope.initChartSettings($scope.chart);
	     $scope.chart.write("chartdiv");	
	};

	$scope.$on('receiveSensorData', function(event, sensorDatas) {
		if ( $scope.connected == false ) {
			return;
		}
		
		for (var i = 0; i < sensorDatas.length; ++i) {
			var data = sensorDatas[i];
			if ($scope.selectDeviceId == data.deviceId && 
	   	 		$scope.selectSensor.sensorId == data.sensorId) {
 	   			
 	   			//$scope.chartData.splice(0, 1);
 	   			$scope.dataSet.dataProvider.push(data);	
 	   			$scope.chart.validateData();
 	   			$scope.chart.validateNow(); 

 	   			if ($scope.dataSet.dataProvider.length == 1 ) {
 	   				$scope.chart.zoom($scope.dataSet.dataProvider[0].date, $scope.dataSet.dataProvider[0].date );
 	   				return;
 	   			}
 	   			
 	   			if ($scope.dataSet.dataProvider.length > 1) {
 	   				$scope.chartLength = ($scope.chartIntervalInMins * 60 / (( new Date(data.date) - $scope.dataSet.dataProvider[$scope.dataSet.dataProvider.length-2].date ) / 1000));
 	   			}
 	   			
 	   			if ($scope.dataSet.dataProvider.length >= $scope.chartLength ) {
 	   				$scope.chartData.splice(0, 1);
 	   			}
 	   			
 	   			var newStartDateLong = $scope.dataSet.dataProvider[0].date;
 	   			var newEndDateLong = data.date;
 	   			var endDate = new Date(newEndDateLong);
 	   		
 	   			endDate.setSeconds(endDate.getSeconds() + 1);  //add one second;
 	   	   	   	$scope.chart.zoom(new Date(newStartDateLong), endDate);
 	   			 	   		
 	   		   	return;
 	   		}
		}
	});
	
	$rootScope.connectWebSocketData();

	$scope.userCancel = function () {
		window.history.back();
	};
	
	$scope.canPlot = function () { 
		if ($scope.numOfSensorSelected == 1) {
			if ( $scope.yAxisMinValue === "" || $scope.yAxisMaxValue === ""
				 || isNaN($scope.yAxisMinValue) ||  isNaN($scope.yAxisMaxValue) ) {
				return false;
			}
		}
		return true;
	};
	
 	$http({
   		url: appMainService.getDomainAddress() + '/rest/diagnosticView/webSocketViewDataByDeviceSensor/',
   		method: 'GET',
          params: {
        	  deviceId: '-1',
		      sensorId: '-1'
	      }
   	}).then(function (response) {

   	},function (response){
   	});
}]);