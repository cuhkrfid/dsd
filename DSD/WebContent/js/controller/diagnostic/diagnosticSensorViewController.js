angular.module('dsd').controller('diagnosticSensorViewCtrl', ['$rootScope', '$scope', '$translate','$routeParams', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', '$window', 'ngDialog', '$timeout',
 function ($rootScope, $scope, $translate, $routeParams,$uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, $window, ngDialog, $timeout) {

	$scope.rowHeight = 30; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 7;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows) - 1;
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
            {field:'dateString',  width: 140, displayName: $translate.instant('DATE'), headerCellClass: 'uiGridthemeHeaderColor', 
 		           cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.dateString}}</a></div>'},
 		 	{field:'readerIds',  width: 100, displayName: $translate.instant('READER_ID'), headerCellClass: 'uiGridthemeHeaderColor',
 	 			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.readerIds}}</a></div>',
				 	filter: { condition: uiGridConstants.filter.EXACT } },
 	 		{field:'deviceId',  width: 100, displayName: $translate.instant('DEVICE_ID'), headerCellClass: 'uiGridthemeHeaderColor',
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.deviceId}}</a></div>',
				 	filter: { condition: uiGridConstants.filter.EXACT } },
 			{field:'readerRssiSnr',  width: 300, displayName: $translate.instant('READER_RSSI_SNR'), headerCellClass: 'uiGridthemeHeaderColor',
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'"  data-toggle="tooltip" title="dBm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.readerRssiSnr}}</a></div>'},
			{field:'coverDetectStr',  width: 120, displayName: $translate.instant('COVER_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',
 	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.coverDetectStr}}</a></div>'},
 	 	 	{field:'waterLevel1DetectStr',  width: 85, displayName: $translate.instant('WATER_LEVEL_1_DETECT'),  headerCellClass: 'uiGridthemeHeaderColor',
 	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.waterLevel1DetectStr}}</a></div>'},		
 	 	 	{field:'waterLevel2DetectStr',  width: 85, displayName: $translate.instant('WATER_LEVEL_2_DETECT'),  headerCellClass: 'uiGridthemeHeaderColor',
 	  	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.waterLevel2DetectStr}}</a></div>'},		
 	  	 	{field:'antennaOptimizedStr',  width: 120, displayName: $translate.instant('ANTENNA_OPTIMIZED'), headerCellClass: 'uiGridthemeHeaderColor',
 	   	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.antennaOptimizedStr}}</a></div>'},		
 	    	{field:'userDefine',  width: 100, displayName: $translate.instant('USER_DEFINE'), headerCellClass: 'uiGridthemeHeaderColor',
 	 	  	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.userDefine}}</a></div>'},		
 	 	  	{field:'adcValue',  width: 110, displayName: $translate.instant('ADC_VALUE'), headerCellClass: 'uiGridthemeHeaderColor',
 	 	   	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.adcValue}}</a></div>'},	
			{field:'waterLevelFromZero2',  width: 115, displayName: $translate.instant('WATER_LEVEL'), headerCellClass: 'uiGridthemeHeaderColor',	
 	 		 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'"  data-toggle="tooltip" title="cm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.waterLevelFromZero2}}</a></div>'},
 	 	 	{field:'h2s',  width: 70, displayName: $translate.instant('H2S'), headerCellClass: 'uiGridthemeHeaderColor',
 	 		 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'"  data-toggle="tooltip" title="ppm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.h2s}}</a></div>'},
 			{field:'so2',  width: 70, displayName: $translate.instant('SO2'),  headerCellClass: 'uiGridthemeHeaderColor',	
 		 			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'"  data-toggle="tooltip" title="ppm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.so2}}</a></div>'},
 			{field:'ch4',  width: 80, displayName: $translate.instant('CH4'), headerCellClass: 'uiGridthemeHeaderColor',
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'"  data-toggle="tooltip" title="ppm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.ch4}}</a></div>'},
			{field:'batteryStr',  width: 90, displayName: $translate.instant('BATTERY'), headerCellClass: 'uiGridthemeHeaderColor',	
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.batteryStr}}</a></div>'},
		    {field:'packetCounter',  width: 90, displayName: $translate.instant('PACKET_COUNTER'),  headerCellClass: 'uiGridthemeHeaderColor',
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.packetCounter}}</a></div>'}, 	 
 	 	    {field:'measurePeriod',  width: 100, displayName: $translate.instant('PERIOD_S'), headerCellClass: 'uiGridthemeHeaderColor',
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.measurePeriod}}</a></div>'},
 			{field:'timeDiffInMs',  width: 130, displayName: $translate.instant('TIME_DIFF'),  headerCellClass: 'uiGridthemeHeaderColor',
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isRead ? \'col.colIndex()\' : \'col.colIndex()\'" ><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.timeDiffInMsStr}}</a></div>'},
 			{field:'details', displayName: $translate.instant('DETAILS'), enableFiltering : false, width: 120, headerCellClass: 'uiGridthemeHeaderColor',
 			 		cellTemplate: '<button tooltip-trigger="mouseenter" tooltip-placement="bottom" tooltip-append-to-body="true" tooltip-popup-delay="0" uib-tooltip="{{\'DIAGNOSTIC_CHART_VIEW\'|translate}}" class="mainContent-btn" ng-click="grid.appScope.openDiagnosticChartView(row.entity.deviceId, row.entity.sensorId)"><img src="img/shortCut/diagnosticChartView.png" width="24" height="24"/></button>'},
		 ];		
		$scope.columnDefs = columnDefs;
	};
	
	$scope.setColumnDefs();

	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: true,
		enableVerticalScrollbar: 0,
		rowHeight: $scope.rowHeight,
		
		exporterMenuPdf : false,
		enableGridMenu: true,
	    enableSelectAll: true,
	    exporterCsvFilename: 'diagnosticSensorViewFile.csv',
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	 
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {

		    });
		},
		columnDefs: $scope.columnDefs
	};
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.gridOptions.data = appMainService.getDiagnosticSensorViewData();
	
	$scope.toggleRealTime = function() {

	};
	
	$scope.getTableStyle= function() {
        return {
            height: (($scope.numberOfRows +  2)* $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight ) + "px"
        };
    };
    
 	$http({
   		url: appMainService.getDomainAddress() + '/rest/diagnosticView/webSocketViewDataByDeviceSensor/',
   		method: 'GET',
          params: {
        	  deviceId: '-1',
		      sensorId: '-1', 
		      viewName: 'diagnostic'
	      }
   	}).then(function (response) {

   	},function (response){
    	alert("Failed to search data, status=" + response.status);
   	});
	
	
	$scope.realTime = false;
	$scope.searchSensorData = function() {
		$scope.realTime = false;
		$scope.isLoading = true;
		
		$scope.loadingMessage = $translate.instant('LOADING_PLEASE_WAIT');
		
		ngDialog.open({
			scope: $scope,
			className: "ngdialog-theme-loading", 
			template: "view/loadingDialog.html",
            showClose: false,
            closeByEscape : false
		});
		
		var now = new Date().getTime();
		var fromDateLong =  now - 24*60*60*1000;
		$http({  
			url: appMainService.getDomainAddress() + '/rest/chartView/webSocketViewData/',
			method: 'GET',
	        params: {
	        	deviceId: null,
	        	sensorId: null,
		        dateFrom: fromDateLong.toString(),
		        dateTo: now.toString(),
		        desc: true
	       }
		}).then(function (response) {
			$scope.isLoading = false;
			$scope.realTime = true;
			if (response.data.length > 0) {
				$scope.gridOptions.data = response.data;
				ngDialog.close();
			} else {
				$scope.loadingMessage = $translate.instant('NO_DATA_FOUND');
			}

		},function (response){
			$scope.isLoading = false;
			$scope.loadingMessage = ("Failed to search data, status=" + response.status);	
		});	
	};
	
	$timeout($scope.searchSensorData, 0);
	
	
	$scope.$on('receiveSensorData', function(event, sensorDatas) {
		if ( $scope.realTime == false ) {
			return;
		}
		
		for (var i = 0; i < sensorDatas.length; ++i) {
			var data = sensorDatas[i];
			
			if (!data.isCoverLevel) {
	   			data.coverLevel = "--";
	   		}
	   		if (!data.isWaterLevel) {
	   			data.waterLevelFromZero2 = "--";
	   		}
	   		if (!data.isH2s) {
	   			data.h2s = "--";
	   		}		   		
	   		if (!data.isSo2) {
	   			data.so2 = "--";
	   		}
	   		if (!data.isCo2) {
	   			data.co2 = "--";
	   		}		   	
	   		if (!data.isCh4) {
	   			data.ch4 = "--";
	   		}		   		
	   		if (!data.isNh3) {
	   			data.nh3 = "--";
	   		}
	   		if (!data.isO2) {
	   			data.o2 = "--";
	   		}
	   		
	   		var sensorData = data;
	   		sensorData.isRead = false;
	   		
	   		sensorData.index = $scope.gridOptions.data.length;
	   		$scope.gridOptions.data.unshift(sensorData);
	   		//remove last element if exceeded 5000
	   		if ( $scope.gridOptions.data.length >= 5000) {
	   			$scope.gridOptions.data.splice( $scope.gridOptions.data.length-1,1);
	   		}
 	   		$scope.$apply();
		}
	   			
	});
	$rootScope.connectWebSocketData();
	
	$scope.userCancel = function () {
		window.history.back();
	};
	
	$scope.openDiagnosticChartView = function(deviceId, sensorId) {
		appMainService.setDiagnosticChartViewDeviceId(deviceId);
		appMainService.setDiagnosticChartViewSensorId(sensorId);
		appMainService.setDiagnosticSensorViewData($scope.gridOptions.data);
		
		var path = 'Diagnostic/DiagnosticChartView/';
		$location.path(path);
	};
	
	$scope.createTagConfiguration = function (row) {
		appMainService.setTagConfiguration(row);
		var path = 'SystemConfiguration/TagConfiguration/New/';
		$location.path(path);		
	};
	
	$scope.entityRowsRead = []; 
	
	$scope.updateReadFlag = function () {
		for (var i = 0; i < $scope.entityRowsRead.length; ++i) {
			$scope.entityRowsRead[i].isRead = true;
		}
	};

	$scope.updateEntity = null; 
	$scope.updateFlag = function() {
		$scope.updateEntity.isRead = false;
	};
	$scope.onClickEntity = function(entity) {
		$scope.updateEntity = entity;
		$timeout($scope.updateFlag,0);
	};
	

	$scope.applicationList= {URL: 'view/diagnosticSensorDataDialog.html'};
}]);

	
