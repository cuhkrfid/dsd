angular.module('dsd').controller('diagnosticTableViewCtrl', ['$rootScope','$scope', '$translate','$timeout', '$interval', '$routeParams', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', 'ngDialog','uiGridGroupingConstants','$window',
 function ($rootScope, $scope, $translate, $timeout, $interval, $routeParams,$uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, ngDialog, uiGridGroupingConstants, $window) {
	$scope.rowHeight = 30; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 7;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows) - 1;
	
	$scope.deviceToConfig = null;
	$scope.realTime = true;	
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
 			{field:'dateString',  width: 140, displayName: $translate.instant('DATE'), headerCellClass: 'uiGridthemeHeaderColor',
		           cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.dateString\'><a>{{row.entity.dateString}}</a></span></div>'},
			{field:'readerIds',  width: 100, displayName: $translate.instant('READER_ID'), headerCellClass: 'uiGridthemeHeaderColor',  
				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.readerIds\'><a>{{row.entity.readerIds}}</a></span></div>', 				 	
				 	filter: { condition: uiGridConstants.filter.EXACT } }, 
			{field:'deviceId',  width: 100, displayName: $translate.instant('DEVICE_ID'), headerCellClass: 'uiGridthemeHeaderColor',
			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.deviceId\'>{{row.entity.deviceId}}</span></div>',
				 	filter: { condition: uiGridConstants.filter.EXACT } },
	 		{field:'coverDetectStr',  width: 120, displayName: $translate.instant('COVER_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',
 	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.coverDetectStr\'><a>{{row.entity.coverDetectStr}}</a></span></div>'},
 	 	 	{field:'waterLevel1DetectStr',  width: 85, displayName: $translate.instant('WATER_LEVEL_1_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',
 	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.waterLevel1DetectStr\'><a>{{row.entity.waterLevel1DetectStr}}</a></span></div>'},		
 	 	 	{field:'waterLevel2DetectStr',  width: 85, displayName: $translate.instant('WATER_LEVEL_2_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',
 	  	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.waterLevel2DetectStr\'><a>{{row.entity.waterLevel2DetectStr}}</a></span></div>'},		
 	  	 	{field:'antennaOptimizedStr',  width: 120, displayName: $translate.instant('ANTENNA_OPTIMIZED'), headerCellClass: 'uiGridthemeHeaderColor',
 	   	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.antennaOptimizedStr\'><a>{{row.entity.antennaOptimizedStr}}</a></span></div>'},		
 	    	{field:'maxStep',  width: 100, displayName: $translate.instant('MAX_STEP'), headerCellClass: 'uiGridthemeHeaderColor',
 	 	 			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.maxStep\'><a>{{row.entity.maxStep}}</a></span></div>'},		
 	 	 	{field:'adcValue',  width: 110, displayName: $translate.instant('ADC_VALUE'), headerCellClass: 'uiGridthemeHeaderColor',
 	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.adcValue\'><a>{{row.entity.adcValue}}</a></span></div>'},	
	 		{field:'waterLevelFromZero2',  width: 115, displayName: $translate.instant('WATER_LEVEL'), headerCellClass: 'uiGridthemeHeaderColor',
	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="cm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.waterLevelFromZero2\'><a>{{row.entity.waterLevelFromZero2}}</a></span></div>'},
			{field:'h2s',  width: 70, displayName: $translate.instant('H2S'), headerCellClass: 'uiGridthemeHeaderColor',
			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="ppm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.h2s\'><a>{{row.entity.h2s}}</a></span></div>'},
			{field:'so2',  width: 70, displayName: $translate.instant('SO2'), headerCellClass: 'uiGridthemeHeaderColor',
		 			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="ppm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.so2\'><a>{{row.entity.so2}}</a></span></div>'},
			{field:'ch4',  width: 80, displayName: $translate.instant('CH4'), headerCellClass: 'uiGridthemeHeaderColor',
			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="ppm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.ch4\'><a>{{row.entity.ch4}}</a></span></div>'},
			{field:'readerRssi',  width: 200, displayName: $translate.instant('READER_RSSI'), headerCellClass: 'uiGridthemeHeaderColor',
				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="dBm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.readerRssi\'><a>{{row.entity.readerRssi}}</a></span></div>'},
			{field:'batteryStr',  width: 90, displayName: $translate.instant('BATTERY'), headerCellClass: 'uiGridthemeHeaderColor',
				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.batteryStr\'><a>{{row.entity.batteryStr}}</a></span></div>'},
		    {field:'packetCounter',  width: 90, displayName: $translate.instant('PACKET_COUNTER'), headerCellClass: 'uiGridthemeHeaderColor',
				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.packetCounter\'><a>{{row.entity.packetCounter}}</a></span></div>'},		
//			{field:'measurePeriod',  width: 100, displayName: $translate.instant('MEASURE_PERIOD'), 
//					cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><span animate-on-change=\'row.entity.measurePeriod\'>{{row.entity.measurePeriod}}</span></div>'},
			{field:'timeDiffInMsStr',  width: 130, displayName: $translate.instant('TIME_DIFF'),  enableFiltering : true, headerCellClass: 'uiGridthemeHeaderColor',
					cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.timeDiffInMsStr\'>{{row.entity.timeDiffInMsStr}}</span></div>'},
			{field:'timeDiffMaxInMsStr',  width: 130, displayName: $translate.instant('MAX_TIME_DIFF'), headerCellClass: 'uiGridthemeHeaderColor',
					cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.timeDiffMaxInMsStr\'>{{row.entity.timeDiffMaxInMsStr}}</span></div>'},
			 ];		
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.getTableStyle= function() {
        return {
            height: (($scope.numberOfRows +  2)* $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight ) + "px"
        };
    };
    
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: false,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		treeRowHeaderAlwaysVisible: false,
		rowHeight: $scope.rowHeight,
		enableColumnResizing : true,
		exporterMenuPdf : false,
	    enableGridMenu: true,
	    enableSelectAll: true,
	    exporterCsvFilename: 'diagnosticTableViewFile.csv',
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	  
	    onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {
				
			});
			$scope.tagConfigurationGridApi.grid.registerDataChangeCallback(function() {
		    });
		},
		columnDefs: $scope.columnDefs
	};

	$scope.gridOptions.data = []; //appMainService.getDiagnosticTableViewData();
	
	$scope.toggleRealTime = function() {

	};

	
 	$http({
   		url: appMainService.getDomainAddress() + '/rest/diagnosticView/webSocketViewDataByDeviceSensor/',
   		method: 'GET',
          params: {
        	  deviceId: '-1',
		      sensorId: '-1'
	      }
   	}).then(function (response) {

   	},function (response){
   	});
 	
 	$scope.isInit = true;
	$scope.$on('receiveSensorData', function(event, sensorDatas) {
		if ( $scope.realTime == false || $scope.isInit == true ) {
			return;
		}
		
		for (var i = 0; i < sensorDatas.length; ++i) {
			var data = sensorDatas[i];

	   		if (!data.isCoverLevel) {
	   			data.coverLevel = "--";
	   		}
	   		if (!data.isWaterLevel) {
	   			data.waterLevelFromZero2 = "--";
	   		}
	   		if (!data.isH2s) {
	   			data.h2s = "--";
	   		}		   		
	   		if (!data.isSo2) {
	   			data.so2 = "--";
	   		}
	   		if (!data.isCo2) {
	   			data.co2 = "--";
	   		}		   	
	   		if (!data.isCh4) {
	   			data.ch4 = "--";
	   		}		   		
	   		if (!data.isNh3) {
	   			data.nh3 = "--";
	   		}
	   		if (!data.isO2) {
	   			data.o2 = "--";
	   		}
	   		
	   		var sensorData = data;
	   		var isFound = false;
	   		for (var i = 0; i < $scope.gridOptions.data.length; ++i) {
	   			if ( $scope.gridOptions.data[i].deviceId == sensorData.deviceId ) {
	 				
	   				$scope.gridOptions.data[i] = sensorData;
	   				isFound = true;
	   				break;
	   			}
	   		}
	   		
	   		if (!isFound) {
	   			$scope.gridOptions.data.push(sensorData);
	   		}
 	    }     
	});
	

	$scope.userCancel = function () {
		window.history.back();
	};

	
	$scope.openDiagnosticChartView = function(row) {
		
		appMainService.setDiagnosticChartViewDeviceId(row.deviceId);
		appMainService.setDiagnosticChartViewSensorId(row.sensorId);
		appMainService.setTagConfiguration(row);
		
		var path = 'Diagnostic/DiagnosticChartView/';
		$location.path(path);
	};
	
	
	$scope.resetTimeDiff = function (row) {
//		$scope.deviceToConfig = row.entity;
//		$http({
//	   		url: appMainService.getDomainAddress() + '/rest/diagnosticView/resetMaxTimeDiff/',
//	   		method: 'GET',
//	          params: {
//	        	  deviceAddress: $scope.deviceToConfig.deviceAddress
//		      }
//	   	}).then(function (response) {
//	   		$scope.deviceToConfig.timeDiffMaxInMsStr = "N/A";
//	   	},function (response){
//	    	alert("Failed to search data, status=" + response.status);
//	   	});
	}
	
	
	$scope.openConfigDevice = function (grid, row) {
		
		$scope.deviceToConfig = row.treeNode.children[0].row.entity;
		$scope.formData = new FormData();
		$scope.formData.measurePeriod = $scope.deviceToConfig.measurePeriod;
			
		ngDialog.openConfirm({template: 'view/diagnostic/configTagView.html',
			scope: $scope //Pass the scope object if you need to access in the template
		}).then(
			//Update
			function(value) {
				
				var fd = new FormData();
		 		fd.append('command',4); 
		 		fd.append('length', 11); 
		 		fd.append('deviceAddress', $scope.deviceToConfig.deviceAddress); 
		 	 	fd.append('readerId', $scope.deviceToConfig.readerId); 
		 		fd.append('measurePeriod', $scope.formData.measurePeriod); 
		 	 	
		 		$http.post(appMainService.getDomainAddress() + '/rest/diagnosticView/sendConfigToTag', fd, {
		            transformRequest: angular.identity,
		            headers: {'Content-Type': undefined}
		        }).then(function (response) {
		        	appMainService.displayMessageResult(response.data, true);
		        },function (response){
					alert(response.data);
			    });
		 	},
			//Cancel
			function(value) {
			}		
		);		
	};
	
	$scope.openTagConfiguration = function (row) {
		var deviceConfig = row.entity;
//		var rowConfigs = [];
//		for (var i = 0; i < $scope.gridOptions.data.length; ++i) {
//   			var rowConfig = $scope.gridOptions.data[i];
//   			if ( rowConfig.deviceId == deviceConfig.deviceId) {
//   				rowConfigs.push(rowConfig);
//   			}
//   		}
   		
		deviceConfig.id = 0;
   		deviceConfig.siteConfigurationId = 0;
   		deviceConfig.lat = null;
   		deviceConfig.lon = null;
   		deviceConfig.description = "";
		deviceConfig.groupId = 1;
   		deviceConfig.name = "Tag_" + deviceConfig.deviceId;
   		deviceConfig.measurePeriod = 5;
   		
   		if (typeof deviceConfig.sensorConfigs == "undefined"
   			|| deviceConfig.sensorConfigs == null 
   			|| deviceConfig.sensorConfigs.length <= 0) {
   			$scope.getAllRecentSensorConfig(deviceConfig.deviceId, true, deviceConfig);
   			
   		} else {
   			appMainService.setTagConfiguration(deviceConfig);
   			appMainService.setDiagnosticTableViewData($scope.gridOptions.data);
   			var path = 'SystemConfiguration/TagConfiguration/New/';
   			$location.path(path);	
   		} 
	};
	
	$scope.name = function(grid,row,col) {
		if(row.groupHeader && row.treeNode.children[0].row.treeNode.children[0]) {
			var entity = row.treeNode.children[0].row.treeNode.children[0].row.entity;
			return entity.deviceAddress;
		}
	    else if(row.treeNode.children[0])
	    {
	    	var entity = row.treeNode.children[0].row.entity;
	    	return entity.deviceAddress;
	    }
		return row.entity.deviceAddress;
	};
	 
	$rootScope.connectWebSocketData();
	
	
	$scope.setIsTagConfigurationExist = function(sensorChartDatas) {
		
		$http({
			url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
			method: 'GET',
	        params: {
	        id: null, 
	        name: null, 
	        deviceId : null,
	        deviceAddress : null,
	        groupId : null,
	        description: null,
	        siteName : null,
	        siteId : null,
	       }
		}).then(function (response) {
			var tagConfigurations = response.data; 
			for (var i = 0; i < sensorChartDatas.length; ++i) {
				sensorChartDatas[i].isTagConfig = false; 
				for ( var j = 0; j < tagConfigurations.length; ++j) {
					if ( tagConfigurations[j].deviceId == sensorChartDatas[i].deviceId ) {
						sensorChartDatas[i].isTagConfig = true;
						break;
					}
				}
				$scope.gridOptions.data.push(sensorData);
			}
			$scope.isInit = false;
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});
	};
	
	$scope.getAllRecentSensorConfig = function(deviceId, navigateToTagConfigurationView, tagConfiguration) {
	    var date = new Date();
		var startDate = new Date(date.getTime() - 24*60*60*1000);
		$http({  
			url: appMainService.getDomainAddress() + '/rest/diagnosticView/getRecentSensorChartDataAllDeviceId/',
			params: {
				deviceId: deviceId,
				dateFrom: startDate.getTime().toString(),
				dateTo: date.getTime().toString(), 
				setSensorConfig: true
			}, 
			method: 'GET',
		}).then(function (response) {
			if ( navigateToTagConfigurationView == true && response.data.length > 0) {
				tagConfiguration.sensorConfigs = [];
				tagConfiguration.sensorConfigs.push(response.data[0]);
				appMainService.setTagConfiguration(tagConfiguration);
	   			var path = 'SystemConfiguration/TagConfiguration/New/';
	   			$location.path(path);
	   			return;
	 		}
			
			if( response.data.length > 0 ) {
				for (var i = 0; i < response.data.length; ++i) {
					var data = response.data[i];
			
			   		if (!data.isCoverLevel) {
			   			data.coverLevel = "--";
			   		}
			   		if (!data.isWaterLevel) {
			   			data.waterLevelFromZero2 = "--";
			   		}
			   		if (!data.isH2s) {
			   			data.h2s = "--";
			   		}		   		
			   		if (!data.isSo2) {
			   			data.so2 = "--";
			   		}
			   		if (!data.isCo2) {
			   			data.co2 = "--";
			   		}		   	
			   		if (!data.isCh4) {
			   			data.ch4 = "--";
			   		}		   		
			   		if (!data.isNh3) {
			   			data.nh3 = "--";
			   		}
			   		if (!data.isO2) {
			   			data.o2 = "--";
			   		}
					
					$scope.gridOptions.data.push(data);
				}
			}
			$scope.isInit = false;
		},function (response){
			alert("Failed to search data, status=" + response.status);
		});	
	};
	$scope.getAllRecentSensorConfig(null, false, null);
}]);