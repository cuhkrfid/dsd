angular.module('dsd').controller('groupCtrl', ['$scope', '$translate', '$http', '$location' , 'appMainService',
    function ($scope, $translate, $http, $location, appMainService){
		
	$scope.getTableStyle= function() {
        var marginHeight = 50;
        var length = $scope.groups.length; 
        return {
            height: (30 * $scope.groupGridOption.rowHeight + $scope.groupGridOption.headerRowHeight + marginHeight ) + "px"
        };
    };
		
	$scope.setColumnDefs = function() {
		var columnDefs = [
		    {field:'groupName', width: 180, displayName: $translate.instant('GROUP_NAME') ,  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openGroup(row.entity)"><a>{{row.entity.name}}</a></div>'},
		    {field:'groupDescription', width: 180, displayName: $translate.instant('DESCRIPTION') ,  headerCellClass: 'uiGridthemeHeaderColor', cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openGroup(row.entity)"><a>{{row.entity.description}}</a></div>'},
			    
		];	
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.groups = [];

	
	$scope.groupGridOption={
		data:'groups', 
		minRowsToShow: 30,
		enableFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableHorizontalScrollbar: true,
		enableVerticalScrollbar: 0,
		enableRowHeaderSelection: false,
		rowHeight: 30,

		onRegisterApi: function(gridApi){
	        $scope.userGridApi = gridApi;
	        $scope.userGridApi.core.on.filterChanged( $scope, function() {
	            var grid = this.grid;
	            var groupName = grid.columns.filter(function (item){
	            	return item.field == "groupName";
	            })[0].filters[0].term;
	    
	        });
	    },
	    columnDefs: $scope.columnDefs
	};
	
	$scope.$on('updateLanguageInUiGrid', function() {
		$scope.setColumnDefs();  
		$scope.groupGridOption.columnDefs = $scope.columnDefs;
	});
	
	$scope.userCancel = function () {
		//$location.path('Admin/GroupMaintenance');
		window.history.back();
	};
	
	$scope.searchGroup = function(groupName) {
		$http({
			url: appMainService.getDomainAddress() + '/rest/group/searchData/',
			method: 'GET',
			params: {
				id: null,
				name: groupName,
				active : null
			}
		}).then(function (response) {
			$scope.groups = response.data;
		},function (response){
			alert("Failed to search group status=" + response.status);
		});		
	};
	
	$scope.openGroup = function(group) {
		appMainService.setGroup(group);
		$location.path('Admin/GroupMaintenance/New/');
	};
	
}]);


angular.module('dsd').controller('groupCreateCtrl', ['$scope', '$translate', '$http', '$location' , 'appMainService',
                                                      function ($scope, $translate, $http, $location, appMainService){

	$scope.groupTitle = 'NEW_RECORD';

	var group = appMainService.getGroup()
	if (group == null) {
		$scope.group = new Object();
		$scope.group.id = 0; 
		
	} else {
		$scope.groupTitle = 'EDIT_RECORD';
		$scope.group = group;
	}

	$scope.createGroupOk = function () {

		if ( $scope.group.id > 0) {
			$scope.editGroupOk();
			return;
		}

		var fd = new FormData();
		fd.append('name', $scope.group.name); 
		fd.append('active', $scope.group.active);
		fd.append('description', $scope.group.description);
		fd.append('groupMenuMapping', $scope.generateGroupMenuMappingJson($scope.assignedMenu));
		
		$http.post(appMainService.getDomainAddress() + '/rest/group/addData', fd, {
			transformRequest: angular.identity,
			headers: {'Content-Type': undefined}
		}).then(function (response) {
			appMainService.displayMessageResult(response.data, true);
			$location.path('Admin/GroupMaintenance/');
		},function (response){
			alert("Failed to add group, status=" + response.status);
		});
	};

	$scope.editGroupOk = function () {

		var fd = new FormData();
		
		fd.append('id', $scope.group.id); 
		fd.append('name', $scope.group.name); 
		fd.append('active', $scope.group.active);
		fd.append('description', $scope.group.description);
		fd.append('groupMenuMapping', $scope.generateGroupMenuMappingJson($scope.assignedMenu));
		
		$http.post(appMainService.getDomainAddress() + '/rest/group/updateData', fd, {
			transformRequest: angular.identity,
			headers: {'Content-Type': undefined}
		}).then(function (response) {
			appMainService.displayMessageResult(response.data, true);
			$location.path('Admin/GroupMaintenance');
		},function (response){
			alert("Failed to edit group, status=" + response.status);
		});
	};

	$scope.userCancel = function () {
		window.history.back();
	};

	$scope.removeGroup = function (userId){
		$http({
			url: appMainService.getDomainAddress() + '/rest/group/deleteData/',
			method: 'DELETE',
			params: {
				id: userId
			}
		}).then(function (response) {
			appMainService.displayMessageResult(response.data, true);
			$location.path('Admin/GroupMaintenance/');
		},function (response){
			alert("Failed to delete group (ID = "+userId+"), status=" + response.status);
		});
	};

	$scope.setColumnDefsUnassign = function() {
		var columnDefs = [
		    {field:'menuFunction',  enableFiltering : false, width: 180, displayName: $translate.instant('FUNCTION') , cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openGroup(row.entity)"><a>{{row.entity.moduleName|translate}} - {{row.entity.menuName|translate}}</a></div>'},
		    
		];	
		$scope.columnDefsUnassign = columnDefs;
	};
	$scope.setColumnDefsUnassign();

	$scope.setColumnDefsAssign = function() {
		var columnDefs = [
		    {field:'menuFunction',  enableFiltering : false, width: 180, displayName: $translate.instant('ASSIGNED_FUNCTION') , cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openGroup(row.entity)"><a>{{row.entity.moduleName|translate}} - {{row.entity.menuName|translate}}</a></div>'},
		    {
		    	field:'permission', width: 160,
		    	name: $translate.instant('PERMISSION'),
		    	enableFiltering : false, 
		    	editableCellTemplate: 'uiSelect',
		    	editDropdownOptionsArray: [
                   'All' ,
                   'Modify' ,
                   'Read only' 
                ]
		    }		    
		    
		];	
		$scope.columnDefsAssign = columnDefs;
	};
	$scope.setColumnDefsAssign();
	
	$scope.unassignedMenu = [];
	$scope.assignedMenu = [];
	
	$scope.menuGridUnassignOption={
		data:'unassignedMenu', 
		minRowsToShow: 90,
		enableFiltering: true,
	    useExternalFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableHorizontalScrollbar: false,
		enableVerticalScrollbar: true,
		enableRowHeaderSelection: true,
		onRegisterApi: function(gridApi){
	        $scope.menuGridUnassignApi = gridApi;
	        $scope.menuGridUnassignApi.core.on.filterChanged( $scope, function() {
	            var grid = this.grid;
	            var groupName = grid.columns.filter(function (item){
	            	return item.field == "groupName";
	            })[0].filters[0].term;
	    
	        });
	    },
	    columnDefs: $scope.columnDefsUnassign
	};	
			
	$scope.menuGridAssignOption={
		data:'assignedMenu', 
		minRowsToShow: 90,
		enableFiltering: true,
	    useExternalFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableHorizontalScrollbar: false,
		enableVerticalScrollbar: true,
		enableRowHeaderSelection: true,
		onRegisterApi: function(gridApi){
	        $scope.menuGridAssignApi = gridApi;
	        $scope.menuGridAssignApi.core.on.filterChanged( $scope, function() {
	            var grid = this.grid;
	            var groupName = grid.columns.filter(function (item){
	            	return item.field == "groupName";
	            })[0].filters[0].term;
	    
	        });
	    },
	    columnDefs: $scope.columnDefsAssign
	};
	
	$scope.$on('updateLanguageInUiGrid', function() {
		$scope.setColumnDefsUnassign();  
		$scope.menuGridUnassignOption.columnDefs = $scope.columnDefsUnassign;
		
		$scope.setColumnDefsAssign();  
		$scope.menuGridAssignOption.columnDefs = $scope.columnDefsAssign;
	});

	
	$http({
		url: appMainService.getDomainAddress() + '/rest/group/getUnAssignFunctionByGroupId/',
		method: 'GET',
		params: {
			groupId: $scope.group.id,
		}
	}).then(function (response) {
		$scope.unassignedMenu = response.data;
	},function (response){
		alert("Failed to find group list, status=" + response.status);
	});

	$http({
		url: appMainService.getDomainAddress() + '/rest/group/getAssignFunctionByGroupId/',
		method: 'GET',
		params: {
			groupId: $scope.group.id,
		}
	}).then(function (response) {
		$scope.assignedMenu = response.data;
	},function (response){
		alert("Failed to find group list, status=" + response.status);
	});
	
	$scope.getTableStyle= function() {
        var marginHeight = 50;
        return {
            height: (20 * $scope.menuGridUnassignOption.rowHeight + $scope.menuGridUnassignOption.headerRowHeight + marginHeight ) + "px"
        };
    };
    
    $scope.assignFunction = function() {
    	var selectedRows = $scope.menuGridUnassignApi.selection.getSelectedRows();
    	for (var i = 0; i < selectedRows.length; ++i ) {
    		for (var j = 0; j < $scope.unassignedMenu.length; ++j ) {
    			if ( $scope.unassignedMenu[j].menuId == selectedRows[i].menuId) {
    				$scope.unassignedMenu.splice(j,1);
    			}
        	}
    		selectedRows[i].permission = "Read only";
        	$scope.assignedMenu.push(selectedRows[i]);
    	}
    	
    	$scope.assignedMenu.sort(function(a, b) {
    		if ( a.menuId > b.menuId)
    			return 1;
    		if ( a.menuId < b.menuId)
    			return -1;
    		return 0;
    	});
    	
    };
    
    $scope.unassignFunction = function() {
    	var selectedRows = $scope.menuGridAssignApi.selection.getSelectedRows();
    	for (var i = 0; i < selectedRows.length; ++i ) {
    		for (var j = 0; j < $scope.assignedMenu.length; ++j ) {
    			if ( $scope.assignedMenu[j].menuId == selectedRows[i].menuId) {
    				$scope.assignedMenu.splice(j,1);
    			}
        	}
        	$scope.unassignedMenu.push(selectedRows[i]);
    	}
    	
    	$scope.unassignedMenu.sort(function(a, b) {
    		if ( a.menuId > b.menuId)
    			return 1;
    		if ( a.menuId < b.menuId)
    			return -1;
    		return 0;
    	});    	
    };
    
    
	$scope.generateGroupMenuMappingJson = function (groupMenuMappings) {
		var groupMenuMappingsJson = '{"groupMenuMappings":[';
		for (var i = 0; i < groupMenuMappings.length; ++i) {
 			var id = '"id":"' + groupMenuMappings[i].id + '"'; 
 			var groupId = '"groupId":"' + groupMenuMappings[i].groupId + '"'; 
			var moduleId = '"moduleId":"' + groupMenuMappings[i].moduleId + '"'; 
			var menuId = '"menuId":"' + groupMenuMappings[i].menuId + '"'; 
			var permissionId = '"permissionId":"' + groupMenuMappings[i].permissionId + '"'; 
			var permission = '"permission":"' + groupMenuMappings[i].permission + '"'; 
			var moduleName = '"moduleName":"' + groupMenuMappings[i].moduleName + '"'; 
			var menuName = '"menuName":"' + groupMenuMappings[i].menuName + '"'; 
			
			var groupMenuMapping = '{' + id + ',' + groupId + ',' + moduleId + ',' + menuId + ',' + permissionId + ',' + permission + ',' + moduleName + ',' + menuName + '}';

			if ( i != groupMenuMappings.length - 1 ) {
				groupMenuMapping += ','
			}
			groupMenuMappingsJson += groupMenuMapping;
		}
		groupMenuMappingsJson += ']}';
		return groupMenuMappingsJson;
	};
}]);
