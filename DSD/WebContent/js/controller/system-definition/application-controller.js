angular.module('dsd').controller('appCtrl', ['$scope', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService',
 function ($scope, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService) {
    $scope.section="";
	$scope.users=[
	];
	$scope.roles=[
	];
	$scope.modules = [];
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.userTotalItems = 100;
	$scope.userCurrentPage = 1;
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
		  
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	$scope.getTableStyle= function() {
        var marginHeight = 50;
        var length = $scope.applications.length; 
        return {
            height: (length * $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight + marginHeight ) + "px"
        };
    };
	
	
	$scope.changeLanguage=function(locale) {
		alert(locale);
	};
	
	$scope.toggleFiltering = function(){
		$scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
	    $scope.applicationGridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	 
	};
	
    $scope.refresh = function () {
    	$scope.searchMenu(null);
        $http({
            url: appMainService.getDomainAddress() + '/rest/user/getLoginUser/',
            method: 'GET',
        }).then(function (response) {
            $scope.currentUser = response.data;
        },function (response){
  		  	alert("Failed to get login user, status=" + response.status);
        });
    };
    $scope.refresh();
    
    

	

	   
	$scope.disconnect = function () {
	    chatClient.close();
	}
	
	$scope.$on('$destroy', function () {
		$scope.disconnect();
	});
	
	$scope.initView = function () {
		$scope.searchApplication(null, null, null, null, null ,null, null, null, false);
		$scope.connect();
	}
    
    //System Settings
    $scope.showSystemSetting = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'view/systemSettingView.html',
			controller: 'SystemSettingCtrl',
			size: 'lg',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
    
    
    
    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });
 
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.persons = {currentPage : 1};
	
	//User Maintenance
	$scope.openApplication = function (applicationId) {
		var path = 'SystemDefinition/ApplicationMaintenance/Edit/';
		path += (applicationId+"");
		$location.path(path);
	};

	$scope.removeApplication = function (row){
		var path = 'SystemDefinition/ApplicationMaintenance/Delete/';
		path += (row.entity.id+"");
		$location.path(path);
	};
	
	$scope.applicationTotalItems = 100;
	$scope.applicationCurrentPage = 1;
	$scope.applications=[];
	$scope.gridOptions={
		data:'applications', 
		minRowsToShow: 30,
		enableFiltering: false,
		useExternalFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableHorizontalScrollbar: 0,
		enableVerticalScrollbar: 0,
		onRegisterApi: function(gridApi){
			$scope.applicationGridApi = gridApi;
			$scope.applicationGridApi.core.on.filterChanged( $scope, function() {
				var grid = this.grid;
				var id = grid.columns.filter(function (item){
					return item.field == "id";
				})[0].filters[0].term;
				var temperature = grid.columns.filter(function (item){
					return item.field == "temperature";
				})[0].filters[0].term;
							
//				$scope.searchApplication(id, name, description, displayOrder, active, path, false);
		    });
		},
		columnDefs: [
//		    {field:'info', width: 30,
//			 		headerCellTemplate: '<a href="javascript:void(0);" ng-click="grid.appScope.toggleFiltering()"><img src="img/normal/filter.png" width="24" height="24" ></img></a>', 
//			 		cellTemplate: '<img src="img/normal/info.png" width="24" height="24"></img>'},
			{field:'id', displayName: 'ID', 
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openApplication(row.entity.id)"><a>{{row.entity.id}}</a></div>'},
			{field:'temperature', displayName: 'Temperature', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openApplication(row.entity.id)"><a>{{row.entity.temperature}}</a></div>'}	    

	]};
	
	$scope.host = window.location.host;
	$scope.path = window.location.pathname;
	
	$scope.webCtx = $scope.path.substring(0, $scope.path.indexOf('/', 1));
	$scope.endPointURL = "ws://" + window.location.host + $scope.webCtx + "/chat";
	
	$scope.chatClient = null;
	$scope.connect = function () {
		chatClient = new WebSocket($scope.endPointURL);
	    chatClient.onmessage = function (event) {
	    	var jsonObj = JSON.parse(event.data);
	 	   	angular.forEach(jsonObj, function(data, index){
	 	   		console.log("id=" + data.id);
	 	   		angular.forEach($scope.applications, function(application, index){
	 	   			if(application.id == data.id){
	 	   				application.temperature = data.temperature;
	 	   				$scope.applicationGridApi.grid.modifyRows($scope.gridOptions.data);
	 	   				$scope.applicationGridApi.selection.selectRow(index);
	 	   			}
	 	        });
	 	   	});
	 	   	
	        $scope.$apply();
	    };
	}
	
	$scope.openApplication = function (applicationId) {
		var path = 'SystemDefinition/ApplicationMaintenance/Edit/';
		path += (applicationId+"");
		$location.path(path);    		
	};

	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
}]);


angular.module('dsd').controller('applicationEditCtrl', ['$scope', '$routeParams', '$http', '$location' , 'appMainService',
                                                    function ($scope, $routeParams, $http, $location, appMainService){
	
	$scope.editApplicationId = $routeParams.applicationId;
	$http({
        url: appMainService.getDomainAddress() + '/rest/application/findApplicationById/',
        method: 'GET',
        params: {
        	id: $routeParams.applicationId
        }
    }).then(function (response) {
    	$scope.editName = response.data.name;
		$scope.editDescription = response.data.description;
		$scope.editDisplayOrder = response.data.displayOrder;
		$scope.editActive = response.data.active;
		$scope.editPath = response.data.path;
		$scope.editApplicationId = response.data.id;
		$scope.editImage = null;
		if ( response.data.image != null)
			$scope.editImage = response.data.image.base64Content;
    },function (response){
		  alert("Failed to find application (ID = "+$scope.editApplicationId+"), status=" + response.status);
    });
	
	$scope.editFileName = '';
	$scope.editImage = null; 
	
	document.getElementById('file').onchange = function(e) {
		$scope.file = e.srcElement.files[0];
			
		var fullPath = document.getElementById('file').value;
		if (fullPath) {
		    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
		    var filename = fullPath.substring(startIndex);
		    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
		    	$scope.editFileName = filename.substring(1);
				//alert($scope.fileName);
		    }
		}
	};
	
	$scope.deleteImage = function() {
		$scope.editFileName = '<deleted_@>';
		$scope.editImage = null;
	}
	
    $scope.editApplicationOk = function () {
		
    	var fd = new FormData();
 		fd.append('id', $routeParams.applicationId);
    	fd.append('name', $scope.editName); 
 		fd.append('description', $scope.editDescription); 
 		fd.append('displayOrder', $scope.editDisplayOrder); 
 		fd.append('active', $scope.editActive); 
 		fd.append('path', $scope.editPath);
		
 		if ( $scope.editFileName == null) {
 			fd.append('fileName', null);
 			fd.append('file', null);
 		} else {
 			fd.append('fileName', $scope.editFileName);
 		 	fd.append('file', $scope.editImage);
 		} 
 		
 		$http.post(appMainService.getDomainAddress() + '/rest/application/updateData', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
		  alert("Edit application success");
		  appMainService.prepForBroadcast('');
		  $location.path('SystemDefinition/ApplicationMaintenance');
	    },function (response){
			  alert("Failed to edit application, status=" + response.status);
	    });
	};
	
	$scope.removeApplication = function (){
		$http({
            url: appMainService.getDomainAddress() + '/rest/application/deleteData/',
            method: 'DELETE',
            params: {
            	id: $routeParams.applicationId
            }
        }).then(function (response) {
      	    alert("Delete application success");
      	    appMainService.prepForBroadcast('');
      	    $location.path('SystemDefinition/ApplicationMaintenance');
        },function (response){
  		  alert("Failed to delete application, status=" + response.status);
        });
	};
	
	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
}]);

angular.module('dsd').controller('applicationCreateCtrl', ['$scope', '$routeParams', '$http', '$location' , 'appMainService',
                                                           function ($scope, $routeParams, $http, $location, appMainService){
     
	$scope.fileName = '';
	$scope.newImage = null; 
	
	document.getElementById('file').onchange = function(e) {
		$scope.file = e.srcElement.files[0];
			
		var fullPath = document.getElementById('file').value;
		if (fullPath) {
		    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
		    var filename = fullPath.substring(startIndex);
		    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
		    	$scope.fileName = filename.substring(1);
				//alert($scope.fileName);
		    }
		}
	};

	$scope.deleteImage = function() {
		$scope.fileName = '';
		$scope.newImage = null;
	}
 	$scope.createApplicationOk = function () {
 		var fd = new FormData();
 		fd.append('name', $scope.newName); 
 		fd.append('description', $scope.newDescription); 
 		fd.append('displayOrder', $scope.newDisplayOrder); 
 		fd.append('active', $scope.newActive); 
 		fd.append('path', $scope.newPath);
		
 		if ( $scope.fileName == null) {
 			fd.append('fileName', null);
 			fd.append('file', null);
 		} else {
 			fd.append('fileName', $scope.fileName);
 		 	fd.append('file', $scope.newImage);
 		} 
 		
 		$http.post(appMainService.getDomainAddress() + '/rest/application/addData', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
        	appMainService.prepForBroadcast('');
		  alert("Add application success");
		  $location.path('SystemDefinition/ApplicationMaintenance');
	    },function (response){
			  alert("Failed to add application, status=" + response.status);
	    });
	};

	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
 }]);


