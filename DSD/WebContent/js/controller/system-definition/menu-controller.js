angular.module('dsd').controller('menuCtrl', ['$scope', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService',
 function ($scope, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService ) {
    $scope.section="";
	
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="#">Impersonate </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
		  	
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	$scope.getTableStyle= function() {
        var marginHeight = 50;
        var length = $scope.menuses.length; 
        return {
            height: (length * $scope.menuGridOption.rowHeight + $scope.menuGridOption.headerRowHeight + marginHeight ) + "px"
        };
    };
	$scope.toggleFiltering = function(){
		$scope.menuGridOption.enableFiltering = !$scope.menuGridOption.enableFiltering;
	    $scope.menuGridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	 
	};

    //System Settings
    $scope.showSystemSetting = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'view/systemSettingView.html',
			controller: 'SystemSettingCtrl',
			size: 'lg',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
    
    
    
    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });
 
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.persons = {currentPage : 1};

	
	$scope.totalItems = 100;
	$scope.currentPage = 1;
	$scope.menuses = [];
	$scope.menuGridOption={
		data:'menuses', 
		minRowsToShow: 30,
		enableFiltering: true,
		useExternalFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableHorizontalScrollbar: 0,
		enableVerticalScrollbar: 0,
		onRegisterApi: function(gridApi){
			$scope.menuGridApi = gridApi;
			$scope.menuGridApi.core.on.filterChanged( $scope, function() {
				var grid = this.grid;
				var id = grid.columns.filter(function (item){
					return item.field == "id";
				})[0].filters[0].term;
				
				var name = grid.columns.filter(function (item){
					return item.field == "name";
				})[0].filters[0].term;
				
				var module = grid.columns.filter(function (item){
		            return item.field == "module";
		        })[0].filters[0].term;
				
				var description = grid.columns.filter(function (item){
					return item.field == "description";
				})[0].filters[0].term;
				
				var displayOrder = grid.columns.filter(function (item){
					return item.field == "displayOrder";
				})[0].filters[0].term;
				
				var active = grid.columns.filter(function (item){
					return item.field == "active";
				})[0].filters[0].term;
				
				var path = grid.columns.filter(function (item){
					return item.field == "path";
				})[0].filters[0].term;

				$scope.searchMenu(id, name, module, description, displayOrder, active, path, false);
		    });
		},
		columnDefs: [
		    {field:'info', width: 30,
			 		headerCellTemplate: '<a href="javascript:void(0);" ng-click="grid.appScope.toggleFiltering()"><img src="img/normal/filter.png" width="24" height="24" ></img></a>', 
			 		cellTemplate: '<img src="img/normal/info.png" width="24" height="24"></img>'},
			{field:'id', displayName: 'ID', 
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openMenu(row.entity.id)"><a>{{row.entity.id}}</a></div>'},
			{field:'name', displayName: 'Name', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openMenu(row.entity.id)"><a>{{row.entity.name}}</a></div>'},
			{field:'module', displayName: 'Module', 
					cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openMenu(row.entity.id)"><a>{{row.entity.module.name}}</a></div>'},
			{field:'description', displayName: 'Description', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openMenu(row.entity.id)"><a>{{row.entity.description}}</a></div>'},
			{field:'displayOrder', displayName:'Display Order', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openMenu(row.entity.id)"><a>{{row.entity.displayOrder}}</a></div>'},
			{field:'active', displayName:'Active', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openMenu(row.entity.id)"><a>{{row.entity.active}}</a></div>'},
			{field:'path', displayName:'Path', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openMenu(row.entity.id)"><a>{{row.entity.path}}</a></div>'}
	    ]
	};
	
	$scope.openMenu = function (menuId) {
		var path = 'SystemDefinition/MenuMaintenance/Edit/';
		path += (menuId+"");
		$location.path(path);    		
	};
	
    $scope.searchMenu = function (searchId, name, module, description, displayOrder, active, path, useOrder) {
    	$http({
    		url: appMainService.getDomainAddress() + '/rest/menu/searchData/',
    		method: 'GET',
            params: {
            id: searchId, 
            name: name, 
            module: module,
            description: description, 
            displayOrder: displayOrder, 
            active: active, 
            path: path,
            useOrder : useOrder
           }
    	}).then(function (response) {
    		$scope.menuses = response.data;
    	},function (response){
     		  alert("Failed to search data, status=" + response.status);
    	});
    };
    
	$scope.userCancel = function () {
		window.history.back();
	};
	
	
}]);


angular.module('dsd').controller('menuEditCtrl', ['$scope', '$routeParams', '$http', '$location' , 'appMainService',
                                                    function ($scope, $routeParams, $http, $location, appMainService){
	
	$scope.menuEditId = $routeParams.menuId;
	$http({
		url: appMainService.getDomainAddress() + '/rest/module/searchData/',
		method: 'GET',
        params: {
        id: null, 
        name: null, 
        description: null, 
        displayOrder: null, 
        homepage : null,
        active: true, 
        path: null,
        useOrder: false
       }
	}).then(function (response) {
		$scope.modules = response.data;
	},function (response){
 		  alert("Failed to search data, status=" + response.status);
	});
	
	$http({
        url: appMainService.getDomainAddress() + '/rest/menu/findMenuById/',
        method: 'GET',
        params: {
        	id: $routeParams.menuId
        }
    }).then(function (response) {
    	$scope.editName = response.data.name;
    	$scope.editModule = response.data.moduleId;
		$scope.editDescription = response.data.description;
		$scope.editDisplayOrder = response.data.displayOrder;
		$scope.editActive = response.data.active;
		$scope.editPath = response.data.path;
		$scope.menuEditId = response.data.id;
		$scope.editImage = null;
		if ( response.data.image != null)
			$scope.editImage = response.data.image.base64Content;
    },function (response){
		  alert("Failed to find menu (ID = "+$scope.menuEditId+"), status=" + response.status);
    });
	
	$scope.editFileName = '';
	$scope.editImage = null; 
	
	document.getElementById('file').onchange = function(e) {
		$scope.file = e.srcElement.files[0];
			
		var fullPath = document.getElementById('file').value;
		if (fullPath) {
		    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
		    var filename = fullPath.substring(startIndex);
		    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
		    	$scope.editFileName = filename.substring(1);
		    }
		}
	};
	
	$scope.deleteImage = function() {
		$scope.editFileName = '<deleted_@>';
		$scope.editImage = null;
	}
	
    $scope.editMenuOk = function () {
		
    	var fd = new FormData();
 		fd.append('id', $routeParams.menuId);
    	fd.append('name', $scope.editName); 
    	fd.append('moduleId', $scope.editModule); 
 		fd.append('description', $scope.editDescription); 
 		fd.append('displayOrder', $scope.editDisplayOrder); 
 		fd.append('active', $scope.editActive); 
 		fd.append('path', $scope.editPath);
		
 		if ( $scope.editFileName == null) {
 			fd.append('fileName', null);
 			fd.append('file', null);
 		} else {
 			fd.append('fileName', $scope.editFileName);
 		 	fd.append('file', $scope.editImage);
 		} 
 		
 		$http.post(appMainService.getDomainAddress() + '/rest/menu/updateData', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
		  alert("Edit menu success");
		  appMainService.prepForBroadcast('menuUpdatedInModuleEvent');	
		  $location.path('SystemDefinition/MenuMaintenance');
	    },function (response){
			  alert("Failed to edit menu, status=" + response.status);
	    });
	};
	
	$scope.removeMenu = function (){
		$http({
            url: appMainService.getDomainAddress() + '/rest/menu/deleteData/',
            method: 'DELETE',
            params: {
            	id: $routeParams.menuId
            }
        }).then(function (response) {
      	    alert("Delete menu success");
      	    appMainService.prepForBroadcast('menuUpdatedInModuleEvent');	
      	    $location.path('SystemDefinition/MenuMaintenance');
        },function (response){
  		  alert("Failed to delete menu, status=" + response.status);
        });
	};
	
	$scope.userCancel = function () {
		window.history.back();
	};
}]);

angular.module('dsd').controller('menuCreateCtrl', ['$scope', '$routeParams', '$http', '$location' , 'appMainService',
                                                           function ($scope, $routeParams, $http, $location, appMainService){
     
	$scope.fileName = '';
	$scope.newImage = null; 
	
	document.getElementById('file').onchange = function(e) {
		$scope.file = e.srcElement.files[0];
			
		var fullPath = document.getElementById('file').value;
		if (fullPath) {
		    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
		    var filename = fullPath.substring(startIndex);
		    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
		    	$scope.fileName = filename.substring(1);
				//alert($scope.fileName);
		    }
		}
	};

	$http({
		url: appMainService.getDomainAddress() + '/rest/module/searchData/',
		method: 'GET',
        params: {
        id: null, 
        name: null, 
        description: null, 
        displayOrder: null, 
        homepage : null,
        active: true, 
        path: null,
        useOrder: false
       }
	}).then(function (response) {
		$scope.modules = response.data;
	},function (response){
 		  alert("Failed to search data, status=" + response.status);
	});
	
	$scope.deleteImage = function() {
		$scope.fileName = '';
		$scope.newImage = null;
	}
 	$scope.createMenuOk = function () {
 		var fd = new FormData();
 		fd.append('name', $scope.newName); 
 		fd.append('moduleId', $scope.newModule); 
 		fd.append('description', $scope.newDescription); 
 		fd.append('displayOrder', $scope.newDisplayOrder); 
 		fd.append('active', $scope.newActive); 
 		fd.append('path', $scope.newPath);
		
 		if ( $scope.fileName == null) {
 			fd.append('fileName', null);
 			fd.append('file', null);
 		} else {
 			fd.append('fileName', $scope.fileName);
 		 	fd.append('file', $scope.newImage);
 		} 
 		
 		$http.post(appMainService.getDomainAddress() + '/rest/menu/addData', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
        	appMainService.prepForBroadcast('');
		  alert("Add menu success");
		  appMainService.prepForBroadcast('menuUpdatedInModuleEvent');	
		  $location.path('SystemDefinition/MenuMaintenance');
	    },function (response){
			  alert("Failed to add menu, status=" + response.status);
	    });
	};

	$scope.userCancel = function () {
		window.history.back();
	};
 }]);


