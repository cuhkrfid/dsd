angular.module('dsd').controller('dashBoardViewCtrl', ['uiGridConstants','$translate','$window','$rootScope','$scope', '$interval', '$timeout','$routeParams', '$http', '$location' , 'appMainService','ngDialog','$route',
                                                         function (uiGridConstants, $translate, $window, $rootScope, $scope, $interval, $timeout, $routeParams, $http, $location, appMainService, ngDialog, $route){
	
	$scope.panelHeight = $window.innerHeight / 2 - 40  + "px";
	$scope.panelHeightNum = $window.innerHeight / 2 - 40;

	$scope.panelHeightStyle = { "height" : $scope.panelHeight };
	
	$scope.tagConfigurations = [];
	$scope.markers = [];
	$scope.tagConfigurationsMap = []; 
	
	$scope.sensorDataQueue = [];
	$scope.sensorDataQueue2 = [];
	$scope.alertsQueue = [];
	$scope.alerts = [];
	
	$scope.getAllRecentSensorConfig = function(deviceId) {
	    var date = new Date();
		var startDate = new Date(date.getTime() - 24*60*60*1000);
		$http({  
			url: appMainService.getDomainAddress() + '/rest/diagnosticView/getRecentSensorChartDataAllDeviceId/',
			params: {
				deviceId: null,
				dateFrom: startDate.getTime().toString(),
				dateTo: date.getTime().toString() 
			}, 
			method: 'GET',
		}).then(function (response) {
			if( response.data.length > 0 ) {
				for (var i = 0; i < response.data.length; ++i) {
					var sensorData = response.data[i];
					if ( sensorData.deviceId == deviceId) {
						$scope.sensorDataQueue.push(sensorData);
						$scope.updateTagConfigurationValues(true);
						break;		
					}
				}
			}
		},function (response){
			alert("Failed to search data, status=" + response.status);
		});	
	};
	
	$scope.searchTagConfigurations = function () {
	   	$http({
	   		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/webSocketViewData/',
	   		method: 'GET',
	           params: {
	           id: null, 
	           name: null, 
	           deviceId : null,
	           sensorId : null,
	           groupId : null,
	           description: null,
	           siteName : null,
	           siteId : null
	          }
	   	}).then(function (response) {
	   		$scope.tagConfigurations = response.data;
	   		$scope.markers = [];
	   		$scope.tagConfigurationsMap = [];
	   		
   			for (var i = 0; i < $scope.tagConfigurations.length; ++i) {
   				var tagConfiguration = $scope.tagConfigurations[i];
   				
   				tagConfiguration.dateString2 = "--";
				tagConfiguration.dateString = "--";
	   			tagConfiguration.rssi = "--";
	   		
	   			tagConfiguration.waterLevel = "--";
	   			tagConfiguration.coverLevel = "--";
	   			tagConfiguration.h2s = "--";
	   			tagConfiguration.so2 = "--";
	   			
	   			tagConfiguration.co2 = "--";
	   			tagConfiguration.ch4 = "--";
	   			tagConfiguration.nh3 = "--";
	   			tagConfiguration.o2 = "--";
	   			
	   			tagConfiguration.coverDetectStr = "--";
	   			tagConfiguration.waterLevel1DetectStr = "--";
	   	   		
	   			tagConfiguration.antennaOptimizedStr = "--";
	   	   		tagConfiguration.waterLevel2DetectStr = "--";
	   	   	
	   	   		tagConfiguration.maxStep = "--";
	   	   		tagConfiguration.adcValue = "--";
	   	   		
	   	   		tagConfiguration.batteryStr = "--";
	   	   		tagConfiguration.packetCounter = "--";
	   	   		
	   	   		tagConfiguration.readerIds = "--"; 
	   	   		
	   			tagConfiguration.isShow = false;
	   			tagConfiguration.numOfSensorDatas = 0;
	   			tagConfiguration.numOfAlerts = 0;
	   			tagConfiguration.alertDatas = [];
   				tagConfiguration.sensorDatas = [];

	   			tagConfiguration.markerClassName = "fa fa-map-marker fa-stack-1x yellowColor";
	            if ( tagConfiguration.validationResult == true) {
		   			tagConfiguration.markerClassName = "fa fa-map-marker fa-stack-1x greenColor";
	            } else if ( tagConfiguration.validationResult == false ) {
		   			tagConfiguration.markerClassName = "fa fa-map-marker fa-stack-1x redColor";
	            }
	            
   				$scope.tagConfigurationsMap[tagConfiguration.deviceId] = tagConfiguration;
	   			$scope.getAllRecentSensorConfig(tagConfiguration.deviceId);
	   			
   			}
   			
   			scrollToMarkers( $scope.tagConfigurations );
   			
   			var selectedTagConfigurationId = appMainService.getDashBoardSelectedTagConfigurationId();
   			if ( selectedTagConfigurationId  > 0) {
   				for (var k=0; k < $scope.tagConfigurations.length; ++k ) {
   					var tagConfiguration = $scope.tagConfigurations[k];
   					if (tagConfiguration.id == selectedTagConfigurationId ) {
   			   			$scope.toggleTagConfiguration(tagConfiguration);
   			   			$scope.onTagLocation(tagConfiguration);
   			   			$scope.onAlertData(tagConfiguration);
   			   			$scope.onChartView(tagConfiguration);
   			   			break;
   					}
   				}
   			}

     	},function (response){
	   		alert("Failed to search data, status=" + response.status);
	   	});
	};
   

    $scope.updateTagConfigurationValues = function(loadFromDb) {
    	var sensorData = $scope.sensorDataQueue.shift();
		var tagConfiguration = $scope.tagConfigurationsMap[sensorData.deviceId];
		
    	tagConfiguration.dateString2 = sensorData.dateString2;
		tagConfiguration.dateString = sensorData.dateString;
		tagConfiguration.rssi = sensorData.rssi;
		
		if ( tagConfiguration.isWaterLevel)
			tagConfiguration.waterLevel =  sensorData.waterLevel;
		
		if ( tagConfiguration.isCoverLevel)
			tagConfiguration.coverLevel =  sensorData.coverLevel;
		
		if ( tagConfiguration.isH2s)
			tagConfiguration.h2s = sensorData.h2s;
		
		if ( tagConfiguration.isSo2)
			tagConfiguration.so2 = sensorData.so2;
		
		if ( tagConfiguration.isCo2)
			tagConfiguration.co2 = sensorData.co2;
		
		if ( tagConfiguration.isCh4)
			tagConfiguration.ch4 = sensorData.ch4;
		
		if ( tagConfiguration.isNh3)	
			tagConfiguration.nh3 = sensorData.nh3;
		
		if ( tagConfiguration.isO2)
			tagConfiguration.o2 = sensorData.o2;
		
		tagConfiguration.coverDetectStr = sensorData.coverDetectStr;
		tagConfiguration.waterLevel1DetectStr = sensorData.waterLevel1DetectStr;
   		
		tagConfiguration.antennaOptimizedStr = sensorData.antennaOptimizedStr;
   		tagConfiguration.waterLevel2DetectStr = sensorData.waterLevel2DetectStr;
   	
   		tagConfiguration.maxStep = sensorData.maxStep;
   		tagConfiguration.adcValue = sensorData.adcValue;
   		
   		tagConfiguration.battery = sensorData.battery;
   		tagConfiguration.batteryStr = sensorData.batteryStr;
   		tagConfiguration.packetCounter = sensorData.packetCounter;
   		
   		tagConfiguration.readerIds = sensorData.readerIds;
   		
   		if ( loadFromDb != true) {
   			tagConfiguration.numOfSensorDatas++;
   			sensorData.isReadSensorData = true;
   		}
   		
   		if ( tagConfiguration.sensorDatas.length == 0) {
   			tagConfiguration.firstSensorDataDate = sensorData.date;
   		}
   		
   		tagConfiguration.sensorDatas.unshift(sensorData);
   		
   		if ( $scope.selectedTagConfigurationForSensorView != null  && $scope.selectedTagConfigurationForSensorView.id == tagConfiguration.id ) {
			var data = sensorData;
			
			if (!data.isCoverLevel) {
	   			data.coverLevel = "--";
	   		}
	   		if (!data.isWaterLevel) {
	   			data.waterLevel = "--";
	   		}
	   		if (!data.isH2s) {
	   			data.h2s = "--";
	   		}		   		
	   		if (!data.isSo2) {
	   			data.so2 = "--";
	   		}
	   		if (!data.isCo2) {
	   			data.co2 = "--";
	   		}		   	
	   		if (!data.isCh4) {
	   			data.ch4 = "--";
	   		}		   		
	   		if (!data.isNh3) {
	   			data.nh3 = "--";
	   		}
	   		if (!data.isO2) {
	   			data.o2 = "--";
	   		}   		

			$scope.sensorViewGridOptions.data.unshift( sensorData);
   		}
    };
    
    $scope.isShiftChart = function () {
    	return true;
    }
    
	$scope.insertSensorDataValuesToChartView = function() {
		var sensorDataInQ = $scope.sensorDataQueue2.shift();
    	if ( sensorDataInQ !== 'undefined' ) {
    		if ($scope.setCoverDetectIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.coverDetect);
    			$scope.chart.series[$scope.setCoverDetectIdx].addPoint(point, true, $scope.isShiftChart());
            }
    		if ($scope.setAntennaOptimizedIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.antennaOptimized);
    			
    			$scope.chart.series[$scope.setAntennaOptimizedIdx].addPoint(point, true, $scope.isShiftChart());
            }
    		if ($scope.setWaterLevel1DetectIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.waterLevel1Detect);
    			$scope.chart.series[$scope.setWaterLevel1DetectIdx].addPoint(point, true, $scope.isShiftChart());
            }
    		if ($scope.setWaterLevel2DetectIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.waterLevel2Detect);
    			$scope.chart.series[$scope.setWaterLevel2DetectIdx].addPoint(point, true, $scope.isShiftChart());
            }
    		if ($scope.setMaxStepIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.maxStep);
    			$scope.chart.series[$scope.setMaxStepIdx].addPoint(point, true, $scope.isShiftChart());
            }
    		if ($scope.setAdcValueIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.adcValue);
    			$scope.chart.series[$scope.setAdcValueIdx].addPoint(point, true, $scope.isShiftChart());
            }
    		if ($scope.setBatteryIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.battery);
    			$scope.chart.series[$scope.setBatteryIdx].addPoint(point, true, $scope.isShiftChart());
            }
    		if ($scope.setPacketCounterIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.packetCounter);
    			$scope.chart.series[$scope.setPacketCounterIdx].addPoint(point, true, $scope.isShiftChart());
            }    		
    		if ($scope.setCoverLevelIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.coverLevel);
    			$scope.chart.series[$scope.setCoverLevelIdx].addPoint(point, true, $scope.isShiftChart());
            }   
    		if ($scope.setWaterLevelIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.waterLevel);
    			$scope.chart.series[$scope.setWaterLevelIdx].addPoint(point, true, $scope.isShiftChart());
            }   
    		if ($scope.setH2sIdx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.h2s);
    			$scope.chart.series[$scope.setH2sIdx].addPoint(point, true, $scope.isShiftChart());
            }   	
    		if ($scope.setSo2Idx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.so2);
    			$scope.chart.series[$scope.setSo2Idx].addPoint(point, true, $scope.isShiftChart());
            }    		
    		if ($scope.setCo2Idx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.co2);
    			$scope.chart.series[$scope.setCo2Idx].addPoint(point, true, $scope.isShiftChart());
            }    	
    		if ($scope.setCh4Idx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.ch4);
    			$scope.chart.series[$scope.setCh4Idx].addPoint(point, true, $scope.isShiftChart());
            }    
    		if ($scope.setNh3Idx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.nh3);
    			$scope.chart.series[$scope.setNh3Idx].addPoint(point, true, $scope.isShiftChart());
            } 
    		if ($scope.setO2Idx > -1) {
     			var point = [];
     			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.o2);
    			$scope.chart.series[$scope.setO2Idx].addPoint(point, true, $scope.isShiftChart());
            }     		
    		if ( $scope.setRssiIdx > -1) {
    			var point = [];
    			point.push(sensorDataInQ.date);
    			point.push(sensorDataInQ.rssi);
    			$scope.chart.series[$scope.setRssiIdx].addPoint(point, true, $scope.isShiftChart());
    		}
		}
	};
	
	$scope.$on('receiveSensorData', function(event, sensorDatas) {

		if (sensorDatas != null && sensorDatas.length > 0) {
		 
			var tagConfiguration = $scope.tagConfigurationsMap[sensorDatas[0].deviceId];
			if (typeof tagConfiguration == 'undefined') 
				return;
		 
			$scope.sensorDataQueue.push(sensorDatas[0]);
			$timeout($scope.updateTagConfigurationValues,0);
			
			if ( $scope.selectedTagConfigurationForChartView != null && $scope.selectedTagConfigurationForChartView.deviceId == sensorDatas[0].deviceId ) {
				$scope.sensorDataQueue2.push(sensorDatas[0]);
				$timeout($scope.insertSensorDataValuesToChartView,0);
			}
		}
	});
	
	$scope.insertAlertValues = function() { 
		while ($scope.alertsQueue.length > 0) {
			var alertInQ = $scope.alertsQueue.shift();
	    	if ( alertInQ !== 'undefined' ) {
	    		$scope.alertViewGridOptions.data.unshift(alertInQ);
	    	} else {
	    		return;
	    	}
		}
	};
    
	$scope.$on('receiveAlert', function(event, alert) {
		
		var tagConfiguration = $scope.tagConfigurationsMap[alert.deviceId];
		
		if (typeof tagConfiguration == 'undefined') 
			return;
		
		tagConfiguration.numOfAlerts++;
		alert.isReadAlertData = true;
		
   		if ( tagConfiguration.alertDatas.length == 0) {
   			tagConfiguration.firstAlertDataDate = alert.createDateTimeLong;
   		}
		
		tagConfiguration.alertDatas.unshift(alert);
			
		if ( $scope.selectedTagConfigurationForAlertView != null && alert.tagConfigurationId == $scope.selectedTagConfigurationForAlertView.id ) {
			$scope.alertsQueue.push(alert);
			$timeout($scope.insertAlertValues,0);
		}
	});
	
	
	$rootScope.connectWebSocketData();
 	
	$scope.userCancel = function () {
		window.history.back();
	};
	
    $scope.toggleTagConfiguration = function(selectedTagConfiguration) {
    	selectedTagConfiguration.isShow = !selectedTagConfiguration.isShow;

		for (var i = 0; i < $scope.tagConfigurations.length; ++i) {
			
			var tagConfiguration = $scope.tagConfigurations[i];
			if ( selectedTagConfiguration.id != tagConfiguration.id ) {
				tagConfiguration.isShow = false;
			}
		}
    };
    
	$scope.isTagConfigurationShown = function(tagConfiguration) {
		//return $scope.shownTagConfiguration === tagConfiguration;
		return tagConfiguration.isShow;
	};
	
	$scope.selectTagConfiguration = function (tagConfiguration) {
		for (var i = 0; i < $scope.tagConfigurations.length; ++i) {
			var selectedTagConfiguration = $scope.tagConfigurations[i];
			selectedTagConfiguration.isShow = false;
		}
		tagConfiguration.isShow = true;
	}
	
	$scope.onTagLocation = function(tagConfiguration) {
		highlightMarker(tagConfiguration, true)
	};
	
	$scope.showAlertView = false;
	$scope.showSensorView = false;
	
	$scope.selectedTagConfigurationForAlertView = null;
	$scope.onAlertData = function(tagConfiguration) {
		$scope.selectedTagConfigurationForAlertView = tagConfiguration;
		$scope.showAlertView = true;
		$scope.showSensorView = false;
		$scope.searchAlertData(tagConfiguration);
	};
	
	
	$scope.selectedTagConfigurationForSensorView = null;
	$scope.onSensorData = function(tagConfiguration) {
		$scope.selectedTagConfigurationForSensorView = tagConfiguration;
		$scope.showAlertView = false;
		$scope.showSensorView = true;

		$scope.searchSensorData(tagConfiguration);
	};
	
	
	$scope.selectedTagConfigurationForChartView = null;
	$scope.onChartView = function(tagConfiguration) {
		$scope.selectedTagConfigurationForChartView = tagConfiguration;
		//$scope.setRssi = true;
		$scope.initIdxValues();
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	}
	
	$rootScope.onAlertLocation = function() {
	};
	
	$scope.searchSensorData = function(tagConfiguration ) {
		$scope.sensorFilterDatas = [];
		$scope.sensorViewGridOptions.data = [];
		
		var dateTo = tagConfiguration.sensorDatas.length > 0 ? tagConfiguration.firstSensorDataDate - 1 :  new Date().getTime();
		var fromDate = dateTo - 24*60*60*1000;
		
		$http({  
			url: appMainService.getDomainAddress() + '/rest/chartView/webSocketViewDataMobile/',
			method: 'GET',
	        params: {
		        deviceId: tagConfiguration.deviceId,
		        sensorId: null,
		        dateFrom: fromDate.toString(),
		        dateTo: dateTo.toString(),
		        desc : true
	       }
		}).then(function (response) {
			for (var i = tagConfiguration.sensorDatas.length-1; i >=0; --i ) {
				response.data.unshift( tagConfiguration.sensorDatas[i]);
			}
			for (var j = 0; j < response.data.length; ++j) {
				if (!tagConfiguration.isCoverLevel) {
					response.data[j].coverLevel = "--";
		   		}
		   		if (!tagConfiguration.isWaterLevel) {
		   			response.data[j].waterLevelFromZero2 = "--";
		   		}
		   		if (!tagConfiguration.isH2s) {
		   			response.data[j].h2s = "--";
		   		}		   		
		   		if (!tagConfiguration.isSo2) {
		   			response.data[j].so2 = "--";
		   		}
		   		if (!tagConfiguration.isCo2) {
		   			response.data[j].co2 = "--";
		   		}		   	
		   		if (!tagConfiguration.isCh4) {
		   			response.data[j].ch4 = "--";
		   		}		   		
		   		if (!tagConfiguration.isNh3) {
		   			response.data[j].nh3 = "--";
		   		}
		   		if (!tagConfiguration.isO2) {
		   			response.data[j].o2 = "--";
		   		}
			}
			
			$scope.sensorViewGridOptions.data = response.data;
			//tagConfiguration.numOfSensorDatas = 0;
				
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});	
	}
	
	$scope.searchAlertData = function(tagConfiguration ) {
		$scope.sensorFilterDatas = [];
		$scope.sensorViewGridOptions.data = [];
		
		var dateTo = tagConfiguration.alertDatas.length > 0 ? tagConfiguration.firstAlertDataDate - 1 :  new Date().getTime();
		var fromDate = dateTo - 24*60*60*1000;
		
		$http({  
			url: appMainService.getDomainAddress() + '/rest/alert/getAlert/',
			method: 'GET',
	        params: {
	        	fromDate: fromDate.toString(),
	        	toDate: dateTo.toString(),
		        level: null,
		        alertSettingId: null,
		        tagConfigurationId: tagConfiguration.id,
		        desc: true
	       }
		}).then(function (response) {
			for (var i = tagConfiguration.alertDatas.length-1; i >=0; --i ) {
				response.data.unshift( tagConfiguration.alertDatas[i]);
			}
			$scope.alertViewGridOptions.data = response.data;
				
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});	
	}
	$scope.rowHeight = 30; 
	var innerHeight = 450;
	$scope.numberOfRows = $scope.panelHeightNum / $scope.rowHeight - 3;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows) - 1;
	
	$scope.updateSensorDataReadFlag = null; 
	$scope.updateFlag = function() {
		$scope.updateSensorDataReadFlag.isReadSensorData = false;
	};
	
	$scope.openSensorData = function(sensorData) {
		$scope.updateSensorDataReadFlag = sensorData;
		
		if ( $scope.selectedTagConfigurationForSensorView != null && $scope.selectedTagConfigurationForSensorView.numOfSensorDatas > 0) {
			$scope.selectedTagConfigurationForSensorView.numOfSensorDatas--;
		} 
		
		$timeout($scope.updateFlag,0);
	};
	
	$scope.openAlertSetting = function(alertSettingId) {
		$http({
	   		url: appMainService.getDomainAddress() + '/rest/alertSetting/getAlertSetting/',
	   		method: 'GET',
         params: {
             id: alertSettingId,
             name: null,
             level: null,
             useDateRange: null,
             fromDate: null,
             toDate: null,
             deviceAddress: null,
             deviceId : null,
             groupId : null,
             isSendEmail: null,
             isCoverLevel: null,
             isWaterLevel: null,
             isH2s: null,
             isSo2: null,
             isCo2: null,
             isCh4: null,
             isNh3: null,
             isO2: null
            }
		}).then(function (response) {
			if (response.data == null || response.data.length == 0 ) {
				return; 
			}
			var data = response.data[0];
			
			appMainService.setAlertSettingHistoryData($scope.alertViewGridOptions.data);
			appMainService.setAlertSetting(data);
			appMainService.setStartDate($scope.startDate);
			appMainService.setEndDate($scope.endDate);
			 
			var path = 'Alert/AlertSetting/New/';
			$location.path(path); 
				
	   	},function (response){
	    	alert("Failed to search data, status=" + response.status);
	   	});
	}
	
	$scope.openAlert = function(alert) {
		$scope.selectedTagConfigurationForAlertView.numOfAlerts--;
		alert.isReadAlertData = false;
		
		var path = 'view/alert/alertDialogView.html'; 
	
		$scope.formData = new FormData();
		$scope.formData.alert = alert;
		
		ngDialog.openConfirm({template: path,
			scope: $scope //Pass the scope object if you need to access in the template
		}).then(
			//Update
			function(value) {
				if ( value == true) {
					appMainService.setDashBoardSelectedTagConfigurationId($scope.formData.alert.tagConfigurationId);
					$route.reload();
					return;
				} else {
					$scope.openAlertSetting($scope.formData.alert.alertSettingId);
				}
			},
			//Cancel
			function(value) {
			}		
		);				 		
	};
	$scope.setColumnDefs = function() {
		var columnDefs = [
            {field:'dateString',  width: 160, displayName: $translate.instant('DATE'), headerCellClass: 'uiGridthemeHeaderColor', 
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)" ><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.dateString}}</a></div>'},
 		 	{field:'readerIds',  width: 100, displayName: $translate.instant('READER_ID'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.readerIds}}</a></div>',
				 	filter: { condition: uiGridConstants.filter.EXACT } },
 	 		{field:'deviceId',  width: 100, displayName: $translate.instant('DEVICE_ID'), headerCellClass: 'uiGridthemeHeaderColor', 
 			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.deviceId}}</a></div>',
				 	filter: { condition: uiGridConstants.filter.EXACT } },
  			{field:'coverDetectStr',  width: 120, displayName: $translate.instant('COVER_DETECT'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.coverDetectStr}}</a></div>'},
 	 	 	{field:'waterLevel1DetectStr',  width: 120, displayName: $translate.instant('WATER_LEVEL_1_DETECT'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 		 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.waterLevel1DetectStr}}</a></div>'},		
 	 	 	{field:'waterLevel2DetectStr',  width: 120, displayName: $translate.instant('WATER_LEVEL_2_DETECT'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 		 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.waterLevel2DetectStr}}</a></div>'},		
 	  	 	{field:'antennaOptimizedStr',  width: 120, displayName: $translate.instant('ANTENNA_OPTIMIZED'), headerCellClass: 'uiGridthemeHeaderColor', 
 	   	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.antennaOptimizedStr}}</a></div>'},		
 	    	{field:'maxStep',  width: 120, displayName: $translate.instant('MAX_STEP'), headerCellClass: 'uiGridthemeHeaderColor', 
 	   	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.maxStep}}</a></div>'},		
 	 	  	{field:'adcValue',  width: 120, displayName: $translate.instant('ADC_VALUE'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.adcValue}}</a></div>'},	
 	 		{field:'coverLevel',  width: 120, displayName: $translate.instant('COVER_LEVEL'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.coverLevel}}</a></div>'},
 			{field:'waterLevel',  width: 120, displayName: $translate.instant('WATER_LEVEL'), headerCellClass: 'uiGridthemeHeaderColor', 
 					cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.waterLevel}}</a></div>'},
 	 	 	{field:'h2s',  width: 100, displayName: $translate.instant('H2S'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.h2s}}</a></div>'},
 			{field:'so2',  width: 100, displayName: $translate.instant('SO2'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.so2}}</a></div>'},
 			{field:'ch4',  width: 100, displayName: $translate.instant('CH4'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.ch4}}</a></div>'},
 			{field:'rssi',  width: 90, displayName: $translate.instant('RSSI'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.rssi}}</a></div>'},
			{field:'batteryStr',  width: 90, displayName: $translate.instant('BATTERY'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.batteryStr}}</a></div>'},
		    {field:'packetCounter',  width: 90, displayName: $translate.instant('PACKET_COUNTER'), headerCellClass: 'uiGridthemeHeaderColor', 
 	 				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadSensorData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'"  ng-click="grid.appScope.openSensorData(row.entity)"><a uib-popover popover-trigger="mouseenter" popover-placement="right" uib-popover-template="\'popover1.html\'">{{row.entity.packetCounter}}</a></div>'} 	 
		 ];		
		$scope.columnDefs = columnDefs;
	};
	

	
	$scope.setColumnDefs();
	
	
	$scope.realTime = true;	
	$scope.sensorViewGridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: true,
		enableVerticalScrollbar: 0,
		rowHeight: $scope.rowHeight,
		
		exporterMenuPdf : false,
		enableGridMenu: true,
	    enableSelectAll: true,
	    exporterCsvFilename: 'diagnosticSensorViewFile.csv',
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	 
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {

		    });
		},
		columnDefs: $scope.columnDefs
	};
	
	$scope.getSensorViewTableStyle= function() {
        return {
            height: (($scope.numberOfRows +  2)* $scope.sensorViewGridOptions.rowHeight + $scope.sensorViewGridOptions.headerRowHeight ) + "px"
        };
    };
    
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.sensorViewGridOptions.columnDefs = $scope.columnDefs;
		
		$scope.setColumnDefsAlert();  
		$scope.alertViewGridOptions.columnDefs = $scope.columnDefsAlert;
	});
	
	$scope.isSensorSelected = function () {
		return ( $scope.setCoverDetect == true 
			|| $scope.setAntennaOptimized == true
			|| $scope.setWaterLevel1Detect == true
			|| $scope.setWaterLevel2Detect == true
			|| $scope.setMaxStep == true
			|| $scope.setAdcValue == true
			|| $scope.setCoverLevel == true
			|| $scope.setWaterLevel == true
			|| $scope.setH2s == true
			|| $scope.setSo2 == true 
			|| $scope.setCh4 == true
			|| $scope.setRssi == true 
			|| $scope.setBattery == true 
			|| $scope.setPacketCounter == true 
			|| $scope.setNh3 == true 
			|| $scope.setO2 == true 
			|| $scope.setCo2 == true );
	};
	
	$scope.initSearchFlags = function () {
		$scope.setCoverDetect = false;
		$scope.setAntennaOptimized = false;
		$scope.setWaterLevel1Detect = false;
		$scope.setWaterLevel2Detect = false;
		$scope.setMaxStep = false;
		$scope.setAdcValue = false;
		$scope.setCoverLevel = false;
		$scope.setWaterLevel = false;
		$scope.setH2s = false;
		$scope.setSo2 = false;
		$scope.setCh4 = false;
		$scope.setRssi = false;
		$scope.setBattery = false;
		$scope.setPacketCounter = false;
		$scope.setNh3 = false;
		$scope.setO2 = false;
		$scope.setCo2 = false;
	};
	$scope.initSearchFlags();
	
	$scope.initIdxValues = function() {
		$scope.setCoverDetectIdx = -1;
		$scope.setAntennaOptimizedIdx = -1;
		$scope.setWaterLevel1DetectIdx = -1;
		$scope.setWaterLevel2DetectIdx = -1;
		$scope.setMaxStepIdx = -1;
		$scope.setAdcValueIdx = -1;
		$scope.setBatteryIdx = -1;
		$scope.setPacketCounterIdx = -1;
		$scope.setCoverLevelIdx = -1;
		$scope.setWaterLevelIdx = -1;
		$scope.setH2sIdx = -1; 
		$scope.setSo2Idx = -1; 
		$scope.setCo2Idx = -1; 
		$scope.setCh4Idx = -1; 
		$scope.setNh3Idx = -1; 
		$scope.setO2Idx = -1; 
		$scope.setRssiIdx = -1; 
		
	}
	$scope.initIdxValues();
	
	$scope.toggleCoverDetect = function() {
		$scope.setCoverDetect = !$scope.setCoverDetect;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleAntennaOptimized = function() {
		$scope.setAntennaOptimized = !$scope.setAntennaOptimized;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleWaterLevel1Detect = function() {
		$scope.setWaterLevel1Detect = !$scope.setWaterLevel1Detect;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleWaterLevel2Detect = function() {
		$scope.setWaterLevel2Detect = !$scope.setWaterLevel2Detect;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};

	$scope.toggleMaxStep = function() {
		$scope.setMaxStep = !$scope.setMaxStep;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleAdcValue = function() {
		$scope.setAdcValue = !$scope.setAdcValue;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleCoverLevel = function() {
		$scope.setCoverLevel = !$scope.setCoverLevel;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleWaterLevel = function() {
		$scope.setWaterLevel = !$scope.setWaterLevel;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleH2s = function() {
		$scope.setH2s = !$scope.setH2s;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleSo2 = function() {
		$scope.setSo2 = !$scope.setSo2;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleCh4 = function() {
		$scope.setCh4 = !$scope.setCh4;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleRssi = function() {
		$scope.setRssi = !$scope.setRssi;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleBattery = function() {
		$scope.setBattery = !$scope.setBattery;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.togglePacketCounter = function() {
		$scope.setPacketCounter = !$scope.setPacketCounter;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleNh3 = function() {
		$scope.setNh3 = !$scope.setNh3;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleO2 = function() {
		$scope.setO2 = !$scope.setO2;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.toggleCo2 = function() {
		$scope.setCo2 = !$scope.setCo2;
		$scope.createChart($scope.selectedTagConfigurationForChartView);
	};
	
	$scope.color = ["#FF0000","#00FF00","#0000FF","	#FFFF00","#FF00FF","#00FFFF","#FF8080","#3366FF","#99CC00","#FF6600","#99CCFF","#FF99CC","#00CCFF", "#CCFFFF","#FFFFCC","#CCCCFF","#000000"];

	$scope.isSearchSuccess = false;
	$scope.createChart = function(tagConfiguration) {
		$scope.isSearchSuccess = false;
		if ( tagConfiguration == null) {
			return;
		}
		
		var endDate = new Date().getTime();
		var startDate = endDate - 7*24*60*60*1000;
		
		$scope.buttons = [];
		$scope.helloWorld = function () {
	
		};
		
		$scope.buttons.push({
		    text:  $scope.setCoverDetect ? "<strong>" + $translate.instant("COVER_DETECT") + "</strong>" : $translate.instant("COVER_DETECT"),
		    onclick: $scope.toggleCoverDetect
		});
	
		$scope.buttons.push({
		    text:  $scope.setWaterLevel1Detect ? "<strong>" + $translate.instant("WATER_LEVEL_1_DETECT") + "</strong>" : $translate.instant("WATER_LEVEL_1_DETECT"),
		    onclick: $scope.toggleWaterLevel1Detect
		});
	
		$scope.buttons.push({
		    text:  $scope.setWaterLevel2Detect ? "<strong>" + $translate.instant("WATER_LEVEL_2_DETECT") + "</strong>" : $translate.instant("WATER_LEVEL_2_DETECT"),
		    onclick: $scope.toggleWaterLevel2Detect
		});	
		
		if ( tagConfiguration.isCoverLevel) {
			$scope.buttons.push({
			    text:  $scope.setCoverLevel ? "<strong>" + $translate.instant("COVER_LEVEL") + "</strong>" : $translate.instant("COVER_LEVEL"),
			    onclick: $scope.toggleCoverLevel
			});		
		}
		
		if ( tagConfiguration.isWaterLevel) {
			$scope.buttons.push({
			    text:  $scope.setWaterLevel ? "<strong>" + $translate.instant("WATER_LEVEL") + "</strong>" : $translate.instant("WATER_LEVEL"),
			    onclick: $scope.toggleWaterLevel
			});		
		}
		
		if ( tagConfiguration.isH2s) {
			$scope.buttons.push({
			    text:  $scope.setH2s ? "<strong>" + $translate.instant("H2S") + "</strong>" : $translate.instant("H2S"),
			    onclick: $scope.toggleH2s
			});			
		}
		
		if ( tagConfiguration.isSo2) {
			$scope.buttons.push({
			    text:  $scope.setSo2 ? "<strong>" + $translate.instant("SO2") + "</strong>" : $translate.instant("SO2"),
			    onclick: $scope.toggleSo2
			});	
		}
		
		if ( tagConfiguration.isCo2) {
			$scope.buttons.push({
			    text:  $scope.setCo2 ? "<strong>" + $translate.instant("CO2") + "</strong>" : $translate.instant("CO2"),
			    onclick: $scope.toggleCo2
			});
		}
		
		if ( tagConfiguration.isCh4) {
			$scope.buttons.push({
			    text:  $scope.setCh4 ? "<strong>" + $translate.instant("CH4") + "</strong>" : $translate.instant("CH4"),
			    onclick: $scope.toggleCh4
			});
		}
		
		if ( tagConfiguration.isNh3) {
			$scope.buttons.push({
			    text:  $scope.setNh3 ? "<strong>" + $translate.instant("NH3") + "</strong>" : $translate.instant("NH3"),
			    onclick: $scope.toggleNh3
			});	
		}
		
		if ( tagConfiguration.isO2) {
			$scope.buttons.push({
			    text:  $scope.setO2 ? "<strong>" + $translate.instant("O2") + "</strong>" : $translate.instant("O2"),
			    onclick: $scope.toggleO2
			});
		}
		
		$scope.buttons.push({
		    text:  $scope.setRssi ? "<strong>" + $translate.instant("RSSI") + "</strong>" : $translate.instant("RSSI"),
		    onclick: $scope.toggleRssi
		});
	
		
		$scope.buttons = $scope.buttons.concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems);
		
		$http({  
			url: appMainService.getDomainAddress() + '/rest/chartView/webSocketViewDataMobile/',
			method: 'GET',
	        params: {
		        deviceId: tagConfiguration.deviceId,
		        sensorId: tagConfiguration.deviceId,
		        dateFrom: startDate.toString(),
		        dateTo: endDate.toString(),
		        desc : false
	       }
		}).then(function (response) {
			if (response.data.length > 0) {
				$scope.isSearchSuccess = true;
    			var coverDetectData = [];
    			var antennaOptimizedData = [];
    			var waterLevel1DetectData = [];
    			var waterLevel2DetectData = [];
    			var maxStepData = [];
    			var adcValueData = [];
    			
    			var coverLevelData = [];
    			var waterLevelData = []; 
    			var h2sData = []; 
    			var so2Data = []; 
    			var co2Data = []; 
    			var ch4Data = []; 
    			var nh3Data = []; 
    			var o2Data = []; 
    			var rssiData = []; 
    			var batteryData = [];
    			var packetCounterData = [];
    			
    			for (var i=0; i < response.data.length; ++i) {
    				var sensorChartData = response.data[i];
    				
    				coverDetectData.push([sensorChartData.dateLong, sensorChartData.coverDetect]);
    				antennaOptimizedData.push([sensorChartData.dateLong, sensorChartData.antennaOptimized]);
    				waterLevel1DetectData.push([sensorChartData.dateLong, sensorChartData.waterLevel1Detect]);
    				waterLevel2DetectData.push([sensorChartData.dateLong, sensorChartData.waterLevel2Detect]);
    				maxStepData.push([sensorChartData.dateLong, sensorChartData.maxStep]);
    				adcValueData.push([sensorChartData.dateLong, sensorChartData.adcValue]);
        			
    				coverLevelData.push([sensorChartData.dateLong, sensorChartData.coverLevel]);
    				waterLevelData.push([sensorChartData.dateLong, sensorChartData.waterLevelFromZero2]);
    				h2sData.push([sensorChartData.dateLong, sensorChartData.h2s]);
    				so2Data.push([sensorChartData.dateLong, sensorChartData.so2]);
    				co2Data.push([sensorChartData.dateLong, sensorChartData.co2]);
    				ch4Data.push([sensorChartData.dateLong, sensorChartData.ch4]);
    				nh3Data.push([sensorChartData.dateLong, sensorChartData.nh3]);
    				o2Data.push([sensorChartData.dateLong, sensorChartData.o2]);
    			
    				batteryData.push([sensorChartData.dateLong, sensorChartData.battery])
    				packetCounterData.push([sensorChartData.dateLong, sensorChartData.packetCounter]);
    				
    				rssiData.push([sensorChartData.dateLong, sensorChartData.rssi ]);
    			}
    			
    			var series = [];
       			var selectIdx = -1;
    			if ($scope.setCoverDetect) {
	     			var coverDetectObject = {
	     				name: $translate.instant('COVER_DETECT') + " (1=Open/0=Closed)",
		            	data: coverDetectData,
		            	color: $scope.color[0], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
	     			series.push(coverDetectObject);
	            	$scope.setCoverDetectIdx = ++selectIdx; 
                }
    			if ($scope.setAntennaOptimized) {
	     			var antennaOptimizedObject = {
		            	name: $translate.instant('ANTENNA_OPTIMIZED'),
		            	data: antennaOptimizedData,
		            	color: $scope.color[1], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
		     		series.push(antennaOptimizedObject);
		     		$scope.setAntennaOptimizedIdx = ++selectIdx; 
                }
    			if ($scope.setWaterLevel1Detect) {
	     			var waterLevel1DetectObject = {
	     				name: $translate.instant('WATER_LEVEL_1_DETECT') + " (1=High/0=Low)",
		            	data: waterLevel1DetectData,
		            	color: $scope.color[2], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
	     			series.push(waterLevel1DetectObject);
	     			$scope.setWaterLevel1DetectIdx = ++selectIdx; 
                }
    			if ($scope.setWaterLevel2Detect) {
	     			var waterLevel2DetectObject = {
	     				name: $translate.instant('WATER_LEVEL_2_DETECT') + " (1=High/0=Low)",
		            	data: waterLevel2DetectData,
		            	color: $scope.color[3], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
		     		series.push(waterLevel2DetectObject);
		        	$scope.setWaterLevel2DetectIdx = ++selectIdx; 
                }
    			if ($scope.setMaxStep) {
	     			var maxStepObject = {
		            	name: $translate.instant('MAX_STEP'),
		            	data: maxStepData,
		            	color: $scope.color[4], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
		     		series.push(maxStepObject);
			     	$scope.setMaxStepIdx = ++selectIdx; 
                }
    			if ($scope.setAdcValue) {
	     			var adcValueObject = {
		            	name: $translate.instant('ADC_VALUE'),
		            	data: adcValueData,
		            	color: $scope.color[5], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
			     	series.push(adcValueObject);
	     	    	$scope.setAdcValueIdx = ++selectIdx; 
                }
    			if ($scope.setBattery) {
	     			var batteryObject = {
	     				name: $translate.instant('BATTERY') + " (1=High/0=Low)",
		            	data: batteryData,
		            	color: $scope.color[6], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
				    series.push(batteryObject);
             		$scope.setBatteryIdx = ++selectIdx; 
                }
    			
    			if ($scope.setPacketCounter) {
	     			var packetCounterObject = {
		            	name: $translate.instant('PACKET_COUNTER'),
		            	data: packetCounterData,
		            	color: $scope.color[7], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
				    series.push(packetCounterObject);
					$scope.setPacketCounterIdx = ++selectIdx; 
                }    			
    			
    			if ($scope.setCoverLevel) {
	     			var coverLevelObject = {
	     				name: $translate.instant('COVER_LEVEL') + " (cm)",
		            	data: coverLevelData,
		            	color: $scope.color[8], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
				    series.push(coverLevelObject);
                	$scope.setCoverLevelIdx = ++selectIdx; 
                }
                if ($scope.setWaterLevel) {
                	
	     			var waterLevelObject = {
	     				name: $translate.instant('WATER_LEVEL') + " (cm) from Top",
		            	data: waterLevelData,
		            	color: $scope.color[9], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
				    series.push(waterLevelObject);
                	$scope.setWaterLevelIdx = ++selectIdx; 
                }
                if ($scope.setH2s) {
	     			var h2sObject = {
	     				name: $translate.instant('H2S') + " (ppm)",
		            	data: h2sData,
		            	color: $scope.color[10], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
				    series.push(h2sObject);
					$scope.setH2sIdx = ++selectIdx; 
                }
                if ($scope.setSo2 ) {
	     			var so2Object = {
		            	name: $translate.instant('SO2') + " (ppm)",
		            	data: so2Data,
		            	color: $scope.color[11], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
					series.push(so2Object);
                	$scope.setSo2Idx = ++selectIdx; 
                }
                if ($scope.setCo2 ) {
	     			var co2Object = {
		            	name: $translate.instant('CO2') + " (ppm)",
		            	data: co2Data,
		            	color: $scope.color[12], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
					series.push(co2Object);
                	$scope.setCo2Idx = ++selectIdx; 
                }
                if ($scope.setCh4 ) {
	     			var ch4Object = {
		            	name: $translate.instant('CH4') + " (ppm)",
		            	data: ch4Data,
		            	color: $scope.color[13], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
					series.push(ch4Object);
                	$scope.setCh4Idx = ++selectIdx; 
                }
                if ($scope.setNh3) {
	     			var nh3Object = {
		            	name: $translate.instant('NH3') + " (ppm)",
		            	data: nh3Data,
		            	color: $scope.color[14], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
					series.push(nh3Data);
                	$scope.setNh3Idx = ++selectIdx; 
                }
                if ($scope.setO2 ) {
	     			var o2Object = {
		            	name: $translate.instant('O2') + " (ppm)",
		            	data: o2Data,
		            	color: $scope.color[15], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
					series.push(o2Object);
                	$scope.setO2Idx = ++selectIdx; 
                }
                if ($scope.setRssi) {
	     			var rssiObject = {
		            	name: $translate.instant('RSSI')+ " (dBm)",
		            	data: rssiData,
		            	color: $scope.color[16], 
		            	lineWidth: 3,
		            	marker: {
		            		enabled: null, // auto
		            		radius: 6,
		            		lineWidth: 1,
		            		lineColor: '#FFFFFF'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	}
	     			};
					series.push(rssiObject);
	     		  	$scope.setRssiIdx = ++selectIdx; 
                }
                
    			Highcharts.setOptions({
    				global: {
               	        useUTC: false
               	    }
               	});
    			$scope.chart = new Highcharts.stockChart('container', {
    		        chart: {
    		            renderTo: 'container',
    		            type:'spline',
    		            zoomType: 'x',
    		            backgroundColor : {
    		                linearGradient : [0,0, 0, 300],
    		                stops : [
		                         [0, 'rgb(53, 174, 201)'],
		                         [1, 'rgb(0, 33, 115)']
    		                ]
    		            }
    		        },
    		        rangeSelector: {
    		        	allButtonsEnabled: true,
    		            buttons: [
    		            {
							type : 'minute',
							count : 1,
							text : $translate.instant('MINUTE'),
//							dataGrouping : {
//								forced : true,
//								units : [ ['second', [ 1 ] ] ]
//							}
						}, 
    		            {
							type : 'hour',
							count : 1,
							text :  $translate.instant('HOUR'),
//							dataGrouping : {
//								forced : true,
//								units : [ ['minute', [ 1 ] ] ]
//							}
    				    }, 
    		            {
							type : 'day',
							count : 1,
							text : $translate.instant('DAY'),
//							dataGrouping : {
//								forced : true,
//								units : [ ['hour', [ 1 ] ] ]
//							}
    					},            
    		            {
							type : 'week',
							count : 1,
							text : $translate.instant('WEEK'),
//							dataGrouping : {
//								forced : true,
//								units : [ ['day', [ 1 ] ] ]
//							}
    					}, 
    		            {
    		                type: 'month',
    		                count: 3,
    		                text: $translate.instant('MONTH'),
//    		                dataGrouping: {
//    		                    forced: true,
//    		                    units: [['day', [1]]]
//    		                }
    		            }, 
    		            {
    		                type: 'year',
    		                count: 1,
    		                text: $translate.instant('YEAR'),
//    		                dataGrouping: {
//    		                    forced: true,
//    		                    units: [['week', [1]]]
//    		                }
    		            },  
    		            {
    		                type: 'all',
    		                count: 1,
    		                text: $translate.instant('ALL'),
//    		                dataGrouping: {
//    		                    forced: true,
//    		                    units: [['month', [1]]]
//    		                }
    		            }],
    		            buttonTheme: {
    		                width: 60
    		            },
    		            selected: 3
    		        },
    		        title: {
    		        	text: "<b>" + $translate.instant('DEVICE_ID') + "</b>: " + response.data[0].deviceId + ",<b>" + $translate.instant('TAG_NAME') + ":</b>" + tagConfiguration.name,
						style: { "color": "#3fff3f", "fontSize": "18px", "fontWeight" : "normal" }
    		      	},
    		        xAxis: {
    		        	lineColor: '#3fff3f',
    		        	type: 'datetime',
    		        	tickInterval: 60 * 1000, //tick interval 1 minute
    		            labels: {
    		                style: {
    		                   color: '#3fff3f',
    		                   font: '12px Trebuchet MS, Verdana, sans-serif'
    		                }
    		             },
    		        },
    		        yAxis: {
    		        	lineColor: '#3fff3f',
    		        	title: {
    		        		text: ""
    		        	},
    		        	gridLineColor: '#3fff3f',
    		            labels: {
    		                style: {
    		                   color: '#3fff3f',
    		                   font: '14px Trebuchet MS, Verdana, sans-serif'
    		                }
    		            },
    		        		 
    		        },
    		        plotOptions: {
    		            spline: {
    		                marker: {
    		                    enabled: true
    		                },
							dataGrouping: {
    		                    enabled: false
    		                }
    		            },
    		        },
    		        tooltip: {
    		          	shared: true,
    		            crosshairs: true,
    		            useHTML: true,
    		            headerFormat: '<span style="font-size: 12px">{point.key}</span><br/>',
    		        },
    		        legend:{
    		          	enabled: true,
    		          	itemStyle: { "color": "#3fff3f", "cursor": "pointer", "fontSize": "12px", "fontWeight": "bold" }
    		        },
    		        exporting: {
    		            buttons: {
    		                contextButton: {
    		                    menuItems: $scope.buttons
    		                }
    		            }
    		        },
    		        series : series
    		   });
    		}
		}
    	,function (response){
			alert("Failed to search data, status=" + response.status);
		});	
  	};
	
	$scope.setColumnDefsAlert = function() {
		var columnDefs = [
  			{field:'info', width: 30, displayName: '',	enableFiltering : false,headerCellClass: 'uiGridthemeHeaderColor', 	 
				cellTemplate: '<img ng-src="{{grid.appScope.getIcon(row.entity)}}" width="24" height="24"></img>'},
		    {field:'createDateTimeStr',  width: 90, displayName: $translate.instant('TIME'),  enableFiltering : false, headerCellClass: 'uiGridthemeHeaderColor', 
			  cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.createTimeStr}}</a></div>'},
		    {field:'alertSettingLevel',  width: 100, displayName: $translate.instant('ALERT_LEVEL'), headerCellClass: 'uiGridthemeHeaderColor', 
			       cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.alertSettingLevel}}</a></div>'},
		    {field:'alertSettingName',  width: 140, displayName: $translate.instant('ALERT_NAME'), headerCellClass: 'uiGridthemeHeaderColor', 
			       cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.alertSettingName}}</a></div>'},
			{field:'coverDetect',  width: 120, displayName: $translate.instant('COVER_DETECT'), headerCellClass: 'uiGridthemeHeaderColor', 
			 	cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
				 	if (row.entity.isCoverDetect == true) {
				 		return 'redCellColor';
				 	}
			 	},
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.coverDetect}}</a></div>'},
	
		 	{field:'waterLevel',  width: 120, displayName: $translate.instant('WATER_LEVEL'), headerCellClass: 'uiGridthemeHeaderColor', 
				 cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
				 	if (row.entity.isWaterLevel == true) {
				 		return 'redCellColor';
				 	}
				},			 		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.waterLevel}}</a></div>'},
			
		 	{field:'h2s',  width: 100, displayName: $translate.instant('H2S'), headerCellClass: 'uiGridthemeHeaderColor', 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isH2s == true) {
				 		return 'redCellColor';
				 	}
				},
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.h2s}}</a></div>'},
			
			{field:'so2',  width: 100, displayName: $translate.instant('SO2'), headerCellClass: 'uiGridthemeHeaderColor', 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isSo2 == true) {
				 		return 'redCellColor';
				 	}
				},			 		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.so2}}</a></div>'},
		 	
			{field:'ch4',  width: 100, displayName: $translate.instant('CH4'), headerCellClass: 'uiGridthemeHeaderColor', 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isCh4 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.ch4}}</a></div>'},	
			{field:'nh3',  width: 100, displayName: $translate.instant('NH3'), headerCellClass: 'uiGridthemeHeaderColor', 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isNh3 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.nh3}}</a></div>'},	
			{field:'o2',  width: 100, displayName: $translate.instant('O2'), headerCellClass: 'uiGridthemeHeaderColor', 
				cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
					if (row.entity.isO2 == true) {
				 		return 'redCellColor';
				 	}
				},		
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="row.entity.isReadAlertData ? \'diagnosticSensorViewFont col.colIndex()\' : \'diagnosticSensorViewFont2 col.colIndex()\'" ng-click="grid.appScope.openAlert(row.entity)"><a>{{row.entity.o2}}</a></div>'}					
		 ];	
		 $scope.columnDefsAlert = columnDefs;
	};
	$scope.setColumnDefsAlert();
	
	$scope.alertViewGridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: false,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		rowHeight: $scope.rowHeight,
		enableColumnResizing : true,
		exporterMenuPdf : false,
		enableGridMenu: true,
	    enableSelectAll: true,
	    exporterCsvFilename: 'alertHistoryViewFile.csv',
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,
	    onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {
			});
			$scope.tagConfigurationGridApi.core.notifyDataChange( uiGridConstants.dataChange.OPTIONS);
		},
		columnDefs: $scope.columnDefsAlert
	};
	
	$scope.getAlertViewTableStyle = function() {
        return {
            height: (($scope.numberOfRows +  1)* $scope.alertViewGridOptions.rowHeight + $scope.alertViewGridOptions.headerRowHeight ) + 30 + "px"
        };
    };

	$scope.getIcon = function (alert) {
		if (alert.alertSettingLevel == "Critical") {
			return "img/basic/criticalalert2.png";
		}
		if (alert.alertSettingLevel == "High") {
			return "img/basic/highalert2.png";
		}
		if (alert.alertSettingLevel == "Medium") {
			return "img/basic/mediumalert2.png";
		}
		return "img/basic/lowalert2.png";
	};
	
	$rootScope.chartWidthOrig = $rootScope.sideMenuOn == false ? document.getElementById("container").offsetWidth : document.getElementById("container").offsetWidth + 115 ;
	$rootScope.chartWidth = $rootScope.sideMenuOn == true ?  $rootScope.chartWidthOrig - 115 + "px" : $rootScope.chartWidthOrig + "px";
	

	$scope.$on('dashBoardViewToggle', function(event, sideMenuOn) {
		if ( $scope.isSearchSuccess == false ) {
			return;
		}
		if ( sideMenuOn == true ) {
			$rootScope.chartWidth = $rootScope.chartWidthOrig - 115 + "px";
		}
		if ( sideMenuOn == false ) {
			$rootScope.chartWidth = $rootScope.chartWidthOrig + "px";
		}
		
		if ( $scope.selectedTagConfigurationForChartView != null) {
			$scope.onChartView($scope.selectedTagConfigurationForChartView);
		}
	});
	
 	$http({
   		url: appMainService.getDomainAddress() + '/rest/diagnosticView/webSocketViewDataByDeviceSensor/',
   		method: 'GET',
          params: {
        	  deviceId: '-1',
		      sensorId: '-1'
	      }
   	}).then(function (response) {

   	},function (response){
   
   	});
}]);