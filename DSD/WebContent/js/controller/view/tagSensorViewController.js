angular.module('dsd').controller('tagSensorViewCtrl', ['$rootScope','$scope', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants',  'appMainService', '$translate', '$window', 'ngDialog',
 function ($rootScope, $scope,$uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, $translate, $window, ngDialog) {
	$scope.rowHeight = 30; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - (8 + 4);
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows) - 1;
	
	$scope.selectedTag = appMainService.getTagConfiguration();
	$scope.searching = false;
	
	$scope.startDate = appMainService.getStartDate();
	$scope.endDate = appMainService.getEndDate();
	
	$scope.realTime = false;	

	$scope.setFromDate = function(date) {
		$scope.startDate = new Date(date).getTime().toString();
	};
	
	$scope.setToDate = function(date) {
		$scope.endDate = new Date(date).getTime().toString();
	}

	$scope.tagConfigurations = [];
	$http({
		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
		method: 'GET',
        params: {
        id: null, 
        name: null, 
        deviceId : null,
        sensorId : null,
        description: null,
        siteName : null,
        siteId : null,
        active: null
       }
	}).then(function (response) {
		$scope.tagConfigurations = response.data;
		if ( $scope.selectedTag != null ) {
			for ( var s=0; s < $scope.tagConfigurations.length; ++s ) {
				if ($scope.tagConfigurations[s].id == $scope.selectedTag.id) {
					$scope.selectedTag = $scope.tagConfigurations[s];
					$scope.onDeviceIdSelected($scope.selectedTag.sensorConfigs[0]);
					$scope.searchSensorData();
					break;
				}
			}
		}
	},function (response){
 		alert("Failed to search data, status=" + response.status);
	});	
	
	$scope.onDeviceIdSelected = function(sensorConfig) {
		$scope.selectedTag.isCoverLevel = sensorConfig.isCoverLevel;
		$scope.selectedTag.isWaterLevel = sensorConfig.isWaterLevel;
		$scope.selectedTag.isH2s = sensorConfig.isH2s;
		$scope.selectedTag.isSo2 = sensorConfig.isSo2;
		$scope.selectedTag.isCo2 = sensorConfig.isCo2;	
		$scope.selectedTag.isCh4 = sensorConfig.isCh4;	
		$scope.selectedTag.isNh3 = sensorConfig.isNh3;	
		$scope.selectedTag.isO2 = sensorConfig.isO2;	
	};
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
        	{field:'dateString',  width: 160, displayName: $translate.instant('DATE'), headerCellClass: 'uiGridthemeHeaderColor',
   		           cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.dateString}}</a></div>'},
 		 	{field:'readerIds',  width: 100, displayName: $translate.instant('READER_ID'), headerCellClass: 'uiGridthemeHeaderColor',
	 			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.readerIds}}</a></div>',
			 	filter: { condition: uiGridConstants.filter.EXACT } },
  	 		{field:'coverDetectStr',  width: 120, displayName: $translate.instant('COVER_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',
   	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.coverDetectStr}}</a></div>'},
   	 	 	{field:'waterLevel1DetectStr',  width: 85, displayName: $translate.instant('WATER_LEVEL_1_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',	
   	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.waterLevel1DetectStr}}</a></div>'},		
   	 	 	{field:'waterLevel2DetectStr',  width: 85, displayName: $translate.instant('WATER_LEVEL_2_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',
   	  	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.waterLevel2DetectStr}}</a></div>'},		
	    	{field:'waterLevelFromZero2',  width: 115, displayName: $translate.instant('WATER_LEVEL'), headerCellClass: 'uiGridthemeHeaderColor',
   	   		 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="cm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.waterLevelFromZero2}}</a></div>'},
  	 		{field:'coverLevel',  width: 110, displayName: $translate.instant('COVER_LEVEL'), headerCellClass: 'uiGridthemeHeaderColor',
   	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="cm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.coverLevel}}</a></div>'},
   		 	{field:'h2s',  width: 70, displayName: $translate.instant('H2S'), headerCellClass: 'uiGridthemeHeaderColor',
   			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="ppm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.h2s}}</a></div>'},
   			{field:'so2',  width: 70, displayName: $translate.instant('SO2'), headerCellClass: 'uiGridthemeHeaderColor',
   			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="ppm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.so2}}</a></div>'},
    		{field:'ch4',  width: 80, displayName: $translate.instant('CH4'), headerCellClass: 'uiGridthemeHeaderColor',	
  	 			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="ppm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.ch4}}</a></div>'},	
  			{field:'readerRssi',  width: 200, displayName: $translate.instant('READER_RSSI'), headerCellClass: 'uiGridthemeHeaderColor',
  				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="dBm"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.readerRssi}}</a></div>'},
  		    {field:'batteryStr',  width: 90, displayName: $translate.instant('BATTERY'), headerCellClass: 'uiGridthemeHeaderColor',
  				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.batteryStr}}</a></div>'},
  		    {field:'packetCounter',  width: 90, displayName: $translate.instant('PACKET_COUNTER'), headerCellClass: 'uiGridthemeHeaderColor', 
  					cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.packetCounter}}</a></div>'}, 	 
  		    {field:'timeDiffInMsStr',  width: 130, displayName: $translate.instant('TIME_DIFF'), headerCellClass: 'uiGridthemeHeaderColor',	
   				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()"><a uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover1.html\'">{{row.entity.timeDiffInMsStr}}</a></div>'}
   		 ];	
		$scope.columnDefs = columnDefs;
	};
	$scope.setColumnDefs();
	
	$scope.getTableStyle= function() {
        return {
            height: (($scope.numberOfRows +  2)* $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight ) + "px"
        };
    };
    
	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: false,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		rowHeight: 30,
		
		exporterMenuPdf : false,
		enableGridMenu: true,
	    enableSelectAll: true,
	    exporterCsvFilename: 'tagSensorViewFile.csv',
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	  
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {

		    });
		},
		columnDefs: $scope.columnDefs
	 };
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	
	$scope.sensorData = [];
	$scope.isSearchSuccess = false;
	
	$scope.searchSensorData = function() {
		$scope.isSearchSuccess = false;
		$scope.isLoading = true;
		$scope.realTime = false;
		$scope.loadingMessage = $translate.instant('LOADING_PLEASE_WAIT');
		
		ngDialog.open({
			scope: $scope,
			className: "ngdialog-theme-loading", 
			template: "view/loadingDialog.html",
            showClose: false,
            closeByEscape : false
		});
		
		$http({  
			url: appMainService.getDomainAddress() + '/rest/chartView/webSocketViewData/',
			method: 'GET',
	        params: {
	        	deviceId: $scope.selectedTag.deviceId,
	        	sensorId: $scope.selectedTag.deviceId, 
		        dateFrom: $scope.startDate.toString(),
		        dateTo: $scope.endDate.toString(),
		        desc: true
	       }
		}).then(function (response) {
			$scope.realTime = true;
			$scope.isLoading = false;
			if (response.data.length > 0) {
				$scope.isSearchSuccess = true;
				var sensorDatas = [];
				for (var i=0; i<response.data.length; ++i) {
					var sensorData = response.data[i];
					
					if (!$scope.selectedTag.isCoverLevel) {
			   			sensorData.coverLevel = "--";
			   		}
			   		if (!$scope.selectedTag.isWaterLevel) {
			   			sensorData.waterLevelFromZero2 = "--";
			   		}
			   		if (!$scope.selectedTag.isH2s) {
			   			sensorData.h2s = "--";
			   		}		   		
			   		if (!$scope.selectedTag.isSo2) {
			   			sensorData.so2 = "--";
			   		}
			   		if (!$scope.selectedTag.isCo2) {
			   			sensorData.co2 = "--";
			   		}		   	
			   		if (!$scope.selectedTag.isCh4) {
			   			sensorData.ch4 = "--";
			   		}		   		
			   		if (!$scope.selectedTag.isNh3) {
			   			sensorData.nh3 = "--";
			   		}
			   		if (!$scope.selectedTag.isO2) {
			   			sensorData.o2 = "--";
			   		}
			   		sensorDatas.push( sensorData );
				}
				
				$scope.gridOptions.data = sensorDatas;
				ngDialog.close();
			} else {
				$scope.isSearchSuccess = false;
				$scope.loadingMessage = $translate.instant('NO_DATA_FOUND');
			}

		},function (response){
			$scope.isLoading = false;
			$scope.isSearchSuccess = false;
			$scope.loadingMessage = ("Failed to search data, status=" + response.status);	
		});	
	};


	$scope.toggleRealTime = function() {

	};
	
	$scope.$on('receiveSensorData', function(event, sensorDatas) {
		console.log('receiveSensorData: ' + $scope.realTime);
		if ( $scope.realTime == false ) {
			return;
		}
		
		for (var i = 0; i < sensorDatas.length; ++i) {
			var sensorData = sensorDatas[i]; 
			
			if ( $scope.selectedTag.deviceId != sensorData.deviceId) {
				continue;
			}
			
			sensorData.isRead = false;
			
			if (!$scope.selectedTag.isCoverLevel) {
	   			sensorData.coverLevel = "--";
	   		}
	   		if (!$scope.selectedTag.isWaterLevel || !sensorData.isWaterLevel) {
	   			sensorData.waterLevelFromZero2 = "--";
	   		}
	   		if (!$scope.selectedTag.isH2s) {
	   			sensorData.h2s = "--";
	   		}		   		
	   		if (!$scope.selectedTag.isSo2) {
	   			sensorData.so2 = "--";
	   		}
	   		if (!$scope.selectedTag.isCo2) {
	   			sensorData.co2 = "--";
	   		}		   	
	   		if (!$scope.selectedTag.isCh4) {
	   			sensorData.ch4 = "--";
	   		}		   		
	   		if (!$scope.selectedTag.isNh3) {
	   			sensorData.nh3 = "--";
	   		}
	   		if (!$scope.selectedTag.isO2) {
	   			sensorData.o2 = "--";
	   		}
			$scope.gridOptions.data.unshift(sensorDatas[i]);
	   		$scope.$apply();
		}
	   			
	});

	$rootScope.connectWebSocketData();
	
	$scope.userCancel = function () {
		window.history.back();
	};
	
	$scope.onSelectTagFromMap = function() {
		$scope.selectedTagConfigurationFromMap = $scope.selectedTag;
		$scope.selectedTagOrig = $scope.selectedTag;
		
		ngDialog.openConfirm({template: "view/selectTagLocationDialog.html",
			scope: $scope //Pass the scope object if you need to access in the template
		}).then(
			//Update
			function(value) {
				$scope.selectedTag = $scope.selectedTagConfigurationFromMap;
		 		if ( $scope.selectedTag != null ) {
					for ( var s=0; s < $scope.tagConfigurations.length; ++s ) {
						if ($scope.tagConfigurations[s].id == $scope.selectedTag.id) {
							$scope.selectedTag = $scope.tagConfigurations[s];
							$scope.onDeviceIdSelected($scope.selectedTag.sensorConfigs[0]);
							break;
						}
					}
				}
		 	},
			//Cancel
			function(value) {
		 		$scope.selectedTag = $scope.selectedTagOrig;	
		 		if ( $scope.selectedTag != null ) {
					for ( var s=0; s < $scope.tagConfigurations.length; ++s ) {
						if ($scope.tagConfigurations[s].id == $scope.selectedTag.id) {
							$scope.selectedTag = $scope.tagConfigurations[s];
							$scope.onDeviceIdSelected($scope.selectedTag.sensorConfigs[0]);
							break;
						}
					}
				}
			}		
		);				 
		
	};
	
	$scope.searchTagConfigurations = function () {
	   	$http({
	   		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/webSocketViewData/',
	   		method: 'GET',
	           params: {
	           id: null, 
	           name: null, 
	           deviceId : null,
	           sensorId : null,
	           groupId : null,
	           description: null,
	           siteName : null,
	           siteId : null
	          }
	   	}).then(function (response) {
	   		$scope.tagConfigurations = response.data;
	   		$scope.markers = [];
	   		
   			for (var i = 0; i < $scope.tagConfigurations.length; ++i) {
   				var tagConfiguration = $scope.tagConfigurations[i];

   				tagConfiguration.markerClassName = "fa fa-map-marker fa-stack-1x yellowColor";
	            if ( tagConfiguration.validationResult == true) {
		   			tagConfiguration.markerClassName = "fa fa-map-marker fa-stack-1x greenColor";
	            } else if ( tagConfiguration.validationResult == false ) {
		   			tagConfiguration.markerClassName = "fa fa-map-marker fa-stack-1x redColor";
	            }
	    	}
   			scrollToMarkers( $scope.tagConfigurations );
   			
     	},function (response){
	   		alert("Failed to search data, status=" + response.status);
	   	});
	};
	
 	$http({
   		url: appMainService.getDomainAddress() + '/rest/diagnosticView/webSocketViewDataByDeviceSensor/',
   		method: 'GET',
          params: {
        	  deviceId: '-1',
		      sensorId: '-1' 
	      }
   	}).then(function (response) {

   	},function (response){
   	});
}]);

	
