angular.module('dsd').controller('tableViewCtrl', ['$rootScope','$scope', '$translate','$interval','$timeout','$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants','appMainService','$window',
 function ($rootScope, $scope, $translate, $interval, $timeout, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, $window) {
    
	$scope.rowHeight = 30; 
	$scope.numberOfRows = $window.innerHeight / $scope.rowHeight - 7;
	$scope.numberOfRowsInt = Math.round($scope.numberOfRows) - 1;
	
	$scope.section="";
	$scope.users=[
	];
	$scope.roles=[
	];
	$scope.modules = [];
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.userTotalItems = 100;
	$scope.userCurrentPage = 1;
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
		
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	
    if ( appMainService.getTableViewGroupId() > 0)
    	$scope.selectedGroupId = appMainService.getTableViewGroupId();
    else 
    	$scope.selectedGroupId = 1; 
    
    $scope.changeLanguage=function(locale) {

	};
	
	$scope.toggleFiltering = function(){
		$scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
	    $scope.tagConfigurationGridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	 
	};
	
    //System Settings
    $scope.showSystemSetting = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'view/systemSettingView.html',
			controller: 'SystemSettingCtrl',
			size: 'lg',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
    
    
    
    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });
 
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.persons = {currentPage : 1};
	
	$scope.openTagConfigurationSensorData = function(sensorConfig) {

		var tagConfiguration = new Object();
		tagConfiguration.id = sensorConfig.tagConfigurationId; 
		tagConfiguration.deviceId = sensorConfig.deviceId; 
		tagConfiguration.sensorId = sensorConfig.sensorId; 
		
		appMainService.setTagConfiguration(tagConfiguration);
		
		appMainService.setTableViewSiteId($scope.selectedSiteId);
		appMainService.setTableViewGroupId($scope.selectedGroupId);
		appMainService.setTableView($scope.gridOptions.data);
		
		var path = 'View/TagConfigurationSensorData/';
		path += (tagConfiguration.id+"");
		$location.path(path);
	};
	
	$scope.openTagConfigurationChartData = function(sensorConfig) {
		var tagConfiguration = new Object();
		tagConfiguration.id = sensorConfig.tagConfigurationId; 
		tagConfiguration.deviceId = sensorConfig.deviceId; 
		tagConfiguration.sensorId = sensorConfig.sensorId; 
		
		appMainService.setTagConfiguration(tagConfiguration);
		
		appMainService.setTableViewSiteId($scope.selectedSiteId);
		appMainService.setTableViewGroupId($scope.selectedGroupId);
		appMainService.setTableView($scope.gridOptions.data);
		
		var path = 'View/ChartView/';
		$location.path(path);
	};
	
	$scope.openTagConfigurationAlertData = function(sensorConfig) {
		
		var tagConfiguration = new Object();
		tagConfiguration.id = sensorConfig.tagConfigurationId; 
		tagConfiguration.deviceId = sensorConfig.deviceId; 
		tagConfiguration.sensorId = sensorConfig.sensorId; 
		
		appMainService.setTagConfiguration(tagConfiguration);
		var path = 'Alert/AlertHistory/';
		$location.path(path);	
	}
	
	$scope.openTagConfigurationMapData = function(sensorConfig) {
		$http({
			url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
			method: 'GET',
	        params: {
	        id: $scope.selectedSiteId, 
	        name: null, 
	        description: null,
	        defaultSite: null
	       }
		}).then(function (response) {
			var siteConfigurations = response.data;
			if ( siteConfigurations.length > 0) {
				appMainService.setSiteConfiguration($scope.siteConfigurations[0]);
				
				var tagConfiguration = new Object();
				tagConfiguration.id = sensorConfig.tagConfigurationId; 
				tagConfiguration.deviceId = sensorConfig.deviceId; 
				tagConfiguration.sensorId = sensorConfig.sensorId; 
				
				appMainService.setTagConfiguration(tagConfiguration);
				var path = 'View/FloorPlanView/';
				$location.path(path);	
			}
		},function (response){
	 		alert("Failed to find site configuration list, status=" + response.status);
		});

	}
	
	
	$scope.removeTagConfiguration = function (row){
		var path = 'SystemConfiguration/TagConfiguration/Delete/';
		path += (row.entity.id+"");
		$location.path(path);
	};
	
	$scope.name = function(grid,row,col) {
		if(row.groupHeader && row.treeNode.children[0].row.treeNode.children[0]) {
			var entity = row.treeNode.children[0].row.treeNode.children[0].row.entity;
			return entity.name;
		}
	    else if(row.treeNode.children[0])
	    {
	    	var entity = row.treeNode.children[0].row.entity;
	    	return entity.name;
	    }
		return row.entity.name;
	}
	
	$scope.tagConfigurationTotalItems = 100;
	$scope.tagConfigurationCurrentPage = 1;
	
	
	$scope.setColumnDefs = function() {
		var columnDefs = [
   	 		{field:'dateString',  width: 140, displayName: $translate.instant('DATE'), enableFiltering : false, headerCellClass: 'uiGridthemeHeaderColor',
 		           cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.dateString\'>{{row.entity.dateString}}</span></div>'},				 	
		    {field:'name', displayName: $translate.instant('TAG_NAME'), width: 200, headerCellClass: 'uiGridthemeHeaderColor',
 				  	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="mouseenter" popover-placement="bottom" uib-popover-template="\'popover2.html\'" ng-mouseenter="grid.appScope.setMouseOverTagConfiguration(row.entity)">{{row.entity.name}}</div>'},	
			{field:'readerIds',  width: 100, displayName: $translate.instant('READER_ID'),  headerCellClass: 'uiGridthemeHeaderColor',
				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.readerIds\'><a>{{row.entity.readerIds}}</a><span></div>', 				 	
				 	filter: { condition: uiGridConstants.filter.EXACT } }, 
 		    {field:'deviceId',  width: 100, displayName: $translate.instant('DEVICE_ID'), headerCellClass: 'uiGridthemeHeaderColor',
	 			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.deviceId\'><a>{{row.entity.deviceId}}</a></span></div>',
			 	filter: { condition: uiGridConstants.filter.EXACT } },
	 		{field:'coverDetectStr',  width: 120, displayName: $translate.instant('COVER_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',
 	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.coverDetectStr\'><a>{{row.entity.coverDetectStr}}</a></span></div>'},
 	 	 	{field:'waterLevel1DetectStr',  width: 85, displayName: $translate.instant('WATER_LEVEL_1_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',
 	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.waterLevel1DetectStr\'><a>{{row.entity.waterLevel1DetectStr}}</a></span></div>'},		
 	 	 	{field:'waterLevel2DetectStr',  width: 85, displayName: $translate.instant('WATER_LEVEL_2_DETECT'), headerCellClass: 'uiGridthemeHeaderColor',
 	  	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.waterLevel2DetectStr\'><a>{{row.entity.waterLevel2DetectStr}}</a></span></div>'},		
	 		{field:'waterLevelFromZero2',  width: 115, displayName: $translate.instant('WATER_LEVEL'), headerCellClass: 'uiGridthemeHeaderColor',
	 	 	 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="cm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.waterLevelFromZero2\'>{{row.entity.waterLevelFromZero2}}</span></div>'},
			{field:'h2s',  width: 70, displayName: $translate.instant('H2S'), headerCellClass: 'uiGridthemeHeaderColor', 
			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="ppm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.h2s\'>{{row.entity.h2s}}</span></div>'},
			{field:'so2',  width: 70, displayName: $translate.instant('SO2'), headerCellClass: 'uiGridthemeHeaderColor',
		 			cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="ppm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.so2\'>{{row.entity.so2}}</span></div>'},
			{field:'ch4',  width: 80, displayName: $translate.instant('CH4'), headerCellClass: 'uiGridthemeHeaderColor',
			 		cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="ppm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.ch4\'>{{row.entity.ch4}}</span></div>'},
		    {field:'readerRssi',  width: 200, displayName: $translate.instant('READER_RSSI'), headerCellClass: 'uiGridthemeHeaderColor', 
				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" data-toggle="tooltip" title="dBm" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.readerRssi\'>{{row.entity.readerRssi}}</span></div>'},	
			{field:'batteryStr',  width: 90, displayName: $translate.instant('BATTERY'), headerCellClass: 'uiGridthemeHeaderColor',
				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.batteryStr\'><a>{{row.entity.batteryStr}}</a></span></div>'},
		    {field:'packetCounter',  width: 90, displayName: $translate.instant('PACKET_COUNTER'), headerCellClass: 'uiGridthemeHeaderColor',
				 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'"><span animate-on-change=\'row.entity.packetCounter\'>{{row.entity.packetCounter}}</span></div>'},			

//			{field:'measurePeriod',  width: 100, displayName: $translate.instant('MEASURE_PERIOD'),  enableFiltering : true,
//			 	 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openTagConfiguration(row.entity.tagConfigurationId)"><span animate-on-change=\'row.entity.measurePeriod\'>{{row.entity.measurePeriod}}</span></div>'},
			{field:'timeDiffInMsStr',  width: 130, displayName: $translate.instant('TIME_DIFF'), headerCellClass: 'uiGridthemeHeaderColor',
			 	 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" uib-popover popover-trigger="outsideClick" popover-placement="bottom" uib-popover-template="\'popover1.html\'" ng-click="grid.appScope.openTagConfiguration(row.entity.tagConfigurationId)"><span animate-on-change=\'row.entity.timeDiffInMsStr\'>{{row.entity.timeDiffInMsStr}}</span></div>'},
		];		
		$scope.columnDefs = columnDefs;
	};
	
	$scope.setColumnDefs();
	
	$scope.$on('updateLanguageInUiGrid', function() {

		$scope.setColumnDefs();  
		$scope.gridOptions.columnDefs = $scope.columnDefs;
	});
	  
	$scope.getTableStyle= function() {
        return {
            height: (($scope.numberOfRows +  2)* $scope.gridOptions.rowHeight + $scope.gridOptions.headerRowHeight ) + "px"
        };
    };
    
	$scope.gridOptions={
		minRowsToShow: $scope.numberOfRowsInt,
		enableFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableRowHeaderSelection: false,
		enableHorizontalScrollbar: 1,
		enableVerticalScrollbar: 1,
		exporterMenuPdf : false,
		rowHeight: $scope.rowHeight,
		enableColumnResizing : true,
	    enableGridMenu: true,
	    exporterCsvFilename: 'tableViewFile.csv',
	    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
	    paginationPageSizes: [$scope.numberOfRowsInt, $scope.numberOfRowsInt*2, $scope.numberOfRowsInt*3, $scope.numberOfRowsInt*4],
	    paginationPageSize: $scope.numberOfRowsInt,	  
		onRegisterApi: function(gridApi){
			$scope.tagConfigurationGridApi = gridApi;
			$scope.tagConfigurationGridApi.core.on.filterChanged( $scope, function() {

		    });
//			$scope.tagConfigurationGridApi.grid.registerDataChangeCallback(function() {
//				$scope.tagConfigurationGridApi.treeBase.expandAllRows();
//		    });
		},
		columnDefs: $scope.columnDefs
	};
	
	$scope.openTagConfiguration = function (tagConfigurationId) {
		$http({
    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
    		method: 'GET',
            params: {
            id: tagConfigurationId, 
            name: null, 
            deviceId : null,
            deviceAddress : null,
            groupId : null,
            description: null,
            siteName : null,
            siteId : null,
           }
    	}).then(function (response) {
    		if (response.data != null && response.data.length > 0) {
    			var tagConfiguration = response.data[0];   			
    			appMainService.setTagConfiguration(tagConfiguration);
    			appMainService.setTableViewSiteId($scope.selectedSiteId);
    			appMainService.setTableViewGroupId($scope.selectedGroupId);
    			appMainService.setTableView($scope.gridOptions.data);
    			
    			$location.path('SystemConfiguration/TagConfiguration/New/');  
    		}
    	},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
	};
	
	$scope.searchTagConfiguration = function (deviceId) {
		$http({
    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
    		method: 'GET',
            params: {
            id: null, 
            name: null, 
            deviceId : deviceId,
            deviceAddress : null,
            groupId : null,
            description: null,
            siteName : null,
            siteId : null,
           }
    	}).then(function (response) {
    		if (response.data != null && response.data.length > 0) {
    			highlightMarker(response.data[0]);
    		}
    	},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
	};
	
	
	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
	
	$scope.siteConfigurations = [];
	$scope.selectedSiteId = 0;
	
	$http({
		url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
		method: 'GET',
        params: {
        id: null, 
        name: null, 
        description: null,
        defaultSite: null
       }
	}).then(function (response) {
		$scope.siteConfigurations = response.data;
		if ( $scope.siteConfigurations.length > 0) {
			if (appMainService.getTableViewSiteId() > 0 ) {
				$scope.selectedSiteId = appMainService.getTableViewSiteId();
				
			} else {
				$scope.selectedSiteId = $scope.siteConfigurations[0].id;
			}
	   		
			var tableViewData = appMainService.getTableView();
			if ( tableViewData.length == 0) {
				$scope.searchTag($scope.selectedSiteId, $scope.selectedGroupId);
			} else {
				$scope.gridOptions.data = tableViewData;
			}
		}
	},function (response){
 		alert("Failed to find site configuration list, status=" + response.status);
	});
	
 	$http({
   		url: appMainService.getDomainAddress() + '/rest/diagnosticView/webSocketViewDataByDeviceSensor/',
   		method: 'GET',
          params: {
        	  deviceId: '-1',
		      sensorId: '-1', 
	      }
   	}).then(function (response) {

   	},function (response){
   	});
 	
	$scope.setSelection = function () {
		var selectedTag = appMainService.getTagConfiguration();
		if (  $scope.gridOptions.data == null || selectedTag == null ) {
			return;
		}
		for (var i = 0; i < $scope.gridOptions.data.length; ++i) {
			if ( $scope.gridOptions.data[i].tagConfigurationId == selectedTag.id ) {
				$scope.tagConfigurationGridApi.selection.selectRow(i);
				$scope.tagConfigurationGridApi.selection.toggleRowSelection($scope.gridOptions.data[i]);
				$scope.$apply();
				break;
			}
		}	
	};
  	
	$scope.getAllRecentSensorConfig = function(deviceIds) {
	    var date = new Date();
		var startDate = new Date(date.getTime() - 24*60*60*1000);
		$http({  
			url: appMainService.getDomainAddress() + '/rest/diagnosticView/getRecentSensorChartDataAllDeviceId/',
			params: {
				deviceId: null,
				dateFrom: startDate.getTime().toString(),
				dateTo: date.getTime().toString() 
			}, 
			method: 'GET',
		}).then(function (response) {
			if( response.data.length > 0 ) {
				for (var s = 0; s < deviceIds.length; ++s) {
					var deviceId = deviceIds[s];
					for (var j = 0; j < response.data.length; ++j ) {
						var respData = response.data[j];
						if ( respData.deviceId == deviceId) {
							for (var i = 0; i < $scope.gridOptions.data.length; ++i) {
								if ( $scope.gridOptions.data[i].deviceId == deviceId) {
									
									var rowToUpdate = $scope.gridOptions.data[i];
									var data = respData;
									
									rowToUpdate.readerId = data.readerId;
									rowToUpdate.dateString2 = data.dateString2;
									rowToUpdate.dateString = data.dateString;
									rowToUpdate.coverLevel = data.coverLevel; 
									rowToUpdate.waterLevelFromZero2 = data.waterLevelFromZero2; 
									rowToUpdate.h2s = data.h2s; 
									rowToUpdate.so2 = data.so2; 
									rowToUpdate.co2 = data.co2; 
									rowToUpdate.ch4 = data.ch4; 		   				
									rowToUpdate.nh3 = data.nh3; 			   				
									rowToUpdate.o2 = data.o2; 	
									rowToUpdate.readerId = data.readerId;
									rowToUpdate.readerIds = data.readerIds;
									
									rowToUpdate.measurePeriod = data.measurePeriod;	
									rowToUpdate.timeDiffInMsStr = data.timeDiffInMsStr;
									
									rowToUpdate.coverDetectStr = data.coverDetectStr;
									rowToUpdate.waterLevel1DetectStr = data.waterLevel1DetectStr;
									rowToUpdate.waterLevel2DetectStr = data.waterLevel2DetectStr;
									rowToUpdate.antennaOptimizedStr = data.antennaOptimizedStr;
									rowToUpdate.maxStep = data.maxStep;
									rowToUpdate.adcValue = data.adcValue;
									
									rowToUpdate.rssi = data.rssi;
									rowToUpdate.packetCounter = data.packetCounter;
									rowToUpdate.battery = data.battery;
									rowToUpdate.batteryStr = data.batteryStr;
									rowToUpdate.readerRssi = data.readerRssi;
									
									if (!rowToUpdate.isCoverLevel) {
										rowToUpdate.coverLevel = "--";
									}
									if (!rowToUpdate.isWaterLevel) {
										rowToUpdate.waterLevelFromZero2 = "--";
									}
									if (!rowToUpdate.isH2s) {
									rowToUpdate.h2s = "--";
									}		   		
									if (!rowToUpdate.isSo2) {
										rowToUpdate.so2 = "--";
									}
									if (!rowToUpdate.isCo2) {
									rowToUpdate.co2 = "--";
									}		   	
									if (!rowToUpdate.isCh4) {
										rowToUpdate.ch4 = "--";
									}		   		
									if (!rowToUpdate.isNh3) {
										rowToUpdate.nh3 = "--";
									}
									if (!rowToUpdate.isO2) {
										rowToUpdate.o2 = "--";
									}
									return;
								}
							}
						}
					}
				}
			}
		},function (response){
			alert("Failed to search data, status=" + response.status);
		});	
	};
	
	$scope.searchTag = function (siteId, groupId) {
		$http({
	   		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchDataTableView/',
	   		method: 'GET',
	           params: {
	        	   groupId : groupId,
	           	   siteId : siteId
	          }
	   	}).then(function (response) {
	   		$scope.gridOptions.data = response.data;
			var deviceIds = [];
	   		for (var i = 0; i < response.data.length; ++i) {
		   		var tagConfiguration = response.data[i];
		   		deviceIds.push(tagConfiguration.deviceId);
		   	}
   			
	   		$scope.getAllRecentSensorConfig(deviceIds);
		 	$timeout($scope.setSelection, 0);
	   	},function (response){
	    	alert("Failed to search data, status=" + response.status);
	   	});		
	}; 
	$scope.$on('receiveSensorData', function(event, sensorDatas) {

		for (var i = 0; i < sensorDatas.length; ++i) {
			var data = sensorDatas[i];
			if (!data.isCoverLevel) {
	   			data.coverLevel = "--";
	   		}
	   		if (!data.isWaterLevel) {
	   			data.waterLevelFromZero2 = "--";
	   		}
	   		if (!data.isH2s) {
	   			data.h2s = "--";
	   		}		   		
	   		if (!data.isSo2) {
	   			data.so2 = "--";
	   		}
	   		if (!data.isCo2) {
	   			data.co2 = "--";
	   		}		   	
	   		if (!data.isCh4) {
	   			data.ch4 = "--";
	   		}		   		
	   		if (!data.isNh3) {
	   			data.nh3 = "--";
	   		}
	   		if (!data.isO2) {
	   			data.o2 = "--";
	   		}
	   		
	   		for (var i = 0; i < $scope.gridOptions.data.length; ++i) {
	   			if ( $scope.gridOptions.data[i].deviceId == data.deviceId) {
	   				
	   				var rowToUpdate = $scope.gridOptions.data[i];
	   				rowToUpdate.dateString2 = data.dateString2;
					rowToUpdate.dateString = data.dateString;
	   				rowToUpdate.coverLevel = data.coverLevel; 
	   				rowToUpdate.waterLevelFromZero2 = data.waterLevelFromZero2; 
	   				rowToUpdate.h2s = data.h2s; 
	   				rowToUpdate.so2 = data.so2; 
	   				rowToUpdate.co2 = data.co2; 
	   				rowToUpdate.ch4 = data.ch4; 		   				
	   				rowToUpdate.nh3 = data.nh3; 			   				
	   				rowToUpdate.o2 = data.o2; 	
	   				rowToUpdate.readerId = data.readerId;
	   				
	   				rowToUpdate.measurePeriod = data.measurePeriod;	
	   				rowToUpdate.timeDiffInMsStr = data.timeDiffInMsStr;
	   				
	   				rowToUpdate.rssi = data.rssi;
	   				rowToUpdate.packetCounter = data.packetCounter;
	   				rowToUpdate.battery = data.battery;
	   				rowToUpdate.readerRssi = data.readerRssi;
	   				break;
	   			}
	   		}
		}
	});
	
	$rootScope.connectWebSocketData();
	
	$scope.mouseOverTagConfiguration = null;
	$scope.setMouseOverTagConfiguration = function(tagConfiguration) {
		$scope.mouseOverTagConfiguration = tagConfiguration;
	};
	
	
	$scope.getMouseOverTagConfiguration = function() {
		var mouseOverTagConfiguration = $scope.mouseOverTagConfiguration;
		$scope.mouseOverTagConfiguration = null;
		return mouseOverTagConfiguration;
	};
	
}]);


