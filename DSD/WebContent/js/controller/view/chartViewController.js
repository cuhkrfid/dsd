angular.module('dsd').controller('chartViewCtrl', ['$rootScope', '$scope', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService', 'ngDialog', '$translate', '$window','$routeParams',
 function ($rootScope, $scope, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService, ngDialog, $translate, $window, $routeParams) {
	$scope.isChartView = $location.path() === "/View/ChartView";
	
	$scope.waterLevelFromZeroMin = null;
	$scope.yAxisMinValue = -20;
    $scope.yAxisMaxValue = 100;
    $scope.yAxisFixMaxValue = $scope.yAxisMaxValue;
    $scope.waterLevelFromTroughMax = null;
    
	var time = new Date().getTime();
	$scope.startDate = time - 7*24*60*60*1000;
	$scope.endDate = time;

	$scope.setFromDate = function(date) {
		$scope.startDate = new Date(date).getTime().toString();
	};
	
	$scope.setToDate = function(date) {
		$scope.endDate = new Date(date).getTime().toString();
	}
	
	
	$scope.color = ["#FF0000","#00FF00","#0000FF","	#FFFF00","#FF00FF","#00FFFF","#FF8080","#3366FF","#99CC00","#FF6600","#9a14bc","#FF99CC","#00CCFF", "#CCFFFF","#FFFFCC","#CCCCFF","#000000"];
	
	$scope.getAllDeviceId = function() {
		var date = new Date();
		var startDate = new Date(date.getTime() - 7*24*60*60*1000);
		$http({  
			url: appMainService.getDomainAddress() + '/rest/diagnosticView/getAllDeviceId/',
			params: {
				dateFrom: startDate.getTime().toString(),
				dateTo: date.getTime().toString(),
			},
			method: 'GET',
		}).then(function (response) {
			$scope.deviceIdConfigurations = response.data;
			for (var i = 0; i <  $scope.deviceIdConfigurations.length; ++i) {
				if ( $scope.deviceIdConfigurations[i] == appMainService.getDiagnosticChartViewDeviceId()) {
					$scope.selectDeviceId = $scope.deviceIdConfigurations[i];
					$scope.getSensor($scope.selectDeviceId, true);
					$scope.searchSensorData();
					break;
				}
			}
			for (var j = 0; j <  $scope.deviceIdConfigurations.length; ++j) {
				if ( $scope.selectDeviceId == null && $scope.deviceIdConfigurations.length > 0) {
					$scope.selectDeviceId = $scope.deviceIdConfigurations[0];
					
					$scope.getSensor($scope.selectDeviceId, true);
				} else {
					$scope.getSensor($scope.deviceIdConfigurations[j], false);
				}
			}
			
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});	
	};
	
	$scope.getSensor = function(deviceId, select) {
		$http({  
			url: appMainService.getDomainAddress() + '/rest/diagnosticView/getAllSensor/',
			method: 'GET',
	        params: {
		        deviceId: deviceId
	       }
		}).then(function (response) {
			if ( response.data != null && response.data.length > 0 ) {
				$scope.sensorConfigurations[deviceId] = response.data[0];
				if (select == true) {
					$scope.onDeviceIdChanged(deviceId);
				}
			}	
		},function (response){
		});	
	};
	
	$scope.onDeviceIdChanged = function(selectDeviceId) {
		var selectSensor = $scope.sensorConfigurations[selectDeviceId];
		if ( selectSensor == null || typeof selectSensor === "undefined")
			return;
		
		$scope.initSearchFlags();
		
		$scope.isCoverLevel = selectSensor.isCoverLevel;
		$scope.isWaterLevel = selectSensor.isWaterLevel;
		$scope.isH2s = selectSensor.isH2s;
		$scope.isSo2 = selectSensor.isSo2;
		$scope.isCo2 = selectSensor.isCo2;	
		$scope.isCh4 = selectSensor.isCh4;
		$scope.isNh3 = selectSensor.isNh3;	
		$scope.isO2 = selectSensor.isO2;	
	};
	
	if ($scope.isChartView) {
		$scope.selectedTag = appMainService.getTagConfiguration();
		$scope.tagConfigurations = [];
		$http({
			url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
			method: 'GET',
	        params: {
	        id: null, 
	        name: null, 
	        deviceId : null,
	        sensorId : null,
	        description: null,
	        siteName : null,
	        siteId : null,
	        active: null
	       }
		}).then(function (response) {
			$scope.tagConfigurations = response.data;
			if ( $scope.selectedTag != null ) {
				for ( var s=0; s < $scope.tagConfigurations.length; ++s ) {
					if ($scope.tagConfigurations[s].id == $scope.selectedTag.id) {
						$scope.selectedTag = $scope.tagConfigurations[s];
						$scope.onTagSelected($scope.selectedTag);
						$scope.searchSensorData();
						break;
					}
				}
			}
		},function (response){
	 		alert("Failed to search data, status=" + response.status);
		});	
	} else {
		$scope.selectDeviceId = null;
		$scope.deviceIdConfigurations = [];
		$scope.sensorConfigurations = [];
		$scope.getAllDeviceId(); 
	}
	
	$scope.initSearchFlags = function() {

		$scope.isCoverLevel = false;
		$scope.isWaterLevel = false;
		$scope.isH2s = false;
		$scope.isSo2 = false;
		$scope.isCo2 = false;
		$scope.isCh4 = false;
		$scope.isNh3 = false;
		$scope.isO2 = false;
		
		$scope.chart = null
		$scope.realTime = false;
		$scope.searching = false;
		
		$scope.isSensorTypeSelected = false;
	};
	
	$scope.initSearchFlags();
	
	$scope.canPlot = function () { 
		if ($scope.numOfSensorSelected == 1) {
			if ( $scope.yAxisMinValue === "" || $scope.yAxisMaxValue === ""
				 || isNaN($scope.yAxisMinValue) ||  isNaN($scope.yAxisMaxValue) ) {
				return false;
			}
		}
		return true;
	};
	
	$scope.onTagSelected = function(tagSelected) {
		if( tagSelected == null)
			return; 
		
		$scope.initSearchFlags();	

		if (typeof tagSelected.sensorConfigs[0] == "undefined") {
			return;
		}

		$scope.isCoverLevel = tagSelected.sensorConfigs[0].isCoverLevel;
		$scope.isWaterLevel = tagSelected.sensorConfigs[0].isWaterLevel;
		$scope.isH2s = tagSelected.sensorConfigs[0].isH2s;
		$scope.isSo2 = tagSelected.sensorConfigs[0].isSo2;
		$scope.isCo2 = tagSelected.sensorConfigs[0].isCo2;	
		$scope.isCh4 = tagSelected.sensorConfigs[0].isCh4;
		$scope.isNh3 = tagSelected.sensorConfigs[0].isNh3;	
		$scope.isO2 = tagSelected.sensorConfigs[0].isO2;	

	}

	
	$scope.initIdxValues = function() {
		$scope.setCoverDetectIdx = -1;
		$scope.setAntennaOptimizedIdx = -1;
		$scope.setWaterLevel1DetectIdx = -1;
		$scope.setWaterLevel2DetectIdx = -1;
		$scope.setMaxStepIdx = -1;
		$scope.setAdcValueIdx = -1;
		$scope.setBatteryIdx = -1;
		$scope.setPacketCounterIdx = -1;
		$scope.setCoverLevelIdx = -1;
		$scope.setWaterLevelIdx = -1;
		$scope.setH2sIdx = -1; 
		$scope.setSo2Idx = -1; 
		$scope.setCo2Idx = -1; 
		$scope.setCh4Idx = -1; 
		$scope.setNh3Idx = -1; 
		$scope.setO2Idx = -1; 
		$scope.setRssiIdx = -1; 
		
	}
	$scope.initIdxValues();
	
	$scope.isSearchSuccess = false;
	
	$scope.exportToCsv = function() {
		alert($scope.chart.getCSV());
	};
	
	$scope.searchSensorData = function() {
		if ( $scope.isChartView == true) {
			if ( $scope.selectedTag == null) 
				return;
		} else {
			if ( $scope.selectDeviceId == null) 
				return;
		}
		
		var deviceId = $scope.isChartView == true ? $scope.selectedTag.deviceId : $scope.selectDeviceId;
		
		$scope.initIdxValues();
		$scope.isLoading = true;
		$scope.isSearchSuccess = false;
		$scope.realTime = false;
		$scope.loadingMessage = $translate.instant('LOADING_PLEASE_WAIT');
		
//		$scope.buttons = [];
//		$scope.buttons.push({
//		    text:  $translate.instant("EXPORT_TO_CSV"),
//		    onclick: $scope.exportToCsv
//		});
		
		$scope.buttons = Highcharts.getOptions().exporting.buttons.contextButton.menuItems;
		$scope.chartHeight = $window.innerHeight - 300 + "px";
		$scope.chartHeightStyle = { "height" : $scope.chartHeight };

		//1: Hour
		//2: Day
		//3: Week
		//4: Month
		//5: Year
		
		$scope.selectChartInterval = 2;
		var timeDiffInMs = $scope.endDate - $scope.startDate; 
		if ( timeDiffInMs >= 24*60*60*1000)
			$scope.selectChartInterval = 3;
		if ( timeDiffInMs >= 7*24*60*60*1000)
			$scope.selectChartInterval = 4;
		if ( timeDiffInMs >= 31*24*60*60*1000)
			$scope.selectChartInterval = 5;
		if ( timeDiffInMs >= 366*24*60*60*1000)
			$scope.selectChartInterval = 5;		
		
		ngDialog.open({
			scope: $scope,
			className: "ngdialog-theme-loading", 
			template: "view/loadingDialog.html",
            showClose: false,
            closeByEscape : false
		});
		
		$http({  
			url: appMainService.getDomainAddress() + '/rest/chartView/webSocketViewDataMobile/',
			method: 'GET',
	        params: {
		        deviceId: deviceId,
		        sensorId: deviceId,
		        dateFrom: $scope.startDate.toString(),
		        dateTo: $scope.endDate.toString(),
		        desc : false
	       }
		}).then(function (response) {
			$scope.isLoading = false;
			$scope.realTime = true;
			if (response.data.length > 0) {
				$scope.isSearchSuccess = true;
				ngDialog.close();
    			var coverDetectData = [];
    			var antennaOptimizedData = [];
    			var waterLevel1DetectData = [];
    			var waterLevel2DetectData = [];
    			var maxStepData = [];
    			var adcValueData = [];
    			
    			var coverLevelData = [];
    			var waterLevelData = []; 
    			var h2sData = []; 
    			var so2Data = []; 
    			var co2Data = []; 
    			var ch4Data = []; 
    			var nh3Data = []; 
    			var o2Data = []; 
    			var rssiData = []; 
    			var batteryData = [];
    			var packetCounterData = [];
				
    			$scope.waterLevelFromZeroMin = response.data[response.data.length-1].waterLevelFromZeroMin;
    			
       			for (var i=0; i < response.data.length; ++i) {
    				var sensorChartData = response.data[i];
    				
    				coverDetectData.push({
    					x: sensorChartData.dateLong, 
    					y: sensorChartData.coverDetect,
    				 	timeDiffStr: sensorChartData.timeDiffStr,
    				 	maxCoverDetect: 1
    				}); 
    				
    				waterLevel1DetectData.push({
    					x: sensorChartData.dateLong, 
    					y: sensorChartData.waterLevel1Detect,
    					timeDiffStr: sensorChartData.timeDiffStr,
    				});
    				
    				waterLevel2DetectData.push({
    					x: sensorChartData.dateLong, 
    					y: sensorChartData.waterLevel2Detect,
    					timeDiffStr: sensorChartData.timeDiffStr,
    				});
    				
    				coverLevelData.push({
    					x: sensorChartData.dateLong, 
    					y: sensorChartData.coverLevel,
    					timeDiffStr: sensorChartData.timeDiffStr,
    				});
    				
    				$scope.waterLevelFromTroughMax = sensorChartData.waterLevelFromTroughMax;
   
    				waterLevelData.push({
    					 x: sensorChartData.dateLong, 
	                     y: $scope.waterLevelChartType === "from_top" ?  sensorChartData.waterLevelFromZero2 : sensorChartData.waterLevelFromZero,
	                     waterLevelFromZero2: sensorChartData.waterLevelFromZero2,
	                     timeDiffStr: sensorChartData.timeDiffStr,
	                     waterLevelFromTroughMax: sensorChartData.waterLevelFromTroughMax,
	                     waterLevelFromZeroDiff: sensorChartData.waterLevelFromZeroDiff,
    				});
    				
    				if ( sensorChartData.h2s < 0) 
    					 sensorChartData.h2s = 0;
    					
    				h2sData.push({
   					 	 x: sensorChartData.dateLong, 
	                     y: sensorChartData.h2s,
	                     timeDiffStr: sensorChartData.timeDiffStr,
	                     maxH2s:  sensorChartData.maxH2s,
	                     h2sDiff: sensorChartData.h2sDiff
    				});
    				
    				if ( sensorChartData.so2 < 0) 
   					 	sensorChartData.so2 = 0;
    				
    				so2Data.push({
    					x: sensorChartData.dateLong, 
    				    y: sensorChartData.so2, 
	                    timeDiffStr:  sensorChartData.timeDiffStr,
	                    maxSo2: sensorChartData.maxSo2,
	                    so2Diff: sensorChartData.so2Diff
    				});
    				
    				if ( sensorChartData.co2 < 0) 
      					 sensorChartData.co2 = 0;
    		
    				co2Data.push({
    					x: sensorChartData.dateLong, 
    				    y: sensorChartData.co2, 
	                    timeDiffStr:  sensorChartData.timeDiffStr,
	                    maxCo2: sensorChartData.maxCo2,
	                    co2Diff: sensorChartData.co2Diff
    				});
    				
    				if ( sensorChartData.ch4 < 0) 
     					 sensorChartData.ch4 = 0;
    				
    				ch4Data.push({
    					x: sensorChartData.dateLong, 
    				    y: sensorChartData.ch4, 
	                    timeDiffStr:  sensorChartData.timeDiffStr,
	                    maxCh4: sensorChartData.maxCh4, 
	                    ch4Diff: sensorChartData.ch4Diff
    				});
    				
    				if ( sensorChartData.nh3 < 0) 
    					 sensorChartData.nh3 = 0;
    				
    				nh3Data.push({
    					x: sensorChartData.dateLong, 
    				    y: sensorChartData.nh3, 
	                    timeDiffStr:  sensorChartData.timeDiffStr,
	                    maxNh3: sensorChartData.maxNh3,
	                    nh3Diff: sensorChartData.nh3Diff
    				});
    				
    				if ( sensorChartData.o2 < 0) 
   					 	sensorChartData.o2 = 0;
    				
    				o2Data.push({
    					x: sensorChartData.dateLong, 
    					y: sensorChartData.o2, 
	                    timeDiffStr:  sensorChartData.timeDiffStr,
	                    maxO2: sensorChartData.maxO2,
	                    o2Diff: sensorChartData.o2Diff
    				});
    				
    				batteryData.push([sensorChartData.dateLong, sensorChartData.battery])
    				packetCounterData.push([sensorChartData.dateLong, sensorChartData.packetCounter]);
    				
    				rssiData.push([sensorChartData.dateLong, sensorChartData.rssi ]);
    			}
       				
       			var series = [];
       			var selectIdx = -1;
       			
       			$scope.coverLevelSeriesName = $translate.instant('COVER_LEVEL') + " (cm)";
       			$scope.waterLevelSeriesName = $translate.instant('WATER_LEVEL') + " (cm)";
       			$scope.h2sSeriesName =  $translate.instant('H2S') + " (ppm)";
       			$scope.so2SeriesName = $translate.instant('SO2') + " (ppm)";
       			$scope.coverDetectSeriesName = $translate.instant('COVER_DETECT') + " (1=Closed/0=Open)";
       			$scope.antennaOptimizedSeriesName =  $translate.instant('ANTENNA_OPTIMIZED');
       			$scope.waterLevel1DetectSeriesName = $translate.instant('WATER_LEVEL_1_DETECT') + " (1=High/0=Low)"; 
       			$scope.waterLevel2DetectSeriesName = $translate.instant('WATER_LEVEL_2_DETECT') + " (1=High/0=Low)";
       			$scope.maxStepSeriesName = $translate.instant('MAX_STEP');
       			$scope.adcSeriesName = $translate.instant('ADC_VALUE'); 
       			$scope.co2SeriesName = $translate.instant('CO2') + " (ppm)";
       			$scope.ch4SeriesName = $translate.instant('CH4') + " (ppm)";
       			$scope.nh3SeriesName = $translate.instant('NH3') + " (ppm)";
       			$scope.o2SeriesName = $translate.instant('O2') + " (ppm)";
       			$scope.rssiSeriesName = $translate.instant('RSSI') + " (dBm)"; 
       			
    			if ($scope.sensorType.name === "setCoverDetect") {
	     			var coverDetectObject = {
	     				type:'spline',
	     				name: $scope.coverDetectSeriesName,
		            	data: coverDetectData,
		            	color: $scope.color[0], 
		            	lineWidth: 2,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[0]
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		            	pointWidth: 3
	     			};
	     			series.push(coverDetectObject);
	            	$scope.setCoverDetectIdx = ++selectIdx; 
                }

    			if ($scope.sensorType.name === "setWaterLevel1Detect") {
	     			var waterLevel1DetectObject = {
	     				type:'spline',
	     				name: $scope.waterLevel1DetectSeriesName,
		            	data: waterLevel1DetectData,
		            	color: $scope.color[2], 
		            	lineWidth: 2,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[2]
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
	     			series.push(waterLevel1DetectObject);
	     			$scope.setWaterLevel1DetectIdx = ++selectIdx; 
                }
    			if ($scope.sensorType.name === "setWaterLevel2Detect") {
	     			var waterLevel2DetectObject = {
	     				type:'spline',
	     				name: $scope.waterLevel2DetectSeriesName,
		            	data: waterLevel2DetectData,
		            	color: $scope.color[3], 
		            	lineWidth: 2,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[3]
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
		     		series.push(waterLevel2DetectObject);
		        	$scope.setWaterLevel2DetectIdx = ++selectIdx; 
                }
    			
    			if ($scope.sensorType.name === "setCoverLevel") {
	     			var coverLevelObject = {
	     				type:'spline',
	     				name: $scope.coverLevelSeriesName,
		            	data: coverLevelData,
		            	color: $scope.color[8], 
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[8], 
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
				    series.push(coverLevelObject);
                	$scope.setCoverLevelIdx = ++selectIdx; 
                }
                if ($scope.sensorType.name === "setWaterLevel") {
                	
                	var waterLevelObject = {
	     				type:'spline',
		            	name: $scope.waterLevelSeriesName,
		            	data: waterLevelData,
		            	color: $scope.color[9], 
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[9] //'red'
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
				    series.push(waterLevelObject);
                	$scope.setWaterLevelIdx = ++selectIdx; 
                }
                if ($scope.sensorType.name === "setH2s") {
	     			var h2sObject = {
	     				type:'spline',
	     				name: $scope.h2sSeriesName,
		            	data: h2sData,
		            	color: $scope.color[10], 
		            	lineWidth: 1,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[10]
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		                pointWidth: 3
	     			};
				    series.push(h2sObject);
					$scope.setH2sIdx = ++selectIdx; 
                }
                if ($scope.sensorType.name === "setSo2" ) {
	     			var so2Object = {
	     				type:'spline',
	     				name: $scope.so2SeriesName,
		            	data: so2Data,
		            	color: $scope.color[11], 
		            	lineWidth: 2,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor:  $scope.color[11]
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
					series.push(so2Object);
                	$scope.setSo2Idx = ++selectIdx; 
                }
                if ($scope.sensorType.name === "setCo2" ) {
	     			var co2Object = {
	     				type:'spline',
	     				name: $scope.co2SeriesName,
		            	data: co2Data,
		            	color: $scope.color[12], 
		            	lineWidth: 2,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[12]
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
					series.push(co2Object);
                	$scope.setCo2Idx = ++selectIdx; 
                }
                if ($scope.sensorType.name === "setCh4" ) {
	     			var ch4Object = {
	     				type:'spline',
	     				name: $scope.ch4SeriesName,
		            	data: ch4Data,
		            	color: $scope.color[13], 
		            	lineWidth: 2,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[13],
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
					series.push(ch4Object);
                	$scope.setCh4Idx = ++selectIdx; 
                }
                if ($scope.sensorType.name === "setNh3") {
	     			var nh3Object = {
	     				type:'spline',
	     				name: $scope.nh3SeriesName,
		            	data: nh3Data,
		            	color: $scope.color[14], 
		            	lineWidth: 2,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[14],
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
					series.push(nh3Data);
                	$scope.setNh3Idx = ++selectIdx; 
                }
                if ($scope.sensorType.name === "setO2") {
             		var o2Object = {
	     				type:'spline',
	     				name: $scope.o2SeriesName,
		            	data: o2Data,
		            	color: $scope.color[15], 
		            	lineWidth: 2,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[15],
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
					series.push(o2Object);
                	$scope.setO2Idx = ++selectIdx; 
                }
          
                if ($scope.sensorType.name === "setRssi") {
          			var rssiObject = {
	     				type:'spline',
	     				name: $scope.rssiSeriesName,
		            	data: rssiData,
		            	color: $scope.color[16], 
		            	lineWidth: 2,
		            	marker: {
		            		enabled: true, // auto
		            		radius: 2,
		            		lineWidth: 2,
		            		lineColor: $scope.color[16]
		            	},
		            	tooltip: {
		            		valueDecimals: 2
		            	},
		            	dataGrouping: {
		                    enabled: false
		                },
		                pointWidth: 3
	     			};
					series.push(rssiObject);
	     		  	$scope.setRssiIdx = ++selectIdx; 
                }
                
    			Highcharts.setOptions({
    				global: {
               	        useUTC: false
               	    }
               	});
    	
				$scope.yAxisMinValue = 0;
    			if ($scope.sensorType.name === "setWaterLevel" && $scope.waterLevelChartType === "from_top" ) {
    				$scope.yAxisMinValue = $scope.waterLevelFromZeroMin - 10;
    			}
    			
    			$scope.yAxisMaxValue = 0; 
    			if ( $scope.sensorType.name === "setCoverDetect") 
    			{
    				$scope.yAxisMaxValue = 1;
    			}
    			
    			if ( $scope.sensorType.name === "setWaterLevel") 
    			{
    				if ( $scope.waterLevelChartType === "from_top" ) {
    					$scope.yAxisMaxValue = 10;
    				} else {
        				$scope.yAxisMaxValue = waterLevelData[waterLevelData.length-1].waterLevelFromTroughMax + 10;
    				}
    			}
    			if ( $scope.sensorType.name === "setH2s") 
    			{
    				$scope.yAxisMaxValue = h2sData[h2sData.length-1].maxH2s + 10;
    			}
        		
    			$scope.yAxisFixMaxValue = $scope.yAxisMaxValue;
    			
    			$scope.chart = new Highcharts.stockChart('chartContainer', {
    		        chart: {
    		            renderTo: 'container',
    		            type:'spline',
    		            zoomType: 'x',
    		            backgroundColor : {
    		                linearGradient : [0,0, 0, 300],
    		                stops : [
		                         [0, 'rgb(53, 174, 201)'],
		                         [1, 'rgb(0, 33, 115)']
    		                ]
    		            }
    		        },
    		        rangeSelector: {
    		        	allButtonsEnabled: true,
    		            buttons: [
    		            {
							type : 'minute',
							count : 1,
							text : $translate.instant('MINUTE'),
						}, 
    		            {
							type : 'hour',
							count : 1,
							text :  $translate.instant('HOUR'),
    				    }, 
    		            {
							type : 'day',
							count : 1,
							text : $translate.instant('DAY'),
    					},            
    		            {
							type : 'week',
							count : 1,
							text : $translate.instant('WEEK'),
    					}, 
    		            {
    		                type: 'month',
    		                count: 3,
    		                text: $translate.instant('MONTH'),
    		            }, 
    		            {
    		                type: 'year',
    		                count: 1,
    		                text: $translate.instant('YEAR'),
    		            },  
    		            {
    		                type: 'all',
    		                count: 1,
    		                text: $translate.instant('ALL'),
    		            }],
    		            buttonTheme: {
    		                width: 60
    		            },
    		            selected: $scope.selectChartInterval
    		        },
    		        title: {
    		        	text: ""
    		      	},
    		        xAxis: {
    		        	ordinal: false,
    		        	lineColor: '#3fff3f',
    		        	type: 'datetime',
    		        	tickInterval: 60 * 1000, //tick interval 1 minute
    		            labels: {
    		                style: {
    		                   color: '#3fff3f',
    		                   font: '12px Trebuchet MS, Verdana, sans-serif'
    		                }
    		             },
    		        },
    		        yAxis: {
    		        	tickInterval: 10,
						min: $scope.yAxisMinValue,
						max: $scope.yAxisMaxValue,
    		        	allowDecimals: !$scope.sensorType.name === "setWaterLevel",
    		        	opposite: false,
    		        	lineColor: '#3fff3f',
    		        	title: {
    		        		text: ""
    		        	},
    		        	gridLineColor: '#3fff3f',
    		            labels: {
    		                style: {
    		                   color: '#3fff3f',
    		                   font: '14px Trebuchet MS, Verdana, sans-serif'
    		                }
    		            },
    		        		 
    		        },
    		        plotOptions: {
    		        	spline: {
    		               	dataGrouping: {
    		                    enabled: false
    		                }
    		           	},
    		            series: {
    		            	turboThreshold: 0
    		            },
    		        },
    		        tooltip: {
    		        	shared: true,
    		            crosshairs: true,
    		            followPointer: true,
    		            followTouchMove: false,
    		            useHTML: true,
    		            headerFormat: '<span style="font-size: 12px"><b>{point.key}</b></span><br/><span style="color:#000000">Time Diff</span>: <b>{point.timeDiffStr}</b><br/>',
    		            pointFormatter: function() {
    		            	var text = '<span style="color:' + this.series.color + '">' + this.series.name + '</span>: <b>' + this.y + '</b><br/>';
    		            	if (this.series.name === $scope.waterLevelSeriesName) {
    		            		var waterLevelFromZeroDiffStr = "0"; 
        		            	if ( this.waterLevelFromZeroDiff > 0 ) 
        		            		waterLevelFromZeroDiffStr = Math.abs(this.waterLevelFromZeroDiff) + " cm <span style=\"font-size: 18px\">&uarr;</span>"; 
        		            	else if ( this.waterLevelFromZeroDiff < 0 ) 
        		            		waterLevelFromZeroDiffStr = Math.abs(this.waterLevelFromZeroDiff) + " cm <span style=\"font-size: 18px\">&darr;</span>" 
        		            	else 
        		            		waterLevelFromZeroDiffStr = Math.abs(this.waterLevelFromZeroDiff);
        		            	
    		            		text += '<span style="color:' + this.series.color + '">Water level Change</span>: <b>' + waterLevelFromZeroDiffStr + '</b><br/>';
    		            		text += '<span style="color:' + this.series.color + '">Water level Sensor Raw</span>: <b>' + this.waterLevelFromZero2 + '</b><br/>';
    		            		
    		            	}  
    		            		
    		             	if (this.series.name === $scope.h2sSeriesName) {
    		    		            		
    		            		var h2sDiffStr = "0"; 
        		            	if ( this.h2sDiff > 0 ) 
        		            		h2sDiffStr = Math.abs(this.h2sDiff) + " ppm <span style=\"font-size: 18px\">&uarr;</span>"; 
        		            	else if ( this.h2sDiff < 0 ) 
        		            		h2sDiffStr = Math.abs(this.h2sDiff) + " ppm <span style=\"font-size: 18px\">&darr;</span>" 
        		            	else 
        		            		h2sDiffStr = Math.abs(this.h2sDiff);
        		            	
    		             		text += '<span style="color:' + this.series.color + '">H2s Change</span>: <b>' + h2sDiffStr + '</b><br/>';
    		            	}
    		             	
    		             	if (this.series.name === $scope.so2SeriesName) {
    		             		
    		            		var so2DiffStr = "0"; 
        		            	if ( this.so2Diff > 0 ) 
        		            		so2DiffStr = Math.abs(this.so2Diff) + " ppm <span style=\"font-size: 18px\">&uarr;</span>"; 
        		            	else if ( this.h2sDiff < 0 ) 
        		            		so2DiffStr = Math.abs(this.so2Diff) + " ppm <span style=\"font-size: 18px\">&darr;</span>" 
        		            	else 
        		            		so2DiffStr = Math.abs(this.so2Diff);
        		            	
    		             		text += '<span style="color:' + this.series.color + '">So2 Change</span>: <b>' + so2DiffStr + '</b><br/>';
    		             	}
    	
    		            	return text;
    		            }
    		        },
    		        legend:{
    		          	enabled: true,
    		          	itemStyle: { "color": "#3fff3f", "cursor": "pointer", "fontSize": "12px", "fontWeight": "bold" }
    		        },
    		        exporting: {
    		            buttons: {
    		                contextButton: {
    		                    menuItems: $scope.buttons
    		                }
    		            },
    		            csv: {
    		                dateFormat: '%Y-%m-%d %H:%M:%S'    //dd/MM/yyyy HH:mm:ss
    		            }
    		        },
    		        navigator: {
    		            series: {
    		                includeInCSVExport: false
    		            
    		            
    		            }
    		        },
    		        series : series
    		   });
    			
    			
			} else {
				$scope.isSearchSuccess = false;
				$scope.loadingMessage = $translate.instant('NO_DATA_FOUND');
			}

		},function (response){
			$scope.isLoading = false;
			$scope.isSearchSuccess = false;
			$scope.loadingMessage = ("Failed to search data, status=" + response.status);
		});	
	};

	$scope.dataSet = null;
	
	$scope.fullSensorDatas = [];
	
	$scope.sensorType = {
      name: null
    };

    $scope.isShiftChart = function () {
    	return true;
    }
    
	$scope.$on('receiveSensorData', function(event, sensorDatas) {
		if ( $scope.realTime == false ) {
			return;
		}
		
		var deviceId = null;
		if ( $scope.isChartView == true && $scope.selectedTag != null ) {
			deviceId = $scope.selectedTag.deviceId;
		}
		if ( $scope.isChartView == false && $scope.selectDeviceId != null ) {
			deviceId = $scope.selectDeviceId;
		}
		
		for (var i = 0; i < sensorDatas.length; ++i) {
			var sensorDataInQ = sensorDatas[i];
 	   	
			if (deviceId == sensorDataInQ.deviceId ) {
				
 	    		if ($scope.setCoverDetectIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.coverDetect);
 	    			$scope.chart.series[$scope.setCoverDetectIdx].addPoint(point, true, $scope.isShiftChart());
 	            }
 	    		if ($scope.setAntennaOptimizedIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.antennaOptimized);
 	    			
 	    			$scope.chart.series[$scope.setAntennaOptimizedIdx].addPoint(point, true, $scope.isShiftChart());
 	            }
 	    		if ($scope.setWaterLevel1DetectIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.waterLevel1Detect);
 	    			$scope.chart.series[$scope.setWaterLevel1DetectIdx].addPoint(point, true, $scope.isShiftChart());
 	            }
 	    		if ($scope.setWaterLevel2DetectIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.waterLevel2Detect);
 	    			$scope.chart.series[$scope.setWaterLevel2DetectIdx].addPoint(point, true, $scope.isShiftChart());
 	            }
 	    		if ($scope.setMaxStepIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.maxStep);
 	    			$scope.chart.series[$scope.setMaxStepIdx].addPoint(point, true, $scope.isShiftChart());
 	            }
 	    		if ($scope.setAdcValueIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.adcValue);
 	    			$scope.chart.series[$scope.setAdcValueIdx].addPoint(point, true, $scope.isShiftChart());
 	            }
 	    		if ($scope.setBatteryIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.battery);
 	    			$scope.chart.series[$scope.setBatteryIdx].addPoint(point, true, $scope.isShiftChart());
 	            }
 	    		if ($scope.setPacketCounterIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.packetCounter);
 	    			$scope.chart.series[$scope.setPacketCounterIdx].addPoint(point, true, $scope.isShiftChart());
 	            }    		
 	    		if ($scope.setCoverLevelIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.coverLevel);
 	    			$scope.chart.series[$scope.setCoverLevelIdx].addPoint(point, true, $scope.isShiftChart());
 	            }   
 	    		if ($scope.setWaterLevelIdx > -1 && $scope.waterLevelFromTroughMax != null) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			
 	     			var waterLevelFromTrough = Math.abs(  $scope.waterLevelFromZeroMin - sensorDataInQ.waterLevelFromZero2 );
 	     			console.log('waterLevelFromTrough=' + waterLevelFromTrough);
 	     			point.push(waterLevelFromTrough);
 	    			
 	    			$scope.chart.series[$scope.setWaterLevelIdx].addPoint(point, true, $scope.isShiftChart());
 	            }   
 	    		if ($scope.setH2sIdx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.h2s);
 	    			$scope.chart.series[$scope.setH2sIdx].addPoint(point, true, $scope.isShiftChart());
 	            }   	
 	    		if ($scope.setSo2Idx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.so2);
 	    			$scope.chart.series[$scope.setSo2Idx].addPoint(point, true, $scope.isShiftChart());
 	            }    		
 	    		if ($scope.setCo2Idx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.co2);
 	    			$scope.chart.series[$scope.setCo2Idx].addPoint(point, true, $scope.isShiftChart());
 	            }    	
 	    		if ($scope.setCh4Idx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.ch4);
 	    			$scope.chart.series[$scope.setCh4Idx].addPoint(point, true, $scope.isShiftChart());
 	            }    
 	    		if ($scope.setNh3Idx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.nh3);
 	    			$scope.chart.series[$scope.setNh3Idx].addPoint(point, true, $scope.isShiftChart());
 	            } 
 	    		if ($scope.setO2Idx > -1) {
 	     			var point = [];
 	     			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.o2);
 	    			$scope.chart.series[$scope.setO2Idx].addPoint(point, true, $scope.isShiftChart());
 	            }     		
 	    		if ( $scope.setRssiIdx > -1) {
 	    			var point = [];
 	    			point.push(sensorDataInQ.date);
 	    			point.push(sensorDataInQ.rssi);
 	    			$scope.chart.series[$scope.setRssiIdx].addPoint(point, true, $scope.isShiftChart());
 	    		}
 	    		return;
 	   		}
		}
	});
	$rootScope.connectWebSocketData();

	$scope.userCancel = function () {
		window.history.back();
	};
	
	$scope.searchTagConfigurations = function () {
	   	$http({
	   		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/webSocketViewData/',
	   		method: 'GET',
	           params: {
	           id: null, 
	           name: null, 
	           deviceId : null,
	           sensorId : null,
	           groupId : null,
	           description: null,
	           siteName : null,
	           siteId : null
	          }
	   	}).then(function (response) {
	   		$scope.tagConfigurations = response.data;
	   		$scope.markers = [];
	   		
   			for (var i = 0; i < $scope.tagConfigurations.length; ++i) {
   				var tagConfiguration = $scope.tagConfigurations[i];

   				tagConfiguration.markerClassName = "fa fa-map-marker fa-stack-1x yellowColor";
	            if ( tagConfiguration.validationResult == true) {
		   			tagConfiguration.markerClassName = "fa fa-map-marker fa-stack-1x greenColor";
	            } else if ( tagConfiguration.validationResult == false ) {
		   			tagConfiguration.markerClassName = "fa fa-map-marker fa-stack-1x redColor";
	            }
	    	}
   			scrollToMarkers( $scope.tagConfigurations );
   			
     	},function (response){
	   		alert("Failed to search data, status=" + response.status);
	   	});
	};
   
	
	$scope.onSelectTagFromMap = function() {
		$scope.selectedTagConfigurationFromMap = $scope.selectedTag;
		$scope.selectedTagOrig = $scope.selectedTag;
		
		ngDialog.openConfirm({template: "view/selectTagLocationDialog.html",
			scope: $scope //Pass the scope object if you need to access in the template
		}).then(
			//Update
			function(value) {
				$scope.selectedTag = $scope.selectedTagConfigurationFromMap;
		 		if ( $scope.selectedTag != null ) {
					for ( var s=0; s < $scope.tagConfigurations.length; ++s ) {
						if ($scope.tagConfigurations[s].id == $scope.selectedTag.id) {
							$scope.selectedTag = $scope.tagConfigurations[s];
							$scope.onTagSelected($scope.selectedTag);
							break;
						}
					}
				}
		 	},
			//Cancel
			function(value) {
		 		$scope.selectedTag = $scope.selectedTagOrig;	
		 		if ( $scope.selectedTag != null ) {
					for ( var s=0; s < $scope.tagConfigurations.length; ++s ) {
						if ($scope.tagConfigurations[s].id == $scope.selectedTag.id) {
							$scope.selectedTag = $scope.tagConfigurations[s];
							$scope.onTagSelected($scope.selectedTag);
							break;
						}
					}
				}
			}		
		);				 
		
	};
	
	$scope.chartWidthOrig = $rootScope.sideMenuOn == false ? document.getElementById("chartContainer").offsetWidth : document.getElementById("chartContainer").offsetWidth + 230 ;
	$scope.chartWidth = $rootScope.sideMenuOn == true ?  $scope.chartWidthOrig - 230 + "px" : $scope.chartWidthOrig + "px";
	
	$scope.chartInnerStyle= { "height" : $scope.chartHeight, "width" :  $scope.chartWidth };
	
	$scope.$on('dashBoardViewToggle', function(event, sideMenuOn) {
		if ( $scope.isSearchSuccess == false ) {
			return;
		}
		if ( sideMenuOn == true ) {
			$scope.chartWidth = $scope.chartWidthOrig - 230 + "px";
			$scope.chartInnerStyle= { "height" : $scope.chartHeight, "width" :  $scope.chartWidth };
		}
		if ( sideMenuOn == false ) {
			$scope.chartWidth = $scope.chartWidthOrig + "px";
			$scope.chartInnerStyle= { "height" : $scope.chartHeight, "width" :  $scope.chartWidth };
		}
	
		$scope.searchSensorData();
	});
	
	$scope.onChangeYAxis = function() {		
		$scope.chart.yAxis[0].update({
			min: $scope.yAxisMinValue,
			max: $scope.yAxisMaxValue
		}); 
	};
	
 	$http({
   		url: appMainService.getDomainAddress() + '/rest/diagnosticView/webSocketViewDataByDeviceSensor/',
   		method: 'GET',
          params: {
        	  deviceId: '-1',
		      sensorId: '-1' 
	      }
   	}).then(function (response) {

   	},function (response){
   	});
 	
 	$scope.waterLevelChartType = "from_top";
 	
 	$scope.onWaterLevelChartType = function(chartType) {
 		$scope.waterLevelChartType = chartType;
 	};
 	
}]);