angular.module('dsd').controller('floorPlanViewCtrl', ['$rootScope','$scope', '$interval', '$timeout','$routeParams', '$http', '$location' , 'appMainService','ngDialog', '$window',
                                                         function ($rootScope, $scope, $interval, $timeout, $routeParams, $http, $location, appMainService, ngDialog, $window){
	
	var height = $window.innerHeight - 140  + "px";
	$scope.mapStyle = { 'height' : height };
	
	$scope.siteConfigurations = [];
	$scope.tagConfigurations = [];
	
	$scope.getTagConfigurations = function() {
		return $scope.tagConfigurations;
	};
	
	$scope.getSiteConfigurations = function () {
		return $scope.siteConfigurations;
	};
	
	$scope.getSelectedSite = function() {
		var siteConfiguration = appMainService.getSiteConfiguration();
		if ( siteConfiguration != null) {
			$scope.selectedSite = siteConfiguration;
		}
		return $scope.selectedSite;
	};
	
	$scope.getSelectedTagConfiguration = function() {
		return appMainService.getTagConfiguration();
	};
	
	$scope.selectedSite = appMainService.getSiteConfiguration();
	
	$http({
		url: appMainService.getDomainAddress() + '/rest/siteConfiguration/searchData/',
		method: 'GET',
        params: {
        id: null, 
        name: null, 
        description: null,
        defaultSite: null
       }
	}).then(function (response) {
		$scope.siteConfigurations = response.data;
		if ( $scope.siteConfigurations.length > 0 ) {
			if ($scope.selectedSite != null) {
				for (var i = 0; i < $scope.siteConfigurations.length; ++i) {
					if ( $scope.selectedSite.id == $scope.siteConfigurations[i].id ) {
						$scope.selectedSite = $scope.siteConfigurations[i];
						$timeout(function() {$scope.searchTagConfigurations($scope.selectedSite)}, 500); 
						break;
					}
				}
			}
//			else {	
//				$scope.selectedSite = $scope.siteConfigurations[0];
//				$timeout(function() {$scope.searchTagConfigurations($scope.selectedSite)}, 500); 
//			}
		}
	},function (response){
 		alert("Failed to find site configuration list, status=" + response.status);
	});

   $scope.enrichContent = function(enrichedContent, tagConfiguration ) {
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.id_", 'g'), tagConfiguration.id);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.name_", 'g'), tagConfiguration.name);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.dateString2_", 'g'), tagConfiguration.dateString2);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.deviceId_", 'g'), tagConfiguration.deviceId);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.readerIds_", 'g'), tagConfiguration.readerIds);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.coverDetectStr_", 'g'), tagConfiguration.coverDetectStr);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.waterLevel_", 'g'), tagConfiguration.waterLevel);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.coverLevel_", 'g'), tagConfiguration.coverLevel);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.waterLevel1DetectStr_", 'g'), tagConfiguration.waterLevel1DetectStr);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.waterLevel2DetectStr_", 'g'), tagConfiguration.waterLevel2DetectStr);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.h2s_", 'g'), tagConfiguration.h2s);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.so2_", 'g'), tagConfiguration.so2);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.ch4_", 'g'), tagConfiguration.ch4);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.rssi_", 'g'), tagConfiguration.rssi);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.batteryStr_", 'g'), tagConfiguration.batteryStr);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.packetCounter_", 'g'), tagConfiguration.packetCounter);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.nh3_", 'g'), tagConfiguration.nh3);
	   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.co2_", 'g'), tagConfiguration.co2);

	   if ( tagConfiguration.imageUrlPath != null ) {
		   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.display_", 'g'), "block");
		   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.imageUrlPath_", 'g'), tagConfiguration.imageUrlPath);
	   } else {
		   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.display_", 'g'), "none");
		   enrichedContent = enrichedContent.replace(new RegExp("_tagConfiguration.imageUrlPath_", 'g'), "");
	   }
	   return enrichedContent;
   };
   
   $scope.updateMarker = function(sensorData) {
	   var marker = getMarkerByDeviceId(sensorData.deviceId);
	   if (marker == null) {
		   return;
	   }
	   
	   var tagConfiguration = marker.tagConfiguration; 
	   
	   tagConfiguration.readerIds = sensorData.readerIds;
	   tagConfiguration.dateString2 = sensorData.dateString2;
	   tagConfiguration.coverLevel = sensorData.coverLevel; 
	   tagConfiguration.waterLevel = sensorData.waterLevelFromZero2; 
	   tagConfiguration.h2s = sensorData.h2s; 
	   tagConfiguration.so2 = sensorData.so2; 
	   tagConfiguration.co2 = sensorData.co2; 
	   tagConfiguration.ch4 = sensorData.ch4; 		   				
	   tagConfiguration.nh3 = sensorData.nh3; 			   				
	   tagConfiguration.o2 = sensorData.o2; 	

		if (!tagConfiguration.isCoverLevel) {
			tagConfiguration.coverLevel = "--";
		}
		if (!tagConfiguration.isWaterLevel) {
			tagConfiguration.waterLevel = "--";
		}
		if (!tagConfiguration.isH2s) {
			tagConfiguration.h2s = "--";
		}		   		
		if (!tagConfiguration.isSo2) {
			tagConfiguration.so2 = "--";
		}
		if (!tagConfiguration.isCo2) {
			tagConfiguration.co2 = "--";
		}		   	
		if (!tagConfiguration.isCh4) {
			tagConfiguration.ch4 = "--";
		}		   		
		if (!tagConfiguration.isNh3) {
			tagConfiguration.nh3 = "--";
		}
		if (!tagConfiguration.isO2) {
			tagConfiguration.o2 = "--";
		}
						
	   tagConfiguration.measurePeriod = sensorData.measurePeriod;	
	   tagConfiguration.timeDiffInMsStr = sensorData.timeDiffInMsStr;

	   tagConfiguration.coverDetectStr = sensorData.coverDetectStr;
	   tagConfiguration.waterLevel1DetectStr = sensorData.waterLevel1DetectStr;
	   tagConfiguration.waterLevel2DetectStr = sensorData.waterLevel2DetectStr;
	   tagConfiguration.antennaOptimizedStr = sensorData.antennaOptimizedStr;
	   tagConfiguration.maxStep = sensorData.maxStep;
	   tagConfiguration.adcValue = sensorData.adcValue;

	   tagConfiguration.rssi = sensorData.rssi;
	   tagConfiguration.packetCounter = sensorData.packetCounter;
	   tagConfiguration.battery = sensorData.battery;
	   tagConfiguration.batteryStr = sensorData.batteryStr;
			
	   
	   var content = $('.markerInfoWindow').html();
	   var enrichedContent =   $scope.enrichContent(content, tagConfiguration );
	   marker.infoWindow.setContent(enrichedContent);
    };

	$scope.getAllRecentSensorConfig = function(tagConfigurations, siteConfiguration) {
		
		var date = new Date();
		var startDate = new Date(date.getTime() - 24*60*60*1000);
		$http({  
			url: appMainService.getDomainAddress() + '/rest/diagnosticView/getRecentSensorChartDataAllDeviceId/',
			params: {
				dateFrom: startDate.getTime().toString(),
				dateTo: date.getTime().toString() 
			}, 
			method: 'GET',
		}).then(function (response) {
			for (var k=0; k < tagConfigurations.length; ++k ) {
				
				var tagConfiguration = tagConfigurations[k];
				
   				tagConfiguration.readerIds = "--";
   				tagConfiguration.dateString2 = "--";
   				tagConfiguration.coverLevel = "--";
   				tagConfiguration.waterLevel = "--";
   				tagConfiguration.h2s = "--";
   				tagConfiguration.so2 = "--";
   				tagConfiguration.co2 = "--";
   				tagConfiguration.ch4 = "--";  				
   				tagConfiguration.nh3 = "--";			   				
   				tagConfiguration.o2 = "--";
   				tagConfiguration.readerIds = "--";
   				
   				tagConfiguration.measurePeriod = "--";
   				tagConfiguration.timeDiffInMsStr = "--";
   				
   				tagConfiguration.coverDetectStr = "--";
   				tagConfiguration.waterLevel1DetectStr = "--";
   				tagConfiguration.waterLevel2DetectStr = "--";
				tagConfiguration.antennaOptimizedStr = "--";
				tagConfiguration.maxStep = "--";
				tagConfiguration.adcValue = "--";
				
   				tagConfiguration.rssi = "--";
   				tagConfiguration.packetCounter = "--";
   				tagConfiguration.battery = "--";
   				tagConfiguration.batteryStr = "--";
   				for (var j = 0; j < response.data.length; ++j ) {
					var respData = response.data[j];
					if ( respData.deviceId == tagConfiguration.deviceId) {
						
		   				tagConfiguration.readerIds = respData.readerIds;
		   				tagConfiguration.dateString2 = respData.dateString2;
		   				tagConfiguration.coverLevel = respData.coverLevel; 
		   				tagConfiguration.waterLevel = respData.waterLevelFromZero2; 
		   				tagConfiguration.h2s = respData.h2s; 
		   				tagConfiguration.so2 = respData.so2; 
		   				tagConfiguration.co2 = respData.co2; 
		   				tagConfiguration.ch4 = respData.ch4; 		   				
		   				tagConfiguration.nh3 = respData.nh3; 			   				
		   				tagConfiguration.o2 = respData.o2; 	
		   				
		   				tagConfiguration.measurePeriod = respData.measurePeriod;	
		   				tagConfiguration.timeDiffInMsStr = respData.timeDiffInMsStr;
		   				
		   				tagConfiguration.coverDetectStr = respData.coverDetectStr;
		   				tagConfiguration.waterLevel1DetectStr = respData.waterLevel1DetectStr;
		   				tagConfiguration.waterLevel2DetectStr = respData.waterLevel2DetectStr;
						tagConfiguration.antennaOptimizedStr = respData.antennaOptimizedStr;
						tagConfiguration.maxStep = respData.maxStep;
						tagConfiguration.adcValue = respData.adcValue;
						
		   				tagConfiguration.rssi = respData.rssi;
		   				tagConfiguration.packetCounter = respData.packetCounter;
		   				tagConfiguration.battery = respData.battery;
		   				tagConfiguration.batteryStr = respData.batteryStr;
		   						
		   				if (!tagConfiguration.isCoverLevel) {
		   					tagConfiguration.coverLevel = "--";
		   		   		}
		   		   		if (!tagConfiguration.isWaterLevel) {
		   		   			tagConfiguration.waterLevel = "--";
		   		   		}
		   		   		if (!tagConfiguration.isH2s) {
		   		   			tagConfiguration.h2s = "--";
		   		   		}		   		
		   		   		if (!tagConfiguration.isSo2) {
		   		   			tagConfiguration.so2 = "--";
		   		   		}
		   		   		if (!tagConfiguration.isCo2) {
		   		   			tagConfiguration.co2 = "--";
		   		   		}		   	
		   		   		if (!tagConfiguration.isCh4) {
		   		   			tagConfiguration.ch4 = "--";
		   		   		}		   		
		   		   		if (!tagConfiguration.isNh3) {
		   		   			tagConfiguration.nh3 = "--";
		   		   		}
		   		   		if (!tagConfiguration.isO2) {
		   		   			tagConfiguration.o2 = "--";
		   		   		}
						
		   				break;
					}
				}
			}
			scrollToMarkers( $scope.tagConfigurations, siteConfiguration);
		},function (response){
			alert("Failed to search data, status=" + response.status);
		});	
	};
	
	$scope.searchTagConfigurations = function (siteConfiguration) {
		$http({
	   		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/webSocketViewData/',
	   		method: 'GET',
	           params: {
	           id: null, 
	           name: null, 
	           deviceId : null,
	           sensorId : null,
	           groupId : null,
	           description: null,
	           siteName : null,
	           siteId : siteConfiguration.id
	          }
	   	}).then(function (response) {
	   		$scope.tagConfigurations = response.data;
	   		$scope.getAllRecentSensorConfig($scope.tagConfigurations, siteConfiguration);

     	},function (response){
	   		alert("Failed to search data, status=" + response.status);
	   	});
	};
   
	
	$scope.$on('receiveSensorData', function(event, datas) {
    	$scope.updateMarker(datas[0]);
	});
	
	
	$rootScope.connectWebSocketData();
	
 	$http({
   		url: appMainService.getDomainAddress() + '/rest/diagnosticView/webSocketViewDataByDeviceSensor/',
   		method: 'GET',
          params: {
        	  deviceId: '-1',
		      sensorId: '-1'
	      }
   	}).then(function (response) {
   	},function (response){
   	});
 	
	$scope.userCancel = function () {
		window.history.back();
	};
	
	$scope.openTagConfiguration = function (tagConfigurationId, command) {
		$http({
    		url: appMainService.getDomainAddress() + '/rest/tagConfiguration/searchData/',
    		method: 'GET',
            params: {
            id: tagConfigurationId, 
            name: null, 
            deviceId : null,
            deviceAddress : null,
            groupId : null,
            description: null,
            siteName : null,
            siteId : null,
           }
    	}).then(function (response) {
    		if (response.data != null && response.data.length > 0) {
    			var tagConfiguration = response.data[0];   			
    			if ( command == "TableView") {
    				appMainService.setTagConfiguration(tagConfiguration);
    				appMainService.setTableViewSiteId(tagConfiguration.siteConfigurationId);
    				appMainService.setTableViewGroupId(tagConfiguration.groupId);
    				$location.path('View/TableView/');  
    			}
    			
    			if ( command == "ChartView") {
    				appMainService.setTagConfiguration(tagConfiguration);
    				$location.path('View/ChartView/');  
    			}
    			
    			if ( command == "TagConfiguration") {
    				appMainService.setTagConfiguration(tagConfiguration);
    				var path = 'SystemConfiguration/TagConfiguration/New/';
    				$location.path(path);	
    			}
 
    			if ( command == "AlertHistory") {
    				appMainService.setTagConfiguration(tagConfiguration);
    				var path = 'Alert/AlertHistory/';
    				$location.path(path);	
    			}
    			
    			if ( command == "TagSensorView") {
    				appMainService.setTagConfiguration(tagConfiguration);
    				var path = 'View/SensorView/';
    				$location.path(path);	
    			}
    		}
    	},function (response){
     		alert("Failed to search data, status=" + response.status);
    	});
	};
	
	
}]);