angular.module('dsd').controller('opexCtrl', ['$scope', '$uibModal', '$log', '$http', '$sce', '$location', 'uiGridConstants', 'appMainService',
 function ($scope, $uibModal, $log, $http, $sce, $location, uiGridConstants, appMainService) {
    $scope.section="";
	$scope.users=[
	];
	$scope.roles=[
	];
	$scope.modules = [];
	$scope.staticData={};
	$scope.animationsEnabled = true;
	$scope.showUserMain = false;
	$scope.showRoleMain = false;
	$scope.userTotalItems = 100;
	$scope.userCurrentPage = 1;
	$scope.gridActions = [{name:"Actions on selected rows..."}, {name:"Delete"}];
	$scope.userButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">Profile </a></li><li><a href="#">Impersonate </a></li><li><a href="logout">Logout</a></li></ul></div>');
	$scope.helpButtonTemplate = $sce.trustAsHtml('<div class="top-popup"><ul><li><a href="#">About</a></li><li><a href="#">User Guide</a></li></ul></div>');
	
	$scope.collapseAll = function(array, isCollapsed){
		for (var i = 0; i < array.length; i++){
			array[i].isCollapsed = isCollapsed;
		}
	};
	$scope.getTableStyle= function() {
        var marginHeight = 50;
        var length = $scope.users.length; 
        return {
            height: (length * $scope.userGridOption.rowHeight + $scope.userGridOption.headerRowHeight + marginHeight ) + "px"
        };
    };
	
	
	
	$scope.toggleCompanyFiltering = function(){
		$scope.companyGridOption.enableFiltering = !$scope.companyGridOption.enableFiltering;
	    $scope.companyGridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
	 
	};
	
    $scope.searchUser = function (searchId, searchChiName, searchEngName, searchLoginId, searchEmail, searchDept) {
        $http({
            url: appMainService.getDomainAddress() + '/rest/user/searchData/',
            method: 'GET',
            params: {
            	id: searchId, 
            	chiName: searchChiName, 
            	engName: searchEngName, 
            	userName: searchLoginId, 
            	email: searchEmail, 
            	dept: searchDept
            }
        }).then(function (response) {
            $scope.users = response.data;
//                                                        	$scope.userTotalItems = response.data.length;
        },function (response){
  		  alert("Failed to search data, status=" + response.status);
        });
    };
    
    
    $scope.refresh = function () {
    	$scope.searchMenu(null);
        $http({
            url: appMainService.getDomainAddress() + '/rest/user/getLoginUser/',
            method: 'GET',
        }).then(function (response) {
            $scope.currentUser = response.data;
        },function (response){
  		  	alert("Failed to get login user, status=" + response.status);
        });
    };
    $scope.refresh();
       
    //System Settings
    $scope.showSystemSetting = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'view/systemSettingView.html',
			controller: 'SystemSettingCtrl',
			size: 'lg',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
    
    
    
    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });
 
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.persons = {currentPage : 1};
	
	//User Maintenance
	$scope.openOpex = function (opexId) {
		var path = 'SystemData/OpexMaintenance/Edit/';
		path += (opexId+"");
		$location.path(path);
	};

	$scope.removeOpex = function (row){
		var path = 'SystemData/OpexMaintenance/Delete/';
		path += (row.entity.id+"");
		$location.path(path);
	};
	
	$scope.opexTotalItems = 100;
	$scope.opexCurrentPage = 1;
	$scope.opexs=[];
	$scope.opexGridOption={
		data:'opexs', 
		minRowsToShow: 30,
		enableFiltering: true,
		useExternalFiltering: true,
		multiSelect: true,
		enableRowSelection: true,
		enableHorizontalScrollbar: 0,
		enableVerticalScrollbar: 0,
		onRegisterApi: function(gridApi){
			$scope.opexGridApi = gridApi;
			$scope.opexGridApi.core.on.filterChanged( $scope, function() {
				var grid = this.grid;
				var id = grid.columns.filter(function (item){
					return item.field == "id";
				})[0].filters[0].term;
				var mainCategoryCode = grid.columns.filter(function (item){
					return item.field == "mainCategoryCode";
				})[0].filters[0].term;
				var mainCategoryName = grid.columns.filter(function (item){
					return item.field == "mainCategoryName";
				})[0].filters[0].term;
				var categoryCode = grid.columns.filter(function (item){
					return item.field == "categoryCode";
				})[0].filters[0].term;
				var categoryName = grid.columns.filter(function (item){
					return item.field == "categoryName";
				})[0].filters[0].term;
				var subCategoryCode = grid.columns.filter(function (item){
					return item.field == "subCategoryCode";
				})[0].filters[0].term;
				var subCategoryName = grid.columns.filter(function (item){
					return item.field == "subCategoryName";
				})[0].filters[0].term;				
				var remark = grid.columns.filter(function (item){
					return item.field == "remark";
				})[0].filters[0].term;
				
				$scope.searchOpex(id, mainCategoryCode, mainCategoryName, categoryCode, categoryName, subCategoryCode, subCategoryName, remark);
		    });
		},
		columnDefs: [
		    {field:'info', width: 30,
			 		headerCellTemplate: '<a href="javascript:void(0);" ng-click="grid.appScope.toggleFiltering()"><img src="img/normal/filter.png" width="24" height="24" ></img></a>', 
			 		cellTemplate: '<img src="img/normal/info.png" width="24" height="24"></img>'},
			{field:'id', displayName: 'ID', 
			 	cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openOpex(row.entity.id)"><a>{{row.entity.id}}</a></div>'},
			{field:'mainCategoryCode', displayName: 'Main Category Code', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openOpex(row.entity.id)"><a>{{row.entity.mainCategoryCode}}</a></div>'},
			{field:'mainCategoryName', displayName: 'Main Category Name', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openOpex(row.entity.id)"><a>{{row.entity.mainCategoryName}}</a></div>'},
			{field:'categoryCode', displayName:'Category Code', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openOpex(row.entity.id)"><a>{{row.entity.categoryCode}}</a></div>'},
			{field:'categoryName', displayName:'Category Name', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openOpex(row.entity.id)"><a>{{row.entity.categoryName}}</a></div>'},
			{field:'subCategoryCode', displayName:'Sub Category Code', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openOpex(row.entity.id)"><a>{{row.entity.subCategoryCode}}</a></div>'},
			{field:'subCategoryName', displayName:'Sub Category Name', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openOpex(row.entity.id)"><a>{{row.entity.subCategoryName}}</a></div>'},
			{field:'remark', displayName:'Remark', 
				cellTemplate: '<div class="ui-grid-cell-contents" ng-class="col.colIndex()" ng-click="grid.appScope.openOpex(row.entity.id)"><a>{{row.entity.remark}}</a></div>'}
	    ]
	};
	
	$scope.openOpex = function (opexId) {
		var path = 'SystemData/OpexMaintenance/Edit/';
		path += (opexId+"");
		$location.path(path);    		
	};
	
	$scope.newOpex = function (){
		var parentScope = $scope;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'newCompanyWindow.html',
			controller: 'NewCompanyWindowCtrl',
			resolve: {
				parentScope: function (){
					return parentScope;
				}
			}
		});
	};
	
    $scope.searchOpex = function (searchId, mainCategoryCode, mainCategoryName, categoryCode, categoryName, subCategoryCode, subCategoryName, remark) {
    	$http({
    		url: appMainService.getDomainAddress() + '/rest/opex/searchData/',
    		method: 'GET',
            params: {
            id: searchId, 
            mainCategoryCode: mainCategoryCode, 
            mainCategoryName: mainCategoryName, 
            categoryCode: categoryCode, 
            categoryName: categoryName, 
            subCategoryCode: subCategoryCode, 
            subCategoryName: subCategoryName, 
            remark: remark
           }
    	}).then(function (response) {
    		$scope.opexs = response.data;
    	},function (response){
     		  alert("Failed to search data, status=" + response.status);
    	});
    };
    
    $scope.createOpexOk = function () {
		$http({
	        url: appMainService.getDomainAddress() + '/rest/opex/addData',
	        method: 'POST',
	        params: {
	        	mainCategoryCode: $scope.newMainCategoryCode,
	        	mainCategoryName: $scope.newMainCategoryName,
	        	categoryCode: $scope.newCategoryCode,
	        	categoryName: $scope.newCategoryName,
	        	subCategoryCode: $scope.newSubCategoryCode,
	        	subCategoryName: $scope.newSubCategoryName,
	        	remark: $scope.newRemark
	        }
	    }).then(function (response) {
		  alert("Add opex success");
		  $location.path('SystemData/OpexMaintenance');
	    },function (response){
			  alert("Failed to add opex, status=" + response.status);
	    });
	};

	
	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};

	
}]);


angular.module('dsd').controller('opexEditCtrl', ['$scope', '$routeParams', '$http', '$location' , 'appMainService',
                                                    function ($scope, $routeParams, $http, $location, appMainService){
	
	$scope.editOpexId = $routeParams.opexId;
	$http({
        url: appMainService.getDomainAddress() + '/rest/opex/findOpexById/',
        method: 'GET',
        params: {
        	id: $routeParams.opexId
        }
    }).then(function (response) {
    	$scope.editMainCategoryCode = response.data.mainCategoryCode;
		$scope.editMainCategoryName = response.data.mainCategoryName;
		$scope.editCategoryCode = response.data.categoryCode;
		$scope.editCategoryName = response.data.categoryName;
		$scope.editSubCategoryCode = response.data.subCategoryCode;
		$scope.editSubCategoryName = response.data.subCategoryName;
		$scope.editRemark = response.data.remark;
		$scope.editOpexId = response.data.id;
	
    },function (response){
		  alert("Failed to find opex (ID = "+$scope.editOpexId+"), status=" + response.status);
    });
	
	
    $scope.editOpexOk = function () {
		$http({
	        url: appMainService.getDomainAddress() + '/rest/opex/updateData/',
	        method: 'PUT',
	        params: {
	        	id: $routeParams.opexId,
	        	mainCategoryCode: $scope.editMainCategoryCode,
	        	mainCategoryName: $scope.editMainCategoryName,
	        	categoryCode: $scope.editCategoryCode,
	        	categoryName: $scope.editCategoryName,
	        	subCategoryCode: $scope.editSubCategoryCode,
	        	subCategoryName: $scope.editSubCategoryName,
	        	remark: $scope.editRemark
	        }
	    }).then(function (response) {
	    	alert("Edit opex success");
		  	$location.path('SystemData/OpexMaintenance');
	    },function (response){
	    	alert("Failed to edit opex, status=" + response.status);
	    });
	};
	
	$scope.removeOpex = function (){
		$http({
            url: appMainService.getDomainAddress() + '/rest/opex/deleteData/',
            method: 'DELETE',
            params: {
            	id: $routeParams.opexId
            }
        }).then(function (response) {
      	    alert("Delete opex success");
      	    $location.path('SystemData/OpexMaintenance');
        },function (response){
  		  alert("Failed to delete opex, status=" + response.status);
        });
	};
	
	$scope.userCancel = function () {
		//$location.path('Admin/UserMaintenance');
		window.history.back();
	};
}]);
